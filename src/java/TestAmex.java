import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import amexsubmisionfile.AmexData;
import amexsubmisionfile.AmexSubTest;
import amexsubmisionfile.TransactionControl;

@WebServlet(name = "test", urlPatterns = { "/TestAmex" })
public class TestAmex extends HttpServlet {

	private static final long serialVersionUID = -7617667799175150679L;
	private static final Logger logger = LoggerFactory.getLogger(TestAmex.class);
	private static final String HTML_TEMPLATE = "<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"es\" lang=\"es\"> <head> <title>Resumen Estado Cuenta MobileCard AMEX</title> <!-- Meta Tags --> <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />  <style type=\"text/css\"> * { padding: 0; margin: 0; } body { font-size: 62.5%; background-color: rgb(255, 255, 255); font-family: verdana, arial, sans-serif; } .page-container { width: 600px; margin: 0px auto; margin-top: 10px; margin-bottom: 10px; border: solid 1px rgb(150, 150, 150); font-size: 1.0em; } /* HEADER */ .header { width: 600px; height: 170px; font-family: \"trebuchet ms\", arial, sans-serif; overflow: visible !important ; overflow: hidden ; } .main-content { display: inline; /; float: left; width: 600px; margin: 0 0 0 30px; overflow: visible !important ; overflow: hidden ; margin-bottom: 10px !important ; margin-bottom: 5px /*IE6*/; }  .main-content h1.pagetitle { margin: 0 0 0.4em 0; padding: 0 0 2px 0; font-family: Verdana, Geneva, sans-serif; color: #000000; font-weight: bold; font-size: 180%; } .main-content p { margin: 0 0 1.0em 0; line-height: 1.5em; font-size: 120%; text-align: justify; color: #000000; } .main-content table { clear: both; width: 90%; margin: 0 0 0 0; table-layout: fixed; border-collapse: collapse; empty-cells: show; background-color: rgb(233, 232, 244); text-align: justify; } .main-content table td { height: 2.5em; padding: 2px 5px 2px 5px; border-left: solid 2px rgb(255, 255, 255); border-right: solid 2px rgb(255, 255, 255); border-top: solid 2px rgb(255, 255, 255); border-bottom: solid 2px rgb(255, 255, 255); background-color: rgb(225, 225, 225); text-align: center; font-weight: normal; color: #000000; font-size: 11.5px; }  .footer { clear: both; width: 600px; height: 7em; padding: 1.1em 0 0; background: rgb(225, 225, 225); font-size: 1.0em; overflow: visible !important ; overflow: hidden; } .footer p { line-height: 1.3em; text-align: center; color: rgb(125, 125, 125); font-weight: bold; font-size: 104%; } </style>  </head> <body> <div class=\"page-container\"> <div class=\"header\"> <img src=\"https://www.mobilecard.mx:8443/AmexSubFile/resources/images/MobileCard_header_600.PNG\" alt=\"http://www.mobilecard.mx\" /> </div> <br>  <div class=\"main-content\">  <h1 class=\"pagetitle\">Resumen Estado de Cuenta AMEX...</h1>  <p> Fecha: <strong>#FECHA#</strong> </p>  <h3>&nbsp;</h3> <h3>&nbsp;</h3> <table align=\"center\" cellpadding=\"3\" id=\"respuesta\"> <tbody> #CADENA# </tbody> </table> <h3>&nbsp;</h3>  </div> <div class=\"footer\"> <p>MobileCard - ABC Capital © 2015 | Todos los derechos  reservados.</p> <p>Powered by Addcel.</p> </div> </div> </body> </html> ";

	protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = response.getWriter();
		try {
			String anio = null;
			String mes = null;
			String dia = null;
			String afiliacion = null;
			String[] afiliaciones = null;
			StringBuilder cadena = new StringBuilder();
			String cadenaResumen = HTML_TEMPLATE;

			if (request.getParameter("anio") != null) {
				anio = request.getParameter("anio").toString();
			}
			if (request.getParameter("mes") != null) {
				mes = request.getParameter("mes").toString();
			}
			if (request.getParameter("dia") != null) {
				dia = request.getParameter("dia").toString();
			}
			if (request.getParameter("afiliacion") != null) {
				afiliacion = request.getParameter("afiliacion").toString();
			}
			if(afiliacion != null && !"".equals(afiliacion)){
				afiliaciones = new String[1];
				afiliaciones[0] = afiliacion;
			}else{
				afiliaciones = new String[6];
				afiliaciones[0] = "9351358974"; // TAE_ADDCEL
				afiliaciones[1] = "9351669578"; // IAVE&PASE&TUTAG
				afiliaciones[2] = "9351669081"; // VIAPASS
				afiliaciones[3] = "9351669032"; // TELEVIA
				afiliaciones[4] = "9351736476"; // INTERJET
				// Julio 9 de 2015
				afiliaciones[5] = "9353192983"; // CJGRANDSHOPPING
			}
			
			
			cadena.append("<TR>");
			cadena.append("<TD><strong>COMERCIO</strong></TD>");
			cadena.append("<TD><strong>AFILIACION</strong></TD>");
			cadena.append("<TD><strong>NUM. REGISTROS</strong></TD>");
			cadena.append("</TR>");

			AmexSubTest test = new AmexSubTest();
			for (int i = 0; i < afiliaciones.length; i++) {
				logger.debug("***    AFILIACION: " + afiliaciones[i] + "    ****");
				AmexData[] data = TransactionControl.getData(afiliaciones[i], anio, mes, dia);
				cadena.append("<TR><TD>");
				cadena.append(afiliaciones[i].equals("9351358974")? "TAE_ADDCEL":
							afiliaciones[i].equals("9351669578")? "IAVE&PASE&TUTAG":
							afiliaciones[i].equals("9351669081")? "VIAPASS":
							afiliaciones[i].equals("9351669032")? "TELEVIA":
							afiliaciones[i].equals("9351736476")? "INTERJET":
							afiliaciones[i].equals("9353192983")? "CJGRANDSHOPPING":"Desconosida");
				cadena.append("</TD><TD>");
				cadena.append(afiliaciones[i]);
				cadena.append("</TD><TD>");
				cadena.append(data != null? data.length: 0);
				cadena.append("</TD></TR>");
				
				if ((data != null) && (data.length > 0)) {
					logger.debug("***" + afiliaciones[i] + "  , Total Registros: " + data.length + "****");
					test.executeSubmitFile(afiliaciones[i], anio, mes, dia);
				}
			}
			cadenaResumen = cadenaResumen.replaceAll("#FECHA#", anio + "-" + mes + "-" + dia);
			cadenaResumen = cadenaResumen.replaceAll("#CADENA#", cadena.toString());
			
			out.println(cadenaResumen);
		} catch (Exception ex) {
			logger.error("Error al generar el archivo conciliacion amex: ", ex);
		} finally {
			out.close();
		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	public String getServletInfo() {
		return "Short description";
	}
}
