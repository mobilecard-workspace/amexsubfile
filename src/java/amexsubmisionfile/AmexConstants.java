/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package amexsubmisionfile;

import com.americanexpress.ips.gfsg.enumerations.CurrencyCode;

/**
 *
 * @author carlosgs
 */
public class AmexConstants {
  public static String AMEX_APPROVAL_CODE_LENGHT = "6";
  public static String AMEX_TERMINAL = "00000001";
  public static String AMEX_ACCEPTOR_ID = "9351358974";
  public static String AMEX_POS = "1009S0S00000";
  public static String AMEX_CURRENCY_CODE = CurrencyCode.Mexican_Peso.getCurrencyCode();
  public static String AMEX_ACQUIRER_REFERENCE_DATA = "000071169033272";
  public static String AMEX_FORWARD_INSTITUTION_ID_CODE = "\\";
  public static String AMEX_TPP_NAME = "S#ADDCEL.COM";
  public static String AMEX_TPP_STREET = "PASEO DE LA ASUNCION";
  public static String AMEX_TPP_CITY = "MEXICO";
  public static String AMEX_TPP_POSTALCODE = "52149";
}
