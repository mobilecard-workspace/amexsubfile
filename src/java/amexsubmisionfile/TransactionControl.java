package amexsubmisionfile;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TransactionControl {
	private static final Logger logger = LoggerFactory.getLogger(TransactionControl.class);
	static int transactionId = 0;
	
	private static final String SELECT_DATA = 
			"SELECT "+
			"   transaccionLocalDateTime, "+
			"   transaccionMonto, "+
			"   transaccionTarjeta, "+
			"   transaccionExpira, "+
			"   transaccionCid, "+
			"   transaccionTipo, "+
			"   transaccionAccReference, "+
			"   transaccionApprovalCode, "+
			"   transaccionIdn, "+
			"   transaccionReversal, "+
			"   transaccionTipoTarjeta, "+
			"   transaccionIdn, "+
			"   transaccionAcceptorId "+
			"FROM "+
			"   ecommerce.Transaccion "+
			"WHERE "+
			"   DATE_FORMAT(transaccionDate,'%Y-%m-%d') = '#FECHA#' "+
			"   and transaccionReversal='N' "+
			"   and transaccionProcesada='N' "+
			"   and MensajeId='000' "+
			"   and transaccionAcceptorId = '#AFILIACION#' "+
			"   and transaccionApprovalCode is not null ";
	
	private static final String UPDATE_DATA = "UPDATE Transaccion SET transaccionProcesada='P'  WHERE transaccionIdn = ";
	
	public static void updateProcesada(String transactionIdn) {
		DatabaseImpl ds = new DatabaseImpl();
		Connection conn = null;
		Statement st = null;
		ResultSet rs = null;
		String query = "";
		try {
			conn = ds.getConnection();
			st = conn.createStatement();

			query = UPDATE_DATA + transactionIdn;
			logger.debug("Update:" + query);
			st.executeUpdate(query);

		} catch (Exception e) {
			logger.debug("ECOMMERCE POOL: General Exception: ", e);
		} finally {
			ds.closeConPrepStResSet(st, rs, conn);
		}
	}
	
	public static AmexData[] getData(String acceptorId, String year, String month, String day) {
		DatabaseImpl ds = new DatabaseImpl();
		Connection conn = null;
		Statement st = null;
		ResultSet rs = null;
		String query = "";
		AmexData data = null;
		ArrayList<AmexData> list = new ArrayList<AmexData>();
		AmexData[] resultado = null;
		try {
			conn = ds.getConnection();
			st = conn.createStatement();

			query =  SELECT_DATA.replaceAll("#AFILIACION#", acceptorId).replaceAll("#FECHA#", year + "-" + month + "-" + day);			
			logger.debug("getData: " + query);
			
			rs = st.executeQuery(query);
			while (rs.next()) {
				data = new AmexData();
				data.setLocalTransactionDateAndTime(rs.getString(1));
				data.setAmount(rs.getString(2));
				data.setPrimaryAccountNumber(rs.getString(3));
				data.setCardExpirationDate(rs.getString(4));
				data.setCid(rs.getString(5));
				data.setMessageType(rs.getString(6));
				data.setAccReferenceData(rs.getString(7));
				data.setApprovalCode(rs.getString(8));
				data.setSystemTraceAuditNumber(rs.getString(9));
				data.setIsReversal(rs.getString(10));
				data.setTipoTarjeta(rs.getString(11));
				data.setTransactionIdn(rs.getString(12));
				data.setAcceptorId(rs.getString(13));
				list.add(data);
			}
			resultado = new AmexData[list.size()];
			for (int i = 0; i < list.size(); i++) {
				resultado[i] = ((AmexData) list.get(i));
			}
		} catch (Exception e) {
			logger.debug("ECOMMERCE POOL: General Exception: ", e);
		} finally {
			ds.closeConPrepStResSet(st, rs, conn);
			list = null;
		}
		return resultado;
	}


//	public static void updateTransaction(int transactionId, AmexResponse response) {
//		updateTransaction(transactionId, response, "");
//	}

//	public static void updateTransaction(int transactionId, AmexResponse response, String accReference) {
//		DatabaseImpl ds = new DatabaseImpl();
//		Connection conn = null;
//		Statement st = null;
//		ResultSet rs = null;
//		String query = "";
//		InitialContext ic = null;
//		int lastError = 0;
//		try {
//			conn = ds.getConnection();
//			st = conn.createStatement();
//
//			query = "LOCK TABLES Transaccion WRITE";
//			st.execute(query);
//
//			query = "UPDATE Transaccion SET MensajeId = '" + response.codigo + "', " + " transaccionAccReference = '" + accReference + "', " + " transaccionApprovalCode = '" + response.approvalCode
//					+ "' " + " WHERE transaccionIdn=" + transactionId;
//
//			logger.debug("Insert:" + query);
//			st.executeUpdate(query);
//			if (response.hasErrors().booleanValue()) {
//				query = "LOCK TABLES Error WRITE";
//				st.execute(query);
//
//				query = "SELECT ifnull(max(errorIdn),0) FROM Error";
//				rs = st.executeQuery(query);
//				if (rs.next()) {
//					lastError = rs.getInt(1);
//				}
//				List errores = response.getErrores();
//				for (int i = 0; i < errores.size(); i++) {
//					AmexResponse error = (AmexResponse) errores.get(i);
//					query = "INSERT INTO Errores (ErrorIdn, ErrorCodigo ,ErrorDsc,transaccionIdn) VALUES (" + lastError + ",'" + error.codigo + "','" + error.mensaje + "'," + transactionId + ")";
//
//					logger.debug("Insert:" + query);
//					st.executeUpdate(query);
//					lastError++;
//				}
//			}
//			query = "UNLOCK TABLES";
//			st.execute(query);
//		} catch (Exception e) {
//			logger.debug("ECOMMERCE POOL: General Exception: ", e);
//		} finally {
//			try {
//				if ((conn != null) && (!conn.isClosed()) && (st != null) && (!st.isClosed())) {
//					st.execute("UNLOCK TABLES");
//				}
//				if ((st != null) && (st.isClosed())) {
//					st.close();
//					st = null;
//				}
//				if (st != null) {
//					st = null;
//				}
//				if ((conn != null) && (conn.isClosed())) {
//					conn.close();
//					conn = null;
//				}
//				if (conn != null) {
//					conn = null;
//				}
//				ds = new DatabaseImpl();
//				conn = ds.getConnection();
//				st = conn.createStatement();
//				st.execute("UNLOCK TABLES");
//				st.close();
//				conn.close();
//				if ((conn != null) && (!conn.isClosed())) {
//					conn.close();
//				}
//			} catch (Exception e) {
//				try {
//					if ((conn != null) && (conn.isClosed())) {
//						conn.close();
//						conn = null;
//					}
//					if (conn != null) {
//						conn = null;
//					}
//					logger.debug("ECOMMERCE POOL: Error interno al intentar desbloquear tablas: ", e);
//				} catch (Exception ex) {
//					logger.debug("ECOMMERCE POOL: Error interno de base de datos: ", ex);
//				}
//			}
//		}
//	}
//
//	public static List<AmexTransaction> getTransactions(String year, String month, String day) {
//		DatabaseImpl ds = new DatabaseImpl();
//		Connection conn = null;
//		Statement st = null;
//		ResultSet rs = null;
//		String query = "";
//		AmexTransaction data = null;
//		ArrayList<AmexTransaction> list = new ArrayList();
//		AmexTransaction[] resultado = null;
//		try {
//			conn = ds.getConnection();
//			st = conn.createStatement();
//
//			query = "SELECT transaccionIdn, transaccionId, transaccionLocalDateTime, transaccionDsc, \ntransaccionUsuario, transaccionTarjeta, transaccionExpira, transaccionMonto, transaccionCid, \ntransaccionNombres, transaccionApellidos, transaccionDireccion, transaccionTelefono, \ntransaccionEmail, transaccionCp, MensajeId, transaccionTipo, transaccionSubTipo, \ntransaccionReversal, transaccionReversalIdn, transaccionAccReference, transaccionApprovalCode, \ntransaccionDate, transaccionProcesada, transaccionTipoTarjeta, transaccionAcceptorId  \n"
//					+ "FROM Transaccion  \nWHERE MensajeId='000' and transaccionReversal='N'  \nand transaccionApprovalCode is not null  \nand transaccionProcesada='N' \nand transaccionDate >= str_to_date('"
//					+ year + "-" + month + "-" + day + "','%Y-%m-%d') \n";
//
//			rs = st.executeQuery(query);
//			logger.debug("ECOMMERCE: " + query);
//			while (rs.next()) {
//				data = new AmexTransaction();
//				data.setTransaccionIdn(rs.getString(1));
//				data.setTransaccionId(rs.getString(2));
//				data.setTransaccionLocalDateTime(rs.getString(3));
//				data.setTransaccionDsc(rs.getString(4));
//				data.setTransaccionUsuario(rs.getString(5));
//				data.setTransaccionTarjeta(rs.getString(6));
//				data.setTransaccionExpira(rs.getString(7));
//				data.setTransaccionMonto(rs.getString(8));
//				data.setTransaccionCid(rs.getString(9));
//				data.setTransaccionNombres(rs.getString(10));
//				data.setTransaccionApellidos(rs.getString(11));
//				data.setTransaccionDireccion(rs.getString(12));
//				data.setTransaccionTelefono(rs.getString(13));
//				data.setTransaccionEmail(rs.getString(14));
//				data.setTransaccionCp(rs.getString(15));
//				data.setMensajeId(rs.getString(16));
//				data.setTransaccionTipo(rs.getString(17));
//				data.setTransaccionSubTipo(rs.getString(18));
//				data.setTransaccionReversal(rs.getString(19));
//				data.setTransaccionReversalIdn(rs.getString(20));
//				data.setTransaccionAccReference(rs.getString(21));
//				data.setTransaccionApprovalCode(rs.getString(22));
//				data.setTransaccionDate(rs.getString(24));
//				data.setTransaccionProcesada(rs.getString(25));
//				data.setTransaccionTipoTarjeta(rs.getString(26));
//				data.setTransaccionAcceptorId(rs.getString(27));
//				list.add(data);
//			}
//		} catch (Exception e) {
//			logger.debug("ECOMMERCE POOL: General Exception: ", e);
//		} finally {
//			try {
//				if ((st != null) && (st.isClosed())) {
//					st.close();
//					st = null;
//				}
//				if (st != null) {
//					st = null;
//				}
//				if ((conn != null) && (conn.isClosed())) {
//					conn.close();
//					conn = null;
//				}
//				if (conn != null) {
//					conn = null;
//				}
//			} catch (Exception e) {
//				try {
//					if ((conn != null) && (conn.isClosed())) {
//						conn.close();
//						conn = null;
//					}
//					if (conn != null) {
//						conn = null;
//					}
//					logger.debug("ECOMMERCE POOL: Error interno al intentar desbloquear tablas: ", e);
//				} catch (Exception ex) {
//					logger.debug("ECOMMERCE POOL: Error interno de base de datos: ", ex);
//				}
//			}
//		}
//		return list;
//	}
//
//	public static void updateReversal(long originalTransactionId, long reversalTransactionId) {
//		DatabaseImpl ds = new DatabaseImpl();
//		Connection conn = null;
//		Statement st = null;
//		ResultSet rs = null;
//		String query = "";
//		InitialContext ic = null;
//		int lastError = 0;
//		try {
//			conn = ds.getConnection();
//			st = conn.createStatement();
//
//			query = "LOCK TABLES Transaccion WRITE";
//			st.execute(query);
//
//			query = "UPDATE Transaccion SET transaccionReversal='R', transaccionReversalIdn=" + reversalTransactionId + " WHERE transaccionIdn=" + originalTransactionId;
//
//			logger.debug("Update:" + query);
//			st.executeUpdate(query);
//
//			query = "UNLOCK TABLES";
//			st.execute(query);
//		} catch (Exception e) {
//			logger.debug("ECOMMERCE POOL: General Exception: ", e);
//		} finally {
//			try {
//				if ((conn != null) && (!conn.isClosed()) && (st != null) && (!st.isClosed())) {
//					st.execute("UNLOCK TABLES");
//				}
//				if ((st != null) && (st.isClosed())) {
//					st.close();
//					st = null;
//				}
//				if (st != null) {
//					st = null;
//				}
//				if ((conn != null) && (conn.isClosed())) {
//					conn.close();
//					conn = null;
//				}
//				if (conn != null) {
//					conn = null;
//				}
//				ds = new DatabaseImpl();
//				conn = ds.getConnection();
//				st = conn.createStatement();
//				st.execute("UNLOCK TABLES");
//				st.close();
//				conn.close();
//				if ((conn != null) && (!conn.isClosed())) {
//					conn.close();
//				}
//			} catch (Exception e) {
//				try {
//					if ((conn != null) && (conn.isClosed())) {
//						conn.close();
//						conn = null;
//					}
//					if (conn != null) {
//						conn = null;
//					}
//					logger.debug("ECOMMERCE POOL: Error interno al intentar desbloquear tablas: ", e);
//				} catch (Exception ex) {
//					logger.debug("ECOMMERCE POOL: Error interno de base de datos: ", ex);
//				}
//			}
//		}
//	}

//	public static int createTransaction(String localDateTime, String id, String dsc, String usr, String tarjeta, String expira, String monto, String cid, String nombres, String apellidos,
//			String direccion, String telefono, String email, String cp, String mensajeId, String transaccionTipo, String transaccionSubTipo) {
//		DatabaseImpl ds = new DatabaseImpl();
//		Connection conn = null;
//		Statement st = null;
//		ResultSet rs = null;
//		String query = "";
//		InitialContext ic = null;
//		try {
//			conn = ds.getConnection();
//			st = conn.createStatement();
//			int lastIdn = 0;
//
//			query = "LOCK TABLES Transaccion WRITE";
//			st.execute(query);
//
//			query = "SELECT ifnull(max(transaccionIdn),0) FROM Transaccion";
//			rs = st.executeQuery(query);
//			if (rs.next()) {
//				lastIdn = rs.getInt(1);
//			}
//			lastIdn++;
//			transactionId = lastIdn;
//
//			query = "INSERT INTO Transaccion (transaccionIdn, transaccionId, transaccionLocalDateTime,transaccionDsc,transaccionUsuario,transaccionTarjeta, transaccionExpira, transaccionMonto, transaccionCid, transaccionNombres,transaccionApellidos,transaccionDireccion,transaccionTelefono,transaccionEmail,transaccionCp,transaccionTipo,transaccionSubTipo,MensajeId) VALUES ("
//					+ lastIdn
//					+ ",'"
//					+ id
//					+ "','"
//					+ localDateTime
//					+ "','"
//					+ dsc
//					+ "','"
//					+ usr
//					+ "'"
//					+ ",'"
//					+ tarjeta
//					+ "','"
//					+ expira
//					+ "','"
//					+ monto
//					+ "','"
//					+ cid
//					+ "','"
//					+ nombres
//					+ "',"
//					+ "'"
//					+ apellidos + "','" + direccion + "','" + telefono + "','" + email + "','" + cp + "'," + "'" + transaccionTipo + "','" + transaccionSubTipo + "','" + mensajeId + "')";
//
//			logger.debug("Insert:" + query);
//			st.executeUpdate(query);
//
//			query = "UNLOCK TABLES";
//			st.execute(query);
//		} catch (Exception e) {
//			logger.debug("ECOMMERCE POOL: General Exception: ", e);
//		} finally {
//			try {
//				if ((conn != null) && (!conn.isClosed()) && (st != null) && (!st.isClosed())) {
//					st.execute("UNLOCK TABLES");
//				}
//				if ((st != null) && (st.isClosed())) {
//					st.close();
//					st = null;
//				}
//				if (st != null) {
//					st = null;
//				}
//				if ((conn != null) && (conn.isClosed())) {
//					conn.close();
//					conn = null;
//				}
//				if (conn != null) {
//					conn = null;
//				}
//				ds = new DatabaseImpl();
//				conn = ds.getConnection();
//				st = conn.createStatement();
//				st.execute("UNLOCK TABLES");
//				st.close();
//				conn.close();
//				if ((conn != null) && (!conn.isClosed())) {
//					conn.close();
//				}
//			} catch (Exception e) {
//				try {
//					if ((conn != null) && (conn.isClosed())) {
//						conn.close();
//						conn = null;
//					}
//					if (conn != null) {
//						conn = null;
//					}
//					logger.debug("ECOMMERCE POOL: Error interno al intentar desbloquear tablas: ", e);
//				} catch (Exception ex) {
//					logger.debug("ECOMMERCE POOL: Error interno de base de datos: ", ex);
//				}
//			}
//		}
//		return transactionId;
//	}

//	public static AmexData getData(long transactionIdn, String systemTraceAuditNumber) {
//		DatabaseImpl ds = new DatabaseImpl();
//		Connection conn = null;
//		Statement st = null;
//		ResultSet rs = null;
//		String query = "";
//		AmexData data = new AmexData();
//		try {
//			conn = ds.getConnection();
//			st = conn.createStatement();
//
//			query = "SELECT transaccionLocalDateTime,transaccionMonto, transaccionTarjeta,transaccionExpira, transaccionCid,transaccionTipo,transaccionAccReference  FROM Transaccion WHERE transaccionIdn="
//					+ transactionIdn;
//
//			rs = st.executeQuery(query);
//			if (rs.next()) {
//				data.setLocalTransactionDateAndTime(rs.getString(1));
//				data.setAmount(rs.getString(2));
//				data.setPrimaryAccountNumber(rs.getString(3));
//				data.setCardExpirationDate(rs.getString(4));
//				data.setCid(rs.getString(5));
//				data.setMessageType(rs.getString(6));
//				data.setAccReferenceData(rs.getString(7));
//				data.setSystemTraceAuditNumber(systemTraceAuditNumber);
//			}
//		} catch (Exception e) {
//			logger.debug("ECOMMERCE POOL: General Exception: ", e);
//		} finally {
//			try {
//				if ((st != null) && (st.isClosed())) {
//					st.close();
//					st = null;
//				}
//				if (st != null) {
//					st = null;
//				}
//				if ((conn != null) && (conn.isClosed())) {
//					conn.close();
//					conn = null;
//				}
//				if (conn != null) {
//					conn = null;
//				}
//			} catch (Exception e) {
//				try {
//					if ((conn != null) && (conn.isClosed())) {
//						conn.close();
//						conn = null;
//					}
//					if (conn != null) {
//						conn = null;
//					}
//					logger.debug("ECOMMERCE POOL: Error interno al intentar desbloquear tablas: ", e);
//				} catch (Exception ex) {
//					logger.debug("ECOMMERCE POOL: Error interno de base de datos: ", ex);
//				}
//			}
//		}
//		return data;
//	}
//
//	public static AmexData[] getData() {
//		DatabaseImpl ds = new DatabaseImpl();
//		Connection conn = null;
//		Statement st = null;
//		ResultSet rs = null;
//		String query = "";
//		AmexData data = null;
//		ArrayList<AmexData> list = new ArrayList();
//		AmexData[] resultado = null;
//		try {
//			conn = ds.getConnection();
//			st = conn.createStatement();
//
//			query = "SELECT transaccionLocalDateTime,transaccionMonto, transaccionTarjeta,transaccionExpira, transaccionCid, \ntransaccionTipo,transaccionAccReference, transaccionApprovalCode,transaccionIdn,\ntransaccionReversal, transaccionTipoTarjeta,transaccionIdn, transaccionAcceptorId \n"
//					+ "FROM Transaccion  \nWHERE transaccionId='1900' and MensajeId='000' and transaccionReversal='N' \n";
//
//			rs = st.executeQuery(query);
//			logger.debug("ECOMMERCE: " + query);
//			while (rs.next()) {
//				data = new AmexData();
//				data.setLocalTransactionDateAndTime(rs.getString(1));
//				data.setAmount(rs.getString(2));
//				data.setPrimaryAccountNumber(rs.getString(3));
//				data.setCardExpirationDate(rs.getString(4));
//				data.setCid(rs.getString(5));
//				data.setMessageType(rs.getString(6));
//				data.setAccReferenceData(rs.getString(7));
//				data.setApprovalCode(rs.getString(8));
//				data.setSystemTraceAuditNumber(rs.getString(9));
//				data.setIsReversal(rs.getString(10));
//				data.setTipoTarjeta(rs.getString(11));
//				data.setTransactionIdn(rs.getString(12));
//				data.setAcceptorId(rs.getString(13));
//				list.add(data);
//			}
//			resultado = new AmexData[list.size()];
//			for (int i = 0; i < list.size(); i++) {
//				resultado[i] = ((AmexData) list.get(i));
//			}
//		} catch (Exception e) {
//			logger.debug("ECOMMERCE POOL: General Exception: ", e);
//		} finally {
//			try {
//				if ((st != null) && (st.isClosed())) {
//					st.close();
//					st = null;
//				}
//				if (st != null) {
//					st = null;
//				}
//				if ((conn != null) && (conn.isClosed())) {
//					conn.close();
//					conn = null;
//				}
//				if (conn != null) {
//					conn = null;
//				}
//			} catch (Exception e) {
//				try {
//					if ((conn != null) && (conn.isClosed())) {
//						conn.close();
//						conn = null;
//					}
//					if (conn != null) {
//						conn = null;
//					}
//					logger.debug("ECOMMERCE POOL: Error interno al intentar desbloquear tablas: ", e);
//				} catch (Exception ex) {
//					logger.debug("ECOMMERCE POOL: Error interno de base de datos: ", ex);
//				}
//			}
//		}
//		return resultado;
//	}
//	public static AmexData[] getData(String acceptorId) {
//		return getData(acceptorId, "2013", "01", "13");
//	}
//
//	public static AmexData getData(long transactionIdn) {
//		DatabaseImpl ds = new DatabaseImpl();
//		Connection conn = null;
//		Statement st = null;
//		ResultSet rs = null;
//		String query = "";
//		AmexData data = new AmexData();
//		try {
//			conn = ds.getConnection();
//			st = conn.createStatement();
//
//			query = "SELECT transaccionLocalDateTime,transaccionMonto, transaccionTarjeta,transaccionExpira, transaccionCid,transaccionTipo,transaccionAccReference  FROM Transaccion WHERE transaccionIdn="
//					+ transactionIdn;
//
//			rs = st.executeQuery(query);
//			if (rs.next()) {
//				data.setLocalTransactionDateAndTime(rs.getString(1));
//				data.setAmount(rs.getString(2));
//				data.setPrimaryAccountNumber(rs.getString(3));
//				data.setCardExpirationDate(rs.getString(4));
//				data.setCid(rs.getString(5));
//				data.setMessageType(rs.getString(6));
//				data.setAccReferenceData(rs.getString(7));
//				data.setSystemTraceAuditNumber(transactionIdn + "");
//			}
//		} catch (Exception e) {
//			logger.debug("ECOMMERCE POOL: General Exception: ", e);
//		} finally {
//			try {
//				if ((st != null) && (st.isClosed())) {
//					st.close();
//					st = null;
//				}
//				if (st != null) {
//					st = null;
//				}
//				if ((conn != null) && (conn.isClosed())) {
//					conn.close();
//					conn = null;
//				}
//				if (conn != null) {
//					conn = null;
//				}
//			} catch (Exception e) {
//				try {
//					if ((conn != null) && (conn.isClosed())) {
//						conn.close();
//						conn = null;
//					}
//					if (conn != null) {
//						conn = null;
//					}
//					logger.debug("ECOMMERCE POOL: Error interno al intentar desbloquear tablas: ", e);
//				} catch (Exception ex) {
//					logger.debug("ECOMMERCE POOL: Error interno de base de datos: ", ex);
//				}
//			}
//		}
//		return data;
//	}
//
//	public static void setBitacora() {
//		DatabaseImpl ds = new DatabaseImpl();
//		Connection conn = null;
//		Statement st = null;
//		ResultSet rs = null;
//		String query = "";
//		InitialContext ic = null;
//		try {
//			conn = ds.getConnection();
//			st = conn.createStatement();
//			int lastIdn = 0;
//
//			query = "LOCK TABLES Bitacora WRITE";
//			st.execute(query);
//
//			query = "SELECT ifnull(max(BitacoraIdn),0) FROM Bitacora";
//			rs = st.executeQuery(query);
//			if (rs.next()) {
//				lastIdn = rs.getInt(1);
//			}
//			lastIdn++;
//
//			logger.debug("Insert:" + query);
//			st.executeUpdate(query);
//
//			query = "UNLOCK TABLES";
//			st.execute(query);
//		} catch (Exception e) {
//		} finally {
//			try {
//				if ((conn != null) && (!conn.isClosed()) && (st != null) && (!st.isClosed())) {
//					st.execute("UNLOCK TABLES");
//				}
//				if ((st != null) && (st.isClosed())) {
//					st.close();
//					st = null;
//				}
//				if (st != null) {
//					st = null;
//				}
//				if ((conn != null) && (conn.isClosed())) {
//					conn.close();
//					conn = null;
//				}
//				if (conn != null) {
//					conn = null;
//				}
//				ds = new DatabaseImpl();
//				conn = ds.getConnection();
//				st = conn.createStatement();
//				st.execute("UNLOCK TABLES");
//				st.close();
//				conn.close();
//				if ((conn != null) && (!conn.isClosed())) {
//					conn.close();
//				}
//			} catch (Exception e) {
//				try {
//					if ((conn != null) && (conn.isClosed())) {
//						conn.close();
//						conn = null;
//					}
//					if (conn != null) {
//						conn = null;
//					}
//					logger.debug("ECOMMERCE POOL: Error interno al intentar desbloquear tablas: ", e);
//				} catch (Exception ex) {
//					logger.debug("ECOMMERCE POOL: Error interno de base de datos: ", ex);
//				}
//			}
//		}
//	}
}
