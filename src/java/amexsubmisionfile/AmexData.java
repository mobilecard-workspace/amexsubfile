/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package amexsubmisionfile;

/**
 *
 * @author carlosgs
 */
public class AmexData {

    private String transactionIdn;
    private String messageType;
    private String processingCode;
    private String primaryAccountNumber;
    private String cardExpirationDate;
    private String systemTraceAuditNumber;
    private String localTransactionDateAndTime;
    private String amount;
    private double numberAmount;
    private String cid;
    private String acceptorId;
    private String billingCP;
    private String billingDireccion;
    private String billingNombre;
    private String billingApellido;
    private String billingTel;
    private String billingEmail;
    private String shipToCP;
    private String shipToDireccion;
    private String shipToNombre;
    private String shipToApellido;
    private String shipToTel;
    private String shipToEmail;
    private String serviceIdentifier;
    private String typeIdentifier;
    private String actionCode;
    private String actionDsc;
    private String accReferenceData;
    private String approvalCode;
    private String isReversal;
    private String tipoTarjeta;
    private String paymentType;
    private String installations;
    private String dpp;

    public String toString() {
        return "systemTraceAuditNumber=" + this.systemTraceAuditNumber + "\n" + "processingCode=" + this.processingCode + "\n" + "messageType=" + this.messageType + "\n" + "primaryAccountNumber=" + this.primaryAccountNumber + "\n" + "cardExpirationDate=" + this.cardExpirationDate + "\n" + "localTransactionDateAndTime=" + this.localTransactionDateAndTime + "\n" + "amount=" + this.amount + "\n" + "cid=" + this.cid + "\n\n" + "billingCP=" + this.billingCP + "\n" + "billingDireccion=" + this.billingDireccion + "\n" + "billingNombre=" + this.billingNombre + "\n" + "billingApellido=" + this.billingApellido + "\n" + "billingTel=" + this.billingTel + "\n" + "billingEmail=" + this.billingEmail + "\n\n" + "shipToCP=" + this.shipToCP + "\n" + "shipToDireccion=" + this.shipToDireccion + "\n" + "shipToNombre=" + this.shipToNombre + "\n" + "shipToApellido=" + this.shipToApellido + "\n" + "shipToTel=" + this.shipToTel + "\n" + "shipToEmail=" + this.shipToEmail + "\n\n" + "serviceIdentifier=" + this.serviceIdentifier + "\n" + "typeIdentifier=" + this.typeIdentifier + "\n";
    }

    public void processResponse(String amexResponse) {
        String[] lineas = amexResponse.split("\n");
        if ((lineas != null) && (lineas.length > 0)) {
            for (int i = 0; i < lineas.length; i++) {
                String[] parms = lineas[i].split("::");
                if ((parms != null) && (parms.length > 0)) {
                    if (parms[0].trim().equals("APPROVAL CODE")) {
                        this.approvalCode = parms[1].trim();
                    }
                    if (parms[0].trim().equals("ACQUIRER REFERENCE DATA")) {
                        this.accReferenceData = parms[1].trim();
                    }
                    if (parms[0].trim().equals("ACTION CODE")) {
                        setActionCode(parms[1].trim());
                        setActionDsc(parms[2].trim());
                    }
                }
            }
        }
    }

    public String getPrimaryAccountNumber() {
        return this.primaryAccountNumber;
    }

    public void setPrimaryAccountNumber(String primaryAccountNumber) {
        this.primaryAccountNumber = primaryAccountNumber;
    }

    public String getCardExpirationDate() {
        return this.cardExpirationDate;
    }

    public void setCardExpirationDate(String cardExpirationDate) {
        this.cardExpirationDate = cardExpirationDate;
    }

    public String getSystemTraceAuditNumber() {
        return this.systemTraceAuditNumber;
    }

    public void setSystemTraceAuditNumber(String systemTraceAuditNumber) {
        this.systemTraceAuditNumber = systemTraceAuditNumber;
    }

    public String getLocalTransactionDateAndTime() {
        return this.localTransactionDateAndTime;
    }

    public void setLocalTransactionDateAndTime(String localTransactionDateAndTime) {
        this.localTransactionDateAndTime = localTransactionDateAndTime;
    }

    public String getAmount() {
        return this.amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCid() {
        return this.cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getMessageType() {
        return this.messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getBillingCP() {
        return this.billingCP;
    }

    public void setBillingCP(String billingCP) {
        this.billingCP = billingCP;
    }

    public String getBillingDireccion() {
        return this.billingDireccion;
    }

    public void setBillingDireccion(String billingDireccion) {
        this.billingDireccion = billingDireccion;
    }

    public String getBillingNombre() {
        return this.billingNombre;
    }

    public void setBillingNombre(String billingNombre) {
        this.billingNombre = billingNombre;
    }

    public String getBillingApellido() {
        return this.billingApellido;
    }

    public void setBillingApellido(String billingApellido) {
        this.billingApellido = billingApellido;
    }

    public String getBillingTel() {
        return this.billingTel;
    }

    public void setBillingTel(String billingTel) {
        this.billingTel = billingTel;
    }

    public String getBillingEmail() {
        return this.billingEmail;
    }

    public void setBillingEmail(String billingEmail) {
        this.billingEmail = billingEmail;
    }

    public String getShipToCP() {
        return this.shipToCP;
    }

    public void setShipToCP(String shipToCP) {
        this.shipToCP = shipToCP;
    }

    public String getShipToDireccion() {
        return this.shipToDireccion;
    }

    public void setShipToDireccion(String shipToDireccion) {
        this.shipToDireccion = shipToDireccion;
    }

    public String getShipToNombre() {
        return this.shipToNombre;
    }

    public void setShipToNombre(String shipToNombre) {
        this.shipToNombre = shipToNombre;
    }

    public String getShipToApellido() {
        return this.shipToApellido;
    }

    public void setShipToApellido(String shipToApellido) {
        this.shipToApellido = shipToApellido;
    }

    public String getShipToTel() {
        return this.shipToTel;
    }

    public void setShipToTel(String shipToTel) {
        this.shipToTel = shipToTel;
    }

    public String getShipToEmail() {
        return this.shipToEmail;
    }

    public void setShipToEmail(String shipToEmail) {
        this.shipToEmail = shipToEmail;
    }

    public double getNumberAmount() {
        return this.numberAmount;
    }

    public void setNumberAmount(double numberAmount) {
        this.numberAmount = numberAmount;
    }

    public String getServiceIdentifier() {
        return this.serviceIdentifier;
    }

    public void setServiceIdentifier(String serviceIdentifier) {
        this.serviceIdentifier = serviceIdentifier;
    }

    public String getTypeIdentifier() {
        return this.typeIdentifier;
    }

    public void setTypeIdentifier(String typeIdentifier) {
        this.typeIdentifier = typeIdentifier;
    }

    public String getAccReferenceData() {
        return this.accReferenceData;
    }

    public void setAccReferenceData(String accReferenceData) {
        this.accReferenceData = accReferenceData;
    }

    public String getPaymentType() {
        return this.paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getInstallations() {
        return this.installations;
    }

    public void setInstallations(String installations) {
        this.installations = installations;
    }

    public String getDpp() {
        return this.dpp;
    }

    public void setDpp(String dpp) {
        this.dpp = dpp;
    }

    public String getProcessingCode() {
        return this.processingCode;
    }

    public void setProcessingCode(String processingCode) {
        this.processingCode = processingCode;
    }

    public String getApprovalCode() {
        return this.approvalCode;
    }

    public void setApprovalCode(String approvalCode) {
        this.approvalCode = approvalCode;
    }

    public String getActionCode() {
        return this.actionCode;
    }

    public void setActionCode(String actionCode) {
        this.actionCode = actionCode;
    }

    public String getActionDsc() {
        return this.actionDsc;
    }

    public void setActionDsc(String actionDsc) {
        this.actionDsc = actionDsc;
    }

    public String getIsReversal() {
        return this.isReversal;
    }

    public void setIsReversal(String isReversal) {
        this.isReversal = isReversal;
    }

    public String getTipoTarjeta() {
        return this.tipoTarjeta;
    }

    public void setTipoTarjeta(String tipoTarjeta) {
        this.tipoTarjeta = tipoTarjeta;
    }

    public String getTransactionIdn() {
        return this.transactionIdn;
    }

    public void setTransactionIdn(String transactionIdn) {
        this.transactionIdn = transactionIdn;
    }

    public String getAcceptorId() {
        return this.acceptorId;
    }

    public void setAcceptorId(String acceptorId) {
        this.acceptorId = acceptorId;
    }
}
