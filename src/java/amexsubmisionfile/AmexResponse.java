/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package amexsubmisionfile;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author carlosgs
 */
public class AmexResponse {

    private List<AmexResponse> errores;
    public String transactionId;
    public String codigo;
    public String mensaje;
    public String accReference;
    public String approvalCode;

    public AmexResponse() {
        this.errores = new ArrayList();
    }

    public void setError(String codigo, String mensaje) {
        AmexResponse error = new AmexResponse();
        error.codigo = codigo;
        error.mensaje = mensaje;
        this.errores.add(error);
    }

    public List getErrores() {
        return this.errores;
    }

    public AmexResponse getRespuesta() {
        AmexResponse respuesta = new AmexResponse();
        respuesta.transactionId = this.transactionId;
        respuesta.codigo = this.codigo;
        respuesta.mensaje = this.mensaje;
        return respuesta;
    }

    public Boolean hasErrors() {
        if (this.errores == null) {
            return Boolean.valueOf(false);
        }
        if ((this.errores != null) && (this.errores.isEmpty())) {
            return Boolean.valueOf(false);
        }
        return Boolean.valueOf(true);
    }
}
