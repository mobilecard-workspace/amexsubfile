package amexsubmisionfile;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DatabaseImpl {
	private static final Logger logger = LoggerFactory.getLogger(DatabaseImpl.class);

    private Connection connection = null;
    private final String userName = "ecommerce";
    private final String password = "AddcellWeb2012";
    private final String url = "jdbc:mysql://DataBaseMC/ecommerce";

    public Connection getConnection()
            throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");
        this.connection = DriverManager.getConnection(this.url, this.userName, this.password);
        return this.connection;
    }

//    public void disconect()
//            throws SQLException {
//        this.connection.close();
//    }
//
//    public List executeQuery(String query) {
//        List tabla = new ArrayList();
//        int size = 0;
//        try {
//            ResultSet resultSet;
//            PreparedStatement statement = getConnection().prepareStatement(query);
//            resultSet = statement.executeQuery();
//            if (resultSet.next()) {
//                while (resultSet.next()) {
//                    size++;
//                }
//                resultSet.first();
//                while (resultSet.next()) {
//                    List linea = new ArrayList();
//                    for (int i = 0; i < size; i++) {
//                        linea.add(resultSet.getString(i));
//                    }
//                    tabla.add(linea);
//                }
//            }
//            return tabla;
//        } catch (SQLException ex) {
//            System.out.println("AMEX Error:" + ex.getMessage());
//        } catch (ClassNotFoundException ex) {
//            System.out.println("AMEX Error:" + ex.getMessage());
//        } finally {
//            try {
//                disconect();
//            } catch (Exception ee) {
//                System.out.println("AMEX Error:" + ee.getMessage());
//            }
//        }
//        
//        return tabla;
//    }
//
//    public String executeScalar(String query) {
//        List tabla = new ArrayList();
//        String resultado = "";
//        int size = 0;
//        try {
//            ResultSet resultSet = null;
//            PreparedStatement statement = getConnection().prepareStatement(query);
//            resultSet = statement.executeQuery();
//            if (resultSet.next()) {
//                while (resultSet.next()) {
//                    size++;
//                }
//                resultSet.first();
//                if (size == 1) {
//                    while (resultSet.next()) {
//                        resultado = resultSet.getString(0);
//                    }
//                }
//            }
//            return resultado;
//        } catch (SQLException ex) {
//            System.out.println("AMEX Error:" + ex.getMessage());
//        } catch (ClassNotFoundException ex) {
//            System.out.println("AMEX Error:" + ex.getMessage());
//        } finally {
//            try {
//                disconect();
//            } catch (Exception ee) {
//                System.out.println("AMEX Error:" + ee.getMessage());
//            }
//        }
//        return resultado;
//    }
//    
    
    /**
     * Cierra los objetos de acceso a base de datos en el ordern:
     * ResultSet, PreparedStatement, connection
     * @param preSt
     * @param res
     * @throws SQLException 
     */
    public void closeConPrepStResSet(PreparedStatement preSt,ResultSet res, Connection conn){
        try{
            closeResultSet(res);
            closePreparedStatement(preSt);
            closeConnection(conn);
        }catch(Exception e){
            logger.error("Error closeConPrepStResSet : General Exception: {}", e);            
        }
    }
    
    /**
     * Cierra los objetos de acceso a base de datos en el ordern:
     * ResultSet, PreparedStatement, connection
     * @param preSt
     * @param res
     * @throws SQLException 
     */
    public void closeConPrepStResSet(Statement st,ResultSet res, Connection conn){
        try{
            closeResultSet(res);
            closeStatement(st);
            closeConnection(conn);
        }catch(Exception e){
            logger.error("Error closeConPrepStResSet : General Exception: {}", e);            
        }
    }
    
    /**
     * Cierra los objetos de acceso a base de datos en el ordern:
     * PreparedStatement, connection
     * @param preSt
     * @throws SQLException 
     */
    public void closeConPreparedSt(PreparedStatement preSt, Connection conn){
        try{
            closePreparedStatement(preSt);
            closeConnection(conn);
        }catch(Exception e){
            logger.error("Error closeConPreparedSt : General Exception: {}", e);            
        }
    }
    
    /**
     * Cierra los objetos de acceso a base de datos en el ordern:
     * PreparedStatement, connection
     * @param preSt
     * @throws SQLException 
     */
    public void closeConSt(Statement preSt, Connection conn){
        try{
            closeStatement(preSt);
            closeConnection(conn);
        }catch(Exception e){
            logger.error("Error closeConPreparedSt : General Exception: {}", e);            
        }
    }
    
     /**
     * Cierra los objetos de acceso a base de datos en el ordern:
     * ResultSet, connection
     * @param res
     * @throws SQLException 
     */
    public void closeConResultSet(ResultSet res, Connection conn){
        try{
            closeResultSet(res);
            closeConnection(conn);
        }catch(Exception e){
            logger.error("Error closeConResultSet : General Exception: {}", e);            
        }
    }
    
    public void closeConnection(Connection conn) {
        try{
            if(conn != null){
                conn.close();
            }
        }catch(SQLException e){
            logger.error("Error closeConnection : General Exception: {}", e);            
        }
    }
    
    private void closePreparedStatement(PreparedStatement preSt)throws SQLException{
        try{
            if(preSt != null){
                preSt.close();
            }
        }catch(SQLException e){
            logger.error("Error closePreparedStatement : General Exception: {}", e);            
        }
    }
    
    private void closeStatement(Statement preSt)throws SQLException{
        try{
            if(preSt != null){
                preSt.close();
            }
        }catch(SQLException e){
            logger.error("Error closeStatement : General Exception: {}", e);            
        }
    }
    
    private void closeResultSet(ResultSet res)throws SQLException{
        try{
            if(res != null){
                res.close();
            }
        }catch(SQLException e){
            logger.error("Error closeResultSet : General Exception: {}", e);            
        }
    }


}
