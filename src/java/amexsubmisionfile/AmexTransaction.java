/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package amexsubmisionfile;

public class AmexTransaction
{
  private String transaccionIdn = "";
  private String transaccionId = "";
  private String transaccionLocalDateTime = "";
  private String transaccionDsc = "";
  private String transaccionUsuario = "";
  private String transaccionTarjeta = "";
  private String transaccionExpira = "";
  private String transaccionMonto = "";
  private String transaccionCid = "";
  private String transaccionNombres = "";
  private String transaccionApellidos = "";
  private String transaccionDireccion = "";
  private String transaccionTelefono = "";
  private String transaccionEmail = "";
  private String transaccionCp = "";
  private String MensajeId = "";
  private String transaccionTipo = "";
  private String transaccionSubTipo = "";
  private String transaccionReversal = "";
  private String transaccionReversalIdn = "";
  private String transaccionAccReference = "";
  private String transaccionApprovalCode = "";
  private String transaccionDate = "";
  private String transaccionProcesada = "";
  private String transaccionTipoTarjeta = "";
  private String transaccionAcceptorId = "";
  
  public String getTransaccionIdn()
  {
    return this.transaccionIdn;
  }
  
  public void setTransaccionIdn(String transaccionIdn)
  {
    this.transaccionIdn = transaccionIdn;
  }
  
  public String getTransaccionId()
  {
    return this.transaccionId;
  }
  
  public void setTransaccionId(String transaccionId)
  {
    this.transaccionId = transaccionId;
  }
  
  public String getTransaccionLocalDateTime()
  {
    return this.transaccionLocalDateTime;
  }
  
  public void setTransaccionLocalDateTime(String transaccionLocalDateTime)
  {
    this.transaccionLocalDateTime = transaccionLocalDateTime;
  }
  
  public String getTransaccionDsc()
  {
    return this.transaccionDsc;
  }
  
  public void setTransaccionDsc(String transaccionDsc)
  {
    this.transaccionDsc = transaccionDsc;
  }
  
  public String getTransaccionUsuario()
  {
    return this.transaccionUsuario;
  }
  
  public void setTransaccionUsuario(String transaccionUsuario)
  {
    this.transaccionUsuario = transaccionUsuario;
  }
  
  public String getTransaccionTarjeta()
  {
    return "********** " + this.transaccionTarjeta.substring(10, this.transaccionTarjeta.length());
  }
  
  public void setTransaccionTarjeta(String transaccionTarjeta)
  {
    this.transaccionTarjeta = transaccionTarjeta;
  }
  
  public String getTransaccionExpira()
  {
    return this.transaccionExpira;
  }
  
  public void setTransaccionExpira(String transaccionExpira)
  {
    this.transaccionExpira = transaccionExpira;
  }
  
  public String getTransaccionMonto()
  {
    return this.transaccionMonto;
  }
  
  public void setTransaccionMonto(String transaccionMonto)
  {
    this.transaccionMonto = transaccionMonto;
  }
  
  public String getTransaccionCid()
  {
    return this.transaccionCid;
  }
  
  public void setTransaccionCid(String transaccionCid)
  {
    this.transaccionCid = transaccionCid;
  }
  
  public String getTransaccionNombres()
  {
    return this.transaccionNombres;
  }
  
  public void setTransaccionNombres(String transaccionNombres)
  {
    this.transaccionNombres = transaccionNombres;
  }
  
  public String getTransaccionApellidos()
  {
    return this.transaccionApellidos;
  }
  
  public void setTransaccionApellidos(String transaccionApellidos)
  {
    this.transaccionApellidos = transaccionApellidos;
  }
  
  public String getTransaccionDireccion()
  {
    return this.transaccionDireccion;
  }
  
  public void setTransaccionDireccion(String transaccionDireccion)
  {
    this.transaccionDireccion = transaccionDireccion;
  }
  
  public String getTransaccionTelefono()
  {
    return this.transaccionTelefono;
  }
  
  public void setTransaccionTelefono(String transaccionTelefono)
  {
    this.transaccionTelefono = transaccionTelefono;
  }
  
  public String getTransaccionEmail()
  {
    return this.transaccionEmail;
  }
  
  public void setTransaccionEmail(String transaccionEmail)
  {
    this.transaccionEmail = transaccionEmail;
  }
  
  public String getTransaccionCp()
  {
    return this.transaccionCp;
  }
  
  public void setTransaccionCp(String transaccionCp)
  {
    this.transaccionCp = transaccionCp;
  }
  
  public String getMensajeId()
  {
    return this.MensajeId;
  }
  
  public void setMensajeId(String MensajeId)
  {
    this.MensajeId = MensajeId;
  }
  
  public String getTransaccionTipo()
  {
    return this.transaccionTipo;
  }
  
  public void setTransaccionTipo(String transaccionTipo)
  {
    this.transaccionTipo = transaccionTipo;
  }
  
  public String getTransaccionSubTipo()
  {
    return this.transaccionSubTipo;
  }
  
  public void setTransaccionSubTipo(String transaccionSubTipo)
  {
    this.transaccionSubTipo = transaccionSubTipo;
  }
  
  public String getTransaccionReversal()
  {
    return this.transaccionReversal;
  }
  
  public void setTransaccionReversal(String transaccionReversal)
  {
    this.transaccionReversal = transaccionReversal;
  }
  
  public String getTransaccionReversalIdn()
  {
    return this.transaccionReversalIdn;
  }
  
  public void setTransaccionReversalIdn(String transaccionReversalIdn)
  {
    this.transaccionReversalIdn = transaccionReversalIdn;
  }
  
  public String getTransaccionAccReference()
  {
    return this.transaccionAccReference;
  }
  
  public void setTransaccionAccReference(String transaccionAccReference)
  {
    this.transaccionAccReference = transaccionAccReference;
  }
  
  public String getTransaccionApprovalCode()
  {
    return this.transaccionApprovalCode;
  }
  
  public void setTransaccionApprovalCode(String transaccionApprovalCode)
  {
    this.transaccionApprovalCode = transaccionApprovalCode;
  }
  
  public String getTransaccionDate()
  {
    return this.transaccionDate;
  }
  
  public void setTransaccionDate(String transaccionDate)
  {
    this.transaccionDate = transaccionDate;
  }
  
  public String getTransaccionProcesada()
  {
    return this.transaccionProcesada;
  }
  
  public void setTransaccionProcesada(String transaccionProcesada)
  {
    this.transaccionProcesada = transaccionProcesada;
  }
  
  public String getTransaccionTipoTarjeta()
  {
    return this.transaccionTipoTarjeta;
  }
  
  public void setTransaccionTipoTarjeta(String transaccionTipoTarjeta)
  {
    this.transaccionTipoTarjeta = transaccionTipoTarjeta;
  }
  
  public String getTransaccionAcceptorId()
  {
    return this.transaccionAcceptorId;
  }
  
  public void setTransaccionAcceptorId(String transaccionAcceptorId)
  {
    this.transaccionAcceptorId = transaccionAcceptorId;
  }
}
