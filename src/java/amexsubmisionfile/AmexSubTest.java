package amexsubmisionfile;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.americanexpress.ips.gfsg.beans.ErrorObject;
import com.americanexpress.ips.gfsg.beans.SettlementRequestDataObject;
import com.americanexpress.ips.gfsg.beans.SettlementResponseDataObject;
import com.americanexpress.ips.gfsg.beans.common.TransactionAdviceBasicBean;
import com.americanexpress.ips.gfsg.beans.common.TransactionBatchTrailerBean;
import com.americanexpress.ips.gfsg.beans.common.TransactionFileHeaderBean;
import com.americanexpress.ips.gfsg.beans.common.TransactionFileSummaryBean;
import com.americanexpress.ips.gfsg.beans.common.TransactionTBTSpecificBean;
import com.americanexpress.ips.gfsg.beans.common.taa.LocationDetailTAABean;
import com.americanexpress.ips.gfsg.connection.PropertyReader;
import com.americanexpress.ips.gfsg.enumerations.AddendaTypeCode;
import com.americanexpress.ips.gfsg.enumerations.CurrencyCode;
import com.americanexpress.ips.gfsg.enumerations.FormatCode;
import com.americanexpress.ips.gfsg.enumerations.ProcessingCode;
import com.americanexpress.ips.gfsg.enumerations.RegionCode1;
import com.americanexpress.ips.gfsg.exceptions.SettlementException;
import com.americanexpress.ips.gfsg.processor.iso.RequestProcessorFactory;

public class AmexSubTest {
	private static final Logger logger = LoggerFactory.getLogger(AmexSubTest.class);

	public void execute() throws Exception {
		file_without_Optional_TAD_or_Optional_TAA_Records();
	}

	public void testProcess() {
		TransactionFileSummaryBean tfsBean = new TransactionFileSummaryBean();

		tfsBean.setNumberOfDebits("0");
		tfsBean.setHashTotalDebitAmount("0");
		tfsBean.setNumberOfCredits("1");
		tfsBean.setHashTotalCreditAmount("1000");
		tfsBean.setHashTotalAmount("1000");

		TransactionBatchTrailerBean tbtBean = new TransactionBatchTrailerBean();
		tbtBean.setMerchantId("9351358974");
		tbtBean.setTbtCreationDate("20120919");
		tbtBean.setTotalNoOfTabs("1");
		tbtBean.setTbtAmount("1000");
		tbtBean.setTbtAmountSign("+");
		tbtBean.setTbtCurrencyCode(CurrencyCode.Mexican_Peso.getCurrencyCode());

		LocationDetailTAABean locationTAABean = new LocationDetailTAABean();
		locationTAABean.setTransactionIdentifier("000000000000000");
		locationTAABean.setAddendaTypeCode(AddendaTypeCode.Location_Detail_Addendum_Message.getAddendaTypeCode());
		locationTAABean.setLocationName("XYZStoresToronto");
		locationTAABean.setLocationAddress("XYZStoresToronto");
		locationTAABean.setLocationCity("Toronto");
		locationTAABean.setLocationRegion(RegionCode1.NOVA_SCOTIA.getCountryCode());
		locationTAABean.setLocationCountryCode(RegionCode1.NOVA_SCOTIA.getCountryCode());
		locationTAABean.setLocationPostalCode("54725");
		locationTAABean.setMerchantCategoryCode("");
		locationTAABean.setSellerId("");

		LocationDetailTAABean locationDetailTAABean = new LocationDetailTAABean();
		locationDetailTAABean.setTransactionIdentifier("000000000000000");
		locationDetailTAABean.setAddendaTypeCode(AddendaTypeCode.Location_Detail_Addendum_Message.getAddendaTypeCode());
		locationDetailTAABean.setLocationName("XYZStoresToronto");
		locationDetailTAABean.setLocationAddress("XYZStoresToronto");
		locationDetailTAABean.setLocationCity("Toronto");
		locationDetailTAABean.setLocationRegion(RegionCode1.NOVA_SCOTIA.getCountryCode());
		locationDetailTAABean.setLocationCountryCode(RegionCode1.NOVA_SCOTIA.getCountryCode());
		locationDetailTAABean.setLocationPostalCode("M3C0C1");
		locationDetailTAABean.setMerchantCategoryCode("");
		locationDetailTAABean.setSellerId("");

		TransactionAdviceBasicBean tabBean = new TransactionAdviceBasicBean();
		tabBean.setTransactionIdentifier("000000000000000");
		tabBean.setFormatCode(FormatCode.General_Retail_TAA_Record.getFormatCode());

		tabBean.setApprovalCode("712389");
		tabBean.setPrimaryAccountNumber("376677849952008");
		tabBean.setTransactionDate("20120919");
		tabBean.setTransactionAmount("1000");
		tabBean.setProcessingCode(ProcessingCode.CREDIT.getProcessingCode());
		tabBean.setTransactionCurrencyCode(CurrencyCode.Mexican_Peso.getCurrencyCode());
		tabBean.setExtendedPaymentData("01");
		tabBean.setMerchantId("9351358974");
		tabBean.setMerchantLocationId("jorge@addcel.com");
		tabBean.setPosDataCode("1009S0S00000");

		TransactionFileHeaderBean tfhBean = new TransactionFileHeaderBean();
		tfhBean.setSubmitterId("017ADD001");
		tfhBean.setSubmitterFileReferenceNumber("G00002001");
		tfhBean.setSubmitterFileSequenceNumber("1");
		tfhBean.setFileCreationDate("20120919");
		tfhBean.setFileVersionNumber("11020000");

		tabBean.setLocationDetailTAABean(locationDetailTAABean);
		List<TransactionAdviceBasicBean> tab = new ArrayList();
		tab.add(tabBean);
		TransactionTBTSpecificBean tbtSpecificBean = new TransactionTBTSpecificBean();
		tbtSpecificBean.setTransactionBatchTrailerBean(tbtBean);
		tbtSpecificBean.setTransactionAdviceBasicType(tab);
		List<TransactionTBTSpecificBean> transactionTBTSpecificBeanList = new ArrayList();
		transactionTBTSpecificBeanList.add(tbtSpecificBean);

		SettlementRequestDataObject settlementRequestBean = new SettlementRequestDataObject();
		settlementRequestBean.setTransactionFileHeaderType(tfhBean);
		settlementRequestBean.setTransactionTBTSpecificType(transactionTBTSpecificBeanList);

		PropertyReader propertyReader = new PropertyReader();
		propertyReader = propertyReader.loadPropertyFile("/settlement.properties");
		try {
			SettlementResponseDataObject settlementResponseDataObject = RequestProcessorFactory.processRequest(settlementRequestBean, propertyReader);
			if (settlementResponseDataObject.getActionCodeDescription() != null) {
				if (!settlementResponseDataObject.getActionCodeDescription().isEmpty()) {
					logger.debug("Error:" + settlementResponseDataObject.getActionCodeDescription());
					for (ErrorObject eo : settlementResponseDataObject.getActionCodeDescription()) {
						logger.debug("ACTION CODE AMEX:" + eo.getErrorCode() + ":" + eo.getErrorDescription());
					}
					Iterator<?> iterater = settlementResponseDataObject.getActionCodeDescription().iterator();
					while (iterater.hasNext()) {
						ErrorObject errorObject = (ErrorObject) iterater.next();
						logger.debug("ERROR AMEX:" + errorObject.getErrorCode() + "-" + errorObject.getErrorDescription() + "\n");
					}
				} else {
					logger.debug("AMEX Error List is empty\n");
				}
			} else {
				logger.debug("No Action Code\n");
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug("Error Object is Empty");
		}
	}

	public String getDate() {
		String DATE_FORMAT = "yyyyMMdd";
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);

		Calendar c1 = Calendar.getInstance();
		return sdf.format(c1.getTime());
	}

	public String paddingLeft(int spaces, int pad) {
		String s = String.format("%0" + spaces + "d", new Object[] { Integer.valueOf(pad) });
		return s;
	}

	public void executeSubmitFile(String acceptorId, String anio, String mes, String dia) throws SettlementException {
		TransactionFileHeaderBean transactionFileHeaderBean = new TransactionFileHeaderBean();
		transactionFileHeaderBean.setSubmitterId("017ADD");
		transactionFileHeaderBean.setSubmitterFileReferenceNumber("G00002004");
		transactionFileHeaderBean.setSubmitterFileSequenceNumber("1");
		transactionFileHeaderBean.setFileCreationDate(getDate());

		transactionFileHeaderBean.setFileVersionNumber("11020000");

		int cCredito = 0;
		int cDebito = 0;
		long montosCredito = 0L;
		long montosDebito = 0L;
		long montoTotal = 0L;
		int c = 0;

		List<TransactionTBTSpecificBean> transactionTBTSpecificType = new ArrayList<TransactionTBTSpecificBean>();

		List<TransactionAdviceBasicBean> transactionAdviceBasicType = new ArrayList<TransactionAdviceBasicBean>();
		AmexData[] transacciones = null;

		transacciones = TransactionControl.getData(acceptorId, anio, mes, dia);

		logger.debug("*** [" + acceptorId + "]  SIZE -> " + transacciones.length);
		for (int i = 0; i < transacciones.length; i++) {
			long monto = Long.parseLong(transacciones[i].getAmount());

			String transactionIdentifier = transacciones[i].getAccReferenceData();

			TransactionAdviceBasicBean transactionAdviceBasicBean = new TransactionAdviceBasicBean();
			transactionAdviceBasicBean.setTransactionIdentifier(transactionIdentifier);
			transactionAdviceBasicBean.setFormatCode("02");
			transactionAdviceBasicBean.setApprovalCode(transacciones[i].getSystemTraceAuditNumber());
			transactionAdviceBasicBean.setPrimaryAccountNumber(transacciones[i].getPrimaryAccountNumber());

			transactionAdviceBasicBean.setTransactionDate(getDate());
			transactionAdviceBasicBean.setTransactionAmount(Long.toString(monto));
			if ((transacciones[i].getTipoTarjeta().equals("C")) && (transacciones[i].getIsReversal().equals("N"))) {
				transactionAdviceBasicBean.setProcessingCode(ProcessingCode.CREDIT.getProcessingCode());
			} else if ((transacciones[i].getTipoTarjeta().equals("C")) && (transacciones[i].getIsReversal().equals("S"))) {
				transactionAdviceBasicBean.setProcessingCode(ProcessingCode.CREDIT_REVERSALS.getProcessingCode());
			} else if ((transacciones[i].getTipoTarjeta().equals("D")) && (transacciones[i].getIsReversal().equals("N"))) {
				transactionAdviceBasicBean.setProcessingCode(ProcessingCode.DEBIT.getProcessingCode());
			} else if ((transacciones[i].getTipoTarjeta().equals("D")) && (transacciones[i].getIsReversal().equals("S"))) {
				transactionAdviceBasicBean.setProcessingCode(ProcessingCode.DEBIT_REVERSALS.getProcessingCode());
			}
			transactionAdviceBasicBean.setProcessingCode(ProcessingCode.CREDIT_REVERSALS.getProcessingCode());

			transactionAdviceBasicBean.setTransactionCurrencyCode(CurrencyCode.Mexican_Peso.getCurrencyCode());
			transactionAdviceBasicBean.setExtendedPaymentData("01");

			transactionAdviceBasicBean.setMerchantId(acceptorId);
			transactionAdviceBasicBean.setMerchantContactInfo("jorge@addcel.com");
			transactionAdviceBasicBean.setPosDataCode("1009S0S00000");

			LocationDetailTAABean locationDetailTAABean = new LocationDetailTAABean();
			locationDetailTAABean.setTransactionIdentifier(transactionIdentifier);
			locationDetailTAABean.setAddendaTypeCode(AddendaTypeCode.Location_Detail_Addendum_Message.getAddendaTypeCode());
			locationDetailTAABean.setLocationName("Mexico");
			locationDetailTAABean.setLocationAddress("Prolongacion Reforma 61");
			locationDetailTAABean.setLocationCity("Mexico");
			locationDetailTAABean.setLocationRegion("MX");
			locationDetailTAABean.setLocationCountryCode("484");
			locationDetailTAABean.setLocationPostalCode("01330");
			locationDetailTAABean.setMerchantCategoryCode("");
			locationDetailTAABean.setSellerId("");

			transactionAdviceBasicBean.setLocationDetailTAABean(locationDetailTAABean);

			transactionAdviceBasicType.add(transactionAdviceBasicBean);
			if (transacciones[i].getTipoTarjeta().equals("C")) {
				montosCredito += monto;
				cCredito++;
			}
			if (transacciones[i].getTipoTarjeta().equals("D")) {
				montosDebito += monto;
				cDebito++;
			}
			montoTotal += monto;
			c++;

//			logger.debug("***TRANSACTION:" + transactionIdentifier + "->" + Long.toString(monto));

			TransactionBatchTrailerBean transactionBatchTrailerBean = new TransactionBatchTrailerBean();

			transactionBatchTrailerBean.setMerchantId(acceptorId);
			transactionBatchTrailerBean.setTbtCreationDate(getDate());
			transactionBatchTrailerBean.setTotalNoOfTabs(Integer.toString(c));
			transactionBatchTrailerBean.setTbtAmount(Long.toString(montoTotal));
			transactionBatchTrailerBean.setTbtAmountSign("+");
			transactionBatchTrailerBean.setTbtCurrencyCode(CurrencyCode.Mexican_Peso.getCurrencyCode());
			TransactionTBTSpecificBean transactionTBTSpecificBean = new TransactionTBTSpecificBean();
			transactionTBTSpecificBean.setTransactionAdviceBasicType(transactionAdviceBasicType);
			transactionTBTSpecificBean.setTransactionBatchTrailerBean(transactionBatchTrailerBean);

			transactionTBTSpecificType = new ArrayList();
			transactionTBTSpecificType.add(transactionTBTSpecificBean);

			String transactionIdn = transacciones[i].getTransactionIdn();
			TransactionControl.updateProcesada(transactionIdn);
		}
		TransactionFileSummaryBean transactionFileSummaryBean = new TransactionFileSummaryBean();

		transactionFileSummaryBean.setNumberOfDebits(Integer.toString(cDebito));
		transactionFileSummaryBean.setHashTotalDebitAmount(Integer.toString(c));
		transactionFileSummaryBean.setNumberOfCredits(Integer.toString(cCredito));
		transactionFileSummaryBean.setHashTotalCreditAmount(Long.toString(montosCredito));
		transactionFileSummaryBean.setHashTotalAmount(Long.toString(montoTotal));

		SettlementRequestDataObject settlementRequestDataObject = new SettlementRequestDataObject();

		settlementRequestDataObject.setTransactionFileHeaderType(transactionFileHeaderBean);

		settlementRequestDataObject.setTransactionTBTSpecificType(transactionTBTSpecificType);

		settlementRequestDataObject.setTransactionFileSummaryType(transactionFileSummaryBean);

		List errorCodes = new ArrayList();

		PropertyReader propertyReader = new PropertyReader();

		propertyReader = propertyReader.loadPropertyFile("/settlement.properties");

		Properties props = new Properties();
		try {
			props.load(AmexSubTest.class.getResourceAsStream("settlement.properties"));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		SettlementResponseDataObject settlementResponseDataObject = RequestProcessorFactory.processRequest(settlementRequestDataObject, propertyReader);
		try {
			if (settlementResponseDataObject != null && settlementResponseDataObject.getActionCodeDescription() != null && 
					!settlementResponseDataObject.getActionCodeDescription().isEmpty()) {
				Iterator<?> iterater = settlementResponseDataObject.getActionCodeDescription().iterator();
				while (iterater.hasNext()) {
					ErrorObject errorObject = (ErrorObject) iterater.next();
					logger.debug(errorObject.getErrorCode() + "-" + errorObject.getErrorDescription() + "\n");
				}
			} else {
				logger.debug("File Successfully Submitted\n");
			}
		} catch (Exception e) {
			logger.debug("Error Object is Empty: ", e);
		}
	}

	private void file_without_Optional_TAD_or_Optional_TAA_Records() throws SettlementException {
		TransactionFileHeaderBean transactionFileHeaderBean = new TransactionFileHeaderBean();
		transactionFileHeaderBean.setSubmitterId("017ADD");
		transactionFileHeaderBean.setSubmitterFileReferenceNumber("G00002001");
		transactionFileHeaderBean.setSubmitterFileSequenceNumber("1");
		transactionFileHeaderBean.setFileCreationDate("20120921");

		transactionFileHeaderBean.setFileVersionNumber("11020000");

		List transactionAdviceBasicType = new ArrayList();

		TransactionAdviceBasicBean transactionAdviceBasicBean = new TransactionAdviceBasicBean();
		transactionAdviceBasicBean.setTransactionIdentifier("000000000000001");
		transactionAdviceBasicBean.setFormatCode(FormatCode.General_Retail_TAA_Record.getFormatCode());

		transactionAdviceBasicBean.setApprovalCode("712389");
		transactionAdviceBasicBean.setPrimaryAccountNumber("376677849952008");

		transactionAdviceBasicBean.setTransactionDate("20120921");

		transactionAdviceBasicBean.setTransactionAmount("1000");
		transactionAdviceBasicBean.setProcessingCode(ProcessingCode.CREDIT.getProcessingCode());
		transactionAdviceBasicBean.setTransactionCurrencyCode(CurrencyCode.Mexican_Peso.getCurrencyCode());
		transactionAdviceBasicBean.setExtendedPaymentData("01");
		transactionAdviceBasicBean.setMerchantId("9351358974");

		transactionAdviceBasicBean.setMerchantContactInfo("jorge@addcel.com");

		transactionAdviceBasicBean.setPosDataCode("1009S0S00000");

		LocationDetailTAABean locationDetailTAABean = new LocationDetailTAABean();

		locationDetailTAABean.setTransactionIdentifier("000000000000000");

		locationDetailTAABean.setAddendaTypeCode(AddendaTypeCode.Location_Detail_Addendum_Message.getAddendaTypeCode());

		locationDetailTAABean.setLocationName("Mexico");

		locationDetailTAABean.setLocationAddress("Prolongacion Reforma 61");

		locationDetailTAABean.setLocationCity("Distrito Federal");

		locationDetailTAABean.setLocationRegion("484");

		locationDetailTAABean.setLocationCountryCode("484");

		locationDetailTAABean.setLocationPostalCode("01330");

		locationDetailTAABean.setMerchantCategoryCode("");
		locationDetailTAABean.setSellerId("");

		transactionAdviceBasicBean.setLocationDetailTAABean(locationDetailTAABean);

		transactionAdviceBasicType.add(transactionAdviceBasicBean);

		TransactionBatchTrailerBean transactionBatchTrailerBean = new TransactionBatchTrailerBean();

		transactionBatchTrailerBean.setMerchantId("9351358974");

		transactionBatchTrailerBean.setTbtCreationDate("20120921");

		transactionBatchTrailerBean.setTotalNoOfTabs("1");

		transactionBatchTrailerBean.setTbtAmount("1000");

		transactionBatchTrailerBean.setTbtAmountSign("+");

		transactionBatchTrailerBean.setTbtCurrencyCode(CurrencyCode.Mexican_Peso.getCurrencyCode());

		TransactionTBTSpecificBean transactionTBTSpecificBean = new TransactionTBTSpecificBean();

		transactionTBTSpecificBean.setTransactionAdviceBasicType(transactionAdviceBasicType);
		transactionTBTSpecificBean.setTransactionBatchTrailerBean(transactionBatchTrailerBean);

		List<TransactionTBTSpecificBean> transactionTBTSpecificType = new ArrayList();

		transactionTBTSpecificType.add(transactionTBTSpecificBean);

		TransactionFileSummaryBean transactionFileSummaryBean = new TransactionFileSummaryBean();

		transactionFileSummaryBean.setNumberOfDebits("0");
		transactionFileSummaryBean.setHashTotalDebitAmount("0");
		transactionFileSummaryBean.setNumberOfCredits("1");
		transactionFileSummaryBean.setHashTotalCreditAmount("1000");
		transactionFileSummaryBean.setHashTotalAmount("1000");

		SettlementRequestDataObject settlementRequestDataObject = new SettlementRequestDataObject();

		settlementRequestDataObject.setTransactionFileHeaderType(transactionFileHeaderBean);

		settlementRequestDataObject.setTransactionTBTSpecificType(transactionTBTSpecificType);

		settlementRequestDataObject.setTransactionFileSummaryType(transactionFileSummaryBean);

		List errorCodes = new ArrayList();

		PropertyReader propertyReader = new PropertyReader();

		propertyReader = propertyReader.loadPropertyFile("/settlement.properties");

		Properties props = new Properties();
		try {
			props.load(AmexSubTest.class.getResourceAsStream("settlement.properties"));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		SettlementResponseDataObject settlementResponseDataObject = RequestProcessorFactory.processRequest(settlementRequestDataObject, propertyReader);
		try {
			if (!settlementResponseDataObject.getActionCodeDescription().isEmpty()) {
				Iterator<?> iterater = settlementResponseDataObject.getActionCodeDescription().iterator();
				while (iterater.hasNext()) {
					ErrorObject errorObject = (ErrorObject) iterater.next();
					logger.debug(errorObject.getErrorCode() + "-" + errorObject.getErrorDescription() + "\n");
				}
			} else {
				logger.debug("File Successfully Submitted\n");
			}
		} catch (Exception e) {
			logger.debug("Error Object is Empty");
		}
	}

//	private void file_with_Optional_TAD_Required_Location_Detail_TAA_and_Optional_Industry_Specific_TAA_Records() throws SettlementException {
//		TransactionFileHeaderBean transactionFileHeaderBean = new TransactionFileHeaderBean();
//
//		transactionFileHeaderBean.setSubmitterId("G00002");
//
//		transactionFileHeaderBean.setSubmitterFileReferenceNumber("G00002001");
//
//		transactionFileHeaderBean.setSubmitterFileSequenceNumber("1");
//
//		transactionFileHeaderBean.setFileCreationDate("20120921");
//
//		transactionFileHeaderBean.setFileCreationTime("143000");
//
//		List transactionAdviceBasicType = new ArrayList();
//
//		TransactionAdviceBasicBean transactionAdviceBasicBean = new TransactionAdviceBasicBean();
//
//		transactionAdviceBasicBean.setTransactionIdentifier("000000000000000");
//
//		transactionAdviceBasicBean.setFormatCode(FormatCode.No_Industry_Specific_TAA_Record.getFormatCode());
//
//		transactionAdviceBasicBean.setMediaCode(MediaCode.Cardmember_signature_on_File.getMediaCode());
//
//		transactionAdviceBasicBean.setSubmissionMethod(SubmissionMethod.Amex_POS_Terminal.getSubmissionMethod());
//
//		transactionAdviceBasicBean.setApprovalCode("712389");
//
//		transactionAdviceBasicBean.setPrimaryAccountNumber("3000000000000000");
//
//		transactionAdviceBasicBean.setCardExpiryDate("1405");
//
//		transactionAdviceBasicBean.setTransactionDate("20120921");
//
//		transactionAdviceBasicBean.setTransactionTime("143000");
//
//		transactionAdviceBasicBean.setTransactionAmount("4300");
//
//		transactionAdviceBasicBean.setProcessingCode(ProcessingCode.CREDIT.getProcessingCode());
//
//		transactionAdviceBasicBean.setTransactionCurrencyCode(CurrencyCode.United_Kingdom_Pound_Sterling.getCurrencyCode());
//
//		transactionAdviceBasicBean.setExtendedPaymentData("01");
//
//		transactionAdviceBasicBean.setMerchantId("5021045083");
//
//		transactionAdviceBasicBean.setMerchantLocationId("CAN100000089012");
//
//		transactionAdviceBasicBean.setMerchantContactInfo(" ");
//
//		transactionAdviceBasicBean.setTerminalId("Ter12345");
//
//		transactionAdviceBasicBean.setPosDataCode("161201254110");
//
//		transactionAdviceBasicBean.setInvoiceReferenceNumber("InvoiceNo123456");
//
//		transactionAdviceBasicBean.setTabImageSequenceNumber("");
//
//		transactionAdviceBasicBean.setMatchingKeyType("");
//
//		transactionAdviceBasicBean.setMatchingKey("");
//
//		transactionAdviceBasicBean.setElectronicCommerceIndicator("");
//
//		TransactionAdviceBasicBean transactionAdviceBasicBean1 = new TransactionAdviceBasicBean();
//
//		transactionAdviceBasicBean1.setTransactionIdentifier("000000000000000");
//
//		transactionAdviceBasicBean1.setFormatCode(FormatCode.No_Industry_Specific_TAA_Record.getFormatCode());
//
//		transactionAdviceBasicBean1.setMediaCode(MediaCode.Cardmember_signature_on_File.getMediaCode());
//
//		transactionAdviceBasicBean1.setSubmissionMethod(SubmissionMethod.Amex_POS_Terminal.getSubmissionMethod());
//
//		transactionAdviceBasicBean1.setApprovalCode("712389");
//
//		transactionAdviceBasicBean1.setPrimaryAccountNumber("30000000000000000");
//
//		transactionAdviceBasicBean1.setCardExpiryDate("1405");
//
//		transactionAdviceBasicBean1.setTransactionDate("20110705");
//
//		transactionAdviceBasicBean1.setTransactionTime("143000");
//
//		transactionAdviceBasicBean1.setTransactionAmount("4300");
//
//		transactionAdviceBasicBean1.setProcessingCode(ProcessingCode.CREDIT.getProcessingCode());
//
//		transactionAdviceBasicBean1.setTransactionCurrencyCode(CurrencyCode.United_Kingdom_Pound_Sterling.getCurrencyCode());
//
//		transactionAdviceBasicBean1.setExtendedPaymentData("01");
//
//		transactionAdviceBasicBean1.setMerchantId("5021045083");
//
//		transactionAdviceBasicBean1.setMerchantLocationId("CAN100000089012");
//
//		transactionAdviceBasicBean1.setMerchantContactInfo(" ");
//
//		transactionAdviceBasicBean1.setTerminalId("Ter12345");
//
//		transactionAdviceBasicBean1.setPosDataCode("161201254110");
//
//		transactionAdviceBasicBean1.setInvoiceReferenceNumber("InvoiceNo123456");
//
//		transactionAdviceBasicBean1.setTabImageSequenceNumber("");
//
//		transactionAdviceBasicBean1.setMatchingKeyType("");
//
//		transactionAdviceBasicBean1.setMatchingKey("");
//
//		transactionAdviceBasicBean1.setElectronicCommerceIndicator("");
//
//		LocationDetailTAABean locationDetailTAABean1 = new LocationDetailTAABean();
//
//		locationDetailTAABean1.setTransactionIdentifier("000000000000000");
//
//		locationDetailTAABean1.setAddendaTypeCode(AddendaTypeCode.Location_Detail_Addendum_Message.getAddendaTypeCode());
//
//		locationDetailTAABean1.setLocationName("XYZStoresToronto");
//
//		locationDetailTAABean1.setLocationAddress("XYZStoresToronto");
//
//		locationDetailTAABean1.setLocationCity("Toronto");
//
//		locationDetailTAABean1.setLocationRegion(RegionCode1.NOVA_SCOTIA.getRegionCode());
//
//		locationDetailTAABean1.setLocationCountryCode(RegionCode1.NOVA_SCOTIA.getCountryCode());
//
//		locationDetailTAABean1.setLocationPostalCode("M3C0C1");
//
//		locationDetailTAABean1.setMerchantCategoryCode("");
//		locationDetailTAABean1.setSellerId("");
//
//		transactionAdviceBasicBean1.setLocationDetailTAABean(locationDetailTAABean1);
//
//		transactionAdviceBasicType.add(transactionAdviceBasicBean1);
//
//		TransactionBatchTrailerBean transactionBatchTrailerBean = new TransactionBatchTrailerBean();
//
//		transactionBatchTrailerBean.setMerchantId("5021045083");
//
//		transactionBatchTrailerBean.setTbtIdentificationNumber("000000000000000");
//
//		transactionBatchTrailerBean.setTbtCreationDate("20110711");
//
//		transactionBatchTrailerBean.setTotalNoOfTabs("2");
//
//		transactionBatchTrailerBean.setTbtAmount("8600");
//
//		transactionBatchTrailerBean.setTbtAmountSign("-");
//
//		transactionBatchTrailerBean.setTbtCurrencyCode(CurrencyCode.United_Kingdom_Pound_Sterling.getCurrencyCode());
//
//		transactionBatchTrailerBean.setTbtImageSequenceNumber("");
//
//		TransactionTBTSpecificBean transactionTBTSpecificBean = new TransactionTBTSpecificBean();
//
//		transactionTBTSpecificBean.setTransactionAdviceBasicType(transactionAdviceBasicType);
//		transactionTBTSpecificBean.setTransactionBatchTrailerBean(transactionBatchTrailerBean);
//
//		List<TransactionTBTSpecificBean> transactionTBTSpecificType = new ArrayList();
//
//		transactionTBTSpecificType.add(transactionTBTSpecificBean);
//
//		TransactionFileSummaryBean transactionFileSummaryBean = new TransactionFileSummaryBean();
//
//		transactionFileSummaryBean.setNumberOfDebits("1");
//		transactionFileSummaryBean.setHashTotalDebitAmount("2");
//		transactionFileSummaryBean.setHashTotalCreditAmount("1000");
//		transactionFileSummaryBean.setHashTotalAmount("1000");
//		transactionFileSummaryBean.setNumberOfCredits("1");
//
//		SettlementRequestDataObject settlementRequestDataObject = new SettlementRequestDataObject();
//
//		settlementRequestDataObject.setTransactionFileHeaderType(transactionFileHeaderBean);
//
//		settlementRequestDataObject.setTransactionTBTSpecificType(transactionTBTSpecificType);
//
//		settlementRequestDataObject.setTransactionFileSummaryType(transactionFileSummaryBean);
//
//		List errorCodes = new ArrayList();
//
//		PropertyReader propertyReader = new PropertyReader();
//
//		propertyReader = propertyReader.loadPropertyFile("/settlement.properties");
//
//		SettlementResponseDataObject settlementResponseDataObject = RequestProcessorFactory.processRequest(settlementRequestDataObject, propertyReader);
//		try {
//			if (!settlementResponseDataObject.getActionCodeDescription().isEmpty()) {
//				Iterator<?> iterater = settlementResponseDataObject.getActionCodeDescription().iterator();
//				while (iterater.hasNext()) {
//					ErrorObject errorObject = (ErrorObject) iterater.next();
//					logger.debug(errorObject.getErrorCode() + "-" + errorObject.getErrorDescription() + "\n");
//				}
//			} else {
//				logger.debug("File Successfully Submitted\n");
//			}
//		} catch (Exception e) {
//			logger.debug("Error Object is Empty");
//		}
//	}
//
//	private void file_with_Optional_Industry_or_Non_Industry_Specific_TAA_Records() throws SettlementException {
//		TransactionFileHeaderBean transactionFileHeaderBean = new TransactionFileHeaderBean();
//
//		transactionFileHeaderBean.setSubmitterId("G00002");
//
//		transactionFileHeaderBean.setSubmitterFileReferenceNumber("G00002001");
//
//		transactionFileHeaderBean.setSubmitterFileSequenceNumber("1");
//
//		transactionFileHeaderBean.setFileCreationDate("20110718");
//
//		transactionFileHeaderBean.setFileCreationTime("143000");
//
//		List transactionAdviceBasicType = new ArrayList();
//
//		TransactionAdviceBasicBean transactionAdviceBasicBean = new TransactionAdviceBasicBean();
//
//		transactionAdviceBasicBean.setTransactionIdentifier("000000000000000");
//
//		transactionAdviceBasicBean.setFormatCode(FormatCode.No_Industry_Specific_TAA_Record.getFormatCode());
//
//		transactionAdviceBasicBean.setMediaCode(MediaCode.Cardmember_signature_on_File.getMediaCode());
//
//		transactionAdviceBasicBean.setSubmissionMethod(SubmissionMethod.Amex_POS_Terminal.getSubmissionMethod());
//
//		transactionAdviceBasicBean.setApprovalCode("712389");
//
//		transactionAdviceBasicBean.setPrimaryAccountNumber("300000000000000000");
//
//		transactionAdviceBasicBean.setCardExpiryDate("1405");
//
//		transactionAdviceBasicBean.setTransactionDate("20110705");
//
//		transactionAdviceBasicBean.setTransactionTime("143000");
//
//		transactionAdviceBasicBean.setTransactionAmount("4300");
//
//		transactionAdviceBasicBean.setProcessingCode(ProcessingCode.CREDIT.getProcessingCode());
//
//		transactionAdviceBasicBean.setTransactionCurrencyCode(CurrencyCode.United_Kingdom_Pound_Sterling.getCurrencyCode());
//
//		transactionAdviceBasicBean.setExtendedPaymentData("01");
//
//		transactionAdviceBasicBean.setMerchantId("50000000000");
//
//		transactionAdviceBasicBean.setMerchantLocationId("CAN123456789012");
//
//		transactionAdviceBasicBean.setMerchantContactInfo(" ");
//
//		transactionAdviceBasicBean.setTerminalId("Ter12345");
//
//		transactionAdviceBasicBean.setPosDataCode("161201254110");
//
//		transactionAdviceBasicBean.setInvoiceReferenceNumber("InvoiceNo123456");
//
//		transactionAdviceBasicBean.setTabImageSequenceNumber("");
//
//		transactionAdviceBasicBean.setMatchingKeyType("");
//
//		transactionAdviceBasicBean.setMatchingKey("");
//
//		transactionAdviceBasicBean.setElectronicCommerceIndicator("");
//
//		AirlineIndustryTAABean airBean = new AirlineIndustryTAABean();
//
//		airBean.setTransactionIdentifier("000000000000000");
//
//		airBean.setFormatCode(FormatCode.Airline_TAA_Record.getFormatCode());
//
//		airBean.setAddendaTypeCode(AddendaTypeCode.Industry_Specific_Detail_Addendum_Message.getAddendaTypeCode());
//
//		airBean.setTransactionType("TKT");
//
//		airBean.setTicketNumber("TKT12345678901");
//
//		airBean.setDocumentType("01");
//
//		airBean.setAirlineProcessIdentifier("A12");
//
//		airBean.setIataNumericCode("TKT12345");
//
//		airBean.setTicketingCarrierName("TKTCAR1234567891234567890");
//		airBean.setTicketIssueCity("NEWYORK");
//		airBean.setTicketIssueDate("20110204");
//		airBean.setNumberInParty("01");
//		airBean.setPassengerName("XYZ");
//		airBean.setConjunctionTicketIndicator("N");
//		airBean.setTotalNumberOfAirSegments("4");
//		airBean.setElectronicTicketIndicator("Y");
//		airBean.setOriginalTransactionAmount("1000");
//		airBean.setOriginalCurrencyCode(CurrencyCode.US_Dollar.getCurrencyCode());
//		airBean.setStopoverIndicator1("O");
//		airBean.setDepartureLocationCodeSegment1("MUM");
//		airBean.setDepartureDateSegment1("20110216");
//		airBean.setArrivalLocationCodeSegment1("DEL");
//		airBean.setSegmentCarrierCode1("A1");
//		airBean.setSegment1FareBasis("FAIRSEG12345678");
//		airBean.setClassOfServiceCodeSegment1("A");
//		airBean.setFlightNumberSegment1("ABC1");
//		airBean.setSegment1Fare("200");
//
//		airBean.setStopoverIndicator2("X");
//		airBean.setDepartureLocationCodeSegment2("DEL");
//		airBean.setDepartureDateSegment2("20110224");
//		airBean.setArrivalLocationCodeSegment2("NYY");
//		airBean.setSegmentCarrierCode2("A1");
//		airBean.setSegment2FareBasis("FAIRSEG12345678");
//		airBean.setClassOfServiceCodeSegment2("A2");
//		airBean.setFlightNumberSegment2("ABC2");
//		airBean.setSegment2Fare("100");
//
//		airBean.setStopoverIndicator3(" ");
//		airBean.setDepartureLocationCodeSegment3("NY");
//		airBean.setDepartureDateSegment3("20110216");
//		airBean.setArrivalLocationCodeSegment3("DAL");
//		airBean.setSegmentCarrierCode3("A1");
//		airBean.setSegment3FareBasis("FAIRSEG12345678");
//		airBean.setClassOfServiceCodeSegment3("A2");
//		airBean.setFlightNumberSegment3("ABC3");
//		airBean.setSegment3Fare("1000");
//
//		airBean.setStopoverIndicator4("O");
//		airBean.setDepartureLocationCodeSegment4("DAL");
//		airBean.setDepartureDateSegment4("20110216");
//		airBean.setArrivalLocationCodeSegment4("PHX");
//		airBean.setSegmentCarrierCode4("A1");
//		airBean.setSegment4FareBasis("FAIRSEG12345678");
//		airBean.setClassOfServiceCodeSegment4("A2");
//		airBean.setFlightNumberSegment4("ABC4");
//		airBean.setSegment4Fare("1000");
//
//		airBean.setExchangedOrOriginalTicketNumber("");
//
//		ArrayList taaAirArray = new ArrayList();
//		taaAirArray.add(airBean);
//		transactionAdviceBasicBean.setArrayTAABean(taaAirArray);
//
//		LocationDetailTAABean locationDetailTAABean = new LocationDetailTAABean();
//
//		locationDetailTAABean.setTransactionIdentifier("000000000000000");
//
//		locationDetailTAABean.setAddendaTypeCode(AddendaTypeCode.Location_Detail_Addendum_Message.getAddendaTypeCode());
//
//		locationDetailTAABean.setLocationName("XYZStoresToronto");
//
//		locationDetailTAABean.setLocationAddress("XYZStoresToronto");
//
//		locationDetailTAABean.setLocationCity("Toronto");
//
//		locationDetailTAABean.setLocationRegion(RegionCode1.NOVA_SCOTIA.getRegionCode());
//
//		locationDetailTAABean.setLocationCountryCode(RegionCode1.NOVA_SCOTIA.getCountryCode());
//
//		locationDetailTAABean.setLocationPostalCode("M3C0C1");
//
//		locationDetailTAABean.setMerchantCategoryCode("");
//		locationDetailTAABean.setSellerId("");
//
//		transactionAdviceBasicBean.setLocationDetailTAABean(locationDetailTAABean);
//
//		transactionAdviceBasicType.add(transactionAdviceBasicBean);
//
//		TransactionAdviceBasicBean transactionAdviceBasicBean1 = new TransactionAdviceBasicBean();
//
//		transactionAdviceBasicBean1.setTransactionIdentifier("000000000000000");
//
//		transactionAdviceBasicBean1.setFormatCode(FormatCode.Communication_Services_TAA_Record.getFormatCode());
//
//		transactionAdviceBasicBean1.setMediaCode(MediaCode.Cardmember_signature_on_File.getMediaCode());
//
//		transactionAdviceBasicBean1.setSubmissionMethod(SubmissionMethod.Amex_POS_Terminal.getSubmissionMethod());
//
//		transactionAdviceBasicBean1.setApprovalCode("712389");
//
//		transactionAdviceBasicBean1.setPrimaryAccountNumber("300000000000000");
//
//		transactionAdviceBasicBean1.setCardExpiryDate("1405");
//
//		transactionAdviceBasicBean1.setTransactionDate("20110705");
//
//		transactionAdviceBasicBean1.setTransactionTime("143000");
//
//		transactionAdviceBasicBean1.setTransactionAmount("4300");
//
//		transactionAdviceBasicBean1.setProcessingCode(ProcessingCode.CREDIT.getProcessingCode());
//
//		transactionAdviceBasicBean1.setTransactionCurrencyCode(CurrencyCode.United_Kingdom_Pound_Sterling.getCurrencyCode());
//
//		transactionAdviceBasicBean1.setExtendedPaymentData("01");
//
//		transactionAdviceBasicBean1.setMerchantId("50000000000");
//
//		transactionAdviceBasicBean1.setMerchantLocationId("CAN123456789012");
//
//		transactionAdviceBasicBean1.setMerchantContactInfo(" ");
//
//		transactionAdviceBasicBean1.setTerminalId("Ter12345");
//
//		transactionAdviceBasicBean1.setPosDataCode("161201254110");
//
//		transactionAdviceBasicBean1.setInvoiceReferenceNumber("InvoiceNo123456");
//
//		transactionAdviceBasicBean1.setTabImageSequenceNumber("");
//
//		transactionAdviceBasicBean1.setMatchingKeyType("");
//
//		transactionAdviceBasicBean1.setMatchingKey("");
//
//		transactionAdviceBasicBean1.setElectronicCommerceIndicator("");
//
//		CommunicationServicesIndustryBean comBean = new CommunicationServicesIndustryBean();
//		comBean.setTransactionIdentifier("000000000000000");
//		comBean.setFormatCode(FormatCode.Communication_Services_TAA_Record.getFormatCode());
//		comBean.setAddendaTypeCode(AddendaTypeCode.Industry_Specific_Detail_Addendum_Message.getAddendaTypeCode());
//		comBean.setCallDate("20080216");
//		comBean.setCallTime("143000");
//		comBean.setCallDurationTime("120000");
//		comBean.setCallFromLocationName("Empire CA");
//		comBean.setCallFromRegionCode("CA");
//		comBean.setCallFromCountryCode("840");
//		comBean.setCallFromPhoneNumber("6025372121");
//		comBean.setCallToLocationName("Easton CA");
//		comBean.setCallToRegionCode("CA");
//		comBean.setCallToCountryCode("840");
//		comBean.setCallToPhoneNumber("6025272854");
//		comBean.setPhoneCardId("12345678");
//		comBean.setServiceDescription("dataandvoice");
//		comBean.setBillingPeriod("");
//		comBean.setCommunicationsCallTypeCode("Local");
//		comBean.setCommunicationsRateClass("D");
//
//		ArrayList taaComArray = new ArrayList();
//		taaComArray.add(comBean);
//		transactionAdviceBasicBean.setArrayTAABean(taaComArray);
//
//		LocationDetailTAABean locationDetailTAABean1 = new LocationDetailTAABean();
//
//		locationDetailTAABean1.setTransactionIdentifier("000000000000000");
//
//		locationDetailTAABean1.setAddendaTypeCode(AddendaTypeCode.Location_Detail_Addendum_Message.getAddendaTypeCode());
//
//		locationDetailTAABean1.setLocationName("XYZStoresToronto");
//
//		locationDetailTAABean1.setLocationAddress("XYZStoresToronto");
//
//		locationDetailTAABean1.setLocationCity("Toronto");
//
//		locationDetailTAABean1.setLocationRegion(RegionCode1.NOVA_SCOTIA.getRegionCode());
//
//		locationDetailTAABean1.setLocationCountryCode(RegionCode1.NOVA_SCOTIA.getCountryCode());
//
//		locationDetailTAABean1.setLocationPostalCode("M3C0C1");
//
//		locationDetailTAABean1.setMerchantCategoryCode("");
//		locationDetailTAABean1.setSellerId("");
//
//		transactionAdviceBasicBean.setLocationDetailTAABean(locationDetailTAABean);
//
//		transactionAdviceBasicType.add(transactionAdviceBasicBean1);
//
//		TransactionAdviceBasicBean transactionAdviceBasicBean2 = new TransactionAdviceBasicBean();
//
//		transactionAdviceBasicBean2.setTransactionIdentifier("000000000000000");
//
//		transactionAdviceBasicBean2.setFormatCode(FormatCode.Entertainment_Ticketing_TAA_Record.getFormatCode());
//
//		transactionAdviceBasicBean2.setMediaCode(MediaCode.Cardmember_signature_on_File.getMediaCode());
//
//		transactionAdviceBasicBean2.setSubmissionMethod(SubmissionMethod.Amex_POS_Terminal.getSubmissionMethod());
//
//		transactionAdviceBasicBean2.setApprovalCode("712389");
//
//		transactionAdviceBasicBean2.setPrimaryAccountNumber("300000000000000");
//
//		transactionAdviceBasicBean2.setCardExpiryDate("1405");
//
//		transactionAdviceBasicBean2.setTransactionDate("20110705");
//
//		transactionAdviceBasicBean2.setTransactionTime("143000");
//
//		transactionAdviceBasicBean2.setTransactionAmount("4300");
//
//		transactionAdviceBasicBean2.setProcessingCode(ProcessingCode.CREDIT.getProcessingCode());
//
//		transactionAdviceBasicBean2.setTransactionCurrencyCode(CurrencyCode.United_Kingdom_Pound_Sterling.getCurrencyCode());
//
//		transactionAdviceBasicBean2.setExtendedPaymentData("01");
//
//		transactionAdviceBasicBean2.setMerchantId("50000000000");
//
//		transactionAdviceBasicBean2.setMerchantLocationId("CAN123456789012");
//
//		transactionAdviceBasicBean2.setMerchantContactInfo(" ");
//
//		transactionAdviceBasicBean2.setTerminalId("Ter12345");
//
//		transactionAdviceBasicBean2.setPosDataCode("161201254110");
//
//		transactionAdviceBasicBean2.setInvoiceReferenceNumber("InvoiceNo123456");
//
//		transactionAdviceBasicBean2.setTabImageSequenceNumber("");
//
//		transactionAdviceBasicBean2.setMatchingKeyType("");
//
//		transactionAdviceBasicBean2.setMatchingKey("");
//
//		transactionAdviceBasicBean2.setElectronicCommerceIndicator("");
//
//		EntertainmentTicketingIndustryTAABean etBean = new EntertainmentTicketingIndustryTAABean();
//
//		etBean.setTransactionIdentifier("000000000000000");
//		etBean.setAddendaTypeCode(AddendaTypeCode.Industry_Specific_Detail_Addendum_Message.getAddendaTypeCode());
//		etBean.setFormatCode("03");
//
//		etBean.setEventName("ANY");
//		etBean.setEventDate("20080216");
//		etBean.setEventIndividualTicketPriceAmount("76000");
//		etBean.setEventTicketQuantity("10");
//		etBean.setEventLocation("JAPAN");
//		etBean.setEventRegionCode("JP");
//		etBean.setEventCountryCode("392");
//
//		ArrayList taaEntArray = new ArrayList();
//		taaEntArray.add(etBean);
//		transactionAdviceBasicBean.setArrayTAABean(taaEntArray);
//
//		LocationDetailTAABean locationDetailTAABean2 = new LocationDetailTAABean();
//
//		locationDetailTAABean2.setTransactionIdentifier("000000000000000");
//
//		locationDetailTAABean2.setAddendaTypeCode(AddendaTypeCode.Location_Detail_Addendum_Message.getAddendaTypeCode());
//
//		locationDetailTAABean2.setLocationName("XYZStoresToronto");
//
//		locationDetailTAABean2.setLocationAddress("XYZStoresToronto");
//
//		locationDetailTAABean2.setLocationCity("Toronto");
//
//		locationDetailTAABean2.setLocationRegion(RegionCode1.NOVA_SCOTIA.getRegionCode());
//
//		locationDetailTAABean2.setLocationCountryCode(RegionCode1.NOVA_SCOTIA.getCountryCode());
//
//		locationDetailTAABean2.setLocationPostalCode("M3C0C1");
//
//		locationDetailTAABean2.setMerchantCategoryCode("");
//		locationDetailTAABean2.setSellerId("");
//
//		transactionAdviceBasicBean2.setLocationDetailTAABean(locationDetailTAABean2);
//
//		transactionAdviceBasicType.add(transactionAdviceBasicBean2);
//
//		TransactionAdviceBasicBean transactionAdviceBasicBean3 = new TransactionAdviceBasicBean();
//
//		transactionAdviceBasicBean3.setTransactionIdentifier("000000000000000");
//
//		transactionAdviceBasicBean3.setFormatCode(FormatCode.Entertainment_Ticketing_TAA_Record.getFormatCode());
//
//		transactionAdviceBasicBean3.setMediaCode(MediaCode.Cardmember_signature_on_File.getMediaCode());
//
//		transactionAdviceBasicBean3.setSubmissionMethod(SubmissionMethod.Amex_POS_Terminal.getSubmissionMethod());
//
//		transactionAdviceBasicBean3.setApprovalCode("712389");
//
//		transactionAdviceBasicBean3.setPrimaryAccountNumber("300000000000000");
//
//		transactionAdviceBasicBean3.setCardExpiryDate("1405");
//
//		transactionAdviceBasicBean3.setTransactionDate("20110705");
//
//		transactionAdviceBasicBean3.setTransactionTime("143000");
//
//		transactionAdviceBasicBean3.setTransactionAmount("4300");
//
//		transactionAdviceBasicBean3.setProcessingCode(ProcessingCode.CREDIT.getProcessingCode());
//
//		transactionAdviceBasicBean3.setTransactionCurrencyCode(CurrencyCode.United_Kingdom_Pound_Sterling.getCurrencyCode());
//
//		transactionAdviceBasicBean3.setExtendedPaymentData("01");
//
//		transactionAdviceBasicBean3.setMerchantId("50000000000");
//
//		transactionAdviceBasicBean3.setMerchantLocationId("CAN123456789012");
//
//		transactionAdviceBasicBean3.setMerchantContactInfo(" ");
//
//		transactionAdviceBasicBean3.setTerminalId("Ter12345");
//
//		transactionAdviceBasicBean3.setPosDataCode("161201254110");
//
//		transactionAdviceBasicBean3.setInvoiceReferenceNumber("InvoiceNo123456");
//
//		transactionAdviceBasicBean3.setTabImageSequenceNumber("");
//
//		transactionAdviceBasicBean3.setMatchingKeyType("");
//
//		transactionAdviceBasicBean3.setMatchingKey("");
//
//		transactionAdviceBasicBean3.setElectronicCommerceIndicator("");
//
//		InsuranceIndustryTAABean insBean = new InsuranceIndustryTAABean();
//
//		insBean.setRecordType("TAA");
//		insBean.setTransactionIdentifier("000000000000000");
//		insBean.setFormatCode(FormatCode.Insurance_TAA_Record.getFormatCode());
//		insBean.setAddendaTypeCode(AddendaTypeCode.Industry_Specific_Detail_Addendum_Message.getAddendaTypeCode());
//		insBean.setInsurancePolicyNumber("123456789012");
//		insBean.setInsuranceCoverageStartDate("20100204");
//		insBean.setInsuranceCoverageEndDate("20120629");
//		insBean.setInsurancePolicyPremiumFrequency("MONTHLY");
//		insBean.setAdditionalInsurancePolicyNumber("");
//		insBean.setNameOfInsured("XYZ");
//		insBean.setTypeOfPolicy("TERM LIFE");
//
//		ArrayList taaInsArray = new ArrayList();
//		taaInsArray.add(insBean);
//		transactionAdviceBasicBean.setArrayTAABean(taaInsArray);
//
//		LocationDetailTAABean locationDetailTAABean3 = new LocationDetailTAABean();
//
//		locationDetailTAABean3.setTransactionIdentifier("000000000000000");
//
//		locationDetailTAABean3.setAddendaTypeCode(AddendaTypeCode.Location_Detail_Addendum_Message.getAddendaTypeCode());
//
//		locationDetailTAABean3.setLocationName("XYZStoresToronto");
//
//		locationDetailTAABean3.setLocationAddress("XYZStoresToronto");
//
//		locationDetailTAABean3.setLocationCity("Toronto");
//
//		locationDetailTAABean3.setLocationRegion(RegionCode1.NOVA_SCOTIA.getRegionCode());
//
//		locationDetailTAABean3.setLocationCountryCode(RegionCode1.NOVA_SCOTIA.getCountryCode());
//
//		locationDetailTAABean3.setLocationPostalCode("M3C0C1");
//
//		locationDetailTAABean3.setMerchantCategoryCode("");
//		locationDetailTAABean3.setSellerId("");
//
//		transactionAdviceBasicBean3.setLocationDetailTAABean(locationDetailTAABean3);
//
//		transactionAdviceBasicType.add(transactionAdviceBasicBean3);
//
//		TransactionAdviceBasicBean transactionAdviceBasicBean4 = new TransactionAdviceBasicBean();
//
//		transactionAdviceBasicBean4.setTransactionIdentifier("000000000000000");
//
//		transactionAdviceBasicBean4.setFormatCode(FormatCode.Entertainment_Ticketing_TAA_Record.getFormatCode());
//
//		transactionAdviceBasicBean4.setMediaCode(MediaCode.Cardmember_signature_on_File.getMediaCode());
//
//		transactionAdviceBasicBean4.setSubmissionMethod(SubmissionMethod.Amex_POS_Terminal.getSubmissionMethod());
//
//		transactionAdviceBasicBean4.setApprovalCode("712389");
//
//		transactionAdviceBasicBean4.setPrimaryAccountNumber("300000000000000");
//
//		transactionAdviceBasicBean4.setCardExpiryDate("1405");
//
//		transactionAdviceBasicBean4.setTransactionDate("20110705");
//
//		transactionAdviceBasicBean4.setTransactionTime("143000");
//
//		transactionAdviceBasicBean4.setTransactionAmount("4300");
//
//		transactionAdviceBasicBean4.setProcessingCode(ProcessingCode.CREDIT.getProcessingCode());
//
//		transactionAdviceBasicBean4.setTransactionCurrencyCode(CurrencyCode.United_Kingdom_Pound_Sterling.getCurrencyCode());
//
//		transactionAdviceBasicBean4.setExtendedPaymentData("01");
//
//		transactionAdviceBasicBean4.setMerchantId("50000000000");
//
//		transactionAdviceBasicBean4.setMerchantLocationId("CAN123456789012");
//
//		transactionAdviceBasicBean4.setMerchantContactInfo(" ");
//
//		transactionAdviceBasicBean4.setTerminalId("Ter12345");
//
//		transactionAdviceBasicBean4.setPosDataCode("161201254110");
//
//		transactionAdviceBasicBean4.setInvoiceReferenceNumber("InvoiceNo123456");
//
//		transactionAdviceBasicBean4.setTabImageSequenceNumber("");
//
//		transactionAdviceBasicBean4.setMatchingKeyType("");
//
//		transactionAdviceBasicBean4.setMatchingKey("");
//
//		transactionAdviceBasicBean4.setElectronicCommerceIndicator("");
//
//		LodgingIndustryTAABean lodgingIndustryTAABean = new LodgingIndustryTAABean();
//
//		lodgingIndustryTAABean.setTransactionIdentifier("000000000000000");
//
//		lodgingIndustryTAABean.setFormatCode(FormatCode.Lodging_TAA_Record.getFormatCode());
//
//		lodgingIndustryTAABean.setAddendaTypeCode(AddendaTypeCode.Industry_Specific_Detail_Addendum_Message.getAddendaTypeCode());
//
//		lodgingIndustryTAABean.setLodgingSpecialProgramCode("1");
//
//		lodgingIndustryTAABean.setLodgingCheckInDate("20110815");
//
//		lodgingIndustryTAABean.setLodgingCheckOutDate("20110819");
//
//		lodgingIndustryTAABean.setLodgingRoomRate1("1000");
//
//		lodgingIndustryTAABean.setNumberOfNightsAtRoomRate1("3");
//
//		lodgingIndustryTAABean.setLodgingRoomRate2("1000");
//
//		lodgingIndustryTAABean.setNumberOfNightsAtRoomRate2("3");
//
//		lodgingIndustryTAABean.setLodgingRoomRate3("1000");
//
//		lodgingIndustryTAABean.setNumberOfNightsAtRoomRate3("3");
//
//		lodgingIndustryTAABean.setLodgingRenterName("ABCRenter");
//
//		lodgingIndustryTAABean.setLodgingFolioNumber("Lod123");
//
//		ArrayList taaLodgArray = new ArrayList();
//		taaLodgArray.add(lodgingIndustryTAABean);
//		transactionAdviceBasicBean.setArrayTAABean(taaLodgArray);
//
//		LocationDetailTAABean locationDetailTAABean4 = new LocationDetailTAABean();
//
//		locationDetailTAABean4.setTransactionIdentifier("000000000000000");
//
//		locationDetailTAABean4.setAddendaTypeCode(AddendaTypeCode.Location_Detail_Addendum_Message.getAddendaTypeCode());
//
//		locationDetailTAABean4.setLocationName("XYZStoresToronto");
//
//		locationDetailTAABean4.setLocationAddress("XYZStoresToronto");
//
//		locationDetailTAABean4.setLocationCity("Toronto");
//
//		locationDetailTAABean4.setLocationRegion(RegionCode1.NOVA_SCOTIA.getRegionCode());
//
//		locationDetailTAABean4.setLocationCountryCode(RegionCode1.NOVA_SCOTIA.getCountryCode());
//
//		locationDetailTAABean4.setLocationPostalCode("M3C0C1");
//
//		locationDetailTAABean4.setMerchantCategoryCode("");
//		locationDetailTAABean4.setSellerId("");
//
//		transactionAdviceBasicBean4.setLocationDetailTAABean(locationDetailTAABean4);
//
//		transactionAdviceBasicType.add(transactionAdviceBasicBean4);
//
//		TransactionAdviceBasicBean transactionAdviceBasicBean5 = new TransactionAdviceBasicBean();
//
//		transactionAdviceBasicBean5.setTransactionIdentifier("000000000000000");
//
//		transactionAdviceBasicBean5.setFormatCode(FormatCode.Entertainment_Ticketing_TAA_Record.getFormatCode());
//
//		transactionAdviceBasicBean5.setMediaCode(MediaCode.Cardmember_signature_on_File.getMediaCode());
//
//		transactionAdviceBasicBean5.setSubmissionMethod(SubmissionMethod.Amex_POS_Terminal.getSubmissionMethod());
//
//		transactionAdviceBasicBean5.setApprovalCode("712389");
//
//		transactionAdviceBasicBean5.setPrimaryAccountNumber("300000000000000");
//
//		transactionAdviceBasicBean5.setCardExpiryDate("1405");
//
//		transactionAdviceBasicBean5.setTransactionDate("20110705");
//
//		transactionAdviceBasicBean5.setTransactionTime("143000");
//
//		transactionAdviceBasicBean5.setTransactionAmount("4300");
//
//		transactionAdviceBasicBean5.setProcessingCode(ProcessingCode.CREDIT.getProcessingCode());
//
//		transactionAdviceBasicBean5.setTransactionCurrencyCode(CurrencyCode.United_Kingdom_Pound_Sterling.getCurrencyCode());
//
//		transactionAdviceBasicBean5.setExtendedPaymentData("01");
//
//		transactionAdviceBasicBean5.setMerchantId("50000000000");
//
//		transactionAdviceBasicBean5.setMerchantLocationId("CAN123456789012");
//
//		transactionAdviceBasicBean5.setMerchantContactInfo(" ");
//
//		transactionAdviceBasicBean5.setTerminalId("Ter12345");
//
//		transactionAdviceBasicBean5.setPosDataCode("161201254110");
//
//		transactionAdviceBasicBean5.setInvoiceReferenceNumber("InvoiceNo123456");
//
//		transactionAdviceBasicBean5.setTabImageSequenceNumber("");
//
//		transactionAdviceBasicBean5.setMatchingKeyType("");
//
//		transactionAdviceBasicBean5.setMatchingKey("");
//
//		transactionAdviceBasicBean5.setElectronicCommerceIndicator("");
//
//		RailIndustryTAABean raiBean = new RailIndustryTAABean();
//
//		raiBean.setTransactionIdentifier("000000000000000");
//		raiBean.setFormatCode(FormatCode.Rail_Rental_TAA_Record.getFormatCode());
//		raiBean.setAddendaTypeCode(AddendaTypeCode.Industry_Specific_Detail_Addendum_Message.getAddendaTypeCode());
//		raiBean.setTransactionType("EXC");
//		raiBean.setTicketNumber("TKT012345678912");
//		raiBean.setPassengerName("RamAurShyam");
//		raiBean.setIataCarrierCode(" ");
//		raiBean.setTicketIssueCity("Berlin");
//		raiBean.setTicketIssuerName("SaiAgency");
//
//		raiBean.setDepartureStation1("BLN");
//		raiBean.setDepartureDate1("20080216");
//		raiBean.setArrivalStation1("CHS");
//
//		raiBean.setDepartureStation2("TBT");
//		raiBean.setDepartureDate2("20080218");
//		raiBean.setArrivalStation2("MSK");
//
//		raiBean.setDepartureStation3("MNC");
//		raiBean.setDepartureDate3("20080220");
//		raiBean.setArrivalStation3("PRS");
//
//		raiBean.setDepartureStation4("RGA");
//		raiBean.setDepartureDate4("20080222");
//		raiBean.setArrivalStation4("STL");
//
//		ArrayList taaRailArray = new ArrayList();
//		taaRailArray.add(raiBean);
//		transactionAdviceBasicBean.setArrayTAABean(taaRailArray);
//
//		LocationDetailTAABean locationDetailTAABean5 = new LocationDetailTAABean();
//
//		locationDetailTAABean5.setTransactionIdentifier("000000000000000");
//
//		locationDetailTAABean5.setAddendaTypeCode(AddendaTypeCode.Location_Detail_Addendum_Message.getAddendaTypeCode());
//
//		locationDetailTAABean5.setLocationName("XYZStoresToronto");
//
//		locationDetailTAABean5.setLocationAddress("XYZStoresToronto");
//
//		locationDetailTAABean5.setLocationCity("Toronto");
//
//		locationDetailTAABean5.setLocationRegion(RegionCode1.NOVA_SCOTIA.getRegionCode());
//
//		locationDetailTAABean5.setLocationCountryCode(RegionCode1.NOVA_SCOTIA.getCountryCode());
//
//		locationDetailTAABean5.setLocationPostalCode("M3C0C1");
//
//		locationDetailTAABean5.setMerchantCategoryCode("");
//		locationDetailTAABean5.setSellerId("");
//
//		transactionAdviceBasicBean5.setLocationDetailTAABean(locationDetailTAABean5);
//
//		transactionAdviceBasicType.add(transactionAdviceBasicBean5);
//
//		TransactionAdviceBasicBean transactionAdviceBasicBean6 = new TransactionAdviceBasicBean();
//
//		transactionAdviceBasicBean6.setTransactionIdentifier("000000000000000");
//
//		transactionAdviceBasicBean6.setFormatCode(FormatCode.Entertainment_Ticketing_TAA_Record.getFormatCode());
//
//		transactionAdviceBasicBean6.setMediaCode(MediaCode.Cardmember_signature_on_File.getMediaCode());
//
//		transactionAdviceBasicBean6.setSubmissionMethod(SubmissionMethod.Amex_POS_Terminal.getSubmissionMethod());
//
//		transactionAdviceBasicBean6.setApprovalCode("712389");
//
//		transactionAdviceBasicBean6.setPrimaryAccountNumber("300000000000000");
//
//		transactionAdviceBasicBean6.setCardExpiryDate("1405");
//
//		transactionAdviceBasicBean6.setTransactionDate("20110705");
//
//		transactionAdviceBasicBean6.setTransactionTime("143000");
//
//		transactionAdviceBasicBean6.setTransactionAmount("4300");
//
//		transactionAdviceBasicBean6.setProcessingCode(ProcessingCode.CREDIT.getProcessingCode());
//
//		transactionAdviceBasicBean6.setTransactionCurrencyCode(CurrencyCode.United_Kingdom_Pound_Sterling.getCurrencyCode());
//
//		transactionAdviceBasicBean6.setExtendedPaymentData("01");
//
//		transactionAdviceBasicBean6.setMerchantId("50000000000");
//
//		transactionAdviceBasicBean6.setMerchantLocationId("CAN123456789012");
//
//		transactionAdviceBasicBean6.setMerchantContactInfo(" ");
//
//		transactionAdviceBasicBean6.setTerminalId("Ter12345");
//
//		transactionAdviceBasicBean6.setPosDataCode("161201254110");
//
//		transactionAdviceBasicBean6.setInvoiceReferenceNumber("InvoiceNo123456");
//
//		transactionAdviceBasicBean6.setTabImageSequenceNumber("");
//
//		transactionAdviceBasicBean6.setMatchingKeyType("");
//
//		transactionAdviceBasicBean6.setMatchingKey("");
//
//		transactionAdviceBasicBean6.setElectronicCommerceIndicator("");
//
//		AutoRentalIndustryTAABean autoBean = new AutoRentalIndustryTAABean();
//		autoBean.setFormatCode(FormatCode.Auto_Rental_TAA_Record.getFormatCode());
//		autoBean.setAddendaTypeCode(AddendaTypeCode.Industry_Specific_Detail_Addendum_Message.getAddendaTypeCode());
//		autoBean.setTransactionIdentifier("000000000000000");
//		autoBean.setAutoRentalAgreementNumber("Auto1234567890");
//		autoBean.setAutoRentalPickupLocation("New Brunswick Caneda");
//		autoBean.setAutoRentalPickupCityName("New Brunswick Caneda");
//		autoBean.setAutoRentalPickupRegionCode(RegionCode3.NEW_YORK.getRegionCode());
//		autoBean.setAutoRentalPickupCountryCode(RegionCode3.NEW_YORK.getCountryCode());
//		autoBean.setAutoRentalPickupTime("143000");
//		autoBean.setAutoRentalPickupDate("20110706");
//		autoBean.setAutoRentalReturnCityName("Phoenix");
//		autoBean.setAutoRentalReturnRegionCode(RegionCode3.NEW_YORK.getRegionCode());
//		autoBean.setAutoRentalReturnCountryCode(RegionCode3.NEW_YORK.getCountryCode());
//		autoBean.setAutoRentalReturnDate("20110706");
//		autoBean.setAutoRentalReturnTime("142000");
//		autoBean.setAutoRentalRenterName("ABC Auto Ltd");
//		autoBean.setAutoRentalVehicleClassId("0011");
//		autoBean.setAutoRentalDistance("145");
//		autoBean.setAutoRentalDistanceUnitOfMeasure("K");
//		autoBean.setAutoRentalAuditAdjustmentIndicator("Y");
//		autoBean.setAutoRentalAuditAdjustmentAmount("1000");
//		autoBean.setReturnDropoffLocation("New York");
//		autoBean.setVehicleIdentificationNumber("NY 1234");
//		autoBean.setDriverIdentificationNumber("DR 1234");
//		autoBean.setDriverTaxNumber("TX1234");
//
//		ArrayList taaAutoArray = new ArrayList();
//		taaAutoArray.add(autoBean);
//		transactionAdviceBasicBean.setArrayTAABean(taaAutoArray);
//
//		LocationDetailTAABean locationDetailTAABean6 = new LocationDetailTAABean();
//
//		locationDetailTAABean6.setTransactionIdentifier("000000000000000");
//
//		locationDetailTAABean6.setAddendaTypeCode(AddendaTypeCode.Location_Detail_Addendum_Message.getAddendaTypeCode());
//
//		locationDetailTAABean6.setLocationName("XYZStoresToronto");
//
//		locationDetailTAABean6.setLocationAddress("XYZStoresToronto");
//
//		locationDetailTAABean6.setLocationCity("Toronto");
//
//		locationDetailTAABean6.setLocationRegion(RegionCode1.NOVA_SCOTIA.getRegionCode());
//
//		locationDetailTAABean6.setLocationCountryCode(RegionCode1.NOVA_SCOTIA.getCountryCode());
//
//		locationDetailTAABean6.setLocationPostalCode("M3C0C1");
//
//		locationDetailTAABean6.setMerchantCategoryCode("");
//		locationDetailTAABean6.setSellerId("");
//
//		transactionAdviceBasicBean6.setLocationDetailTAABean(locationDetailTAABean6);
//
//		transactionAdviceBasicType.add(transactionAdviceBasicBean6);
//
//		TransactionAdviceBasicBean transactionAdviceBasicBean7 = new TransactionAdviceBasicBean();
//
//		transactionAdviceBasicBean7.setTransactionIdentifier("000000000000000");
//
//		transactionAdviceBasicBean7.setFormatCode(FormatCode.Entertainment_Ticketing_TAA_Record.getFormatCode());
//
//		transactionAdviceBasicBean7.setMediaCode(MediaCode.Cardmember_signature_on_File.getMediaCode());
//
//		transactionAdviceBasicBean7.setSubmissionMethod(SubmissionMethod.Amex_POS_Terminal.getSubmissionMethod());
//
//		transactionAdviceBasicBean7.setApprovalCode("712389");
//
//		transactionAdviceBasicBean7.setPrimaryAccountNumber("300000000000000");
//
//		transactionAdviceBasicBean7.setCardExpiryDate("1405");
//
//		transactionAdviceBasicBean7.setTransactionDate("20110705");
//
//		transactionAdviceBasicBean7.setTransactionTime("143000");
//
//		transactionAdviceBasicBean7.setTransactionAmount("4300");
//
//		transactionAdviceBasicBean7.setProcessingCode(ProcessingCode.CREDIT.getProcessingCode());
//
//		transactionAdviceBasicBean7.setTransactionCurrencyCode(CurrencyCode.United_Kingdom_Pound_Sterling.getCurrencyCode());
//
//		transactionAdviceBasicBean7.setExtendedPaymentData("01");
//
//		transactionAdviceBasicBean7.setMerchantId("50000000000");
//
//		transactionAdviceBasicBean7.setMerchantLocationId("CAN123456789012");
//
//		transactionAdviceBasicBean7.setMerchantContactInfo(" ");
//
//		transactionAdviceBasicBean7.setTerminalId("Ter12345");
//
//		transactionAdviceBasicBean7.setPosDataCode("161201254110");
//
//		transactionAdviceBasicBean7.setInvoiceReferenceNumber("InvoiceNo123456");
//
//		transactionAdviceBasicBean7.setTabImageSequenceNumber("");
//
//		transactionAdviceBasicBean7.setMatchingKeyType("");
//
//		transactionAdviceBasicBean7.setMatchingKey("");
//
//		transactionAdviceBasicBean7.setElectronicCommerceIndicator("");
//
//		CPSLevel2Bean cpsl2Bean = new CPSLevel2Bean();
//		cpsl2Bean.setTransactionIdentifier("000000000000000");
//		cpsl2Bean.setAddendaTypeCode(AddendaTypeCode.Corporate_Purchasing_Solutions.getAddendaTypeCode());
//		cpsl2Bean.setRequesterName("RAM");
//		cpsl2Bean.setChargeDescription1("Pen");
//		cpsl2Bean.setChargeItemQuantity1("1");
//		cpsl2Bean.setChargeItemAmount1("7300");
//		cpsl2Bean.setChargeDescription2("Pencil");
//		cpsl2Bean.setChargeItemQuantity2("4");
//		cpsl2Bean.setChargeItemAmount2("500");
//		cpsl2Bean.setChargeDescription3("Note Book");
//		cpsl2Bean.setChargeItemQuantity3("4");
//		cpsl2Bean.setChargeItemAmount3("500");
//		cpsl2Bean.setChargeDescription4("Note Pad");
//		cpsl2Bean.setChargeItemQuantity4("4");
//		cpsl2Bean.setChargeItemAmount4("4000");
//		cpsl2Bean.setCardMemberReferenceNumber("CARDNO123456789");
//		cpsl2Bean.setShipToPostalCode("SHIP0123456789");
//		cpsl2Bean.setTotalTaxAmount("10");
//		cpsl2Bean.setTaxTypeCode(TaxTypeCd.Sales_Tax.getTaxTypeCd());
//
//		ArrayList taaCpsl2Array = new ArrayList();
//		taaCpsl2Array.add(cpsl2Bean);
//		transactionAdviceBasicBean.setArrayTAABean(taaCpsl2Array);
//
//		LocationDetailTAABean locationDetailTAABean7 = new LocationDetailTAABean();
//
//		locationDetailTAABean7.setTransactionIdentifier("000000000000000");
//
//		locationDetailTAABean7.setAddendaTypeCode(AddendaTypeCode.Location_Detail_Addendum_Message.getAddendaTypeCode());
//
//		locationDetailTAABean7.setLocationName("XYZStoresToronto");
//
//		locationDetailTAABean7.setLocationAddress("XYZStoresToronto");
//
//		locationDetailTAABean7.setLocationCity("Toronto");
//
//		locationDetailTAABean7.setLocationRegion(RegionCode1.NOVA_SCOTIA.getRegionCode());
//
//		locationDetailTAABean7.setLocationCountryCode(RegionCode1.NOVA_SCOTIA.getCountryCode());
//
//		locationDetailTAABean7.setLocationPostalCode("M3C0C1");
//
//		locationDetailTAABean7.setMerchantCategoryCode("");
//		locationDetailTAABean7.setSellerId("");
//
//		transactionAdviceBasicBean7.setLocationDetailTAABean(locationDetailTAABean7);
//
//		transactionAdviceBasicType.add(transactionAdviceBasicBean7);
//
//		TransactionAdviceBasicBean transactionAdviceBasicBean8 = new TransactionAdviceBasicBean();
//
//		transactionAdviceBasicBean8.setTransactionIdentifier("000000000000000");
//
//		transactionAdviceBasicBean8.setFormatCode(FormatCode.Entertainment_Ticketing_TAA_Record.getFormatCode());
//
//		transactionAdviceBasicBean8.setMediaCode(MediaCode.Cardmember_signature_on_File.getMediaCode());
//
//		transactionAdviceBasicBean8.setSubmissionMethod(SubmissionMethod.Amex_POS_Terminal.getSubmissionMethod());
//
//		transactionAdviceBasicBean8.setApprovalCode("712389");
//
//		transactionAdviceBasicBean8.setPrimaryAccountNumber("300000000000000");
//
//		transactionAdviceBasicBean8.setCardExpiryDate("1405");
//
//		transactionAdviceBasicBean8.setTransactionDate("20110705");
//
//		transactionAdviceBasicBean8.setTransactionTime("143000");
//
//		transactionAdviceBasicBean8.setTransactionAmount("4300");
//
//		transactionAdviceBasicBean8.setProcessingCode(ProcessingCode.CREDIT.getProcessingCode());
//
//		transactionAdviceBasicBean8.setTransactionCurrencyCode(CurrencyCode.United_Kingdom_Pound_Sterling.getCurrencyCode());
//
//		transactionAdviceBasicBean8.setExtendedPaymentData("01");
//
//		transactionAdviceBasicBean8.setMerchantId("50000000000");
//
//		transactionAdviceBasicBean8.setMerchantLocationId("CAN123456789012");
//
//		transactionAdviceBasicBean8.setMerchantContactInfo(" ");
//
//		transactionAdviceBasicBean8.setTerminalId("Ter12345");
//
//		transactionAdviceBasicBean8.setPosDataCode("161201254110");
//
//		transactionAdviceBasicBean8.setInvoiceReferenceNumber("InvoiceNo123456");
//
//		transactionAdviceBasicBean8.setTabImageSequenceNumber("");
//
//		transactionAdviceBasicBean8.setMatchingKeyType("");
//
//		transactionAdviceBasicBean8.setMatchingKey("");
//
//		transactionAdviceBasicBean8.setElectronicCommerceIndicator("");
//
//		DeferredPaymentPlanBean dpPlanBean = new DeferredPaymentPlanBean();
//
//		dpPlanBean.setTransactionIdentifier("000000000000000");
//		dpPlanBean.setAddendaTypeCode(AddendaTypeCode.Deferred_Payment_Plan_Addendum_Message.getAddendaTypeCode());
//		dpPlanBean.setRecordType("TAA");
//		dpPlanBean.setFullTransactionAmount("000000000000");
//		dpPlanBean.setTypeOfPlanCode("005");
//		dpPlanBean.setNoOfInstallments("0002");
//		dpPlanBean.setAmountOfInstallment("000000000000");
//		dpPlanBean.setInstallmentNumber("0001");
//		dpPlanBean.setContractNumber("0");
//		dpPlanBean.setPaymentTypeCode1(" ");
//		dpPlanBean.setPaymentTypeAmount1("000000000000");
//		dpPlanBean.setPaymentTypeCode2(" ");
//		dpPlanBean.setPaymentTypeAmount2("000000000000");
//		dpPlanBean.setPaymentTypeCode3(" ");
//		dpPlanBean.setPaymentTypeAmount3("000000000000");
//		dpPlanBean.setPaymentTypeCode4(" ");
//		dpPlanBean.setPaymentTypeAmount4("000000000000");
//		dpPlanBean.setPaymentTypeCode5(" ");
//		dpPlanBean.setPaymentTypeAmount5("000000000000");
//
//		ArrayList taaDppArray = new ArrayList();
//		taaDppArray.add(dpPlanBean);
//		transactionAdviceBasicBean.setArrayTAABean(taaDppArray);
//
//		EMVBean emvBean = new EMVBean();
//
//		emvBean.setTransactionIdentifier("000000000000000");
//		emvBean.setAddendaTypeCode(AddendaTypeCode.EMV_Addendum_Message.getAddendaTypeCode());
//		emvBean.setEMVFormatType("01");
//		emvBean.setFormatCode(FormatCode.No_Industry_Specific_TAA_Record.getFormatCode());
//		emvBean.setICCSystemRelatedData("AGNS000110A65CE789EEA36206020103A081106046F755000200A000800008010100000000010000012401245C0000000001000000806456464564645654654654645645645645645645645654645645645654645654645645645645645645646456546456464646456546456456456546456456456456456456464564564564");
//
//		ArrayList taaEmvArray = new ArrayList();
//		taaEmvArray.add(emvBean);
//		transactionAdviceBasicBean.setArrayTAABean(taaEmvArray);
//
//		TransactionBatchTrailerBean transactionBatchTrailerBean = new TransactionBatchTrailerBean();
//
//		transactionBatchTrailerBean.setMerchantId("50000000000");
//
//		transactionBatchTrailerBean.setTbtIdentificationNumber("000000000000000");
//
//		transactionBatchTrailerBean.setTbtCreationDate("20110711");
//
//		transactionBatchTrailerBean.setTotalNoOfTabs("10");
//
//		transactionBatchTrailerBean.setTbtAmount("21500");
//
//		transactionBatchTrailerBean.setTbtAmountSign("-");
//
//		transactionBatchTrailerBean.setTbtCurrencyCode(CurrencyCode.United_Kingdom_Pound_Sterling.getCurrencyCode());
//
//		transactionBatchTrailerBean.setTbtImageSequenceNumber("");
//
//		TransactionTBTSpecificBean transactionTBTSpecificBean = new TransactionTBTSpecificBean();
//
//		transactionTBTSpecificBean.setTransactionAdviceBasicType(transactionAdviceBasicType);
//		transactionTBTSpecificBean.setTransactionBatchTrailerBean(transactionBatchTrailerBean);
//
//		List<TransactionTBTSpecificBean> transactionTBTSpecificType = new ArrayList();
//
//		transactionTBTSpecificType.add(transactionTBTSpecificBean);
//
//		SettlementRequestDataObject settlementRequestDataObject = new SettlementRequestDataObject();
//
//		settlementRequestDataObject.setTransactionFileHeaderType(transactionFileHeaderBean);
//
//		settlementRequestDataObject.setTransactionTBTSpecificType(transactionTBTSpecificType);
//
//		List errorCodes = new ArrayList();
//
//		PropertyReader propertyReader = new PropertyReader();
//
//		propertyReader = propertyReader.loadPropertyFile("/settlement.properties");
//
//		SettlementResponseDataObject settlementResponseDataObject = RequestProcessorFactory.processRequest(settlementRequestDataObject, propertyReader);
//		try {
//			if (!settlementResponseDataObject.getActionCodeDescription().isEmpty()) {
//				Iterator<?> iterater = settlementResponseDataObject.getActionCodeDescription().iterator();
//				while (iterater.hasNext()) {
//					ErrorObject errorObject = (ErrorObject) iterater.next();
//					logger.debug(errorObject.getErrorCode() + "-" + errorObject.getErrorDescription() + "\n");
//				}
//			} else {
//				logger.debug("File Successfully Submitted\n");
//			}
//		} catch (Exception e) {
//			logger.debug("Error Object is Empty");
//		}
//	}
//
//	private void file_with_Optional_TAD_Required_Location_Detail_TAA_and_Optional_Industry_or_Non_Industry_Specific_TAA_Records() throws SettlementException {
//		ArrayList taaRecArray = new ArrayList();
//
//		TransactionFileHeaderBean transactionFileHeaderBean = new TransactionFileHeaderBean();
//
//		transactionFileHeaderBean.setSubmitterId("G00002");
//
//		transactionFileHeaderBean.setSubmitterFileReferenceNumber("G00002001");
//
//		transactionFileHeaderBean.setSubmitterFileSequenceNumber("1");
//
//		transactionFileHeaderBean.setFileCreationDate("20110718");
//
//		transactionFileHeaderBean.setFileCreationTime("143000");
//
//		List transactionAdviceBasicType = new ArrayList();
//
//		TransactionAdviceBasicBean transactionAdviceBasicBean = new TransactionAdviceBasicBean();
//
//		transactionAdviceBasicBean.setTransactionIdentifier("000000000000000");
//
//		transactionAdviceBasicBean.setFormatCode(FormatCode.No_Industry_Specific_TAA_Record.getFormatCode());
//
//		transactionAdviceBasicBean.setMediaCode(MediaCode.Cardmember_signature_on_File.getMediaCode());
//
//		transactionAdviceBasicBean.setSubmissionMethod(SubmissionMethod.Amex_POS_Terminal.getSubmissionMethod());
//
//		transactionAdviceBasicBean.setApprovalCode("712389");
//
//		transactionAdviceBasicBean.setPrimaryAccountNumber("300000000000000000");
//
//		transactionAdviceBasicBean.setCardExpiryDate("1405");
//
//		transactionAdviceBasicBean.setTransactionDate("20110705");
//
//		transactionAdviceBasicBean.setTransactionTime("143000");
//
//		transactionAdviceBasicBean.setTransactionAmount("4300");
//
//		transactionAdviceBasicBean.setProcessingCode(ProcessingCode.CREDIT.getProcessingCode());
//
//		transactionAdviceBasicBean.setTransactionCurrencyCode(CurrencyCode.United_Kingdom_Pound_Sterling.getCurrencyCode());
//
//		transactionAdviceBasicBean.setExtendedPaymentData("01");
//
//		transactionAdviceBasicBean.setMerchantId("50000000000");
//
//		transactionAdviceBasicBean.setMerchantLocationId("CAN123456789012");
//
//		transactionAdviceBasicBean.setMerchantContactInfo(" ");
//
//		transactionAdviceBasicBean.setTerminalId("Ter12345");
//
//		transactionAdviceBasicBean.setPosDataCode("161201254110");
//
//		transactionAdviceBasicBean.setInvoiceReferenceNumber("InvoiceNo123456");
//
//		transactionAdviceBasicBean.setTabImageSequenceNumber("");
//
//		transactionAdviceBasicBean.setMatchingKeyType("");
//
//		transactionAdviceBasicBean.setMatchingKey("");
//
//		transactionAdviceBasicBean.setElectronicCommerceIndicator("");
//
//		AirlineIndustryTAABean airBean = new AirlineIndustryTAABean();
//
//		airBean.setTransactionIdentifier("000000000000000");
//
//		airBean.setFormatCode(FormatCode.Airline_TAA_Record.getFormatCode());
//
//		airBean.setAddendaTypeCode(AddendaTypeCode.Industry_Specific_Detail_Addendum_Message.getAddendaTypeCode());
//
//		airBean.setTransactionType("TKT");
//
//		airBean.setTicketNumber("TKT12345678901");
//
//		airBean.setDocumentType("01");
//
//		airBean.setAirlineProcessIdentifier("A12");
//
//		airBean.setIataNumericCode("TKT12345");
//
//		airBean.setTicketingCarrierName("TKTCAR1234567891234567890");
//		airBean.setTicketIssueCity("NEWYORK");
//		airBean.setTicketIssueDate("20110204");
//		airBean.setNumberInParty("01");
//		airBean.setPassengerName("XYZ");
//		airBean.setConjunctionTicketIndicator("N");
//		airBean.setTotalNumberOfAirSegments("4");
//		airBean.setElectronicTicketIndicator("Y");
//		airBean.setOriginalTransactionAmount("1000");
//		airBean.setOriginalCurrencyCode(CurrencyCode.US_Dollar.getCurrencyCode());
//		airBean.setStopoverIndicator1("O");
//		airBean.setDepartureLocationCodeSegment1("MUM");
//		airBean.setDepartureDateSegment1("20110216");
//		airBean.setArrivalLocationCodeSegment1("DEL");
//		airBean.setSegmentCarrierCode1("A1");
//		airBean.setSegment1FareBasis("FAIRSEG12345678");
//		airBean.setClassOfServiceCodeSegment1("A");
//		airBean.setFlightNumberSegment1("ABC1");
//		airBean.setSegment1Fare("200");
//
//		airBean.setStopoverIndicator2("X");
//		airBean.setDepartureLocationCodeSegment2("DEL");
//		airBean.setDepartureDateSegment2("20110224");
//		airBean.setArrivalLocationCodeSegment2("NYY");
//		airBean.setSegmentCarrierCode2("A1");
//		airBean.setSegment2FareBasis("FAIRSEG12345678");
//		airBean.setClassOfServiceCodeSegment2("A2");
//		airBean.setFlightNumberSegment2("ABC2");
//		airBean.setSegment2Fare("100");
//
//		airBean.setStopoverIndicator3(" ");
//		airBean.setDepartureLocationCodeSegment3("NY");
//		airBean.setDepartureDateSegment3("20110216");
//		airBean.setArrivalLocationCodeSegment3("DAL");
//		airBean.setSegmentCarrierCode3("A1");
//		airBean.setSegment3FareBasis("FAIRSEG12345678");
//		airBean.setClassOfServiceCodeSegment3("A2");
//		airBean.setFlightNumberSegment3("ABC3");
//		airBean.setSegment3Fare("1000");
//
//		airBean.setStopoverIndicator4("O");
//		airBean.setDepartureLocationCodeSegment4("DAL");
//		airBean.setDepartureDateSegment4("20110216");
//		airBean.setArrivalLocationCodeSegment4("PHX");
//		airBean.setSegmentCarrierCode4("A1");
//		airBean.setSegment4FareBasis("FAIRSEG12345678");
//		airBean.setClassOfServiceCodeSegment4("A2");
//		airBean.setFlightNumberSegment4("ABC4");
//		airBean.setSegment4Fare("1000");
//
//		airBean.setExchangedOrOriginalTicketNumber("");
//
//		taaRecArray.add(airBean);
//		transactionAdviceBasicBean.setArrayTAABean(taaRecArray);
//
//		CPSLevel2Bean cpsl2Bean = new CPSLevel2Bean();
//		cpsl2Bean.setTransactionIdentifier("000000000000000");
//		cpsl2Bean.setAddendaTypeCode(AddendaTypeCode.Corporate_Purchasing_Solutions.getAddendaTypeCode());
//		cpsl2Bean.setRequesterName("RAM");
//		cpsl2Bean.setChargeDescription1("Pen");
//		cpsl2Bean.setChargeItemQuantity1("1");
//		cpsl2Bean.setChargeItemAmount1("7300");
//		cpsl2Bean.setChargeDescription2("Pencil");
//		cpsl2Bean.setChargeItemQuantity2("4");
//		cpsl2Bean.setChargeItemAmount2("500");
//		cpsl2Bean.setChargeDescription3("Note Book");
//		cpsl2Bean.setChargeItemQuantity3("4");
//		cpsl2Bean.setChargeItemAmount3("500");
//		cpsl2Bean.setChargeDescription4("Note Pad");
//		cpsl2Bean.setChargeItemQuantity4("4");
//		cpsl2Bean.setChargeItemAmount4("4000");
//		cpsl2Bean.setCardMemberReferenceNumber("CARDNO123456789");
//		cpsl2Bean.setShipToPostalCode("SHIP0123456789");
//		cpsl2Bean.setTotalTaxAmount("10");
//		cpsl2Bean.setTaxTypeCode(TaxTypeCd.Sales_Tax.getTaxTypeCd());
//
//		taaRecArray.add(cpsl2Bean);
//		transactionAdviceBasicBean.setArrayTAABean(taaRecArray);
//
//		DeferredPaymentPlanBean dpPlanBean = new DeferredPaymentPlanBean();
//
//		dpPlanBean.setTransactionIdentifier("000000000000000");
//		dpPlanBean.setAddendaTypeCode(AddendaTypeCode.Deferred_Payment_Plan_Addendum_Message.getAddendaTypeCode());
//		dpPlanBean.setRecordType("TAA");
//		dpPlanBean.setFullTransactionAmount("000000000000");
//		dpPlanBean.setTypeOfPlanCode("005");
//		dpPlanBean.setNoOfInstallments("0002");
//		dpPlanBean.setAmountOfInstallment("000000000000");
//		dpPlanBean.setInstallmentNumber("0001");
//		dpPlanBean.setContractNumber("0");
//		dpPlanBean.setPaymentTypeCode1(" ");
//		dpPlanBean.setPaymentTypeAmount1("000000000000");
//		dpPlanBean.setPaymentTypeCode2(" ");
//		dpPlanBean.setPaymentTypeAmount2("000000000000");
//		dpPlanBean.setPaymentTypeCode3(" ");
//		dpPlanBean.setPaymentTypeAmount3("000000000000");
//		dpPlanBean.setPaymentTypeCode4(" ");
//		dpPlanBean.setPaymentTypeAmount4("000000000000");
//		dpPlanBean.setPaymentTypeCode5(" ");
//		dpPlanBean.setPaymentTypeAmount5("000000000000");
//
//		taaRecArray.add(dpPlanBean);
//		transactionAdviceBasicBean.setArrayTAABean(taaRecArray);
//
//		EMVBean emvBean = new EMVBean();
//
//		emvBean.setTransactionIdentifier("000000000000000");
//		emvBean.setAddendaTypeCode(AddendaTypeCode.EMV_Addendum_Message.getAddendaTypeCode());
//		emvBean.setEMVFormatType("01");
//		emvBean.setFormatCode(FormatCode.No_Industry_Specific_TAA_Record.getFormatCode());
//		emvBean.setICCSystemRelatedData("AGNS000110A65CE789EEA36206020103A081106046F755000200A000800008010100000000010000012401245C0000000001000000806456464564645654654654645645645645645645645654645645645654645654645645645645645645646456546456464646456546456456456546456456456456456456464564564564");
//
//		taaRecArray.add(emvBean);
//		transactionAdviceBasicBean.setArrayTAABean(taaRecArray);
//
//		LocationDetailTAABean locationDetailTAABean = new LocationDetailTAABean();
//
//		locationDetailTAABean.setTransactionIdentifier("000000000000000");
//
//		locationDetailTAABean.setAddendaTypeCode(AddendaTypeCode.Location_Detail_Addendum_Message.getAddendaTypeCode());
//
//		locationDetailTAABean.setLocationName("XYZStoresToronto");
//
//		locationDetailTAABean.setLocationAddress("XYZStoresToronto");
//
//		locationDetailTAABean.setLocationCity("Toronto");
//
//		locationDetailTAABean.setLocationRegion(RegionCode1.NOVA_SCOTIA.getRegionCode());
//
//		locationDetailTAABean.setLocationCountryCode(RegionCode1.NOVA_SCOTIA.getCountryCode());
//
//		locationDetailTAABean.setLocationPostalCode("M3C0C1");
//
//		locationDetailTAABean.setMerchantCategoryCode("");
//		locationDetailTAABean.setSellerId("");
//
//		transactionAdviceBasicBean.setLocationDetailTAABean(locationDetailTAABean);
//
//		TransactionBatchTrailerBean transactionBatchTrailerBean = new TransactionBatchTrailerBean();
//
//		transactionBatchTrailerBean.setMerchantId("50000000000");
//
//		transactionBatchTrailerBean.setTbtIdentificationNumber("000000000000000");
//
//		transactionBatchTrailerBean.setTbtCreationDate("20110711");
//
//		transactionBatchTrailerBean.setTotalNoOfTabs("10");
//
//		transactionBatchTrailerBean.setTbtAmount("21500");
//
//		transactionBatchTrailerBean.setTbtAmountSign("-");
//
//		transactionBatchTrailerBean.setTbtCurrencyCode(CurrencyCode.United_Kingdom_Pound_Sterling.getCurrencyCode());
//
//		transactionBatchTrailerBean.setTbtImageSequenceNumber("");
//
//		TransactionTBTSpecificBean transactionTBTSpecificBean = new TransactionTBTSpecificBean();
//
//		transactionTBTSpecificBean.setTransactionAdviceBasicType(transactionAdviceBasicType);
//		transactionTBTSpecificBean.setTransactionBatchTrailerBean(transactionBatchTrailerBean);
//
//		List<TransactionTBTSpecificBean> transactionTBTSpecificType = new ArrayList();
//
//		transactionTBTSpecificType.add(transactionTBTSpecificBean);
//
//		TransactionFileSummaryBean transactionFileSummaryBean = new TransactionFileSummaryBean();
//
//		transactionFileSummaryBean.setNumberOfDebits("1");
//		transactionFileSummaryBean.setHashTotalDebitAmount("2");
//		transactionFileSummaryBean.setHashTotalCreditAmount("1000");
//		transactionFileSummaryBean.setHashTotalAmount("1000");
//		transactionFileSummaryBean.setNumberOfCredits("1");
//
//		SettlementRequestDataObject settlementRequestDataObject = new SettlementRequestDataObject();
//
//		settlementRequestDataObject.setTransactionFileHeaderType(transactionFileHeaderBean);
//
//		settlementRequestDataObject.setTransactionTBTSpecificType(transactionTBTSpecificType);
//
//		settlementRequestDataObject.setTransactionFileSummaryType(transactionFileSummaryBean);
//
//		List errorCodes = new ArrayList();
//
//		PropertyReader propertyReader = new PropertyReader();
//
//		propertyReader = propertyReader.loadPropertyFile("/settlement.properties");
//
//		SettlementResponseDataObject settlementResponseDataObject = RequestProcessorFactory.processRequest(settlementRequestDataObject, propertyReader);
//		try {
//			if (!settlementResponseDataObject.getActionCodeDescription().isEmpty()) {
//				Iterator<?> iterater = settlementResponseDataObject.getActionCodeDescription().iterator();
//				while (iterater.hasNext()) {
//					ErrorObject errorObject = (ErrorObject) iterater.next();
//					logger.debug(errorObject.getErrorCode() + "-" + errorObject.getErrorDescription() + "\n");
//				}
//			} else {
//				logger.debug("File Successfully Submitted\n");
//			}
//		} catch (Exception e) {
//			logger.debug("Error Object is Empty");
//		}
//	}
//
//	private void file_with_Multiple_Merchant_SE_Number() throws SettlementException {
//		TransactionFileHeaderBean transactionFileHeaderBean = new TransactionFileHeaderBean();
//
//		transactionFileHeaderBean.setSubmitterId("G00002");
//
//		transactionFileHeaderBean.setSubmitterFileReferenceNumber("G00002001");
//
//		transactionFileHeaderBean.setSubmitterFileSequenceNumber("1");
//
//		transactionFileHeaderBean.setFileCreationDate("20110718");
//
//		transactionFileHeaderBean.setFileCreationTime("143000");
//
//		List<TransactionAdviceBasicBean> transactionAdviceBasicType = new ArrayList();
//
//		TransactionAdviceBasicBean transactionAdviceBasicBean = new TransactionAdviceBasicBean();
//
//		transactionAdviceBasicBean.setTransactionIdentifier("000000000000000");
//
//		transactionAdviceBasicBean.setFormatCode(FormatCode.No_Industry_Specific_TAA_Record.getFormatCode());
//
//		transactionAdviceBasicBean.setMediaCode(MediaCode.Cardmember_signature_on_File.getMediaCode());
//
//		transactionAdviceBasicBean.setSubmissionMethod(SubmissionMethod.Amex_POS_Terminal.getSubmissionMethod());
//
//		transactionAdviceBasicBean.setApprovalCode("712389");
//
//		transactionAdviceBasicBean.setPrimaryAccountNumber("300000000000000");
//
//		transactionAdviceBasicBean.setCardExpiryDate("1405");
//
//		transactionAdviceBasicBean.setTransactionDate("20110705");
//
//		transactionAdviceBasicBean.setTransactionTime("143000");
//
//		transactionAdviceBasicBean.setTransactionAmount("4300");
//
//		transactionAdviceBasicBean.setProcessingCode(ProcessingCode.CREDIT.getProcessingCode());
//
//		transactionAdviceBasicBean.setTransactionCurrencyCode(CurrencyCode.United_Kingdom_Pound_Sterling.getCurrencyCode());
//
//		transactionAdviceBasicBean.setExtendedPaymentData("01");
//
//		transactionAdviceBasicBean.setMerchantId("5000000000");
//
//		transactionAdviceBasicBean.setMerchantLocationId("CAN100000089012");
//
//		transactionAdviceBasicBean.setMerchantContactInfo(" ");
//
//		transactionAdviceBasicBean.setTerminalId("Ter12345");
//
//		transactionAdviceBasicBean.setPosDataCode("161201254110");
//
//		transactionAdviceBasicBean.setInvoiceReferenceNumber("InvoiceNo123456");
//
//		transactionAdviceBasicBean.setTabImageSequenceNumber("");
//
//		transactionAdviceBasicBean.setMatchingKeyType("");
//
//		transactionAdviceBasicBean.setMatchingKey("");
//
//		transactionAdviceBasicBean.setElectronicCommerceIndicator("");
//
//		LocationDetailTAABean locationDetailTAABean = new LocationDetailTAABean();
//
//		locationDetailTAABean.setTransactionIdentifier("000000000000000");
//
//		locationDetailTAABean.setAddendaTypeCode(AddendaTypeCode.Location_Detail_Addendum_Message.getAddendaTypeCode());
//
//		locationDetailTAABean.setLocationName("XYZStoresToronto");
//
//		locationDetailTAABean.setLocationAddress("XYZStoresToronto");
//
//		locationDetailTAABean.setLocationCity("Toronto");
//
//		locationDetailTAABean.setLocationRegion(RegionCode1.NOVA_SCOTIA.getRegionCode());
//
//		locationDetailTAABean.setLocationCountryCode(RegionCode1.NOVA_SCOTIA.getCountryCode());
//
//		locationDetailTAABean.setLocationPostalCode("M3C0C1");
//
//		locationDetailTAABean.setMerchantCategoryCode("");
//		locationDetailTAABean.setSellerId("");
//
//		transactionAdviceBasicBean.setLocationDetailTAABean(locationDetailTAABean);
//
//		transactionAdviceBasicType.add(transactionAdviceBasicBean);
//
//		TransactionAdviceBasicBean transactionAdviceBasicBean1 = new TransactionAdviceBasicBean();
//
//		transactionAdviceBasicBean1.setTransactionIdentifier("000000000000000");
//
//		transactionAdviceBasicBean1.setFormatCode(FormatCode.No_Industry_Specific_TAA_Record.getFormatCode());
//
//		transactionAdviceBasicBean1.setMediaCode(MediaCode.Cardmember_signature_on_File.getMediaCode());
//
//		transactionAdviceBasicBean1.setSubmissionMethod(SubmissionMethod.Amex_POS_Terminal.getSubmissionMethod());
//
//		transactionAdviceBasicBean1.setApprovalCode("712389");
//
//		transactionAdviceBasicBean1.setPrimaryAccountNumber("3000000000000000");
//
//		transactionAdviceBasicBean1.setCardExpiryDate("1405");
//
//		transactionAdviceBasicBean1.setTransactionDate("20110705");
//
//		transactionAdviceBasicBean1.setTransactionTime("143000");
//
//		transactionAdviceBasicBean1.setTransactionAmount("4300");
//
//		transactionAdviceBasicBean1.setProcessingCode(ProcessingCode.CREDIT.getProcessingCode());
//
//		transactionAdviceBasicBean1.setTransactionCurrencyCode(CurrencyCode.United_Kingdom_Pound_Sterling.getCurrencyCode());
//
//		transactionAdviceBasicBean1.setExtendedPaymentData("01");
//
//		transactionAdviceBasicBean1.setMerchantId("5021045083");
//
//		transactionAdviceBasicBean1.setMerchantLocationId("CAN100000089012");
//
//		transactionAdviceBasicBean1.setMerchantContactInfo(" ");
//
//		transactionAdviceBasicBean1.setTerminalId("Ter12345");
//
//		transactionAdviceBasicBean1.setPosDataCode("161201254110");
//
//		transactionAdviceBasicBean1.setInvoiceReferenceNumber("InvoiceNo123456");
//
//		transactionAdviceBasicBean1.setTabImageSequenceNumber("");
//
//		transactionAdviceBasicBean1.setMatchingKeyType("");
//
//		transactionAdviceBasicBean1.setMatchingKey("");
//
//		transactionAdviceBasicBean1.setElectronicCommerceIndicator("");
//
//		LocationDetailTAABean locationDetailTAABean1 = new LocationDetailTAABean();
//
//		locationDetailTAABean1.setTransactionIdentifier("000000000000000");
//
//		locationDetailTAABean1.setAddendaTypeCode(AddendaTypeCode.Location_Detail_Addendum_Message.getAddendaTypeCode());
//
//		locationDetailTAABean1.setLocationName("XYZStoresToronto");
//
//		locationDetailTAABean1.setLocationAddress("XYZStoresToronto");
//
//		locationDetailTAABean1.setLocationCity("Toronto");
//
//		locationDetailTAABean1.setLocationRegion(RegionCode1.NOVA_SCOTIA.getRegionCode());
//
//		locationDetailTAABean1.setLocationCountryCode(RegionCode1.NOVA_SCOTIA.getCountryCode());
//
//		locationDetailTAABean1.setLocationPostalCode("M3C0C1");
//
//		locationDetailTAABean1.setMerchantCategoryCode("");
//		locationDetailTAABean1.setSellerId("");
//
//		transactionAdviceBasicBean1.setLocationDetailTAABean(locationDetailTAABean1);
//
//		transactionAdviceBasicType.add(transactionAdviceBasicBean1);
//
//		TransactionAdviceBasicBean transactionAdviceBasicBean2 = new TransactionAdviceBasicBean();
//
//		transactionAdviceBasicBean2.setTransactionIdentifier("000000000000000");
//
//		transactionAdviceBasicBean2.setFormatCode(FormatCode.No_Industry_Specific_TAA_Record.getFormatCode());
//
//		transactionAdviceBasicBean2.setMediaCode(MediaCode.Cardmember_signature_on_File.getMediaCode());
//
//		transactionAdviceBasicBean2.setSubmissionMethod(SubmissionMethod.Amex_POS_Terminal.getSubmissionMethod());
//
//		transactionAdviceBasicBean2.setApprovalCode("712389");
//
//		transactionAdviceBasicBean2.setPrimaryAccountNumber("3000000000000000");
//
//		transactionAdviceBasicBean2.setCardExpiryDate("1405");
//
//		transactionAdviceBasicBean2.setTransactionDate("20110705");
//
//		transactionAdviceBasicBean2.setTransactionTime("143000");
//
//		transactionAdviceBasicBean2.setTransactionAmount("4300");
//
//		transactionAdviceBasicBean2.setProcessingCode(ProcessingCode.CREDIT.getProcessingCode());
//
//		transactionAdviceBasicBean2.setTransactionCurrencyCode(CurrencyCode.United_Kingdom_Pound_Sterling.getCurrencyCode());
//
//		transactionAdviceBasicBean2.setExtendedPaymentData("01");
//
//		transactionAdviceBasicBean2.setMerchantId("5021045083");
//
//		transactionAdviceBasicBean2.setMerchantLocationId("CAN100000089012");
//
//		transactionAdviceBasicBean2.setMerchantContactInfo(" ");
//
//		transactionAdviceBasicBean2.setTerminalId("Ter12345");
//
//		transactionAdviceBasicBean2.setPosDataCode("161201254110");
//
//		transactionAdviceBasicBean2.setInvoiceReferenceNumber("InvoiceNo123456");
//
//		transactionAdviceBasicBean2.setTabImageSequenceNumber("");
//
//		transactionAdviceBasicBean2.setMatchingKeyType("");
//
//		transactionAdviceBasicBean2.setMatchingKey("");
//
//		transactionAdviceBasicBean2.setElectronicCommerceIndicator("");
//
//		LocationDetailTAABean locationDetailTAABean2 = new LocationDetailTAABean();
//
//		locationDetailTAABean2.setTransactionIdentifier("000000000000000");
//
//		locationDetailTAABean2.setAddendaTypeCode(AddendaTypeCode.Location_Detail_Addendum_Message.getAddendaTypeCode());
//
//		locationDetailTAABean2.setLocationName("XYZStoresToronto");
//
//		locationDetailTAABean2.setLocationAddress("XYZStoresToronto");
//
//		locationDetailTAABean2.setLocationCity("Toronto");
//
//		locationDetailTAABean2.setLocationRegion(RegionCode1.NOVA_SCOTIA.getRegionCode());
//
//		locationDetailTAABean2.setLocationCountryCode(RegionCode1.NOVA_SCOTIA.getCountryCode());
//
//		locationDetailTAABean2.setLocationPostalCode("M3C0C1");
//
//		locationDetailTAABean2.setMerchantCategoryCode("");
//		locationDetailTAABean2.setSellerId("");
//
//		transactionAdviceBasicBean2.setLocationDetailTAABean(locationDetailTAABean2);
//
//		transactionAdviceBasicType.add(transactionAdviceBasicBean2);
//
//		TransactionBatchTrailerBean transactionBatchTrailerBean = new TransactionBatchTrailerBean();
//
//		transactionBatchTrailerBean.setMerchantId("5021045083");
//
//		transactionBatchTrailerBean.setTbtIdentificationNumber("000000000000000");
//
//		transactionBatchTrailerBean.setTbtCreationDate("20110711");
//
//		transactionBatchTrailerBean.setTotalNoOfTabs("2");
//
//		transactionBatchTrailerBean.setTbtAmount("8600");
//
//		transactionBatchTrailerBean.setTbtAmountSign("-");
//
//		transactionBatchTrailerBean.setTbtCurrencyCode(CurrencyCode.United_Kingdom_Pound_Sterling.getCurrencyCode());
//
//		transactionBatchTrailerBean.setTbtImageSequenceNumber("");
//
//		TransactionTBTSpecificBean transactionTBTSpecificBean = new TransactionTBTSpecificBean();
//
//		transactionTBTSpecificBean.setTransactionAdviceBasicType(transactionAdviceBasicType);
//		transactionTBTSpecificBean.setTransactionBatchTrailerBean(transactionBatchTrailerBean);
//
//		List<TransactionTBTSpecificBean> transactionTBTSpecificType = new ArrayList();
//
//		transactionTBTSpecificType.add(transactionTBTSpecificBean);
//
//		TransactionAdviceBasicBean transactionAdviceBasicBean3 = new TransactionAdviceBasicBean();
//
//		transactionAdviceBasicBean3.setTransactionIdentifier("000000000000000");
//
//		transactionAdviceBasicBean3.setFormatCode(FormatCode.No_Industry_Specific_TAA_Record.getFormatCode());
//
//		transactionAdviceBasicBean3.setMediaCode(MediaCode.Cardmember_signature_on_File.getMediaCode());
//
//		transactionAdviceBasicBean3.setSubmissionMethod(SubmissionMethod.Amex_POS_Terminal.getSubmissionMethod());
//
//		transactionAdviceBasicBean3.setApprovalCode("712389");
//
//		transactionAdviceBasicBean3.setPrimaryAccountNumber("300000000000000");
//
//		transactionAdviceBasicBean3.setCardExpiryDate("1405");
//
//		transactionAdviceBasicBean3.setTransactionDate("20110705");
//
//		transactionAdviceBasicBean3.setTransactionTime("143000");
//
//		transactionAdviceBasicBean3.setTransactionAmount("4300");
//
//		transactionAdviceBasicBean3.setProcessingCode(ProcessingCode.CREDIT.getProcessingCode());
//
//		transactionAdviceBasicBean3.setTransactionCurrencyCode(CurrencyCode.United_Kingdom_Pound_Sterling.getCurrencyCode());
//
//		transactionAdviceBasicBean3.setExtendedPaymentData("01");
//
//		transactionAdviceBasicBean3.setMerchantId("5000000000");
//
//		transactionAdviceBasicBean3.setMerchantLocationId("CAN100000089012");
//
//		transactionAdviceBasicBean3.setMerchantContactInfo(" ");
//
//		transactionAdviceBasicBean3.setTerminalId("Ter12345");
//
//		transactionAdviceBasicBean3.setPosDataCode("161201254110");
//
//		transactionAdviceBasicBean3.setInvoiceReferenceNumber("InvoiceNo123456");
//
//		transactionAdviceBasicBean3.setTabImageSequenceNumber("");
//
//		transactionAdviceBasicBean3.setMatchingKeyType("");
//
//		transactionAdviceBasicBean3.setMatchingKey("");
//
//		transactionAdviceBasicBean3.setElectronicCommerceIndicator("");
//
//		LocationDetailTAABean locationDetailTAABean3 = new LocationDetailTAABean();
//
//		locationDetailTAABean3.setTransactionIdentifier("000000000000000");
//
//		locationDetailTAABean3.setAddendaTypeCode(AddendaTypeCode.Location_Detail_Addendum_Message.getAddendaTypeCode());
//
//		locationDetailTAABean3.setLocationName("XYZStoresToronto");
//
//		locationDetailTAABean3.setLocationAddress("XYZStoresToronto");
//
//		locationDetailTAABean3.setLocationCity("Toronto");
//
//		locationDetailTAABean3.setLocationRegion(RegionCode1.NOVA_SCOTIA.getRegionCode());
//
//		locationDetailTAABean3.setLocationCountryCode(RegionCode1.NOVA_SCOTIA.getCountryCode());
//
//		locationDetailTAABean3.setLocationPostalCode("M3C0C1");
//
//		locationDetailTAABean3.setMerchantCategoryCode("");
//		locationDetailTAABean3.setSellerId("");
//
//		transactionAdviceBasicBean3.setLocationDetailTAABean(locationDetailTAABean3);
//
//		transactionAdviceBasicType.add(transactionAdviceBasicBean3);
//
//		TransactionAdviceBasicBean transactionAdviceBasicBean4 = new TransactionAdviceBasicBean();
//
//		transactionAdviceBasicBean4.setTransactionIdentifier("000000000000000");
//
//		transactionAdviceBasicBean4.setFormatCode(FormatCode.No_Industry_Specific_TAA_Record.getFormatCode());
//
//		transactionAdviceBasicBean4.setMediaCode(MediaCode.Cardmember_signature_on_File.getMediaCode());
//
//		transactionAdviceBasicBean4.setSubmissionMethod(SubmissionMethod.Amex_POS_Terminal.getSubmissionMethod());
//
//		transactionAdviceBasicBean4.setApprovalCode("712389");
//
//		transactionAdviceBasicBean4.setPrimaryAccountNumber("3000000000000000");
//
//		transactionAdviceBasicBean4.setCardExpiryDate("1405");
//
//		transactionAdviceBasicBean4.setTransactionDate("20110705");
//
//		transactionAdviceBasicBean4.setTransactionTime("143000");
//
//		transactionAdviceBasicBean4.setTransactionAmount("4300");
//
//		transactionAdviceBasicBean4.setProcessingCode(ProcessingCode.CREDIT.getProcessingCode());
//
//		transactionAdviceBasicBean4.setTransactionCurrencyCode(CurrencyCode.United_Kingdom_Pound_Sterling.getCurrencyCode());
//
//		transactionAdviceBasicBean4.setExtendedPaymentData("01");
//
//		transactionAdviceBasicBean4.setMerchantId("5021045083");
//
//		transactionAdviceBasicBean4.setMerchantLocationId("CAN100000089012");
//
//		transactionAdviceBasicBean4.setMerchantContactInfo(" ");
//
//		transactionAdviceBasicBean4.setTerminalId("Ter12345");
//
//		transactionAdviceBasicBean4.setPosDataCode("161201254110");
//
//		transactionAdviceBasicBean4.setInvoiceReferenceNumber("InvoiceNo123456");
//
//		transactionAdviceBasicBean4.setTabImageSequenceNumber("");
//
//		transactionAdviceBasicBean4.setMatchingKeyType("");
//
//		transactionAdviceBasicBean4.setMatchingKey("");
//
//		transactionAdviceBasicBean4.setElectronicCommerceIndicator("");
//
//		LocationDetailTAABean locationDetailTAABean4 = new LocationDetailTAABean();
//
//		locationDetailTAABean4.setTransactionIdentifier("000000000000000");
//
//		locationDetailTAABean4.setAddendaTypeCode(AddendaTypeCode.Location_Detail_Addendum_Message.getAddendaTypeCode());
//
//		locationDetailTAABean4.setLocationName("XYZStoresToronto");
//
//		locationDetailTAABean4.setLocationAddress("XYZStoresToronto");
//
//		locationDetailTAABean4.setLocationCity("Toronto");
//
//		locationDetailTAABean4.setLocationRegion(RegionCode1.NOVA_SCOTIA.getRegionCode());
//
//		locationDetailTAABean4.setLocationCountryCode(RegionCode1.NOVA_SCOTIA.getCountryCode());
//
//		locationDetailTAABean4.setLocationPostalCode("M3C0C1");
//
//		locationDetailTAABean4.setMerchantCategoryCode("");
//		locationDetailTAABean4.setSellerId("");
//
//		transactionAdviceBasicBean4.setLocationDetailTAABean(locationDetailTAABean4);
//
//		transactionAdviceBasicType.add(transactionAdviceBasicBean4);
//
//		TransactionAdviceBasicBean transactionAdviceBasicBean5 = new TransactionAdviceBasicBean();
//
//		transactionAdviceBasicBean5.setTransactionIdentifier("000000000000000");
//
//		transactionAdviceBasicBean5.setFormatCode(FormatCode.No_Industry_Specific_TAA_Record.getFormatCode());
//
//		transactionAdviceBasicBean5.setMediaCode(MediaCode.Cardmember_signature_on_File.getMediaCode());
//
//		transactionAdviceBasicBean5.setSubmissionMethod(SubmissionMethod.Amex_POS_Terminal.getSubmissionMethod());
//
//		transactionAdviceBasicBean5.setApprovalCode("712389");
//
//		transactionAdviceBasicBean5.setPrimaryAccountNumber("3000000000000000");
//
//		transactionAdviceBasicBean5.setCardExpiryDate("1405");
//
//		transactionAdviceBasicBean5.setTransactionDate("20110705");
//
//		transactionAdviceBasicBean5.setTransactionTime("143000");
//
//		transactionAdviceBasicBean5.setTransactionAmount("4300");
//
//		transactionAdviceBasicBean5.setProcessingCode(ProcessingCode.CREDIT.getProcessingCode());
//
//		transactionAdviceBasicBean5.setTransactionCurrencyCode(CurrencyCode.United_Kingdom_Pound_Sterling.getCurrencyCode());
//
//		transactionAdviceBasicBean5.setExtendedPaymentData("01");
//
//		transactionAdviceBasicBean5.setMerchantId("5021045083");
//
//		transactionAdviceBasicBean5.setMerchantLocationId("CAN100000089012");
//
//		transactionAdviceBasicBean5.setMerchantContactInfo(" ");
//
//		transactionAdviceBasicBean5.setTerminalId("Ter12345");
//
//		transactionAdviceBasicBean5.setPosDataCode("161201254110");
//
//		transactionAdviceBasicBean5.setInvoiceReferenceNumber("InvoiceNo123456");
//
//		transactionAdviceBasicBean5.setTabImageSequenceNumber("");
//
//		transactionAdviceBasicBean5.setMatchingKeyType("");
//
//		transactionAdviceBasicBean5.setMatchingKey("");
//
//		transactionAdviceBasicBean5.setElectronicCommerceIndicator("");
//
//		LocationDetailTAABean locationDetailTAABean5 = new LocationDetailTAABean();
//
//		locationDetailTAABean5.setTransactionIdentifier("000000000000000");
//
//		locationDetailTAABean5.setAddendaTypeCode(AddendaTypeCode.Location_Detail_Addendum_Message.getAddendaTypeCode());
//
//		locationDetailTAABean5.setLocationName("XYZStoresToronto");
//
//		locationDetailTAABean5.setLocationAddress("XYZStoresToronto");
//
//		locationDetailTAABean5.setLocationCity("Toronto");
//
//		locationDetailTAABean5.setLocationRegion(RegionCode1.NOVA_SCOTIA.getRegionCode());
//
//		locationDetailTAABean5.setLocationCountryCode(RegionCode1.NOVA_SCOTIA.getCountryCode());
//
//		locationDetailTAABean5.setLocationPostalCode("M3C0C1");
//
//		locationDetailTAABean5.setMerchantCategoryCode("");
//		locationDetailTAABean5.setSellerId("");
//
//		transactionAdviceBasicBean5.setLocationDetailTAABean(locationDetailTAABean5);
//
//		transactionAdviceBasicType.add(transactionAdviceBasicBean5);
//
//		TransactionBatchTrailerBean transactionBatchTrailerBean1 = new TransactionBatchTrailerBean();
//
//		transactionBatchTrailerBean1.setMerchantId("5021045083");
//
//		transactionBatchTrailerBean1.setTbtIdentificationNumber("000000000000000");
//
//		transactionBatchTrailerBean1.setTbtCreationDate("20110711");
//
//		transactionBatchTrailerBean1.setTotalNoOfTabs("2");
//
//		transactionBatchTrailerBean1.setTbtAmount("8600");
//
//		transactionBatchTrailerBean1.setTbtAmountSign("-");
//
//		transactionBatchTrailerBean1.setTbtCurrencyCode(CurrencyCode.United_Kingdom_Pound_Sterling.getCurrencyCode());
//
//		transactionBatchTrailerBean1.setTbtImageSequenceNumber("");
//
//		transactionTBTSpecificBean.setTransactionAdviceBasicType(transactionAdviceBasicType);
//		transactionTBTSpecificBean.setTransactionBatchTrailerBean(transactionBatchTrailerBean1);
//
//		transactionTBTSpecificType = new ArrayList();
//
//		transactionTBTSpecificType.add(transactionTBTSpecificBean);
//
//		TransactionFileSummaryBean transactionFileSummaryBean = new TransactionFileSummaryBean();
//
//		transactionFileSummaryBean.setNumberOfDebits("1");
//		transactionFileSummaryBean.setHashTotalDebitAmount("2");
//		transactionFileSummaryBean.setHashTotalCreditAmount("1000");
//		transactionFileSummaryBean.setHashTotalAmount("1000");
//		transactionFileSummaryBean.setNumberOfCredits("1");
//
//		SettlementRequestDataObject settlementRequestDataObject = new SettlementRequestDataObject();
//
//		settlementRequestDataObject.setTransactionFileHeaderType(transactionFileHeaderBean);
//
//		settlementRequestDataObject.setTransactionTBTSpecificType(transactionTBTSpecificType);
//
//		settlementRequestDataObject.setTransactionFileSummaryType(transactionFileSummaryBean);
//
//		List<Object> errorCodes = new ArrayList();
//
//		PropertyReader propertyReader = new PropertyReader();
//
//		propertyReader = propertyReader.loadPropertyFile("/settlement.properties");
//
//		SettlementResponseDataObject settlementResponseDataObject = RequestProcessorFactory.processRequest(settlementRequestDataObject, propertyReader);
//		try {
//			if (!settlementResponseDataObject.getActionCodeDescription().isEmpty()) {
//				Iterator<?> iterater = settlementResponseDataObject.getActionCodeDescription().iterator();
//				while (iterater.hasNext()) {
//					ErrorObject errorObject = (ErrorObject) iterater.next();
//					logger.debug(errorObject.getErrorCode() + "-" + errorObject.getErrorDescription() + "\n");
//				}
//			} else {
//				logger.debug("File Successfully Submitted\n");
//			}
//		} catch (Exception e) {
//			logger.debug("Error Object is Empty");
//		}
//	}
//
//	private void file_with_Multiple_Currencies_and_Multiple_Merchant_SE_Number() throws SettlementException {
//		TransactionFileHeaderBean transactionFileHeaderBean = new TransactionFileHeaderBean();
//
//		transactionFileHeaderBean.setSubmitterId("G00002");
//
//		transactionFileHeaderBean.setSubmitterFileReferenceNumber("G00002001");
//
//		transactionFileHeaderBean.setSubmitterFileSequenceNumber("1");
//
//		transactionFileHeaderBean.setFileCreationDate("20110718");
//
//		transactionFileHeaderBean.setFileCreationTime("143000");
//
//		List<TransactionAdviceBasicBean> transactionAdviceBasicType = new ArrayList();
//
//		TransactionAdviceBasicBean transactionAdviceBasicBean = new TransactionAdviceBasicBean();
//
//		transactionAdviceBasicBean.setTransactionIdentifier("000000000000000");
//
//		transactionAdviceBasicBean.setFormatCode(FormatCode.No_Industry_Specific_TAA_Record.getFormatCode());
//
//		transactionAdviceBasicBean.setMediaCode(MediaCode.Cardmember_signature_on_File.getMediaCode());
//
//		transactionAdviceBasicBean.setSubmissionMethod(SubmissionMethod.Amex_POS_Terminal.getSubmissionMethod());
//
//		transactionAdviceBasicBean.setApprovalCode("712389");
//
//		transactionAdviceBasicBean.setPrimaryAccountNumber("300000000000000");
//
//		transactionAdviceBasicBean.setCardExpiryDate("1405");
//
//		transactionAdviceBasicBean.setTransactionDate("20110705");
//
//		transactionAdviceBasicBean.setTransactionTime("143000");
//
//		transactionAdviceBasicBean.setTransactionAmount("4300");
//
//		transactionAdviceBasicBean.setProcessingCode(ProcessingCode.CREDIT.getProcessingCode());
//
//		transactionAdviceBasicBean.setTransactionCurrencyCode(CurrencyCode.United_Kingdom_Pound_Sterling.getCurrencyCode());
//
//		transactionAdviceBasicBean.setExtendedPaymentData("01");
//
//		transactionAdviceBasicBean.setMerchantId("5000000000");
//
//		transactionAdviceBasicBean.setMerchantLocationId("CAN100000089012");
//
//		transactionAdviceBasicBean.setMerchantContactInfo(" ");
//
//		transactionAdviceBasicBean.setTerminalId("Ter12345");
//
//		transactionAdviceBasicBean.setPosDataCode("161201254110");
//
//		transactionAdviceBasicBean.setInvoiceReferenceNumber("InvoiceNo123456");
//
//		transactionAdviceBasicBean.setTabImageSequenceNumber("");
//
//		transactionAdviceBasicBean.setMatchingKeyType("");
//
//		transactionAdviceBasicBean.setMatchingKey("");
//
//		transactionAdviceBasicBean.setElectronicCommerceIndicator("");
//
//		LocationDetailTAABean locationDetailTAABean = new LocationDetailTAABean();
//
//		locationDetailTAABean.setTransactionIdentifier("000000000000000");
//
//		locationDetailTAABean.setAddendaTypeCode(AddendaTypeCode.Location_Detail_Addendum_Message.getAddendaTypeCode());
//
//		locationDetailTAABean.setLocationName("XYZStoresToronto");
//
//		locationDetailTAABean.setLocationAddress("XYZStoresToronto");
//
//		locationDetailTAABean.setLocationCity("Toronto");
//
//		locationDetailTAABean.setLocationRegion(RegionCode1.NOVA_SCOTIA.getRegionCode());
//
//		locationDetailTAABean.setLocationCountryCode(RegionCode1.NOVA_SCOTIA.getCountryCode());
//
//		locationDetailTAABean.setLocationPostalCode("M3C0C1");
//
//		locationDetailTAABean.setMerchantCategoryCode("");
//		locationDetailTAABean.setSellerId("");
//
//		transactionAdviceBasicBean.setLocationDetailTAABean(locationDetailTAABean);
//
//		transactionAdviceBasicType.add(transactionAdviceBasicBean);
//
//		TransactionAdviceBasicBean transactionAdviceBasicBean1 = new TransactionAdviceBasicBean();
//
//		transactionAdviceBasicBean1.setTransactionIdentifier("000000000000000");
//
//		transactionAdviceBasicBean1.setFormatCode(FormatCode.No_Industry_Specific_TAA_Record.getFormatCode());
//
//		transactionAdviceBasicBean1.setMediaCode(MediaCode.Cardmember_signature_on_File.getMediaCode());
//
//		transactionAdviceBasicBean1.setSubmissionMethod(SubmissionMethod.Amex_POS_Terminal.getSubmissionMethod());
//
//		transactionAdviceBasicBean1.setApprovalCode("712389");
//
//		transactionAdviceBasicBean1.setPrimaryAccountNumber("3000000000000000");
//
//		transactionAdviceBasicBean1.setCardExpiryDate("1405");
//
//		transactionAdviceBasicBean1.setTransactionDate("20110705");
//
//		transactionAdviceBasicBean1.setTransactionTime("143000");
//
//		transactionAdviceBasicBean1.setTransactionAmount("4300");
//
//		transactionAdviceBasicBean1.setProcessingCode(ProcessingCode.CREDIT.getProcessingCode());
//
//		transactionAdviceBasicBean1.setTransactionCurrencyCode(CurrencyCode.United_Kingdom_Pound_Sterling.getCurrencyCode());
//
//		transactionAdviceBasicBean1.setExtendedPaymentData("01");
//
//		transactionAdviceBasicBean1.setMerchantId("5021045083");
//
//		transactionAdviceBasicBean1.setMerchantLocationId("CAN100000089012");
//
//		transactionAdviceBasicBean1.setMerchantContactInfo(" ");
//
//		transactionAdviceBasicBean1.setTerminalId("Ter12345");
//
//		transactionAdviceBasicBean1.setPosDataCode("161201254110");
//
//		transactionAdviceBasicBean1.setInvoiceReferenceNumber("InvoiceNo123456");
//
//		transactionAdviceBasicBean1.setTabImageSequenceNumber("");
//
//		transactionAdviceBasicBean1.setMatchingKeyType("");
//
//		transactionAdviceBasicBean1.setMatchingKey("");
//
//		transactionAdviceBasicBean1.setElectronicCommerceIndicator("");
//
//		LocationDetailTAABean locationDetailTAABean1 = new LocationDetailTAABean();
//
//		locationDetailTAABean1.setTransactionIdentifier("000000000000000");
//
//		locationDetailTAABean1.setAddendaTypeCode(AddendaTypeCode.Location_Detail_Addendum_Message.getAddendaTypeCode());
//
//		locationDetailTAABean1.setLocationName("XYZStoresToronto");
//
//		locationDetailTAABean1.setLocationAddress("XYZStoresToronto");
//
//		locationDetailTAABean1.setLocationCity("Toronto");
//
//		locationDetailTAABean1.setLocationRegion(RegionCode1.NOVA_SCOTIA.getRegionCode());
//
//		locationDetailTAABean1.setLocationCountryCode(RegionCode1.NOVA_SCOTIA.getCountryCode());
//
//		locationDetailTAABean1.setLocationPostalCode("M3C0C1");
//
//		locationDetailTAABean1.setMerchantCategoryCode("");
//		locationDetailTAABean1.setSellerId("");
//
//		transactionAdviceBasicBean1.setLocationDetailTAABean(locationDetailTAABean1);
//
//		transactionAdviceBasicType.add(transactionAdviceBasicBean1);
//
//		TransactionAdviceBasicBean transactionAdviceBasicBean2 = new TransactionAdviceBasicBean();
//
//		transactionAdviceBasicBean2.setTransactionIdentifier("000000000000000");
//
//		transactionAdviceBasicBean2.setFormatCode(FormatCode.No_Industry_Specific_TAA_Record.getFormatCode());
//
//		transactionAdviceBasicBean2.setMediaCode(MediaCode.Cardmember_signature_on_File.getMediaCode());
//
//		transactionAdviceBasicBean2.setSubmissionMethod(SubmissionMethod.Amex_POS_Terminal.getSubmissionMethod());
//
//		transactionAdviceBasicBean2.setApprovalCode("712389");
//
//		transactionAdviceBasicBean2.setPrimaryAccountNumber("3000000000000000");
//
//		transactionAdviceBasicBean2.setCardExpiryDate("1405");
//
//		transactionAdviceBasicBean2.setTransactionDate("20110705");
//
//		transactionAdviceBasicBean2.setTransactionTime("143000");
//
//		transactionAdviceBasicBean2.setTransactionAmount("4300");
//
//		transactionAdviceBasicBean2.setProcessingCode(ProcessingCode.CREDIT.getProcessingCode());
//
//		transactionAdviceBasicBean2.setTransactionCurrencyCode(CurrencyCode.United_Kingdom_Pound_Sterling.getCurrencyCode());
//
//		transactionAdviceBasicBean2.setExtendedPaymentData("01");
//
//		transactionAdviceBasicBean2.setMerchantId("5021045083");
//
//		transactionAdviceBasicBean2.setMerchantLocationId("CAN100000089012");
//
//		transactionAdviceBasicBean2.setMerchantContactInfo(" ");
//
//		transactionAdviceBasicBean2.setTerminalId("Ter12345");
//
//		transactionAdviceBasicBean2.setPosDataCode("161201254110");
//
//		transactionAdviceBasicBean2.setInvoiceReferenceNumber("InvoiceNo123456");
//
//		transactionAdviceBasicBean2.setTabImageSequenceNumber("");
//
//		transactionAdviceBasicBean2.setMatchingKeyType("");
//
//		transactionAdviceBasicBean2.setMatchingKey("");
//
//		transactionAdviceBasicBean2.setElectronicCommerceIndicator("");
//
//		LocationDetailTAABean locationDetailTAABean2 = new LocationDetailTAABean();
//
//		locationDetailTAABean2.setTransactionIdentifier("000000000000000");
//
//		locationDetailTAABean2.setAddendaTypeCode(AddendaTypeCode.Location_Detail_Addendum_Message.getAddendaTypeCode());
//
//		locationDetailTAABean2.setLocationName("XYZStoresToronto");
//
//		locationDetailTAABean2.setLocationAddress("XYZStoresToronto");
//
//		locationDetailTAABean2.setLocationCity("Toronto");
//
//		locationDetailTAABean2.setLocationRegion(RegionCode1.NOVA_SCOTIA.getRegionCode());
//
//		locationDetailTAABean2.setLocationCountryCode(RegionCode1.NOVA_SCOTIA.getCountryCode());
//
//		locationDetailTAABean2.setLocationPostalCode("M3C0C1");
//
//		locationDetailTAABean2.setMerchantCategoryCode("");
//		locationDetailTAABean2.setSellerId("");
//
//		transactionAdviceBasicBean2.setLocationDetailTAABean(locationDetailTAABean2);
//
//		transactionAdviceBasicType.add(transactionAdviceBasicBean2);
//
//		TransactionBatchTrailerBean transactionBatchTrailerBean = new TransactionBatchTrailerBean();
//
//		transactionBatchTrailerBean.setMerchantId("5021045083");
//
//		transactionBatchTrailerBean.setTbtIdentificationNumber("000000000000000");
//
//		transactionBatchTrailerBean.setTbtCreationDate("20110711");
//
//		transactionBatchTrailerBean.setTotalNoOfTabs("2");
//
//		transactionBatchTrailerBean.setTbtAmount("8600");
//
//		transactionBatchTrailerBean.setTbtAmountSign("-");
//
//		transactionBatchTrailerBean.setTbtCurrencyCode(CurrencyCode.United_Kingdom_Pound_Sterling.getCurrencyCode());
//
//		transactionBatchTrailerBean.setTbtImageSequenceNumber("");
//
//		TransactionTBTSpecificBean transactionTBTSpecificBean = new TransactionTBTSpecificBean();
//
//		transactionTBTSpecificBean.setTransactionAdviceBasicType(transactionAdviceBasicType);
//		transactionTBTSpecificBean.setTransactionBatchTrailerBean(transactionBatchTrailerBean);
//
//		List<TransactionTBTSpecificBean> transactionTBTSpecificType = new ArrayList();
//
//		transactionTBTSpecificType.add(transactionTBTSpecificBean);
//
//		TransactionAdviceBasicBean transactionAdviceBasicBean3 = new TransactionAdviceBasicBean();
//
//		transactionAdviceBasicBean3.setTransactionIdentifier("000000000000000");
//
//		transactionAdviceBasicBean3.setFormatCode(FormatCode.No_Industry_Specific_TAA_Record.getFormatCode());
//
//		transactionAdviceBasicBean3.setMediaCode(MediaCode.Cardmember_signature_on_File.getMediaCode());
//
//		transactionAdviceBasicBean3.setSubmissionMethod(SubmissionMethod.Amex_POS_Terminal.getSubmissionMethod());
//
//		transactionAdviceBasicBean3.setApprovalCode("712389");
//
//		transactionAdviceBasicBean3.setPrimaryAccountNumber("300000000000000");
//
//		transactionAdviceBasicBean3.setCardExpiryDate("1405");
//
//		transactionAdviceBasicBean3.setTransactionDate("20110705");
//
//		transactionAdviceBasicBean3.setTransactionTime("143000");
//
//		transactionAdviceBasicBean3.setTransactionAmount("4300");
//
//		transactionAdviceBasicBean3.setProcessingCode(ProcessingCode.CREDIT.getProcessingCode());
//
//		transactionAdviceBasicBean3.setTransactionCurrencyCode(CurrencyCode.United_Kingdom_Pound_Sterling.getCurrencyCode());
//
//		transactionAdviceBasicBean3.setExtendedPaymentData("01");
//
//		transactionAdviceBasicBean3.setMerchantId("5000000000");
//
//		transactionAdviceBasicBean3.setMerchantLocationId("CAN100000089012");
//
//		transactionAdviceBasicBean3.setMerchantContactInfo(" ");
//
//		transactionAdviceBasicBean3.setTerminalId("Ter12345");
//
//		transactionAdviceBasicBean3.setPosDataCode("161201254110");
//
//		transactionAdviceBasicBean3.setInvoiceReferenceNumber("InvoiceNo123456");
//
//		transactionAdviceBasicBean3.setTabImageSequenceNumber("");
//
//		transactionAdviceBasicBean3.setMatchingKeyType("");
//
//		transactionAdviceBasicBean3.setMatchingKey("");
//
//		transactionAdviceBasicBean3.setElectronicCommerceIndicator("");
//
//		LocationDetailTAABean locationDetailTAABean3 = new LocationDetailTAABean();
//
//		locationDetailTAABean3.setTransactionIdentifier("000000000000000");
//
//		locationDetailTAABean3.setAddendaTypeCode(AddendaTypeCode.Location_Detail_Addendum_Message.getAddendaTypeCode());
//
//		locationDetailTAABean3.setLocationName("XYZStoresToronto");
//
//		locationDetailTAABean3.setLocationAddress("XYZStoresToronto");
//
//		locationDetailTAABean3.setLocationCity("Toronto");
//
//		locationDetailTAABean3.setLocationRegion(RegionCode1.NOVA_SCOTIA.getRegionCode());
//
//		locationDetailTAABean3.setLocationCountryCode(RegionCode1.NOVA_SCOTIA.getCountryCode());
//
//		locationDetailTAABean3.setLocationPostalCode("M3C0C1");
//
//		locationDetailTAABean3.setMerchantCategoryCode("");
//		locationDetailTAABean3.setSellerId("");
//
//		transactionAdviceBasicBean3.setLocationDetailTAABean(locationDetailTAABean3);
//
//		transactionAdviceBasicType.add(transactionAdviceBasicBean3);
//
//		TransactionAdviceBasicBean transactionAdviceBasicBean4 = new TransactionAdviceBasicBean();
//
//		transactionAdviceBasicBean4.setTransactionIdentifier("000000000000000");
//
//		transactionAdviceBasicBean4.setFormatCode(FormatCode.No_Industry_Specific_TAA_Record.getFormatCode());
//
//		transactionAdviceBasicBean4.setMediaCode(MediaCode.Cardmember_signature_on_File.getMediaCode());
//
//		transactionAdviceBasicBean4.setSubmissionMethod(SubmissionMethod.Amex_POS_Terminal.getSubmissionMethod());
//
//		transactionAdviceBasicBean4.setApprovalCode("712389");
//
//		transactionAdviceBasicBean4.setPrimaryAccountNumber("3000000000000000");
//
//		transactionAdviceBasicBean4.setCardExpiryDate("1405");
//
//		transactionAdviceBasicBean4.setTransactionDate("20110705");
//
//		transactionAdviceBasicBean4.setTransactionTime("143000");
//
//		transactionAdviceBasicBean4.setTransactionAmount("4300");
//
//		transactionAdviceBasicBean4.setProcessingCode(ProcessingCode.CREDIT.getProcessingCode());
//
//		transactionAdviceBasicBean4.setTransactionCurrencyCode(CurrencyCode.United_Kingdom_Pound_Sterling.getCurrencyCode());
//
//		transactionAdviceBasicBean4.setExtendedPaymentData("01");
//
//		transactionAdviceBasicBean4.setMerchantId("5021045083");
//
//		transactionAdviceBasicBean4.setMerchantLocationId("CAN100000089012");
//
//		transactionAdviceBasicBean4.setMerchantContactInfo(" ");
//
//		transactionAdviceBasicBean4.setTerminalId("Ter12345");
//
//		transactionAdviceBasicBean4.setPosDataCode("161201254110");
//
//		transactionAdviceBasicBean4.setInvoiceReferenceNumber("InvoiceNo123456");
//
//		transactionAdviceBasicBean4.setTabImageSequenceNumber("");
//
//		transactionAdviceBasicBean4.setMatchingKeyType("");
//
//		transactionAdviceBasicBean4.setMatchingKey("");
//
//		transactionAdviceBasicBean4.setElectronicCommerceIndicator("");
//
//		LocationDetailTAABean locationDetailTAABean4 = new LocationDetailTAABean();
//
//		locationDetailTAABean4.setTransactionIdentifier("000000000000000");
//
//		locationDetailTAABean4.setAddendaTypeCode(AddendaTypeCode.Location_Detail_Addendum_Message.getAddendaTypeCode());
//
//		locationDetailTAABean4.setLocationName("XYZStoresToronto");
//
//		locationDetailTAABean4.setLocationAddress("XYZStoresToronto");
//
//		locationDetailTAABean4.setLocationCity("Toronto");
//
//		locationDetailTAABean4.setLocationRegion(RegionCode1.NOVA_SCOTIA.getRegionCode());
//
//		locationDetailTAABean4.setLocationCountryCode(RegionCode1.NOVA_SCOTIA.getCountryCode());
//
//		locationDetailTAABean4.setLocationPostalCode("M3C0C1");
//
//		locationDetailTAABean4.setMerchantCategoryCode("");
//		locationDetailTAABean4.setSellerId("");
//
//		transactionAdviceBasicBean4.setLocationDetailTAABean(locationDetailTAABean4);
//
//		transactionAdviceBasicType.add(transactionAdviceBasicBean4);
//
//		TransactionAdviceBasicBean transactionAdviceBasicBean5 = new TransactionAdviceBasicBean();
//
//		transactionAdviceBasicBean5.setTransactionIdentifier("000000000000000");
//
//		transactionAdviceBasicBean5.setFormatCode(FormatCode.No_Industry_Specific_TAA_Record.getFormatCode());
//
//		transactionAdviceBasicBean5.setMediaCode(MediaCode.Cardmember_signature_on_File.getMediaCode());
//
//		transactionAdviceBasicBean5.setSubmissionMethod(SubmissionMethod.Amex_POS_Terminal.getSubmissionMethod());
//
//		transactionAdviceBasicBean5.setApprovalCode("712389");
//
//		transactionAdviceBasicBean5.setPrimaryAccountNumber("3000000000000000");
//
//		transactionAdviceBasicBean5.setCardExpiryDate("1405");
//
//		transactionAdviceBasicBean5.setTransactionDate("20110705");
//
//		transactionAdviceBasicBean5.setTransactionTime("143000");
//
//		transactionAdviceBasicBean5.setTransactionAmount("4300");
//
//		transactionAdviceBasicBean5.setProcessingCode(ProcessingCode.CREDIT.getProcessingCode());
//
//		transactionAdviceBasicBean5.setTransactionCurrencyCode(CurrencyCode.United_Kingdom_Pound_Sterling.getCurrencyCode());
//
//		transactionAdviceBasicBean5.setExtendedPaymentData("01");
//
//		transactionAdviceBasicBean5.setMerchantId("5021045083");
//
//		transactionAdviceBasicBean5.setMerchantLocationId("CAN100000089012");
//
//		transactionAdviceBasicBean5.setMerchantContactInfo(" ");
//
//		transactionAdviceBasicBean5.setTerminalId("Ter12345");
//
//		transactionAdviceBasicBean5.setPosDataCode("161201254110");
//
//		transactionAdviceBasicBean5.setInvoiceReferenceNumber("InvoiceNo123456");
//
//		transactionAdviceBasicBean5.setTabImageSequenceNumber("");
//
//		transactionAdviceBasicBean5.setMatchingKeyType("");
//
//		transactionAdviceBasicBean5.setMatchingKey("");
//
//		transactionAdviceBasicBean5.setElectronicCommerceIndicator("");
//
//		LocationDetailTAABean locationDetailTAABean5 = new LocationDetailTAABean();
//
//		locationDetailTAABean5.setTransactionIdentifier("000000000000000");
//
//		locationDetailTAABean5.setAddendaTypeCode(AddendaTypeCode.Location_Detail_Addendum_Message.getAddendaTypeCode());
//
//		locationDetailTAABean5.setLocationName("XYZStoresToronto");
//
//		locationDetailTAABean5.setLocationAddress("XYZStoresToronto");
//
//		locationDetailTAABean5.setLocationCity("Toronto");
//
//		locationDetailTAABean5.setLocationRegion(RegionCode1.NOVA_SCOTIA.getRegionCode());
//
//		locationDetailTAABean5.setLocationCountryCode(RegionCode1.NOVA_SCOTIA.getCountryCode());
//
//		locationDetailTAABean5.setLocationPostalCode("M3C0C1");
//
//		locationDetailTAABean5.setMerchantCategoryCode("");
//		locationDetailTAABean5.setSellerId("");
//
//		transactionAdviceBasicBean5.setLocationDetailTAABean(locationDetailTAABean5);
//
//		transactionAdviceBasicType.add(transactionAdviceBasicBean5);
//
//		TransactionBatchTrailerBean transactionBatchTrailerBean1 = new TransactionBatchTrailerBean();
//
//		transactionBatchTrailerBean1.setMerchantId("5021045083");
//
//		transactionBatchTrailerBean1.setTbtIdentificationNumber("000000000000000");
//
//		transactionBatchTrailerBean1.setTbtCreationDate("20110711");
//
//		transactionBatchTrailerBean1.setTotalNoOfTabs("2");
//
//		transactionBatchTrailerBean1.setTbtAmount("8600");
//
//		transactionBatchTrailerBean1.setTbtAmountSign("-");
//
//		transactionBatchTrailerBean1.setTbtCurrencyCode(CurrencyCode.United_Kingdom_Pound_Sterling.getCurrencyCode());
//
//		transactionBatchTrailerBean1.setTbtImageSequenceNumber("");
//
//		transactionTBTSpecificBean.setTransactionAdviceBasicType(transactionAdviceBasicType);
//		transactionTBTSpecificBean.setTransactionBatchTrailerBean(transactionBatchTrailerBean1);
//
//		transactionTBTSpecificType = new ArrayList();
//
//		transactionTBTSpecificType.add(transactionTBTSpecificBean);
//
//		TransactionFileSummaryBean transactionFileSummaryBean = new TransactionFileSummaryBean();
//
//		transactionFileSummaryBean.setNumberOfDebits("1");
//		transactionFileSummaryBean.setHashTotalDebitAmount("2");
//		transactionFileSummaryBean.setHashTotalCreditAmount("1000");
//		transactionFileSummaryBean.setHashTotalAmount("1000");
//		transactionFileSummaryBean.setNumberOfCredits("1");
//
//		SettlementRequestDataObject settlementRequestDataObject = new SettlementRequestDataObject();
//
//		settlementRequestDataObject.setTransactionFileHeaderType(transactionFileHeaderBean);
//
//		settlementRequestDataObject.setTransactionTBTSpecificType(transactionTBTSpecificType);
//
//		settlementRequestDataObject.setTransactionFileSummaryType(transactionFileSummaryBean);
//
//		List<Object> errorCodes = new ArrayList();
//
//		PropertyReader propertyReader = new PropertyReader();
//
//		propertyReader = propertyReader.loadPropertyFile("/settlement.properties");
//
//		SettlementResponseDataObject settlementResponseDataObject = RequestProcessorFactory.processRequest(settlementRequestDataObject, propertyReader);
//		try {
//			if (!settlementResponseDataObject.getActionCodeDescription().isEmpty()) {
//				Iterator<?> iterater = settlementResponseDataObject.getActionCodeDescription().iterator();
//				while (iterater.hasNext()) {
//					ErrorObject errorObject = (ErrorObject) iterater.next();
//					logger.debug(errorObject.getErrorCode() + "-" + errorObject.getErrorDescription() + "\n");
//				}
//			} else {
//				logger.debug("File Successfully Submitted\n");
//			}
//		} catch (Exception e) {
//			logger.debug("Error Object is Empty");
//		}
//	}
}
