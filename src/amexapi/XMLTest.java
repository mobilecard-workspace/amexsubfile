/*     */ import com.americanexpress.ips.gfsg.beans.ErrorObject;
/*     */ import com.americanexpress.ips.gfsg.beans.xml.batchadmin.BatchAdminRequestBean;
/*     */ import com.americanexpress.ips.gfsg.beans.xml.batchadmin.BatchAdminResponseBean;
/*     */ import com.americanexpress.ips.gfsg.beans.xml.batchadmin.CardAcceptorDetail;
/*     */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.DataCaptureRequestBean;
/*     */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.DataCaptureResponseBean;
/*     */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.PointOfServiceDataBean;
/*     */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.RailSegInfoBean;
/*     */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.TAARailIndustryBean;
/*     */ import com.americanexpress.ips.gfsg.connection.PropertyReader;
/*     */ import com.americanexpress.ips.gfsg.processor.xml.XmlRequestProcessor;
/*     */ import java.io.PrintStream;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Iterator;
/*     */ import java.util.List;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class XMLTest
/*     */ {
/*     */   public static void main(String[] args)
/*     */     throws Exception
/*     */   {}
/*     */   
/*     */   public static void createBatchAdminRequest()
/*     */   {
/*  44 */     BatchAdminRequestBean batchAdmin = new BatchAdminRequestBean();
/*  45 */     BatchAdminResponseBean batchAdminReponseBean = null;
/*  46 */     List<?> errorList = null;
/*  47 */     CardAcceptorDetail cardAcceptorDetail = new CardAcceptorDetail();
/*     */     
/*  49 */     batchAdmin.setBatchId("102035");
/*  50 */     batchAdmin.setBatchOperation("01");
/*  51 */     batchAdmin.setMerId("2420061582");
/*  52 */     batchAdmin.setMerTrmnlId("88885561");
/*  53 */     batchAdmin.setReturnBatchSummary("00");
/*  54 */     batchAdmin.setSubmitterCode("GWSTST");
/*     */     
/*  56 */     batchAdmin.setVersion("12010000");
/*     */     
/*  58 */     cardAcceptorDetail.setCardAcptCityNm("STRING");
/*  59 */     cardAcceptorDetail.setCardAcptCtryCd("840");
/*  60 */     cardAcceptorDetail.setCardAcptNm("Phoenix");
/*  61 */     cardAcceptorDetail.setCardAcptPostCd("85013");
/*  62 */     cardAcceptorDetail.setCardAcptRegCd("AZ");
/*  63 */     cardAcceptorDetail.setCardAcptStreetNm("STRING");
/*  64 */     batchAdmin.setCardAcceptorDetail(cardAcceptorDetail);
/*     */     
/*     */ 
/*  67 */     XmlRequestProcessor xmlRequestProcessor = new XmlRequestProcessor();
/*     */     
/*  69 */     PropertyReader propertyReader = new PropertyReader();
/*     */     
/*     */ 
/*  72 */     propertyReader = propertyReader.loadPropertyFile("/settlement.properties");
/*     */     
/*  74 */     if (batchAdmin.getBatchOperation().equalsIgnoreCase("01"))
/*     */     {
/*  76 */       batchAdminReponseBean = xmlRequestProcessor.createBatchOpenRequest(batchAdmin, propertyReader);
/*     */ 
/*     */ 
/*     */     }
/*  80 */     else if (batchAdmin.getBatchOperation().equalsIgnoreCase("02"))
/*     */     {
/*     */ 
/*  83 */       batchAdminReponseBean = xmlRequestProcessor.createBatchCloseRequest(batchAdmin, propertyReader);
/*  84 */     } else if (batchAdmin.getBatchOperation().equalsIgnoreCase("03"))
/*     */     {
/*  86 */       batchAdminReponseBean = xmlRequestProcessor.createBatchPurgeRequest(batchAdmin, propertyReader);
/*  87 */     } else if (batchAdmin.getBatchOperation().equalsIgnoreCase("04"))
/*     */     {
/*  89 */       batchAdminReponseBean = xmlRequestProcessor.createBatchStatusRequest(batchAdmin, propertyReader);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*  95 */     if (batchAdminReponseBean != null)
/*     */     {
/*  97 */       errorList = batchAdminReponseBean.getActionCodeDescription();
/*     */       
/*  99 */       if ((errorList != null) && (!errorList.isEmpty())) {
/* 100 */         Iterator<?> iterater = errorList.iterator();
/*     */         
/* 102 */         while (iterater.hasNext())
/*     */         {
/* 104 */           ErrorObject localErrorObject = (ErrorObject)iterater.next();
/*     */         }
/*     */       }
/*     */       else
/*     */       {
/* 109 */         System.out.println("Request XML" + 
/* 110 */           batchAdminReponseBean.getRequestxml());
/* 111 */         System.out.println("Response XML" + 
/* 112 */           batchAdminReponseBean.getResponsexml());
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void createDataCaptureRequest()
/*     */   {
/* 123 */     DataCaptureRequestBean dataBean = new DataCaptureRequestBean();
/* 124 */     PointOfServiceDataBean posbean = new PointOfServiceDataBean();
/* 125 */     TAARailIndustryBean railIndustryBean = new TAARailIndustryBean();
/* 126 */     RailSegInfoBean railSegInfoBean = new RailSegInfoBean();
/*     */     
/* 128 */     List<?> errorList = null;
/*     */     
/* 130 */     dataBean.setVersion("12010000");
/* 131 */     dataBean.setBatchId("102035");
/* 132 */     dataBean.setRefNumber("040RAIL");
/* 133 */     dataBean.setTransId("000041642724011");
/* 134 */     dataBean.setTransAddCd("06");
/* 135 */     dataBean.setMediaCd("01");
/* 136 */     dataBean.setSubmMthdCd("03");
/* 137 */     dataBean.setTransAprvCd("979670");
/* 138 */     dataBean.setCardNbr("378597935014687");
/* 139 */     dataBean.setCardExprDt("1208");
/* 140 */     dataBean.setTransDt("20110120");
/* 141 */     dataBean.setTransTm("164514");
/* 142 */     dataBean.setTransAmt("100");
/* 143 */     dataBean.setTransProcCd("000000");
/* 144 */     dataBean.setTransCurrCd("840");
/* 145 */     dataBean.setMerId("2420061582");
/* 146 */     dataBean.setMerLocId("999123000000001");
/* 147 */     dataBean.setMerCtcInfoTxt("CONTECHNOLOG");
/* 148 */     dataBean.setMerTrmnlId("88885561");
/* 149 */     posbean.setCardCptrCpblCd("2");
/* 150 */     posbean.setCardDataInpCpblCd("6");
/* 151 */     posbean.setCardDataInpModeCd("1");
/* 152 */     posbean.setCardDataOpCpblCd("1");
/* 153 */     posbean.setCardPresentCd("0");
/* 154 */     posbean.setCmAuthnCpblCd("1");
/* 155 */     posbean.setCmAuthnEnttyCd("2");
/* 156 */     posbean.setCmAuthnMthdCd("0");
/* 157 */     posbean.setCmPresentCd("0");
/* 158 */     posbean.setOprEnvirCd("1");
/* 159 */     posbean.setPinCptrCpblCd("2");
/* 160 */     posbean.setTrmnlOpCpblCd("0");
/*     */     
/*     */ 
/* 163 */     dataBean.setPointOfServiceData(posbean);
/* 164 */     dataBean.setBatchImageSeqNbr("87654321");
/* 165 */     dataBean.setMatchKeyTypeCd("99");
/* 166 */     dataBean.setMatchKeyId("ABC123456789");
/* 167 */     dataBean.setElecComrceInd("06");
/* 168 */     dataBean.setMerCtgyCd("2563");
/* 169 */     dataBean.setSellId("SELLERID4567890");
/*     */     
/*     */ 
/*     */ 
/* 173 */     railIndustryBean.setTravTransTypeCd("TKT");
/* 174 */     railIndustryBean.setTktNbr("TKTRAIL1234567");
/* 175 */     railIndustryBean.setPassNm("LAURA JIMENEZ");
/* 176 */     railIndustryBean.setIATACarrierCd("2V");
/* 177 */     railIndustryBean.setTktIssNm("AMTRAK");
/* 178 */     railIndustryBean.setTktIssCityNm("MILL VALLEY");
/*     */     
/* 180 */     railSegInfoBean.setArrStnCd("LAX");
/* 181 */     railSegInfoBean.setDprtDt("20101125");
/* 182 */     railSegInfoBean.setDprtStnCd("LAX");
/*     */     
/* 184 */     List<RailSegInfoBean> railSegInfo = new ArrayList();
/* 185 */     railIndustryBean.setRailSegInfoBean(railSegInfo);
/* 186 */     dataBean.setTaaRailIndustry(railIndustryBean);
/* 187 */     dataBean.setServAgtMerId("1234567890");
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 205 */     XmlRequestProcessor xmlRequestProcessor = new XmlRequestProcessor();
/* 206 */     List<DataCaptureRequestBean> dataList = new ArrayList();
/* 207 */     dataList.add(dataBean);
/* 208 */     PropertyReader propertyReader = new PropertyReader();
/*     */     
/*     */ 
/* 211 */     propertyReader = propertyReader.loadPropertyFile("/settlement.properties");
/*     */     
/* 213 */     DataCaptureResponseBean dataCaptureResponseBean = xmlRequestProcessor.processXMLDataCaptureTransaction(dataBean, propertyReader);
/*     */     
/*     */ 
/* 216 */     if (dataCaptureResponseBean != null) {
/* 217 */       System.out.println("errorresponseMessage !=null" + 
/* 218 */         dataCaptureResponseBean != null);
/* 219 */       errorList = dataCaptureResponseBean.getActionCodeDescription();
/*     */       
/* 221 */       if ((errorList != null) && (!errorList.isEmpty())) {
/* 222 */         Iterator<?> iterater = errorList.iterator();
/*     */         
/* 224 */         while (iterater.hasNext()) {
/* 225 */           ErrorObject errorObject = (ErrorObject)iterater.next();
/* 226 */           System.out.println(errorObject.getErrorCode() + "-" + 
/* 227 */             errorObject.getErrorDescription());
/*     */         }
/*     */       }
/*     */       else {
/* 231 */         System.out.println("Request XML" + 
/* 232 */           dataCaptureResponseBean.getRequestXML());
/* 233 */         System.out.println("Response XML" + 
/* 234 */           dataCaptureResponseBean.getResponseXML());
/*     */       }
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\XMLTest.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */