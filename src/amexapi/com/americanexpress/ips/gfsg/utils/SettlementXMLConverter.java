package com.americanexpress.ips.gfsg.utils;

import java.io.CharArrayWriter;
import java.io.StringReader;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.apache.log4j.Logger;

import com.americanexpress.ips.gfsg.beans.xml.batchadmin.BatchAdminResponseBean;
import com.americanexpress.ips.gfsg.beans.xml.batchadmin.BatchSummaryResponseBean;
import com.americanexpress.ips.gfsg.beans.xml.batchadmin.response.BatchAdminResponse;
import com.americanexpress.ips.gfsg.beans.xml.datacapture.CardTransactionDetailBean;
import com.americanexpress.ips.gfsg.beans.xml.datacapture.DataCaptureRequestBean;
import com.americanexpress.ips.gfsg.beans.xml.datacapture.DataCaptureResponseBean;
import com.americanexpress.ips.gfsg.beans.xml.datacapture.request.AddAmtInfoType;
import com.americanexpress.ips.gfsg.beans.xml.datacapture.request.CardTransDetailType;
import com.americanexpress.ips.gfsg.beans.xml.datacapture.request.DataCaptureRequest;
import com.americanexpress.ips.gfsg.beans.xml.datacapture.request.ObjectFactory;
import com.americanexpress.ips.gfsg.beans.xml.datacapture.request.PointOfServiceDataType;
import com.americanexpress.ips.gfsg.beans.xml.datacapture.response.DataCaptureResponse;
import com.americanexpress.ips.gfsg.connection.xml.XMLRequestConnection;
import com.americanexpress.ips.gfsg.exceptions.SettlementException;
import com.americanexpress.ips.gfsg.validator.CommonValidator;

public class SettlementXMLConverter {
	private static final Logger LOGGER = Logger.getLogger(SettlementXMLConverter.class);

	public static String convertDataCaptureReqBeanToJAXBSettlementType(DataCaptureRequestBean dataCaptureRequestBean, DataCaptureRequest dataRequest) throws SettlementException {
		dataRequest.setVersion(dataCaptureRequestBean.getVersion());
		dataRequest.setMerId(Long.parseLong(dataCaptureRequestBean.getMerId()));
		dataRequest.setMerTrmnlId(dataCaptureRequestBean.getMerTrmnlId());
		dataRequest.setBatchID(new BigInteger(dataCaptureRequestBean.getBatchId()));
		dataRequest.setRefNumber(dataCaptureRequestBean.getRefNumber());
		dataRequest.setCardNbr(dataCaptureRequestBean.getCardNbr());
		dataRequest.setTransProcCd(dataCaptureRequestBean.getTransProcCd());

		if (!CommonValidator.isNullOrEmpty(dataCaptureRequestBean.getCardExprDt())) {
			dataRequest.setCardExprDt(dataCaptureRequestBean.getCardExprDt());
		}
		if (!CommonValidator.isNullOrEmpty(dataCaptureRequestBean.getTransDt())) {
			dataRequest.setTransDt(dataCaptureRequestBean.getTransDt());
		}
		if (!CommonValidator.isNullOrEmpty(dataCaptureRequestBean.getTransTm())) {
			dataRequest.setTransTm(dataCaptureRequestBean.getTransTm());
		}
		if (!CommonValidator.isNullOrEmpty(dataCaptureRequestBean.getTransAmt())) {
			dataRequest.setTransAmt(new BigInteger(dataCaptureRequestBean.getTransAmt()));
		}
		if (!CommonValidator.isNullOrEmpty(dataCaptureRequestBean.getTransCurrCd())) {
			dataRequest.setTransCurrCd(dataCaptureRequestBean.getTransCurrCd());
		}
		if (!CommonValidator.isNullOrEmpty(dataCaptureRequestBean.getTransId())) {
			dataRequest.setTransId(dataCaptureRequestBean.getTransId());
		}
		if (!CommonValidator.isNullOrEmpty(dataCaptureRequestBean.getTransAprvCd())) {
			dataRequest.setTransAprvCd(dataCaptureRequestBean.getTransAprvCd());
		}

		if (!CommonValidator.isNullOrEmpty(dataCaptureRequestBean.getTransImageSeqNbr())) {
			dataRequest.setTransImageSeqNbr(dataCaptureRequestBean.getTransImageSeqNbr());
		}

		PointOfServiceDataType posdata = new ObjectFactory().createPointOfServiceDataType();
		posdata.setCardCptrCpblCd(dataCaptureRequestBean.getPointOfServiceData().getCardCptrCpblCd());
		posdata.setCardDataInpCpblCd(dataCaptureRequestBean.getPointOfServiceData().getCardDataInpCpblCd());
		posdata.setCMAuthnCpblCd(dataCaptureRequestBean.getPointOfServiceData().getCmAuthnCpblCd());
		posdata.setOprEnvirCd(dataCaptureRequestBean.getPointOfServiceData().getOprEnvirCd());
		posdata.setCardPresentCd(dataCaptureRequestBean.getPointOfServiceData().getCardPresentCd());
		posdata.setCardDataInpModeCd(dataCaptureRequestBean.getPointOfServiceData().getCardDataInpModeCd());
		posdata.setCMAuthnMthdCd(dataCaptureRequestBean.getPointOfServiceData().getCmAuthnMthdCd());
		posdata.setCMAuthnEnttyCd(dataCaptureRequestBean.getPointOfServiceData().getCmAuthnEnttyCd());
		posdata.setCardDataOpCpblCd(dataCaptureRequestBean.getPointOfServiceData().getCardDataOpCpblCd());
		posdata.setTrmnlOpCpblCd(dataCaptureRequestBean.getPointOfServiceData().getTrmnlOpCpblCd());
		posdata.setPINCptrCpblCd(dataCaptureRequestBean.getPointOfServiceData().getPinCptrCpblCd());
		posdata.setCMPresentCd(dataCaptureRequestBean.getPointOfServiceData().getCmPresentCd());
		dataRequest.setPointOfServiceData(posdata);

		if (!CommonValidator.isNullOrEmpty(dataCaptureRequestBean.getElecComrceInd())) {
			dataRequest.setElecComrceInd(dataCaptureRequestBean.getElecComrceInd());
		}

		if (!CommonValidator.isNullOrEmpty(dataCaptureRequestBean.getSubmMthdCd())) {
			dataRequest.setSubmMthdCd(dataCaptureRequestBean.getSubmMthdCd());
		}
		if (!CommonValidator.isNullOrEmpty(dataCaptureRequestBean.getMerLocId())) {
			dataRequest.setMerLocId(dataCaptureRequestBean.getMerLocId());
		}
		if (!CommonValidator.isNullOrEmpty(dataCaptureRequestBean.getMerCtcInfoTxt())) {
			dataRequest.setMerCtcInfoTxt(dataCaptureRequestBean.getMerCtcInfoTxt());
		}
		if (!CommonValidator.isNullOrEmpty(dataCaptureRequestBean.getTransAddCd())) {
			dataRequest.setTransAddCd(dataCaptureRequestBean.getTransAddCd());
		}
		if (!CommonValidator.isNullOrEmpty(dataCaptureRequestBean.getMediaCd())) {
			dataRequest.setMediaCd(dataCaptureRequestBean.getMediaCd());
		}

		if (!CommonValidator.isNullOrEmpty(dataCaptureRequestBean.getSubmMthdCd())) {
			dataRequest.setSubmMthdCd(dataCaptureRequestBean.getSubmMthdCd());
		}

		if (!CommonValidator.isNullOrEmpty(dataCaptureRequestBean.getMerLocId())) {
			dataRequest.setMerLocId(dataCaptureRequestBean.getMerLocId());
		}

		if (!CommonValidator.isNullOrEmpty(dataCaptureRequestBean.getMerCtcInfoTxt())) {
			dataRequest.setMerCtcInfoTxt(dataCaptureRequestBean.getMerCtcInfoTxt());
		}
		if (!CommonValidator.isNullOrEmpty(dataCaptureRequestBean.getMatchKeyTypeCd())) {
			dataRequest.setMatchKeyTypeCd(dataCaptureRequestBean.getMatchKeyTypeCd());
		}
		if (!CommonValidator.isNullOrEmpty(dataCaptureRequestBean.getMatchKeyId())) {
			dataRequest.setMatchKeyId(dataCaptureRequestBean.getMatchKeyId());
		}

		if (!CommonValidator.isNullOrEmpty(dataCaptureRequestBean.getDefPaymentPlan())) {
			dataRequest.setDefPaymentPlan(dataCaptureRequestBean.getDefPaymentPlan());
		}
		if (!CommonValidator.isNullOrEmpty(dataCaptureRequestBean.getNumDefPayments())) {
			dataRequest.setNumDefPayments(dataCaptureRequestBean.getNumDefPayments());
		}
		if (!CommonValidator.isNullOrEmpty(dataCaptureRequestBean.getEmvData())) {
			dataRequest.setEMVData(dataCaptureRequestBean.getEmvData());
		}
		if (!CommonValidator.isNullOrEmpty(dataCaptureRequestBean.getMerCtgyCd())) {
			dataRequest.setMerCtgyCd(dataCaptureRequestBean.getMerCtgyCd());
		}
		if (!CommonValidator.isNullOrEmpty(dataCaptureRequestBean.getSellId())) {
			dataRequest.setSellId(dataCaptureRequestBean.getSellId());
		}

		if (!CommonValidator.isNullOrEmpty(dataCaptureRequestBean.getTransAddCd())) {
			dataRequest.setTransAddCd(dataCaptureRequestBean.getTransAddCd());
		}

		CardTransDetailType cardTransDetailType = new ObjectFactory().createCardTransDetailType();
		if ((dataCaptureRequestBean.getCardTransDetail() != null) && (!dataCaptureRequestBean.getCardTransDetail().isEmpty())) {
			for (CardTransactionDetailBean cardTransactionDetail : dataCaptureRequestBean.getCardTransDetail()) {
				AddAmtInfoType addAmtInfoType = new ObjectFactory().createAddAmtInfoType();
				if (!CommonValidator.isNullOrEmpty(cardTransactionDetail.getAddAmtTypeCd())) {
					addAmtInfoType.setAddAmtTypeCd(cardTransactionDetail.getAddAmtTypeCd());
				}
				if (!CommonValidator.isNullOrEmpty(cardTransactionDetail.getAddAmt())) {
					addAmtInfoType.setAddAmt(cardTransactionDetail.getAddAmt());
				}
				if (!CommonValidator.isNullOrEmpty(cardTransactionDetail.getSignInd())) {
					addAmtInfoType.setSignInd(cardTransactionDetail.getSignInd());
				}
				cardTransDetailType.getAddAmtInfo().add(addAmtInfoType);
				dataRequest.setCardTransDetail(cardTransDetailType);
			}
		}

		String xmlMessage = createXMLMessage(dataRequest, "com.americanexpress.ips.gfsg.beans.xml.datacapture.request");

		return xmlMessage;
	}

	public static BatchAdminResponseBean convertResponseXMLToBean(String XMLString) throws SettlementException {
		BatchAdminResponseBean batchAdminResponse = new BatchAdminResponseBean();

		Map<String, List<String>> resHeaderParam = XMLRequestConnection.getResponseHeader();

		BatchAdminResponse batchAdminRespon = (BatchAdminResponse) parseXMLMessage(XMLString, "com.americanexpress.ips.gfsg.beans.xml.batchadmin.response");

		batchAdminResponse.setBatchId(Integer.valueOf(batchAdminRespon.getBatchID()).toString());
		batchAdminResponse.setBatchStatus(batchAdminRespon.getBatchStatus());

		if ((resHeaderParam != null) && (resHeaderParam.get("gfsg-status") != null)) {
			batchAdminResponse.setBatchStatusText(batchAdminRespon.getBatchStatusText() + " | " + (String) ((List) resHeaderParam.get("gfsg-status")).get(0));
		} else {
			batchAdminResponse.setBatchStatusText(batchAdminRespon.getBatchStatusText());
		}
		batchAdminResponse.setMerId(Long.valueOf(batchAdminRespon.getMerId()).toString());
		batchAdminResponse.setMerTrmnlId(batchAdminRespon.getMerTrmnlId());
		batchAdminResponse.setVersion(Long.valueOf(batchAdminRespon.getVersion()).toString());

		if (!CommonValidator.isNullOrEmpty(batchAdminRespon.getOpenedDt())) {
			batchAdminResponse.setOpenedDate(batchAdminRespon.getOpenedDt());
		}
		if (!CommonValidator.isNullOrEmpty(batchAdminRespon.getOpenedTm())) {
			batchAdminResponse.setOpenedTm(batchAdminRespon.getOpenedTm());
		}
		if (batchAdminRespon.getTransAmountCredit() != null) {
			batchAdminResponse.setTransAmountCredit(batchAdminRespon.getTransAmountCredit().toString());
		}
		if (batchAdminRespon.getTransCountCredit() != null) {
			batchAdminResponse.setTransCountCredit(batchAdminRespon.getTransCountCredit().toString());
		}
		if (batchAdminRespon.getTransAmountDebit() != null) {
			batchAdminResponse.setTransAmountDebit(batchAdminRespon.getTransAmountDebit().toString());
		}
		if (batchAdminRespon.getTransAmountTotal() != null) {
			batchAdminResponse.setTransAmountTotal(batchAdminRespon.getTransAmountTotal().toString());
		}
		if (batchAdminRespon.getTransCountDebit() != null) {
			batchAdminResponse.setTransCountDebit(batchAdminRespon.getTransCountDebit().toString());
		}
		if (batchAdminRespon.getTransCountTotal() != null) {
			batchAdminResponse.setTransCountTotal(batchAdminRespon.getTransCountTotal().toString());
		}
		if (batchAdminRespon.getTransCountTotal() != null) {
			batchAdminResponse.setTransCountTotal(batchAdminRespon.getTransCountTotal().toString());
		}

		if (!CommonValidator.isNullOrEmpty(batchAdminRespon.getClosedDt())) {
			batchAdminResponse.setClosedDt(batchAdminRespon.getClosedDt());
		}

		if (!CommonValidator.isNullOrEmpty(batchAdminRespon.getClosedTm())) {
			batchAdminResponse.setClosedTm(batchAdminRespon.getClosedTm());
		}

		if (!CommonValidator.isNullOrEmpty(batchAdminRespon.getProcessedDt())) {
			batchAdminResponse.setProcessedDt(batchAdminRespon.getProcessedDt());
		}
		if (!CommonValidator.isNullOrEmpty(batchAdminRespon.getProcessedTm())) {
			batchAdminResponse.setProcessedTm(batchAdminRespon.getProcessedTm());
		}
		if (!CommonValidator.isNullOrEmpty(batchAdminRespon.getTransCurrCd())) {
			batchAdminResponse.setTransCurrCd(batchAdminRespon.getTransCurrCd());
		}
		if (!CommonValidator.isNullOrEmpty(batchAdminRespon.getTransDt())) {
			batchAdminResponse.setTransDt(batchAdminRespon.getTransDt());
		}
		if (!CommonValidator.isNullOrEmpty(batchAdminRespon.getTransTm())) {
			batchAdminResponse.setTransTm(batchAdminRespon.getTransTm());
		}
		if (batchAdminRespon.getLastRefNumber() != null) {
			batchAdminResponse.setLastRefNumber(batchAdminRespon.getLastRefNumber().toString());
		}

		return batchAdminResponse;
	}

	public static BatchSummaryResponseBean convertResponseXMLToSummaryBean(String XMLString) throws SettlementException {
		BatchSummaryResponseBean batchAdminResponse = new BatchSummaryResponseBean();

		Map<String, List<String>> resHeaderParam = XMLRequestConnection.getResponseHeader();

		BatchAdminResponse batchAdminRespon = (BatchAdminResponse) parseXMLMessage(XMLString, "com.americanexpress.ips.gfsg.beans.xml.batchadmin.response");

		batchAdminResponse.setBatchId(Integer.valueOf(batchAdminRespon.getBatchID()).toString());
		batchAdminResponse.setBatchStatus(batchAdminRespon.getBatchStatus());
		if ((resHeaderParam != null) && (resHeaderParam.get("gfsg-status") != null)) {
			batchAdminResponse.setBatchStatusText(batchAdminRespon.getBatchStatusText() + " | " + (String) ((List) resHeaderParam.get("gfsg-status")).get(0));
		} else {
			batchAdminResponse.setBatchStatusText(batchAdminRespon.getBatchStatusText());
		}
		batchAdminResponse.setMerId(Long.valueOf(batchAdminRespon.getMerId()).toString());
		batchAdminResponse.setMerTrmnlId(batchAdminRespon.getMerTrmnlId());
		batchAdminResponse.setVersion(Long.valueOf(batchAdminRespon.getVersion()).toString());
		batchAdminResponse.setOpenedDate(batchAdminRespon.getOpenedDt());
		batchAdminResponse.setClosedDt(batchAdminRespon.getClosedDt());
		batchAdminResponse.setOpenedTm(batchAdminRespon.getOpenedTm());
		batchAdminResponse.setClosedTm(batchAdminRespon.getClosedTm());
		batchAdminResponse.setProcessedDt(batchAdminRespon.getProcessedDt());
		batchAdminResponse.setProcessedTm(batchAdminRespon.getProcessedTm());
		batchAdminResponse.setTransDt(batchAdminRespon.getTransDt());
		batchAdminResponse.setTransTm(batchAdminRespon.getTransTm());
		batchAdminResponse.setTransCurrCd(batchAdminRespon.getTransCurrCd());
		if (batchAdminRespon.getTransAmountCredit() != null) {
			batchAdminResponse.setTransAmountCredit(Long.valueOf(batchAdminRespon.getTransAmountCredit().longValue()).toString());
		}
		if (batchAdminRespon.getTransCountCredit() != null) {
			batchAdminResponse.setTransCountCredit(Long.valueOf(batchAdminRespon.getTransCountCredit().intValue()).toString());
		}
		if (batchAdminRespon.getTransAmountDebit() != null) {
			batchAdminResponse.setTransAmountDebit(Long.valueOf(batchAdminRespon.getTransAmountDebit().longValue()).toString());
		}
		if (batchAdminRespon.getTransCountDebit() != null) {
			batchAdminResponse.setTransCountDebit(Long.valueOf(batchAdminRespon.getTransCountDebit().intValue()).toString());
		}
		if (batchAdminRespon.getTransAmountTotal() != null) {
			batchAdminResponse.setTransAmountTotal(Long.valueOf(batchAdminRespon.getTransAmountTotal().longValue()).toString());
		}
		if (batchAdminRespon.getTransCountTotal() != null) {
			batchAdminResponse.setTransCountTotal(Integer.valueOf(batchAdminRespon.getTransCountTotal().intValue()).toString());
		}
		if (batchAdminRespon.getLastRefNumber() != null) {
			batchAdminResponse.setLastRefNumber(Long.valueOf(batchAdminRespon.getLastRefNumber().longValue()).toString());
		}

		return batchAdminResponse;
	}

	public static DataCaptureResponseBean convertResXMLToDataCaptureResBean(String XMLString) throws SettlementException {
		DataCaptureResponseBean dataCaptResBean = new DataCaptureResponseBean();

		DataCaptureResponse dataCaptureRes = (DataCaptureResponse) parseXMLMessage(XMLString, "com.americanexpress.ips.gfsg.beans.xml.datacapture.response");
		dataCaptResBean.setVersion(Long.valueOf(dataCaptureRes.getVersion()).toString());
		dataCaptResBean.setMerId(Long.valueOf(dataCaptureRes.getMerId()).toString());
		dataCaptResBean.setMerTrmnlId(dataCaptureRes.getMerTrmnlId().toString());
		dataCaptResBean.setBatchId(Integer.valueOf(dataCaptureRes.getBatchID()).toString());
		dataCaptResBean.setBatchStatus(Integer.valueOf(dataCaptureRes.getStatusCode()).toString());

		dataCaptResBean.setBatchStatusText(dataCaptureRes.getStatusText());

		dataCaptResBean.setRefNumber(dataCaptureRes.getRefNumber());

		return dataCaptResBean;
	}

	public static String createXMLMessage(Object inputData, String beansLocation) throws SettlementException {
		Marshaller m = null;
		JAXBContext jc = null;
		String requestXML = null;
		try {
			jc = JAXBContext.newInstance(beansLocation);
			m = jc.createMarshaller();
			m.setProperty("jaxb.formatted.output", Boolean.TRUE);
			CharArrayWriter arr = new CharArrayWriter();
			m.marshal(inputData, arr);

			requestXML = arr.toString();
		} catch (JAXBException jaxe) {
			LOGGER.error("EXCEPTION_CAUGHT_WHILE while Creating Request xml of GWSService ", jaxe);
			LOGGER.fatal("BUSINESS_XML_PARSING_EXCEPTIONError while parsing the XML");
		} catch (Exception ex) {
			LOGGER.error("EXCEPTION_CAUGHT_WHILEwhile Creating Request xml of  NGMSService ", ex);
			LOGGER.fatal("CRITICAL_GWS_SERVICE_EXECUTION_EXCEPTION Error while executing the NGMS service");
			throw new SettlementException("CRITICAL_GWS_SERVICE_EXECUTION_EXCEPTION", ex);
		}

		return requestXML;
	}

	public static Object parseXMLMessage(String responseXML, String beansLocation) throws SettlementException {
		JAXBContext jc = null;
		Unmarshaller unMarshaller = null;
		Object obj = null;
		try {
			jc = JAXBContext.newInstance(beansLocation);
			unMarshaller = jc.createUnmarshaller();
			StringReader xmlReader = new StringReader(responseXML);
			obj = unMarshaller.unmarshal(new StreamSource(xmlReader));

		} catch (JAXBException jaxe) {
			LOGGER.error("EXCEPTION_CAUGHT_WHILE  while Parsing Response xml of  GWS Service ", jaxe);
			LOGGER.fatal("BUSINESS_XML_PARSING_EXCEPTION Error while parsing the XML");
			throw new SettlementException("BUSINESS_XML_PARSING_EXCEPTION");
		} catch (Exception ex) {
			LOGGER.error("EXCEPTION_CAUGHT_WHILE while Parsing Response xml of  GWS Service  ", ex);
			LOGGER.fatal("CRITICAL_NGMS_SERVICE_EXECUTION_EXCEPTION Error while executing the GWS service");
			throw new SettlementException("CRITICAL_NGMS_SERVICE_EXECUTION_EXCEPTION");
		}

		return obj;
	}
}
