package com.americanexpress.ips.gfsg.utils;

import com.americanexpress.ips.gfsg.beans.SettlementRequestDataObject;
import com.americanexpress.ips.gfsg.beans.common.TransactionAdviceAddendumBean;
import com.americanexpress.ips.gfsg.beans.common.TransactionAdviceBasicBean;
import com.americanexpress.ips.gfsg.beans.common.TransactionAdviceDetailBean;
import com.americanexpress.ips.gfsg.beans.common.TransactionBatchTrailerBean;
import com.americanexpress.ips.gfsg.beans.common.TransactionFileHeaderBean;
import com.americanexpress.ips.gfsg.beans.common.TransactionFileSummaryBean;
import com.americanexpress.ips.gfsg.beans.common.TransactionTBTSpecificBean;
import com.americanexpress.ips.gfsg.beans.common.taa.LocationDetailTAABean;
import com.americanexpress.ips.gfsg.enumerations.ProcessingCode;
import com.americanexpress.ips.gfsg.validator.CommonValidator;
import java.util.ArrayList;
import java.util.List;

public class FieldsAutoPopulator {
	public static void updateTFSRecord(SettlementRequestDataObject settlementReqObject) {
		int numberOfDebits = 0;
		int numberOfcredits = 0;
		long hashTotalDebitAmount = 0L;
		long hashTotalCreditAmount = 0L;
		long hashTotalAmount = 0L;

		for (TransactionTBTSpecificBean transactionTBTSpecifictType : settlementReqObject.getTransactionTBTSpecificType()) {
			if (!transactionTBTSpecifictType.getTransactionAdviceBasicType().isEmpty()) {
				for (TransactionAdviceBasicBean transactionAdviceBasicType : transactionTBTSpecifictType.getTransactionAdviceBasicType()) {
					if ((transactionAdviceBasicType.getProcessingCode().equals(ProcessingCode.DEBIT.getProcessingCode()))
							|| (transactionAdviceBasicType.getProcessingCode().equals(ProcessingCode.CREDIT_REVERSALS.getProcessingCode()))
							|| (transactionAdviceBasicType.getProcessingCode().equals(ProcessingCode.BONUS_PAY_DEBIT.getProcessingCode()))
							|| (transactionAdviceBasicType.getProcessingCode().equals(ProcessingCode.BONUS_PAY_CREDIT_REVERSAL.getProcessingCode()))) {
						numberOfDebits++;
						if (!CommonValidator.isNullOrEmpty(transactionAdviceBasicType.getTransactionAmount())) {
							hashTotalDebitAmount += Long.parseLong(transactionAdviceBasicType.getTransactionAmount());
						}
					} else if ((transactionAdviceBasicType.getProcessingCode().equals(ProcessingCode.CREDIT.getProcessingCode()))
							|| (transactionAdviceBasicType.getProcessingCode().equals(ProcessingCode.DEBIT_REVERSALS.getProcessingCode()))
							|| (transactionAdviceBasicType.getProcessingCode().equals(ProcessingCode.BONUS_PAY_CREDIT.getProcessingCode()))
							|| (transactionAdviceBasicType.getProcessingCode().equals(ProcessingCode.BONUS_PAY_DEBIT_REVERSAL.getProcessingCode()))) {
						numberOfcredits++;
						if (!CommonValidator.isNullOrEmpty(transactionAdviceBasicType.getTransactionAmount())) {
							hashTotalCreditAmount += Long.parseLong(transactionAdviceBasicType.getTransactionAmount());
						}
					}

					if (!CommonValidator.isNullOrEmpty(transactionAdviceBasicType.getTransactionAmount())) {
						hashTotalAmount += Long.parseLong(transactionAdviceBasicType.getTransactionAmount());
					}
				}
			}
		}

		if (settlementReqObject != null) {
			TransactionFileSummaryBean tfsBean = settlementReqObject.getTransactionFileSummaryType();
			tfsBean.setNumberOfDebits(String.valueOf(numberOfDebits));
			tfsBean.setNumberOfCredits(String.valueOf(numberOfcredits));
			tfsBean.setHashTotalCreditAmount(String.valueOf(hashTotalCreditAmount));
			tfsBean.setHashTotalDebitAmount(String.valueOf(hashTotalDebitAmount));
			tfsBean.setHashTotalAmount(String.valueOf(hashTotalAmount));
			settlementReqObject.setTransactionFileSummaryType(tfsBean);
		}
	}

	public static void updateRecordNumber(SettlementRequestDataObject settlementRequestBean) {
		TransactionFileHeaderBean tfh = settlementRequestBean.getTransactionFileHeaderType();
		String recordNumber = "1";
		String nextRecordNumber = getNextRecordNumber(recordNumber);
		if (CommonValidator.isNullOrEmpty(tfh.getRecordNumber())) {
			tfh.setRecordNumber(padString(recordNumber, 8, "0", false, true));
		}
		List<TransactionTBTSpecificBean> tbtSpecificBeanList = settlementRequestBean.getTransactionTBTSpecificType();

		if ((tbtSpecificBeanList != null) && (!tbtSpecificBeanList.isEmpty())) {
			for (TransactionTBTSpecificBean tbtSpecificBean : tbtSpecificBeanList) {
				List<TransactionAdviceBasicBean> tabBeanList = tbtSpecificBean.getTransactionAdviceBasicType();
				int totalNoOfTABs = 0;
				if ((tabBeanList != null) && (!tabBeanList.isEmpty())) {
					totalNoOfTABs = tabBeanList.size();
					for (TransactionAdviceBasicBean tabBean : tabBeanList) {
						tabBean.setRecordNumber(nextRecordNumber);
						nextRecordNumber = getNextRecordNumber(nextRecordNumber);
						TransactionAdviceDetailBean tadBean = tabBean.getTransactionAdviceDetailBean();
						if (tadBean != null) {
							tadBean.setRecordNumber(nextRecordNumber);
							nextRecordNumber = getNextRecordNumber(nextRecordNumber);
						}

						ArrayList<TransactionAdviceAddendumBean> taaBeanList = tabBean.getArrayTAABean();

						if (taaBeanList != null) {
							for (TransactionAdviceAddendumBean taaBean : taaBeanList) {

								if (taaBean != null) {
									taaBean.setRecordNumber(nextRecordNumber);
									nextRecordNumber = getNextRecordNumber(nextRecordNumber);
								}
							}
						}

						LocationDetailTAABean locationTAA = tabBean.getLocationDetailTAABean();
						if (locationTAA != null) {
							locationTAA.setRecordNumber(nextRecordNumber);
							nextRecordNumber = getNextRecordNumber(nextRecordNumber);
						}
					}
				}
				TransactionBatchTrailerBean tbtBean = tbtSpecificBean.getTransactionBatchTrailerBean();
				if (tbtBean != null) {
					tbtBean.setTotalNoOfTabs(String.valueOf(totalNoOfTABs));
					tbtBean.setRecordNumber(nextRecordNumber);
					nextRecordNumber = getNextRecordNumber(nextRecordNumber);
				}
			}
		}
		TransactionFileSummaryBean tfs = settlementRequestBean.getTransactionFileSummaryType();
		if (tfs != null) {
			tfs.setRecordNumber(nextRecordNumber);
		}
	}

	private static String getNextRecordNumber(String recordNumber) {
		int currentRecordNo = Integer.parseInt(recordNumber);
		currentRecordNo++;
		String nextRecordNo = padString(String.valueOf(currentRecordNo), 8, "0", false, true);
		return nextRecordNo;
	}

	private static String padString(String value, int maxLength, String padWith, boolean isLeftJustify, boolean isRightJustify) {
		if (value == null) {
			value = "";
		}
		int length = value.length();
		StringBuffer newValue = new StringBuffer();
		for (; length < maxLength; length++) {
			newValue = newValue.append(padWith);
		}
		if (isLeftJustify) {
			value = value + newValue;
		} else if (isRightJustify) {
			value = newValue + value;
		}

		return value;
	}
}
