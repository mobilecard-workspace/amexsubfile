package com.americanexpress.ips.gfsg.utils;

import com.americanexpress.ips.gfsg.beans.ErrorObject;
import com.americanexpress.ips.gfsg.beans.xml.batchadmin.BatchAdminRequestBean;
import com.americanexpress.ips.gfsg.beans.xml.datacapture.DataCaptureRequestBean;
import com.americanexpress.ips.gfsg.connection.PropertyReader;
import com.americanexpress.ips.gfsg.enumerations.XMLSubmissionErrorCodes;
import com.americanexpress.ips.gfsg.exceptions.SettlementException;
import com.americanexpress.ips.gfsg.validator.CommonValidator;
import com.americanexpress.ips.gfsg.validator.xml.BatchAdminRequestValidator;
import com.americanexpress.ips.gfsg.validator.xml.DataCaptureRequestValidator;
import com.americanexpress.ips.gfsg.validator.xml.InvoiceMsgInfoValidator;
import com.americanexpress.ips.gfsg.validator.xml.TAACorpPurchSolLvl2Validator;
import com.americanexpress.ips.gfsg.validator.xml.industry.TAAAirIndusValidator;
import com.americanexpress.ips.gfsg.validator.xml.industry.TAAAutoRentalIndusValidator;
import com.americanexpress.ips.gfsg.validator.xml.industry.TAACommunServIndusValidator;
import com.americanexpress.ips.gfsg.validator.xml.industry.TAAEntTktIndusValidator;
import com.americanexpress.ips.gfsg.validator.xml.industry.TAAInsrIndusValidator;
import com.americanexpress.ips.gfsg.validator.xml.industry.TAALdgIndusValidator;
import com.americanexpress.ips.gfsg.validator.xml.industry.TAARailIndusValidator;
import com.americanexpress.ips.gfsg.validator.xml.industry.TAARetailIndusValidator;
import com.americanexpress.ips.gfsg.validator.xml.industry.TAATravCruiseIndusValidator;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

public class XMLValidator {
	private static final Logger LOGGER = Logger.getLogger(XMLValidator.class);

	public static List<ErrorObject> validateBatchOpenRequest(BatchAdminRequestBean batchAdminRequestBean) throws SettlementException {
		LOGGER.info("Entering into validateBatchOpenRequest");

		List<ErrorObject> errorCodesList = new ArrayList();

		BatchAdminRequestValidator.validateBatchAdminRequestRec(batchAdminRequestBean, errorCodesList);

		BatchAdminRequestValidator.validateCardAcceptorDetail(batchAdminRequestBean, errorCodesList);

		LOGGER.info("Exiting from validateBatchOpenRequest");
		return errorCodesList;
	}

	public static List<ErrorObject> validateBatchStatusRequest(BatchAdminRequestBean batchAdminRequestBean) throws SettlementException {
		LOGGER.info("Entering into validateBatchStatusRequest");

		List<ErrorObject> errorCodesList = new ArrayList();

		BatchAdminRequestValidator.validateBatchAdminRequestRec(batchAdminRequestBean, errorCodesList);

		LOGGER.info("Exiting from validateBatchStatusRequest");

		return errorCodesList;
	}

	public static List<ErrorObject> validateBatchPurgeRequest(BatchAdminRequestBean batchAdminRequestBean) throws SettlementException {
		LOGGER.info("Entering into validateBatchPurgeRequest");

		List<ErrorObject> errorCodesList = new ArrayList();

		BatchAdminRequestValidator.validateBatchAdminRequestRec(batchAdminRequestBean, errorCodesList);

		LOGGER.info("Exiting from validateBatchPurgeRequest");

		return errorCodesList;
	}

	public static List<ErrorObject> validateBatchCloseRequest(BatchAdminRequestBean batchAdminRequestBean) throws SettlementException {
		LOGGER.info("Entering into validateBatchCloseRequest");

		List<ErrorObject> errorCodesList = new ArrayList();

		BatchAdminRequestValidator.validateBatchAdminRequestRec(batchAdminRequestBean, errorCodesList);

		LOGGER.info("Exiting from validateBatchCloseRequest");

		return errorCodesList;
	}

	public static List<ErrorObject> validateDataCaptureRequest(DataCaptureRequestBean dataCaptureRequestBean) throws SettlementException {
		LOGGER.info("Entering into validateDataCaptureRequest");

		List<ErrorObject> errorList = new ArrayList();
		ErrorObject errorObj = null;

		DataCaptureRequestValidator.validateDataCaptureRequestRec(dataCaptureRequestBean, errorList);

		if (CommonValidator.isNullOrEmpty(dataCaptureRequestBean.getTransAddCd())) {
			if (dataCaptureRequestBean.getTaaCorpPurchSolLvl2() != null) {
				if (!CommonValidator.isIndustryRecordPresent(dataCaptureRequestBean)) {
					TAACorpPurchSolLvl2Validator.validateTAACorpPurchSolLvl2(dataCaptureRequestBean.getTaaCorpPurchSolLvl2(), errorList, dataCaptureRequestBean.getTransCurrCd());
				} else {
					errorObj = new ErrorObject(XMLSubmissionErrorCodes.DCS_ERROR_TAA_CORP_PURCH_SOL_LVL2);

					errorList.add(errorObj);
				}
			}
		} else if (dataCaptureRequestBean.getTaaCorpPurchSolLvl2() != null) {
			errorObj = new ErrorObject(XMLSubmissionErrorCodes.DCS_ERROR_TAA_CORP_PURCH_SOL_LVL2);

			errorList.add(errorObj);
		} else if (dataCaptureRequestBean.getTransAddCd().equals("01")) {
			if (dataCaptureRequestBean.getTaaAirIndustry() == null) {
				errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_ADDENDUMCODE);

				errorList.add(errorObj);
			} else {
				TAAAirIndusValidator.validateAirLineIndustry(dataCaptureRequestBean.getTaaAirIndustry(), errorList, dataCaptureRequestBean.getTransCurrCd());
			}
		} else if (dataCaptureRequestBean.getTransAddCd().equals("04")) {
			if (dataCaptureRequestBean.getTaaInsuranceIndustry() == null) {
				errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_ADDENDUMCODE);

				errorList.add(errorObj);
			} else {
				TAAInsrIndusValidator.validateInsuranceIndustry(dataCaptureRequestBean.getTaaInsuranceIndustry(), errorList);
			}
		} else if (dataCaptureRequestBean.getTransAddCd().equals("05")) {
			if (dataCaptureRequestBean.getTaaAutoRentalIndustry() == null) {
				errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_ADDENDUMCODE);

				errorList.add(errorObj);
			} else {
				TAAAutoRentalIndusValidator.validateAutoRentalIndustry(dataCaptureRequestBean.getTaaAutoRentalIndustry(), errorList, dataCaptureRequestBean.getTransCurrCd());
			}
		} else if (dataCaptureRequestBean.getTransAddCd().equals("06")) {
			if (dataCaptureRequestBean.getTaaRailIndustry() == null) {
				errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_ADDENDUMCODE);

				errorList.add(errorObj);
			} else {
				TAARailIndusValidator.validateRailIndustry(dataCaptureRequestBean.getTaaRailIndustry(), errorList);
			}

		} else if (dataCaptureRequestBean.getTransAddCd().equals("11")) {
			if (dataCaptureRequestBean.getTaaLodgingIndustry() == null) {
				errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_ADDENDUMCODE);

				errorList.add(errorObj);
			} else {
				TAALdgIndusValidator.validateLodgingIndustry(dataCaptureRequestBean.getTaaLodgingIndustry(), errorList, dataCaptureRequestBean.getTransCurrCd());
			}
		} else if (dataCaptureRequestBean.getTransAddCd().equals("13")) {
			if (dataCaptureRequestBean.getTaaCommunServIndustry() == null) {
				errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_ADDENDUMCODE);

				errorList.add(errorObj);
			} else {
				TAACommunServIndusValidator.validateCommunicationServiceIndustry(

				dataCaptureRequestBean.getTaaCommunServIndustry(), errorList);
			}
		} else if (dataCaptureRequestBean.getTransAddCd().equals("14")) {
			if (dataCaptureRequestBean.getTaaTravCruiseIndustry() == null) {
				errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_ADDENDUMCODE);

				errorList.add(errorObj);
			} else {
				TAATravCruiseIndusValidator.validateTravelCruiseIndustry(dataCaptureRequestBean.getTaaTravCruiseIndustry(), errorList, dataCaptureRequestBean.getTransCurrCd());
			}
		} else if (dataCaptureRequestBean.getTransAddCd().equals("20")) {
			if (dataCaptureRequestBean.getTaaRetailIndustry() == null) {
				errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_ADDENDUMCODE);

				errorList.add(errorObj);
			} else {
				TAARetailIndusValidator.validateRetailIndustry(dataCaptureRequestBean.getTaaRetailIndustry(), errorList, dataCaptureRequestBean.getTransCurrCd());
			}
		} else if (dataCaptureRequestBean.getTransAddCd().equals("22")) {
			if (dataCaptureRequestBean.getTaaEntertainmentTktIndustry() == null) {
				errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_ADDENDUMCODE);

				errorList.add(errorObj);
			} else {
				TAAEntTktIndusValidator.validateEntTktIndustry(dataCaptureRequestBean.getTaaEntertainmentTktIndustry(), errorList, dataCaptureRequestBean.getTransCurrCd());
			}
		}

		if (dataCaptureRequestBean.getInvoiceMassageInfoBean() != null) {
			InvoiceMsgInfoValidator.validateInvoiceMsgInfo(dataCaptureRequestBean.getInvoiceMassageInfoBean(), errorList);
		}

		LOGGER.info("Exiting from validateDataCaptureRequest");

		return errorList;
	}

	public static List<ErrorObject> validateConnectionInfo(PropertyReader propertyReader) {
		if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
			LOGGER.info("Entered into validateConnectionInfo()");
		}

		List<ErrorObject> connectionInfoErrorList = new ArrayList();

		if (CommonValidator.isNullOrEmpty(PropertyReader.getXmlhttpsUrl())) {
			ErrorObject connErrorObj = new ErrorObject("7", "Invalid request HTTPS_URL");
			connectionInfoErrorList.add(connErrorObj);
			if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
				LOGGER.error("Invalid request HTTPS_URL");
			}
		}

		if (propertyReader.getHttpsPort() != 443) {
			ErrorObject connErrorObj = new ErrorObject("8", "Invalid request HTTPS_PORT");
			connectionInfoErrorList.add(connErrorObj);
			if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
				LOGGER.error("Invalid request HTTPS_PORT");
			}
		}

		if (CommonValidator.isNullOrEmpty(PropertyReader.getRegion())) {
			ErrorObject connErrorObj = new ErrorObject("1", "Invalid REQUEST_HEADER_REGION");
			connectionInfoErrorList.add(connErrorObj);
			if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
				LOGGER.error("Invalid request header region");
			}
		}

		if (CommonValidator.isNullOrEmpty(PropertyReader.getMerchantCountry())) {
			ErrorObject connErrorObj = new ErrorObject("2", "Invalid REQUEST_HEADER_COUNTRY");
			connectionInfoErrorList.add(connErrorObj);
			if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
				LOGGER.error("Invalid request header country");
			}
		}

		if (CommonValidator.isNullOrEmpty(PropertyReader.getMerchantNumber())) {
			ErrorObject connErrorObj = new ErrorObject("4", "Invalid REQUEST_HEADER_MERCHNBR");
			connectionInfoErrorList.add(connErrorObj);
			if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
				LOGGER.error("Invalid request header merchant number ");
			}
		}

		if (CommonValidator.isNullOrEmpty(PropertyReader.getRoutingIndicator())) {
			ErrorObject connErrorObj = new ErrorObject("5", "Invalid REQUEST_HEADER_RTIND");
			connectionInfoErrorList.add(connErrorObj);
			if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
				LOGGER.error("Invalid request header routing indicator ");
			}
		}

		if (CommonValidator.isNullOrEmpty(PropertyReader.getOrigin())) {
			ErrorObject connErrorObj = new ErrorObject("6", "Invalid REQUEST_HEADER_ORIGIN");
			connectionInfoErrorList.add(connErrorObj);
			if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
				LOGGER.error("Invalid request header origin");
			}
		}

		if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
			LOGGER.info("Errors found in validating the connection:" + connectionInfoErrorList.size());
			LOGGER.info("Exiting from validateConnectionInfo()");
		}
		return connectionInfoErrorList;
	}
}
