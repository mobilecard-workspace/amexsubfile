package com.americanexpress.ips.gfsg.utils;

import com.americanexpress.ips.gfsg.beans.EncryptedData;
import com.americanexpress.util.juice.Juice;

public class DataConversion {
	EncryptedData objEncryptedData = new EncryptedData();
	private final String key = "51867F0C0E55766E34E1FE7E053CBFB4";

	private static char[] ASCII_translate_EBCDIC = {

	'\000', '\001', '\002', '\003', '\004', '\005', '\006', '\007', '\b', '\t', '\n', '\013', '\f', '\r', '\016', '\017', '\020', '\021', '\022', '\023', '\024', '\025', '\026', '\027', '\030',
			'\031', '\032', '\033', '\034', '\035', '\036', '\037', '@', 'Z', '', '{', '[', 'l', 'P', '}', 'M', ']', '\\', 'N', 'k', '`', 'K', 'a', 'ð', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', '÷', 'ø', 'ù',
			'z', '^', 'L', '~', 'n', 'o', '|', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', '×', 'Ø', 'Ù', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', '­', 'à', '½', '_',
			'm', '}', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '¢', '£', '¤', '¥', '¦', '§', '¨', '©', 'À', 'j', 'Ð', '¡', 'K', 'K', 'K', 'K', 'K',
			'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K',
			'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K',
			'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K',
			'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K' };

	public static String convertNumericsToBCD(String getValue) {
		String[] BCD = { "0000", "0001", "0010", "0011", "0100", "0101", "0110", "0111", "1000", "1001" };

		StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < getValue.length(); i++) {
			char temp = getValue.charAt(i);

			int temp1 = Integer.parseInt(temp + "");

			buffer.append(BCD[temp1]);
		}

		return buffer.toString();
	}

	public static String convertASCIIToEBCDICString(String input) {
		StringBuffer output = new StringBuffer("");
		for (int i = 0; i < input.length(); i++) {
			char temp = input.charAt(i);
			int x = ASCII_translate_EBCDIC[temp];
			output.append(Integer.toHexString(x).toUpperCase());
		}

		return output.toString();
	}

	public EncryptedData encryptSFTCredentials(String userID, String passWord) {
		try {
			Juice juice = new Juice("AES/ECB/NoPadding", "51867F0C0E55766E34E1FE7E053CBFB4");
			String resultUserID = juice.encrypt(userID);
			String resultPwd = juice.encrypt(passWord);

			this.objEncryptedData.setEncryptedUserID(resultUserID);
			this.objEncryptedData.setEncryptedPwd(resultPwd);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this.objEncryptedData;
	}
}
