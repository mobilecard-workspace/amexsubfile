package com.americanexpress.ips.gfsg.constants;

public abstract interface Constants
{
  public static final String EN_BINRY = "ENC_BINRY";
  public static final String EN_PACKD = "ENC_PACKD";
  public static final String DEBUG_FLAG_ON = "ON";
  public static final String DEBUG_FLAG_OFF = "OFF";
  public static final String EN_HEX = "ENC_HEX";
  public static final String REQUEST_HEADER_REGION = "region";
  public static final String REQUEST_HEADER_COUNTRY = "country";
  public static final String REQUEST_HEADER_MESSAGE = "message";
  public static final String REQUEST_HEADER_MERCHNBR = "MerchNbr";
  public static final String REQUEST_HEADER_ORIGIN = "origin";
  public static final String REQUEST_HEADER_RTIND = "RtInd";
  public static final String REQUEST_HEADER_API_VERSION = "API_Version";
  public static final String FILE_TYPE_VARIABLE = "VARIABLE";
  public static final String FILE_TYPE_FIXED = "FIXED";
  public static final String FILE_STORAGE_ON = "ON";
  public static final String FILE_STORAGE_OFF = "OFF";
  public static final String ACKNOWLEDGE_TYPE = "TEXT";
  public static final String INDICATOR_Y = "Y";
  public static final String INDICATOR_N = "N";
  public static final String MASKING_CHARACTER = "*";
  public static final String REQUEST_CHANNEL_TYPE_FTPS = "FTPs";
  public static final String REQUEST_CHANNEL_TYPE_HTTPS = "HTTPs";
  public static final String REQUEST_CHANNEL_TYPE_SSH = "SSH";
  public static final String CONNECTION_TYPE_FTP = "FTP";
  public static final String GREATER_THAN_MAX = "greaterThanMax";
  public static final String LESS_THAN_MIN = "lessThanMin";
  public static final String EQUAL = "equal";
  public static final String TRANSACTION_TYPE_TKT = "TKT";
  public static final String TRANSACTION_TYPE_REF = "REF";
  public static final String TRANSACTION_TYPE_EXC = "EXC";
  public static final String TRANSACTION_TYPE_MSC = "MSC";
  public static final String DATE_FORMAT_YYMM = "YYMM";
  public static final String DATE_FORMAT_CCYYMMDD = "CCYYMMDD";
  public static final String DATE_FORMAT_YYYYMMDD = "yyyyMMdd";
  public static final String STATUS_EXPIRED = "expired";
  public static final String STATUS_FUTUREDATE = "futureDate";
  public static final String MULTIPLE_ADJUSTMENTS = "X";
  public static final String ONE_ADJUSTMENT_ONLY_CARDMEMBER_NOTIFIED = "Y";
  public static final String ONE_ADJUSTMENT_ONLY_CARDMEMBER_NOT_NOTIFIED = "Z";
  public static final String EMV_FORMAT_TYPE_PACKED = "00";
  public static final String EMV_FORMAT_TYPE_UNPACKED = "01";
  public static final String EMV_TRANSACTION_TYPE_DEBIT = "00";
  public static final String EMV_TRANSACTION_TYPE_CASH = "01";
  public static final String EMV_TRANSACTION_TYPE_CREDIT = "20";
  public static final String FTP_REMOTE_DIRECTORY = "/outbox";
  public static final String FTP_REMOTE_DIRECTORY_INBOX = "/inbox";
  public static final String FTP_CHANNEL_TYPE = "sftp";
  public static final int FTP_PORT = 22;
  public static final String XML_BATCH_ADMIN_REQUEST_MESSAGE_TYPE = "GFSG XML BAR";
  public static final String XML_DATA_CAPTURE_REQUEST_MESSAGE_TYPE = "GFSG XML DCR";
  public static final String MATCHING_KEY_TYPE_RESERVED_FOR_AMEX_INTERNAL_USE = "01";
  public static final String MATCHING_KEY_TYPE_ENHANCED_BUSINESS_TRAVEL_ACCOUNT = "02";
  public static final String MATCHING_KEY_TYPE_MEXICO_SUBMISSIONS_IDENTIFIER = "99";
  public static final String MATCHING_KEY_TYPE_RETAIL_LID = "03";
  public static final String MATCHING_KEY_TYPE_RESERVED_2_FOR_AMEX_INTERNAL_USE = "04";
  public static final String DEFAULT_TFH_RECORD_NUMBER = "00000001";
  public static final String DEFAULT_FILE_VERSION_NUMBER = "12010000";
  public static final String DEFAULT_INSTALLMENT_NUMBER = "0001";
  public static final String DEFAULT_CRYPTO_PROVIDER = "com.ibm.crypto.provider.DESKey@fffe7f1e";
}


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\constants\Constants.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */