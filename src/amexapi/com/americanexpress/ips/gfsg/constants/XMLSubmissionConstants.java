/*     */ package com.americanexpress.ips.gfsg.constants;
/*     */ 
/*     */ 
/*     */ public abstract interface XMLSubmissionConstants
/*     */ {
/*     */   public static final String TRAVDISUNIT_MILES = "M";
/*     */   
/*     */   public static final String TRAVDISUNIT_KILOMETERS = "K";
/*     */   
/*     */   public static final String AUDITADJIND_MULTIPLE_ADJUSTMENTS = "X";
/*     */   
/*     */   public static final String AUDITADJIND_ONE_ADJUSTMENT_ONLY_CARDMEMBER_NOTIFIED = "Y";
/*     */   
/*     */   public static final String AUDITADJIND_ONE_ADJUSTMENT_ONLY_CARDMEMBER_NOT_NOTIFIED = "Z";
/*     */   
/*     */   public static final String BATCHADMIN_VERSION = "12010000";
/*     */   
/*     */   public static final String DATA_CAPTURE_VERSION = "12010000";
/*     */   
/*     */   public static final String DATE_FORMAT_YYMM = "YYMM";
/*     */   
/*     */   public static final String DATE_FORMAT_CCYYMMDD = "CCYYMMDD";
/*     */   
/*     */   public static final String DATE_FORMAT_YYYYMMDD = "yyyyMMdd";
/*     */   
/*     */   public static final String DATE_FORMAT_CCYYMM = "CCYYMM";
/*     */   
/*     */   public static final String ADDENDUM_AIRLINE = "01";
/*     */   
/*     */   public static final String ADDENDUM_AUTORENTAL = "05";
/*     */   
/*     */   public static final String ADDENDUM_COMMUNICATION = "13";
/*     */   
/*     */   public static final String ADDENDUM_INSURANCE = "04";
/*     */   
/*     */   public static final String ADDENDUM_TKT_ENT = "22";
/*     */   
/*     */   public static final String ADDENDUM_RETAIL = "20";
/*     */   
/*     */   public static final String ADDENDUM_TRAV_CRIUSE = "14";
/*     */   
/*     */   public static final String ADDENDUM_RAIL = "06";
/*     */   
/*     */   public static final String ADDENDUM_LODGING = "11";
/*     */   
/*     */   public static final String STATUS_EXPIRED = "expired";
/*     */   
/*     */   public static final String STATUS_FUTUREDATE = "futureDate";
/*     */   public static final String MULTIPLE_ADJUSTMENTS = "X";
/*     */   public static final String ONE_ADJUSTMENT_ONLY_CARDMEMBER_NOTIFIED = "Y";
/*     */   public static final String ONE_ADJUSTMENT_ONLY_CARDMEMBER_NOT_NOTIFIED = "Z";
/*     */   public static final String EMV_FORMAT_TYPE_PACKED = "00";
/*     */   public static final String EMV_FORMAT_TYPE_UNPACKED = "01";
/*     */   public static final String EMV_TRANSACTION_TYPE_DEBIT = "00";
/*     */   public static final String EMV_TRANSACTION_TYPE_CASH = "01";
/*     */   public static final String EMV_TRANSACTION_TYPE_CREDIT = "20";
/*     */   public static final String USAGE_PROHIBITED = "X";
/*     */   public static final String CAR_PACKAGE = "C";
/*     */   public static final String AIRLINE_PACKAGE = "A";
/*     */   public static final String BOTH_CAR_AND_AIRLINE_PACKAGE = "B";
/*     */   public static final String UNKNOWN_PACKAGE = "N";
/*     */   public static final String PLAN_N = "0003";
/*     */   public static final String DPP = "0005";
/*  64 */   public static final String[] RECORD_VALUE = {
/*     */   
/*  66 */     "[0-9]+$", 
/*  67 */     "[0-9]+$", 
/*  68 */     "[A-Za-z0-9- ]+$", 
/*  69 */     "[0-9]+$", 
/*  70 */     "[0-9]+$", 
/*  71 */     "[0-9]+$", 
/*  72 */     "[A-Za-z0-9- ]+$", 
/*  73 */     "[A-Za-z0-9- ]+$", 
/*     */     
/*  75 */     "[A-Za-z0-9- ]+$", 
/*  76 */     "[A-Za-z0-9- ]+$", 
/*  77 */     "[A-Za-z0-9- ]+$", 
/*  78 */     "[A-Za-z0-9- ]+$", 
/*  79 */     "[A-Za-z0-9- ]+$", 
/*  80 */     "[A-Za-z0-9- ]+$", 
/*  81 */     "[0-9]+$", 
/*  82 */     "[0-9]+$", 
/*  83 */     "[0-9]+$", 
/*  84 */     "[0-9]+$", 
/*  85 */     "[0-9]+$", 
/*  86 */     "[0-9]+$", 
/*  87 */     "[A-Za-z0-9- ]+$", 
/*  88 */     "[0-9]+$", 
/*  89 */     "[0-9]+$", 
/*  90 */     "[0-9]+$", 
/*  91 */     "[0-9]+$", 
/*  92 */     "[0-9]+$", 
/*  93 */     "[0-9]+$", 
/*  94 */     "[0-9]+$", 
/*  95 */     "[A-Za-z0-9- ]+$", 
/*  96 */     "[0-9]+$", 
/*  97 */     "[0-9]+$", 
/*  98 */     "[0-9]+$", 
/*  99 */     "[0-9]+$", 
/* 100 */     "[0-9]+$", 
/* 101 */     "[A-Za-z0-9- ]+$", 
/* 102 */     "[0-9]+$", 
/* 103 */     "[0-9]+$", 
/* 104 */     "[0-9]+$", 
/* 105 */     "[A-Za-z0-9- ]+$", 
/* 106 */     "[A-Za-z0-9- ]+$", 
/* 107 */     "[A-Za-z0-9- ]+$", 
/* 108 */     "[A-Za-z0-9- ]+$", 
/* 109 */     "[A-Za-z0-9- ]+$", 
/* 110 */     "[A-Za-z0-9- ]+$", 
/* 111 */     "[A-Za-z0-9- ]+$", 
/* 112 */     "[A-Za-z0-9- ]+$", 
/* 113 */     "[A-Za-z0-9- ]+$", 
/* 114 */     "[A-Za-z0-9- ]+$", 
/* 115 */     "[A-Za-z0-9- ]+$", 
/* 116 */     "[A-Za-z0-9- ]+$", 
/* 117 */     "[0-9]+$", 
/* 118 */     "[0-9]+$", 
/* 119 */     "[0-9]+$", 
/* 120 */     "[A-Za-z0-9- ]+$", 
/* 121 */     "[A-Za-z0-9-!\"#$%&'()*+,./:;<=>?@[\\\\]^_`{|}~áàåäâçéêèëíìîïñóôòöúûüùÁÅÄßÉÊÈÍÎÑÓÔÖÚÛ ]+$", 
/*     */     
/* 123 */     "[0-9]+$", 
/* 124 */     "[A-Za-z0-9- ]+$", 
/* 125 */     "[0-9]+$", 
/* 126 */     "[0-9]+$", 
/* 127 */     "[A-Za-z0-9-!\"#$%&'()*+,./:;<=>?@[\\\\]^_`{|}~ ]+$", 
/* 128 */     "[A-Za-z0-9]+$", 
/* 129 */     "[A-Za-z0-9]+$", 
/*     */     
/* 131 */     "[0-9]+$", 
/* 132 */     "[0-9]+$", 
/* 133 */     "[+-]+$", 
/* 134 */     "[0-9]+$", 
/* 135 */     "[A-Za-z- ]+$", 
/* 136 */     "[A-Za-z0-9- ]+$", 
/* 137 */     "[0-9]+$", 
/* 138 */     "[A-Za-z0-9- ]+$", 
/* 139 */     "[A-Za-z0-9- ]+$", 
/* 140 */     "[A-Za-z0-9- ]+$", 
/* 141 */     "[A-Za-z0-9- ]+$", 
/* 142 */     "[0-9]+$", 
/* 143 */     "[0-9]+$", 
/* 144 */     "[A-Za-z0-9- ]+$", 
/* 145 */     "[A-Za-z- ]+$", 
/* 146 */     "[A-Za-z- ]+$", 
/* 147 */     "[A-Za-z0-9- ]+$", 
/* 148 */     "[A-Za-z- ]+$", 
/* 149 */     "[A-Za-z0-9- ]+$", 
/* 150 */     "[0-9]+$", 
/* 151 */     "[A-Za-z0-9- ]+$", 
/* 152 */     "[A-Za-z0-9- ]+$", 
/* 153 */     "[A-Za-z0-9- ]+$", 
/* 154 */     "[A-Za-z0-9- ]+$", 
/* 155 */     "[A-Za-z0-9- ]+$", 
/* 156 */     "[0-9]+$", 
/* 157 */     "[0-9]+$", 
/* 158 */     "[A-Za-z0-9]+$", 
/* 159 */     "[A-Za-z0-9- ]+$", 
/* 160 */     "[A-Za-z0-9- ]+$", 
/* 161 */     "[A-Za-z0-9- ]+$", 
/* 162 */     "[A-Za-z0-9- ]+$", 
/* 163 */     "[A-Za-z0-9- ]+$", 
/* 164 */     "[0-9]+$", 
/* 165 */     "[0-9]+$", 
/* 166 */     "[A-Za-z0-9- ]+$", 
/* 167 */     "[A-Za-z0-9- ]+$", 
/* 168 */     "[A-Za-z0-9- ]+$", 
/* 169 */     "[0-9]+$", 
/* 170 */     "[0-9]+$", 
/* 171 */     "[A-Za-z0-9- ]+$", 
/* 172 */     "[0-9]+$", 
/* 173 */     "[0-9]+$", 
/* 174 */     "[0-9]+$", 
/* 175 */     "[A-Za-z- ]+$", 
/* 176 */     "[0-9]+$", 
/* 177 */     "[0-9]+$", 
/* 178 */     "[0-9]+$", 
/* 179 */     "[0-9]+$", 
/* 180 */     "[A-Za-z0-9- ]+$", 
/* 181 */     "[A-Za-z0-9- ]+$", 
/* 182 */     "[A-Za-z0-9- ]+$", 
/* 183 */     "[A-Za-z0-9-!\"#$%&'()*+,./:;<=>?@[\\\\]^_`{|}~áàåäâçéêèëíìîïñóôòöúûüùÁÅÄßÉÊÈÍÎÑÓÔÖÚÛ ]+$", 
/* 184 */     "[A-Za-z0-9- ]+$", 
/* 185 */     "[A-Za-z0-9- ]+$", 
/* 186 */     "[A-Za-z0-9- ]+$", 
/* 187 */     "[A-Za-z0-9-!\"#$%&'()*+,./:;<=>?@[\\\\]^_`{|}~áàåäâçéêèëíìîïñóôòöúûüùÁÅÄßÉÊÈÍÎÑÓÔÖÚÛ ]+$", 
/* 188 */     "[A-Za-z0-9- ]+$", 
/* 189 */     "[A-Za-z0-9- ]+$", 
/* 190 */     "[A-Za-z0-9- ]+$", 
/* 191 */     "[A-Za-z- ]+$", 
/* 192 */     "[A-Za-z- ]+$", 
/* 193 */     "[A-Za-z0-9- ]+$", 
/* 194 */     "[0-9]+$", 
/* 195 */     "[0-9]+$", 
/* 196 */     "[0-9]+$", 
/* 197 */     "[A-Za-z0-9- ]+$", 
/* 198 */     "[A-Za-z0-9- ]+$", 
/* 199 */     "[A-Za-z0-9- ]+$", 
/* 200 */     "[A-Za-z0-9- ]+$", 
/* 201 */     "[0-9]+$", 
/* 202 */     "[0-9]+$", 
/* 203 */     "[A-Za-z0-9- ]+$", 
/* 204 */     "[A-Za-z0-9- ]+$", 
/* 205 */     "[A-Za-z0-9- ]+$", 
/* 206 */     "[A-Za-z0-9- ]+$", 
/* 207 */     "[0-9]+$", 
/* 208 */     "[0-9]+$", 
/* 209 */     "[0-9]+$", 
/* 210 */     "[0-9]+$", 
/* 211 */     "[0-9]+$", 
/* 212 */     "[A-Za-z0-9- ]+$", 
/* 213 */     "[A-Za-z0-9- ]+$", 
/* 214 */     "[A-Za-z- ]+$", 
/* 215 */     "[A-Za-z0-9- ]+$", 
/* 216 */     "[A-Za-z0-9- ]+$", 
/* 217 */     "[A-Za-z0-9- ]+$", 
/* 218 */     "[A-Za-z0-9- ]+$", 
/* 219 */     "[A-Za-z0-9- ]+$", 
/* 220 */     "[A-Za-z0-9- ]+$", 
/* 221 */     "[0-9]+$", 
/* 222 */     "[A-Za-z0-9- ]+$", 
/* 223 */     "[A-Za-z0-9- ]+$", 
/* 224 */     "[A-Za-z0-9- ]+$", 
/* 225 */     "[0-9]+$", 
/* 226 */     "[0-9]+$", 
/* 227 */     "[A-Za-z0-9- ]+$", 
/* 228 */     "[A-Za-z0-9- ]+$", 
/* 229 */     "[A-Za-z- ]+$", 
/* 230 */     "[A-Za-z0-9- ]+$", 
/* 231 */     "[A-Za-z0-9- ]+$", 
/* 232 */     "[0-9]+$", 
/* 233 */     "[0-9]+$", 
/* 234 */     "[0-9]+$", 
/* 235 */     "[0-9]+$", 
/* 236 */     "[A-Za-z0-9- ]+$", 
/* 237 */     "[A-Za-z0-9- ]+$", 
/* 238 */     "[A-Za-z0-9- ]+$", 
/* 239 */     "[A-Za-z0-9- ]+$", 
/* 240 */     "[0-9]+$", 
/* 241 */     "[A-Za-z0-9- ]+$", 
/* 242 */     "[A-Za-z0-9- ]+$", 
/* 243 */     "[A-Za-z0-9- ]+$", 
/* 244 */     "[A-Za-z0-9- ]+$", 
/* 245 */     "[A-Za-z0-9- ]+$", 
/* 246 */     "[A-Za-z0-9- ]+$", 
/* 247 */     "[A-Za-z0-9- ]+$", 
/* 248 */     "[0-9]+$", 
/* 249 */     "[0-9]+$", 
/* 250 */     "[A-Za-z0-9- ]+$", 
/* 251 */     "[A-Za-z0-9- ]+$", 
/* 252 */     "[0-9]+$", 
/* 253 */     "[A-Za-z0-9- ]+$", 
/* 254 */     "[A-Za-z0-9- ]+$", 
/* 255 */     "[A-Za-z0-9- ]+$", 
/* 256 */     "[A-Za-z0-9- ]+$", 
/* 257 */     "[0-9]+$", 
/* 258 */     "[0-9]+$", 
/* 259 */     "[0-9]+$", 
/* 260 */     "[0-9]+$", 
/* 261 */     "[A-Za-z0-9-!\"#$%&'()*+,./:;<=>?@[\\\\]^_`{|}~ ]+$", 
/* 262 */     "[0-9]+$", 
/* 263 */     "[0-9]+$", 
/* 264 */     "[A-Za-z0-9-!\"#$%&'()*+,./:;<=>?@[\\\\]^_`{|}~áàåäâçéêèëíìîïñóôòöúûüùÁÅÄßÉÊÈÍÎÑÓÔÖÚÛ ]+$", 
/* 265 */     "[A-Za-z0-9-!\"#$%&'()*+,./:;<=>?@[\\\\]^_`{|}~áàåäâçéêèëíìîïñóôòöúûüùÁÅÄßÉÊÈÍÎÑÓÔÖÚÛ ]+$", 
/* 266 */     "[A-Za-z0-9-!\"#$%&'()*+,./:;<=>?@[\\\\]^_`{|}~áàåäâçéêèëíìîïñóôòöúûüùÁÅÄßÉÊÈÍÎÑÓÔÖÚÛ ]+$", 
/* 267 */     "[A-Za-z0-9-!\"#$%&'()*+,./:;<=>?@[\\\\]^_`{|}~áàåäâçéêèëíìîïñóôòöúûüùÁÅÄßÉÊÈÍÎÑÓÔÖÚÛ ]+$" };
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\constants\XMLSubmissionConstants.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */