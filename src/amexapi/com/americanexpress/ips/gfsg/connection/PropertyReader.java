/*     */ package com.americanexpress.ips.gfsg.connection;
/*     */ 
/*     */ import com.americanexpress.ips.gfsg.validator.CommonValidator;
/*     */ import java.util.Properties;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class PropertyReader
/*     */ {
/*  25 */   private static String merchantNumber = "5021011432";
/*     */   
/*     */   public static final String USER_AGENT = "Terminal1";
/*  28 */   private static String debuggerFlag = "ON";
/*  29 */   private static String origin = "TEST-SV-SYSTEMS";
/*     */   
/*     */   public static final String API_VERSION = "Payments API 1.0";
/*  32 */   private static String submissionType = "ISO";
/*  33 */   private static String region = "USA";
/*  34 */   private static String methodType = "POST";
/*  35 */   private static String routingIndicator = "015";
/*     */   
/*  37 */   private static String merchantCountry = "840";
/*  38 */   private static String ftpPort = "22";
/*  39 */   private static String ftpUserID = "GWSJAVARTST";
/*  40 */   private static String ftpPassword = "amex123";
/*  41 */   private static String ftpURL = "148.171.39.41";
/*     */   
/*     */ 
/*  44 */   private static String ftpLocalUploadFilePath = "";
/*  45 */   private static String ftpLocalDownloadFilePath = "";
/*  46 */   private static String ftpUploadFileName = "GWSJAVARTST.SETTLEMENT.GWS002";
/*  47 */   private static String ftpDownloadAcknowledgeFileName = "TST004.GWS001.A";
/*  48 */   private static String ftpDownloadPrintedFileName = "TST004.GWS001.C";
/*  49 */   private static String ftpDownloadRawFileName = "TST004.GWS002.R";
/*     */   
/*  51 */   private static String propsLocation = "./settlement.properties";
/*  52 */   private static String xmlProxyPort = "8080";
/*     */   private static String xmlProxyUserid;
/*     */   private static String xmlProxyPassword;
/*  55 */   private static String xmlhttpsUrl = "https://qwww318.americanexpress.com/IPPayments/inter/CardAuthorization.do";
/*  56 */   private static String xmlProxy = "YES";
/*  57 */   private static String xmlProxyAddress = "172.19.32.171";
/*  58 */   private static String xmlProxyAuthentication = "Basic";
/*     */   private static String fileStorage;
/*  60 */   private static String fileType = "VARIABLE";
/*  61 */   private static String acknowledgeType = "TEXT";
/*     */   
/*     */   private static int httpsPort;
/*     */   
/*     */ 
/*     */   public static String getXmlProxy()
/*     */   {
/*  68 */     return xmlProxy;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static void setXmlProxy(String xmlProxy)
/*     */   {
/*  75 */     xmlProxy = xmlProxy;
/*     */   }
/*     */   
/*  78 */   Properties clientprops = new Properties();
/*  79 */   PropertyReader propertyHolder = null;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private PropertyReader loadISOProperties()
/*     */   {
/*  91 */     this.propertyHolder = new PropertyReader();
/*     */     
/*     */ 
/*     */ 
/*  95 */     setFtpPort(this.clientprops.getProperty("FTP_PORT"));
/*  96 */     setFtpUserID(this.clientprops.getProperty("FTP_USERID"));
/*  97 */     setFtpPassword(this.clientprops.getProperty("FTP_PASSWORD"));
/*  98 */     setFtpURL(this.clientprops.getProperty("FTP_URL"));
/*  99 */     if (!CommonValidator.isNullOrEmpty(this.clientprops.getProperty("FTP_UPLOADED_LOCAL_FILE_PATH")))
/* 100 */       setFtpLocalUploadFilePath(this.clientprops.getProperty("FTP_UPLOADED_LOCAL_FILE_PATH"));
/* 101 */     if (!CommonValidator.isNullOrEmpty(this.clientprops.getProperty("FTP_DOWNLOADED_LOCAL_FILE_PATH")))
/* 102 */       setFtpLocalDownloadFilePath(this.clientprops.getProperty("FTP_DOWNLOADED_LOCAL_FILE_PATH"));
/* 103 */     setFtpUploadFileName(this.clientprops.getProperty("FTP_UPLOAD_FILE_NAME"));
/* 104 */     setAcknowledgeType(this.clientprops.getProperty("ACKNOWLEDGE_TYPE"));
/* 105 */     setFtpDownloadAcknowledgeFileName(this.clientprops.getProperty("FTP_DOWNLOAD_ACKNOWLEDGE_FILE_NAME"));
/* 106 */     setFtpDownloadPrintedFileName(this.clientprops.getProperty("FTP_DOWNLOAD_PRINTED_FILE_NAME"));
/* 107 */     setFtpDownloadRawFileName(this.clientprops.getProperty("FTP_DOWNLOAD_RAW_FILE_NAME"));
/* 108 */     if (!CommonValidator.isNullOrEmpty(this.clientprops.getProperty("ACKNOWLEDGE_TYPE")))
/* 109 */       setFileType(this.clientprops.getProperty("ACKNOWLEDGE_TYPE"));
/* 110 */     setFileStorage(this.clientprops.getProperty("FILE_UPLOAD_BACKUP"));
/*     */     
/*     */ 
/* 113 */     return this;
/*     */   }
/*     */   
/*     */   /* Error */
/*     */   public PropertyReader loadPropertyFile(String propertyfilepath)
/*     */   {
/*     */     // Byte code:
/*     */     //   0: aconst_null
/*     */     //   1: astore_2
/*     */     //   2: ldc 1
/*     */     //   4: aload_1
/*     */     //   5: invokevirtual 248	java/lang/Class:getResource	(Ljava/lang/String;)Ljava/net/URL;
/*     */     //   8: astore_2
/*     */     //   9: aload_0
/*     */     //   10: getfield 161	com/americanexpress/ips/gfsg/connection/PropertyReader:clientprops	Ljava/util/Properties;
/*     */     //   13: aload_2
/*     */     //   14: invokevirtual 254	java/net/URL:openStream	()Ljava/io/InputStream;
/*     */     //   17: invokevirtual 260	java/util/Properties:load	(Ljava/io/InputStream;)V
/*     */     //   20: goto +4 -> 24
/*     */     //   23: astore_3
/*     */     //   24: ldc 63
/*     */     //   26: aload_0
/*     */     //   27: getfield 161	com/americanexpress/ips/gfsg/connection/PropertyReader:clientprops	Ljava/util/Properties;
/*     */     //   30: ldc_w 264
/*     */     //   33: invokevirtual 175	java/util/Properties:getProperty	(Ljava/lang/String;)Ljava/lang/String;
/*     */     //   36: invokevirtual 266	java/lang/String:equals	(Ljava/lang/Object;)Z
/*     */     //   39: ifeq +14 -> 53
/*     */     //   42: aload_0
/*     */     //   43: aload_0
/*     */     //   44: invokespecial 272	com/americanexpress/ips/gfsg/connection/PropertyReader:loadISOProperties	()Lcom/americanexpress/ips/gfsg/connection/PropertyReader;
/*     */     //   47: putfield 163	com/americanexpress/ips/gfsg/connection/PropertyReader:propertyHolder	Lcom/americanexpress/ips/gfsg/connection/PropertyReader;
/*     */     //   50: goto +30 -> 80
/*     */     //   53: aload_0
/*     */     //   54: getfield 161	com/americanexpress/ips/gfsg/connection/PropertyReader:clientprops	Ljava/util/Properties;
/*     */     //   57: ldc_w 264
/*     */     //   60: invokevirtual 175	java/util/Properties:getProperty	(Ljava/lang/String;)Ljava/lang/String;
/*     */     //   63: ldc_w 274
/*     */     //   66: invokevirtual 266	java/lang/String:equals	(Ljava/lang/Object;)Z
/*     */     //   69: ifeq +11 -> 80
/*     */     //   72: aload_0
/*     */     //   73: aload_0
/*     */     //   74: invokespecial 276	com/americanexpress/ips/gfsg/connection/PropertyReader:loadXMLProperties	()Lcom/americanexpress/ips/gfsg/connection/PropertyReader;
/*     */     //   77: putfield 163	com/americanexpress/ips/gfsg/connection/PropertyReader:propertyHolder	Lcom/americanexpress/ips/gfsg/connection/PropertyReader;
/*     */     //   80: aload_0
/*     */     //   81: getfield 161	com/americanexpress/ips/gfsg/connection/PropertyReader:clientprops	Ljava/util/Properties;
/*     */     //   84: ldc_w 279
/*     */     //   87: invokevirtual 175	java/util/Properties:getProperty	(Ljava/lang/String;)Ljava/lang/String;
/*     */     //   90: invokestatic 281	com/americanexpress/ips/gfsg/connection/PropertyReader:setDebuggerFlag	(Ljava/lang/String;)V
/*     */     //   93: goto +36 -> 129
/*     */     //   96: astore_3
/*     */     //   97: aload_2
/*     */     //   98: invokevirtual 254	java/net/URL:openStream	()Ljava/io/InputStream;
/*     */     //   101: invokevirtual 284	java/io/InputStream:close	()V
/*     */     //   104: goto +37 -> 141
/*     */     //   107: astore 5
/*     */     //   109: goto +32 -> 141
/*     */     //   112: astore 4
/*     */     //   114: aload_2
/*     */     //   115: invokevirtual 254	java/net/URL:openStream	()Ljava/io/InputStream;
/*     */     //   118: invokevirtual 284	java/io/InputStream:close	()V
/*     */     //   121: goto +5 -> 126
/*     */     //   124: astore 5
/*     */     //   126: aload 4
/*     */     //   128: athrow
/*     */     //   129: aload_2
/*     */     //   130: invokevirtual 254	java/net/URL:openStream	()Ljava/io/InputStream;
/*     */     //   133: invokevirtual 284	java/io/InputStream:close	()V
/*     */     //   136: goto +5 -> 141
/*     */     //   139: astore 5
/*     */     //   141: aload_0
/*     */     //   142: getfield 163	com/americanexpress/ips/gfsg/connection/PropertyReader:propertyHolder	Lcom/americanexpress/ips/gfsg/connection/PropertyReader;
/*     */     //   145: areturn
/*     */     // Line number table:
/*     */     //   Java source line #124	-> byte code offset #0
/*     */     //   Java source line #128	-> byte code offset #2
/*     */     //   Java source line #129	-> byte code offset #4
/*     */     //   Java source line #128	-> byte code offset #8
/*     */     //   Java source line #133	-> byte code offset #9
/*     */     //   Java source line #135	-> byte code offset #23
/*     */     //   Java source line #142	-> byte code offset #24
/*     */     //   Java source line #143	-> byte code offset #42
/*     */     //   Java source line #144	-> byte code offset #53
/*     */     //   Java source line #145	-> byte code offset #63
/*     */     //   Java source line #146	-> byte code offset #72
/*     */     //   Java source line #149	-> byte code offset #80
/*     */     //   Java source line #151	-> byte code offset #96
/*     */     //   Java source line #157	-> byte code offset #97
/*     */     //   Java source line #159	-> byte code offset #107
/*     */     //   Java source line #155	-> byte code offset #112
/*     */     //   Java source line #157	-> byte code offset #114
/*     */     //   Java source line #159	-> byte code offset #124
/*     */     //   Java source line #162	-> byte code offset #126
/*     */     //   Java source line #157	-> byte code offset #129
/*     */     //   Java source line #159	-> byte code offset #139
/*     */     //   Java source line #163	-> byte code offset #141
/*     */     // Local variable table:
/*     */     //   start	length	slot	name	signature
/*     */     //   0	146	0	this	PropertyReader
/*     */     //   0	146	1	propertyfilepath	String
/*     */     //   1	129	2	url	java.net.URL
/*     */     //   23	1	3	localException	Exception
/*     */     //   96	1	3	localException1	Exception
/*     */     //   112	15	4	localObject	Object
/*     */     //   107	1	5	localIOException	java.io.IOException
/*     */     //   124	1	5	localIOException1	java.io.IOException
/*     */     //   139	1	5	localIOException2	java.io.IOException
/*     */     // Exception table:
/*     */     //   from	to	target	type
/*     */     //   9	20	23	java/lang/Exception
/*     */     //   2	93	96	java/lang/Exception
/*     */     //   97	104	107	java/io/IOException
/*     */     //   2	97	112	finally
/*     */     //   114	121	124	java/io/IOException
/*     */     //   129	136	139	java/io/IOException
/*     */   }
/*     */   
/*     */   /* Error */
/*     */   public PropertyReader getPropertyValuesDataBase()
/*     */   {
/*     */     // Byte code:
/*     */     //   0: aconst_null
/*     */     //   1: astore_1
/*     */     //   2: aconst_null
/*     */     //   3: astore_2
/*     */     //   4: iconst_2
/*     */     //   5: istore_3
/*     */     //   6: ldc_w 297
/*     */     //   9: invokestatic 299	java/lang/Class:forName	(Ljava/lang/String;)Ljava/lang/Class;
/*     */     //   12: pop
/*     */     //   13: ldc_w 303
/*     */     //   16: astore 4
/*     */     //   18: aload 4
/*     */     //   20: ldc_w 305
/*     */     //   23: ldc_w 307
/*     */     //   26: invokestatic 309	java/sql/DriverManager:getConnection	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/sql/Connection;
/*     */     //   29: astore_1
/*     */     //   30: ldc_w 315
/*     */     //   33: astore 5
/*     */     //   35: aload_1
/*     */     //   36: aload 5
/*     */     //   38: invokeinterface 317 2 0
/*     */     //   43: astore 6
/*     */     //   45: aload 6
/*     */     //   47: iconst_1
/*     */     //   48: iload_3
/*     */     //   49: invokeinterface 323 3 0
/*     */     //   54: aload 6
/*     */     //   56: invokeinterface 329 1 0
/*     */     //   61: astore_2
/*     */     //   62: goto +75 -> 137
/*     */     //   65: aload_0
/*     */     //   66: getfield 161	com/americanexpress/ips/gfsg/connection/PropertyReader:clientprops	Ljava/util/Properties;
/*     */     //   69: ldc_w 333
/*     */     //   72: aload_2
/*     */     //   73: iconst_1
/*     */     //   74: invokeinterface 335 2 0
/*     */     //   79: invokevirtual 341	java/util/Properties:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/*     */     //   82: pop
/*     */     //   83: aload_0
/*     */     //   84: getfield 161	com/americanexpress/ips/gfsg/connection/PropertyReader:clientprops	Ljava/util/Properties;
/*     */     //   87: ldc_w 345
/*     */     //   90: aload_2
/*     */     //   91: iconst_2
/*     */     //   92: invokeinterface 335 2 0
/*     */     //   97: invokevirtual 341	java/util/Properties:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/*     */     //   100: pop
/*     */     //   101: aload_0
/*     */     //   102: getfield 161	com/americanexpress/ips/gfsg/connection/PropertyReader:clientprops	Ljava/util/Properties;
/*     */     //   105: ldc_w 347
/*     */     //   108: aload_2
/*     */     //   109: iconst_3
/*     */     //   110: invokeinterface 335 2 0
/*     */     //   115: invokevirtual 341	java/util/Properties:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/*     */     //   118: pop
/*     */     //   119: aload_0
/*     */     //   120: getfield 161	com/americanexpress/ips/gfsg/connection/PropertyReader:clientprops	Ljava/util/Properties;
/*     */     //   123: ldc_w 349
/*     */     //   126: aload_2
/*     */     //   127: iconst_4
/*     */     //   128: invokeinterface 335 2 0
/*     */     //   133: invokevirtual 341	java/util/Properties:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/*     */     //   136: pop
/*     */     //   137: aload_2
/*     */     //   138: invokeinterface 351 1 0
/*     */     //   143: ifne -78 -> 65
/*     */     //   146: aload_0
/*     */     //   147: aload_0
/*     */     //   148: invokespecial 272	com/americanexpress/ips/gfsg/connection/PropertyReader:loadISOProperties	()Lcom/americanexpress/ips/gfsg/connection/PropertyReader;
/*     */     //   151: putfield 163	com/americanexpress/ips/gfsg/connection/PropertyReader:propertyHolder	Lcom/americanexpress/ips/gfsg/connection/PropertyReader;
/*     */     //   154: goto +46 -> 200
/*     */     //   157: astore_3
/*     */     //   158: aload_2
/*     */     //   159: invokeinterface 355 1 0
/*     */     //   164: aload_1
/*     */     //   165: invokeinterface 356 1 0
/*     */     //   170: goto +47 -> 217
/*     */     //   173: astore 8
/*     */     //   175: goto +42 -> 217
/*     */     //   178: astore 7
/*     */     //   180: aload_2
/*     */     //   181: invokeinterface 355 1 0
/*     */     //   186: aload_1
/*     */     //   187: invokeinterface 356 1 0
/*     */     //   192: goto +5 -> 197
/*     */     //   195: astore 8
/*     */     //   197: aload 7
/*     */     //   199: athrow
/*     */     //   200: aload_2
/*     */     //   201: invokeinterface 355 1 0
/*     */     //   206: aload_1
/*     */     //   207: invokeinterface 356 1 0
/*     */     //   212: goto +5 -> 217
/*     */     //   215: astore 8
/*     */     //   217: aload_0
/*     */     //   218: getfield 163	com/americanexpress/ips/gfsg/connection/PropertyReader:propertyHolder	Lcom/americanexpress/ips/gfsg/connection/PropertyReader;
/*     */     //   221: areturn
/*     */     // Line number table:
/*     */     //   Java source line #176	-> byte code offset #0
/*     */     //   Java source line #177	-> byte code offset #2
/*     */     //   Java source line #180	-> byte code offset #4
/*     */     //   Java source line #182	-> byte code offset #6
/*     */     //   Java source line #184	-> byte code offset #13
/*     */     //   Java source line #186	-> byte code offset #18
/*     */     //   Java source line #188	-> byte code offset #30
/*     */     //   Java source line #190	-> byte code offset #35
/*     */     //   Java source line #191	-> byte code offset #45
/*     */     //   Java source line #193	-> byte code offset #54
/*     */     //   Java source line #196	-> byte code offset #62
/*     */     //   Java source line #198	-> byte code offset #65
/*     */     //   Java source line #199	-> byte code offset #83
/*     */     //   Java source line #200	-> byte code offset #101
/*     */     //   Java source line #201	-> byte code offset #119
/*     */     //   Java source line #196	-> byte code offset #137
/*     */     //   Java source line #204	-> byte code offset #146
/*     */     //   Java source line #207	-> byte code offset #157
/*     */     //   Java source line #214	-> byte code offset #158
/*     */     //   Java source line #215	-> byte code offset #164
/*     */     //   Java source line #217	-> byte code offset #173
/*     */     //   Java source line #212	-> byte code offset #178
/*     */     //   Java source line #214	-> byte code offset #180
/*     */     //   Java source line #215	-> byte code offset #186
/*     */     //   Java source line #217	-> byte code offset #195
/*     */     //   Java source line #220	-> byte code offset #197
/*     */     //   Java source line #214	-> byte code offset #200
/*     */     //   Java source line #215	-> byte code offset #206
/*     */     //   Java source line #217	-> byte code offset #215
/*     */     //   Java source line #221	-> byte code offset #217
/*     */     // Local variable table:
/*     */     //   start	length	slot	name	signature
/*     */     //   0	222	0	this	PropertyReader
/*     */     //   1	206	1	connection	java.sql.Connection
/*     */     //   3	198	2	resultSet	java.sql.ResultSet
/*     */     //   5	44	3	proxyid	int
/*     */     //   157	1	3	localException	Exception
/*     */     //   16	3	4	url	String
/*     */     //   33	4	5	selectStatement	String
/*     */     //   43	12	6	doSelect	java.sql.PreparedStatement
/*     */     //   178	20	7	localObject	Object
/*     */     //   173	1	8	localSQLException	java.sql.SQLException
/*     */     //   195	1	8	localSQLException1	java.sql.SQLException
/*     */     //   215	1	8	localSQLException2	java.sql.SQLException
/*     */     // Exception table:
/*     */     //   from	to	target	type
/*     */     //   4	154	157	java/lang/Exception
/*     */     //   158	170	173	java/sql/SQLException
/*     */     //   4	158	178	finally
/*     */     //   180	192	195	java/sql/SQLException
/*     */     //   200	212	215	java/sql/SQLException
/*     */   }
/*     */   
/*     */   /* Error */
/*     */   public PropertyReader loadPropFileFromSDkJar()
/*     */     throws java.lang.Exception
/*     */   {
/*     */     // Byte code:
/*     */     //   0: aconst_null
/*     */     //   1: astore_1
/*     */     //   2: new 369	java/io/FileInputStream
/*     */     //   5: dup
/*     */     //   6: getstatic 123	com/americanexpress/ips/gfsg/connection/PropertyReader:propsLocation	Ljava/lang/String;
/*     */     //   9: invokespecial 371	java/io/FileInputStream:<init>	(Ljava/lang/String;)V
/*     */     //   12: astore_1
/*     */     //   13: aload_0
/*     */     //   14: getfield 161	com/americanexpress/ips/gfsg/connection/PropertyReader:clientprops	Ljava/util/Properties;
/*     */     //   17: aload_1
/*     */     //   18: invokevirtual 260	java/util/Properties:load	(Ljava/io/InputStream;)V
/*     */     //   21: aload_0
/*     */     //   22: aload_0
/*     */     //   23: invokespecial 272	com/americanexpress/ips/gfsg/connection/PropertyReader:loadISOProperties	()Lcom/americanexpress/ips/gfsg/connection/PropertyReader;
/*     */     //   26: putfield 163	com/americanexpress/ips/gfsg/connection/PropertyReader:propertyHolder	Lcom/americanexpress/ips/gfsg/connection/PropertyReader;
/*     */     //   29: goto +18 -> 47
/*     */     //   32: astore_2
/*     */     //   33: aload_1
/*     */     //   34: invokevirtual 373	java/io/FileInputStream:close	()V
/*     */     //   37: goto +14 -> 51
/*     */     //   40: astore_3
/*     */     //   41: aload_1
/*     */     //   42: invokevirtual 373	java/io/FileInputStream:close	()V
/*     */     //   45: aload_3
/*     */     //   46: athrow
/*     */     //   47: aload_1
/*     */     //   48: invokevirtual 373	java/io/FileInputStream:close	()V
/*     */     //   51: aload_0
/*     */     //   52: getfield 163	com/americanexpress/ips/gfsg/connection/PropertyReader:propertyHolder	Lcom/americanexpress/ips/gfsg/connection/PropertyReader;
/*     */     //   55: areturn
/*     */     // Line number table:
/*     */     //   Java source line #226	-> byte code offset #0
/*     */     //   Java source line #228	-> byte code offset #2
/*     */     //   Java source line #229	-> byte code offset #13
/*     */     //   Java source line #230	-> byte code offset #21
/*     */     //   Java source line #233	-> byte code offset #32
/*     */     //   Java source line #238	-> byte code offset #33
/*     */     //   Java source line #237	-> byte code offset #40
/*     */     //   Java source line #238	-> byte code offset #41
/*     */     //   Java source line #239	-> byte code offset #45
/*     */     //   Java source line #238	-> byte code offset #47
/*     */     //   Java source line #240	-> byte code offset #51
/*     */     // Local variable table:
/*     */     //   start	length	slot	name	signature
/*     */     //   0	56	0	this	PropertyReader
/*     */     //   1	47	1	fis	java.io.FileInputStream
/*     */     //   32	1	2	localException	Exception
/*     */     //   40	6	3	localObject	Object
/*     */     // Exception table:
/*     */     //   from	to	target	type
/*     */     //   2	29	32	java/lang/Exception
/*     */     //   2	33	40	finally
/*     */   }
/*     */   
/*     */   private PropertyReader loadXMLProperties()
/*     */   {
/* 246 */     this.propertyHolder = new PropertyReader();
/*     */     
/*     */ 
/* 249 */     setXmlProxyPort(this.clientprops.getProperty("PROXY_PORT"));
/* 250 */     setXmlProxyUserid(this.clientprops.getProperty("PROXY_USER_ID"));
/* 251 */     setXmlProxyPassword(this.clientprops.getProperty("PROXY_PASSWORD"));
/* 252 */     setXmlhttpsUrl(this.clientprops.getProperty("HTTPS_URL"));
/* 253 */     setXmlProxyAddress(this.clientprops.getProperty("PROXY_ADDRESS"));
/* 254 */     setXmlProxyAuthentication(this.clientprops.getProperty("PROXY_AUTHENTICATION"));
/* 255 */     setXmlProxy(this.clientprops.getProperty("PROXY"));
/* 256 */     setMerchantNumber(this.clientprops.getProperty("MERCHANT_NUMBER"));
/* 257 */     setOrigin(this.clientprops.getProperty("ORIGIN"));
/* 258 */     setRegion(this.clientprops.getProperty("REGION"));
/*     */     
/* 260 */     setMerchantCountry(this.clientprops.getProperty("MERCHANT_COUNTRY"));
/* 261 */     setHttpsPort(new Integer(this.clientprops.getProperty("HTTPS_PORT")).intValue());
/*     */     
/* 263 */     return this.propertyHolder;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static String getMerchantNumber()
/*     */   {
/* 270 */     return merchantNumber;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static void setMerchantNumber(String merchantNumber)
/*     */   {
/* 277 */     merchantNumber = merchantNumber;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static String getDebuggerFlag()
/*     */   {
/* 284 */     return debuggerFlag;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static void setDebuggerFlag(String debuggerFlag)
/*     */   {
/* 291 */     debuggerFlag = debuggerFlag;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static String getOrigin()
/*     */   {
/* 298 */     return origin;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static void setOrigin(String origin)
/*     */   {
/* 305 */     origin = origin;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static String getRegion()
/*     */   {
/* 312 */     return region;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static void setRegion(String region)
/*     */   {
/* 319 */     region = region;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static String getMethodType()
/*     */   {
/* 326 */     return methodType;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static void setMethodType(String methodType)
/*     */   {
/* 333 */     methodType = methodType;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static String getRoutingIndicator()
/*     */   {
/* 340 */     return routingIndicator;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static void setRoutingIndicator(String routingIndicator)
/*     */   {
/* 347 */     routingIndicator = routingIndicator;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static String getMerchantCountry()
/*     */   {
/* 354 */     return merchantCountry;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static void setMerchantCountry(String merchantCountry)
/*     */   {
/* 361 */     merchantCountry = merchantCountry;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static String getFtpPort()
/*     */   {
/* 368 */     return ftpPort;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static void setFtpPort(String ftpPort)
/*     */   {
/* 375 */     ftpPort = ftpPort;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static String getFtpUserID()
/*     */   {
/* 382 */     return ftpUserID;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static void setFtpUserID(String ftpUserID)
/*     */   {
/* 389 */     ftpUserID = ftpUserID;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static String getFtpPassword()
/*     */   {
/* 396 */     return ftpPassword;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static void setFtpPassword(String ftpPassword)
/*     */   {
/* 403 */     ftpPassword = ftpPassword;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static String getFtpURL()
/*     */   {
/* 410 */     return ftpURL;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static void setFtpURL(String ftpURL)
/*     */   {
/* 417 */     ftpURL = ftpURL;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static String getFtpLocalUploadFilePath()
/*     */   {
/* 424 */     return ftpLocalUploadFilePath;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static void setFtpLocalUploadFilePath(String ftpLocalUploadFilePath)
/*     */   {
/* 431 */     ftpLocalUploadFilePath = ftpLocalUploadFilePath;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static String getFtpLocalDownloadFilePath()
/*     */   {
/* 438 */     return ftpLocalDownloadFilePath;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static void setFtpLocalDownloadFilePath(String ftpLocalDownloadFilePath)
/*     */   {
/* 445 */     ftpLocalDownloadFilePath = ftpLocalDownloadFilePath;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static String getFtpUploadFileName()
/*     */   {
/* 452 */     return ftpUploadFileName;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static void setFtpUploadFileName(String ftpUploadFileName)
/*     */   {
/* 459 */     ftpUploadFileName = ftpUploadFileName;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static String getFtpDownloadAcknowledgeFileName()
/*     */   {
/* 466 */     return ftpDownloadAcknowledgeFileName;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void setFtpDownloadAcknowledgeFileName(String ftpDownloadAcknowledgeFileName)
/*     */   {
/* 474 */     ftpDownloadAcknowledgeFileName = ftpDownloadAcknowledgeFileName;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static String getFtpDownloadPrintedFileName()
/*     */   {
/* 481 */     return ftpDownloadPrintedFileName;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void setFtpDownloadPrintedFileName(String ftpDownloadPrintedFileName)
/*     */   {
/* 489 */     ftpDownloadPrintedFileName = ftpDownloadPrintedFileName;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static String getFtpDownloadRawFileName()
/*     */   {
/* 496 */     return ftpDownloadRawFileName;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static void setFtpDownloadRawFileName(String ftpDownloadRawFileName)
/*     */   {
/* 503 */     ftpDownloadRawFileName = ftpDownloadRawFileName;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static String getPropsLocation()
/*     */   {
/* 510 */     return propsLocation;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static void setPropsLocation(String propsLocation)
/*     */   {
/* 517 */     propsLocation = propsLocation;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static String getXmlProxyAddress()
/*     */   {
/* 526 */     return xmlProxyAddress;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static void setXmlProxyAddress(String xmlProxyAddress)
/*     */   {
/* 533 */     xmlProxyAddress = xmlProxyAddress;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static String getFileStorage()
/*     */   {
/* 540 */     return fileStorage;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static void setFileStorage(String fileStorage)
/*     */   {
/* 547 */     fileStorage = fileStorage;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public static String getFileType()
/*     */   {
/* 555 */     return fileType;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static void setFileType(String fileType)
/*     */   {
/* 562 */     fileType = fileType;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static String getSubmissionType()
/*     */   {
/* 569 */     return submissionType;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static void setSubmissionType(String submissionType)
/*     */   {
/* 576 */     submissionType = submissionType;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static String getAcknowledgeType()
/*     */   {
/* 583 */     return acknowledgeType;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static void setAcknowledgeType(String acknowledgeType)
/*     */   {
/* 590 */     acknowledgeType = acknowledgeType;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static String getXmlProxyPort()
/*     */   {
/* 597 */     return xmlProxyPort;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static void setXmlProxyPort(String xmlProxyPort)
/*     */   {
/* 604 */     xmlProxyPort = xmlProxyPort;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static String getXmlProxyUserid()
/*     */   {
/* 611 */     return xmlProxyUserid;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static void setXmlProxyUserid(String xmlProxyUserid)
/*     */   {
/* 618 */     xmlProxyUserid = xmlProxyUserid;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static String getXmlProxyPassword()
/*     */   {
/* 625 */     return xmlProxyPassword;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static void setXmlProxyPassword(String xmlProxyPassword)
/*     */   {
/* 632 */     xmlProxyPassword = xmlProxyPassword;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static String getXmlhttpsUrl()
/*     */   {
/* 639 */     return xmlhttpsUrl;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static void setXmlhttpsUrl(String xmlhttpsUrl)
/*     */   {
/* 646 */     xmlhttpsUrl = xmlhttpsUrl;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static String getXmlProxyAuthentication()
/*     */   {
/* 653 */     return xmlProxyAuthentication;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static void setXmlProxyAuthentication(String xmlProxyAuthentication)
/*     */   {
/* 660 */     xmlProxyAuthentication = xmlProxyAuthentication;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public int getHttpsPort()
/*     */   {
/* 667 */     return httpsPort;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void setHttpsPort(int httpsPort)
/*     */   {
/* 675 */     httpsPort = httpsPort;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\connection\PropertyReader.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */