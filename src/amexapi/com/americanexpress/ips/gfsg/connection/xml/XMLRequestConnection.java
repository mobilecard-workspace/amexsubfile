package com.americanexpress.ips.gfsg.connection.xml;

import com.americanexpress.ips.gfsg.connection.PropertyReader;
import com.americanexpress.ips.gfsg.exceptions.SSLConnectionException;
import com.americanexpress.ips.gfsg.exceptions.SettlementException;
import com.americanexpress.ips.gfsg.validator.CommonValidator;
import java.io.IOException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

public class XMLRequestConnection {
	private String endPointUrl = null;
	private Properties requestProp = null;
	private Properties inputProp = null;
	private URL callingURL = null;
	private String payload = null;
	private HttpsURLConnection httpsconn = null;
	StringBuffer serviceResponseString = new StringBuffer();
	private String serverRespCode = null;
	boolean debugFlag = false;
	private static final Logger LOGGER = Logger.getLogger(XMLRequestConnection.class);
	private static Map<String, List<String>> responseHdrKeys = null;

	public final void createHTTPsConnection(PropertyReader propertyReader) throws SettlementException, SSLConnectionException {
		if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
			this.debugFlag = true;
			LOGGER.info("Entered into createHTTPsConnection()");
		}

		try {
			this.endPointUrl = PropertyReader.getXmlhttpsUrl();
			if (this.debugFlag) {
				LOGGER.info("End Point URL:" + this.endPointUrl);
			}
			this.callingURL = new URL(this.endPointUrl);
			trustAllHttpsCertificates();

			if (this.debugFlag) {
				LOGGER.info("Sending request through proxy :");
			}

			System.getProperties().put("proxySet", "true");

			System.getProperties().put("proxyHost", PropertyReader.getXmlProxyAddress());
			System.getProperties().put("proxyPort", PropertyReader.getXmlProxyPort());

			this.httpsconn = ((HttpsURLConnection) this.callingURL.openConnection());

			if (this.debugFlag) {
				LOGGER.info("Httpsconn is Created");
			}

			if (this.debugFlag) {
				LOGGER.info("Setting the User-Agent,Host,Proxy-Authorization");
			}
			this.httpsconn.setRequestProperty("User-Agent", "Terminal1");

			this.httpsconn.setRequestProperty("Host", PropertyReader.getXmlProxyPort());

			String authentication = "Basic "
					+ new String(Base64.encodeBase64(new StringBuilder(String.valueOf(PropertyReader.getXmlProxyUserid())).append(":").append(PropertyReader.getXmlProxyPassword()).toString()
							.getBytes()));

			this.httpsconn.setRequestProperty("Proxy-Authorization", authentication);
			if (this.debugFlag) {
				LOGGER.info("Proxy-Authorization");
			}
		} catch (IOException ioex) {
			if (this.debugFlag) {
				LOGGER.fatal("Error in createHTTPsConnection() method.. EXCEPITON : " + ioex.getMessage());
			}
			throw new SSLConnectionException(ioex.getMessage(), ioex);
		} catch (Exception ex) {
			if (this.debugFlag) {
				LOGGER.fatal("Error in createHTTPsConnection() method..  EXCEPITON : " + ex);
			}
			throw new SettlementException(ex.getMessage(), ex);
		}
	}

	/* Error */
	public final String sendHttpsRequest(String xmlReqString, String requestType, PropertyReader propertyReader) throws IOException, SettlementException, SSLConnectionException {
		// Byte code:
		// 0: invokestatic 74
		// com/americanexpress/ips/gfsg/connection/PropertyReader:getDebuggerFlag
		// ()Ljava/lang/String;
		// 3: ldc 80
		// 5: invokevirtual 82 java/lang/String:equalsIgnoreCase
		// (Ljava/lang/String;)Z
		// 8: ifeq +16 -> 24
		// 11: aload_0
		// 12: iconst_1
		// 13: putfield 63
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:debugFlag
		// Z
		// 16: getstatic 35
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:LOGGER
		// Lorg/apache/log4j/Logger;
		// 19: ldc -31
		// 21: invokevirtual 90 org/apache/log4j/Logger:info
		// (Ljava/lang/Object;)V
		// 24: aconst_null
		// 25: astore 4
		// 27: aload_0
		// 28: getfield 54
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:httpsconn
		// Ljavax/net/ssl/HttpsURLConnection;
		// 31: iconst_1
		// 32: invokevirtual 227 javax/net/ssl/HttpsURLConnection:setDoInput
		// (Z)V
		// 35: aload_0
		// 36: getfield 54
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:httpsconn
		// Ljavax/net/ssl/HttpsURLConnection;
		// 39: iconst_1
		// 40: invokevirtual 231 javax/net/ssl/HttpsURLConnection:setDoOutput
		// (Z)V
		// 43: aload_0
		// 44: new 130 java/util/Properties
		// 47: dup
		// 48: invokespecial 234 java/util/Properties:<init> ()V
		// 51: putfield 46
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:requestProp
		// Ljava/util/Properties;
		// 54: aload_0
		// 55: getfield 63
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:debugFlag
		// Z
		// 58: ifeq +11 -> 69
		// 61: getstatic 35
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:LOGGER
		// Lorg/apache/log4j/Logger;
		// 64: ldc -21
		// 66: invokevirtual 90 org/apache/log4j/Logger:info
		// (Ljava/lang/Object;)V
		// 69: aload_0
		// 70: getfield 46
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:requestProp
		// Ljava/util/Properties;
		// 73: ldc -19
		// 75: ldc -17
		// 77: invokevirtual 241 java/util/Properties:setProperty
		// (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;
		// 80: pop
		// 81: aload_0
		// 82: getfield 46
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:requestProp
		// Ljava/util/Properties;
		// 85: ldc -11
		// 87: invokestatic 247
		// com/americanexpress/ips/gfsg/connection/PropertyReader:getRegion
		// ()Ljava/lang/String;
		// 90: invokevirtual 241 java/util/Properties:setProperty
		// (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;
		// 93: pop
		// 94: aload_0
		// 95: getfield 46
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:requestProp
		// Ljava/util/Properties;
		// 98: ldc -6
		// 100: invokestatic 252
		// com/americanexpress/ips/gfsg/connection/PropertyReader:getMerchantCountry
		// ()Ljava/lang/String;
		// 103: invokevirtual 241 java/util/Properties:setProperty
		// (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;
		// 106: pop
		// 107: aload_0
		// 108: getfield 46
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:requestProp
		// Ljava/util/Properties;
		// 111: ldc -1
		// 113: aload_2
		// 114: invokevirtual 241 java/util/Properties:setProperty
		// (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;
		// 117: pop
		// 118: aload_0
		// 119: getfield 46
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:requestProp
		// Ljava/util/Properties;
		// 122: ldc_w 257
		// 125: invokestatic 259
		// com/americanexpress/ips/gfsg/connection/PropertyReader:getMerchantNumber
		// ()Ljava/lang/String;
		// 128: invokevirtual 241 java/util/Properties:setProperty
		// (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;
		// 131: pop
		// 132: aload_0
		// 133: getfield 46
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:requestProp
		// Ljava/util/Properties;
		// 136: ldc_w 262
		// 139: invokestatic 264
		// com/americanexpress/ips/gfsg/connection/PropertyReader:getRoutingIndicator
		// ()Ljava/lang/String;
		// 142: invokevirtual 241 java/util/Properties:setProperty
		// (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;
		// 145: pop
		// 146: aload_0
		// 147: getfield 46
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:requestProp
		// Ljava/util/Properties;
		// 150: ldc_w 267
		// 153: invokestatic 269
		// com/americanexpress/ips/gfsg/connection/PropertyReader:getOrigin
		// ()Ljava/lang/String;
		// 156: invokevirtual 241 java/util/Properties:setProperty
		// (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;
		// 159: pop
		// 160: aload_0
		// 161: new 130 java/util/Properties
		// 164: dup
		// 165: invokespecial 234 java/util/Properties:<init> ()V
		// 168: putfield 48
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:inputProp
		// Ljava/util/Properties;
		// 171: aload_1
		// 172: ifnull +8 -> 180
		// 175: aload_0
		// 176: aload_1
		// 177: putfield 52
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:payload
		// Ljava/lang/String;
		// 180: aload_0
		// 181: getfield 52
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:payload
		// Ljava/lang/String;
		// 184: ifnull +21 -> 205
		// 187: aload_0
		// 188: getfield 48
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:inputProp
		// Ljava/util/Properties;
		// 191: ldc_w 272
		// 194: aload_0
		// 195: getfield 52
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:payload
		// Ljava/lang/String;
		// 198: invokevirtual 274 java/lang/String:trim ()Ljava/lang/String;
		// 201: invokevirtual 241 java/util/Properties:setProperty
		// (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;
		// 204: pop
		// 205: ldc_w 277
		// 208: astore 5
		// 210: aload_0
		// 211: getfield 48
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:inputProp
		// Ljava/util/Properties;
		// 214: ifnull +13 -> 227
		// 217: aload_0
		// 218: aload_0
		// 219: getfield 48
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:inputProp
		// Ljava/util/Properties;
		// 222: invokespecial 279
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:toEncodedString
		// (Ljava/util/Properties;)Ljava/lang/String;
		// 225: astore 5
		// 227: aload_0
		// 228: getfield 54
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:httpsconn
		// Ljavax/net/ssl/HttpsURLConnection;
		// 231: ifnull +564 -> 795
		// 234: aload_0
		// 235: getfield 46
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:requestProp
		// Ljava/util/Properties;
		// 238: ifnull +100 -> 338
		// 241: aload_0
		// 242: getfield 46
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:requestProp
		// Ljava/util/Properties;
		// 245: invokevirtual 283 java/util/Properties:propertyNames
		// ()Ljava/util/Enumeration;
		// 248: astore 6
		// 250: goto +78 -> 328
		// 253: aload 6
		// 255: invokeinterface 287 1 0
		// 260: checkcast 83 java/lang/String
		// 263: astore 7
		// 265: aload_0
		// 266: getfield 46
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:requestProp
		// Ljava/util/Properties;
		// 269: aload 7
		// 271: invokevirtual 293 java/util/Properties:getProperty
		// (Ljava/lang/String;)Ljava/lang/String;
		// 274: astore 8
		// 276: getstatic 35
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:LOGGER
		// Lorg/apache/log4j/Logger;
		// 279: new 97 java/lang/StringBuilder
		// 282: dup
		// 283: ldc_w 297
		// 286: invokespecial 101 java/lang/StringBuilder:<init>
		// (Ljava/lang/String;)V
		// 289: aload 7
		// 291: invokevirtual 104 java/lang/StringBuilder:append
		// (Ljava/lang/String;)Ljava/lang/StringBuilder;
		// 294: ldc_w 299
		// 297: invokevirtual 104 java/lang/StringBuilder:append
		// (Ljava/lang/String;)Ljava/lang/StringBuilder;
		// 300: ldc_w 301
		// 303: invokevirtual 104 java/lang/StringBuilder:append
		// (Ljava/lang/String;)Ljava/lang/StringBuilder;
		// 306: aload 8
		// 308: invokevirtual 104 java/lang/StringBuilder:append
		// (Ljava/lang/String;)Ljava/lang/StringBuilder;
		// 311: invokevirtual 108 java/lang/StringBuilder:toString
		// ()Ljava/lang/String;
		// 314: invokevirtual 90 org/apache/log4j/Logger:info
		// (Ljava/lang/Object;)V
		// 317: aload_0
		// 318: getfield 54
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:httpsconn
		// Ljavax/net/ssl/HttpsURLConnection;
		// 321: aload 7
		// 323: aload 8
		// 325: invokevirtual 159
		// javax/net/ssl/HttpsURLConnection:setRequestProperty
		// (Ljava/lang/String;Ljava/lang/String;)V
		// 328: aload 6
		// 330: invokeinterface 303 1 0
		// 335: ifne -82 -> 253
		// 338: aload_0
		// 339: getfield 54
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:httpsconn
		// Ljavax/net/ssl/HttpsURLConnection;
		// 342: iconst_0
		// 343: invokevirtual 307 javax/net/ssl/HttpsURLConnection:setUseCaches
		// (Z)V
		// 346: new 310 java DataOutputStream
		// 349: dup
		// 350: aload_0
		// 351: getfield 54
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:httpsconn
		// Ljavax/net/ssl/HttpsURLConnection;
		// 354: invokevirtual 312
		// javax/net/ssl/HttpsURLConnection:getOutputStream ()Ljava
		// OutputStream;
		// 357: invokespecial 316 java DataOutputStream:<init> (Ljava
		// OutputStream;)V
		// 360: astore 4
		// 362: aload 4
		// 364: aload 5
		// 366: invokevirtual 319 java DataOutputStream:writeBytes
		// (Ljava/lang/String;)V
		// 369: aload 4
		// 371: invokevirtual 322 java DataOutputStream:flush ()V
		// 374: aload_0
		// 375: getfield 54
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:httpsconn
		// Ljavax/net/ssl/HttpsURLConnection;
		// 378: invokevirtual 325
		// javax/net/ssl/HttpsURLConnection:getHeaderFields ()Ljava/util/Map;
		// 381: astore 6
		// 383: aload 6
		// 385: invokeinterface 329 1 0
		// 390: astore 7
		// 392: aload 7
		// 394: invokeinterface 335 1 0
		// 399: astore 9
		// 401: goto +57 -> 458
		// 404: aload 9
		// 406: invokeinterface 341 1 0
		// 411: checkcast 83 java/lang/String
		// 414: astore 8
		// 416: getstatic 35
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:LOGGER
		// Lorg/apache/log4j/Logger;
		// 419: new 97 java/lang/StringBuilder
		// 422: dup
		// 423: ldc_w 346
		// 426: invokespecial 101 java/lang/StringBuilder:<init>
		// (Ljava/lang/String;)V
		// 429: aload 8
		// 431: invokevirtual 104 java/lang/StringBuilder:append
		// (Ljava/lang/String;)Ljava/lang/StringBuilder;
		// 434: ldc_w 348
		// 437: invokevirtual 104 java/lang/StringBuilder:append
		// (Ljava/lang/String;)Ljava/lang/StringBuilder;
		// 440: aload 6
		// 442: aload 8
		// 444: invokeinterface 350 2 0
		// 449: invokevirtual 209 java/lang/StringBuilder:append
		// (Ljava/lang/Object;)Ljava/lang/StringBuilder;
		// 452: invokevirtual 108 java/lang/StringBuilder:toString
		// ()Ljava/lang/String;
		// 455: invokevirtual 90 org/apache/log4j/Logger:info
		// (Ljava/lang/Object;)V
		// 458: aload 9
		// 460: invokeinterface 354 1 0
		// 465: ifne -61 -> 404
		// 468: new 357 java BufferedReader
		// 471: dup
		// 472: new 359 java InputStreamReader
		// 475: dup
		// 476: aload_0
		// 477: getfield 54
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:httpsconn
		// Ljavax/net/ssl/HttpsURLConnection;
		// 480: invokevirtual 361
		// javax/net/ssl/HttpsURLConnection:getInputStream ()Ljava InputStream;
		// 483: invokespecial 365 java InputStreamReader:<init> (Ljava
		// InputStream;)V
		// 486: invokespecial 368 java BufferedReader:<init> (Ljava Reader;)V
		// 489: astore 9
		// 491: goto +13 -> 504
		// 494: aload_0
		// 495: getfield 59
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:serviceResponseString
		// Ljava/lang/StringBuffer;
		// 498: aload 8
		// 500: invokevirtual 371 java/lang/StringBuffer:append
		// (Ljava/lang/String;)Ljava/lang/StringBuffer;
		// 503: pop
		// 504: aload 9
		// 506: invokevirtual 374 java BufferedReader:readLine
		// ()Ljava/lang/String;
		// 509: dup
		// 510: astore 8
		// 512: ifnonnull -18 -> 494
		// 515: aload_0
		// 516: getfield 54
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:httpsconn
		// Ljavax/net/ssl/HttpsURLConnection;
		// 519: invokevirtual 325
		// javax/net/ssl/HttpsURLConnection:getHeaderFields ()Ljava/util/Map;
		// 522: astore 10
		// 524: aload 10
		// 526: invokeinterface 329 1 0
		// 531: astore 11
		// 533: aload 11
		// 535: invokeinterface 335 1 0
		// 540: astore 13
		// 542: goto +57 -> 599
		// 545: aload 13
		// 547: invokeinterface 341 1 0
		// 552: checkcast 83 java/lang/String
		// 555: astore 12
		// 557: getstatic 35
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:LOGGER
		// Lorg/apache/log4j/Logger;
		// 560: new 97 java/lang/StringBuilder
		// 563: dup
		// 564: ldc_w 377
		// 567: invokespecial 101 java/lang/StringBuilder:<init>
		// (Ljava/lang/String;)V
		// 570: aload 12
		// 572: invokevirtual 104 java/lang/StringBuilder:append
		// (Ljava/lang/String;)Ljava/lang/StringBuilder;
		// 575: ldc_w 348
		// 578: invokevirtual 104 java/lang/StringBuilder:append
		// (Ljava/lang/String;)Ljava/lang/StringBuilder;
		// 581: aload 10
		// 583: aload 12
		// 585: invokeinterface 350 2 0
		// 590: invokevirtual 209 java/lang/StringBuilder:append
		// (Ljava/lang/Object;)Ljava/lang/StringBuilder;
		// 593: invokevirtual 108 java/lang/StringBuilder:toString
		// ()Ljava/lang/String;
		// 596: invokevirtual 90 org/apache/log4j/Logger:info
		// (Ljava/lang/Object;)V
		// 599: aload 13
		// 601: invokeinterface 354 1 0
		// 606: ifne -61 -> 545
		// 609: getstatic 35
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:LOGGER
		// Lorg/apache/log4j/Logger;
		// 612: new 97 java/lang/StringBuilder
		// 615: dup
		// 616: ldc_w 379
		// 619: invokespecial 101 java/lang/StringBuilder:<init>
		// (Ljava/lang/String;)V
		// 622: aload 10
		// 624: invokevirtual 209 java/lang/StringBuilder:append
		// (Ljava/lang/Object;)Ljava/lang/StringBuilder;
		// 627: invokevirtual 108 java/lang/StringBuilder:toString
		// ()Ljava/lang/String;
		// 630: invokevirtual 90 org/apache/log4j/Logger:info
		// (Ljava/lang/Object;)V
		// 633: aload_0
		// 634: getfield 54
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:httpsconn
		// Ljavax/net/ssl/HttpsURLConnection;
		// 637: invokevirtual 325
		// javax/net/ssl/HttpsURLConnection:getHeaderFields ()Ljava/util/Map;
		// 640: putstatic 37
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:responseHdrKeys
		// Ljava/util/Map;
		// 643: goto +152 -> 795
		// 646: astore 5
		// 648: aload_0
		// 649: getfield 63
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:debugFlag
		// Z
		// 652: ifeq +27 -> 679
		// 655: getstatic 35
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:LOGGER
		// Lorg/apache/log4j/Logger;
		// 658: new 97 java/lang/StringBuilder
		// 661: dup
		// 662: ldc_w 381
		// 665: invokespecial 101 java/lang/StringBuilder:<init>
		// (Ljava/lang/String;)V
		// 668: aload 5
		// 670: invokevirtual 209 java/lang/StringBuilder:append
		// (Ljava/lang/Object;)Ljava/lang/StringBuilder;
		// 673: invokevirtual 108 java/lang/StringBuilder:toString
		// ()Ljava/lang/String;
		// 676: invokevirtual 201 org/apache/log4j/Logger:fatal
		// (Ljava/lang/Object;)V
		// 679: aload 4
		// 681: invokevirtual 383 java DataOutputStream:close ()V
		// 684: goto +116 -> 800
		// 687: astore 5
		// 689: aload_0
		// 690: getfield 54
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:httpsconn
		// Ljavax/net/ssl/HttpsURLConnection;
		// 693: ifnull +28 -> 721
		// 696: aload_0
		// 697: getfield 54
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:httpsconn
		// Ljavax/net/ssl/HttpsURLConnection;
		// 700: invokevirtual 386
		// javax/net/ssl/HttpsURLConnection:getResponseCode ()I
		// 703: iconst_m1
		// 704: if_icmple +17 -> 721
		// 707: aload_0
		// 708: aload_0
		// 709: getfield 54
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:httpsconn
		// Ljavax/net/ssl/HttpsURLConnection;
		// 712: invokevirtual 386
		// javax/net/ssl/HttpsURLConnection:getResponseCode ()I
		// 715: invokestatic 390 java/lang/Integer:toString
		// (I)Ljava/lang/String;
		// 718: putfield 61
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:serverRespCode
		// Ljava/lang/String;
		// 721: aload_0
		// 722: getfield 63
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:debugFlag
		// Z
		// 725: ifeq +53 -> 778
		// 728: getstatic 35
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:LOGGER
		// Lorg/apache/log4j/Logger;
		// 731: new 97 java/lang/StringBuilder
		// 734: dup
		// 735: ldc_w 395
		// 738: invokespecial 101 java/lang/StringBuilder:<init>
		// (Ljava/lang/String;)V
		// 741: aload 5
		// 743: invokevirtual 209 java/lang/StringBuilder:append
		// (Ljava/lang/Object;)Ljava/lang/StringBuilder;
		// 746: invokevirtual 108 java/lang/StringBuilder:toString
		// ()Ljava/lang/String;
		// 749: invokevirtual 201 org/apache/log4j/Logger:fatal
		// (Ljava/lang/Object;)V
		// 752: getstatic 35
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:LOGGER
		// Lorg/apache/log4j/Logger;
		// 755: new 97 java/lang/StringBuilder
		// 758: dup
		// 759: ldc_w 397
		// 762: invokespecial 101 java/lang/StringBuilder:<init>
		// (Ljava/lang/String;)V
		// 765: aload_0
		// 766: getfield 61
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:serverRespCode
		// Ljava/lang/String;
		// 769: invokevirtual 104 java/lang/StringBuilder:append
		// (Ljava/lang/String;)Ljava/lang/StringBuilder;
		// 772: invokevirtual 108 java/lang/StringBuilder:toString
		// ()Ljava/lang/String;
		// 775: invokevirtual 201 org/apache/log4j/Logger:fatal
		// (Ljava/lang/Object;)V
		// 778: aload 4
		// 780: invokevirtual 383 java DataOutputStream:close ()V
		// 783: aconst_null
		// 784: areturn
		// 785: astore 14
		// 787: aload 4
		// 789: invokevirtual 383 java DataOutputStream:close ()V
		// 792: aload 14
		// 794: athrow
		// 795: aload 4
		// 797: invokevirtual 383 java DataOutputStream:close ()V
		// 800: aload_0
		// 801: getfield 63
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:debugFlag
		// Z
		// 804: ifeq +38 -> 842
		// 807: getstatic 35
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:LOGGER
		// Lorg/apache/log4j/Logger;
		// 810: new 97 java/lang/StringBuilder
		// 813: dup
		// 814: ldc_w 399
		// 817: invokespecial 101 java/lang/StringBuilder:<init>
		// (Ljava/lang/String;)V
		// 820: aload_0
		// 821: getfield 59
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:serviceResponseString
		// Ljava/lang/StringBuffer;
		// 824: invokevirtual 209 java/lang/StringBuilder:append
		// (Ljava/lang/Object;)Ljava/lang/StringBuilder;
		// 827: invokevirtual 108 java/lang/StringBuilder:toString
		// ()Ljava/lang/String;
		// 830: invokevirtual 90 org/apache/log4j/Logger:info
		// (Ljava/lang/Object;)V
		// 833: getstatic 35
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:LOGGER
		// Lorg/apache/log4j/Logger;
		// 836: ldc_w 401
		// 839: invokevirtual 90 org/apache/log4j/Logger:info
		// (Ljava/lang/Object;)V
		// 842: aload_0
		// 843: getfield 59
		// com/americanexpress/ips/gfsg/connection/xml/XMLRequestConnection:serviceResponseString
		// Ljava/lang/StringBuffer;
		// 846: invokevirtual 403 java/lang/StringBuffer:toString
		// ()Ljava/lang/String;
		// 849: areturn
		// Line number table:
		// Java source line #167 -> byte code offset #0
		// Java source line #168 -> byte code offset #11
		// Java source line #169 -> byte code offset #16
		// Java source line #171 -> byte code offset #24
		// Java source line #173 -> byte code offset #27
		// Java source line #174 -> byte code offset #35
		// Java source line #175 -> byte code offset #43
		// Java source line #177 -> byte code offset #54
		// Java source line #178 -> byte code offset #61
		// Java source line #181 -> byte code offset #69
		// Java source line #183 -> byte code offset #81
		// Java source line #184 -> byte code offset #94
		// Java source line #185 -> byte code offset #107
		// Java source line #186 -> byte code offset #118
		// Java source line #187 -> byte code offset #132
		// Java source line #188 -> byte code offset #146
		// Java source line #190 -> byte code offset #160
		// Java source line #192 -> byte code offset #171
		// Java source line #193 -> byte code offset #175
		// Java source line #196 -> byte code offset #180
		// Java source line #197 -> byte code offset #187
		// Java source line #199 -> byte code offset #205
		// Java source line #201 -> byte code offset #210
		// Java source line #203 -> byte code offset #217
		// Java source line #205 -> byte code offset #227
		// Java source line #208 -> byte code offset #234
		// Java source line #210 -> byte code offset #241
		// Java source line #211 -> byte code offset #250
		// Java source line #212 -> byte code offset #253
		// Java source line #213 -> byte code offset #265
		// Java source line #214 -> byte code offset #276
		// Java source line #215 -> byte code offset #317
		// Java source line #211 -> byte code offset #328
		// Java source line #224 -> byte code offset #338
		// Java source line #227 -> byte code offset #346
		// Java source line #228 -> byte code offset #362
		// Java source line #229 -> byte code offset #369
		// Java source line #232 -> byte code offset #374
		// Java source line #233 -> byte code offset #383
		// Java source line #234 -> byte code offset #392
		// Java source line #235 -> byte code offset #416
		// Java source line #234 -> byte code offset #458
		// Java source line #239 -> byte code offset #468
		// Java source line #240 -> byte code offset #472
		// Java source line #239 -> byte code offset #486
		// Java source line #242 -> byte code offset #491
		// Java source line #243 -> byte code offset #494
		// Java source line #242 -> byte code offset #504
		// Java source line #247 -> byte code offset #515
		// Java source line #248 -> byte code offset #524
		// Java source line #249 -> byte code offset #533
		// Java source line #250 -> byte code offset #557
		// Java source line #249 -> byte code offset #599
		// Java source line #253 -> byte code offset #609
		// Java source line #255 -> byte code offset #633
		// Java source line #258 -> byte code offset #646
		// Java source line #259 -> byte code offset #648
		// Java source line #260 -> byte code offset #655
		// Java source line #277 -> byte code offset #679
		// Java source line #262 -> byte code offset #687
		// Java source line #263 -> byte code offset #689
		// Java source line #264 -> byte code offset #707
		// Java source line #268 -> byte code offset #721
		// Java source line #269 -> byte code offset #728
		// Java source line #270 -> byte code offset #752
		// Java source line #277 -> byte code offset #778
		// Java source line #273 -> byte code offset #783
		// Java source line #276 -> byte code offset #785
		// Java source line #277 -> byte code offset #787
		// Java source line #278 -> byte code offset #792
		// Java source line #277 -> byte code offset #795
		// Java source line #285 -> byte code offset #800
		// Java source line #286 -> byte code offset #807
		// Java source line #287 -> byte code offset #833
		// Java source line #290 -> byte code offset #842
		// Local variable table:
		// start length slot name signature
		// 0 850 0 this XMLRequestConnection
		// 0 850 1 xmlReqString String
		// 0 850 2 requestType String
		// 0 850 3 propertyReader PropertyReader
		// 25 771 4 out java.io.DataOutputStream
		// 208 157 5 argString String
		// 646 23 5 e IOException
		// 687 55 5 e Exception
		// 248 81 6 names Enumeration<?>
		// 381 60 6 hdrs1 Map<String, List<String>>
		// 263 59 7 name String
		// 390 3 7 hdrKeys1 java.util.Set<String>
		// 274 50 8 value String
		// 414 29 8 k String
		// 494 5 8 strLine String
		// 510 3 8 strLine String
		// 399 60 9 localIterator1 java.util.Iterator
		// 489 16 9 buffReader java.io.BufferedReader
		// 522 101 10 hdrs Map<String, List<String>>
		// 531 3 11 hdrKeys java.util.Set<String>
		// 555 29 12 k String
		// 540 60 13 localIterator2 java.util.Iterator
		// 785 8 14 localObject Object
		// Exception table:
		// from to target type
		// 27 643 646 java IOException
		// 27 643 687 java/lang/Exception
		// 27 679 785 finally
		// 687 778 785 finally
	}

	private void trustAllHttpsCertificates() throws SettlementException, SSLConnectionException {
		try {
			TrustManager[] trustAllCerts = new TrustManager[1];

			TrustManager trusrManager = new miTM();

			trustAllCerts[0] = trusrManager;

			SSLContext sc = SSLContext.getInstance("SSL");

			sc.init(null, trustAllCerts, null);

			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		} catch (NoSuchAlgorithmException e) {
			throw new SSLConnectionException("Error in trustAllHttpsCertificates() method..  Exception:", e);
		} catch (IllegalArgumentException e) {
			throw new SSLConnectionException(e.getMessage(), e);
		} catch (Exception e) {
			if (this.debugFlag) {
				LOGGER.fatal("Error in trustAllHttpsCertificates() method..  Exception:" + e);
			}
			throw new SettlementException(e);
		}
	}

	public static class miTM implements TrustManager, X509TrustManager {
		public X509Certificate[] getAcceptedIssuers() {
			return null;
		}

		public boolean isServerTrusted(X509Certificate[] certs) {
			return true;
		}

		public boolean isClientTrusted(X509Certificate[] certs) {
			return true;
		}

		public void checkServerTrusted(X509Certificate[] certs, String authType) throws CertificateException {
		}

		public void checkClientTrusted(X509Certificate[] certs, String authType) {
		}
	}

	private String toEncodedString(Properties args) {
		StringBuffer buf = new StringBuffer();

		Enumeration<?> names = args.propertyNames();

		while (names.hasMoreElements()) {
			String name = (String) names.nextElement();
			String value = args.getProperty(name);
			buf.append(name);
			buf.append('=');
			buf.append(value);

			if (names.hasMoreElements()) {
				buf.append('&');
			}
		}
		return buf.toString();
	}

	public static String constructWebServiceURL(String ipsURL, int httpPort) {
		StringBuffer gwsURL = new StringBuffer();

		if (!CommonValidator.isNullOrEmpty(ipsURL)) {
			if (!CommonValidator.isNullOrEmpty(new Integer(httpPort).toString())) {
				gwsURL.append(ipsURL.substring(0, 35));
				gwsURL.append(":");
				gwsURL.append(httpPort);
				gwsURL.append(ipsURL.substring(35, ipsURL.length()));
			}
		}

		return gwsURL.toString();
	}

	public static Map<String, List<String>> getResponseHeader() {
		return responseHdrKeys;
	}
}

/*
 * Location:
 * C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar
 * !\com\americanexpress\ips\gfsg\connection\xml\XMLRequestConnection.class Java
 * compiler version: 5 (49.0) JD-Core Version: 0.7.1
 */