/*     */ package com.americanexpress.ips.gfsg.processor.xml;
/*     */ 
/*     */ import com.americanexpress.ips.gfsg.beans.ErrorObject;
/*     */ import com.americanexpress.ips.gfsg.beans.xml.batchadmin.BatchAdminRequestBean;
/*     */ import com.americanexpress.ips.gfsg.beans.xml.batchadmin.BatchAdminResponseBean;
/*     */ import com.americanexpress.ips.gfsg.beans.xml.batchadmin.BatchSummaryResponseBean;
/*     */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.DataCaptureRequestBean;
/*     */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.DataCaptureResponseBean;
/*     */ import com.americanexpress.ips.gfsg.connection.PropertyReader;
/*     */ import com.americanexpress.ips.gfsg.connection.xml.XMLRequestConnection;
/*     */ import com.americanexpress.ips.gfsg.exceptions.SSLConnectionException;
/*     */ import com.americanexpress.ips.gfsg.exceptions.SettlementException;
/*     */ import com.americanexpress.ips.gfsg.messagecreator.XMLMessageCreator;
/*     */ import com.americanexpress.ips.gfsg.utils.SettlementXMLConverter;
/*     */ import com.americanexpress.ips.gfsg.utils.XMLValidator;
/*     */ import java.util.List;
/*     */ import org.apache.log4j.Logger;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class XmlRequestProcessor
/*     */   implements IXmlRequestProcessor
/*     */ {
/*  38 */   private static final Logger LOGGER = Logger.getLogger(XmlRequestProcessor.class);
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public BatchAdminResponseBean createBatchOpenRequest(BatchAdminRequestBean batchAdminRequestBean, PropertyReader propertyReader)
/*     */   {
/*  55 */     BatchAdminResponseBean batchAdminReponseBean = new BatchAdminResponseBean();
/*  56 */     String xmlMessage = "";
/*  57 */     List<ErrorObject> validationErrorList = null;
/*  58 */     String responseXml = "";
/*     */     
/*     */ 
/*     */     try
/*     */     {
/*  63 */       validationErrorList = XMLValidator.validateBatchOpenRequest(batchAdminRequestBean);
/*     */       
/*     */ 
/*     */ 
/*  67 */       if ((validationErrorList == null) || (validationErrorList.isEmpty()))
/*     */       {
/*     */ 
/*  70 */         xmlMessage = XMLMessageCreator.createtXMLBatchAdminRequest(batchAdminRequestBean);
/*  71 */         if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
/*  72 */           LOGGER.info("BatchAdminOpenRequest XMLMessage:" + xmlMessage);
/*     */         }
/*  74 */         if (!xmlMessage.equalsIgnoreCase(""))
/*     */         {
/*  76 */           validationErrorList = XMLValidator.validateConnectionInfo(propertyReader);
/*  77 */           if ((validationErrorList == null) || (validationErrorList.isEmpty()))
/*     */           {
/*  79 */             responseXml = sendHTTPSRequest(xmlMessage, "GFSG XML BAR", propertyReader);
/*     */             
/*  81 */             batchAdminReponseBean = SettlementXMLConverter.convertResponseXMLToBean(responseXml);
/*  82 */             if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
/*  83 */               LOGGER.info("BatchAdminOpenResponse XMLMessage:" + responseXml);
/*     */             }
/*  85 */             batchAdminReponseBean.setRequestxml(xmlMessage);
/*  86 */             String responseXML = "";
/*  87 */             if ((responseXml != null) && (batchAdminReponseBean.getBatchStatusText() != null)) {
/*  88 */               responseXML = apendStatusTextXML(responseXml, batchAdminReponseBean.getBatchStatusText());
/*     */             }
/*  90 */             batchAdminReponseBean.setResponsexml(responseXML);
/*     */           }
/*     */           else {
/*  93 */             batchAdminReponseBean.setActionCodeDescription(validationErrorList);
/*     */           }
/*     */         }
/*     */       }
/*     */       else {
/*  98 */         batchAdminReponseBean.setActionCodeDescription(validationErrorList);
/*     */       }
/*     */     }
/*     */     catch (SettlementException e)
/*     */     {
/* 103 */       LOGGER.fatal("Error in BatchAdminOpenRequest::" + e);
/*     */     }
/*     */     
/* 106 */     return batchAdminReponseBean;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public BatchAdminResponseBean createBatchCloseRequest(BatchAdminRequestBean batchAdminRequestBean, PropertyReader propertyReader)
/*     */   {
/* 125 */     BatchAdminResponseBean batchAdminReponseBean = new BatchAdminResponseBean();
/* 126 */     String xmlMessage = "";
/* 127 */     List<ErrorObject> validationErrorList = null;
/* 128 */     String responseXml = "";
/*     */     
/*     */ 
/*     */ 
/*     */     try
/*     */     {
/* 134 */       validationErrorList = XMLValidator.validateBatchCloseRequest(batchAdminRequestBean);
/*     */       
/*     */ 
/*     */ 
/* 138 */       if ((validationErrorList == null) || (validationErrorList.isEmpty()))
/*     */       {
/* 140 */         xmlMessage = XMLMessageCreator.createtXMLBatchAdminRequest(batchAdminRequestBean);
/* 141 */         if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
/* 142 */           LOGGER.info("BatchAdminCloseRequest XMLMessage:" + xmlMessage);
/*     */         }
/* 144 */         if (!xmlMessage.equalsIgnoreCase(""))
/*     */         {
/* 146 */           validationErrorList = XMLValidator.validateConnectionInfo(propertyReader);
/* 147 */           if ((validationErrorList == null) || (validationErrorList.isEmpty()))
/*     */           {
/* 149 */             responseXml = sendHTTPSRequest(xmlMessage, "GFSG XML BAR", propertyReader);
/*     */             
/* 151 */             batchAdminReponseBean = SettlementXMLConverter.convertResponseXMLToBean(responseXml);
/* 152 */             if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
/* 153 */               LOGGER.info("BatchAdminCloseResponse XMLMessage" + responseXml);
/*     */             }
/* 155 */             batchAdminReponseBean.setRequestxml(xmlMessage);
/* 156 */             String responseXML = "";
/* 157 */             if ((responseXml != null) && (batchAdminReponseBean.getBatchStatusText() != null)) {
/* 158 */               responseXML = apendStatusTextXML(responseXml, batchAdminReponseBean.getBatchStatusText());
/*     */             }
/*     */             
/* 161 */             batchAdminReponseBean.setResponsexml(responseXML);
/*     */           } else {
/* 163 */             batchAdminReponseBean.setActionCodeDescription(validationErrorList);
/*     */           }
/*     */         }
/*     */       }
/*     */       else {
/* 168 */         batchAdminReponseBean.setActionCodeDescription(validationErrorList);
/*     */       }
/*     */     }
/*     */     catch (SettlementException e)
/*     */     {
/* 173 */       LOGGER.fatal("Error In BatchAdminCloseRequest::" + e);
/*     */     }
/* 175 */     return batchAdminReponseBean;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public BatchAdminResponseBean createBatchPurgeRequest(BatchAdminRequestBean batchAdminRequestBean, PropertyReader propertyReader)
/*     */   {
/* 194 */     BatchAdminResponseBean batchAdminReponseBean = new BatchAdminResponseBean();
/* 195 */     String xmlMessage = "";
/* 196 */     List<ErrorObject> validationErrorList = null;
/* 197 */     String responseXml = "";
/*     */     
/*     */ 
/*     */ 
/*     */     try
/*     */     {
/* 203 */       validationErrorList = XMLValidator.validateBatchCloseRequest(batchAdminRequestBean);
/*     */       
/*     */ 
/*     */ 
/* 207 */       if ((validationErrorList == null) || (validationErrorList.isEmpty()))
/*     */       {
/*     */ 
/* 210 */         xmlMessage = XMLMessageCreator.createtXMLBatchAdminRequest(batchAdminRequestBean);
/* 211 */         if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
/* 212 */           LOGGER.info("BatchAdminPurgeRequest XMLMessage:" + xmlMessage);
/*     */         }
/* 214 */         if (!xmlMessage.equalsIgnoreCase(""))
/*     */         {
/* 216 */           validationErrorList = XMLValidator.validateConnectionInfo(propertyReader);
/* 217 */           if ((validationErrorList == null) || (validationErrorList.isEmpty()))
/*     */           {
/* 219 */             responseXml = sendHTTPSRequest(xmlMessage, "GFSG XML BAR", propertyReader);
/* 220 */             batchAdminReponseBean = SettlementXMLConverter.convertResponseXMLToBean(responseXml);
/* 221 */             if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
/* 222 */               LOGGER.info("BatchAdminPurgeResponse XMLMessage:" + responseXml);
/*     */             }
/* 224 */             batchAdminReponseBean.setRequestxml(xmlMessage);
/* 225 */             String responseXML = "";
/* 226 */             if ((responseXml != null) && (batchAdminReponseBean.getBatchStatusText() != null)) {
/* 227 */               responseXML = apendStatusTextXML(responseXml, batchAdminReponseBean.getBatchStatusText());
/*     */             }
/* 229 */             batchAdminReponseBean.setResponsexml(responseXML);
/*     */           }
/*     */           else {
/* 232 */             batchAdminReponseBean.setActionCodeDescription(validationErrorList);
/*     */           }
/*     */         }
/*     */       }
/*     */       else {
/* 237 */         batchAdminReponseBean.setActionCodeDescription(validationErrorList);
/*     */       }
/*     */     }
/*     */     catch (SettlementException e)
/*     */     {
/* 242 */       LOGGER.fatal("Error In BatchAdminPurgeRequest ::" + e);
/*     */     }
/* 244 */     return batchAdminReponseBean;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public BatchSummaryResponseBean createBatchStatusRequest(BatchAdminRequestBean batchAdminRequestBean, PropertyReader propertyReader)
/*     */   {
/* 264 */     BatchSummaryResponseBean batchSummaryResponseBean = new BatchSummaryResponseBean();
/* 265 */     String xmlMessage = "";
/* 266 */     List<ErrorObject> validationErrorList = null;
/* 267 */     String responseXml = "";
/*     */     
/*     */ 
/*     */ 
/*     */     try
/*     */     {
/* 273 */       validationErrorList = XMLValidator.validateBatchStatusRequest(batchAdminRequestBean);
/*     */       
/*     */ 
/*     */ 
/* 277 */       if ((validationErrorList == null) || (validationErrorList.isEmpty()))
/*     */       {
/* 279 */         xmlMessage = XMLMessageCreator.createtXMLBatchAdminRequest(batchAdminRequestBean);
/* 280 */         if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
/* 281 */           LOGGER.info("BatchStatusRequest XMLMessage:" + xmlMessage);
/*     */         }
/* 283 */         if (!xmlMessage.equalsIgnoreCase(""))
/*     */         {
/* 285 */           validationErrorList = XMLValidator.validateConnectionInfo(propertyReader);
/* 286 */           if ((validationErrorList == null) || (validationErrorList.isEmpty()))
/*     */           {
/* 288 */             responseXml = sendHTTPSRequest(xmlMessage, "GFSG XML BAR", propertyReader);
/*     */             
/* 290 */             batchSummaryResponseBean = SettlementXMLConverter.convertResponseXMLToSummaryBean(responseXml);
/* 291 */             if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
/* 292 */               LOGGER.info("BatchStatusResponse XMLMessage:" + responseXml);
/*     */             }
/* 294 */             batchSummaryResponseBean.setRequestxml(xmlMessage);
/* 295 */             String responseXML = "";
/* 296 */             if ((responseXml != null) && (batchSummaryResponseBean.getBatchStatusText() != null)) {
/* 297 */               responseXML = apendStatusTextXML(responseXml, batchSummaryResponseBean.getBatchStatusText());
/*     */             }
/* 299 */             batchSummaryResponseBean.setResponsexml(responseXML);
/*     */           }
/*     */           else {
/* 302 */             batchSummaryResponseBean.setActionCodeDescription(validationErrorList);
/*     */           }
/*     */         }
/*     */       }
/*     */       else
/*     */       {
/* 308 */         batchSummaryResponseBean.setActionCodeDescription(validationErrorList);
/*     */       }
/*     */     }
/*     */     catch (SettlementException e)
/*     */     {
/* 313 */       LOGGER.fatal("Error In BatchStatusRequest::" + e);
/*     */     }
/* 315 */     return batchSummaryResponseBean;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public DataCaptureResponseBean processXMLDataCaptureTransaction(DataCaptureRequestBean dataCaptureRequestBean, PropertyReader propertyReader)
/*     */   {
/* 334 */     DataCaptureResponseBean dataCaptureResponseBean = new DataCaptureResponseBean();
/* 335 */     String responseXML = null;
/* 336 */     String requestXML = null;
/*     */     try {
/* 338 */       List<ErrorObject> validationErrorList = null;
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 344 */       LOGGER.info("Process XML DataCapture Transaction");
/* 345 */       if (dataCaptureRequestBean != null) {
/* 346 */         validationErrorList = XMLValidator.validateDataCaptureRequest(dataCaptureRequestBean);
/*     */         
/*     */ 
/* 349 */         if ((validationErrorList == null) || (validationErrorList.isEmpty())) {
/* 350 */           requestXML = XMLMessageCreator.convertDataCaptureReqBeanToXML(dataCaptureRequestBean);
/*     */           
/*     */ 
/* 353 */           LOGGER.info("DataCapture Request XML:" + requestXML);
/* 354 */           if (requestXML != null)
/*     */           {
/* 356 */             validationErrorList = XMLValidator.validateConnectionInfo(propertyReader);
/* 357 */             if ((validationErrorList == null) || (validationErrorList.isEmpty()))
/*     */             {
/* 359 */               responseXML = sendHTTPSRequest(requestXML, "GFSG XML DCR", propertyReader);
/*     */               
/* 361 */               dataCaptureResponseBean = SettlementXMLConverter.convertResXMLToDataCaptureResBean(responseXML);
/*     */               
/*     */ 
/*     */ 
/*     */ 
/* 366 */               dataCaptureResponseBean.setResponseXML(responseXML);
/*     */               
/*     */ 
/* 369 */               dataCaptureResponseBean.setRequestXML(requestXML);
/* 370 */               LOGGER.info("DataCapture Response XML:" + responseXML);
/*     */             }
/*     */           } else {
/* 373 */             dataCaptureResponseBean.setActionCodeDescription(validationErrorList);
/*     */           }
/*     */         }
/*     */         else {
/* 377 */           dataCaptureResponseBean.setActionCodeDescription(validationErrorList);
/*     */         }
/*     */       }
/*     */     }
/*     */     catch (SettlementException e) {
/* 382 */       LOGGER.fatal("Error in DataCapture Request::" + e);
/*     */     }
/* 384 */     return dataCaptureResponseBean;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public final String sendHTTPSRequest(String xmlReqString, String requestType, PropertyReader propertyReader)
/*     */     throws SettlementException
/*     */   {
/* 405 */     String responseXML = null;
/*     */     try
/*     */     {
/* 408 */       XMLRequestConnection xmlConnection = new XMLRequestConnection();
/*     */       
/* 410 */       PropertyReader.setXmlhttpsUrl(XMLRequestConnection.constructWebServiceURL(PropertyReader.getXmlhttpsUrl(), propertyReader.getHttpsPort()));
/* 411 */       xmlConnection.createHTTPsConnection(propertyReader);
/*     */       
/* 413 */       responseXML = xmlConnection.sendHttpsRequest(xmlReqString, requestType, propertyReader);
/*     */     }
/*     */     catch (SSLConnectionException sex)
/*     */     {
/* 417 */       throw new SettlementException();
/*     */     }
/*     */     catch (SettlementException sex)
/*     */     {
/* 421 */       throw new SettlementException();
/*     */     }
/*     */     catch (Exception sex) {
/* 424 */       throw new SettlementException();
/*     */     }
/*     */     
/*     */ 
/* 428 */     return responseXML;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private String apendStatusTextXML(String responseXML, String statusText)
/*     */   {
/* 446 */     String partTillStatusText = responseXML.substring(0, responseXML.indexOf("<BatchStatusText>") - 1);
/*     */     
/* 448 */     String partStatusText = responseXML.substring(responseXML.indexOf("<BatchStatusText>"));
/* 449 */     partStatusText = partStatusText.substring(0, partStatusText.indexOf(">") + 1) + 
/* 450 */       statusText + partStatusText.substring(partStatusText.indexOf("</"));
/*     */     
/*     */ 
/* 453 */     return partTillStatusText + partStatusText;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private String apendStatusTextXMLForDataCapture(String responseXML, String statusText)
/*     */   {
/* 470 */     String partTillStatusText = responseXML.substring(0, responseXML.indexOf("<StatusText>") - 1);
/*     */     
/* 472 */     String partStatusText = responseXML.substring(responseXML.indexOf("<StatusText>"));
/* 473 */     partStatusText = partStatusText.substring(0, partStatusText.indexOf(">") + 1) + 
/* 474 */       statusText + partStatusText.substring(partStatusText.indexOf("</"));
/*     */     
/*     */ 
/* 477 */     return partTillStatusText + partStatusText;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\processor\xml\XmlRequestProcessor.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */