package com.americanexpress.ips.gfsg.processor.xml;

import com.americanexpress.ips.gfsg.beans.xml.batchadmin.BatchAdminRequestBean;
import com.americanexpress.ips.gfsg.beans.xml.batchadmin.BatchAdminResponseBean;
import com.americanexpress.ips.gfsg.beans.xml.batchadmin.BatchSummaryResponseBean;
import com.americanexpress.ips.gfsg.beans.xml.datacapture.DataCaptureRequestBean;
import com.americanexpress.ips.gfsg.beans.xml.datacapture.DataCaptureResponseBean;
import com.americanexpress.ips.gfsg.connection.PropertyReader;

public abstract interface IXmlRequestProcessor
{
  public abstract BatchAdminResponseBean createBatchOpenRequest(BatchAdminRequestBean paramBatchAdminRequestBean, PropertyReader paramPropertyReader);
  
  public abstract BatchSummaryResponseBean createBatchStatusRequest(BatchAdminRequestBean paramBatchAdminRequestBean, PropertyReader paramPropertyReader);
  
  public abstract BatchAdminResponseBean createBatchPurgeRequest(BatchAdminRequestBean paramBatchAdminRequestBean, PropertyReader paramPropertyReader);
  
  public abstract BatchAdminResponseBean createBatchCloseRequest(BatchAdminRequestBean paramBatchAdminRequestBean, PropertyReader paramPropertyReader);
  
  public abstract DataCaptureResponseBean processXMLDataCaptureTransaction(DataCaptureRequestBean paramDataCaptureRequestBean, PropertyReader paramPropertyReader);
}


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\processor\xml\IXmlRequestProcessor.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */