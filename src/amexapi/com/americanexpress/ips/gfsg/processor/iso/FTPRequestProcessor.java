/*     */ package com.americanexpress.ips.gfsg.processor.iso;
/*     */ 
/*     */ import com.americanexpress.ips.gfsg.beans.EncryptedData;
/*     */ import com.americanexpress.ips.gfsg.beans.ErrorObject;
/*     */ import com.americanexpress.ips.gfsg.beans.SettlementRequestDataObject;
/*     */ import com.americanexpress.ips.gfsg.beans.SettlementResponseDataObject;
/*     */ import com.americanexpress.ips.gfsg.beans.acknowledgement.AcknowledgementReportObject;
/*     */ import com.americanexpress.ips.gfsg.beans.conformation.ConfirmationReportObject;
/*     */ import com.americanexpress.ips.gfsg.connection.PropertyReader;
/*     */ import com.americanexpress.ips.gfsg.exceptions.SettlementException;
/*     */ import com.americanexpress.ips.gfsg.formats.SettlementMessageFormatter;
/*     */ import com.americanexpress.ips.gfsg.messagecreator.AcknowledgementReportObjectCreator;
/*     */ import com.americanexpress.ips.gfsg.messagecreator.ConfirmationReportObjectCreator;
/*     */ import com.americanexpress.ips.gfsg.utils.FieldsAutoPopulator;
/*     */ import com.americanexpress.ips.gfsg.validator.SettlementValidator;
/*     */ import com.americanexpress.util.juice.Juice;
/*     */ import com.jcraft.jsch.Channel;
/*     */ import com.jcraft.jsch.ChannelSftp;
/*     */ import com.jcraft.jsch.JSchException;
/*     */ import com.jcraft.jsch.Session;
/*     */ import java.io.BufferedWriter;
/*     */ import java.io.File;
/*     */ import java.io.FileNotFoundException;
/*     */ import java.io.FileWriter;
/*     */ import java.io.IOException;
/*     */ import java.util.List;
/*     */ import org.apache.log4j.Logger;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class FTPRequestProcessor
/*     */   implements IRequestProcessor
/*     */ {
/*  59 */   private static final Logger LOGGER = Logger.getLogger(FTPRequestProcessor.class);
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private boolean debugFlag;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public SettlementResponseDataObject processRequest(SettlementRequestDataObject requestDataObject, PropertyReader propertyReader)
/*     */     throws SettlementException
/*     */   {
/*  79 */     if (PropertyReader.getDebuggerFlag().equalsIgnoreCase(
/*  80 */       "ON")) {
/*  81 */       this.debugFlag = true;
/*  82 */       LOGGER.info("Entered into processRequest()");
/*     */     }
/*     */     
/*  85 */     SettlementResponseDataObject settlementResBean = new SettlementResponseDataObject();
/*     */     
/*     */ 
/*     */ 
/*  89 */     StringBuffer localUploadFilePath = new StringBuffer();
/*  90 */     List<ErrorObject> validationErrorList = null;
/*     */     try
/*     */     {
/*  93 */       FieldsAutoPopulator.updateRecordNumber(requestDataObject);
/*     */       
/*  95 */       FieldsAutoPopulator.updateTFSRecord(requestDataObject);
/*     */       
/*  97 */       validationErrorList = 
/*  98 */         SettlementValidator.validateISOSettlementRecords(requestDataObject);
/*     */       
/*     */ 
/*     */ 
/* 102 */       List<ErrorObject> connectionInfoErrorList = SettlementValidator.validateConnectionInfo(propertyReader);
/* 103 */       if ((connectionInfoErrorList != null) && (!connectionInfoErrorList.isEmpty())) {
/* 104 */         validationErrorList.addAll(connectionInfoErrorList);
/*     */       }
/*     */       
/* 107 */       if ((validationErrorList == null) || (validationErrorList.isEmpty()))
/*     */       {
/* 109 */         List<String> sttlementRecordList = 
/* 110 */           SettlementMessageFormatter.formatISORequest(requestDataObject);
/*     */         
/* 112 */         if ((sttlementRecordList != null) && 
/* 113 */           (!sttlementRecordList.isEmpty()))
/*     */         {
/* 115 */           if (PropertyReader.getFileStorage() != null)
/*     */           {
/* 117 */             if (PropertyReader.getFileStorage().equalsIgnoreCase("ON"))
/* 118 */               localUploadFilePath.append(
/* 119 */                 PropertyReader.getFtpLocalUploadFilePath());
/*     */           }
/* 121 */           localUploadFilePath.append(
/* 122 */             PropertyReader.getFtpUploadFileName());
/*     */           
/* 124 */           List<ErrorObject> sftpUploadErrorList = 
/* 125 */             createSettelmentFileAndAppendData(localUploadFilePath.toString(), sttlementRecordList);
/*     */           
/* 127 */           if ((sftpUploadErrorList != null) && (!sftpUploadErrorList.isEmpty())) {
/* 128 */             validationErrorList.addAll(sftpUploadErrorList);
/* 129 */             settlementResBean.setActionCodeDescription(validationErrorList);
/*     */           }
/*     */         }
/*     */       } else {
/* 133 */         settlementResBean.setActionCodeDescription(validationErrorList);
/*     */       }
/*     */     } catch (FileNotFoundException fnfe) {
/* 136 */       if (this.debugFlag) {
/* 137 */         LOGGER.fatal("ERROR_FILE_NOT_FOUND", fnfe);
/*     */       }
/* 139 */       throw new SettlementException("ERROR_FILE_NOT_FOUND", fnfe);
/*     */     }
/*     */     catch (IOException ioe) {
/* 142 */       if (this.debugFlag) {
/* 143 */         LOGGER.fatal("ERROR_READ_WRITE_FILE", ioe);
/*     */       }
/* 145 */       throw new SettlementException("ERROR_READ_WRITE_FILE", ioe);
/*     */     } catch (Exception e) {
/* 147 */       if (this.debugFlag) {
/* 148 */         LOGGER.fatal("ERROR_Settlement_REQ_FILE", e);
/*     */       }
/* 150 */       throw new SettlementException("ERROR_Settlement_REQ_FILE", e);
/*     */     }
/*     */     
/* 153 */     if (this.debugFlag) {
/* 154 */       LOGGER.info("Exiting from processRequest()");
/*     */     }
/*     */     
/* 157 */     return settlementResBean;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<File> fetchConfirmationReportInPrintedFormat(PropertyReader propertyReader)
/*     */     throws SettlementException
/*     */   {
/* 173 */     LOGGER.info("LOGGER_ENTER");
/* 174 */     List<File> file = null;
/* 175 */     List<ErrorObject> connErrorList = SettlementValidator.validateConnectionInfo(propertyReader);
/* 176 */     if ((connErrorList == null) || (connErrorList.isEmpty())) {
/* 177 */       StringBuffer convertedFilePath = new StringBuffer();
/* 178 */       String localDirectoryPath = 
/* 179 */         PropertyReader.getFtpLocalDownloadFilePath();
/* 180 */       String downloadFileName = PropertyReader.getFtpDownloadPrintedFileName();
/* 181 */       if ((PropertyReader.getFileStorage() != null) && (PropertyReader.getFileStorage().equalsIgnoreCase("ON"))) {
/* 182 */         convertedFilePath.append(localDirectoryPath);
/* 183 */         convertedFilePath.append(downloadFileName);
/* 184 */         LOGGER.info("Download File Path:" + convertedFilePath.toString());
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */       try
/*     */       {
/* 191 */         file = downloadFileFromSFTP(downloadFileName, convertedFilePath.toString());
/*     */       }
/*     */       catch (Exception exp) {
/* 194 */         throw new SettlementException("XXXX.5 -   SFT Download failed");
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 199 */     return file;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<ConfirmationReportObject> fetchConfirmationReportInRawData(PropertyReader propertyReader)
/*     */     throws SettlementException
/*     */   {
/* 217 */     LOGGER.info("LOGGER_ENTER");
/* 218 */     List<ConfirmationReportObject> confirmationReportObjectList = null;
/* 219 */     List<ErrorObject> connErrorList = SettlementValidator.validateConnectionInfo(propertyReader);
/* 220 */     if ((connErrorList == null) || (connErrorList.isEmpty())) {
/* 221 */       StringBuffer convertedFilePath = new StringBuffer();
/* 222 */       String localDirectoryPath = 
/* 223 */         PropertyReader.getFtpLocalDownloadFilePath();
/* 224 */       String downloadFileName = PropertyReader.getFtpDownloadRawFileName();
/* 225 */       if ((PropertyReader.getFileStorage() != null) && (PropertyReader.getFileStorage().equalsIgnoreCase("ON"))) {
/* 226 */         convertedFilePath.append(localDirectoryPath);
/* 227 */         convertedFilePath.append(downloadFileName);
/* 228 */         LOGGER.info("Download File Path:" + convertedFilePath.toString());
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */       try
/*     */       {
/* 236 */         List<File> fileList = downloadFileFromSFTP(downloadFileName, 
/* 237 */           localDirectoryPath);
/* 238 */         confirmationReportObjectList = 
/* 239 */           ConfirmationReportObjectCreator.convertRawFiletoResponse(fileList);
/*     */       }
/*     */       catch (Exception exp) {
/* 242 */         throw new SettlementException("XXXX.5 -   SFT Download failed");
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 252 */     return confirmationReportObjectList;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<AcknowledgementReportObject> fetchAcknowledgementInPrintedFormat(PropertyReader propertyReader, List<ErrorObject> connErrorList)
/*     */     throws SettlementException
/*     */   {
/* 267 */     LOGGER.info("LOGGER_ENTER");
/* 268 */     List<File> file = null;
/* 269 */     List<AcknowledgementReportObject> acknowledgementReportObjectList = null;
/* 270 */     connErrorList = SettlementValidator.validateConnectionInfo(propertyReader);
/* 271 */     if ((connErrorList == null) || (connErrorList.isEmpty())) {
/* 272 */       StringBuffer convertedFilePath = new StringBuffer();
/* 273 */       String localDirectoryPath = 
/* 274 */         PropertyReader.getFtpLocalDownloadFilePath();
/*     */       
/* 276 */       String downloadFileName = PropertyReader.getFtpDownloadAcknowledgeFileName();
/* 277 */       if ((PropertyReader.getFileStorage() != null) && (PropertyReader.getFileStorage().equalsIgnoreCase("ON")) && (PropertyReader.getAcknowledgeType().equals("TEXT"))) {
/* 278 */         convertedFilePath.append(localDirectoryPath);
/* 279 */         convertedFilePath.append(downloadFileName);
/* 280 */         LOGGER.info("Download File Path:" + convertedFilePath.toString());
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */       try
/*     */       {
/* 289 */         file = downloadFileFromSFTP(downloadFileName, 
/* 290 */           convertedFilePath.toString());
/*     */         
/*     */ 
/* 293 */         acknowledgementReportObjectList = 
/* 294 */           AcknowledgementReportObjectCreator.convertAckFiletoResponse(file);
/*     */       }
/*     */       catch (Exception exp)
/*     */       {
/* 298 */         throw new SettlementException("XXXX.5 -   SFT Download failed");
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 308 */     return acknowledgementReportObjectList;
/*     */   }
/*     */   
/*     */   /* Error */
/*     */   private List<ErrorObject> createSettelmentFileAndAppendData(String filePath, List<String> settlementRecords)
/*     */     throws SettlementException
/*     */   {
/*     */     // Byte code:
/*     */     //   0: getstatic 20	com/americanexpress/ips/gfsg/processor/iso/FTPRequestProcessor:LOGGER	Lorg/apache/log4j/Logger;
/*     */     //   3: new 174	java/lang/StringBuilder
/*     */     //   6: dup
/*     */     //   7: ldc -19
/*     */     //   9: invokespecial 178	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
/*     */     //   12: aload_1
/*     */     //   13: invokevirtual 181	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
/*     */     //   16: invokevirtual 184	java/lang/StringBuilder:toString	()Ljava/lang/String;
/*     */     //   19: invokevirtual 52	org/apache/log4j/Logger:info	(Ljava/lang/Object;)V
/*     */     //   22: iconst_0
/*     */     //   23: istore 4
/*     */     //   25: new 239	java/util/ArrayList
/*     */     //   28: dup
/*     */     //   29: invokespecial 241	java/util/ArrayList:<init>	()V
/*     */     //   32: astore 5
/*     */     //   34: aload_1
/*     */     //   35: ifnull +12 -> 47
/*     */     //   38: aload_1
/*     */     //   39: ldc -14
/*     */     //   41: invokevirtual 225	java/lang/String:equals	(Ljava/lang/Object;)Z
/*     */     //   44: ifeq +13 -> 57
/*     */     //   47: new 32	com/americanexpress/ips/gfsg/exceptions/SettlementException
/*     */     //   50: dup
/*     */     //   51: ldc -12
/*     */     //   53: invokespecial 191	com/americanexpress/ips/gfsg/exceptions/SettlementException:<init>	(Ljava/lang/String;)V
/*     */     //   56: athrow
/*     */     //   57: aload_0
/*     */     //   58: aload_1
/*     */     //   59: aload_2
/*     */     //   60: invokespecial 246	com/americanexpress/ips/gfsg/processor/iso/FTPRequestProcessor:createSettlementFile	(Ljava/lang/String;Ljava/util/List;)Ljava/io/File;
/*     */     //   63: astore_3
/*     */     //   64: aload_0
/*     */     //   65: aload_3
/*     */     //   66: ldc -6
/*     */     //   68: invokespecial 252	com/americanexpress/ips/gfsg/processor/iso/FTPRequestProcessor:uploadFileToSFTP	(Ljava/io/File;Ljava/lang/String;)Z
/*     */     //   71: istore 4
/*     */     //   73: goto +99 -> 172
/*     */     //   76: astore 6
/*     */     //   78: new 256	com/americanexpress/ips/gfsg/beans/ErrorObject
/*     */     //   81: dup
/*     */     //   82: ldc_w 258
/*     */     //   85: ldc_w 260
/*     */     //   88: invokespecial 262	com/americanexpress/ips/gfsg/beans/ErrorObject:<init>	(Ljava/lang/String;Ljava/lang/String;)V
/*     */     //   91: astore 7
/*     */     //   93: aload 5
/*     */     //   95: aload 7
/*     */     //   97: invokeinterface 265 2 0
/*     */     //   102: pop
/*     */     //   103: aload 6
/*     */     //   105: invokevirtual 268	java/lang/Exception:getMessage	()Ljava/lang/String;
/*     */     //   108: astore 8
/*     */     //   110: aload_3
/*     */     //   111: invokevirtual 271	java/io/File:exists	()Z
/*     */     //   114: ifeq +85 -> 199
/*     */     //   117: aload_3
/*     */     //   118: invokevirtual 276	java/io/File:delete	()Z
/*     */     //   121: istore 10
/*     */     //   123: iload 10
/*     */     //   125: ifeq +74 -> 199
/*     */     //   128: getstatic 20	com/americanexpress/ips/gfsg/processor/iso/FTPRequestProcessor:LOGGER	Lorg/apache/log4j/Logger;
/*     */     //   131: ldc_w 279
/*     */     //   134: invokevirtual 52	org/apache/log4j/Logger:info	(Ljava/lang/Object;)V
/*     */     //   137: goto +62 -> 199
/*     */     //   140: astore 9
/*     */     //   142: aload_3
/*     */     //   143: invokevirtual 271	java/io/File:exists	()Z
/*     */     //   146: ifeq +23 -> 169
/*     */     //   149: aload_3
/*     */     //   150: invokevirtual 276	java/io/File:delete	()Z
/*     */     //   153: istore 10
/*     */     //   155: iload 10
/*     */     //   157: ifeq +12 -> 169
/*     */     //   160: getstatic 20	com/americanexpress/ips/gfsg/processor/iso/FTPRequestProcessor:LOGGER	Lorg/apache/log4j/Logger;
/*     */     //   163: ldc_w 279
/*     */     //   166: invokevirtual 52	org/apache/log4j/Logger:info	(Ljava/lang/Object;)V
/*     */     //   169: aload 9
/*     */     //   171: athrow
/*     */     //   172: aload_3
/*     */     //   173: invokevirtual 271	java/io/File:exists	()Z
/*     */     //   176: ifeq +23 -> 199
/*     */     //   179: aload_3
/*     */     //   180: invokevirtual 276	java/io/File:delete	()Z
/*     */     //   183: istore 10
/*     */     //   185: iload 10
/*     */     //   187: ifeq +12 -> 199
/*     */     //   190: getstatic 20	com/americanexpress/ips/gfsg/processor/iso/FTPRequestProcessor:LOGGER	Lorg/apache/log4j/Logger;
/*     */     //   193: ldc_w 279
/*     */     //   196: invokevirtual 52	org/apache/log4j/Logger:info	(Ljava/lang/Object;)V
/*     */     //   199: iload 4
/*     */     //   201: ifeq +299 -> 500
/*     */     //   204: invokestatic 96	com/americanexpress/ips/gfsg/connection/PropertyReader:getFileStorage	()Ljava/lang/String;
/*     */     //   207: ifnull +293 -> 500
/*     */     //   210: invokestatic 96	com/americanexpress/ips/gfsg/connection/PropertyReader:getFileStorage	()Ljava/lang/String;
/*     */     //   213: ldc 40
/*     */     //   215: invokevirtual 42	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
/*     */     //   218: ifeq +282 -> 500
/*     */     //   221: getstatic 20	com/americanexpress/ips/gfsg/processor/iso/FTPRequestProcessor:LOGGER	Lorg/apache/log4j/Logger;
/*     */     //   224: new 174	java/lang/StringBuilder
/*     */     //   227: dup
/*     */     //   228: ldc_w 281
/*     */     //   231: invokespecial 178	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
/*     */     //   234: invokestatic 99	com/americanexpress/ips/gfsg/connection/PropertyReader:getFtpLocalUploadFilePath	()Ljava/lang/String;
/*     */     //   237: invokevirtual 181	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
/*     */     //   240: invokevirtual 184	java/lang/StringBuilder:toString	()Ljava/lang/String;
/*     */     //   243: invokevirtual 52	org/apache/log4j/Logger:info	(Ljava/lang/Object;)V
/*     */     //   246: new 59	java/lang/StringBuffer
/*     */     //   249: dup
/*     */     //   250: invokespecial 61	java/lang/StringBuffer:<init>	()V
/*     */     //   253: astore 6
/*     */     //   255: aload 6
/*     */     //   257: invokestatic 99	com/americanexpress/ips/gfsg/connection/PropertyReader:getFtpLocalUploadFilePath	()Ljava/lang/String;
/*     */     //   260: invokevirtual 102	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
/*     */     //   263: pop
/*     */     //   264: aload 6
/*     */     //   266: invokestatic 106	com/americanexpress/ips/gfsg/connection/PropertyReader:getFtpUploadFileName	()Ljava/lang/String;
/*     */     //   269: invokevirtual 102	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
/*     */     //   272: pop
/*     */     //   273: new 272	java/io/File
/*     */     //   276: dup
/*     */     //   277: aload 6
/*     */     //   279: invokevirtual 109	java/lang/StringBuffer:toString	()Ljava/lang/String;
/*     */     //   282: invokespecial 283	java/io/File:<init>	(Ljava/lang/String;)V
/*     */     //   285: astore 7
/*     */     //   287: aload 7
/*     */     //   289: invokevirtual 271	java/io/File:exists	()Z
/*     */     //   292: ifeq +142 -> 434
/*     */     //   295: aload 7
/*     */     //   297: invokevirtual 284	java/io/File:lastModified	()J
/*     */     //   300: lstore 8
/*     */     //   302: new 288	java/util/Date
/*     */     //   305: dup
/*     */     //   306: lload 8
/*     */     //   308: invokespecial 290	java/util/Date:<init>	(J)V
/*     */     //   311: astore 10
/*     */     //   313: new 293	java/text/SimpleDateFormat
/*     */     //   316: dup
/*     */     //   317: ldc_w 295
/*     */     //   320: invokespecial 297	java/text/SimpleDateFormat:<init>	(Ljava/lang/String;)V
/*     */     //   323: astore 11
/*     */     //   325: aload 11
/*     */     //   327: aload 10
/*     */     //   329: invokevirtual 298	java/text/SimpleDateFormat:format	(Ljava/util/Date;)Ljava/lang/String;
/*     */     //   332: astore 12
/*     */     //   334: new 293	java/text/SimpleDateFormat
/*     */     //   337: dup
/*     */     //   338: ldc_w 295
/*     */     //   341: invokespecial 297	java/text/SimpleDateFormat:<init>	(Ljava/lang/String;)V
/*     */     //   344: astore 11
/*     */     //   346: invokestatic 302	java/text/DateFormat:getTimeInstance	()Ljava/text/DateFormat;
/*     */     //   349: aload 10
/*     */     //   351: invokevirtual 308	java/text/DateFormat:format	(Ljava/util/Date;)Ljava/lang/String;
/*     */     //   354: astore 13
/*     */     //   356: new 59	java/lang/StringBuffer
/*     */     //   359: dup
/*     */     //   360: aload 6
/*     */     //   362: invokespecial 309	java/lang/StringBuffer:<init>	(Ljava/lang/CharSequence;)V
/*     */     //   365: astore 14
/*     */     //   367: aload 14
/*     */     //   369: ldc_w 312
/*     */     //   372: invokevirtual 102	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
/*     */     //   375: pop
/*     */     //   376: aload 14
/*     */     //   378: aload 12
/*     */     //   380: invokevirtual 102	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
/*     */     //   383: pop
/*     */     //   384: aload 14
/*     */     //   386: aload 13
/*     */     //   388: iconst_0
/*     */     //   389: aload 13
/*     */     //   391: ldc_w 314
/*     */     //   394: invokevirtual 316	java/lang/String:indexOf	(Ljava/lang/String;)I
/*     */     //   397: invokevirtual 320	java/lang/String:substring	(II)Ljava/lang/String;
/*     */     //   400: ldc_w 324
/*     */     //   403: ldc -14
/*     */     //   405: invokevirtual 326	java/lang/String:replaceAll	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
/*     */     //   408: invokevirtual 102	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
/*     */     //   411: pop
/*     */     //   412: new 272	java/io/File
/*     */     //   415: dup
/*     */     //   416: aload 14
/*     */     //   418: invokevirtual 109	java/lang/StringBuffer:toString	()Ljava/lang/String;
/*     */     //   421: invokespecial 283	java/io/File:<init>	(Ljava/lang/String;)V
/*     */     //   424: astore 15
/*     */     //   426: aload 7
/*     */     //   428: aload 15
/*     */     //   430: invokevirtual 330	java/io/File:renameTo	(Ljava/io/File;)Z
/*     */     //   433: pop
/*     */     //   434: getstatic 334	java/lang/System:out	Ljava/io/PrintStream;
/*     */     //   437: ldc_w 340
/*     */     //   440: invokevirtual 342	java/io/PrintStream:println	(Ljava/lang/String;)V
/*     */     //   443: aload_0
/*     */     //   444: aload 6
/*     */     //   446: invokevirtual 109	java/lang/StringBuffer:toString	()Ljava/lang/String;
/*     */     //   449: aload_2
/*     */     //   450: invokespecial 246	com/americanexpress/ips/gfsg/processor/iso/FTPRequestProcessor:createSettlementFile	(Ljava/lang/String;Ljava/util/List;)Ljava/io/File;
/*     */     //   453: pop
/*     */     //   454: goto +46 -> 500
/*     */     //   457: astore 6
/*     */     //   459: new 32	com/americanexpress/ips/gfsg/exceptions/SettlementException
/*     */     //   462: dup
/*     */     //   463: ldc 120
/*     */     //   465: aload 6
/*     */     //   467: invokespecial 126	com/americanexpress/ips/gfsg/exceptions/SettlementException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
/*     */     //   470: athrow
/*     */     //   471: astore 6
/*     */     //   473: new 32	com/americanexpress/ips/gfsg/exceptions/SettlementException
/*     */     //   476: dup
/*     */     //   477: ldc -127
/*     */     //   479: aload 6
/*     */     //   481: invokespecial 126	com/americanexpress/ips/gfsg/exceptions/SettlementException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
/*     */     //   484: athrow
/*     */     //   485: astore 6
/*     */     //   487: new 32	com/americanexpress/ips/gfsg/exceptions/SettlementException
/*     */     //   490: dup
/*     */     //   491: ldc_w 347
/*     */     //   494: aload 6
/*     */     //   496: invokespecial 126	com/americanexpress/ips/gfsg/exceptions/SettlementException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
/*     */     //   499: athrow
/*     */     //   500: aload 5
/*     */     //   502: areturn
/*     */     // Line number table:
/*     */     //   Java source line #331	-> byte code offset #0
/*     */     //   Java source line #332	-> byte code offset #22
/*     */     //   Java source line #333	-> byte code offset #25
/*     */     //   Java source line #335	-> byte code offset #34
/*     */     //   Java source line #336	-> byte code offset #47
/*     */     //   Java source line #338	-> byte code offset #57
/*     */     //   Java source line #342	-> byte code offset #64
/*     */     //   Java source line #345	-> byte code offset #76
/*     */     //   Java source line #346	-> byte code offset #78
/*     */     //   Java source line #347	-> byte code offset #93
/*     */     //   Java source line #348	-> byte code offset #103
/*     */     //   Java source line #355	-> byte code offset #110
/*     */     //   Java source line #356	-> byte code offset #117
/*     */     //   Java source line #357	-> byte code offset #123
/*     */     //   Java source line #358	-> byte code offset #128
/*     */     //   Java source line #353	-> byte code offset #140
/*     */     //   Java source line #355	-> byte code offset #142
/*     */     //   Java source line #356	-> byte code offset #149
/*     */     //   Java source line #357	-> byte code offset #155
/*     */     //   Java source line #358	-> byte code offset #160
/*     */     //   Java source line #363	-> byte code offset #169
/*     */     //   Java source line #355	-> byte code offset #172
/*     */     //   Java source line #356	-> byte code offset #179
/*     */     //   Java source line #357	-> byte code offset #185
/*     */     //   Java source line #358	-> byte code offset #190
/*     */     //   Java source line #364	-> byte code offset #199
/*     */     //   Java source line #365	-> byte code offset #204
/*     */     //   Java source line #366	-> byte code offset #210
/*     */     //   Java source line #367	-> byte code offset #213
/*     */     //   Java source line #366	-> byte code offset #215
/*     */     //   Java source line #368	-> byte code offset #221
/*     */     //   Java source line #369	-> byte code offset #234
/*     */     //   Java source line #368	-> byte code offset #237
/*     */     //   Java source line #370	-> byte code offset #246
/*     */     //   Java source line #371	-> byte code offset #255
/*     */     //   Java source line #372	-> byte code offset #257
/*     */     //   Java source line #371	-> byte code offset #260
/*     */     //   Java source line #373	-> byte code offset #264
/*     */     //   Java source line #374	-> byte code offset #266
/*     */     //   Java source line #373	-> byte code offset #269
/*     */     //   Java source line #375	-> byte code offset #273
/*     */     //   Java source line #376	-> byte code offset #287
/*     */     //   Java source line #377	-> byte code offset #295
/*     */     //   Java source line #378	-> byte code offset #302
/*     */     //   Java source line #379	-> byte code offset #313
/*     */     //   Java source line #380	-> byte code offset #325
/*     */     //   Java source line #381	-> byte code offset #334
/*     */     //   Java source line #382	-> byte code offset #346
/*     */     //   Java source line #383	-> byte code offset #356
/*     */     //   Java source line #384	-> byte code offset #367
/*     */     //   Java source line #385	-> byte code offset #376
/*     */     //   Java source line #386	-> byte code offset #384
/*     */     //   Java source line #387	-> byte code offset #412
/*     */     //   Java source line #388	-> byte code offset #426
/*     */     //   Java source line #390	-> byte code offset #434
/*     */     //   Java source line #391	-> byte code offset #443
/*     */     //   Java source line #392	-> byte code offset #449
/*     */     //   Java source line #391	-> byte code offset #450
/*     */     //   Java source line #396	-> byte code offset #457
/*     */     //   Java source line #398	-> byte code offset #459
/*     */     //   Java source line #399	-> byte code offset #471
/*     */     //   Java source line #401	-> byte code offset #473
/*     */     //   Java source line #402	-> byte code offset #485
/*     */     //   Java source line #404	-> byte code offset #487
/*     */     //   Java source line #408	-> byte code offset #500
/*     */     // Local variable table:
/*     */     //   start	length	slot	name	signature
/*     */     //   0	503	0	this	FTPRequestProcessor
/*     */     //   0	503	1	filePath	String
/*     */     //   0	503	2	settlementRecords	List<String>
/*     */     //   63	117	3	settelmentFile	File
/*     */     //   500	1	3	settelmentFile	File
/*     */     //   23	177	4	isUploaded	boolean
/*     */     //   32	469	5	sftpUploadErrorList	List<ErrorObject>
/*     */     //   76	28	6	exp	Exception
/*     */     //   253	192	6	localUploadFilePath	StringBuffer
/*     */     //   457	9	6	fnfe	FileNotFoundException
/*     */     //   471	9	6	ioe	IOException
/*     */     //   485	10	6	e	Exception
/*     */     //   91	5	7	sftpErrorObj	ErrorObject
/*     */     //   285	142	7	file	File
/*     */     //   108	1	8	str1	String
/*     */     //   300	7	8	fileCreationDateTime	long
/*     */     //   140	30	9	localObject	Object
/*     */     //   121	3	10	isDeleted	boolean
/*     */     //   153	3	10	isDeleted	boolean
/*     */     //   183	3	10	isDeleted	boolean
/*     */     //   311	39	10	date	java.util.Date
/*     */     //   323	22	11	dateFormat	java.text.SimpleDateFormat
/*     */     //   332	47	12	shortDateInstance	String
/*     */     //   354	36	13	shortTimeInstance	String
/*     */     //   365	52	14	renamedFileName	StringBuffer
/*     */     //   424	5	15	renamedFile	File
/*     */     // Exception table:
/*     */     //   from	to	target	type
/*     */     //   64	73	76	java/lang/Exception
/*     */     //   64	110	140	finally
/*     */     //   34	454	457	java/io/FileNotFoundException
/*     */     //   34	454	471	java/io/IOException
/*     */     //   34	454	485	java/lang/Exception
/*     */   }
/*     */   
/*     */   /* Error */
/*     */   private boolean uploadFileToSFTP(File uploadFile, String ftpRemoteDirectory)
/*     */     throws SettlementException
/*     */   {
/*     */     // Byte code:
/*     */     //   0: getstatic 20	com/americanexpress/ips/gfsg/processor/iso/FTPRequestProcessor:LOGGER	Lorg/apache/log4j/Logger;
/*     */     //   3: ldc -90
/*     */     //   5: invokevirtual 52	org/apache/log4j/Logger:info	(Ljava/lang/Object;)V
/*     */     //   8: aconst_null
/*     */     //   9: astore 4
/*     */     //   11: aconst_null
/*     */     //   12: astore 5
/*     */     //   14: aload_0
/*     */     //   15: invokespecial 367	com/americanexpress/ips/gfsg/processor/iso/FTPRequestProcessor:getFtpSession	()Lcom/jcraft/jsch/Session;
/*     */     //   18: astore 5
/*     */     //   20: aload_0
/*     */     //   21: aload 5
/*     */     //   23: invokespecial 371	com/americanexpress/ips/gfsg/processor/iso/FTPRequestProcessor:getSftpChannel	(Lcom/jcraft/jsch/Session;)Lcom/jcraft/jsch/ChannelSftp;
/*     */     //   26: astore 4
/*     */     //   28: getstatic 20	com/americanexpress/ips/gfsg/processor/iso/FTPRequestProcessor:LOGGER	Lorg/apache/log4j/Logger;
/*     */     //   31: ldc_w 375
/*     */     //   34: invokevirtual 52	org/apache/log4j/Logger:info	(Ljava/lang/Object;)V
/*     */     //   37: aload 4
/*     */     //   39: aload_2
/*     */     //   40: invokevirtual 377	com/jcraft/jsch/ChannelSftp:cd	(Ljava/lang/String;)V
/*     */     //   43: new 382	java/io/FileInputStream
/*     */     //   46: dup
/*     */     //   47: aload_1
/*     */     //   48: invokespecial 384	java/io/FileInputStream:<init>	(Ljava/io/File;)V
/*     */     //   51: astore 6
/*     */     //   53: aload 4
/*     */     //   55: aload 6
/*     */     //   57: aload_1
/*     */     //   58: invokevirtual 387	java/io/File:getName	()Ljava/lang/String;
/*     */     //   61: invokevirtual 390	com/jcraft/jsch/ChannelSftp:put	(Ljava/io/InputStream;Ljava/lang/String;)V
/*     */     //   64: getstatic 20	com/americanexpress/ips/gfsg/processor/iso/FTPRequestProcessor:LOGGER	Lorg/apache/log4j/Logger;
/*     */     //   67: ldc_w 394
/*     */     //   70: invokevirtual 52	org/apache/log4j/Logger:info	(Ljava/lang/Object;)V
/*     */     //   73: getstatic 20	com/americanexpress/ips/gfsg/processor/iso/FTPRequestProcessor:LOGGER	Lorg/apache/log4j/Logger;
/*     */     //   76: new 174	java/lang/StringBuilder
/*     */     //   79: dup
/*     */     //   80: ldc_w 396
/*     */     //   83: invokespecial 178	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
/*     */     //   86: aload_1
/*     */     //   87: invokevirtual 387	java/io/File:getName	()Ljava/lang/String;
/*     */     //   90: invokevirtual 181	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
/*     */     //   93: invokevirtual 184	java/lang/StringBuilder:toString	()Ljava/lang/String;
/*     */     //   96: invokevirtual 52	org/apache/log4j/Logger:info	(Ljava/lang/Object;)V
/*     */     //   99: iconst_1
/*     */     //   100: istore_3
/*     */     //   101: aload 6
/*     */     //   103: invokevirtual 398	java/io/FileInputStream:close	()V
/*     */     //   106: goto +144 -> 250
/*     */     //   109: astore 7
/*     */     //   111: getstatic 20	com/americanexpress/ips/gfsg/processor/iso/FTPRequestProcessor:LOGGER	Lorg/apache/log4j/Logger;
/*     */     //   114: new 174	java/lang/StringBuilder
/*     */     //   117: dup
/*     */     //   118: ldc_w 401
/*     */     //   121: invokespecial 178	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
/*     */     //   124: aload 7
/*     */     //   126: invokevirtual 403	java/io/FileNotFoundException:getMessage	()Ljava/lang/String;
/*     */     //   129: invokevirtual 181	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
/*     */     //   132: invokevirtual 184	java/lang/StringBuilder:toString	()Ljava/lang/String;
/*     */     //   135: invokevirtual 404	org/apache/log4j/Logger:fatal	(Ljava/lang/Object;)V
/*     */     //   138: new 32	com/americanexpress/ips/gfsg/exceptions/SettlementException
/*     */     //   141: dup
/*     */     //   142: ldc_w 406
/*     */     //   145: aload 7
/*     */     //   147: invokespecial 126	com/americanexpress/ips/gfsg/exceptions/SettlementException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
/*     */     //   150: athrow
/*     */     //   151: astore 7
/*     */     //   153: getstatic 20	com/americanexpress/ips/gfsg/processor/iso/FTPRequestProcessor:LOGGER	Lorg/apache/log4j/Logger;
/*     */     //   156: ldc_w 408
/*     */     //   159: aload 7
/*     */     //   161: invokevirtual 122	org/apache/log4j/Logger:fatal	(Ljava/lang/Object;Ljava/lang/Throwable;)V
/*     */     //   164: new 32	com/americanexpress/ips/gfsg/exceptions/SettlementException
/*     */     //   167: dup
/*     */     //   168: ldc_w 410
/*     */     //   171: invokespecial 191	com/americanexpress/ips/gfsg/exceptions/SettlementException:<init>	(Ljava/lang/String;)V
/*     */     //   174: athrow
/*     */     //   175: astore 7
/*     */     //   177: getstatic 20	com/americanexpress/ips/gfsg/processor/iso/FTPRequestProcessor:LOGGER	Lorg/apache/log4j/Logger;
/*     */     //   180: ldc_w 408
/*     */     //   183: aload 7
/*     */     //   185: invokevirtual 122	org/apache/log4j/Logger:fatal	(Ljava/lang/Object;Ljava/lang/Throwable;)V
/*     */     //   188: new 32	com/americanexpress/ips/gfsg/exceptions/SettlementException
/*     */     //   191: dup
/*     */     //   192: ldc_w 410
/*     */     //   195: aload 7
/*     */     //   197: invokespecial 126	com/americanexpress/ips/gfsg/exceptions/SettlementException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
/*     */     //   200: athrow
/*     */     //   201: astore 8
/*     */     //   203: aload 4
/*     */     //   205: ifnull +16 -> 221
/*     */     //   208: aload 4
/*     */     //   210: invokevirtual 412	com/jcraft/jsch/ChannelSftp:isConnected	()Z
/*     */     //   213: ifeq +8 -> 221
/*     */     //   216: aload 4
/*     */     //   218: invokevirtual 415	com/jcraft/jsch/ChannelSftp:disconnect	()V
/*     */     //   221: aload 5
/*     */     //   223: ifnull +16 -> 239
/*     */     //   226: aload 5
/*     */     //   228: invokevirtual 418	com/jcraft/jsch/Session:isConnected	()Z
/*     */     //   231: ifeq +8 -> 239
/*     */     //   234: aload 5
/*     */     //   236: invokevirtual 421	com/jcraft/jsch/Session:disconnect	()V
/*     */     //   239: aconst_null
/*     */     //   240: astore 4
/*     */     //   242: aconst_null
/*     */     //   243: astore_1
/*     */     //   244: aconst_null
/*     */     //   245: astore 5
/*     */     //   247: aload 8
/*     */     //   249: athrow
/*     */     //   250: aload 4
/*     */     //   252: ifnull +16 -> 268
/*     */     //   255: aload 4
/*     */     //   257: invokevirtual 412	com/jcraft/jsch/ChannelSftp:isConnected	()Z
/*     */     //   260: ifeq +8 -> 268
/*     */     //   263: aload 4
/*     */     //   265: invokevirtual 415	com/jcraft/jsch/ChannelSftp:disconnect	()V
/*     */     //   268: aload 5
/*     */     //   270: ifnull +16 -> 286
/*     */     //   273: aload 5
/*     */     //   275: invokevirtual 418	com/jcraft/jsch/Session:isConnected	()Z
/*     */     //   278: ifeq +8 -> 286
/*     */     //   281: aload 5
/*     */     //   283: invokevirtual 421	com/jcraft/jsch/Session:disconnect	()V
/*     */     //   286: aconst_null
/*     */     //   287: astore 4
/*     */     //   289: aconst_null
/*     */     //   290: astore_1
/*     */     //   291: aconst_null
/*     */     //   292: astore 5
/*     */     //   294: getstatic 20	com/americanexpress/ips/gfsg/processor/iso/FTPRequestProcessor:LOGGER	Lorg/apache/log4j/Logger;
/*     */     //   297: ldc_w 422
/*     */     //   300: invokevirtual 52	org/apache/log4j/Logger:info	(Ljava/lang/Object;)V
/*     */     //   303: iload_3
/*     */     //   304: ireturn
/*     */     // Line number table:
/*     */     //   Java source line #428	-> byte code offset #0
/*     */     //   Java source line #431	-> byte code offset #8
/*     */     //   Java source line #432	-> byte code offset #11
/*     */     //   Java source line #437	-> byte code offset #14
/*     */     //   Java source line #439	-> byte code offset #20
/*     */     //   Java source line #440	-> byte code offset #28
/*     */     //   Java source line #442	-> byte code offset #37
/*     */     //   Java source line #445	-> byte code offset #43
/*     */     //   Java source line #448	-> byte code offset #53
/*     */     //   Java source line #450	-> byte code offset #64
/*     */     //   Java source line #452	-> byte code offset #73
/*     */     //   Java source line #453	-> byte code offset #86
/*     */     //   Java source line #452	-> byte code offset #96
/*     */     //   Java source line #455	-> byte code offset #99
/*     */     //   Java source line #456	-> byte code offset #101
/*     */     //   Java source line #458	-> byte code offset #109
/*     */     //   Java source line #459	-> byte code offset #111
/*     */     //   Java source line #460	-> byte code offset #124
/*     */     //   Java source line #459	-> byte code offset #135
/*     */     //   Java source line #462	-> byte code offset #138
/*     */     //   Java source line #463	-> byte code offset #151
/*     */     //   Java source line #464	-> byte code offset #153
/*     */     //   Java source line #465	-> byte code offset #156
/*     */     //   Java source line #466	-> byte code offset #159
/*     */     //   Java source line #464	-> byte code offset #161
/*     */     //   Java source line #467	-> byte code offset #164
/*     */     //   Java source line #468	-> byte code offset #175
/*     */     //   Java source line #469	-> byte code offset #177
/*     */     //   Java source line #470	-> byte code offset #180
/*     */     //   Java source line #469	-> byte code offset #185
/*     */     //   Java source line #471	-> byte code offset #188
/*     */     //   Java source line #472	-> byte code offset #201
/*     */     //   Java source line #474	-> byte code offset #203
/*     */     //   Java source line #475	-> byte code offset #216
/*     */     //   Java source line #477	-> byte code offset #221
/*     */     //   Java source line #478	-> byte code offset #234
/*     */     //   Java source line #480	-> byte code offset #239
/*     */     //   Java source line #481	-> byte code offset #242
/*     */     //   Java source line #482	-> byte code offset #244
/*     */     //   Java source line #483	-> byte code offset #247
/*     */     //   Java source line #474	-> byte code offset #250
/*     */     //   Java source line #475	-> byte code offset #263
/*     */     //   Java source line #477	-> byte code offset #268
/*     */     //   Java source line #478	-> byte code offset #281
/*     */     //   Java source line #480	-> byte code offset #286
/*     */     //   Java source line #481	-> byte code offset #289
/*     */     //   Java source line #482	-> byte code offset #291
/*     */     //   Java source line #485	-> byte code offset #294
/*     */     //   Java source line #486	-> byte code offset #303
/*     */     // Local variable table:
/*     */     //   start	length	slot	name	signature
/*     */     //   0	305	0	this	FTPRequestProcessor
/*     */     //   0	305	1	uploadFile	File
/*     */     //   0	305	2	ftpRemoteDirectory	String
/*     */     //   100	2	3	isUploded	boolean
/*     */     //   250	1	3	isUploded	boolean
/*     */     //   294	10	3	isUploded	boolean
/*     */     //   9	279	4	sftpChannel	ChannelSftp
/*     */     //   12	281	5	ftpSession	Session
/*     */     //   51	51	6	fileInputStream	java.io.FileInputStream
/*     */     //   250	1	6	fileInputStream	java.io.FileInputStream
/*     */     //   294	1	6	fileInputStream	java.io.FileInputStream
/*     */     //   109	37	7	fnfe	FileNotFoundException
/*     */     //   151	9	7	sftpe	com.jcraft.jsch.SftpException
/*     */     //   175	21	7	e	Exception
/*     */     //   201	47	8	localObject	Object
/*     */     // Exception table:
/*     */     //   from	to	target	type
/*     */     //   14	106	109	java/io/FileNotFoundException
/*     */     //   14	106	151	com/jcraft/jsch/SftpException
/*     */     //   14	106	175	java/lang/Exception
/*     */     //   14	201	201	finally
/*     */   }
/*     */   
/*     */   /* Error */
/*     */   private List<File> downloadFileFromSFTP(String downloadFilePath, String localDirectoryPath)
/*     */     throws SettlementException
/*     */   {
/*     */     // Byte code:
/*     */     //   0: getstatic 20	com/americanexpress/ips/gfsg/processor/iso/FTPRequestProcessor:LOGGER	Lorg/apache/log4j/Logger;
/*     */     //   3: ldc -90
/*     */     //   5: invokevirtual 52	org/apache/log4j/Logger:info	(Ljava/lang/Object;)V
/*     */     //   8: ldc_w 438
/*     */     //   11: astore_3
/*     */     //   12: aconst_null
/*     */     //   13: astore 4
/*     */     //   15: aconst_null
/*     */     //   16: astore 5
/*     */     //   18: new 239	java/util/ArrayList
/*     */     //   21: dup
/*     */     //   22: invokespecial 241	java/util/ArrayList:<init>	()V
/*     */     //   25: astore 6
/*     */     //   27: aconst_null
/*     */     //   28: astore 7
/*     */     //   30: aconst_null
/*     */     //   31: astore 8
/*     */     //   33: aconst_null
/*     */     //   34: astore 9
/*     */     //   36: aconst_null
/*     */     //   37: astore 10
/*     */     //   39: aconst_null
/*     */     //   40: astore 11
/*     */     //   42: aload_0
/*     */     //   43: invokespecial 367	com/americanexpress/ips/gfsg/processor/iso/FTPRequestProcessor:getFtpSession	()Lcom/jcraft/jsch/Session;
/*     */     //   46: astore 10
/*     */     //   48: aload_0
/*     */     //   49: aload 10
/*     */     //   51: invokespecial 371	com/americanexpress/ips/gfsg/processor/iso/FTPRequestProcessor:getSftpChannel	(Lcom/jcraft/jsch/Session;)Lcom/jcraft/jsch/ChannelSftp;
/*     */     //   54: astore 9
/*     */     //   56: aload 9
/*     */     //   58: aload_3
/*     */     //   59: invokevirtual 377	com/jcraft/jsch/ChannelSftp:cd	(Ljava/lang/String;)V
/*     */     //   62: aload 9
/*     */     //   64: aload_3
/*     */     //   65: invokevirtual 440	com/jcraft/jsch/ChannelSftp:ls	(Ljava/lang/String;)Ljava/util/Vector;
/*     */     //   68: astore 5
/*     */     //   70: new 59	java/lang/StringBuffer
/*     */     //   73: dup
/*     */     //   74: invokespecial 61	java/lang/StringBuffer:<init>	()V
/*     */     //   77: astore 8
/*     */     //   79: getstatic 20	com/americanexpress/ips/gfsg/processor/iso/FTPRequestProcessor:LOGGER	Lorg/apache/log4j/Logger;
/*     */     //   82: new 174	java/lang/StringBuilder
/*     */     //   85: dup
/*     */     //   86: ldc_w 444
/*     */     //   89: invokespecial 178	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
/*     */     //   92: invokestatic 168	com/americanexpress/ips/gfsg/connection/PropertyReader:getFtpLocalDownloadFilePath	()Ljava/lang/String;
/*     */     //   95: invokevirtual 181	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
/*     */     //   98: invokevirtual 184	java/lang/StringBuilder:toString	()Ljava/lang/String;
/*     */     //   101: invokevirtual 52	org/apache/log4j/Logger:info	(Ljava/lang/Object;)V
/*     */     //   104: aload 5
/*     */     //   106: invokevirtual 446	java/util/Vector:isEmpty	()Z
/*     */     //   109: ifeq +15 -> 124
/*     */     //   112: getstatic 20	com/americanexpress/ips/gfsg/processor/iso/FTPRequestProcessor:LOGGER	Lorg/apache/log4j/Logger;
/*     */     //   115: ldc_w 449
/*     */     //   118: invokevirtual 52	org/apache/log4j/Logger:info	(Ljava/lang/Object;)V
/*     */     //   121: goto +355 -> 476
/*     */     //   124: iconst_0
/*     */     //   125: istore 12
/*     */     //   127: goto +183 -> 310
/*     */     //   130: aconst_null
/*     */     //   131: astore 7
/*     */     //   133: aload 5
/*     */     //   135: iload 12
/*     */     //   137: invokevirtual 451	java/util/Vector:get	(I)Ljava/lang/Object;
/*     */     //   140: checkcast 455	com/jcraft/jsch/ChannelSftp$LsEntry
/*     */     //   143: astore 7
/*     */     //   145: aconst_null
/*     */     //   146: astore 11
/*     */     //   148: aload 7
/*     */     //   150: invokevirtual 457	com/jcraft/jsch/ChannelSftp$LsEntry:getFilename	()Ljava/lang/String;
/*     */     //   153: astore 11
/*     */     //   155: aload 8
/*     */     //   157: iconst_0
/*     */     //   158: invokevirtual 460	java/lang/StringBuffer:setLength	(I)V
/*     */     //   161: aconst_null
/*     */     //   162: astore 4
/*     */     //   164: getstatic 20	com/americanexpress/ips/gfsg/processor/iso/FTPRequestProcessor:LOGGER	Lorg/apache/log4j/Logger;
/*     */     //   167: new 174	java/lang/StringBuilder
/*     */     //   170: dup
/*     */     //   171: ldc_w 464
/*     */     //   174: invokespecial 178	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
/*     */     //   177: aload 11
/*     */     //   179: invokevirtual 181	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
/*     */     //   182: invokevirtual 184	java/lang/StringBuilder:toString	()Ljava/lang/String;
/*     */     //   185: invokevirtual 52	org/apache/log4j/Logger:info	(Ljava/lang/Object;)V
/*     */     //   188: ldc_w 466
/*     */     //   191: aload 11
/*     */     //   193: invokevirtual 225	java/lang/String:equals	(Ljava/lang/Object;)Z
/*     */     //   196: ifne +111 -> 307
/*     */     //   199: ldc_w 468
/*     */     //   202: aload 11
/*     */     //   204: invokevirtual 225	java/lang/String:equals	(Ljava/lang/Object;)Z
/*     */     //   207: ifne +100 -> 307
/*     */     //   210: aload_2
/*     */     //   211: ldc -14
/*     */     //   213: invokevirtual 42	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
/*     */     //   216: ifne +19 -> 235
/*     */     //   219: aload 8
/*     */     //   221: aload_2
/*     */     //   222: invokevirtual 102	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
/*     */     //   225: pop
/*     */     //   226: aload 8
/*     */     //   228: getstatic 470	java/io/File:separator	Ljava/lang/String;
/*     */     //   231: invokevirtual 102	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
/*     */     //   234: pop
/*     */     //   235: aload 8
/*     */     //   237: aload 11
/*     */     //   239: invokevirtual 102	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
/*     */     //   242: pop
/*     */     //   243: new 272	java/io/File
/*     */     //   246: dup
/*     */     //   247: aload 8
/*     */     //   249: invokevirtual 109	java/lang/StringBuffer:toString	()Ljava/lang/String;
/*     */     //   252: invokespecial 283	java/io/File:<init>	(Ljava/lang/String;)V
/*     */     //   255: astore 4
/*     */     //   257: aload 9
/*     */     //   259: aload 11
/*     */     //   261: new 473	java/io/FileOutputStream
/*     */     //   264: dup
/*     */     //   265: aload 4
/*     */     //   267: invokespecial 475	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
/*     */     //   270: invokevirtual 476	com/jcraft/jsch/ChannelSftp:get	(Ljava/lang/String;Ljava/io/OutputStream;)V
/*     */     //   273: aload 6
/*     */     //   275: aload 4
/*     */     //   277: invokeinterface 265 2 0
/*     */     //   282: pop
/*     */     //   283: getstatic 20	com/americanexpress/ips/gfsg/processor/iso/FTPRequestProcessor:LOGGER	Lorg/apache/log4j/Logger;
/*     */     //   286: new 174	java/lang/StringBuilder
/*     */     //   289: dup
/*     */     //   290: ldc_w 479
/*     */     //   293: invokespecial 178	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
/*     */     //   296: aload 11
/*     */     //   298: invokevirtual 181	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
/*     */     //   301: invokevirtual 184	java/lang/StringBuilder:toString	()Ljava/lang/String;
/*     */     //   304: invokevirtual 52	org/apache/log4j/Logger:info	(Ljava/lang/Object;)V
/*     */     //   307: iinc 12 1
/*     */     //   310: iload 12
/*     */     //   312: aload 5
/*     */     //   314: invokevirtual 481	java/util/Vector:size	()I
/*     */     //   317: if_icmplt -187 -> 130
/*     */     //   320: goto +156 -> 476
/*     */     //   323: astore 12
/*     */     //   325: getstatic 20	com/americanexpress/ips/gfsg/processor/iso/FTPRequestProcessor:LOGGER	Lorg/apache/log4j/Logger;
/*     */     //   328: new 174	java/lang/StringBuilder
/*     */     //   331: dup
/*     */     //   332: ldc_w 401
/*     */     //   335: invokespecial 178	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
/*     */     //   338: aload 12
/*     */     //   340: invokevirtual 403	java/io/FileNotFoundException:getMessage	()Ljava/lang/String;
/*     */     //   343: invokevirtual 181	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
/*     */     //   346: invokevirtual 184	java/lang/StringBuilder:toString	()Ljava/lang/String;
/*     */     //   349: invokevirtual 404	org/apache/log4j/Logger:fatal	(Ljava/lang/Object;)V
/*     */     //   352: new 32	com/americanexpress/ips/gfsg/exceptions/SettlementException
/*     */     //   355: dup
/*     */     //   356: ldc_w 406
/*     */     //   359: aload 12
/*     */     //   361: invokespecial 126	com/americanexpress/ips/gfsg/exceptions/SettlementException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
/*     */     //   364: athrow
/*     */     //   365: astore 12
/*     */     //   367: getstatic 20	com/americanexpress/ips/gfsg/processor/iso/FTPRequestProcessor:LOGGER	Lorg/apache/log4j/Logger;
/*     */     //   370: ldc_w 485
/*     */     //   373: aload 12
/*     */     //   375: invokevirtual 122	org/apache/log4j/Logger:fatal	(Ljava/lang/Object;Ljava/lang/Throwable;)V
/*     */     //   378: new 32	com/americanexpress/ips/gfsg/exceptions/SettlementException
/*     */     //   381: dup
/*     */     //   382: ldc_w 487
/*     */     //   385: aload 12
/*     */     //   387: invokespecial 126	com/americanexpress/ips/gfsg/exceptions/SettlementException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
/*     */     //   390: athrow
/*     */     //   391: astore 12
/*     */     //   393: getstatic 20	com/americanexpress/ips/gfsg/processor/iso/FTPRequestProcessor:LOGGER	Lorg/apache/log4j/Logger;
/*     */     //   396: ldc_w 485
/*     */     //   399: aload 12
/*     */     //   401: invokevirtual 489	org/apache/log4j/Logger:error	(Ljava/lang/Object;Ljava/lang/Throwable;)V
/*     */     //   404: new 32	com/americanexpress/ips/gfsg/exceptions/SettlementException
/*     */     //   407: dup
/*     */     //   408: ldc_w 487
/*     */     //   411: aload 12
/*     */     //   413: invokespecial 126	com/americanexpress/ips/gfsg/exceptions/SettlementException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
/*     */     //   416: athrow
/*     */     //   417: astore 13
/*     */     //   419: aload 9
/*     */     //   421: ifnull +16 -> 437
/*     */     //   424: aload 9
/*     */     //   426: invokevirtual 412	com/jcraft/jsch/ChannelSftp:isConnected	()Z
/*     */     //   429: ifeq +8 -> 437
/*     */     //   432: aload 9
/*     */     //   434: invokevirtual 415	com/jcraft/jsch/ChannelSftp:disconnect	()V
/*     */     //   437: aload 10
/*     */     //   439: ifnull +16 -> 455
/*     */     //   442: aload 10
/*     */     //   444: invokevirtual 418	com/jcraft/jsch/Session:isConnected	()Z
/*     */     //   447: ifeq +8 -> 455
/*     */     //   450: aload 10
/*     */     //   452: invokevirtual 421	com/jcraft/jsch/Session:disconnect	()V
/*     */     //   455: aconst_null
/*     */     //   456: astore 5
/*     */     //   458: aconst_null
/*     */     //   459: astore 7
/*     */     //   461: aconst_null
/*     */     //   462: astore 8
/*     */     //   464: aconst_null
/*     */     //   465: astore 9
/*     */     //   467: aconst_null
/*     */     //   468: astore 10
/*     */     //   470: aconst_null
/*     */     //   471: astore 11
/*     */     //   473: aload 13
/*     */     //   475: athrow
/*     */     //   476: aload 9
/*     */     //   478: ifnull +16 -> 494
/*     */     //   481: aload 9
/*     */     //   483: invokevirtual 412	com/jcraft/jsch/ChannelSftp:isConnected	()Z
/*     */     //   486: ifeq +8 -> 494
/*     */     //   489: aload 9
/*     */     //   491: invokevirtual 415	com/jcraft/jsch/ChannelSftp:disconnect	()V
/*     */     //   494: aload 10
/*     */     //   496: ifnull +16 -> 512
/*     */     //   499: aload 10
/*     */     //   501: invokevirtual 418	com/jcraft/jsch/Session:isConnected	()Z
/*     */     //   504: ifeq +8 -> 512
/*     */     //   507: aload 10
/*     */     //   509: invokevirtual 421	com/jcraft/jsch/Session:disconnect	()V
/*     */     //   512: aconst_null
/*     */     //   513: astore 5
/*     */     //   515: aconst_null
/*     */     //   516: astore 7
/*     */     //   518: aconst_null
/*     */     //   519: astore 8
/*     */     //   521: aconst_null
/*     */     //   522: astore 9
/*     */     //   524: aconst_null
/*     */     //   525: astore 10
/*     */     //   527: aconst_null
/*     */     //   528: astore 11
/*     */     //   530: getstatic 20	com/americanexpress/ips/gfsg/processor/iso/FTPRequestProcessor:LOGGER	Lorg/apache/log4j/Logger;
/*     */     //   533: ldc_w 422
/*     */     //   536: invokevirtual 52	org/apache/log4j/Logger:info	(Ljava/lang/Object;)V
/*     */     //   539: aload 6
/*     */     //   541: areturn
/*     */     // Line number table:
/*     */     //   Java source line #508	-> byte code offset #0
/*     */     //   Java source line #509	-> byte code offset #8
/*     */     //   Java source line #511	-> byte code offset #12
/*     */     //   Java source line #513	-> byte code offset #15
/*     */     //   Java source line #514	-> byte code offset #18
/*     */     //   Java source line #515	-> byte code offset #27
/*     */     //   Java source line #516	-> byte code offset #30
/*     */     //   Java source line #517	-> byte code offset #33
/*     */     //   Java source line #518	-> byte code offset #36
/*     */     //   Java source line #519	-> byte code offset #39
/*     */     //   Java source line #523	-> byte code offset #42
/*     */     //   Java source line #525	-> byte code offset #48
/*     */     //   Java source line #527	-> byte code offset #56
/*     */     //   Java source line #529	-> byte code offset #62
/*     */     //   Java source line #530	-> byte code offset #70
/*     */     //   Java source line #532	-> byte code offset #79
/*     */     //   Java source line #533	-> byte code offset #92
/*     */     //   Java source line #532	-> byte code offset #95
/*     */     //   Java source line #535	-> byte code offset #104
/*     */     //   Java source line #536	-> byte code offset #112
/*     */     //   Java source line #541	-> byte code offset #124
/*     */     //   Java source line #542	-> byte code offset #130
/*     */     //   Java source line #543	-> byte code offset #133
/*     */     //   Java source line #544	-> byte code offset #145
/*     */     //   Java source line #545	-> byte code offset #148
/*     */     //   Java source line #546	-> byte code offset #155
/*     */     //   Java source line #547	-> byte code offset #161
/*     */     //   Java source line #548	-> byte code offset #164
/*     */     //   Java source line #549	-> byte code offset #177
/*     */     //   Java source line #548	-> byte code offset #185
/*     */     //   Java source line #552	-> byte code offset #188
/*     */     //   Java source line #553	-> byte code offset #199
/*     */     //   Java source line #558	-> byte code offset #210
/*     */     //   Java source line #560	-> byte code offset #219
/*     */     //   Java source line #561	-> byte code offset #226
/*     */     //   Java source line #564	-> byte code offset #235
/*     */     //   Java source line #567	-> byte code offset #243
/*     */     //   Java source line #570	-> byte code offset #257
/*     */     //   Java source line #571	-> byte code offset #265
/*     */     //   Java source line #570	-> byte code offset #270
/*     */     //   Java source line #573	-> byte code offset #273
/*     */     //   Java source line #575	-> byte code offset #283
/*     */     //   Java source line #541	-> byte code offset #307
/*     */     //   Java source line #581	-> byte code offset #323
/*     */     //   Java source line #582	-> byte code offset #325
/*     */     //   Java source line #583	-> byte code offset #338
/*     */     //   Java source line #582	-> byte code offset #349
/*     */     //   Java source line #586	-> byte code offset #352
/*     */     //   Java source line #587	-> byte code offset #365
/*     */     //   Java source line #588	-> byte code offset #367
/*     */     //   Java source line #589	-> byte code offset #370
/*     */     //   Java source line #590	-> byte code offset #373
/*     */     //   Java source line #588	-> byte code offset #375
/*     */     //   Java source line #591	-> byte code offset #378
/*     */     //   Java source line #592	-> byte code offset #391
/*     */     //   Java source line #594	-> byte code offset #393
/*     */     //   Java source line #595	-> byte code offset #396
/*     */     //   Java source line #596	-> byte code offset #399
/*     */     //   Java source line #594	-> byte code offset #401
/*     */     //   Java source line #597	-> byte code offset #404
/*     */     //   Java source line #598	-> byte code offset #417
/*     */     //   Java source line #600	-> byte code offset #419
/*     */     //   Java source line #601	-> byte code offset #432
/*     */     //   Java source line #603	-> byte code offset #437
/*     */     //   Java source line #604	-> byte code offset #450
/*     */     //   Java source line #608	-> byte code offset #455
/*     */     //   Java source line #609	-> byte code offset #458
/*     */     //   Java source line #610	-> byte code offset #461
/*     */     //   Java source line #611	-> byte code offset #464
/*     */     //   Java source line #612	-> byte code offset #467
/*     */     //   Java source line #613	-> byte code offset #470
/*     */     //   Java source line #614	-> byte code offset #473
/*     */     //   Java source line #600	-> byte code offset #476
/*     */     //   Java source line #601	-> byte code offset #489
/*     */     //   Java source line #603	-> byte code offset #494
/*     */     //   Java source line #604	-> byte code offset #507
/*     */     //   Java source line #608	-> byte code offset #512
/*     */     //   Java source line #609	-> byte code offset #515
/*     */     //   Java source line #610	-> byte code offset #518
/*     */     //   Java source line #611	-> byte code offset #521
/*     */     //   Java source line #612	-> byte code offset #524
/*     */     //   Java source line #613	-> byte code offset #527
/*     */     //   Java source line #615	-> byte code offset #530
/*     */     //   Java source line #617	-> byte code offset #539
/*     */     // Local variable table:
/*     */     //   start	length	slot	name	signature
/*     */     //   0	542	0	this	FTPRequestProcessor
/*     */     //   0	542	1	downloadFilePath	String
/*     */     //   0	542	2	localDirectoryPath	String
/*     */     //   11	54	3	ftpRemoteDirectory	String
/*     */     //   13	263	4	file	File
/*     */     //   16	498	5	vectorFiles	java.util.Vector<?>
/*     */     //   25	515	6	fileList	List<File>
/*     */     //   28	489	7	lsEntry	com.jcraft.jsch.ChannelSftp.LsEntry
/*     */     //   31	489	8	outputFileName	StringBuffer
/*     */     //   34	489	9	sftpChannel	ChannelSftp
/*     */     //   37	489	10	ftpSession	Session
/*     */     //   40	489	11	remoteFileName	String
/*     */     //   125	186	12	i	int
/*     */     //   323	37	12	fnfe	FileNotFoundException
/*     */     //   365	21	12	sftpe	com.jcraft.jsch.SftpException
/*     */     //   391	21	12	e	Exception
/*     */     //   417	57	13	localObject	Object
/*     */     // Exception table:
/*     */     //   from	to	target	type
/*     */     //   42	320	323	java/io/FileNotFoundException
/*     */     //   42	320	365	com/jcraft/jsch/SftpException
/*     */     //   42	320	391	java/lang/Exception
/*     */     //   42	417	417	finally
/*     */   }
/*     */   
/*     */   /* Error */
/*     */   private Session getFtpSession()
/*     */     throws SettlementException
/*     */   {
/*     */     // Byte code:
/*     */     //   0: new 502	com/americanexpress/ips/gfsg/beans/EncryptedData
/*     */     //   3: dup
/*     */     //   4: invokespecial 504	com/americanexpress/ips/gfsg/beans/EncryptedData:<init>	()V
/*     */     //   7: astore_1
/*     */     //   8: aload_0
/*     */     //   9: invokestatic 505	com/americanexpress/ips/gfsg/connection/PropertyReader:getFtpUserID	()Ljava/lang/String;
/*     */     //   12: invokestatic 508	com/americanexpress/ips/gfsg/connection/PropertyReader:getFtpPassword	()Ljava/lang/String;
/*     */     //   15: invokespecial 511	com/americanexpress/ips/gfsg/processor/iso/FTPRequestProcessor:decryptSFTCredentials	(Ljava/lang/String;Ljava/lang/String;)Lcom/americanexpress/ips/gfsg/beans/EncryptedData;
/*     */     //   18: astore_1
/*     */     //   19: getstatic 20	com/americanexpress/ips/gfsg/processor/iso/FTPRequestProcessor:LOGGER	Lorg/apache/log4j/Logger;
/*     */     //   22: ldc -90
/*     */     //   24: invokevirtual 52	org/apache/log4j/Logger:info	(Ljava/lang/Object;)V
/*     */     //   27: invokestatic 515	com/americanexpress/ips/gfsg/connection/PropertyReader:getFtpURL	()Ljava/lang/String;
/*     */     //   30: astore_2
/*     */     //   31: getstatic 20	com/americanexpress/ips/gfsg/processor/iso/FTPRequestProcessor:LOGGER	Lorg/apache/log4j/Logger;
/*     */     //   34: new 174	java/lang/StringBuilder
/*     */     //   37: dup
/*     */     //   38: ldc_w 518
/*     */     //   41: invokespecial 178	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
/*     */     //   44: aload_2
/*     */     //   45: invokevirtual 181	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
/*     */     //   48: invokevirtual 184	java/lang/StringBuilder:toString	()Ljava/lang/String;
/*     */     //   51: invokevirtual 52	org/apache/log4j/Logger:info	(Ljava/lang/Object;)V
/*     */     //   54: new 520	java/lang/Integer
/*     */     //   57: dup
/*     */     //   58: invokestatic 522	com/americanexpress/ips/gfsg/connection/PropertyReader:getFtpPort	()Ljava/lang/String;
/*     */     //   61: invokespecial 525	java/lang/Integer:<init>	(Ljava/lang/String;)V
/*     */     //   64: invokevirtual 526	java/lang/Integer:intValue	()I
/*     */     //   67: istore_3
/*     */     //   68: getstatic 20	com/americanexpress/ips/gfsg/processor/iso/FTPRequestProcessor:LOGGER	Lorg/apache/log4j/Logger;
/*     */     //   71: new 174	java/lang/StringBuilder
/*     */     //   74: dup
/*     */     //   75: ldc_w 529
/*     */     //   78: invokespecial 178	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
/*     */     //   81: iload_3
/*     */     //   82: invokevirtual 531	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
/*     */     //   85: invokevirtual 184	java/lang/StringBuilder:toString	()Ljava/lang/String;
/*     */     //   88: invokevirtual 52	org/apache/log4j/Logger:info	(Ljava/lang/Object;)V
/*     */     //   91: aload_1
/*     */     //   92: invokevirtual 534	com/americanexpress/ips/gfsg/beans/EncryptedData:getDecryptedUserID	()Ljava/lang/String;
/*     */     //   95: invokevirtual 537	java/lang/String:trim	()Ljava/lang/String;
/*     */     //   98: astore 4
/*     */     //   100: aload_1
/*     */     //   101: invokevirtual 540	com/americanexpress/ips/gfsg/beans/EncryptedData:getDecryptedPwd	()Ljava/lang/String;
/*     */     //   104: invokevirtual 537	java/lang/String:trim	()Ljava/lang/String;
/*     */     //   107: astore 5
/*     */     //   109: aconst_null
/*     */     //   110: astore 6
/*     */     //   112: aconst_null
/*     */     //   113: astore 7
/*     */     //   115: aconst_null
/*     */     //   116: astore 8
/*     */     //   118: new 543	com/jcraft/jsch/JSch
/*     */     //   121: dup
/*     */     //   122: invokespecial 545	com/jcraft/jsch/JSch:<init>	()V
/*     */     //   125: astore 6
/*     */     //   127: aload 6
/*     */     //   129: aload_2
/*     */     //   130: invokevirtual 546	com/jcraft/jsch/JSch:setKnownHosts	(Ljava/lang/String;)V
/*     */     //   133: aload 6
/*     */     //   135: aload 4
/*     */     //   137: aload_2
/*     */     //   138: iload_3
/*     */     //   139: invokevirtual 549	com/jcraft/jsch/JSch:getSession	(Ljava/lang/String;Ljava/lang/String;I)Lcom/jcraft/jsch/Session;
/*     */     //   142: astore 8
/*     */     //   144: aload 8
/*     */     //   146: aload 5
/*     */     //   148: invokevirtual 553	com/jcraft/jsch/Session:setPassword	(Ljava/lang/String;)V
/*     */     //   151: new 556	java/util/Properties
/*     */     //   154: dup
/*     */     //   155: invokespecial 558	java/util/Properties:<init>	()V
/*     */     //   158: astore 7
/*     */     //   160: aload 7
/*     */     //   162: ldc_w 559
/*     */     //   165: ldc_w 561
/*     */     //   168: invokevirtual 563	java/util/Properties:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/*     */     //   171: pop
/*     */     //   172: aload 8
/*     */     //   174: aload 7
/*     */     //   176: invokevirtual 566	com/jcraft/jsch/Session:setConfig	(Ljava/util/Properties;)V
/*     */     //   179: aload 8
/*     */     //   181: invokevirtual 570	com/jcraft/jsch/Session:connect	()V
/*     */     //   184: getstatic 20	com/americanexpress/ips/gfsg/processor/iso/FTPRequestProcessor:LOGGER	Lorg/apache/log4j/Logger;
/*     */     //   187: ldc_w 573
/*     */     //   190: invokevirtual 52	org/apache/log4j/Logger:info	(Ljava/lang/Object;)V
/*     */     //   193: goto +56 -> 249
/*     */     //   196: astore 9
/*     */     //   198: getstatic 20	com/americanexpress/ips/gfsg/processor/iso/FTPRequestProcessor:LOGGER	Lorg/apache/log4j/Logger;
/*     */     //   201: new 174	java/lang/StringBuilder
/*     */     //   204: dup
/*     */     //   205: ldc_w 575
/*     */     //   208: invokespecial 178	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
/*     */     //   211: aload 9
/*     */     //   213: invokevirtual 577	com/jcraft/jsch/JSchException:getMessage	()Ljava/lang/String;
/*     */     //   216: invokevirtual 181	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
/*     */     //   219: invokevirtual 184	java/lang/StringBuilder:toString	()Ljava/lang/String;
/*     */     //   222: invokevirtual 404	org/apache/log4j/Logger:fatal	(Ljava/lang/Object;)V
/*     */     //   225: new 32	com/americanexpress/ips/gfsg/exceptions/SettlementException
/*     */     //   228: dup
/*     */     //   229: ldc_w 580
/*     */     //   232: aload 9
/*     */     //   234: invokespecial 126	com/americanexpress/ips/gfsg/exceptions/SettlementException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
/*     */     //   237: athrow
/*     */     //   238: astore 10
/*     */     //   240: aconst_null
/*     */     //   241: astore 7
/*     */     //   243: aconst_null
/*     */     //   244: astore 6
/*     */     //   246: aload 10
/*     */     //   248: athrow
/*     */     //   249: aconst_null
/*     */     //   250: astore 7
/*     */     //   252: aconst_null
/*     */     //   253: astore 6
/*     */     //   255: getstatic 20	com/americanexpress/ips/gfsg/processor/iso/FTPRequestProcessor:LOGGER	Lorg/apache/log4j/Logger;
/*     */     //   258: ldc_w 422
/*     */     //   261: invokevirtual 52	org/apache/log4j/Logger:info	(Ljava/lang/Object;)V
/*     */     //   264: aload 8
/*     */     //   266: areturn
/*     */     // Line number table:
/*     */     //   Java source line #631	-> byte code offset #0
/*     */     //   Java source line #632	-> byte code offset #8
/*     */     //   Java source line #634	-> byte code offset #19
/*     */     //   Java source line #635	-> byte code offset #27
/*     */     //   Java source line #636	-> byte code offset #31
/*     */     //   Java source line #637	-> byte code offset #54
/*     */     //   Java source line #638	-> byte code offset #68
/*     */     //   Java source line #641	-> byte code offset #91
/*     */     //   Java source line #644	-> byte code offset #100
/*     */     //   Java source line #648	-> byte code offset #109
/*     */     //   Java source line #649	-> byte code offset #112
/*     */     //   Java source line #650	-> byte code offset #115
/*     */     //   Java source line #653	-> byte code offset #118
/*     */     //   Java source line #655	-> byte code offset #127
/*     */     //   Java source line #657	-> byte code offset #133
/*     */     //   Java source line #660	-> byte code offset #144
/*     */     //   Java source line #663	-> byte code offset #151
/*     */     //   Java source line #664	-> byte code offset #160
/*     */     //   Java source line #666	-> byte code offset #172
/*     */     //   Java source line #667	-> byte code offset #179
/*     */     //   Java source line #668	-> byte code offset #184
/*     */     //   Java source line #670	-> byte code offset #196
/*     */     //   Java source line #671	-> byte code offset #198
/*     */     //   Java source line #672	-> byte code offset #211
/*     */     //   Java source line #671	-> byte code offset #222
/*     */     //   Java source line #674	-> byte code offset #225
/*     */     //   Java source line #675	-> byte code offset #232
/*     */     //   Java source line #674	-> byte code offset #234
/*     */     //   Java source line #676	-> byte code offset #238
/*     */     //   Java source line #677	-> byte code offset #240
/*     */     //   Java source line #678	-> byte code offset #243
/*     */     //   Java source line #679	-> byte code offset #246
/*     */     //   Java source line #677	-> byte code offset #249
/*     */     //   Java source line #678	-> byte code offset #252
/*     */     //   Java source line #680	-> byte code offset #255
/*     */     //   Java source line #681	-> byte code offset #264
/*     */     // Local variable table:
/*     */     //   start	length	slot	name	signature
/*     */     //   0	267	0	this	FTPRequestProcessor
/*     */     //   7	94	1	encryptData	EncryptedData
/*     */     //   30	108	2	ftpHost	String
/*     */     //   67	72	3	ftpPort	int
/*     */     //   98	38	4	ftpUserName	String
/*     */     //   107	40	5	ftpPassword	String
/*     */     //   110	144	6	jsch	com.jcraft.jsch.JSch
/*     */     //   113	138	7	properties	java.util.Properties
/*     */     //   116	149	8	ftpSession	Session
/*     */     //   196	37	9	jsche	JSchException
/*     */     //   238	9	10	localObject	Object
/*     */     // Exception table:
/*     */     //   from	to	target	type
/*     */     //   118	193	196	com/jcraft/jsch/JSchException
/*     */     //   118	238	238	finally
/*     */   }
/*     */   
/*     */   private ChannelSftp getSftpChannel(Session ftpSession)
/*     */     throws SettlementException
/*     */   {
/* 693 */     LOGGER.info("LOGGER_ENTER");
/*     */     
/* 695 */     ChannelSftp sftpChannel = null;
/* 696 */     Channel channel = null;
/*     */     
/*     */     try
/*     */     {
/* 700 */       channel = ftpSession.openChannel("sftp");
/* 701 */       channel.connect();
/* 702 */       sftpChannel = (ChannelSftp)channel;
/*     */     }
/*     */     catch (JSchException jsche) {
/* 705 */       LOGGER.fatal("Error creating SFTP connection. Exception is " + 
/* 706 */         jsche.getMessage());
/* 707 */       throw new SettlementException("CRITICAL_SFTP_CONNECTION_FAILED");
/*     */     }
/* 709 */     LOGGER.info("LOGGER_EXIT");
/* 710 */     return sftpChannel;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private File createSettlementFile(String filePath, List<String> settlementRecords)
/*     */     throws IOException
/*     */   {
/* 723 */     File settelmentFile = new File(filePath);
/*     */     
/* 725 */     FileWriter fileWriter = new FileWriter(settelmentFile);
/*     */     
/* 727 */     BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
/* 728 */     if (!settlementRecords.isEmpty()) {
/* 729 */       for (String settlementRecordValue : settlementRecords) {
/* 730 */         bufferedWriter.write(settlementRecordValue);
/* 731 */         bufferedWriter.newLine();
/* 732 */         bufferedWriter.flush();
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 737 */     fileWriter.close();
/* 738 */     return settelmentFile;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private EncryptedData decryptSFTCredentials(String userID, String passWord)
/*     */   {
/* 748 */     String key = "51867F0C0E55766E34E1FE7E053CBFB4";
/* 749 */     EncryptedData objEncryptedData = new EncryptedData();
/*     */     
/*     */ 
/*     */     try
/*     */     {
/* 754 */       Juice juice = new Juice("AES/ECB/NoPadding", "51867F0C0E55766E34E1FE7E053CBFB4");
/* 755 */       String resultDUserID = juice.decrypt(userID);
/* 756 */       String resultDPwd = juice.decrypt(passWord);
/*     */       
/* 758 */       objEncryptedData.setDecryptedUserID(resultDUserID.trim());
/* 759 */       objEncryptedData.setDecryptedPwd(resultDPwd.trim());
/*     */     }
/*     */     catch (Exception e)
/*     */     {
/* 763 */       e.printStackTrace();
/*     */     }
/* 765 */     return objEncryptedData;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\processor\iso\FTPRequestProcessor.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */