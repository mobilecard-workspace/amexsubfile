/*     */ package com.americanexpress.ips.gfsg.processor.iso;
/*     */ 
/*     */ import com.americanexpress.ips.gfsg.beans.ErrorObject;
/*     */ import com.americanexpress.ips.gfsg.beans.SettlementRequestDataObject;
/*     */ import com.americanexpress.ips.gfsg.beans.SettlementResponseDataObject;
/*     */ import com.americanexpress.ips.gfsg.beans.acknowledgement.AcknowledgementReportObject;
/*     */ import com.americanexpress.ips.gfsg.beans.conformation.ConfirmationReportObject;
/*     */ import com.americanexpress.ips.gfsg.connection.PropertyReader;
/*     */ import com.americanexpress.ips.gfsg.exceptions.SettlementException;
/*     */ import java.io.File;
/*     */ import java.util.List;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class RequestProcessorFactory
/*     */ {
/*     */   public static SettlementResponseDataObject processRequest(SettlementRequestDataObject requestDataObject, PropertyReader propertyReader)
/*     */     throws SettlementException
/*     */   {
/*  48 */     SettlementResponseDataObject responseObject = null;
/*  49 */     FTPRequestProcessor ftpRequest = new FTPRequestProcessor();
/*  50 */     responseObject = ftpRequest.processRequest(requestDataObject, propertyReader);
/*  51 */     return responseObject;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static List<File> fetchConfirmationReportInPrintedFormat(PropertyReader propertyReader, List<ErrorObject> connErrorList)
/*     */     throws SettlementException
/*     */   {
/*  70 */     List<File> file = null;
/*     */     
/*  72 */     FTPRequestProcessor ftpRequest = new FTPRequestProcessor();
/*  73 */     file = ftpRequest.fetchConfirmationReportInPrintedFormat(
/*  74 */       propertyReader);
/*     */     
/*  76 */     return file;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static List<AcknowledgementReportObject> fetchAcknowledgementInPrintedFormat(PropertyReader propertyReader, List<ErrorObject> connErrorList)
/*     */     throws SettlementException
/*     */   {
/*  96 */     List<AcknowledgementReportObject> ackReptList = null;
/*  97 */     FTPRequestProcessor ftpRequest = new FTPRequestProcessor();
/*     */     
/*     */ 
/*     */ 
/* 101 */     ackReptList = ftpRequest.fetchAcknowledgementInPrintedFormat(propertyReader, 
/* 102 */       connErrorList);
/* 103 */     return ackReptList;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static List<ConfirmationReportObject> fetchConfirmationReportInRawData(PropertyReader propertyReader, List<ErrorObject> connErrorList)
/*     */     throws SettlementException
/*     */   {
/* 123 */     List<ConfirmationReportObject> confReptList = null;
/*     */     
/* 125 */     FTPRequestProcessor ftpRequest = new FTPRequestProcessor();
/* 126 */     confReptList = ftpRequest.fetchConfirmationReportInRawData(
/* 127 */       propertyReader);
/*     */     
/* 129 */     return confReptList;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\processor\iso\RequestProcessorFactory.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */