package com.americanexpress.ips.gfsg.processor.iso;

import com.americanexpress.ips.gfsg.beans.ErrorObject;
import com.americanexpress.ips.gfsg.beans.SettlementRequestDataObject;
import com.americanexpress.ips.gfsg.beans.SettlementResponseDataObject;
import com.americanexpress.ips.gfsg.beans.acknowledgement.AcknowledgementReportObject;
import com.americanexpress.ips.gfsg.beans.conformation.ConfirmationReportObject;
import com.americanexpress.ips.gfsg.connection.PropertyReader;
import com.americanexpress.ips.gfsg.exceptions.SettlementException;
import java.io.File;
import java.util.List;

public abstract interface IRequestProcessor
{
  public abstract SettlementResponseDataObject processRequest(SettlementRequestDataObject paramSettlementRequestDataObject, PropertyReader paramPropertyReader)
    throws SettlementException;
  
  public abstract List<ConfirmationReportObject> fetchConfirmationReportInRawData(PropertyReader paramPropertyReader)
    throws SettlementException;
  
  public abstract List<File> fetchConfirmationReportInPrintedFormat(PropertyReader paramPropertyReader)
    throws SettlementException;
  
  public abstract List<AcknowledgementReportObject> fetchAcknowledgementInPrintedFormat(PropertyReader paramPropertyReader, List<ErrorObject> paramList)
    throws SettlementException;
}


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\processor\iso\IRequestProcessor.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */