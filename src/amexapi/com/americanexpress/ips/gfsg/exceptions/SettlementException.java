/*    */ package com.americanexpress.ips.gfsg.exceptions;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class SettlementException
/*    */   extends Exception
/*    */ {
/*    */   public SettlementException() {}
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public SettlementException(String message)
/*    */   {
/* 19 */     super(message);
/*    */   }
/*    */   
/*    */   public SettlementException(Throwable cause)
/*    */   {
/* 24 */     super(cause);
/*    */   }
/*    */   
/*    */   public SettlementException(String message, Throwable cause)
/*    */   {
/* 29 */     super(message, cause);
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\exceptions\SettlementException.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */