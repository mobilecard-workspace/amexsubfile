/*    */ package com.americanexpress.ips.gfsg.exceptions;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class SSLConnectionException
/*    */   extends Exception
/*    */ {
/*    */   public SSLConnectionException() {}
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public SSLConnectionException(String message)
/*    */   {
/* 19 */     super(message);
/*    */   }
/*    */   
/*    */   public SSLConnectionException(Throwable cause) {
/* 23 */     super(cause);
/*    */   }
/*    */   
/*    */   public SSLConnectionException(String message, Throwable cause) {
/* 27 */     super(message, cause);
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\exceptions\SSLConnectionException.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */