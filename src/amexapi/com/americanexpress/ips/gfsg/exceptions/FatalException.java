/*    */ package com.americanexpress.ips.gfsg.exceptions;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class FatalException
/*    */   extends Exception
/*    */ {
/*    */   public FatalException() {}
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public FatalException(String message)
/*    */   {
/* 19 */     super(message);
/*    */   }
/*    */   
/*    */   public FatalException(Throwable cause) {
/* 23 */     super(cause);
/*    */   }
/*    */   
/*    */   public FatalException(String message, Throwable cause) {
/* 27 */     super(message, cause);
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\exceptions\FatalException.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */