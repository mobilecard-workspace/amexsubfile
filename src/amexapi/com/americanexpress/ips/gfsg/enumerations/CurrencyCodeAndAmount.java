/*     */ package com.americanexpress.ips.gfsg.enumerations;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public enum CurrencyCodeAndAmount
/*     */ {
/*  14 */   Afghanistan_Afghani("971", ""), 
/*  15 */   Albanian_Lek("008", "099999999999"), 
/*  16 */   Algerian_Dinar("012", "099999999999"), 
/*  17 */   Angolan_Kwanza("973", ""), 
/*  18 */   Argentine_Peso("032", ""), 
/*  19 */   Armenian_Dram("051", ""), 
/*  20 */   Aruban_Guilder("533", "000999999999"), 
/*  21 */   Australian_Dollar("036", "000999999999"), 
/*  22 */   Azerbaijanian_Manat("944", "099999999999"), 
/*  23 */   Bahamian_Dollar("044", "000999999999"), 
/*  24 */   Bahraini_Dinar("048", "099999999999"), 
/*  25 */   Bangladesh_Taka("050", "000999999999"), 
/*  26 */   Barbados_Dollar("052", "000999999999"), 
/*  27 */   Belarusian_Ruble("974", ""), 
/*  28 */   Belize_Dollar("084", "000099999999"), 
/*  29 */   Bermudian_Dollar("060", "000099999999"), 
/*  30 */   Bhutan_Ngultrum("064", ""), 
/*  31 */   Bolivar_Fuerte("937", ""), 
/*  32 */   Bolivian_Boliviano("068", "009999999999"), 
/*  33 */   Bosnian_Mark("977", "099999999999"), 
/*  34 */   Botswana_Pula("072", "099999999999"), 
/*  35 */   Brazilian_Real("986", ""), 
/*  36 */   Brunei_Dollar("096", "000999999999"), 
/*  37 */   Bulgarian_Lev("975", "099999999999"), 
/*  38 */   Burundi_Franc("108", ""), 
/*  39 */   CFA_Franc_BCEAO("952", "099999999999"), 
/*  40 */   CFA_Franc_BEAC("950", "099999999999"), 
/*  41 */   Cambodian_Riel("116", ""), 
/*  42 */   Canadian_Dollar("124", "000999999999"), 
/*  43 */   Cape_Verde_Escudo("132", ""), 
/*  44 */   Cayman_Islands_Dollar("136", "000999999999"), 
/*  45 */   Central_French_Polynesian_Franc("953", ""), 
/*  46 */   Chilean_Peso("152", ""), 
/*  47 */   China_Yuan_Renminbi("156", "000999999999"), 
/*  48 */   Colombian_Peso("170", ""), 
/*  49 */   Comoro_Franc("174", ""), 
/*  50 */   Costa_Rican_Colon("188", "000099999999"), 
/*  51 */   Congolese_Franc("976", ""), 
/*  52 */   Croatian_Kuna("191", "099999999999"), 
/*  53 */   Czech_Koruna("203", "099999999999"), 
/*  54 */   Danish_Krone("208", "099999999999"), 
/*  55 */   Djibouti_Franc("262", ""), 
/*  56 */   Dominican_Republic_Peso("214", "000099999999"), 
/*  57 */   E_Caribbean_Dollar("951", "000999999999"), 
/*  58 */   Egyptian_Pound("818", "099999999999"), 
/*  59 */   El_Salvadorian_Colon("222", "000099999999"), 
/*  60 */   Ethiopian_Birr("230", "099999999999"), 
/*  61 */   Euro("978", "099999999999"), 
/*  62 */   Falkland_Islands_Pound("238", ""), 
/*  63 */   Fiji_Dollar("242", "000999999999"), 
/*  64 */   Gambia_Dalasi("270", "099999999999"), 
/*  65 */   Georgia_Lari("981", "099999999999"), 
/*  66 */   Ghanaian_Cedi("936", ""), 
/*  67 */   Gibraltar_Pound("292", ""), 
/*  68 */   Guatemalan_Quetzal("320", "000099999999"), 
/*  69 */   Guinean_Franc("324", ""), 
/*  70 */   Guyana_Dollar("328", "099999999999"), 
/*  71 */   Haiti_Gourde("332", "000999999999"), 
/*  72 */   Honduran_Lempira("340", "000099999999"), 
/*  73 */   Hong_Kong_Dollar("344", "000999999999"), 
/*  74 */   Hungarian_Forint("348", "099999999999"), 
/*  75 */   Icelandic_Krona("352", "099999999999"), 
/*  76 */   Indian_Rupee("356", "000999999999"), 
/*  77 */   Indonesian_Rupiah("360", ""), 
/*  78 */   Iraqi_Dinar("368", ""), 
/*  79 */   Jamaican_Dollar("388", "009999999999"), 
/*  80 */   Japanese_Yen("392", "000999999999"), 
/*  81 */   Jordanian_Dinar("400", "099999999999"), 
/*  82 */   Kazakhstan_Tenge("398", "099999999999"), 
/*  83 */   Kenyan_Shilling("404", "099999999999"), 
/*  84 */   Kuwaiti_Dinar("414", "099999999999"), 
/*  85 */   Kyrgyzstan_Som("417", "099999999999"), 
/*  86 */   Laotian_Kip("418", ""), 
/*  87 */   Latvian_Lats("428", "099999999999"), 
/*  88 */   Lebanese_Pound("422", "099999999999"), 
/*  89 */   Lesotho_Loti("426", ""), 
/*  90 */   Liberian_Dollar("430", "099999999999"), 
/*  91 */   Libyan_Dinar("434", ""), 
/*  92 */   Lithuanian_Litas("440", "099999999999"), 
/*  93 */   Macao_Pataca("446", ""), 
/*  94 */   Macedonia_Denar("807", "099999999999"), 
/*  95 */   Malagasy_Ariary("969", ""), 
/*  96 */   Malawi_Kwacha("454", "099999999999"), 
/*  97 */   Malaysian_Ringgit("458", "000999999999"), 
/*  98 */   Maldive_Rufiyaa("462", ""), 
/*  99 */   Mauritania_Ouguiya("478", "099999999999"), 
/* 100 */   Mauritius_Rupee("480", "099999999999"), 
/* 101 */   Metical("943", ""), 
/* 102 */   Mexican_Peso("484", "000099999999"), 
/* 103 */   Moldovan_Leu("498", "099999999999"), 
/* 104 */   Mongolian_Tugrik("496", ""), 
/* 105 */   Moroccan_Dirham("504", "099999999999"), 
/* 106 */   Namibian_Dollar("516", ""), 
/* 107 */   Nepalese_Rupee("524", "000999999999"), 
/* 108 */   Netherlands_Antillian_Guilder("532", "000999999999"), 
/* 109 */   New_Israeli_Shekel("376", "099999999999"), 
/* 110 */   New_Taiwan_Dollar("901", "000999999999"), 
/* 111 */   New_Zealand_Dollar("554", "000999999999"), 
/* 112 */   Nicaraguan_Cordoba_Oro("558", "000099999999"), 
/* 113 */   Nigeria_Naira("566", "099999999999"), 
/* 114 */   Norwegian_Krone("578", "099999999999"), 
/* 115 */   Omani_Rial("512", "099999999999"), 
/* 116 */   Pakistan_Rupee("586", "000999999999"), 
/* 117 */   Panama_Balboa("591", "000999999999"), 
/* 118 */   Papua_New_Guinea_Kina("598", "000999999999"), 
/* 119 */   Paraguan_Guarani("600", "000999999999"), 
/* 120 */   Peruvian_Nuevo_Sol("604", "000099999999"), 
/* 121 */   Peso_Uruguayo("858", "000099999999"), 
/* 122 */   Philippine_Peso("608", "000999999999"), 
/* 123 */   Polish_Zloty("985", "099999999999"), 
/* 124 */   Qatari_Rial("634", "099999999999"), 
/* 125 */   Romanian_Leu("946", "099999999999"), 
/* 126 */   Russian_Federation_Ruble("643", "099999999999"), 
/* 127 */   Rwanda_Franc("646", ""), 
/* 128 */   Samoa_Tala("882", "000999999999"), 
/* 129 */   Sao_Tome_and_Principe_Dobra("678", ""), 
/* 130 */   Saudi_Arabian_Riyal("682", "099999999999"), 
/* 131 */   Serbian_Dinar("941", "099999999999"), 
/* 132 */   Seychelles_Rupee("690", "099999999999"), 
/* 133 */   Sierra_Leone_Leone("694", ""), 
/* 134 */   Singapore_Dollar("702", "000999999999"), 
/* 135 */   Solomon_Islands_Dollar("090", "000999999999"), 
/* 136 */   Somali_Shilling("706", "099999999999"), 
/* 137 */   South_African_Rand("710", "099999999999"), 
/* 138 */   South_Korean_Won("410", "000999999999"), 
/* 139 */   Sri_Lanka_Rupee("144", "000999999999"), 
/* 140 */   St_Helena_Pound("654", ""), 
/* 141 */   Surinam_Dollar("968", "000999999999"), 
/* 142 */   Swaziland_Emalengeni("748", ""), 
/* 143 */   Swedish_Krona("752", "099999999999"), 
/* 144 */   Swiss_Franc("756", "099999999999"), 
/* 145 */   Syrian_Pound("760", "099999999999"), 
/* 146 */   Tajik_Somoni("972", "099999999999"), 
/* 147 */   Tanzanian_Shilling("834", "099999999999"), 
/* 148 */   Thailand_Baht("764", "000999999999"), 
/* 149 */   Tongan_Paanga("776", "000999999999"), 
/* 150 */   Trinidad_and_Tobago_Dollar("780", "009999999999"), 
/* 151 */   Tunisian_Dinar("788", "099999999999"), 
/* 152 */   Turkish_Lira("949", "099999999999"), 
/* 153 */   Turkmenistan_NewManat("934", "000999999999"), 
/* 154 */   UAE_Dirham("784", "099999999999"), 
/* 155 */   US_Dollar("840", "000999999999"), 
/* 156 */   Uganda_Shilling("800", ""), 
/* 157 */   Ukraine_Hryvnia("980", "099999999999"), 
/* 158 */   United_Kingdom_Pound_Sterling("826", "099999999999"), 
/* 159 */   Uzbekistan_Som("860", ""), 
/* 160 */   Vanuatu_Vatu("548", "000999999999"), 
/* 161 */   Vietnamese_Dong("704", ""), 
/* 162 */   Yemeni_Rial("886", ""), 
/* 163 */   Zambia_Kwacha("894", "099999999999"), 
/* 164 */   Zimbabwe_Dollar("932", "099999999999");
/*     */   
/*     */ 
/*     */   private final String currencyCd;
/*     */   
/*     */   private final String amount;
/*     */   
/*     */ 
/*     */   private CurrencyCodeAndAmount(String curCd, String amount)
/*     */   {
/* 174 */     this.currencyCd = curCd;
/* 175 */     this.amount = amount;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public String getCurrencyCode()
/*     */   {
/* 182 */     return this.currencyCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public String getAmount()
/*     */   {
/* 189 */     return this.amount;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\enumerations\CurrencyCodeAndAmount.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */