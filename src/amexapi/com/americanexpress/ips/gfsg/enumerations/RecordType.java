/*    */ package com.americanexpress.ips.gfsg.enumerations;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public enum RecordType
/*    */ {
/* 11 */   Transaction_File_Header("TFH"),  Transaction_Advice_Basic("TAB"),  Transaction_Advice_Detail(
/* 12 */     "TAD"),  Transaction_Advice_Addendum("TAA"),  Transaction_Batch_Trailer(
/* 13 */     "TBT"),  Transaction_File_Summary("TFS");
/*    */   
/*    */ 
/*    */ 
/*    */   private final String recordType;
/*    */   
/*    */ 
/*    */   private RecordType(String recordType)
/*    */   {
/* 22 */     this.recordType = recordType;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public String getRecordType()
/*    */   {
/* 29 */     return this.recordType;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\enumerations\RecordType.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */