/*    */ package com.americanexpress.ips.gfsg.enumerations;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public enum VehicleClassCodes
/*    */ {
/* 12 */   Mini("0001"), 
/* 13 */   Subcompact("0002"), 
/* 14 */   Economy("0003"), 
/* 15 */   Compact("0004"), 
/* 16 */   Midsize("0005"), 
/* 17 */   Intermediate("0006"), 
/* 18 */   Standard("0007"), 
/* 19 */   Full_size("0008"), 
/* 20 */   Luxury("0009"), 
/* 21 */   Premium("0010"), 
/* 22 */   Minivan("0011"), 
/* 23 */   passenger_van_12("0012"), 
/* 24 */   Moving_van("0013"), 
/* 25 */   passenger_van_15("0014"), 
/* 26 */   Cargo_van("0015"), 
/* 27 */   foot_truck_12("0016"), 
/* 28 */   foot_truck_20("0017"), 
/* 29 */   foot_truck_24("0018"), 
/* 30 */   foot_truck_26("0019"), 
/* 31 */   Moped("0020"), 
/* 32 */   Stretch("0021"), 
/* 33 */   Regular("0022"), 
/* 34 */   Unique("0023"), 
/* 35 */   Exotic("0024"), 
/* 36 */   Small_medium_truck("0025"), 
/* 37 */   Large_truck("0026"), 
/* 38 */   Small_SUV("0027"), 
/* 39 */   Medium_SUV("0028"), 
/* 40 */   Large_SUV("0029"), 
/* 41 */   Exotic_SUV("0030"), 
/* 42 */   Four_Wheel_Drive("0031"), 
/* 43 */   Special("0032"), 
/* 44 */   Taxi("0099"), 
/* 45 */   Miscellaneous("9999");
/*    */   
/*    */ 
/*    */ 
/*    */   private final String vehicleClassCodes;
/*    */   
/*    */ 
/*    */   public String getVehicleClassCodes()
/*    */   {
/* 54 */     return this.vehicleClassCodes;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   private VehicleClassCodes(String vehicleClassCodes)
/*    */   {
/* 61 */     this.vehicleClassCodes = vehicleClassCodes;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\enumerations\VehicleClassCodes.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */