/*    */ package com.americanexpress.ips.gfsg.enumerations;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public enum FormatCode
/*    */ {
/* 12 */   Airline_TAA_Record("01"),  No_Industry_Specific_TAA_Record("02"), 
/* 13 */   Insurance_TAA_Record("04"),  Auto_Rental_TAA_Record("05"),  Rail_Rental_TAA_Record("06"), 
/* 14 */   Lodging_TAA_Record("11"),  Communication_Services_TAA_Record("13"),  Travel_Cruise_TAA_Record("14"), 
/* 15 */   General_Retail_TAA_Record("20"),  Entertainment_Ticketing_TAA_Record("22");
/*    */   
/*    */ 
/*    */ 
/*    */   private final String formatCode;
/*    */   
/*    */ 
/*    */   private FormatCode(String formatCode)
/*    */   {
/* 24 */     this.formatCode = formatCode;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public String getFormatCode()
/*    */   {
/* 32 */     return this.formatCode;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\enumerations\FormatCode.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */