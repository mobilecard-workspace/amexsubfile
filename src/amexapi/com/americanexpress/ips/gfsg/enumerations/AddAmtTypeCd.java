/*    */ package com.americanexpress.ips.gfsg.enumerations;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public enum AddAmtTypeCd
/*    */ {
/* 15 */   Goods_and_Services_Tax_GST("001"), 
/* 16 */   Consumption_Tax("002"), 
/* 17 */   Provincial_Sales_TaxPST("003"), 
/* 18 */   Quebec_Sales_TaxQST("004"), 
/* 19 */   Harmonized_Sales_TaxHST("005"), 
/* 20 */   Insurance_Premium_TaxIPT("006"), 
/* 21 */   Circulation_of_Merchandise_and_Service_TaxICMS("007"), 
/* 22 */   Industrialized_Products_Federal_Tributary_TaxIPI_Federal_Tributary("008"), 
/* 23 */   Inland_Revenue_Income_Tax_IR_Income_Tax("009"), 
/* 24 */   International_Students_and_Scholars_Income_TaxISS_Income_Tax("010"), 
/* 25 */   Income_Security_and_Reform_Tax_ISR_Income_Tax("011"), 
/* 26 */   Occupancy_Tax("012"), 
/* 27 */   Room_Tax("013"), 
/* 28 */   Surcharge_Tax("014"), 
/* 29 */   Airport_Tax("015"), 
/* 30 */   Ticket_Tax("043"), 
/* 31 */   Miscellaneous_Tax("046"), 
/* 32 */   Sales_Tax("056"), 
/* 33 */   Stamp_Duty("067"), 
/* 34 */   Value_Added_Tax_VAT("057"), 
/* 35 */   Exempt_No_GST_charged("068"), 
/* 36 */   Bar("019"), 
/* 37 */   Bar_MiniBar("023"), 
/* 38 */   Barber_Beauty_Salon("028"), 
/* 39 */   Beverage("017"), 
/* 40 */   Business_Center("036"), 
/* 41 */   Catering_Charges("022"), 
/* 42 */   Convention_Fees("037"), 
/* 43 */   Food("016"), 
/* 44 */   Food_Beverage("018"), 
/* 45 */   Gift_Shop("030"), 
/* 46 */   Health_Fitness("029"), 
/* 47 */   Internet_Service("025"), 
/* 48 */   Insurance_Purchased("052"), 
/* 49 */   Laundry_DryCleaning("027"), 
/* 50 */   Lodging("020"), 
/* 51 */   Movies_PayPerView("026"), 
/* 52 */   Pet_Fees("033"), 
/* 53 */   Phone("024"), 
/* 54 */   Pro_Shop("031"), 
/* 55 */   Restaurant_Room_Service("021"), 
/* 56 */   Reward_Program_Transaction("047"), 
/* 57 */   Tip_Gratuity("058"), 
/* 58 */   Tours("034"), 
/* 59 */   Additional_Miles_Kilometers_Distance("062"), 
/* 60 */   Auto_Rental_Adjustment("060"), 
/* 61 */   Cancellation_Adjustment("065"), 
/* 62 */   Charges_Added_After_CheckOut_Departure("041"), 
/* 63 */   Convenience_Charge("050"), 
/* 64 */   Delivery_Charge("051"), 
/* 65 */   Discount("053"), 
/* 66 */   Equipment_Rental("035"), 
/* 67 */   Express_Service_Charge("040"), 
/* 68 */   Freight_Shipping_Handling("055"), 
/* 69 */   Fuel_Charge("061"), 
/* 70 */   Late_Return("063"), 
/* 71 */   Meeting_Conference_Charges("038"), 
/* 72 */   Misc_Charges_Fees("042"), 
/* 73 */   No_Show_Charge("039"), 
/* 74 */   Order_Processing_Charge("049"), 
/* 75 */   Parking_Fee("032"), 
/* 76 */   Policy_Adjustment("066"), 
/* 77 */   Repairs("064"), 
/* 78 */   Surcharge("048"), 
/* 79 */   Tickets_Violations("054");
/*    */   
/*    */ 
/*    */   private final String addAmtTypeCd;
/*    */   
/*    */ 
/*    */   public String getAddAmtTypeCd()
/*    */   {
/* 87 */     return this.addAmtTypeCd;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   private AddAmtTypeCd(String addAmtTypeCd)
/*    */   {
/* 94 */     this.addAmtTypeCd = addAmtTypeCd;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\enumerations\AddAmtTypeCd.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */