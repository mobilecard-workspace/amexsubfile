/*    */ package com.americanexpress.ips.gfsg.enumerations;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public enum TravPackTypeCd
/*    */ {
/* 12 */   Car_package("C"), 
/* 13 */   Airline_package("A"), 
/* 14 */   Both_car_airline_package("B"), 
/* 15 */   Unknown("N");
/*    */   
/*    */ 
/*    */   private final String travPackTypeCd;
/*    */   
/*    */ 
/*    */   public String getTravPackTypeCd()
/*    */   {
/* 23 */     return this.travPackTypeCd;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   private TravPackTypeCd(String travPackTypeCd)
/*    */   {
/* 30 */     this.travPackTypeCd = travPackTypeCd;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\enumerations\TravPackTypeCd.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */