/*    */ package com.americanexpress.ips.gfsg.enumerations;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public enum SubmissionMethod
/*    */ {
/* 11 */   Amex_POS_Terminal("01"),  Non_Amex_POS_Terminal("02"), 
/* 12 */   Non_Amex_Integrated_POS_Terminal("03"),  EIPP("04"), 
/* 13 */   Paper_external_vendor_paper_processor("05"),  Purchase_express("06"), 
/* 14 */   PayFlow("07"),  Dial_Payment_system("10"),  Automated_Bill_Payment_Platform("13");
/*    */   
/*    */ 
/*    */ 
/*    */   private final String submissionMethod;
/*    */   
/*    */ 
/*    */ 
/*    */   private SubmissionMethod(String submissionMethod)
/*    */   {
/* 24 */     this.submissionMethod = submissionMethod;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public String getSubmissionMethod()
/*    */   {
/* 31 */     return this.submissionMethod;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\enumerations\SubmissionMethod.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */