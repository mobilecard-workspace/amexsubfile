/*    */ package com.americanexpress.ips.gfsg.enumerations;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public enum TaxTypeCd
/*    */ {
/* 12 */   Sales_Tax("056"), 
/* 13 */   All_Taxes("TX");
/*    */   
/*    */ 
/*    */   private final String taxTypeCd;
/*    */   
/*    */ 
/*    */   private TaxTypeCd(String taxTypeCd)
/*    */   {
/* 21 */     this.taxTypeCd = taxTypeCd;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public String getTaxTypeCd()
/*    */   {
/* 28 */     return this.taxTypeCd;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\enumerations\TaxTypeCd.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */