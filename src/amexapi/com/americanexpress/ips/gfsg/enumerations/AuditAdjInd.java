/*    */ package com.americanexpress.ips.gfsg.enumerations;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public enum AuditAdjInd
/*    */ {
/* 12 */   Multiple_adjustments("X"), 
/* 13 */   One_adjustment_only_Cardmember_notified("Y"), 
/* 14 */   One_adjustment_only_Cardmember_not_notified("Z");
/*    */   
/*    */ 
/*    */ 
/*    */   private final String auditAdjInd;
/*    */   
/*    */ 
/*    */   private AuditAdjInd(String auditAdjInd)
/*    */   {
/* 23 */     this.auditAdjInd = auditAdjInd;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public String getAuditAdjInd()
/*    */   {
/* 30 */     return this.auditAdjInd;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\enumerations\AuditAdjInd.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */