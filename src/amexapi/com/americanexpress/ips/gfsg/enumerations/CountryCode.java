/*    */ package com.americanexpress.ips.gfsg.enumerations;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public enum CountryCode
/*    */ {
/* 12 */   Afghanistan("004"),  Aland_Islands("248"),  Albania("008"),  Algeria("012"),  American_Samoa(
/* 13 */     "016"),  Andorra("020"),  Angola("024"),  Anguilla("660"),  Antarctica(
/* 14 */     "010"),  Antigua_And_Barbuda("028"),  Argentina("032"),  Armenia("051"),  Aruba(
/* 15 */     "533"),  Australia("036"),  Austria("040"),  Azerbaijan("031"),  Bahamas(
/* 16 */     "044"),  Bahrain("048"),  Bangladesh("050"),  Barbados("052"),  Belarus(
/* 17 */     "112"),  Belgium("056"),  Belize("084"),  Benin("204"),  Bermuda("060"),  Bhutan(
/* 18 */     "064"),  Bolivia("068"),  Bosnia_And_Herzegovina("070"),  Botswana(
/* 19 */     "072"),  Bouvet_Island("074"),  Brazil("076"),  British_Indian_Ocean_Territory(
/* 20 */     "086"),  Brunei_Darussalam("096"),  Bulgaria("100"),  Burkina_Faso(
/* 21 */     "854"),  Burundi("108"),  Cambodia("116"),  Cameroon("120"),  Canada(
/* 22 */     "124"),  Cape_Verde("132"),  Cayman_Islands("136"),  Central_African_Republic(
/* 23 */     "140"),  Chad("148"),  Chile("152"),  China("156"),  Christmas_Island(
/* 24 */     "162"),  Cocos_Islands("166"),  Colombia("170"),  Comoros("174"),  Republic_Of_The_Congo(
/* 25 */     "178"),  Democratic_Republic_Of_Congo("180"),  Cook_Islands("184"),  Costa_Rica(
/* 26 */     "188"),  Cote_Dvoire("384"),  Republic_Of_Croatia("191"),  Cuba("192"),  Cyprus(
/* 27 */     "196"),  Czech_Republic("203"),  Denmark("208"),  Djibouti("262"),  Dominica(
/* 28 */     "212"),  Dominican_Republic("214"),  Ecuador("218"),  Egypt("818"),  El_Salvador(
/* 29 */     "222"),  EquatorialGuinea("226"),  Eritrea("232"),  Estonia("233"),  Ethiopia(
/* 30 */     "231"),  Falkland_Islands("238"),  Faroe_Islands("234"),  Fiji("242"),  Finland(
/* 31 */     "246"),  France("250"),  French_Guiana("254"),  French_Polynesia("258"),  French_Southern_Territories(
/* 32 */     "260"),  Gabon("266"),  Gambia("270"),  Georgia("268"),  Germany("276"),  Ghana(
/* 33 */     "288"),  Gibraltar("292"),  Greece("300"),  Greenland("304"),  Grenada(
/* 34 */     "308"),  Guadeloupe("312"),  Guam("316"),  Guatemala("320"),  Guernsey(
/* 35 */     "831"),  Guinea("324"),  Guinea_Bissau("624"),  Guyana("328"),  Haiti(
/* 36 */     "332"),  Heard_And_McDonald_Islands("334"),  Honduras("340"),  HongKong(
/* 37 */     "344"),  Hungary("348"),  Iceland("352"),  India("356"),  Indonesia(
/* 38 */     "360"),  Islamic_Republic_Of_Iran("364"),  Iraq("368"),  Ireland("372"),  IsleOfMan(
/* 39 */     "833"),  Israel("376"),  Italy("380"),  Jamaica("388"),  Japan("392"),  Jersey(
/* 40 */     "832"),  Jordan("400"),  Kazakhstan("398"),  Kenya("404"),  Kiribati(
/* 41 */     "296"),  Democratic_Peoples_Republic_Of_Korea("408"),  Republic_Of_Korea(
/* 42 */     "410"),  Kuwait("414"),  Kyrgyzstan("417"),  Lao_Peoples_Democratic_Republic(
/* 43 */     "418"),  Latvia("428"),  Lebanon("422"),  Lesotho("426"),  Liberia(
/* 44 */     "430"),  Libyan_Arab_Jamahiriya("434"),  Liechtenstein("438"),  Lithuania(
/* 45 */     "440"),  Luxembourg("442"),  Macau("446"),  Macedonia("807"),  Madagascar(
/* 46 */     "450"),  Malawi("454"),  Malaysia("458"),  Maldives("462"),  Mali("466"),  Malta(
/* 47 */     "470"),  Marshall_Islands("584"),  Martinique("474"),  Mauritania(
/* 48 */     "478"),  Mauritius("480"),  Mayotte("175"),  Mexico("484"),  Federated_States_Of_Micronesia(
/* 49 */     "583"),  Republic_Of_Moldova("498"),  Monaco("492"),  Mongolia("496"),  Montenegro(
/* 50 */     "499"),  Montserrat("500"),  Morocco("504"),  Mozambique("508"),  Myanmar(
/* 51 */     "104"),  Namibia("516"),  Nauru("520"),  Nepal("524"),  Netherlands(
/* 52 */     "528"),  Netherlands_Antilles("530"),  New_Caledonia("540"),  New_Zealand(
/* 53 */     "554"),  Nicaragua("558"),  Niger("562"),  Nigeria("566"),  Niue("570"),  Norfolk_Island(
/* 54 */     "574"),  Northern_Mariana_Islands("580"),  Norway("578"),  Oman("512"),  Pakistan(
/* 55 */     "586"),  Palau("585"),  Palestinian_Territory_Occupied("275"),  Panama(
/* 56 */     "591"),  Papua_New_Guinea("598"),  Paraguay("600"),  Peru("604"),  Philippines(
/* 57 */     "608"),  Pitcairn("612"),  Poland("616"),  Portugal("620"),  PuertoRico(
/* 58 */     "630"),  Qatar("634"),  Reunion("638"),  Romania("642"),  Russian_Federation(
/* 59 */     "643"),  Rwanda("646"),  Samoa("882"),  SanMarino("674"),  Sao_Tome_And_Principe(
/* 60 */     "678"),  SaudiArabia("682"),  Senegal("686"),  Serbia("688"),  Seychelles(
/* 61 */     "690"),  SierraLeone("694"),  Singapore("702"),  Slovakia("703"),  Slovenia(
/* 62 */     "705"),  SolomonIslands("090"),  Somalia("706"),  SouthAfrica("710"),  South_Georgia_And_South_Sandwich(
/* 63 */     "239"),  Spain("724"),  SriLanka("144"),  St_Barthelemy("652"),  St_Helena(
/* 64 */     "654"),  St_Kitts_Nevis("659"),  St_Lucia("662"),  St_Martin("663"),  St_Pierre_And_Miquelon(
/* 65 */     "666"),  St_Vincent_And_The_Grenadines("670"),  Sudan("736"),  Suriname(
/* 66 */     "740"),  Svalbard_And_Jan_Mayen_Islands("744"),  Swaziland("748"),  Sweden(
/* 67 */     "752"),  Switzerland("756"),  Syrian_Arab_Republic("760"),  Taiwan(
/* 68 */     "158"),  Tajikistan("762"),  United_Republic_Of_Tanzania("834"),  Thailand(
/* 69 */     "764"),  Timor_Leste("626"),  Togo("768"),  Tokelau("772"),  Tonga(
/* 70 */     "776"),  Trinidad_And_Tobago("780"),  Tunisia("788"),  Turkey("792"),  Turkmenistan(
/* 71 */     "795"),  Turksand_Caicos_Islands("796"),  Tuvalu("798"),  US_Minor_Outlying_Islands(
/* 72 */     "581"),  Uganda("800"),  Ukraine("804"),  United_Arab_Emirates("784"),  United_Kingdom(
/* 73 */     "826"),  United_States("840"),  Uruguay("858"),  Uzbekistan("860"),  Vanuatu(
/* 74 */     "548"),  Vatican_City_State("336"),  Venezuela("704"),  Vietnam("704"),  Virgin_Islands_British(
/* 75 */     "092"),  VirginIslandsUS("850"),  Wallis_And_Futuna_Islands("876"),  Western_Sahara(
/* 76 */     "732"),  Yemen("887"),  Zambia("894"),  Zimbabwe("716");
/*    */   
/*    */ 
/*    */ 
/*    */   private final String countryCd;
/*    */   
/*    */ 
/*    */ 
/*    */   private CountryCode(String ctyCd)
/*    */   {
/* 86 */     this.countryCd = ctyCd;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public String getCountryCode()
/*    */   {
/* 93 */     return this.countryCd;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\enumerations\CountryCode.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */