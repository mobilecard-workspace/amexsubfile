/*    */ package com.americanexpress.ips.gfsg.enumerations;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public enum CommunicationsCallTypeCode
/*    */ {
/* 12 */   InternationaToll_Free_Number("ITF"), 
/* 13 */   International_Subscriber_Dialing("ISD"), 
/* 14 */   Trunk_Dialing_Subscriber("TDS"), 
/* 15 */   Anuntimed_charge_service("LOCAL");
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   private final String communicationsCallTypeCode;
/*    */   
/*    */ 
/*    */ 
/*    */   private CommunicationsCallTypeCode(String communicationsCallTypeCode)
/*    */   {
/* 26 */     this.communicationsCallTypeCode = communicationsCallTypeCode;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public String getCommunicationsCallTypeCode()
/*    */   {
/* 34 */     return this.communicationsCallTypeCode;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\enumerations\CommunicationsCallTypeCode.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */