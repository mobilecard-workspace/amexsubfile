/*    */ package com.americanexpress.ips.gfsg.enumerations;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public enum AutoRentalDistanceUnitOfMeasure
/*    */ {
/* 12 */   Miles("M"),  Kilometers("K");
/*    */   
/*    */ 
/*    */ 
/*    */   private final String autoRentalDistanceUnitOfMeasure;
/*    */   
/*    */ 
/*    */   private AutoRentalDistanceUnitOfMeasure(String autoRentalDistanceUnitOfMeasure)
/*    */   {
/* 21 */     this.autoRentalDistanceUnitOfMeasure = autoRentalDistanceUnitOfMeasure;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public String getAutoRentalDistanceUnitOfMeasure()
/*    */   {
/* 28 */     return this.autoRentalDistanceUnitOfMeasure;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\enumerations\AutoRentalDistanceUnitOfMeasure.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */