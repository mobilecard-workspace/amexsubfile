/*    */ package com.americanexpress.ips.gfsg.enumerations;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public enum RateClassCd
/*    */ {
/* 12 */   Day("D"), 
/* 13 */   Evening("E"), 
/* 14 */   Night("N"), 
/* 15 */   Special_Discount("S");
/*    */   
/*    */ 
/*    */   private final String rateClassCd;
/*    */   
/*    */ 
/*    */   private RateClassCd(String rateClassCd)
/*    */   {
/* 23 */     this.rateClassCd = rateClassCd;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public String getRateClassCd()
/*    */   {
/* 30 */     return this.rateClassCd;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\enumerations\RateClassCd.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */