/*    */ package com.americanexpress.ips.gfsg.enumerations;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public enum StopOverIndicator
/*    */ {
/* 12 */   Stopover_permitted("O"), 
/* 13 */   No_stopoverpermitted("X"), 
/* 14 */   Unknown(" ");
/*    */   
/*    */ 
/*    */ 
/*    */   private final String stopOverIndicator;
/*    */   
/*    */ 
/*    */ 
/*    */   private StopOverIndicator(String StopOverIndicator)
/*    */   {
/* 24 */     this.stopOverIndicator = StopOverIndicator;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public String getStopOverIndicator()
/*    */   {
/* 32 */     return this.stopOverIndicator;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\enumerations\StopOverIndicator.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */