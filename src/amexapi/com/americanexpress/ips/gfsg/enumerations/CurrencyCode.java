/*    */ package com.americanexpress.ips.gfsg.enumerations;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public enum CurrencyCode
/*    */ {
/* 14 */   Afghanistan_Afghani("971"),  Albanian_Lek("008"),  Algerian_Dinar("012"),  Angolan_Kwanza(
/* 15 */     "973"),  Argentine_Peso("032"),  Armenian_Dram("051"),  Aruban_Guilder(
/* 16 */     "533"),  Australian_Dollar("036"),  Azerbaijanian_Manat("944"),  Bahamian_Dollar(
/* 17 */     "044"),  Bahraini_Dinar("048"),  Bangladesh_Taka("050"),  Barbados_Dollar(
/* 18 */     "052"),  Belarusian_Ruble("974"),  Belize_Dollar("084"),  Bermudian_Dollar(
/* 19 */     "060"),  Bhutan_Ngultrum("064"),  Bolivar_Fuerte("937"),  Bolivian_Boliviano(
/* 20 */     "068"),  Bosnian_Mark("977"),  Botswana_Pula("072"),  Brazilian_Real(
/* 21 */     "986"),  Brunei_Dollar("096"),  Bulgarian_Lev("975"),  Burundi_Franc(
/* 22 */     "108"),  CFA_Franc_BCEAO("952"),  CFA_Franc_BEAC("950"),  Cambodian_Riel(
/* 23 */     "116"),  Canadian_Dollar("124"),  Cape_Verde_Escudo("132"),  Cayman_Islands_Dollar(
/* 24 */     "136"),  Central_French_Polynesian_Franc("953"),  Chilean_Peso("152"),  China_Yuan_Renminbi(
/* 25 */     "156"),  Colombian_Peso("170"),  Comoro_Franc("174"),  Congolese_Franc(
/* 26 */     "976"),  Costa_Rican_Colon("188"),  Czech_Koruna("203"),  Danish_Krone(
/* 27 */     "208"),  Djibouti_Franc("262"),  Dominican_Republic_Peso("214"),  E_Caribbean_Dollar(
/* 28 */     "951"),  Ecuadorian_Sucre("218"),  Egyptian_Pound("818"),  El_Salvadorian_Colon(
/* 29 */     "222"),  Estonia_Kroon("233"),  Ethiopian_Birr("230"),  Euro("978"),  Falkland_Islands_Pound(
/* 30 */     "238"),  Fiji_Dollar("242"),  Gambia_Dalasi("270"),  Georgia_Lari(
/* 31 */     "981"),  Ghanaian_Cedi("936"),  Gibraltar_Pound("292"),  Guatemalan_Quetzal(
/* 32 */     "320"),  Guinean_Franc("324"),  Guyana_Dollar("328"),  Haiti_Gourde(
/* 33 */     "332"),  Honduran_Lempira("340"),  Hong_Kong_Dollar("344"),  Hungarian_Forint(
/* 34 */     "348"),  Icelandic_Krona("352"),  Indian_Rupee("356"),  Indonesian_Rupiah(
/* 35 */     "360"),  Iraqi_Dinar("368"),  Jamaican_Dollar("388"),  Japanese_Yen(
/* 36 */     "392"),  Jordanian_Dinar("400"),  Kazakhstan_Tenge("398"),  Kenyan_Shilling(
/* 37 */     "404"),  Kuna("191"),  Kuwaiti_Dinar("414"),  Kyrgyzstan_Som("417"),  Laotian_Kip(
/* 38 */     "418"),  Latvian_Lats("428"),  Lebanese_Pound("422"),  Lesotho_Loti(
/* 39 */     "426"),  Liberian_Dollar("430"),  Libyan_Dinar("434"),  Lithuanian_Litas(
/* 40 */     "440"),  Macao_Pataca("446"),  Macedonia_Denar("807"),  Malagasy_Ariary(
/* 41 */     "969"),  Malawi_Kwacha("454"),  Malaysian_Ringgit("458"),  Maldive_Rufiyaa(
/* 42 */     "462"),  Mauritania_Ouguiya("478"),  Mauritius_Rupee("480"),  Metical(
/* 43 */     "943"),  Mexican_Peso("484"),  Moldovan_Leu("498"),  Mongolian_Tugrik(
/* 44 */     "496"),  Moroccan_Dirham("504"),  Namibian_Dollar("516"),  Nepalese_Rupee(
/* 45 */     "524"),  Netherlands_Antillian_Guilder("532"),  New_Israeli_Shekel(
/* 46 */     "376"),  New_Taiwan_Dollar("901"),  New_Zealand_Dollar("554"),  Nicaraguan_Cordoba_Oro(
/* 47 */     "558"),  Nigeria_Naira("566"),  Norwegian_Krone("578"),  Omani_Rial(
/* 48 */     "512"),  Pakistan_Rupee("586"),  Panama_Balboa("591"),  Papua_New_Guinea_Kina("598"),  Paraguan_Guarani(
/* 49 */     "600"),  Peruvian_Nuevo_Sol("604"),  Peso_Uruguayo("858"),  Philippine_Peso(
/* 50 */     "608"),  Polish_Zloty("985"),  Qatari_Rial("634"),  Romanian_Leu("946"),  Russian_Federation_Ruble(
/* 51 */     "643"),  Rwanda_Franc("646"),  Samoa_Tala("882"),  Sao_Tome_and_Principe_Dobra(
/* 52 */     "678"),  Saudi_Arabian_Riyal("682"),  Serbian_Dinar("941"),  Seychelles_Rupee(
/* 53 */     "690"),  Sierra_Leone_Leone("694"),  Singapore_Dollar("702"),  Solomon_Islands_Dollar(
/* 54 */     "090"),  Somali_Shilling("706"),  South_African_Rand("710"),  South_Korean_Won(
/* 55 */     "410"),  Sri_Lanka_Rupee("144"),  St_Helena_Pound("654"),  Surinam_Dollar(
/* 56 */     "968"),  Swaziland_Emalengeni("748"),  Swedish_Krona("752"),  Swiss_Franc(
/* 57 */     "756"),  Syrian_Pound("760"),  Tajik_Somoni("972"),  Tanzanian_Shilling(
/* 58 */     "834"),  Thailand_Baht("764"),  Tongan_Paanga("776"),  Trinidad_and_Tobago_Dollar(
/* 59 */     "780"),  Tunisian_Dinar("788"),  Turkish_Lira("949"),  Turkmenistan_New_Manat("934"),  UAE_Dirham(
/* 60 */     "784"),  US_Dollar("840"),  Uganda_Shilling("800"),  Ukraine_Hryvnia(
/* 61 */     "980"),  United_Kingdom_Pound_Sterling("826"),  Uzbekistan_Som("860"),  Vanuatu_Vatu(
/* 62 */     "548"),  Vietnamese_Dong("704"),  Yemeni_Rial("886"),  Zambia_Kwacha(
/* 63 */     "894"),  Zimbabwe_Dollar("716");
/*    */   
/*    */ 
/*    */ 
/*    */   private final String currencyCd;
/*    */   
/*    */ 
/*    */   private CurrencyCode(String curCd)
/*    */   {
/* 72 */     this.currencyCd = curCd;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public String getCurrencyCode()
/*    */   {
/* 79 */     return this.currencyCd;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\enumerations\CurrencyCode.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */