/*    */ package com.americanexpress.ips.gfsg.enumerations;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public enum ProcessingCode
/*    */ {
/* 23 */   CREDIT("200000"), 
/* 24 */   DEBIT("000000"), 
/* 25 */   CREDIT_REVERSALS("220808"), 
/* 26 */   DEBIT_REVERSALS("020000"), 
/*    */   
/* 28 */   BONUS_PAY_DEBIT("000090"), 
/* 29 */   BONUS_PAY_CREDIT("200090"), 
/* 30 */   BONUS_PAY_DEBIT_REVERSAL("020809"), 
/* 31 */   BONUS_PAY_CREDIT_REVERSAL("220809");
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   private final String processingCode;
/*    */   
/*    */ 
/*    */ 
/*    */   private ProcessingCode(String processingCode)
/*    */   {
/* 42 */     this.processingCode = processingCode;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public String getProcessingCode()
/*    */   {
/* 49 */     return this.processingCode;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\enumerations\ProcessingCode.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */