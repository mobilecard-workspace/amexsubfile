/*    */ package com.americanexpress.ips.gfsg.enumerations;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public enum ProhibitedCountryCodes
/*    */ {
/* 13 */   Cuba("192"),  Islamic_Republic_Of_Iran("364"),  Democratic_Peoples_Republic_Of_Korea("408"), 
/* 14 */   Myanmar("104"),  Sudan("736");
/*    */   
/*    */ 
/*    */ 
/*    */   private final String countryCd;
/*    */   
/*    */ 
/*    */ 
/*    */   private ProhibitedCountryCodes(String ctyCd)
/*    */   {
/* 24 */     this.countryCd = ctyCd;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public String getCountryCode()
/*    */   {
/* 31 */     return this.countryCd;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\enumerations\ProhibitedCountryCodes.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */