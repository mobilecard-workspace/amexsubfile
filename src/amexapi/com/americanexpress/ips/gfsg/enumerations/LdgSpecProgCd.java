/*    */ package com.americanexpress.ips.gfsg.enumerations;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public enum LdgSpecProgCd
/*    */ {
/* 12 */   Lodging("1"), 
/* 13 */   No_Show("2"), 
/* 14 */   Advanced_Deposit("3");
/*    */   
/*    */ 
/*    */   private final String ldgSpecProgCd;
/*    */   
/*    */ 
/*    */   private LdgSpecProgCd(String ldgSpecProgCd)
/*    */   {
/* 22 */     this.ldgSpecProgCd = ldgSpecProgCd;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public String getLdgSpecProgCd()
/*    */   {
/* 29 */     return this.ldgSpecProgCd;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\enumerations\LdgSpecProgCd.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */