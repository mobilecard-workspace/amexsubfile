/*    */ package com.americanexpress.ips.gfsg.enumerations;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public enum MediaCode
/*    */ {
/* 14 */   Cardmember_signature_on_File("01"), 
/* 15 */   Phone_order("02"),  Mail_Order("03"), 
/* 16 */   Internet_Order("04"),  Recurring_Billing("05");
/*    */   
/*    */ 
/*    */   private final String mediacode;
/*    */   
/*    */ 
/*    */   private MediaCode(String mediaCode)
/*    */   {
/* 24 */     this.mediacode = mediaCode;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public String getMediaCode()
/*    */   {
/* 32 */     return this.mediacode;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\enumerations\MediaCode.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */