/*    */ package com.americanexpress.ips.gfsg.enumerations;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public enum CallTypeCd
/*    */ {
/* 12 */   International_Subscriber_Dialing("ISD"), 
/* 13 */   International_TollFree("ITF"), 
/* 14 */   Local_Call("LOCAL"), 
/* 15 */   Trunk_Dialing_Subscriber("TDS");
/*    */   
/*    */ 
/*    */   private final String callTypeCd;
/*    */   
/*    */ 
/*    */   private CallTypeCd(String callTypeCd)
/*    */   {
/* 23 */     this.callTypeCd = callTypeCd;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public String getCallTypeCd()
/*    */   {
/* 30 */     return this.callTypeCd;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\enumerations\CallTypeCd.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */