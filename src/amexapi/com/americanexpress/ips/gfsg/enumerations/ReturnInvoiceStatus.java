/*    */ package com.americanexpress.ips.gfsg.enumerations;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public enum ReturnInvoiceStatus
/*    */ {
/* 13 */   No("00"), 
/* 14 */   Yes("01");
/*    */   
/*    */ 
/*    */   private final String returnInvoiceStatus;
/*    */   
/*    */ 
/*    */   private ReturnInvoiceStatus(String returnInvoiceStatus)
/*    */   {
/* 22 */     this.returnInvoiceStatus = returnInvoiceStatus;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public String getReturnInvoiceStatus()
/*    */   {
/* 29 */     return this.returnInvoiceStatus;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\enumerations\ReturnInvoiceStatus.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */