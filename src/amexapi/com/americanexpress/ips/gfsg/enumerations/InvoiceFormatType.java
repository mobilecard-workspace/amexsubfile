/*    */ package com.americanexpress.ips.gfsg.enumerations;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public enum InvoiceFormatType
/*    */ {
/* 13 */   EMEA_APACS("000"), 
/* 14 */   US_LID("001"), 
/* 15 */   LAC_Argentina("002"), 
/* 16 */   LAC_Brazil("003"), 
/* 17 */   LAC_Mexico("004");
/*    */   
/*    */ 
/*    */   private final String invoiceFormatType;
/*    */   
/*    */ 
/*    */   private InvoiceFormatType(String invoiceFormatType)
/*    */   {
/* 25 */     this.invoiceFormatType = invoiceFormatType;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public String getInvoiceFormatType()
/*    */   {
/* 32 */     return this.invoiceFormatType;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\enumerations\InvoiceFormatType.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */