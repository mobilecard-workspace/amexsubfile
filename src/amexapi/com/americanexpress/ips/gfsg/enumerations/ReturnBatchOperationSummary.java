/*    */ package com.americanexpress.ips.gfsg.enumerations;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public enum ReturnBatchOperationSummary
/*    */ {
/* 12 */   No_summary("00"), 
/* 13 */   Send_summary("01");
/*    */   
/*    */ 
/*    */   private final String returnBatchOperationSummary;
/*    */   
/*    */   private ReturnBatchOperationSummary(String returnBatchOperationSummary)
/*    */   {
/* 20 */     this.returnBatchOperationSummary = returnBatchOperationSummary;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public String getReturnBatchOperationSummary()
/*    */   {
/* 27 */     return this.returnBatchOperationSummary;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\enumerations\ReturnBatchOperationSummary.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */