/*    */ package com.americanexpress.ips.gfsg.enumerations;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public enum TravDisUnit
/*    */ {
/* 12 */   Miles("M"), 
/* 13 */   Kilometers("K");
/*    */   
/*    */ 
/*    */   private final String travDisUnit;
/*    */   
/*    */ 
/*    */   private TravDisUnit(String travDisUnit)
/*    */   {
/* 21 */     this.travDisUnit = travDisUnit;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public String getTravDisUnit()
/*    */   {
/* 28 */     return this.travDisUnit;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\enumerations\TravDisUnit.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */