/*    */ package com.americanexpress.ips.gfsg.enumerations;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public enum ElectronicCommerceIndicator
/*    */ {
/* 12 */   Authenticated("05"), 
/* 13 */   Attempted("06"), 
/* 14 */   Not_authenticated("07");
/*    */   
/*    */ 
/*    */   private final String electronicCommerceIndicator;
/*    */   
/*    */ 
/*    */   private ElectronicCommerceIndicator(String electronicCommerceIndicator)
/*    */   {
/* 22 */     this.electronicCommerceIndicator = electronicCommerceIndicator;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public String getElectronicCommerceIndicator()
/*    */   {
/* 29 */     return this.electronicCommerceIndicator;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\enumerations\ElectronicCommerceIndicator.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */