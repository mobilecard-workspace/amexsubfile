/*    */ package com.americanexpress.ips.gfsg.enumerations;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public enum TransProcCd
/*    */ {
/* 12 */   CREDIT("200000"), 
/* 13 */   DEBIT("000000"), 
/* 14 */   DEBIT_VOID("999999"), 
/* 15 */   CREDIT_VOID("299999");
/*    */   
/*    */ 
/*    */   private final String transProcCd;
/*    */   
/*    */ 
/*    */   private TransProcCd(String transProcCd)
/*    */   {
/* 23 */     this.transProcCd = transProcCd;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public String getTransProcCd()
/*    */   {
/* 30 */     return this.transProcCd;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\enumerations\TransProcCd.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */