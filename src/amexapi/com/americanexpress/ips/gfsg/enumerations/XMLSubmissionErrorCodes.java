/*     */ package com.americanexpress.ips.gfsg.enumerations;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public enum XMLSubmissionErrorCodes
/*     */ {
/*  13 */   ERROR_VERSION("100", "Invalid Version Number"), 
/*  14 */   ERROR_REFERENCENUMBER("105", "Invalid Reference Number"), 
/*  15 */   ERROR_BATCHOPERATION("115", "Invalid Batch Operation"), 
/*  16 */   ERROR_RETURNBATCHSUMMARY("116", "Invalid Return Batch Summary"), 
/*  17 */   ERROR_CURRENCYFORBATCHID("123", "Invalid Currency for Batch ID"), 
/*  18 */   ERROR_MAXAMOUNTFORBATCH("124", "Reached Max Amount for a Batch"), 
/*  19 */   ERROR_MAXTRANSACTIONSFORBATCH("125", "Reached Max Number of Transactions for a Batch"), 
/*  20 */   ERROR_SUBMITTERCODE("131", "Invalid Submitter Code"), 
/*     */   
/*  22 */   ERROR_DEFERREDPAYMENTPLAN("132", "Invalid Deferred Payment Plan"), 
/*  23 */   ERROR_NUMBEROFDEFERREDPAYMENTS("133", "Invalid Number of Deferred Payments"), 
/*  24 */   ERROR_EMVDATA("134", "Invalid EMV Data"), 
/*  25 */   ERROR_INVOICENUMBER("137", "Invalid Invoice Number"), 
/*  26 */   ERROR_RELEASENUMBER("140", "Invalid Release Number"), 
/*  27 */   ERROR_INVOICELIDFORMAT("148", "Invalid Invoice LID Format"), 
/*  28 */   ERROR_RETURNINVOICESTATUS("149", "Invalid Return Invoice Status"), 
/*     */   
/*     */ 
/*  31 */   ERROR_MERID("300", "Invalid Merchant ID"), 
/*  32 */   ERROR_MERTRMNLID("301", "Invalid Merchant Terminal ID"), 
/*  33 */   ERROR_BATCHID("302", "Invalid Batch ID"), 
/*  34 */   ERROR_CARDTRANSDETAIL("303", "Invalid Card Acceptor Details"), 
/*  35 */   ERROR_CARDACPTSTREETNM("305", "Invalid Card Acceptor Street Name"), 
/*  36 */   ERROR_CARDACPTNM("304", "Invalid Card Acceptor  Name"), 
/*  37 */   ERROR_CARDACPTCITYNM("306", "Invalid Card Acceptor City Name"), 
/*  38 */   ERROR_CARDACPTRGNCD("307", "Invalid Card Acceptor Region Code"), 
/*  39 */   ERROR_CARDACPTCTRYCD("308", "Invalid Card Acceptor Country Code"), 
/*  40 */   ERROR_CARDACPTPOSTCD("309", "Invalid Card Acceptor Postal Code"), 
/*  41 */   ERROR_CARDNBR("312", "Invalid Card Number"), 
/*  42 */   ERROR_CARDEXPRDT("313", "Invalid Card Expiration Date"), 
/*  43 */   ERROR_TRANSDT("314", "Invalid Transaction Date"), 
/*  44 */   ERROR_TRANSTM("315", "Invalid Transaction Time"), 
/*  45 */   ERROR_TRANSAMT("316", "Invalid Transaction Amount"), 
/*  46 */   ERROR_TRANSCURRCD("317", "Invalid Transaction Currency Code"), 
/*  47 */   ERROR_TRANSPROCCD("318", "Invalid Transaction Processing Code"), 
/*  48 */   ERROR_TRANSID("319", "Invalid Transaction ID"), 
/*  49 */   ERROR_TRANSAPRVCD("320", "Invalid Transaction Approval Code"), 
/*  50 */   ERROR_POINTOFSERVICEDATA("321", "Invalid Point of Service Data Code"), 
/*  51 */   ERROR_POINTOFSERVICEDATA_CARDDATAINPCPBLCD("321", "Invalid Point of Service Data Code subfield - CardDataInpCpblCd"), 
/*  52 */   ERROR_POINTOFSERVICEDATA_CMAUTHNCPBLCD("321", "Invalid Point of Service Data Code subfield - CMAuthnCpblCd"), 
/*  53 */   ERROR_POINTOFSERVICEDATA_CARDCPTRCPBLCD("321", "Invalid Point of Service Data Code subfield - CardCptrCpblCd"), 
/*  54 */   ERROR_POINTOFSERVICEDATA_OPRENVIRCD("321", "Invalid Point of Service Data Code subfield -OprEnvirCd"), 
/*  55 */   ERROR_POINTOFSERVICEDATA_CMPRESENTCD("321", "Invalid Point of Service Data Code subfield - CMPresentCd"), 
/*  56 */   ERROR_POINTOFSERVICEDATA_CARDPRESENTCD("321", "Invalid Point of Service Data Code subfield - CardPresentCd"), 
/*  57 */   ERROR_POINTOFSERVICEDATA_CARDDATAINPMODECD("321", "Invalid Point of Service Data Code subfield - CardDataInpModeCd"), 
/*  58 */   ERROR_POINTOFSERVICEDATA_CMAUTHNMTHDCD("321", "Invalid Point of Service Data Code subfield -CMAuthnMthdCd"), 
/*  59 */   ERROR_POINTOFSERVICEDATA_CMAUTHNENTTYCD("321", "Invalid Point of Service Data Code subfield - CMAuthnEnttyCd"), 
/*  60 */   ERROR_POINTOFSERVICEDATA_CARDDATAOPCPBLCD("321", "Invalid Point of Service Data Code subfield - CardDataOpCpblCd"), 
/*  61 */   ERROR_POINTOFSERVICEDATA_TRMNLOPCPBLCD("321", "Invalid Point of Service Data Code subfield -TrmnlOpCpblCd"), 
/*  62 */   ERROR_POINTOFSERVICEDATA_PINCPTRCPBLCD("321", "Invalid Point of Service Data Code subfield -PINCptrCpblCd"), 
/*  63 */   ERROR_ELECCOMRCEIND("322", "Invalid Electronic Commerce Indicator"), 
/*  64 */   ERROR_MEDIACD("323", "Invalid Media Code"), 
/*  65 */   ERROR_SUBMMTHDCD("324", "Invalid Submission Method Code"), 
/*  66 */   ERROR_MERLOCID("325", "Invalid Merchant Location ID"), 
/*  67 */   ERROR_MERCTCINFOTXT("326", "Invalid Merchant Contact Information"), 
/*  68 */   ERROR_MATCHKEYTYPECD("328", "Invalid Matching Key Type Code"), 
/*  69 */   ERROR_MATCHKEYID("329", "Invalid Matching Key ID"), 
/*  70 */   ERROR_MERCTGYCD("330", "Invalid Merchant Category Code"), 
/*  71 */   ERROR_SELLID("331", "Invalid Seller ID"), 
/*  72 */   ERROR_ADDITIONALAMOUNTTYPE("332", "Invalid Additional Amount Type Code"), 
/*  73 */   ERROR_ADDITIONALAMOUNT("333", "Invalid Additional Amount"), 
/*  74 */   ERROR_ADDITIONALAMOUNTSIGN("334", "Invalid Additional Amount Sign"), 
/*  75 */   ERROR_ADDENDUMCODE("335", "Invalid Addendum Code"), 
/*  76 */   ERROR_ADDENDUMNOTFOUND("336", "Addendum Not Found"), 
/*  77 */   ERROR_AIRTRAVTRANSTYPECD("337", "Invalid Airline Transaction Type Code"), 
/*  78 */   ERROR_AIRTKTNBR("338", "Invalid Ticket Number\t"), 
/*  79 */   ERROR_DOCTYPECD("339", "Invalid Document Type Code"), 
/*  80 */   ERROR_AIRLINEIATACARRIERCD("340", "Invalid IATA Carrier Code"), 
/*  81 */   ERROR_AIRIATAAGCYNBR("341", "Invalid IATA Agency Number"), 
/*  82 */   ERROR_TKTCARRIERNM("342", "Invalid Ticketing Carrier Name"), 
/*  83 */   ERROR_AIRLINETKTISSCITYNM("343", "Invalid Ticket Issue City Name"), 
/*  84 */   ERROR_TKTISSDT("344", "Invalid Ticket Issue Date"), 
/*  85 */   ERROR_PASSCNT("345", "Invalid Passenger Count"), 
/*  86 */   ERROR_AIRPASSNM("346", "Invalid Passenger Name"), 
/*  87 */   ERROR_CONJTKTIND("347", "Invalid Conjunction Ticket Indicator"), 
/*  88 */   ERROR_ELECTKTIND("348", "Invalid Electronic Ticket Indicator"), 
/*  89 */   ERROR_EXCHTKTNBR("349", "Invalid Exchange Ticket Number"), 
/*  90 */   ERROR_AIRLINESEGINFO("350", "Invalid Air Travel Segment Information"), 
/*  91 */   ERROR_STOPOVERCD("351", "Invalid Stopover Indicator Code"), 
/*  92 */   ERROR_DPRTLOCCD("352", "Invalid Departure Location Code"), 
/*  93 */   ERROR_AIRDPRTDT("353", "Invalid Departure Date"), 
/*  94 */   ERROR_ARRLOCCD("354", "Invalid Arrival Location Code"), 
/*  95 */   ERROR_AIRSEGIATACARRIERCD("355", "Invalid Segment IATA Carrier Code"), 
/*     */   
/*  97 */   ERROR_FAREBASISTXT("356", "Invalid Fare Basis"), 
/*  98 */   ERROR_AIRSERVCLASSCD("357", "Invalid Class of Service Code"), 
/*  99 */   ERROR_AIRFLGHTNBR("358", "Invalid Flight Number"), 
/* 100 */   ERROR_SEGTOTFAREAMT("359", "Invalid Total Segment Fare Amount"), 
/* 101 */   ERROR_ORIGTRANSAMT("360", "Invalid Original Transaction Amount"), 
/* 102 */   ERROR_ORIGCURRCD("361", "Invalid Original Currency Code"), 
/* 103 */   ERROR_AGRNBR("362", "Invalid Agreement Number"), 
/* 104 */   ERROR_PICKUPLOCNM("363", "Invalid Pickup Location Name"), 
/* 105 */   ERROR_PICKUPCITYNM("364", "Invalid Pickup City Name"), 
/* 106 */   ERROR_PICKUPRGNCD("365", "Invalid Pickup Region Code"), 
/* 107 */   ERROR_PICKUPCTRYCD("366", "Invalid Pickup Country Code"), 
/* 108 */   ERROR_PICKUPDT("367", "Invalid Pickup Date"), 
/* 109 */   ERROR_PICKUPTM("368", "Invalid Pickup Time"), 
/* 110 */   ERROR_RTRNCITYNM("369", "Invalid Return City Name"), 
/* 111 */   ERROR_RTRNRGNCD("370", "Invalid Return Region Code"), 
/* 112 */   ERROR_RTRNCTRYCD("371", "Invalid Return Country Code"), 
/* 113 */   ERROR_RTRNDT("372", "Invalid Return Date"), 
/*     */   
/* 115 */   ERROR_RTRNTM("373", "Invalid Return Time"), 
/* 116 */   ERROR_RENTERNM("374", "Invalid Renter Name"), 
/* 117 */   ERROR_VEHCLASSID("375", "Invalid Vehicle Class ID"), 
/* 118 */   ERROR_TRAVDISCNT("376", "Invalid Travel Distance"), 
/* 119 */   ERROR_TRAVDISUNIT("377", "Invalid Distance Units"), 
/* 120 */   ERROR_AUDITADJIND("378", "Invalid Audit Adjustment Indicator"), 
/* 121 */   ERROR_AUDITADJAMT("379", "Invalid Audit Adjustment Amount"), 
/* 122 */   ERROR_CALLDT("380", "Invalid Call Date"), 
/* 123 */   ERROR_CALLTM("391", "Invalid Call Time"), 
/* 124 */   ERROR_CallDurTm("382", "Invalid Call Duration Time"), 
/* 125 */   ERROR_CALLFROMLOCNM("383", "Invalid Call From Location"), 
/* 126 */   ERROR_CallFromRgnCd("384", "Invalid Call From Region Code"), 
/* 127 */   ERROR_CALLFROMCTRYCD("385", "Invalid Call From Country Code"), 
/* 128 */   ERROR_CALLFROMPHONENBR("386", "Invalid Call From Phone Number"), 
/* 129 */   ERROR_CALLTOLOCNM("387", "Invalid Call to Location"), 
/* 130 */   ERROR_CALLTORGNCD("388", "Invalid Call To Region Code"), 
/* 131 */   ERROR_CALLTOCTRYCD("389", "Invalid Call To Country Code"), 
/* 132 */   ERROR_CALLTOPHONENBR("390", "Invalid Call To Phone Number"), 
/* 133 */   ERROR_CALLINGCARDID("391", "Invalid Calling Card ID"), 
/* 134 */   ERROR_SERVTYPEDESCTXT("392", "Invalid Service Type Description"), 
/* 135 */   ERROR_BILLPERTXT("393", "Invalid Billing Period"), 
/* 136 */   ERROR_CALLTYPECD("394", "Invalid Call Type Code"), 
/* 137 */   ERROR_RATECLASSCD("395", "Invalid Rate Class Code"), 
/* 138 */   ERROR_EVENTNM("396", "Invalid Event Name"), 
/* 139 */   ERROR_EVENTDT("397", "Invalid Event Date"), 
/* 140 */   ERROR_EVENTTKTPRCAMT("398", "Invalid Ticket Price"), 
/* 141 */   ERROR_EVENTTKTCNT("399", "Invalid Number of Tickets"), 
/* 142 */   ERROR_EVENTLOCNM("400", "Invalid Event Location Name"), 
/* 143 */   ERROR_EVENTRGNCD("401", "Invalid Event Region Code"), 
/* 144 */   ERROR_EVENTCTRYCD("402", "Invalid Event Country Code"), 
/* 145 */   ERROR_INSRPLCYNBR("403", "Invalid Insurance Policy Number"), 
/* 146 */   ERROR_INSRCOVSTARTDT("404", "Invalid Insurance Coverage Start Date"), 
/* 147 */   ERROR_INSRCOVENDDT("405", "Invalid Insurance Coverage End Date"), 
/* 148 */   ERROR_INSRPLCYPREMFREQTXT("406", "Invalid Policy Premium Frequency"), 
/* 149 */   ERROR_ADDINSRPLCYNO("407", "Invalid Additional Policy Number"), 
/* 150 */   ERROR_PLCYTYPETXT("408", "Invalid Insurance Policy Type"), 
/* 151 */   ERROR_INSRNM("409", "Invalid Name Of Insured"), 
/* 152 */   ERROR_LDGSPECPROGCD("410", "Invalid Lodging Program Code"), 
/* 153 */   ERROR_LODGINGLDGCHECKINDT("411", "Invalid Check-in Date"), 
/* 154 */   ERROR_LODGINGLDGCHECKOUTDT("412", "Invalid Check-out Date"), 
/* 155 */   ERROR_LODGINGLDGRMRATE("413", "Invalid Lodging Room Rate"), 
/* 156 */   ERROR_LODGINGNBROFNGTCNT("414", "Invalid Number of Nights At Room Rate"), 
/* 157 */   ERROR_LDGRENTNM("415", "Invalid Lodging Renter Name"), 
/* 158 */   ERROR_LDGFOLIONBR("416", "Invalid Lodging Folio Number"), 
/* 159 */   ERROR_TRAVTRANSTYPECD("417", "Invalid Rail Transaction Type Code"), 
/* 160 */   ERROR_LODGINGTKTNBR("418", "Invalid Ticket Number"), 
/* 161 */   ERROR_LODGINGPASSNM("419", "Invalid Passenger Name"), 
/* 162 */   ERROR_RAILIATACARRIERCD("420", "Invalid IATA Carrier Code"), 
/* 163 */   ERROR_TKTISSNM("421", "Invalid Ticket Issue Name"), 
/* 164 */   ERROR_TKTISSCITYNM("422", "Invalid Ticket Issue City"), 
/* 165 */   ERROR_DPRTSTNCD("423", "Invalid Departure Station Code"), 
/* 166 */   ERROR_LODGINGDPRTDT("424", "Invalid Departure Date"), 
/* 167 */   ERROR_ARRSTNCD("425", "Invalid Arrival Station Code"), 
/* 168 */   ERROR_RETAILDEPTNM("426", "Invalid Department Name"), 
/* 169 */   ERROR_RETAILITEMINFO("427", "Invalid Retail Item Information"), 
/* 170 */   ERROR_RETAILITEMDESC("428", "Invalid Retail Item Description"), 
/* 171 */   ERROR_RETAILITEMQTY("429", "Invalid Retail Item Quantity"), 
/* 172 */   ERROR_RETAILITEMAMT("430", "Invalid Retail Item Amount"), 
/* 173 */   ERROR_RETAILIATACARRIERCD("431", "Invalid IATA Carrier Code"), 
/* 174 */   ERROR_IATAAGCYNBR("432", "Invalid IATA Agency Number"), 
/* 175 */   ERROR_TRAVPACKTYPECD("433", "Invalid Travel Package Type Code"), 
/* 176 */   ERROR_TKTNBR("434", "Invalid Ticket Number"), 
/* 177 */   ERROR_PASSNM("435", "Invalid Passenger Name"), 
/* 178 */   ERROR_LDGCHECKINDT("436", "Invalid Lodging Check-in Date"), 
/* 179 */   ERROR_LDGCHECKOUTDT("437", "Invalid Lodging Check-out Date"), 
/* 180 */   ERROR_LDGRMRATE("438", "Invalid Lodging Room Rate"), 
/* 181 */   ERROR_NBROFNGTCNT("439", "Invalid Number of Nights At Room Rate"), 
/* 182 */   ERROR_LDGNM("440", "Invalid Lodging Name"), 
/* 183 */   ERROR_LdgCityNm("441", "Invalid Lodging City"), 
/* 184 */   ERROR_LDGRGNCD("442", "Invalid Lodging Region Code"), 
/* 185 */   ERROR_LDGCTRYCD("443", "Invalid Lodging Country Code"), 
/* 186 */   ERROR_DPRTDT("444", "Invalid Departure Date"), 
/* 187 */   ERROR_DESTCD("445", "Invalid Travel Destination Code"), 
/* 188 */   ERROR_DPRTAIRCD("446", "Invalid Departure Airport Code"), 
/* 189 */   ERROR_SEGIATACARRIERCD("447", "Invalid Segment IATA Carrier Code"), 
/* 190 */   ERROR_FLGHTNBR("448", "Invalid Flight Number"), 
/* 191 */   ERROR_SERVCLASSCD("449", "Invalid Class of Service Code"), 
/*     */   
/*     */ 
/* 194 */   ERROR_RTRNLOCNM("459", "Invalid Return Location Name"), 
/* 195 */   ERROR_VEHICLEIDNBR("460", "Invalid Vehicle Identification Number"), 
/* 196 */   ERROR_DRIVERIDNBR("461", "Invalid Driver Identification Number"), 
/* 197 */   ERROR_DRIVERTAXID("462", "Invalid Driver Tax Identification"), 
/*     */   
/* 199 */   DCS_ERROR_REQ_NM("450", "Invalid Requester Name"), 
/* 200 */   DCS_ERROR_TAA_CORP_PURCH_SOL_LVL2("451", "Invalid Corporate Purchasing Addendum"), 
/* 201 */   DCS_ERROR_CHRG_DESC("452", "Invalid Charge Description"), 
/* 202 */   DCS_ERROR_CHRG_ITEM_QTY("453", "Invalid Charge Item Quantity"), 
/* 203 */   DCS_ERROR_CHRG_ITEM_AMT("454", "Invalid Charge Item Amount"), 
/* 204 */   DCS_ERROR_CM_REF_NBR("455", "Invalid Cardmember Reference Number"), 
/* 205 */   DCS_ERROR_SHIP_TO_POST_CD("456", "Invalid Ship To Postal Code"), 
/* 206 */   DCS_ERROR_TAX_TOT_AMT("457", "Invalid Total Tax Amount"), 
/* 207 */   DCS_ERROR_TAX_TYPE_CD("458", "Invalid Tax Type Code"), 
/* 208 */   DCS_ERROR_INVALID_INVOICE_NBR("137", "Invalid Invoice Number"), 
/* 209 */   DCS_ERROR_INVALID_RELEASE_NBR("140", "Invalid Release Number"), 
/* 210 */   DCS_ERROR_DESCRIPTION("113", "DCS_ERROR_DESCRIPTION"), 
/* 211 */   DCS_ERROR_TRANS_IMAGE_SEQ_NBR("327", "DCS_ERROR_TRANS_IMAGE_SEQ_NBR"), 
/* 212 */   DCS_ERROR_BATCH_IMAGE_SEQ_NBR("310", "DCS_ERROR_BATCH_IMAGE_SEQ_NBR"), 
/* 213 */   DCS_ERROR_SERV_AGT_MER_ID("311", "DCS_ERROR_SERV_AGT_MER_ID"), 
/* 214 */   DCS_ERROR_INVALID_LID_FORMAT_TYPE("148", "Invalid Invoice LID Format"), 
/* 215 */   DCS_ERROR_INVALID_RETURN_INV_STATUS("149", "Invalid Return Invoice Status");
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private final String errorCode;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private final String errorDescription;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getErrorCode()
/*     */   {
/* 255 */     return this.errorCode;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public String getErrorDescription()
/*     */   {
/* 262 */     return this.errorDescription;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   private XMLSubmissionErrorCodes(String errorCode, String errorDescription)
/*     */   {
/* 270 */     this.errorCode = errorCode;
/* 271 */     this.errorDescription = errorDescription;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\enumerations\XMLSubmissionErrorCodes.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */