/*    */ package com.americanexpress.ips.gfsg.enumerations.pos;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public enum CardDataOutputCapable
/*    */ {
/* 13 */   POC_UNKONOWN("0"),  POC_NONE("1"),  MAGNETIC_STRIPE_WRITE("2"),  INTEGRATED_CIRCUIT_CARD(
/* 14 */     "3"),  RESERVE_FOR_ISO_USE_4("4"),  RESERVE_FOR_ISO_USE_5("5"),  RESERVE_FOR_NATIONAL_USE_6(
/* 15 */     "6"),  RESERVE_FOR_NATIONAL_USE_7("7"),  RESERVE_FOR_PRIVATE_USE_8(
/* 16 */     "8"),  RESERVE_FOR_PRIVATE_USE_9("9"),  RESERVE_FOR_ISO_USE_A("A"),  RESERVE_FOR_ISO_USE_B(
/* 17 */     "B"),  RESERVE_FOR_ISO_USE_C("C"),  RESERVE_FOR_ISO_USE_D("D"),  RESERVE_FOR_ISO_USE_E(
/* 18 */     "E"),  RESERVE_FOR_ISO_USE_F("F"),  RESERVE_FOR_ISO_USE_G("G"),  RESERVE_FOR_ISO_USE_H(
/* 19 */     "H"),  RESERVE_FOR_ISO_USE_I("I"),  RESERVE_FOR_NATIONAL_USE_J("J"),  RESERVE_FOR_NATIONAL_USE_K(
/* 20 */     "K"),  RESERVE_FOR_NATIONAL_USE_L("L"),  RESERVE_FOR_NATIONAL_USE_M(
/* 21 */     "M"),  RESERVE_FOR_NATIONAL_USE_N("N"),  RESERVE_FOR_NATIONAL_USE_O(
/* 22 */     "O"),  RESERVE_FOR_NATIONAL_USE_P("P"),  RESERVE_FOR_NATIONAL_USE_Q(
/* 23 */     "Q"),  RESERVE_FOR_NATIONAL_USE_R("R"),  RESERVE_FOR_PRIVATE_USE_S(
/* 24 */     "S"),  RESERVE_FOR_PRIVATE_USE_T("T"),  RESERVE_FOR_PRIVATE_USE_U("U"),  RESERVE_FOR_PRIVATE_USE_V(
/* 25 */     "V"),  RESERVE_FOR_PRIVATE_USE_W("W"),  RESERVE_FOR_PRIVATE_USE_X("X"),  RESERVE_FOR_PRIVATE_USE_Y(
/* 26 */     "Y"),  RESERVE_FOR_PRIVATE_USE_Z("Z");
/*    */   
/*    */ 
/*    */   private final String cardDataOutputCapable;
/*    */   
/*    */ 
/*    */   private CardDataOutputCapable(String cardDataOutputCap)
/*    */   {
/* 34 */     this.cardDataOutputCapable = cardDataOutputCap;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public String getCardMemberAuthMethod()
/*    */   {
/* 41 */     return this.cardDataOutputCapable;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\enumerations\pos\CardDataOutputCapable.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */