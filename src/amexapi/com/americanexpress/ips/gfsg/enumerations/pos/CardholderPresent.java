/*    */ package com.americanexpress.ips.gfsg.enumerations.pos;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public enum CardholderPresent
/*    */ {
/* 14 */   POC_CARDMEMBER_PRESENT("0"),  POC_CARDMEMBER_NOT_PRESENT("1"),  POC_CARDMEMBER_NOT_PRESENT_MAIL_ORDER(
/* 15 */     "2"),  POC_CARDMEMBER_NOT_PRESENT_TELEPHONE("3"),  POC_CARDMEMBER_NOT_PRESENT_STANDING_AUTH(
/* 16 */     "4"),  RESERVE_FOR_ISO_USE_5("5"),  RESERVE_FOR_ISO_USE_6("6"),  RESERVE_FOR_NATIONAL_USE_7(
/* 17 */     "7"),  RESERVE_FOR_NATIONAL_USE_8("8"),  POC_CARDMEMBER_NOT_PRESENT_MAIL_RECURRENT(
/* 18 */     "9"),  RESERVE_FOR_ISO_USE_A("A"),  RESERVE_FOR_ISO_USE_B("B"),  RESERVE_FOR_ISO_USE_C(
/* 19 */     "C"),  RESERVE_FOR_ISO_USE_D("D"),  RESERVE_FOR_ISO_USE_E("E"),  RESERVE_FOR_ISO_USE_F(
/* 20 */     "F"),  RESERVE_FOR_ISO_USE_G("G"),  RESERVE_FOR_ISO_USE_H("H"),  RESERVE_FOR_ISO_USE_I(
/* 21 */     "I"),  RESERVE_FOR_NATIONAL_USE_J("J"),  RESERVE_FOR_NATIONAL_USE_K(
/* 22 */     "K"),  RESERVE_FOR_NATIONAL_USE_L("L"),  RESERVE_FOR_NATIONAL_USE_M(
/* 23 */     "M"),  RESERVE_FOR_NATIONAL_USE_N("N"),  RESERVE_FOR_NATIONAL_USE_O(
/* 24 */     "O"),  RESERVE_FOR_NATIONAL_USE_P("P"),  RESERVE_FOR_NATIONAL_USE_Q(
/* 25 */     "Q"),  RESERVE_FOR_NATIONAL_USE_R("R"),  POC_CARDMEMBER_NOT_PRESENT_MAIL_ORDER_INTERNET(
/* 26 */     "S"),  RESERVE_FOR_AMEX_NETWORK_USE_T("T"),  RESERVE_FOR_PRIVATE_USE_U(
/* 27 */     "U"),  RESERVE_FOR_PRIVATE_USE_V("V"),  RESERVE_FOR_PRIVATE_USE_W("W"),  RESERVE_FOR_PRIVATE_USE_X(
/* 28 */     "X"),  RESERVE_FOR_PRIVATE_USE_Y("Y"),  RESERVE_FOR_PRIVATE_USE_Z("Z");
/*    */   
/*    */ 
/*    */ 
/*    */   private final String CardholderPresent;
/*    */   
/*    */ 
/*    */   private CardholderPresent(String CrdHolderPresent)
/*    */   {
/* 37 */     this.CardholderPresent = CrdHolderPresent;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public String getCardholderPresent()
/*    */   {
/* 44 */     return this.CardholderPresent;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\enumerations\pos\CardholderPresent.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */