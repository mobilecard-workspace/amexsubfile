/*    */ package com.americanexpress.ips.gfsg.enumerations.pos;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public enum PINCaptureCapable
/*    */ {
/* 14 */   POC_NO_PIN_CAPTURE_CAPABILITY("0"),  POC_UNKNOWN("1"),  RESERVE_FOR_ISO_USE_2(
/* 15 */     "2"),  RESERVE_FOR_ISO_USE_3("3"),  POC_FOUR_CHARS("4"),  POC_FIVE_CHARS(
/* 16 */     "5"),  POC_SIX_CHARS("6"),  POC_SEVEN_CHARS("7"),  POC_EIGHT_CHARS("8"),  POC_NINE_CHARS(
/* 17 */     "9"),  POC_TEN_CHARS("A"),  POC_ELEVEN_CHARS("B"),  POC_TWELVE_CHARS(
/* 18 */     "C"),  RESERVE_FOR_ISO_USE_D("D"),  RESERVE_FOR_ISO_USE_E("E"),  RESERVE_FOR_ISO_USE_F(
/* 19 */     "F"),  RESERVE_FOR_ISO_USE_G("G"),  RESERVE_FOR_ISO_USE_H("H"),  RESERVE_FOR_ISO_USE_I(
/* 20 */     "I"),  RESERVE_FOR_NATIONAL_USE_J("J"),  RESERVE_FOR_NATIONAL_USE_K(
/* 21 */     "K"),  RESERVE_FOR_NATIONAL_USE_L("L"),  RESERVE_FOR_NATIONAL_USE_M(
/* 22 */     "M"),  RESERVE_FOR_NATIONAL_USE_N("N"),  RESERVE_FOR_NATIONAL_USE_O(
/* 23 */     "O"),  RESERVE_FOR_NATIONAL_USE_P("P"),  RESERVE_FOR_NATIONAL_USE_Q(
/* 24 */     "Q"),  RESERVE_FOR_NATIONAL_USE_R("R"),  RESERVE_FOR_PRIVATE_USE_S(
/* 25 */     "S"),  RESERVE_FOR_PRIVATE_USE_T("T"),  RESERVE_FOR_PRIVATE_USE_U("U"),  RESERVE_FOR_PRIVATE_USE_V(
/* 26 */     "V"),  RESERVE_FOR_PRIVATE_USE_W("W"),  RESERVE_FOR_PRIVATE_USE_X("X"),  RESERVE_FOR_PRIVATE_USE_Y(
/* 27 */     "Y"),  RESERVE_FOR_PRIVATE_USE_Z("Z");
/*    */   
/*    */ 
/*    */ 
/*    */   private final String PINCaptureCapable;
/*    */   
/*    */ 
/*    */   private PINCaptureCapable(String PINCaptureCap)
/*    */   {
/* 36 */     this.PINCaptureCapable = PINCaptureCap;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public String getPINCaptureCapable()
/*    */   {
/* 43 */     return this.PINCaptureCapable;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\enumerations\pos\PINCaptureCapable.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */