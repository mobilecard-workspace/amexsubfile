/*    */ package com.americanexpress.ips.gfsg.enumerations.pos;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public enum OperatingEnvironment
/*    */ {
/* 14 */   POC_UNKNOWN("0"),  POC_ON_PREMISES_OF_CARD_ACCEPTOR_ATTENDED("1"),  POC_ON_PREMISES_OF_CARD_ACCEPTOR_UNATTENDED(
/* 15 */     "2"),  POC_OFF_PREMISES_OF_CARD_ACCEPTOR_ATTENDED("3"),  POC_OFF_PREMISES_OF_CARD_ACCEPTOR_UNATTENDED(
/* 16 */     "4"),  POC_ON_PREMISES_OF_CARD_MEMBER_UNATTENDED("5"),  RESERVE_FOR_ISO_USE_6(
/* 17 */     "6"),  RESERVE_FOR_ISO_USE_7("7"),  RESERVE_FOR_NATIONAL_USE_8("8"),  POC_UNKNOWN_DELIVERY_MODE(
/* 18 */     "9"),  RESERVE_FOR_ISO_USE_A("A"),  RESERVE_FOR_ISO_USE_B("B"),  RESERVE_FOR_ISO_USE_C(
/* 19 */     "C"),  RESERVE_FOR_ISO_USE_D("D"),  RESERVE_FOR_ISO_USE_E("E"),  RESERVE_FOR_ISO_USE_F(
/* 20 */     "F"),  RESERVE_FOR_ISO_USE_G("G"),  RESERVE_FOR_ISO_USE_H("H"),  RESERVE_FOR_ISO_USE_I(
/* 21 */     "I"),  RESERVE_FOR_NATIONAL_USE_J("J"),  RESERVE_FOR_NATIONAL_USE_K(
/* 22 */     "K"),  RESERVE_FOR_NATIONAL_USE_L("L"),  RESERVE_FOR_NATIONAL_USE_M(
/* 23 */     "M"),  RESERVE_FOR_NATIONAL_USE_N("N"),  RESERVE_FOR_NATIONAL_USE_O(
/* 24 */     "O"),  RESERVE_FOR_NATIONAL_USE_P("P"),  RESERVE_FOR_NATIONAL_USE_Q(
/* 25 */     "Q"),  RESERVE_FOR_NATIONAL_USE_R("R"),  POC_ELECTRONIC_DELIVERY_OF_PRODUCT(
/* 26 */     "S"),  POC_PHYSICAL_DELIVERY_OF_PRODUCT("T"),  RESERVE_FOR_AMEX_NETWORK_USE_U(
/* 27 */     "U"),  RESERVE_FOR_AMEX_NETWORK_USE_V("V"),  RESERVE_FOR_AMEX_NETWORK_USE_W(
/* 28 */     "W"),  RESERVE_FOR_PRIVATE_USE_X("X"),  RESERVE_FOR_PRIVATE_USE_Y("Y"),  RESERVE_FOR_PRIVATE_USE_Z(
/* 29 */     "Z");
/*    */   
/*    */ 
/*    */ 
/*    */   private final String OperatingEnvironment;
/*    */   
/*    */ 
/*    */   private OperatingEnvironment(String Opnv)
/*    */   {
/* 38 */     this.OperatingEnvironment = Opnv;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public String getOperatingEnvironment()
/*    */   {
/* 45 */     return this.OperatingEnvironment;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\enumerations\pos\OperatingEnvironment.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */