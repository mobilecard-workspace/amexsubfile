/*    */ package com.americanexpress.ips.gfsg.enumerations.pos;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public enum CardInputCapability
/*    */ {
/* 12 */   POC_UNKNOWN("0"),  POC_MANUAL("1"),  POC_MAGNETIC_STRIPE_READ("2"),  POC_MAGNETIC_BARCODE(
/* 13 */     "3"),  POC_OCR("4"),  POC_INTEGERATED_CIRCUIT_CARD("5"),  POC_KEY_ENTERED(
/* 14 */     "6"),  RESERVE_FOR_ISO_USE_7("7"),  RESERVE_FOR_NATIONAL_USE_8("8"),  RESERVE_FOR_NATIONAL_USE_9(
/* 15 */     "9"),  RESERVE_FOR_ISO_USE_A("A"),  RESERVE_FOR_ISO_USE_B("B"),  RESERVE_FOR_ISO_USE_C(
/* 16 */     "C"),  RESERVE_FOR_ISO_USE_D("D"),  RESERVE_FOR_ISO_USE_E("E"),  RESERVE_FOR_ISO_USE_F(
/* 17 */     "F"),  RESERVE_FOR_ISO_USE_G("G"),  RESERVE_FOR_ISO_USE_H("H"),  RESERVE_FOR_ISO_USE_I(
/* 18 */     "I"),  RESERVE_FOR_NATIONAL_USE_J("J"),  RESERVE_FOR_NATIONAL_USE_K(
/* 19 */     "K"),  RESERVE_FOR_NATIONAL_USE_L("L"),  RESERVE_FOR_NATIONAL_USE_M(
/* 20 */     "M"),  RESERVE_FOR_NATIONAL_USE_N("N"),  RESERVE_FOR_NATIONAL_USE_O(
/* 21 */     "O"),  RESERVE_FOR_NATIONAL_USE_P("P"),  RESERVE_FOR_NATIONAL_USE_Q(
/* 22 */     "Q"),  RESERVE_FOR_NATIONAL_USE_R("R"),  RESERVE_FOR_PRIVATE_USE_S(
/* 23 */     "S"),  RESERVE_FOR_PRIVATE_USE_T("T"),  RESERVE_FOR_PRIVATE_USE_U("U"),  RESERVE_FOR_PRIVATE_USE_V(
/* 24 */     "V"),  RESERVE_FOR_PRIVATE_USE_W("W"),  POC_MAGNETIC_STRIPE_SIGNATURE(
/* 25 */     "X"),  RESERVE_FOR_PRIVATE_USE_Y("Y"),  RESERVE_FOR_PRIVATE_USE_Z("Z");
/*    */   
/*    */ 
/*    */ 
/*    */   private final String CardInputCapability;
/*    */   
/*    */ 
/*    */   private CardInputCapability(String cardInputcap)
/*    */   {
/* 34 */     this.CardInputCapability = cardInputcap;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public String getCardInputCapability()
/*    */   {
/* 41 */     return this.CardInputCapability;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\enumerations\pos\CardInputCapability.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */