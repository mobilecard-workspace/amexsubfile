/*    */ package com.americanexpress.ips.gfsg.enumerations.pos;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public enum CardMemberAuthMethod
/*    */ {
/* 13 */   POC_UNKONOWN("0"),  POC_PINL("1"),  POC_ELECTRONIC_SGNATURE_ANALYSIS("2"),  POC_BIOMETRICS(
/* 14 */     "3"),  POC_BIOGRAPHIC("4"),  POC_MANUAL_SIGN_VERIFICATION("5"),  POC_OTHER_MANUAL_VERIFICATION(
/* 15 */     "6"),  RESERVE_FOR_ISO_USE_7("7"),  ESERVE_FOR_NATIONAL_USE_8("8"),  RESERVE_FOR_PRIVATE_USE_9(
/* 16 */     "9"),  RESERVE_FOR_ISO_USE_A("A"),  RESERVE_FOR_ISO_USE_B("B"),  RESERVE_FOR_ISO_USE_C(
/* 17 */     "C"),  RESERVE_FOR_ISO_USE_D("D"),  RESERVE_FOR_ISO_USE_E("E"),  RESERVE_FOR_ISO_USE_F(
/* 18 */     "F"),  RESERVE_FOR_ISO_USE_G("G"),  RESERVE_FOR_ISO_USE_H("H"),  RESERVE_FOR_ISO_USE_I(
/* 19 */     "I"),  RESERVE_FOR_NATIONAL_USE_J("J"),  RESERVE_FOR_NATIONAL_USE_K(
/* 20 */     "K"),  RESERVE_FOR_NATIONAL_USE_L("L"),  RESERVE_FOR_NATIONAL_USE_M(
/* 21 */     "M"),  RESERVE_FOR_NATIONAL_USE_N("N"),  RESERVE_FOR_NATIONAL_USE_O(
/* 22 */     "O"),  RESERVE_FOR_NATIONAL_USE_P("P"),  RESERVE_FOR_NATIONAL_USE_Q(
/* 23 */     "Q"),  RESERVE_FOR_NATIONAL_USE_R("R"),  POC_ELECTRONIC_TICKET_ENVIRONMENT(
/* 24 */     "S"),  RESERVE_FOR_PRIVATE_USE_T("T"),  RESERVE_FOR_PRIVATE_USE_U("U"),  RESERVE_FOR_PRIVATE_USE_V(
/* 25 */     "V"),  RESERVE_FOR_PRIVATE_USE_W("W"),  RESERVE_FOR_PRIVATE_USE_X("X"),  RESERVE_FOR_PRIVATE_USE_Y(
/* 26 */     "Y"),  RESERVE_FOR_PRIVATE_USE_Z("Z");
/*    */   
/*    */ 
/*    */ 
/*    */   private final String cardMemberAuthMethod;
/*    */   
/*    */ 
/*    */   private CardMemberAuthMethod(String crdMemberAuthMeth)
/*    */   {
/* 35 */     this.cardMemberAuthMethod = crdMemberAuthMeth;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public String getCardMemberAuthMethod()
/*    */   {
/* 42 */     return this.cardMemberAuthMethod;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\enumerations\pos\CardMemberAuthMethod.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */