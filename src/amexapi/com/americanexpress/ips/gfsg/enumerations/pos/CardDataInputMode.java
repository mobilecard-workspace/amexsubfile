/*    */ package com.americanexpress.ips.gfsg.enumerations.pos;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public enum CardDataInputMode
/*    */ {
/* 13 */   POC_UNKONOWN("0"),  POC_MANUAL("1"),  POC_MAGNETIC_STRIPE_READ("2"),  POC_BAR_CODE(
/* 14 */     "3"),  POC_OCR("4"),  POC_INTEGRATED_CKT_CARD("5"),  POC_KEY_ENTERED(
/* 15 */     "6"),  RESERVE_FOR_ISO_USE_7("7"),  RESERVE_FOR_NATIONAL_USE_8("8"),  POC_TECHNICAL_FALLBACK(
/* 16 */     "9"),  RESERVE_FOR_ISO_USE_A("A"),  RESERVE_FOR_ISO_USE_B("B"),  RESERVE_FOR_ISO_USE_C(
/* 17 */     "C"),  RESERVE_FOR_ISO_USE_D("D"),  RESERVE_FOR_ISO_USE_E("E"),  RESERVE_FOR_ISO_USE_F(
/* 18 */     "F"),  RESERVE_FOR_ISO_USE_G("G"),  RESERVE_FOR_ISO_USE_H("H"),  RESERVE_FOR_ISO_USE_I(
/* 19 */     "I"),  RESERVE_FOR_NATIONAL_USE_J("J"),  RESERVE_FOR_NATIONAL_USE_K(
/* 20 */     "K"),  RESERVE_FOR_NATIONAL_USE_L("L"),  RESERVE_FOR_NATIONAL_USE_M(
/* 21 */     "M"),  RESERVE_FOR_NATIONAL_USE_N("N"),  RESERVE_FOR_NATIONAL_USE_O(
/* 22 */     "O"),  RESERVE_FOR_NATIONAL_USE_P("P"),  RESERVE_FOR_NATIONAL_USE_Q(
/* 23 */     "Q"),  RESERVE_FOR_NATIONAL_USE_R("R"),  POC_MANUALLY_ENTERED("S"),  RESERVE_FOR_PRIVATE_USE_T(
/* 24 */     "T"),  RESERVE_FOR_PRIVATE_USE_U("U"),  RESERVE_FOR_PRIVATE_USE_V("V"),  POC_SWIPED(
/* 25 */     "W"),  POC_MAGNETIC_STRIPE_SIGNATURE("X"),  POC_MAGNETIC_STRIPE_SIGNATURE_KEY(
/* 26 */     "Y"),  RESERVE_FOR_PRIVATE_USE_Z("Z");
/*    */   
/*    */ 
/*    */ 
/*    */   private final String cardDataInputMode;
/*    */   
/*    */ 
/*    */   private CardDataInputMode(String cardMode)
/*    */   {
/* 35 */     this.cardDataInputMode = cardMode;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public String getCardDataInputMode()
/*    */   {
/* 42 */     return this.cardDataInputMode;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\enumerations\pos\CardDataInputMode.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */