/*    */ package com.americanexpress.ips.gfsg.enumerations.pos;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public enum TerminalOutputCapable
/*    */ {
/* 14 */   POC_UNKONOWN("0"),  POC_NONE("1"),  POC_PRINTING("2"),  POC_DISPLAY("3"),  POC_PRINTING_AND_DISPLAY(
/* 15 */     "4"),  RESERVE_FOR_ISO_USE_5("5"),  RESERVE_FOR_ISO_USE_6("6"),  RESERVE_FOR_NATIONAL_USE_7(
/* 16 */     "7"),  RESERVE_FOR_NATIONAL_USE_8("8"),  RESERVE_FOR_PRIVATE_USE_9(
/* 17 */     "9"),  RESERVE_FOR_ISO_USE_A("A"),  RESERVE_FOR_ISO_USE_B("B"),  RESERVE_FOR_ISO_USE_C(
/* 18 */     "C"),  RESERVE_FOR_ISO_USE_D("D"),  RESERVE_FOR_ISO_USE_E("E"),  RESERVE_FOR_ISO_USE_F(
/* 19 */     "F"),  RESERVE_FOR_ISO_USE_G("G"),  RESERVE_FOR_ISO_USE_H("H"),  RESERVE_FOR_ISO_USE_I(
/* 20 */     "I"),  RESERVE_FOR_NATIONAL_USE_J("J"),  RESERVE_FOR_NATIONAL_USE_K(
/* 21 */     "K"),  RESERVE_FOR_NATIONAL_USE_L("L"),  RESERVE_FOR_NATIONAL_USE_M(
/* 22 */     "M"),  RESERVE_FOR_NATIONAL_USE_N("N"),  RESERVE_FOR_NATIONAL_USE_O(
/* 23 */     "O"),  RESERVE_FOR_NATIONAL_USE_P("P"),  RESERVE_FOR_NATIONAL_USE_Q(
/* 24 */     "Q"),  RESERVE_FOR_NATIONAL_USE_R("R"),  RESERVE_FOR_PRIVATE_USE_S(
/* 25 */     "S"),  RESERVE_FOR_PRIVATE_USE_T("T"),  RESERVE_FOR_PRIVATE_USE_U("U"),  RESERVE_FOR_PRIVATE_USE_V(
/* 26 */     "V"),  RESERVE_FOR_PRIVATE_USE_W("W"),  RESERVE_FOR_PRIVATE_USE_X("X"),  RESERVE_FOR_PRIVATE_USE_Y(
/* 27 */     "Y"),  RESERVE_FOR_PRIVATE_USE_Z("Z");
/*    */   
/*    */ 
/*    */ 
/*    */   private final String terminalOutputCapable;
/*    */   
/*    */ 
/*    */   private TerminalOutputCapable(String terminalOutputCap)
/*    */   {
/* 36 */     this.terminalOutputCapable = terminalOutputCap;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public String getTerminalOutputCapable()
/*    */   {
/* 43 */     return this.terminalOutputCapable;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\enumerations\pos\TerminalOutputCapable.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */