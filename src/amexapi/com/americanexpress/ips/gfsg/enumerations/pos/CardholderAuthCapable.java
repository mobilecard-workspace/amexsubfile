/*    */ package com.americanexpress.ips.gfsg.enumerations.pos;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public enum CardholderAuthCapable
/*    */ {
/* 14 */   POC_UNKNOWN("0"),  POC_PIN("1"),  POC_ELECTRONIC_SIGNATURE_ANALYSIS("2"),  POC_BIOMETRICS(
/* 15 */     "3"),  POC_BIOGRAPHIC("4"),  POC_ELECTRONIC_AUTHENTICATION_INOPERATIVE(
/* 16 */     "5"),  POC_OTHER("6"),  RESERVE_FOR_ISO_USE_7("7"),  RESERVE_FOR_NATIONAL_USE_8(
/* 17 */     "8"),  RESERVE_FOR_PRIVATE_USE_9("9"),  RESERVE_FOR_ISO_USE_A("A"),  RESERVE_FOR_ISO_USE_B(
/* 18 */     "B"),  RESERVE_FOR_ISO_USE_C("C"),  RESERVE_FOR_ISO_USE_D("D"),  RESERVE_FOR_ISO_USE_E(
/* 19 */     "E"),  RESERVE_FOR_ISO_USE_F("F"),  RESERVE_FOR_ISO_USE_G("G"),  RESERVE_FOR_ISO_USE_H(
/* 20 */     "H"),  RESERVE_FOR_ISO_USE_I("I"),  RESERVE_FOR_NATIONAL_USE_J("J"),  RESERVE_FOR_NATIONAL_USE_K(
/* 21 */     "K"),  RESERVE_FOR_NATIONAL_USE_L("L"),  RESERVE_FOR_NATIONAL_USE_M(
/* 22 */     "M"),  RESERVE_FOR_NATIONAL_USE_N("N"),  RESERVE_FOR_NATIONAL_USE_O(
/* 23 */     "O"),  RESERVE_FOR_NATIONAL_USE_P("P"),  RESERVE_FOR_NATIONAL_USE_Q(
/* 24 */     "Q"),  RESERVE_FOR_NATIONAL_USE_R("R"),  RESERVE_FOR_PRIVATE_USE_S(
/* 25 */     "S"),  RESERVE_FOR_PRIVATE_USE_T("T"),  RESERVE_FOR_PRIVATE_USE_U("U"),  RESERVE_FOR_PRIVATE_USE_V(
/* 26 */     "V"),  RESERVE_FOR_PRIVATE_USE_W("W"),  RESERVE_FOR_PRIVATE_USE_X("X"),  RESERVE_FOR_PRIVATE_USE_Y(
/* 27 */     "Y"),  RESERVE_FOR_PRIVATE_USE_Z("Z");
/*    */   
/*    */ 
/*    */ 
/*    */   private final String CardHolderAuthCapable;
/*    */   
/*    */ 
/*    */   private CardholderAuthCapable(String CardHolderAuthCap)
/*    */   {
/* 36 */     this.CardHolderAuthCapable = CardHolderAuthCap;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public String getCardHolderAuthCapability()
/*    */   {
/* 43 */     return this.CardHolderAuthCapable;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\enumerations\pos\CardholderAuthCapable.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */