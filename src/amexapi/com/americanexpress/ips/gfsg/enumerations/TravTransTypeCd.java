/*    */ package com.americanexpress.ips.gfsg.enumerations;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public enum TravTransTypeCd
/*    */ {
/* 12 */   Ticket("TKT"), 
/* 13 */   Exchange_Ticket("EXC"), 
/* 14 */   Refund("REF"), 
/* 15 */   Miscellaneous("MSC");
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   private final String travTransTypeCd;
/*    */   
/*    */ 
/*    */ 
/*    */   private TravTransTypeCd(String travTransTypeCd)
/*    */   {
/* 26 */     this.travTransTypeCd = travTransTypeCd;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public String getTravTransTypeCd()
/*    */   {
/* 34 */     return this.travTransTypeCd;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\enumerations\TravTransTypeCd.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */