/*    */ package com.americanexpress.ips.gfsg.enumerations;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public enum BatchOperation
/*    */ {
/* 15 */   Open("01"),  Close("02"),  Purge("03"),  Status("04");
/*    */   
/*    */ 
/*    */   private final String batchOperation;
/*    */   
/*    */ 
/*    */   private BatchOperation(String batchOperation)
/*    */   {
/* 23 */     this.batchOperation = batchOperation;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public String getBatchOperation()
/*    */   {
/* 31 */     return this.batchOperation;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\enumerations\BatchOperation.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */