/*    */ package com.americanexpress.ips.gfsg.enumerations;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public enum AddendaTypeCode
/*    */ {
/* 12 */   Deferred_Payment_Plan_Addendum_Message("02"), 
/* 13 */   Industry_Specific_Detail_Addendum_Message("03"), 
/* 14 */   Corporate_Purchasing_Solutions("05"), 
/* 15 */   EMV_Addendum_Message("07"), 
/* 16 */   Location_Detail_Addendum_Message("99");
/*    */   
/*    */ 
/*    */   private final String addendaTypeCode;
/*    */   
/*    */ 
/*    */   private AddendaTypeCode(String AddendaType)
/*    */   {
/* 24 */     this.addendaTypeCode = AddendaType;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public String getAddendaTypeCode()
/*    */   {
/* 32 */     return this.addendaTypeCode;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\enumerations\AddendaTypeCode.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */