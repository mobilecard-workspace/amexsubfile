/*     */ package com.americanexpress.ips.gfsg.enumerations.xml;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public enum XmlCountryCode
/*     */ {
/*  12 */   Afghanistan("004", "AFG", "F"), 
/*  13 */   Aland_Islands("248", "ALA", "S"), 
/*  14 */   Albania("008", "ALB", "S"), 
/*  15 */   Algeria("012", "DZA", "S"), 
/*  16 */   American_Samoa("016", "ASM", "S"), 
/*  17 */   Andorra("020", "AND", "S"), 
/*  18 */   Angola("024", "AGO", "F"), 
/*  19 */   Anguilla("660", "AIA", "S"), 
/*  20 */   Antarctica("010", "ATA", "F"), 
/*  21 */   Antigua_and_Barbuda("028", "ATG", "S"), 
/*  22 */   Argentina("032", "ARG", "F"), 
/*  23 */   Armenia("051", "ARM", "F"), 
/*  24 */   Aruba("533", "ABW", "S"), 
/*  25 */   Australia("036", "AUS", "S"), 
/*  26 */   Austria("040", "AUT", "S"), 
/*  27 */   Azerbaijan("031", "AZE", "F"), 
/*  28 */   Bahamas("044", "BHS", "S"), 
/*  29 */   Bahrain("048", "BHR", "S"), 
/*  30 */   Bangladesh("050", "BGD", "F"), 
/*  31 */   Barbados("052", "BRB", "S"), 
/*  32 */   Belarus("112", "BLR", "F"), 
/*  33 */   Belgium("056", "BEL", "S"), 
/*  34 */   Belize("084", "BLZ", "F"), 
/*  35 */   Benin("204", "BEN", "F"), 
/*  36 */   Bermuda("060", "BMU", "S"), 
/*  37 */   Bhutan("064", "BTN", "F"), 
/*  38 */   Bolivia("068", "BOL", "S"), 
/*  39 */   Bosnia_and_Herzegovina("070", "BIH", "S"), 
/*  40 */   Botswana("072", "BWA", "S"), 
/*  41 */   Bouvet_Island("074", "BVT", "F"), 
/*  42 */   Brazil("076", "BRA", "F"), 
/*  43 */   British_Indian_Ocean_Territory("086", "IOT", "F"), 
/*  44 */   Brunei_Darussalam("096", "BRN", "S"), 
/*  45 */   Bulgaria("100", "BGR", "S"), 
/*  46 */   Burkina_Faso("854", "BFA", "F"), 
/*  47 */   Burundi("108", "BDI", "F"), 
/*  48 */   Cambodia("116", "KHM", "F"), 
/*  49 */   Cameroon("120", "CMR", "F"), 
/*  50 */   Canada("124", "CAN", "S"), 
/*  51 */   Cape_Verde("132", "CPV", "F"), 
/*  52 */   Cayman_Islands("136", "CYM", "S"), 
/*  53 */   Central_African_Republic("140", "CAF", "S"), 
/*  54 */   Chad("148", "TCD", "F"), 
/*  55 */   Chile("152", "CHL", "F"), 
/*  56 */   China("156", "CHN", "S"), 
/*  57 */   Christmas_Island("162", "CXR", "F"), 
/*  58 */   Cocos_Keeling_Islands("166", "CCK", "F"), 
/*  59 */   Colombia("170", "COL", "F"), 
/*  60 */   Comoros("174", "COM", "F"), 
/*  61 */   Congo("178", "COG", "F"), 
/*  62 */   Congo_The_Democratic_Republic_of("180", "COD", "F"), 
/*  63 */   Cook_Islands("184", "COK", "F"), 
/*  64 */   Costa_Rica("188", "CRI", "S"), 
/*  65 */   Cote_DIvoire_Ivory_Coast("384", "CIV", "F"), 
/*  66 */   Croatia_Republic_of("191", "HRV", "S"), 
/*  67 */   Cuba("192", "CUB", "X"), 
/*  68 */   Cyprus("196", "CYP", "S"), 
/*  69 */   Czech_Republic("203", "CZE", "S"), 
/*  70 */   Denmark("208", "DNK", "S"), 
/*  71 */   Djibouti("262", "DJI", "F"), 
/*  72 */   Dominica("212", "DMA", "S"), 
/*  73 */   Dominican_Republic("214", "DOM", "F"), 
/*  74 */   Ecuador("218", "ECU", "F"), 
/*  75 */   Egypt("818", "EGY", "S"), 
/*  76 */   El_Salvador("222", "SLV", "S"), 
/*  77 */   Equatorial_Guinea("226", "GNQ", "F"), 
/*  78 */   Eritrea("232", "ERI", "F"), 
/*  79 */   Estonia("233", "EST", "S"), 
/*  80 */   Ethiopia("231", "ETH", "F"), 
/*  81 */   Falkland_Islands_Malvinas("238", "FLK", "F"), 
/*  82 */   Faroe_Islands("234", "FRO", "F"), 
/*  83 */   Fiji("242", "FJI", "S"), 
/*  84 */   Finland("246", "FIN", "S"), 
/*  85 */   France("250", "FRA", "S"), 
/*  86 */   French_Guiana("254", "GUF", "F"), 
/*  87 */   French_Polynesia("258", "PYF", "F"), 
/*  88 */   French_Southern_Territories("260", "ATF", "F"), 
/*  89 */   Gabon("266", "GAB", "F"), 
/*  90 */   Gambia("270", "GMB", "S"), 
/*  91 */   Georgia("268", "GEO", "F"), 
/*  92 */   Germany("276", "DEU", "S"), 
/*  93 */   Ghana("288", "GHA", "F"), 
/*  94 */   Gibraltar("292", "GIB", "F"), 
/*  95 */   Greece("300", "GRC", "S"), 
/*  96 */   Greenland("304", "GRL", "F"), 
/*  97 */   Grenada("308", "GRD", "S"), 
/*  98 */   Guadeloupe("312", "GLP", "S"), 
/*  99 */   Guam("316", "GUM", "S"), 
/* 100 */   Guatemala("320", "GTM", "F"), 
/* 101 */   Guernsey("831", "GGY", "S"), 
/* 102 */   Guinea("324", "GIN", "F"), 
/* 103 */   Guinea_Bissau("624", "GNB", "F"), 
/* 104 */   Guyana("328", "GUY", "S"), 
/* 105 */   Haiti("332", "HTI", "S"), 
/* 106 */   Heard_and_McDonald_Islands("334", "HMD", "F"), 
/* 107 */   Honduras("340", "HND", "F"), 
/* 108 */   Hong_Kong("344", "HKG", "S"), 
/* 109 */   Hungary("348", "HUN", "S"), 
/* 110 */   Iceland("352", "ISL", "S"), 
/* 111 */   India("356", "IND", "F"), 
/* 112 */   Indonesia("360", "IDN", "F"), 
/* 113 */   Iran_Islamic_Republic_of("364", "IRN", "X"), 
/* 114 */   Iraq("368", "IRQ", "F"), 
/* 115 */   Ireland("372", "IRL", "S"), 
/* 116 */   Isle_of_Man("833", "IMN", "S"), 
/* 117 */   Israel("376", "ISR", "S"), 
/* 118 */   Italy("380", "ITA", "S"), 
/* 119 */   Jamaica("388", "JAM", "S"), 
/* 120 */   Japan("392", "JPN", "S"), 
/* 121 */   Jersey("832", "JEY", "S"), 
/* 122 */   Jordan("400", "JOR", "S"), 
/* 123 */   Kazakhstan("398", "KAZ", "S"), 
/* 124 */   Kenya("404", "KEN", "S"), 
/* 125 */   Kiribati("296", "KIR", "F"), 
/* 126 */   Korea_Democratic_Peoples_Republic_of("408", "PRK", "X"), 
/* 127 */   Korea_Republic_of("410", "KOR", "S"), 
/* 128 */   Kuwait("414", "KWT", "S"), 
/* 129 */   Kyrgyzstan("417", "KGZ", "F"), 
/* 130 */   Lao_Peoples_Democratic_Republic("418", "LAO", "F"), 
/* 131 */   Latvia("428", "LVA", "S"), 
/* 132 */   Lebanon("422", "LBN", "S"), 
/* 133 */   Lesotho("426", "LSO", "F"), 
/* 134 */   Liberia("430", "LBR", "S"), 
/* 135 */   Libyan_Arab_Jamahiriya("434", "LBY", "F"), 
/* 136 */   Liechtenstein("438", "LIE", "F"), 
/* 137 */   Lithuania("440", "LTU", "S"), 
/* 138 */   Luxembourg("442", "LUX", "S"), 
/* 139 */   Macao("446", "MAC", "F"), 
/* 140 */   Macedonia("807", "MKD", "S"), 
/* 141 */   Madagascar("450", "MDG", "F"), 
/* 142 */   Malawi("454", "MWI", "S"), 
/* 143 */   Malaysia("458", "MYS", "S"), 
/* 144 */   Maldives("462", "MDV", "F"), 
/* 145 */   Mali("466", "MLI", "F"), 
/* 146 */   Malta("470", "MLT", "S"), 
/* 147 */   Marshall_Islands("584", "MHL", "S"), 
/* 148 */   Martinique("474", "MTQ", "F"), 
/* 149 */   Mauritania("478", "MRT", "S"), 
/* 150 */   Mauritius("480", "MUS", "S"), 
/* 151 */   Mayotte("175", "MYT", "F"), 
/* 152 */   Mexico("484", "MEX", "S"), 
/* 153 */   Micronesia_Federated_States_of("583", "FSM", "S"), 
/* 154 */   Moldova("498", "MDA", "S"), 
/* 155 */   Monaco("492", "MCO", "S"), 
/* 156 */   Mongolia("496", "MNG", "F"), 
/* 157 */   Montenegro("499", "MNE", "S"), 
/* 158 */   Montserrat("500", "MSR", "S"), 
/* 159 */   Morocco("504", "MAR", "S"), 
/* 160 */   Mozambique("508", "MOZ", "F"), 
/* 161 */   Myanmar("104", "MMR", "X"), 
/* 162 */   Namibia("516", "NAM", "F"), 
/* 163 */   Nauru("520", "NRU", "F"), 
/* 164 */   Nepal("524", "NPL", "S"), 
/* 165 */   Netherlands("528", "NLD", "S"), 
/* 166 */   Netherlands_Antilles("530", "ANT", "S"), 
/* 167 */   New_Caledonia("540", "NCL", "F"), 
/* 168 */   New_Zealand("554", "NZL", "S"), 
/* 169 */   Nicaragua("558", "NIC", "F"), 
/* 170 */   Niger("562", "NER", "F"), 
/* 171 */   Nigeria("566", "NGA", "S"), 
/* 172 */   Niue("570", "NIU", "F"), 
/* 173 */   Norfolk_Island("574", "NFK", "F"), 
/* 174 */   Northern_Mariana_Islands("580", "MNP", "S"), 
/* 175 */   Norway("578", "NOR", "S"), 
/* 176 */   Oman("512", "OMN", "S"), 
/* 177 */   Pakistan("586", "PAK", "S"), 
/* 178 */   Palau("585", "PLW", "S"), 
/* 179 */   Palestinian_Territory_Occupied("275", "PSE", "F"), 
/* 180 */   Panama("591", "PAN", "S"), 
/* 181 */   Papua_New_Guinea("598", "PNG", "S"), 
/* 182 */   Paraguay("600", "PRY", "F"), 
/* 183 */   Peru("604", "PER", "F"), 
/* 184 */   Philippines("608", "PHL", "F"), 
/* 185 */   Pitcairn("612", "PCN", "F"), 
/* 186 */   Poland("616", "POL", "S"), 
/* 187 */   Portugal("620", "PRT", "S"), 
/* 188 */   Puerto_Rico("630", "PRI", "S"), 
/* 189 */   Qatar("634", "QAT", "S"), 
/* 190 */   Reunion("638", "REU", "F"), 
/* 191 */   Romania("642", "ROU", "S"), 
/* 192 */   Russian_Federation("643", "RUS", "S"), 
/* 193 */   Rwanda("646", "RWA", "F"), 
/* 194 */   Samoa("882", "WSM", "F"), 
/* 195 */   San_Marino("674", "SMR", "S"), 
/* 196 */   Sao_Tome_and_Principe("678", "STP", "F"), 
/* 197 */   Saudi_Arabia("682", "SAU", "S"), 
/* 198 */   Senegal("686", "SEN", "F"), 
/* 199 */   Serbia("688", "RSD", "S"), 
/* 200 */   Seychelles("690", "SYC", "S"), 
/* 201 */   Sierra_Leone("694", "SLE", "F"), 
/* 202 */   Singapore("702", "SGP", "S"), 
/* 203 */   Slovakia("703", "SVK", "S"), 
/* 204 */   Slovenia("705", "SVN", "S"), 
/* 205 */   Solomon_Islands("90", "SLB", "F"), 
/* 206 */   Somalia("706", "SOM", "F"), 
/* 207 */   South_Africa("710", "ZAF", "F"), 
/* 208 */   South_Georgia_South_Sandwich("239", "SGS", "F"), 
/* 209 */   Spain("724", "ESP", "S"), 
/* 210 */   Sri_Lanka("144", "LKA", "S"), 
/* 211 */   St_Barthelemy("652", "BLM", "S"), 
/* 212 */   St_Helena("654", "SHN", "F"), 
/* 213 */   St_Kitts_Nevis("659", "KNA", "S"), 
/* 214 */   St_Lucia("662", "LCA", "S"), 
/* 215 */   St_Martin("663", "MAF", "S"), 
/* 216 */   St_Pierre_and_Miquelon("666", "SPM", "F"), 
/* 217 */   St_Vincent_and_the_Grenadines("670", "VCT", "S"), 
/* 218 */   Sudan("736", "SDN", "X"), 
/* 219 */   Suriname("740", "SUR", "S"), 
/* 220 */   Svalbard_and_Jan_Mayen_Islands("744", "SJM", "S"), 
/* 221 */   Swaziland("748", "SWZ", "F"), 
/* 222 */   Sweden("752", "SWE", "S"), 
/* 223 */   Switzerland("756", "CHE", "S"), 
/* 224 */   Syrian_Arab_Republic("760", "SYR", "S"), 
/* 225 */   Taiwan("158", "TWN", "S"), 
/* 226 */   Tajikistan("762", "TJK", "F"), 
/* 227 */   Tanzania_United_Republic_of("834", "TZA", "S"), 
/* 228 */   Thailand("764", "THA", "S"), 
/* 229 */   Timor_Leste("626", "TLS", "F"), 
/* 230 */   Togo("768", "TGO", "F"), 
/* 231 */   Tokelau("772", "TKL", "F"), 
/* 232 */   Tonga("776", "TON", "F"), 
/* 233 */   Trinidad_and_Tobago("780", "TTO", "S"), 
/* 234 */   Tunisia("788", "TUN", "S"), 
/* 235 */   Turkey("792", "TUR", "S"), 
/* 236 */   Turkmenistan("795", "TKM", "F"), 
/* 237 */   Turks_and_Caicos_Islands("796", "TCA", "S"), 
/* 238 */   Tuvalu("798", "TUV", "F"), 
/* 239 */   U_S_Minor_Outlying_Islands("581", "UMI", "S"), 
/* 240 */   Uganda("800", "UGA", "F"), 
/* 241 */   Ukraine("804", "UKR", "S"), 
/* 242 */   United_Arab_Emirates("784", "ARE", "S"), 
/* 243 */   United_Kingdom("826", "GBR", "S"), 
/* 244 */   United_States("840", "USA", "S"), 
/* 245 */   Uruguay("858", "URY", "F"), 
/* 246 */   Uzbekistan("860", "UZB", "F"), 
/* 247 */   Vanuatu("548", "VUT", "S"), 
/* 248 */   Vatican_City_State_Holy_See("336", "VAT", "S"), 
/* 249 */   Venezuela("862", "VEN", "F"), 
/* 250 */   Vietnam("704", "VNM", "F"), 
/* 251 */   Virgin_Islands_British("092", "VGB", "S"), 
/* 252 */   Virgin_Islands_U_S("850", "VIR", "S"), 
/* 253 */   Wallis_and_Futuna_Islands("876", "WLF", "F"), 
/* 254 */   Western_Sahara("732", "ESH", "F"), 
/* 255 */   Yemen("887", "YEM", "F"), 
/* 256 */   Zambia("894", "ZMB", "S"), 
/* 257 */   Zimbabwe("716", "ZWE", "S");
/*     */   
/*     */ 
/*     */ 
/*     */   private final String numCountryCode;
/*     */   
/*     */ 
/*     */   private final String alphaCountryCode;
/*     */   
/*     */ 
/*     */   private final String usage;
/*     */   
/*     */ 
/*     */   private XmlCountryCode(String numCountryCode, String alphaCountryCode, String usage)
/*     */   {
/* 272 */     this.alphaCountryCode = alphaCountryCode;
/* 273 */     this.numCountryCode = numCountryCode;
/* 274 */     this.usage = usage;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getAlphaCountryCode()
/*     */   {
/* 280 */     return this.alphaCountryCode;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getNumCountryCode()
/*     */   {
/* 286 */     return this.numCountryCode;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getUsage()
/*     */   {
/* 292 */     return this.usage;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\enumerations\xml\XmlCountryCode.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */