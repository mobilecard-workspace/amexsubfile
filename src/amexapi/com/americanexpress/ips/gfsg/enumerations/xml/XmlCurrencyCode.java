/*     */ package com.americanexpress.ips.gfsg.enumerations.xml;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public enum XmlCurrencyCode
/*     */ {
/*  14 */   Afghanistan_Afghani("971", "AFN", "F", "2", "99999999999"), 
/*  15 */   Albanian_Lek("8", "ALL", "S", "2", "99999999999"), 
/*  16 */   Algerian_Dinar("12", "DZD", "S", "2", "99999999999"), 
/*  17 */   Angolan_Kwanza("973", "AOA", "F", "2", "99999999999"), 
/*  18 */   Argentine_Peso("32", "ARS", "F", "2", "99999999"), 
/*  19 */   Armenian_Dram("51", "AMD", "F", "2", "99999999999"), 
/*  20 */   Aruban_Guilder("533", "AWG", "S", "2", "99999999"), 
/*  21 */   Australian_Dollar("36", "AUD", "S", "2", "999999999"), 
/*  22 */   Azerbaijanian_Manat("944", "AZN", "F", "2", "99999999999"), 
/*  23 */   Bahamian_Dollar("44", "BSD", "S", "2", "99999999"), 
/*  24 */   Bahraini_Dinar("48", "BHD", "S", "3", "99999999999"), 
/*  25 */   Bangladesh_Taka("50", "BDT", "F", "2", "999999999"), 
/*  26 */   Barbados_Dollar("52", "BBD", "S", "2", "99999999"), 
/*  27 */   Belarusian_Ruble("974", "BYR", "F", "0", "99999999999"), 
/*  28 */   Belize_Dollar("84", "BZD", "F", "2", "99999999"), 
/*  29 */   Bermudian_Dollar("60", "BMD", "S", "2", "99999999"), 
/*  30 */   Bhutan_Ngultrum("64", "BTN", "F", "2", "999999999"), 
/*  31 */   Bolivian_Boliviano("68", "BOB", "S", "2", "99999999"), 
/*  32 */   Bosnian_Mark("977", "BAM", "S", "2", "99999999999"), 
/*  33 */   Botswana_Pula("72", "BWP", "S", "2", "99999999999"), 
/*  34 */   Brazilian_Real("986", "BRL", "F", "2", "99999999"), 
/*  35 */   Brunei_Dollar("96", "BND", "S", "2", "999999999"), 
/*  36 */   Bulgarian_Lev("975", "BGN", "S", "2", "99999999999"), 
/*  37 */   Burundi_Franc("108", "BIF", "F", "0", "99999999999"), 
/*  38 */   Cambodian_Riel("116", "KHR", "F", "2", "999999999"), 
/*  39 */   Canadian_Dollar("124", "CAD", "S", "2", "000999999999"), 
/*  40 */   Cape_Verde_Escudo("132", "CVE", "F", "2", "99999999999"), 
/*  41 */   Cayman_Islands_Dollar("136", "KYD", "S", "2", "99999999"), 
/*  42 */   Central_French_Polynesian_Franc("953", "XPF", "F", "0", "999999999"), 
/*  43 */   CFA_Franc_BCEAO_West_Africa("952", "XOF", "S", "0", "99999999999"), 
/*  44 */   CFA_Franc_BEAC_Central_Africa("950", "XAF", "S", "0", "99999999999"), 
/*  45 */   Chilean_Peso("152", "CLP", "F", "0", "99999999"), 
/*  46 */   China_Yuan_Renminbi("156", "CNY", "S", "2", "999999999"), 
/*  47 */   Colombian_Peso("170", "COP", "F", "2", "99999999"), 
/*  48 */   Comoro_Franc("174", "KMF", "F", "0", "99999999999"), 
/*  49 */   Costa_Rican_Colon("188", "CRC", "S", "2", "99999999"), 
/*  50 */   Croatian_Kuna("191", "HRK", "S", "2", "99999999999"), 
/*  51 */   Czech_Koruna("203", "CZK", "S", "2", "99999999999"), 
/*  52 */   Danish_Krone("208", "DKK", "S", "2", "99999999999"), 
/*  53 */   Djibouti_Franc("262", "DJF", "F", "0", "99999999999"), 
/*  54 */   Dominican_Republic_Peso("214", "DOP", "F", "2", "99999999"), 
/*  55 */   E_Caribbean_Dollar("951", "XCD", "S", "2", "99999999"), 
/*  56 */   Egyptian_Pound("818", "EGP", "S", "2", "99999999999"), 
/*  57 */   El_Salvadorian_Colon("222", "SVC", "S", "2", "99999999"), 
/*  58 */   Estonia_Kroon("233", "EEK", "S", "2", "99999999999"), 
/*  59 */   Ethiopian_Birr("230", "ETB", "F", "2", "99999999999"), 
/*  60 */   Euro("978", "EUR", "S", "2", "99999999999"), 
/*  61 */   Falkland_Islands_Pound("238", "FKP", "F", "2", "99999999999"), 
/*  62 */   Fiji_Dollar("242", "FJD", "S", "2", "999999999"), 
/*  63 */   Gambia_Dalasi("270", "GMD", "S", "2", "99999999999"), 
/*  64 */   Georgia_Lari("981", "GEL", "F", "2", "99999999999"), 
/*  65 */   Ghanaian_Cedi("936", "GHS", "F", "2", "99999999999"), 
/*  66 */   Gibraltar_Pound("292", "GIP", "F", "2", "99999999999"), 
/*  67 */   Guatemalan_Quetzal("320", "GTQ", "F", "2", "99999999"), 
/*  68 */   Guinean_Franc("324", "GNF", "F", "0", "99999999999"), 
/*  69 */   Guyana_Dollar("328", "GYD", "S", "2", "99999999"), 
/*  70 */   Haiti_Gourde("332", "HTG", "S", "2", "99999999"), 
/*  71 */   Honduran_Lempira("340", "HNL", "F", "2", "99999999"), 
/*  72 */   Hong_Kong_Dollar("344", "HKD", "S", "2", "999999999"), 
/*  73 */   Hungarian_Forint("348", "HUF", "S", "2", "99999999999"), 
/*  74 */   Icelandic_Krona("352", "ISK", "S", "2", "99999999999"), 
/*  75 */   Indian_Rupee("356", "INR", "F", "2", "999999999"), 
/*  76 */   Indonesian_Rupiah("360", "IDR", "F", "2", "999999999"), 
/*  77 */   Iraqi_Dinar("368", "IQD", "F", "3", "99999999999"), 
/*  78 */   Jamaican_Dollar("388", "JMD", "S", "2", "99999999"), 
/*  79 */   Japanese_Yen("392", "JPY", "S", "0", "999999999"), 
/*  80 */   Jordanian_Dinar("400", "JOD", "S", "3", "99999999999"), 
/*  81 */   Kazakhstan_Tenge("398", "KZT", "S", "2", "99999999999"), 
/*  82 */   Kenyan_Shilling("404", "KES", "S", "2", "99999999999"), 
/*  83 */   Kuwaiti_Dinar("414", "KWD", "S", "3", "99999999999"), 
/*  84 */   Kyrgyzstan_Som("417", "KGS", "F", "2", "99999999999"), 
/*  85 */   Laotian_Kip("418", "LAK", "F", "2", "999999999"), 
/*  86 */   Latvian_Lats("428", "LVL", "S", "2", "99999999999"), 
/*  87 */   Lebanese_Pound("422", "LBP", "S", "2", "99999999999"), 
/*  88 */   Lesotho_Loti("426", "LSL", "F", "2", "99999999999"), 
/*  89 */   Liberian_Dollar("430", "LRD", "S", "2", "99999999999"), 
/*  90 */   Libyan_Dinar("434", "LYD", "F", "3", "99999999999"), 
/*  91 */   Lithuanian_Litas("440", "LTL", "S", "2", "99999999999"), 
/*  92 */   Macao_Pataca("446", "MOP", "F", "2", "999999999"), 
/*  93 */   Macedonia_Denar("807", "MKD", "S", "2", "99999999999"), 
/*  94 */   Malagasy_Ariary("969", "MGA", "F", "2", "99999999999"), 
/*  95 */   Malawi_Kwacha("454", "MWK", "S", "2", "99999999999"), 
/*  96 */   Malaysian_Ringgit("458", "MYR", "S", "2", "999999999"), 
/*  97 */   Maldive_Rufiyaa("462", "MVR", "F", "2", "999999999"), 
/*  98 */   Mauritania_Ouguiya("478", "MRO", "S", "2", "99999999999"), 
/*  99 */   Mauritius_Rupee("480", "MUR", "S", "2", "99999999999"), 
/* 100 */   Mexican_Peso("484", "MXN", "S", "2", "99999999"), 
/* 101 */   Moldovan_Leu("498", "MDL", "S", "2", "99999999999"), 
/* 102 */   Mongolian_Tugrik("496", "MNT", "F", "2", "999999999"), 
/* 103 */   Moroccan_Dirham("504", "MAD", "S", "2", "99999999999"), 
/* 104 */   Mozambique_Metical("943", "MZN", "F", "2", "99999999999"), 
/* 105 */   Namibian_Dollar("516", "NAD", "F", "2", "99999999999"), 
/* 106 */   Nepalese_Rupee("524", "NPR", "S", "2", "999999999"), 
/* 107 */   Netherlands_Antillian_Guilder("532", "ANG", "S", "2", "99999999"), 
/* 108 */   New_Israeli_Shekel("376", "ILS", "S", "2", "99999999999"), 
/* 109 */   New_Taiwan_Dollar("901", "TWD", "S", "2", "999999999"), 
/* 110 */   New_Zealand_Dollar("554", "NZD", "S", "2", "999999999"), 
/* 111 */   Nicaraguan_Cordoba_Oro("558", "NIO", "F", "2", "99999999"), 
/* 112 */   Nigeria_Naira("566", "NGN", "S", "2", "99999999999"), 
/* 113 */   Norwegian_Krone("578", "NOK", "S", "2", "99999999999"), 
/* 114 */   Oman_Rial("512", "OMR", "S", "3", "99999999999"), 
/* 115 */   Pakistan_Rupee("586", "PKR", "S", "2", "999999999"), 
/* 116 */   Panama_Balboa("591", "PAB", "F", "2", "999999999"), 
/* 117 */   Papua_New_Guinea_Kina("598", "PGK", "S", "2", "999999999"), 
/* 118 */   Paraguayan_Guarani("600", "PYG", "F", "2", "99999999"), 
/* 119 */   Peruvian_Nuevo_Sol("604", "PEN", "F", "2", "99999999"), 
/* 120 */   Philippine_Peso("608", "PHP", "F", "2", "999999999"), 
/* 121 */   Polish_Zloty("985", "PLN", "S", "2", "99999999999"), 
/* 122 */   Qatari_Rial("634", "QAR", "S", "2", "99999999999"), 
/* 123 */   Romanian_Leu("946", "RON", "S", "2", "99999999999"), 
/* 124 */   Russian_Federation_Ruble("643", "RUB", "S", "2", "99999999999"), 
/* 125 */   Rwanda_Franc("646", "RWF", "F", "0", "99999999999"), 
/* 126 */   Samoa_Tala("882", "WST", "F", "2", "999999999"), 
/* 127 */   Sao_Tome_and_Principe_Dobra("678", "STD", "F", "2", "99999999999"), 
/* 128 */   Saudi_Arabian_Riyal("682", "SAR", "S", "2", "99999999999"), 
/* 129 */   Serbian_Dinar("941", "RSD", "S", "2", "99999999999"), 
/* 130 */   Seychelles_Rupee("690", "SCR", "S", "2", "99999999999"), 
/* 131 */   Sierra_Leone_Leone("694", "SLL", "F", "2", "99999999999"), 
/* 132 */   Singapore_Dollar("702", "SGD", "S", "2", "999999999"), 
/* 133 */   Solomon_Islands_Dollar("90", "SBD", "F", "2", "999999999"), 
/* 134 */   Somali_Shilling("706", "SOS", "F", "2", "99999999999"), 
/* 135 */   South_African_Rand("710", "ZAR", "F", "2", "99999999999"), 
/* 136 */   South_Korean_Won("410", "KRW", "S", "0", "999999999"), 
/* 137 */   Sri_Lanka_Rupee("144", "LKR", "S", "2", "999999999"), 
/* 138 */   St_Helena_Pound("654", "SHP", "F", "2", "99999999999"), 
/* 139 */   Surinam_Dollar("968", "SRD", "S", "2", "99999999"), 
/* 140 */   Swaziland_Lilangeni("748", "SZL", "F", "2", "99999999999"), 
/* 141 */   Swedish_Krona("752", "SEK", "S", "2", "99999999999"), 
/* 142 */   Swiss_Franc("756", "CHF", "S", "2", "99999999999"), 
/* 143 */   Syrian_Pound("760", "SYP", "S", "2", "99999999999"), 
/* 144 */   Tajik_Somoni("972", "TJS", "F", "2", "99999999999"), 
/* 145 */   Tanzanian_Shilling("834", "TZS", "S", "2", "99999999999"), 
/* 146 */   Thailand_Baht("764", "THB", "S", "2", "999999999"), 
/* 147 */   Tongan_Paanga("776", "TOP", "F", "2", "999999999"), 
/* 148 */   Trinidad_and_Tobago_Dollar("780", "TTD", "S", "2", "99999999"), 
/* 149 */   Tunisian_Dinar("788", "TND", "S", "3", "99999999999"), 
/* 150 */   Turkish_Lira("949", "TRY", "S", "2", "99999999999"), 
/* 151 */   Turkmenistan_New_Manat("934", "TMT", "F", "2", "99999999999"), 
/* 152 */   UAE_Dirham("784", "AED", "S", "2", "99999999999"), 
/* 153 */   US_Dollar("840", "USD", "S", "2", "999999999"), 
/* 154 */   Uganda_Shilling("800", "UGX", "F", "2", "99999999999"), 
/* 155 */   Ukraine_Hryvnia("980", "UAH", "S", "2", "99999999999"), 
/* 156 */   United_Kingdom_Pound_Sterling("826", "GBP", "S", "2", "99999999999"), 
/* 157 */   Uruguayan_Peso("858", "UYU", "F", "2", "99999999"), 
/* 158 */   Uzbekistan_Som("860", "UZS", "F", "2", "99999999999"), 
/* 159 */   Vanuatu_Vatu("548", "VUV", "S", "2", "999999999"), 
/* 160 */   Venezuelan_Bolivar_Fuerte("937", "VEF", "F", "2", "99999999"), 
/* 161 */   Vietnamese_Dong("704", "VND", "F", "2", "999999999"), 
/* 162 */   Yemeni_Rial("886", "YER", "F", "2", "99999999999"), 
/* 163 */   Zambia_Kwacha("894", "ZMK", "S", "2", "99999999999"), 
/* 164 */   Zimbabwe_Dollar("932", "ZWL", "S", "2", "99999999999"), 
/* 165 */   Cuban_Peso("", "", "X", "", ""),  Iranian_Real("", "", "X", "", ""),  Myanmar_Kyat("", "", "X", "", ""), 
/* 166 */   North_Korean_Won("", "", "X", "", ""),  Sudanese_Pound("", "", "X", "", "");
/*     */   
/*     */ 
/*     */   private final String numCurrencyCode;
/*     */   
/*     */   private final String alphaCurrencyCode;
/*     */   
/*     */   private final String usage;
/*     */   
/*     */   private final String dec;
/*     */   
/*     */   private final String maxTransAmt;
/*     */   
/*     */ 
/*     */   private XmlCurrencyCode(String numCurrencyCode, String alphaCurrencyCode, String usage, String dec, String maxTransAmt)
/*     */   {
/* 182 */     this.numCurrencyCode = numCurrencyCode;
/* 183 */     this.alphaCurrencyCode = alphaCurrencyCode;
/* 184 */     this.usage = usage;
/* 185 */     this.dec = dec;
/* 186 */     this.maxTransAmt = maxTransAmt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public String getAlphaCurrencyCode()
/*     */   {
/* 193 */     return this.alphaCurrencyCode;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public String getNumCurrencyCode()
/*     */   {
/* 200 */     return this.numCurrencyCode;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public String getUsage()
/*     */   {
/* 207 */     return this.usage;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public String getDec()
/*     */   {
/* 214 */     return this.dec;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getMaxTransAmt()
/*     */   {
/* 222 */     return this.maxTransAmt;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\enumerations\xml\XmlCurrencyCode.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */