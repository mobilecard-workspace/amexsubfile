/*     */ package com.americanexpress.ips.gfsg.formats;
/*     */ 
/*     */ import com.americanexpress.ips.gfsg.beans.SettlementRequestDataObject;
/*     */ import com.americanexpress.ips.gfsg.beans.common.TransactionAdviceAddendumBean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.TransactionAdviceBasicBean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.TransactionTBTSpecificBean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.taa.IndustryTypeTAABean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.taa.LocationDetailTAABean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.taa.NonIndustrySpecificTAABean;
/*     */ import com.americanexpress.ips.gfsg.connection.PropertyReader;
/*     */ import com.americanexpress.ips.gfsg.validator.CommonValidator;
/*     */ import java.io.IOException;
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ import org.apache.log4j.Logger;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class SettlementMessageFormatter
/*     */ {
/*  34 */   private static final Logger LOGGER = Logger.getLogger(SettlementMessageFormatter.class);
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static List<String> formatISORequest(SettlementRequestDataObject settlementReqBean)
/*     */     throws IOException
/*     */   {
/*  47 */     List<String> formatedMessageList = new ArrayList();
/*     */     
/*     */ 
/*     */ 
/*  51 */     TFHRFormatter.formatTFHRecord(settlementReqBean.getTransactionFileHeaderType(), formatedMessageList);
/*     */     
/*  53 */     if ((settlementReqBean.getTransactionTBTSpecificType() != null) && (!settlementReqBean.getTransactionTBTSpecificType().isEmpty()))
/*     */     {
/*     */ 
/*  56 */       for (TransactionTBTSpecificBean transactionTBTSpecifictType : settlementReqBean.getTransactionTBTSpecificType())
/*     */       {
/*     */ 
/*  59 */         if ((transactionTBTSpecifictType.getTransactionAdviceBasicType() != null) && (!transactionTBTSpecifictType.getTransactionAdviceBasicType().isEmpty()))
/*     */         {
/*     */ 
/*  62 */           for (TransactionAdviceBasicBean transactionAdviceBasicType : transactionTBTSpecifictType.getTransactionAdviceBasicType())
/*     */           {
/*  64 */             formatTABRecordType(transactionAdviceBasicType, formatedMessageList);
/*     */           }
/*     */         }
/*     */         
/*     */ 
/*     */ 
/*  70 */         TBTRecordFormatter.formatTBTRecord(transactionTBTSpecifictType.getTransactionBatchTrailerBean(), formatedMessageList);
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*  76 */     TFSRecordFormatter.formatTFSRecord(settlementReqBean.getTransactionFileSummaryType(), formatedMessageList);
/*     */     
/*  78 */     return formatedMessageList;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void formatTABRecordType(TransactionAdviceBasicBean transactionAdviceBasicBean, List<String> formatedMessageList)
/*     */     throws IOException
/*     */   {
/*  90 */     StringBuffer formattedMessage = new StringBuffer();
/*     */     
/*     */ 
/*  93 */     formatValue(transactionAdviceBasicBean.getRecordType(), formattedMessage, 3, false, true, true, false, true);
/*     */     
/*  95 */     formatValue(transactionAdviceBasicBean.getRecordNumber(), formattedMessage, 8, true, false, false, true, false);
/*     */     
/*  97 */     formatValue(transactionAdviceBasicBean.getTransactionIdentifier(), formattedMessage, 15, true, false, false, true, false);
/*     */     
/*     */ 
/* 100 */     formatValue(transactionAdviceBasicBean.getFormatCode(), formattedMessage, 2, false, true, false, false, true);
/*     */     
/*     */ 
/* 103 */     formatValue(transactionAdviceBasicBean.getMediaCode(), formattedMessage, 2, false, true, false, false, true);
/*     */     
/* 105 */     formatValue(transactionAdviceBasicBean.getSubmissionMethod(), formattedMessage, 2, false, true, false, false, true);
/*     */     
/*     */ 
/* 108 */     characterSpacesFill(formattedMessage, 10);
/*     */     
/* 110 */     formatValue(transactionAdviceBasicBean.getApprovalCode(), formattedMessage, 6, false, true, false, false, true);
/*     */     
/* 112 */     formatPANNumber(transactionAdviceBasicBean.getPrimaryAccountNumber(), formattedMessage, 19, false, true, false, false, true);
/*     */     
/* 114 */     formatValue(transactionAdviceBasicBean.getCardExpiryDate(), formattedMessage, 4, true, false, false, true, false);
/*     */     
/* 116 */     formatValue(transactionAdviceBasicBean.getTransactionDate(), formattedMessage, 8, true, false, false, true, false);
/*     */     
/* 118 */     formatValue(transactionAdviceBasicBean.getTransactionTime(), formattedMessage, 6, true, false, false, true, false);
/*     */     
/*     */ 
/* 121 */     zeroFill(formattedMessage, 3);
/*     */     
/* 123 */     formatValue(transactionAdviceBasicBean.getTransactionAmount(), formattedMessage, 12, true, false, false, true, false);
/*     */     
/* 125 */     formatValue(transactionAdviceBasicBean.getProcessingCode(), formattedMessage, 6, true, false, false, true, false);
/*     */     
/* 127 */     formatValue(transactionAdviceBasicBean.getTransactionCurrencyCode(), formattedMessage, 3, false, true, true, false, true);
/*     */     
/* 129 */     formatValue(transactionAdviceBasicBean.getExtendedPaymentData(), formattedMessage, 2, true, false, false, true, false);
/*     */     
/* 131 */     formatValue(transactionAdviceBasicBean.getMerchantId(), formattedMessage, 15, false, true, false, false, true);
/*     */     
/* 133 */     formatValue(transactionAdviceBasicBean.getMerchantLocationId(), formattedMessage, 15, false, true, true, false, true);
/*     */     
/* 135 */     formatValue(transactionAdviceBasicBean.getMerchantContactInfo(), formattedMessage, 40, false, true, true, false, true);
/*     */     
/*     */ 
/* 138 */     formatValue(transactionAdviceBasicBean.getTerminalId(), formattedMessage, 8, false, true, true, false, true);
/*     */     
/* 140 */     formatValue(transactionAdviceBasicBean.getPosDataCode(), formattedMessage, 12, false, true, true, false, true);
/*     */     
/*     */ 
/*     */ 
/* 144 */     zeroFill(formattedMessage, 3);
/*     */     
/*     */ 
/* 147 */     zeroFill(formattedMessage, 12);
/*     */     
/*     */ 
/* 150 */     characterSpacesFill(formattedMessage, 3);
/*     */     
/* 152 */     formatValue(transactionAdviceBasicBean.getInvoiceReferenceNumber(), formattedMessage, 30, false, true, true, false, true);
/*     */     
/*     */ 
/*     */ 
/* 156 */     characterSpacesFill(formattedMessage, 15);
/*     */     
/* 158 */     formatValue(transactionAdviceBasicBean.getTabImageSequenceNumber(), formattedMessage, 8, false, true, false, false, true);
/*     */     
/* 160 */     formatValue(transactionAdviceBasicBean.getMatchingKeyType(), formattedMessage, 2, false, true, false, false, true);
/*     */     
/* 162 */     formatValue(transactionAdviceBasicBean.getMatchingKey(), formattedMessage, 21, false, true, true, false, true);
/*     */     
/* 164 */     formatValue(transactionAdviceBasicBean.getElectronicCommerceIndicator(), formattedMessage, 2, false, true, false, false, true);
/*     */     
/*     */ 
/* 167 */     if (PropertyReader.getFileType().equalsIgnoreCase("FIXED")) {
/* 168 */       characterSpacesFill(formattedMessage, 403);
/*     */     }
/* 170 */     formatedMessageList.add(formattedMessage.toString());
/*     */     
/* 172 */     if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
/* 173 */       LOGGER.info("Formated TAB Type Record :" + formattedMessage.toString());
/*     */     }
/*     */     
/* 176 */     if (transactionAdviceBasicBean.getTransactionAdviceDetailBean() != null)
/*     */     {
/* 178 */       TADRecordFormatter.formatTADRecord(transactionAdviceBasicBean.getTransactionAdviceDetailBean(), formatedMessageList);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 184 */     ArrayList<TransactionAdviceAddendumBean> taaBeanList = transactionAdviceBasicBean.getArrayTAABean();
/*     */     
/* 186 */     if (taaBeanList != null) {
/* 187 */       for (TransactionAdviceAddendumBean taaBean : taaBeanList)
/*     */       {
/* 189 */         if ((taaBean instanceof NonIndustrySpecificTAABean)) {
/* 190 */           NonIndustrySpecificTAABean nonIndustrySpecificTAABean = (NonIndustrySpecificTAABean)taaBean;
/*     */           
/* 192 */           NonIndustryTAARecordFormatter.formatNonIndustrySpecificTypeTAARecord(nonIndustrySpecificTAABean, formatedMessageList);
/*     */         }
/*     */         
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 199 */         if ((taaBean instanceof IndustryTypeTAABean))
/*     */         {
/* 201 */           IndustryTypeTAABean industrySpecificTAABean = (IndustryTypeTAABean)taaBean;
/* 202 */           IndustryTAARecordFormatter.formatIndustrySpecificTypeTAARecord(industrySpecificTAABean, formatedMessageList);
/*     */         }
/*     */       }
/*     */     }
/*     */     
/* 207 */     if (transactionAdviceBasicBean.getLocationDetailTAABean() == null) {
/* 208 */       transactionAdviceBasicBean.setLocationDetailTAABean(new LocationDetailTAABean());
/*     */     }
/* 210 */     LocationTAAFormatter.formatLocationTAARecord(transactionAdviceBasicBean.getLocationDetailTAABean(), formatedMessageList);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static String formatValue(String fieldValue, StringBuffer sb, int maxLength, boolean isRightJustify, boolean isLeftJustify, boolean isUppercase, boolean isZeroFill, boolean isCharSpacesfill)
/*     */   {
/* 241 */     String formattedValue = "";
/*     */     
/* 243 */     if (CommonValidator.isNullOrEmpty(fieldValue))
/*     */     {
/* 245 */       if (isZeroFill)
/*     */       {
/* 247 */         zeroFill(sb, maxLength);
/*     */       }
/*     */       
/*     */ 
/* 251 */       if (isCharSpacesfill)
/*     */       {
/* 253 */         characterSpacesFill(sb, maxLength);
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 258 */     if (!CommonValidator.isNullOrEmpty(fieldValue))
/*     */     {
/* 260 */       formattedValue = fieldValue;
/*     */       
/* 262 */       if (isUppercase)
/*     */       {
/* 264 */         formattedValue = formattedValue.toUpperCase();
/*     */       }
/*     */       
/* 267 */       if (isLeftJustify) {
/* 268 */         formattedValue = leftJustify(formattedValue, maxLength);
/*     */       }
/*     */       
/*     */ 
/* 272 */       if (isRightJustify) {
/* 273 */         formattedValue = rightJustify(formattedValue, maxLength);
/*     */       }
/*     */       
/*     */ 
/* 277 */       sb.append(formattedValue);
/*     */     }
/*     */     
/* 280 */     if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
/* 281 */       LOGGER.info("Formated ISO Field Value:" + fieldValue);
/*     */     }
/*     */     
/*     */ 
/* 285 */     return sb.toString();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void characterSpacesFill(StringBuffer stringBuffer, int length)
/*     */   {
/* 298 */     for (int i = 0; i < length; i++) {
/* 299 */       stringBuffer.append(' ');
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void zeroFill(StringBuffer stringBuffer, int length)
/*     */   {
/* 311 */     for (int i = 0; i < length; i++) {
/* 312 */       stringBuffer.append('0');
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static String rightJustify(String dataField, int length)
/*     */   {
/* 326 */     int j = dataField.length();
/* 327 */     for (int i = j; i < length; i++) {
/* 328 */       dataField = "0" + dataField;
/*     */     }
/* 330 */     return dataField;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static String leftJustify(String dataField, int length)
/*     */   {
/* 343 */     int j = dataField.length();
/* 344 */     for (int i = j; i < length; i++) {
/* 345 */       dataField = dataField + " ";
/*     */     }
/* 347 */     return dataField;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static String formatPANNumber(String fieldValue, StringBuffer sb, int maxLength, boolean isRightJustify, boolean isLeftJustify, boolean isUppercase, boolean isZeroFill, boolean isCharSpacesfill)
/*     */   {
/* 373 */     String formattedValue = "";
/*     */     
/* 375 */     if (CommonValidator.isNullOrEmpty(fieldValue))
/*     */     {
/* 377 */       if (isZeroFill)
/*     */       {
/* 379 */         zeroFill(sb, maxLength);
/*     */       }
/*     */       
/*     */ 
/* 383 */       if (isCharSpacesfill)
/*     */       {
/* 385 */         characterSpacesFill(sb, maxLength);
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 390 */     if (!CommonValidator.isNullOrEmpty(fieldValue))
/*     */     {
/* 392 */       formattedValue = fieldValue;
/*     */       
/* 394 */       if (isUppercase)
/*     */       {
/* 396 */         formattedValue = formattedValue.toUpperCase();
/*     */       }
/*     */       
/* 399 */       if (isLeftJustify) {
/* 400 */         formattedValue = leftJustify(formattedValue, maxLength);
/*     */       }
/*     */       
/*     */ 
/* 404 */       if (isRightJustify) {
/* 405 */         formattedValue = rightJustify(formattedValue, maxLength);
/*     */       }
/*     */       
/*     */ 
/* 409 */       sb.append(formattedValue);
/*     */     }
/*     */     
/* 412 */     if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
/* 413 */       LOGGER.info("Formated ISO Field Value:" + performMasking(fieldValue, false, true, 5, 4));
/*     */     }
/*     */     
/*     */ 
/* 417 */     return sb.toString();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static String performMasking(String value, boolean isAll, boolean isNotAll, int startIndex, int endIndex)
/*     */   {
/* 436 */     String temp = "";
/* 437 */     String temp2 = "";
/* 438 */     String temp3 = "";
/* 439 */     if (isAll) {
/* 440 */       value = CommonValidator.getMasked(value.length(), "*");
/* 441 */     } else if (isNotAll) {
/* 442 */       if (startIndex < value.length()) {
/* 443 */         temp = value.substring(0, startIndex);
/* 444 */         temp2 = value.substring(startIndex, value.length() - endIndex);
/* 445 */         temp2 = CommonValidator.getMasked(temp2.length(), "0");
/* 446 */         temp3 = value.substring(temp.length() + temp2.length(), temp.length() + temp2.length() + endIndex);
/*     */       }
/* 448 */       value = temp + temp2 + temp3;
/*     */     }
/* 450 */     return value;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\formats\SettlementMessageFormatter.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */