/*      */ package com.americanexpress.ips.gfsg.formats;
/*      */ 
/*      */ import com.americanexpress.ips.gfsg.beans.common.taa.IndustryTypeTAABean;
/*      */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.AirlineIndustryTAABean;
/*      */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.AutoRentalIndustryTAABean;
/*      */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.CommunicationServicesIndustryBean;
/*      */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.EntertainmentTicketingIndustryTAABean;
/*      */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.InsuranceIndustryTAABean;
/*      */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.LodgingIndustryTAABean;
/*      */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.RailIndustryTAABean;
/*      */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.RetailIndustryTAABean;
/*      */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.TravelCruiseIndustryTAABean;
/*      */ import com.americanexpress.ips.gfsg.connection.PropertyReader;
/*      */ import com.americanexpress.ips.gfsg.validator.CommonValidator;
/*      */ import java.io.IOException;
/*      */ import java.util.List;
/*      */ import org.apache.log4j.Logger;
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ public final class IndustryTAARecordFormatter
/*      */ {
/*   33 */   private static final Logger LOGGER = Logger.getLogger(IndustryTAARecordFormatter.class);
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void formatIndustrySpecificTypeTAARecord(IndustryTypeTAABean industryTypeTAABean, List<String> formatedMessageList)
/*      */     throws IOException
/*      */   {
/*   49 */     if ((industryTypeTAABean instanceof AirlineIndustryTAABean)) {
/*   50 */       AirlineIndustryTAABean airlineIndustryTAABean = (AirlineIndustryTAABean)industryTypeTAABean;
/*   51 */       formatAirlineIndustryTypeRecord(airlineIndustryTAABean, 
/*   52 */         formatedMessageList);
/*      */     }
/*   54 */     else if ((industryTypeTAABean instanceof AutoRentalIndustryTAABean)) {
/*   55 */       AutoRentalIndustryTAABean autoRentalIndustryTAABean = (AutoRentalIndustryTAABean)industryTypeTAABean;
/*   56 */       formatAutoRentalIndustryTypeRecord(autoRentalIndustryTAABean, 
/*   57 */         formatedMessageList);
/*      */     }
/*   59 */     else if ((industryTypeTAABean instanceof CommunicationServicesIndustryBean)) {
/*   60 */       CommunicationServicesIndustryBean communicationServicesIndustryBean = (CommunicationServicesIndustryBean)industryTypeTAABean;
/*   61 */       formatCommunicationServicesIndustryTypeRecord(
/*   62 */         communicationServicesIndustryBean, formatedMessageList);
/*      */     }
/*   64 */     else if ((industryTypeTAABean instanceof EntertainmentTicketingIndustryTAABean)) {
/*   65 */       EntertainmentTicketingIndustryTAABean entertainmentTicketingIndustryTAABean = (EntertainmentTicketingIndustryTAABean)industryTypeTAABean;
/*   66 */       formatEntertainmentTicketingIndustryTypeRecord(
/*   67 */         entertainmentTicketingIndustryTAABean, formatedMessageList);
/*      */     }
/*   69 */     else if ((industryTypeTAABean instanceof InsuranceIndustryTAABean)) {
/*   70 */       InsuranceIndustryTAABean insuranceIndustryTAABean = (InsuranceIndustryTAABean)industryTypeTAABean;
/*   71 */       formatInsuranceIndustryTypeRecord(insuranceIndustryTAABean, 
/*   72 */         formatedMessageList);
/*      */     }
/*   74 */     else if ((industryTypeTAABean instanceof LodgingIndustryTAABean))
/*      */     {
/*   76 */       LodgingIndustryTAABean lodgingIndustryTAABean = (LodgingIndustryTAABean)industryTypeTAABean;
/*   77 */       formatLodgingTypeRecord(lodgingIndustryTAABean, formatedMessageList);
/*   78 */     } else if ((industryTypeTAABean instanceof RailIndustryTAABean)) {
/*   79 */       RailIndustryTAABean railIndustryTAABean = (RailIndustryTAABean)industryTypeTAABean;
/*   80 */       formatRailIndustryTypeRecord(railIndustryTAABean, 
/*   81 */         formatedMessageList);
/*      */     }
/*   83 */     else if ((industryTypeTAABean instanceof RetailIndustryTAABean)) {
/*   84 */       RetailIndustryTAABean retailIndustryTAABean = (RetailIndustryTAABean)industryTypeTAABean;
/*   85 */       formatRetailIndustryTypeRecord(retailIndustryTAABean, 
/*   86 */         formatedMessageList);
/*      */     }
/*   88 */     else if ((industryTypeTAABean instanceof TravelCruiseIndustryTAABean)) {
/*   89 */       TravelCruiseIndustryTAABean travelCruiseIndustryTAABean = (TravelCruiseIndustryTAABean)industryTypeTAABean;
/*   90 */       formatTravelCruiseIndustryTypeRecord(travelCruiseIndustryTAABean, 
/*   91 */         formatedMessageList);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void formatLodgingTypeRecord(LodgingIndustryTAABean lodgingIndustryTAABean, List<String> formatedMessageList)
/*      */     throws IOException
/*      */   {
/*  108 */     if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
/*  109 */       LOGGER.info("Entered in to formatLodgingTypeRecord()");
/*      */     }
/*      */     
/*  112 */     StringBuffer formattedMessage = new StringBuffer();
/*      */     
/*  114 */     SettlementMessageFormatter.formatValue(lodgingIndustryTAABean
/*  115 */       .getRecordType(), formattedMessage, 3, false, true, true, 
/*  116 */       false, true);
/*      */     
/*  118 */     SettlementMessageFormatter.formatValue(lodgingIndustryTAABean
/*  119 */       .getRecordNumber(), formattedMessage, 8, true, false, false, 
/*  120 */       true, false);
/*      */     
/*  122 */     SettlementMessageFormatter.formatValue(lodgingIndustryTAABean
/*  123 */       .getTransactionIdentifier(), formattedMessage, 15, true, 
/*  124 */       false, false, true, false);
/*      */     
/*  126 */     SettlementMessageFormatter.formatValue(lodgingIndustryTAABean
/*  127 */       .getFormatCode(), formattedMessage, 2, false, true, false, 
/*  128 */       false, true);
/*      */     
/*  130 */     SettlementMessageFormatter.formatValue(lodgingIndustryTAABean
/*  131 */       .getAddendaTypeCode(), formattedMessage, 2, false, true, 
/*  132 */       false, false, true);
/*      */     
/*  134 */     if (CommonValidator.isNullOrEmpty(lodgingIndustryTAABean
/*  135 */       .getLodgingSpecialProgramCode()))
/*      */     {
/*  137 */       lodgingIndustryTAABean.setLodgingSpecialProgramCode("1");
/*      */     }
/*      */     
/*      */ 
/*  141 */     SettlementMessageFormatter.formatValue(lodgingIndustryTAABean
/*  142 */       .getLodgingSpecialProgramCode(), formattedMessage, 1, true, 
/*  143 */       false, false, true, false);
/*      */     
/*  145 */     SettlementMessageFormatter.formatValue(lodgingIndustryTAABean
/*  146 */       .getLodgingCheckInDate(), formattedMessage, 8, true, false, 
/*  147 */       false, true, false);
/*      */     
/*  149 */     SettlementMessageFormatter.formatValue(lodgingIndustryTAABean
/*  150 */       .getLodgingCheckOutDate(), formattedMessage, 8, true, false, 
/*  151 */       false, true, false);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  158 */     SettlementMessageFormatter.zeroFill(formattedMessage, 3);
/*      */     
/*  160 */     SettlementMessageFormatter.formatValue(lodgingIndustryTAABean
/*  161 */       .getLodgingRoomRate1(), formattedMessage, 12, true, false, 
/*  162 */       false, true, false);
/*      */     
/*  164 */     SettlementMessageFormatter.formatValue(lodgingIndustryTAABean
/*  165 */       .getNumberOfNightsAtRoomRate2(), formattedMessage, 2, true, 
/*  166 */       false, false, true, false);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  173 */     SettlementMessageFormatter.zeroFill(formattedMessage, 3);
/*      */     
/*  175 */     SettlementMessageFormatter.formatValue(lodgingIndustryTAABean
/*  176 */       .getLodgingRoomRate2(), formattedMessage, 12, true, false, 
/*  177 */       false, true, false);
/*      */     
/*  179 */     SettlementMessageFormatter.formatValue(lodgingIndustryTAABean
/*  180 */       .getNumberOfNightsAtRoomRate2(), formattedMessage, 2, true, 
/*  181 */       false, false, true, false);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  188 */     SettlementMessageFormatter.zeroFill(formattedMessage, 3);
/*      */     
/*  190 */     SettlementMessageFormatter.formatValue(lodgingIndustryTAABean
/*  191 */       .getLodgingRoomRate3(), formattedMessage, 12, true, false, 
/*  192 */       false, true, false);
/*      */     
/*  194 */     SettlementMessageFormatter.formatValue(lodgingIndustryTAABean
/*  195 */       .getNumberOfNightsAtRoomRate3(), formattedMessage, 2, true, 
/*  196 */       false, false, true, false);
/*      */     
/*  198 */     SettlementMessageFormatter.formatValue(lodgingIndustryTAABean
/*  199 */       .getLodgingRenterName(), formattedMessage, 26, false, true, 
/*  200 */       true, false, true);
/*      */     
/*  202 */     SettlementMessageFormatter.formatValue(lodgingIndustryTAABean
/*  203 */       .getLodgingFolioNumber(), formattedMessage, 12, false, true, 
/*  204 */       true, false, true);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  210 */     if (PropertyReader.getFileType().equalsIgnoreCase("FIXED")) {
/*  211 */       SettlementMessageFormatter.characterSpacesFill(formattedMessage, 564);
/*      */     }
/*  213 */     formatedMessageList.add(formattedMessage.toString());
/*  214 */     if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
/*  215 */       LOGGER.info("Formated Lodging Industry Type Record :" + formattedMessage.toString());
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void formatAirlineIndustryTypeRecord(AirlineIndustryTAABean airlineIndustryTAABean, List<String> formatedMessageList)
/*      */     throws IOException
/*      */   {
/*  231 */     StringBuffer formattedMessage = new StringBuffer();
/*      */     
/*  233 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  234 */       .getRecordType(), formattedMessage, 3, false, true, true, 
/*  235 */       false, true);
/*      */     
/*  237 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  238 */       .getRecordNumber(), formattedMessage, 8, true, false, false, 
/*  239 */       true, false);
/*      */     
/*  241 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  242 */       .getTransactionIdentifier(), formattedMessage, 15, true, 
/*  243 */       false, false, true, false);
/*      */     
/*  245 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  246 */       .getFormatCode(), formattedMessage, 2, false, true, false, 
/*  247 */       false, true);
/*      */     
/*  249 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  250 */       .getAddendaTypeCode(), formattedMessage, 2, false, true, 
/*  251 */       false, false, true);
/*      */     
/*  253 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  254 */       .getTransactionType(), formattedMessage, 3, false, true, true, 
/*  255 */       false, true);
/*      */     
/*  257 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  258 */       .getTicketNumber(), formattedMessage, 14, false, true, true, 
/*  259 */       false, true);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  266 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  267 */       .getDocumentType(), formattedMessage, 2, false, true, false, 
/*  268 */       false, true);
/*      */     
/*  270 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  271 */       .getAirlineProcessIdentifier(), formattedMessage, 3, false, 
/*  272 */       true, true, false, true);
/*      */     
/*  274 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  275 */       .getIataNumericCode(), formattedMessage, 8, false, true, true, 
/*  276 */       false, true);
/*      */     
/*  278 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  279 */       .getTicketingCarrierName(), formattedMessage, 25, false, true, 
/*  280 */       true, false, true);
/*      */     
/*  282 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  283 */       .getTicketIssueCity(), formattedMessage, 18, false, true, true, 
/*  284 */       false, true);
/*      */     
/*  286 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  287 */       .getTicketIssueDate(), formattedMessage, 8, true, false, 
/*  288 */       false, true, false);
/*  289 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  290 */       .getNumberInParty(), formattedMessage, 3, true, false, false, 
/*  291 */       true, false);
/*      */     
/*  293 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  294 */       .getPassengerName(), formattedMessage, 25, false, true, true, 
/*  295 */       false, true);
/*      */     
/*  297 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  298 */       .getConjunctionTicketIndicator(), formattedMessage, 1, false, 
/*  299 */       true, false, false, true);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  306 */     SettlementMessageFormatter.zeroFill(formattedMessage, 3);
/*      */     
/*  308 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  309 */       .getOriginalTransactionAmount(), formattedMessage, 12, true, 
/*  310 */       false, false, true, false);
/*  311 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  312 */       .getOriginalCurrencyCode(), formattedMessage, 3, false, true, 
/*  313 */       false, false, true);
/*      */     
/*  315 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  316 */       .getElectronicTicketIndicator(), formattedMessage, 1, false, 
/*  317 */       true, false, false, true);
/*      */     
/*  319 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  320 */       .getTotalNumberOfAirSegments(), formattedMessage, 1, true, 
/*  321 */       false, false, true, false);
/*      */     
/*  323 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  324 */       .getStopoverIndicator1(), formattedMessage, 1, false, true, 
/*  325 */       false, false, true);
/*      */     
/*  327 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  328 */       .getDepartureLocationCodeSegment1(), formattedMessage, 3, 
/*  329 */       false, true, false, false, true);
/*      */     
/*      */ 
/*  332 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  333 */       .getDepartureDateSegment1(), formattedMessage, 8, false, true, 
/*  334 */       false, true, false);
/*      */     
/*      */ 
/*  337 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  338 */       .getArrivalLocationCodeSegment1(), formattedMessage, 3, false, 
/*  339 */       true, false, false, true);
/*      */     
/*  341 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  342 */       .getSegmentCarrierCode1(), formattedMessage, 2, false, true, 
/*  343 */       false, false, true);
/*      */     
/*  345 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  346 */       .getSegment1FareBasis(), formattedMessage, 15, false, true, 
/*  347 */       true, false, true);
/*      */     
/*  349 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  350 */       .getClassOfServiceCodeSegment1(), formattedMessage, 2, false, 
/*  351 */       true, false, false, true);
/*      */     
/*  353 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  354 */       .getFlightNumberSegment1(), formattedMessage, 4, false, true, 
/*  355 */       true, false, true);
/*      */     
/*  357 */     SettlementMessageFormatter.zeroFill(formattedMessage, 3);
/*      */     
/*  359 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  360 */       .getSegment1Fare(), formattedMessage, 12, true, false, false, 
/*  361 */       true, false);
/*      */     
/*  363 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  364 */       .getStopoverIndicator2(), formattedMessage, 1, false, true, 
/*  365 */       false, false, true);
/*      */     
/*  367 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  368 */       .getDepartureLocationCodeSegment2(), formattedMessage, 3, 
/*  369 */       false, true, false, false, true);
/*      */     
/*  371 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  372 */       .getDepartureDateSegment2(), formattedMessage, 8, true, false, 
/*  373 */       false, true, false);
/*      */     
/*  375 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  376 */       .getArrivalLocationCodeSegment2(), formattedMessage, 3, false, 
/*  377 */       true, false, false, true);
/*      */     
/*  379 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  380 */       .getSegmentCarrierCode2(), formattedMessage, 2, false, true, 
/*  381 */       false, false, true);
/*      */     
/*  383 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  384 */       .getSegment2FareBasis(), formattedMessage, 15, false, true, 
/*  385 */       true, false, true);
/*      */     
/*  387 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  388 */       .getClassOfServiceCodeSegment2(), formattedMessage, 2, false, 
/*  389 */       true, false, false, true);
/*      */     
/*  391 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  392 */       .getFlightNumberSegment2(), formattedMessage, 4, false, true, 
/*  393 */       true, false, true);
/*      */     
/*  395 */     SettlementMessageFormatter.zeroFill(formattedMessage, 3);
/*      */     
/*  397 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  398 */       .getSegment2Fare(), formattedMessage, 12, true, false, false, 
/*  399 */       true, false);
/*      */     
/*  401 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  402 */       .getStopoverIndicator3(), formattedMessage, 1, false, true, 
/*  403 */       false, false, true);
/*      */     
/*  405 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  406 */       .getDepartureLocationCodeSegment3(), formattedMessage, 3, 
/*  407 */       false, true, false, false, true);
/*      */     
/*  409 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  410 */       .getDepartureDateSegment3(), formattedMessage, 8, true, false, 
/*  411 */       false, true, false);
/*      */     
/*  413 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  414 */       .getArrivalLocationCodeSegment3(), formattedMessage, 3, false, 
/*  415 */       true, false, false, true);
/*      */     
/*  417 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  418 */       .getSegmentCarrierCode3(), formattedMessage, 2, false, true, 
/*  419 */       false, false, true);
/*      */     
/*  421 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  422 */       .getSegment3FareBasis(), formattedMessage, 15, false, true, 
/*  423 */       true, false, true);
/*      */     
/*  425 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  426 */       .getClassOfServiceCodeSegment3(), formattedMessage, 2, false, 
/*  427 */       true, false, false, true);
/*      */     
/*  429 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  430 */       .getFlightNumberSegment3(), formattedMessage, 4, false, true, 
/*  431 */       true, false, true);
/*      */     
/*  433 */     SettlementMessageFormatter.zeroFill(formattedMessage, 3);
/*      */     
/*  435 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  436 */       .getSegment3Fare(), formattedMessage, 12, true, false, false, 
/*  437 */       true, false);
/*      */     
/*  439 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  440 */       .getStopoverIndicator4(), formattedMessage, 1, false, true, 
/*  441 */       false, false, true);
/*      */     
/*  443 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  444 */       .getDepartureLocationCodeSegment4(), formattedMessage, 3, 
/*  445 */       false, true, false, false, true);
/*      */     
/*  447 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  448 */       .getDepartureDateSegment4(), formattedMessage, 8, true, false, 
/*  449 */       false, true, false);
/*      */     
/*  451 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  452 */       .getArrivalLocationCodeSegment4(), formattedMessage, 3, false, 
/*  453 */       true, false, false, true);
/*      */     
/*  455 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  456 */       .getSegmentCarrierCode4(), formattedMessage, 2, false, true, 
/*  457 */       false, false, true);
/*      */     
/*  459 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  460 */       .getSegment4FareBasis(), formattedMessage, 15, false, true, 
/*  461 */       true, false, true);
/*      */     
/*  463 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  464 */       .getClassOfServiceCodeSegment4(), formattedMessage, 2, false, 
/*  465 */       true, false, false, true);
/*      */     
/*  467 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  468 */       .getFlightNumberSegment4(), formattedMessage, 4, false, true, 
/*  469 */       true, false, true);
/*      */     
/*  471 */     SettlementMessageFormatter.zeroFill(formattedMessage, 3);
/*      */     
/*  473 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  474 */       .getSegment4Fare(), formattedMessage, 12, true, false, false, 
/*  475 */       true, false);
/*      */     
/*  477 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  478 */       .getStopoverIndicator5(), formattedMessage, 1, false, true, 
/*  479 */       false, false, true);
/*      */     
/*  481 */     SettlementMessageFormatter.formatValue(airlineIndustryTAABean
/*  482 */       .getExchangedOrOriginalTicketNumber(), formattedMessage, 14, 
/*  483 */       false, true, true, false, true);
/*      */     
/*  485 */     if (PropertyReader.getFileType().equalsIgnoreCase("FIXED")) {
/*  486 */       SettlementMessageFormatter.characterSpacesFill(formattedMessage, 313);
/*      */     }
/*      */     
/*      */ 
/*  490 */     formatedMessageList.add(formattedMessage.toString());
/*      */     
/*  492 */     if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
/*  493 */       LOGGER.info("Formated AirLine Industry Type Record :" + formattedMessage.toString());
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void formatAutoRentalIndustryTypeRecord(AutoRentalIndustryTAABean autoRentalIndustryTAABean, List<String> formatedMessageList)
/*      */     throws IOException
/*      */   {
/*  509 */     StringBuffer formattedMessage = new StringBuffer();
/*      */     
/*  511 */     SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
/*  512 */       .getRecordType(), formattedMessage, 3, false, true, true, 
/*  513 */       false, true);
/*      */     
/*  515 */     SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
/*  516 */       .getRecordNumber(), formattedMessage, 8, true, false, false, 
/*  517 */       true, false);
/*      */     
/*  519 */     SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
/*  520 */       .getTransactionIdentifier(), formattedMessage, 15, true, 
/*  521 */       false, false, true, false);
/*      */     
/*  523 */     SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
/*  524 */       .getFormatCode(), formattedMessage, 2, false, true, false, 
/*  525 */       false, true);
/*      */     
/*  527 */     SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
/*  528 */       .getAddendaTypeCode(), formattedMessage, 2, false, true, 
/*  529 */       false, false, true);
/*      */     
/*  531 */     SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
/*  532 */       .getAutoRentalAgreementNumber(), formattedMessage, 14, false, 
/*  533 */       true, true, false, true);
/*  534 */     SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
/*  535 */       .getAutoRentalPickupLocation(), formattedMessage, 38, false, 
/*  536 */       true, true, false, true);
/*      */     
/*  538 */     SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
/*  539 */       .getAutoRentalPickupCityName(), formattedMessage, 18, false, 
/*  540 */       true, true, false, true);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  547 */     SettlementMessageFormatter.characterSpacesFill(formattedMessage, 3);
/*      */     
/*  549 */     SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
/*  550 */       .getAutoRentalPickupRegionCode(), formattedMessage, 3, false, 
/*  551 */       true, true, false, true);
/*      */     
/*  553 */     SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
/*  554 */       .getAutoRentalPickupCountryCode(), formattedMessage, 3, false, 
/*  555 */       true, true, false, true);
/*  556 */     SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
/*  557 */       .getAutoRentalPickupDate(), formattedMessage, 8, true, false, 
/*  558 */       false, true, false);
/*      */     
/*  560 */     SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
/*  561 */       .getAutoRentalPickupTime(), formattedMessage, 6, false, false, 
/*  562 */       false, true, false);
/*      */     
/*  564 */     SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
/*  565 */       .getAutoRentalReturnCityName(), formattedMessage, 18, false, 
/*  566 */       true, true, false, true);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  572 */     SettlementMessageFormatter.characterSpacesFill(formattedMessage, 3);
/*      */     
/*  574 */     SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
/*  575 */       .getAutoRentalReturnRegionCode(), formattedMessage, 3, false, 
/*  576 */       true, true, false, true);
/*      */     
/*  578 */     SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
/*  579 */       .getAutoRentalReturnCountryCode(), formattedMessage, 3, false, 
/*  580 */       true, true, false, true);
/*  581 */     SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
/*  582 */       .getAutoRentalReturnDate(), formattedMessage, 8, true, false, 
/*  583 */       false, true, false);
/*  584 */     SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
/*  585 */       .getAutoRentalReturnTime(), formattedMessage, 6, true, false, 
/*  586 */       false, true, false);
/*      */     
/*  588 */     SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
/*  589 */       .getAutoRentalRenterName(), formattedMessage, 26, false, true, 
/*  590 */       true, false, true);
/*      */     
/*  592 */     SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
/*  593 */       .getAutoRentalVehicleClassId(), formattedMessage, 4, false, 
/*  594 */       true, false, false, true);
/*  595 */     SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
/*  596 */       .getAutoRentalDistance(), formattedMessage, 5, true, false, 
/*  597 */       false, true, false);
/*      */     
/*  599 */     SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
/*  600 */       .getAutoRentalDistanceUnitOfMeasure(), formattedMessage, 1, 
/*  601 */       false, true, true, false, true);
/*  602 */     SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
/*  603 */       .getAutoRentalAuditAdjustmentIndicator(), formattedMessage, 1, 
/*  604 */       false, true, true, false, true);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  610 */     SettlementMessageFormatter.zeroFill(formattedMessage, 3);
/*      */     
/*  612 */     SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
/*  613 */       .getAutoRentalAuditAdjustmentAmount(), formattedMessage, 12, 
/*  614 */       true, false, false, true, false);
/*      */     
/*  616 */     SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
/*  617 */       .getReturnDropoffLocation(), formattedMessage, 38, false, 
/*  618 */       true, true, false, true);
/*      */     
/*  620 */     SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
/*  621 */       .getVehicleIdentificationNumber(), formattedMessage, 20, false, 
/*  622 */       true, true, false, true);
/*      */     
/*  624 */     SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
/*  625 */       .getDriverIdentificationNumber(), formattedMessage, 20, false, 
/*  626 */       true, true, false, true);
/*      */     
/*  628 */     SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
/*  629 */       .getDriverTaxNumber(), formattedMessage, 20, false, 
/*  630 */       true, true, false, true);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  636 */     if (PropertyReader.getFileType().equalsIgnoreCase("FIXED")) {
/*  637 */       SettlementMessageFormatter.characterSpacesFill(formattedMessage, 386);
/*      */     }
/*  639 */     formatedMessageList.add(formattedMessage.toString());
/*  640 */     if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
/*  641 */       LOGGER.info("Formated AutoRental Industry Type Record :" + formattedMessage.toString());
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void formatCommunicationServicesIndustryTypeRecord(CommunicationServicesIndustryBean communicationServicesIndustryBean, List<String> formatedMessageList)
/*      */     throws IOException
/*      */   {
/*  658 */     StringBuffer formattedMessage = new StringBuffer();
/*      */     
/*  660 */     SettlementMessageFormatter.formatValue(
/*  661 */       communicationServicesIndustryBean.getRecordType(), 
/*  662 */       formattedMessage, 3, false, true, true, false, true);
/*  663 */     SettlementMessageFormatter.formatValue(
/*  664 */       communicationServicesIndustryBean.getRecordNumber(), 
/*  665 */       formattedMessage, 8, true, false, false, true, false);
/*      */     
/*  667 */     SettlementMessageFormatter.formatValue(
/*  668 */       communicationServicesIndustryBean.getTransactionIdentifier(), 
/*  669 */       formattedMessage, 15, true, false, false, true, false);
/*  670 */     SettlementMessageFormatter.formatValue(
/*  671 */       communicationServicesIndustryBean.getFormatCode(), 
/*  672 */       formattedMessage, 2, false, true, false, false, true);
/*  673 */     SettlementMessageFormatter.formatValue(
/*  674 */       communicationServicesIndustryBean.getAddendaTypeCode(), 
/*  675 */       formattedMessage, 2, false, true, false, false, true);
/*  676 */     SettlementMessageFormatter.formatValue(
/*  677 */       communicationServicesIndustryBean.getCallDate(), 
/*  678 */       formattedMessage, 8, true, false, false, true, false);
/*  679 */     SettlementMessageFormatter.formatValue(
/*  680 */       communicationServicesIndustryBean.getCallTime(), 
/*  681 */       formattedMessage, 6, true, false, false, true, false);
/*  682 */     SettlementMessageFormatter.formatValue(
/*  683 */       communicationServicesIndustryBean.getCallDurationTime(), 
/*  684 */       formattedMessage, 6, true, false, false, true, false);
/*  685 */     SettlementMessageFormatter.formatValue(
/*  686 */       communicationServicesIndustryBean.getCallFromLocationName(), 
/*  687 */       formattedMessage, 18, false, true, true, false, true);
/*      */     
/*  689 */     SettlementMessageFormatter.formatValue(
/*  690 */       communicationServicesIndustryBean.getCallFromRegionCode(), 
/*  691 */       formattedMessage, 3, false, true, true, false, true);
/*      */     
/*  693 */     SettlementMessageFormatter.formatValue(
/*  694 */       communicationServicesIndustryBean.getCallFromCountryCode(), 
/*  695 */       formattedMessage, 3, false, true, true, false, true);
/*  696 */     SettlementMessageFormatter.formatValue(
/*  697 */       communicationServicesIndustryBean.getCallFromPhoneNumber(), 
/*  698 */       formattedMessage, 16, false, true, true, false, true);
/*  699 */     SettlementMessageFormatter.formatValue(
/*  700 */       communicationServicesIndustryBean.getCallToLocationName(), 
/*  701 */       formattedMessage, 18, false, true, true, false, true);
/*      */     
/*  703 */     SettlementMessageFormatter.formatValue(
/*  704 */       communicationServicesIndustryBean.getCallToRegionCode(), 
/*  705 */       formattedMessage, 3, false, true, true, false, true);
/*      */     
/*  707 */     SettlementMessageFormatter.formatValue(
/*  708 */       communicationServicesIndustryBean.getCallToCountryCode(), 
/*  709 */       formattedMessage, 3, false, true, true, false, true);
/*  710 */     SettlementMessageFormatter.formatValue(
/*  711 */       communicationServicesIndustryBean.getCallToPhoneNumber(), 
/*  712 */       formattedMessage, 16, false, true, true, false, true);
/*      */     
/*  714 */     SettlementMessageFormatter.formatValue(
/*  715 */       communicationServicesIndustryBean.getPhoneCardId(), 
/*  716 */       formattedMessage, 8, false, true, true, false, true);
/*  717 */     SettlementMessageFormatter.formatValue(
/*  718 */       communicationServicesIndustryBean.getServiceDescription(), 
/*  719 */       formattedMessage, 20, false, true, true, false, true);
/*      */     
/*  721 */     SettlementMessageFormatter.formatValue(
/*  722 */       communicationServicesIndustryBean.getBillingPeriod(), 
/*  723 */       formattedMessage, 18, false, true, true, false, true);
/*      */     
/*  725 */     SettlementMessageFormatter.formatValue(
/*  726 */       communicationServicesIndustryBean
/*  727 */       .getCommunicationsCallTypeCode(), formattedMessage, 5, 
/*  728 */       false, true, true, false, true);
/*  729 */     SettlementMessageFormatter.formatValue(
/*  730 */       communicationServicesIndustryBean.getCommunicationsRateClass(), 
/*  731 */       formattedMessage, 1, false, true, true, false, true);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  737 */     if (PropertyReader.getFileType().equalsIgnoreCase("FIXED")) {
/*  738 */       SettlementMessageFormatter.characterSpacesFill(formattedMessage, 518);
/*      */     }
/*  740 */     formatedMessageList.add(formattedMessage.toString());
/*  741 */     if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
/*  742 */       LOGGER.info("Formated Communication Services Industry Type Record :" + formattedMessage.toString());
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void formatEntertainmentTicketingIndustryTypeRecord(EntertainmentTicketingIndustryTAABean entertainmentTicketingIndustryTAABean, List<String> formatedMessageList)
/*      */     throws IOException
/*      */   {
/*  759 */     StringBuffer formattedMessage = new StringBuffer();
/*      */     
/*  761 */     SettlementMessageFormatter.formatValue(
/*  762 */       entertainmentTicketingIndustryTAABean.getRecordType(), 
/*  763 */       formattedMessage, 3, false, true, true, false, true);
/*  764 */     SettlementMessageFormatter.formatValue(
/*  765 */       entertainmentTicketingIndustryTAABean.getRecordNumber(), 
/*  766 */       formattedMessage, 8, true, false, false, true, false);
/*      */     
/*  768 */     SettlementMessageFormatter.formatValue(
/*  769 */       entertainmentTicketingIndustryTAABean
/*  770 */       .getTransactionIdentifier(), formattedMessage, 15, 
/*  771 */       false, false, false, true, false);
/*      */     
/*  773 */     SettlementMessageFormatter.formatValue(
/*  774 */       entertainmentTicketingIndustryTAABean.getFormatCode(), 
/*  775 */       formattedMessage, 2, false, true, false, false, true);
/*  776 */     SettlementMessageFormatter.formatValue(
/*  777 */       entertainmentTicketingIndustryTAABean.getAddendaTypeCode(), 
/*  778 */       formattedMessage, 2, false, true, false, false, true);
/*  779 */     SettlementMessageFormatter.formatValue(
/*  780 */       entertainmentTicketingIndustryTAABean.getEventName(), 
/*  781 */       formattedMessage, 30, false, true, true, false, true);
/*      */     
/*  783 */     SettlementMessageFormatter.formatValue(
/*  784 */       entertainmentTicketingIndustryTAABean.getEventDate(), 
/*  785 */       formattedMessage, 8, true, false, false, true, false);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  791 */     SettlementMessageFormatter.zeroFill(formattedMessage, 3);
/*      */     
/*  793 */     SettlementMessageFormatter.formatValue(
/*  794 */       entertainmentTicketingIndustryTAABean
/*  795 */       .getEventIndividualTicketPriceAmount(), 
/*  796 */       formattedMessage, 12, true, false, false, true, false);
/*      */     
/*  798 */     SettlementMessageFormatter.formatValue(
/*  799 */       entertainmentTicketingIndustryTAABean.getEventTicketQuantity(), 
/*  800 */       formattedMessage, 4, true, false, false, true, false);
/*      */     
/*  802 */     SettlementMessageFormatter.formatValue(
/*  803 */       entertainmentTicketingIndustryTAABean.getEventLocation(), 
/*  804 */       formattedMessage, 40, false, true, true, false, true);
/*      */     
/*  806 */     SettlementMessageFormatter.formatValue(
/*  807 */       entertainmentTicketingIndustryTAABean.getEventRegionCode(), 
/*  808 */       formattedMessage, 3, false, true, true, false, true);
/*  809 */     SettlementMessageFormatter.formatValue(
/*  810 */       entertainmentTicketingIndustryTAABean.getEventCountryCode(), 
/*  811 */       formattedMessage, 3, false, true, true, false, true);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*  816 */     if (PropertyReader.getFileType().equalsIgnoreCase("FIXED")) {
/*  817 */       SettlementMessageFormatter.characterSpacesFill(formattedMessage, 567);
/*      */     }
/*  819 */     formatedMessageList.add(formattedMessage.toString());
/*  820 */     if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
/*  821 */       LOGGER.info("Formated Entertainment Ticketing Industry Type Record :" + formattedMessage.toString());
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void formatInsuranceIndustryTypeRecord(InsuranceIndustryTAABean insuranceIndustryTAABean, List<String> formatedMessageList)
/*      */     throws IOException
/*      */   {
/*  837 */     StringBuffer formattedMessage = new StringBuffer();
/*      */     
/*  839 */     SettlementMessageFormatter.formatValue(insuranceIndustryTAABean
/*  840 */       .getRecordType(), formattedMessage, 3, false, true, true, 
/*  841 */       false, true);
/*      */     
/*  843 */     SettlementMessageFormatter.formatValue(insuranceIndustryTAABean
/*  844 */       .getRecordNumber(), formattedMessage, 8, true, false, false, 
/*  845 */       true, false);
/*      */     
/*  847 */     SettlementMessageFormatter.formatValue(insuranceIndustryTAABean
/*  848 */       .getTransactionIdentifier(), formattedMessage, 15, true, 
/*  849 */       false, false, true, false);
/*      */     
/*  851 */     SettlementMessageFormatter.formatValue(insuranceIndustryTAABean
/*  852 */       .getFormatCode(), formattedMessage, 2, false, true, false, 
/*  853 */       false, true);
/*      */     
/*  855 */     SettlementMessageFormatter.formatValue(insuranceIndustryTAABean
/*  856 */       .getAddendaTypeCode(), formattedMessage, 2, false, true, 
/*  857 */       false, false, true);
/*      */     
/*  859 */     SettlementMessageFormatter.formatValue(insuranceIndustryTAABean
/*  860 */       .getInsurancePolicyNumber(), formattedMessage, 23, false, true, 
/*  861 */       true, false, true);
/*      */     
/*  863 */     SettlementMessageFormatter.formatValue(insuranceIndustryTAABean
/*  864 */       .getInsuranceCoverageStartDate(), formattedMessage, 8, true, 
/*  865 */       false, false, true, false);
/*      */     
/*  867 */     SettlementMessageFormatter.formatValue(insuranceIndustryTAABean
/*  868 */       .getInsuranceCoverageEndDate(), formattedMessage, 8, true, 
/*  869 */       false, false, true, false);
/*      */     
/*  871 */     SettlementMessageFormatter.formatValue(insuranceIndustryTAABean
/*  872 */       .getInsurancePolicyPremiumFrequency(), formattedMessage, 7, 
/*  873 */       false, true, true, false, true);
/*      */     
/*  875 */     SettlementMessageFormatter.formatValue(insuranceIndustryTAABean
/*  876 */       .getAdditionalInsurancePolicyNumber(), formattedMessage, 23, 
/*  877 */       false, true, true, false, true);
/*      */     
/*  879 */     SettlementMessageFormatter.formatValue(insuranceIndustryTAABean
/*  880 */       .getTypeOfPolicy(), formattedMessage, 25, false, true, true, 
/*  881 */       false, true);
/*      */     
/*  883 */     SettlementMessageFormatter.formatValue(insuranceIndustryTAABean
/*  884 */       .getNameOfInsured(), formattedMessage, 30, false, true, true, 
/*  885 */       false, true);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  892 */     if (PropertyReader.getFileType().equalsIgnoreCase("FIXED")) {
/*  893 */       SettlementMessageFormatter.characterSpacesFill(formattedMessage, 546);
/*      */     }
/*  895 */     formatedMessageList.add(formattedMessage.toString());
/*  896 */     if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
/*  897 */       LOGGER.info("Formated Insurance Industry Type Record :" + formattedMessage.toString());
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void formatRailIndustryTypeRecord(RailIndustryTAABean railIndustryTAABean, List<String> formatedMessageList)
/*      */     throws IOException
/*      */   {
/*  914 */     StringBuffer formattedMessage = new StringBuffer();
/*      */     
/*  916 */     SettlementMessageFormatter.formatValue(railIndustryTAABean
/*  917 */       .getRecordType(), formattedMessage, 3, false, true, true, 
/*  918 */       false, true);
/*      */     
/*  920 */     SettlementMessageFormatter.formatValue(railIndustryTAABean
/*  921 */       .getRecordNumber(), formattedMessage, 8, true, false, false, 
/*  922 */       true, false);
/*      */     
/*  924 */     SettlementMessageFormatter.formatValue(railIndustryTAABean
/*  925 */       .getTransactionIdentifier(), formattedMessage, 15, true, 
/*  926 */       false, false, true, false);
/*      */     
/*  928 */     SettlementMessageFormatter.formatValue(railIndustryTAABean
/*  929 */       .getFormatCode(), formattedMessage, 2, false, true, false, 
/*  930 */       false, true);
/*      */     
/*  932 */     SettlementMessageFormatter.formatValue(railIndustryTAABean
/*  933 */       .getAddendaTypeCode(), formattedMessage, 2, false, true, 
/*  934 */       false, false, true);
/*      */     
/*  936 */     SettlementMessageFormatter.formatValue(railIndustryTAABean
/*  937 */       .getTransactionType(), formattedMessage, 3, false, true, true, 
/*  938 */       false, true);
/*  939 */     SettlementMessageFormatter.formatValue(railIndustryTAABean
/*  940 */       .getTicketNumber(), formattedMessage, 14, false, true, true, 
/*  941 */       false, true);
/*      */     
/*  943 */     SettlementMessageFormatter.formatValue(railIndustryTAABean
/*  944 */       .getPassengerName(), formattedMessage, 25, false, true, true, 
/*  945 */       false, true);
/*      */     
/*  947 */     SettlementMessageFormatter.formatValue(railIndustryTAABean
/*  948 */       .getIataCarrierCode(), formattedMessage, 3, false, true, true, 
/*  949 */       false, true);
/*      */     
/*  951 */     SettlementMessageFormatter.formatValue(railIndustryTAABean
/*  952 */       .getTicketIssuerName(), formattedMessage, 32, false, true, 
/*  953 */       true, false, true);
/*      */     
/*  955 */     SettlementMessageFormatter.formatValue(railIndustryTAABean
/*  956 */       .getTicketIssueCity(), formattedMessage, 18, false, true, true, 
/*  957 */       false, true);
/*  958 */     SettlementMessageFormatter.formatValue(railIndustryTAABean
/*  959 */       .getDepartureStation1(), formattedMessage, 3, false, true, 
/*  960 */       false, false, true);
/*  961 */     SettlementMessageFormatter.formatValue(railIndustryTAABean
/*  962 */       .getDepartureDate1(), formattedMessage, 8, true, false, false, 
/*  963 */       true, false);
/*  964 */     SettlementMessageFormatter.formatValue(railIndustryTAABean
/*  965 */       .getArrivalStation1(), formattedMessage, 3, false, true, 
/*  966 */       false, false, true);
/*      */     
/*  968 */     SettlementMessageFormatter.formatValue(railIndustryTAABean
/*  969 */       .getDepartureStation2(), formattedMessage, 3, false, true, 
/*  970 */       false, false, true);
/*  971 */     SettlementMessageFormatter.formatValue(railIndustryTAABean
/*  972 */       .getDepartureDate2(), formattedMessage, 8, true, false, false, 
/*  973 */       true, false);
/*  974 */     SettlementMessageFormatter.formatValue(railIndustryTAABean
/*  975 */       .getArrivalStation2(), formattedMessage, 3, false, true, 
/*  976 */       false, false, true);
/*      */     
/*  978 */     SettlementMessageFormatter.formatValue(railIndustryTAABean
/*  979 */       .getDepartureStation3(), formattedMessage, 3, false, true, 
/*  980 */       false, false, true);
/*  981 */     SettlementMessageFormatter.formatValue(railIndustryTAABean
/*  982 */       .getDepartureDate3(), formattedMessage, 8, true, false, false, 
/*  983 */       true, false);
/*  984 */     SettlementMessageFormatter.formatValue(railIndustryTAABean
/*  985 */       .getArrivalStation3(), formattedMessage, 3, false, true, 
/*  986 */       false, false, true);
/*      */     
/*  988 */     SettlementMessageFormatter.formatValue(railIndustryTAABean
/*  989 */       .getDepartureStation4(), formattedMessage, 3, false, true, 
/*  990 */       false, false, true);
/*  991 */     SettlementMessageFormatter.formatValue(railIndustryTAABean
/*  992 */       .getDepartureDate4(), formattedMessage, 8, true, false, false, 
/*  993 */       true, false);
/*  994 */     SettlementMessageFormatter.formatValue(railIndustryTAABean
/*  995 */       .getArrivalStation4(), formattedMessage, 3, false, true, 
/*  996 */       false, false, true);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1002 */     if (PropertyReader.getFileType().equalsIgnoreCase("FIXED")) {
/* 1003 */       SettlementMessageFormatter.characterSpacesFill(formattedMessage, 519);
/*      */     }
/* 1005 */     formatedMessageList.add(formattedMessage.toString());
/* 1006 */     if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
/* 1007 */       LOGGER.info("Formated Rail Industry Type Record :" + formattedMessage.toString());
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void formatRetailIndustryTypeRecord(RetailIndustryTAABean retailIndustryTAABean, List<String> formatedMessageList)
/*      */     throws IOException
/*      */   {
/* 1023 */     StringBuffer formattedMessage = new StringBuffer();
/*      */     
/* 1025 */     SettlementMessageFormatter.formatValue(retailIndustryTAABean
/* 1026 */       .getRecordType(), formattedMessage, 3, false, true, true, 
/* 1027 */       false, true);
/*      */     
/* 1029 */     SettlementMessageFormatter.formatValue(retailIndustryTAABean
/* 1030 */       .getRecordNumber(), formattedMessage, 8, true, false, false, 
/* 1031 */       true, false);
/*      */     
/* 1033 */     SettlementMessageFormatter.formatValue(retailIndustryTAABean
/* 1034 */       .getTransactionIdentifier(), formattedMessage, 15, true, 
/* 1035 */       false, false, true, false);
/*      */     
/* 1037 */     SettlementMessageFormatter.formatValue(retailIndustryTAABean
/* 1038 */       .getFormatCode(), formattedMessage, 2, false, true, false, 
/* 1039 */       false, true);
/*      */     
/* 1041 */     SettlementMessageFormatter.formatValue(retailIndustryTAABean
/* 1042 */       .getAddendaTypeCode(), formattedMessage, 2, false, true, 
/* 1043 */       false, false, true);
/* 1044 */     SettlementMessageFormatter.formatValue(retailIndustryTAABean
/* 1045 */       .getRetailDepartmentName(), formattedMessage, 40, false, true, 
/* 1046 */       true, false, true);
/*      */     
/* 1048 */     SettlementMessageFormatter.formatValue(retailIndustryTAABean
/* 1049 */       .getRetailItemDescription1(), formattedMessage, 19, false, 
/* 1050 */       true, true, false, true);
/* 1051 */     SettlementMessageFormatter.formatValue(retailIndustryTAABean
/* 1052 */       .getRetailItemQuantity1(), formattedMessage, 3, true, false, 
/* 1053 */       false, true, false);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1059 */     SettlementMessageFormatter.zeroFill(formattedMessage, 3);
/*      */     
/* 1061 */     SettlementMessageFormatter.formatValue(retailIndustryTAABean
/* 1062 */       .getRetailItemAmount1(), formattedMessage, 12, true, false, 
/* 1063 */       false, true, false);
/*      */     
/* 1065 */     SettlementMessageFormatter.formatValue(retailIndustryTAABean
/* 1066 */       .getRetailItemDescription2(), formattedMessage, 19, false, 
/* 1067 */       true, true, false, true);
/* 1068 */     SettlementMessageFormatter.formatValue(retailIndustryTAABean
/* 1069 */       .getRetailItemQuantity2(), formattedMessage, 3, true, false, 
/* 1070 */       false, true, false);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1076 */     SettlementMessageFormatter.zeroFill(formattedMessage, 3);
/*      */     
/* 1078 */     SettlementMessageFormatter.formatValue(retailIndustryTAABean
/* 1079 */       .getRetailItemAmount2(), formattedMessage, 12, true, false, 
/* 1080 */       false, true, false);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1087 */     SettlementMessageFormatter.formatValue(retailIndustryTAABean
/* 1088 */       .getRetailItemDescription3(), formattedMessage, 19, false, 
/* 1089 */       true, true, false, true);
/* 1090 */     SettlementMessageFormatter.formatValue(retailIndustryTAABean
/* 1091 */       .getRetailItemQuantity3(), formattedMessage, 3, true, false, 
/* 1092 */       false, true, false);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1098 */     SettlementMessageFormatter.zeroFill(formattedMessage, 3);
/*      */     
/* 1100 */     SettlementMessageFormatter.formatValue(retailIndustryTAABean
/* 1101 */       .getRetailItemAmount3(), formattedMessage, 12, true, false, 
/* 1102 */       false, true, false);
/*      */     
/* 1104 */     SettlementMessageFormatter.formatValue(retailIndustryTAABean
/* 1105 */       .getRetailItemDescription4(), formattedMessage, 19, false, 
/* 1106 */       true, true, false, true);
/* 1107 */     SettlementMessageFormatter.formatValue(retailIndustryTAABean
/* 1108 */       .getRetailItemQuantity4(), formattedMessage, 3, true, false, 
/* 1109 */       false, true, false);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1115 */     SettlementMessageFormatter.zeroFill(formattedMessage, 3);
/*      */     
/* 1117 */     SettlementMessageFormatter.formatValue(retailIndustryTAABean
/* 1118 */       .getRetailItemAmount4(), formattedMessage, 12, true, false, 
/* 1119 */       false, true, false);
/*      */     
/* 1121 */     SettlementMessageFormatter.formatValue(retailIndustryTAABean
/* 1122 */       .getRetailItemDescription5(), formattedMessage, 19, false, 
/* 1123 */       true, true, false, true);
/* 1124 */     SettlementMessageFormatter.formatValue(retailIndustryTAABean
/* 1125 */       .getRetailItemQuantity5(), formattedMessage, 3, true, false, 
/* 1126 */       false, true, false);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1132 */     SettlementMessageFormatter.zeroFill(formattedMessage, 3);
/*      */     
/* 1134 */     SettlementMessageFormatter.formatValue(retailIndustryTAABean
/* 1135 */       .getRetailItemAmount5(), formattedMessage, 12, true, false, 
/* 1136 */       false, true, false);
/*      */     
/* 1138 */     SettlementMessageFormatter.formatValue(retailIndustryTAABean
/* 1139 */       .getRetailItemDescription6(), formattedMessage, 19, false, 
/* 1140 */       true, true, false, true);
/* 1141 */     SettlementMessageFormatter.formatValue(retailIndustryTAABean
/* 1142 */       .getRetailItemQuantity6(), formattedMessage, 3, true, false, 
/* 1143 */       false, true, false);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1149 */     SettlementMessageFormatter.zeroFill(formattedMessage, 3);
/*      */     
/* 1151 */     SettlementMessageFormatter.formatValue(retailIndustryTAABean
/* 1152 */       .getRetailItemAmount6(), formattedMessage, 12, true, false, 
/* 1153 */       false, true, false);
/* 1154 */     if (PropertyReader.getFileType().equalsIgnoreCase("FIXED")) {
/* 1155 */       SettlementMessageFormatter.characterSpacesFill(formattedMessage, 408);
/*      */     }
/* 1157 */     formatedMessageList.add(formattedMessage.toString());
/* 1158 */     if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
/* 1159 */       LOGGER.info("Formated Rrtail Industry Type Record :" + formattedMessage.toString());
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void formatTravelCruiseIndustryTypeRecord(TravelCruiseIndustryTAABean travelCruiseIndustryTAABean, List<String> formatedMessageList)
/*      */     throws IOException
/*      */   {
/* 1176 */     StringBuffer formattedMessage = new StringBuffer();
/*      */     
/* 1178 */     SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
/* 1179 */       .getRecordType(), formattedMessage, 3, false, true, true, 
/* 1180 */       false, true);
/*      */     
/* 1182 */     SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
/* 1183 */       .getRecordNumber(), formattedMessage, 8, true, false, false, 
/* 1184 */       true, false);
/*      */     
/* 1186 */     SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
/* 1187 */       .getTransactionIdentifier(), formattedMessage, 15, true, 
/* 1188 */       false, false, true, false);
/*      */     
/* 1190 */     SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
/* 1191 */       .getFormatCode(), formattedMessage, 2, false, true, false, 
/* 1192 */       false, true);
/*      */     
/* 1194 */     SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
/* 1195 */       .getAddendaTypeCode(), formattedMessage, 2, false, true, 
/* 1196 */       false, false, true);
/*      */     
/* 1198 */     SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
/* 1199 */       .getIataCarrierCode(), formattedMessage, 3, false, true, true, 
/* 1200 */       false, true);
/*      */     
/* 1202 */     SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
/* 1203 */       .getIataAgencyNumber(), formattedMessage, 8, false, true, true, 
/* 1204 */       false, true);
/*      */     
/* 1206 */     SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
/* 1207 */       .getTravelPackageIndicator(), formattedMessage, 1, false, 
/* 1208 */       true, false, false, true);
/* 1209 */     SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
/* 1210 */       .getPassengerName(), formattedMessage, 25, false, true, true, 
/* 1211 */       false, true);
/*      */     
/* 1213 */     SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
/* 1214 */       .getTravelTicketNumber(), formattedMessage, 14, false, true, 
/* 1215 */       true, false, true);
/*      */     
/* 1217 */     SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
/* 1218 */       .getTravelDepartureDateSegment1(), formattedMessage, 8, true, 
/* 1219 */       false, false, true, false);
/*      */     
/* 1221 */     SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
/* 1222 */       .getTravelDestinationCodeSegment1(), formattedMessage, 3, 
/* 1223 */       false, true, false, false, true);
/* 1224 */     SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
/* 1225 */       .getTravelDepartureAirportSegment1(), formattedMessage, 3, 
/* 1226 */       false, true, false, false, true);
/* 1227 */     SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
/* 1228 */       .getAirCarrierCodeSegment1(), formattedMessage, 2, false, 
/* 1229 */       true, false, false, true);
/* 1230 */     SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
/* 1231 */       .getFlightNumberSegment1(), formattedMessage, 4, false, true, 
/* 1232 */       false, false, true);
/*      */     
/* 1234 */     SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
/* 1235 */       .getClassCodeSegment1(), formattedMessage, 1, false, true, 
/* 1236 */       false, false, true);
/*      */     
/* 1238 */     SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
/* 1239 */       .getTravelDepartureDateSegment2(), formattedMessage, 8, true, 
/* 1240 */       false, false, true, false);
/*      */     
/* 1242 */     SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
/* 1243 */       .getTravelDestinationCodeSegment2(), formattedMessage, 3, 
/* 1244 */       false, true, false, false, true);
/* 1245 */     SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
/* 1246 */       .getTravelDepartureAirportSegment2(), formattedMessage, 3, 
/* 1247 */       false, true, false, false, true);
/* 1248 */     SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
/* 1249 */       .getAirCarrierCodeSegment2(), formattedMessage, 2, false, 
/* 1250 */       true, false, false, true);
/* 1251 */     SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
/* 1252 */       .getFlightNumberSegment2(), formattedMessage, 4, false, true, 
/* 1253 */       false, false, true);
/*      */     
/* 1255 */     SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
/* 1256 */       .getClassCodeSegment2(), formattedMessage, 1, false, true, 
/* 1257 */       false, false, true);
/*      */     
/* 1259 */     SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
/* 1260 */       .getTravelDepartureDateSegment3(), formattedMessage, 8, true, 
/* 1261 */       false, false, true, false);
/*      */     
/* 1263 */     SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
/* 1264 */       .getTravelDestinationCodeSegment3(), formattedMessage, 3, 
/* 1265 */       false, true, false, false, true);
/* 1266 */     SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
/* 1267 */       .getTravelDepartureAirportSegment3(), formattedMessage, 3, 
/* 1268 */       false, true, false, false, true);
/* 1269 */     SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
/* 1270 */       .getAirCarrierCodeSegment3(), formattedMessage, 2, false, 
/* 1271 */       true, false, false, true);
/* 1272 */     SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
/* 1273 */       .getFlightNumberSegment3(), formattedMessage, 4, false, true, 
/* 1274 */       false, false, true);
/*      */     
/* 1276 */     SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
/* 1277 */       .getClassCodeSegment3(), formattedMessage, 1, false, true, 
/* 1278 */       false, false, true);
/*      */     
/* 1280 */     SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
/* 1281 */       .getTravelDepartureDateSegment4(), formattedMessage, 8, true, 
/* 1282 */       false, false, true, false);
/*      */     
/* 1284 */     SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
/* 1285 */       .getTravelDestinationCodeSegment4(), formattedMessage, 3, 
/* 1286 */       false, true, false, false, true);
/* 1287 */     SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
/* 1288 */       .getTravelDepartureAirportSegment4(), formattedMessage, 3, 
/* 1289 */       false, true, false, false, true);
/* 1290 */     SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
/* 1291 */       .getAirCarrierCodeSegment4(), formattedMessage, 2, false, 
/* 1292 */       true, false, false, true);
/* 1293 */     SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
/* 1294 */       .getFlightNumberSegment4(), formattedMessage, 4, false, true, 
/* 1295 */       false, false, true);
/*      */     
/* 1297 */     SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
/* 1298 */       .getClassCodeSegment4(), formattedMessage, 1, false, true, 
/* 1299 */       false, false, true);
/*      */     
/* 1301 */     SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
/* 1302 */       .getLodgingCheckInDate(), formattedMessage, 8, true, false, 
/* 1303 */       false, true, false);
/*      */     
/* 1305 */     SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
/* 1306 */       .getLodgingCheckOutDate(), formattedMessage, 8, true, false, 
/* 1307 */       false, true, false);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1313 */     SettlementMessageFormatter.zeroFill(formattedMessage, 3);
/*      */     
/* 1315 */     SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
/* 1316 */       .getLodgingRoomRate1(), formattedMessage, 12, true, false, 
/* 1317 */       false, true, false);
/*      */     
/* 1319 */     SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
/* 1320 */       .getNumberOfNightsAtRoomRate1(), formattedMessage, 2, true, 
/* 1321 */       false, false, true, false);
/*      */     
/* 1323 */     SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
/* 1324 */       .getLodgingName(), formattedMessage, 20, false, true, true, 
/* 1325 */       false, true);
/*      */     
/* 1327 */     SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
/* 1328 */       .getLodgingRegionCode(), formattedMessage, 3, false, true, 
/* 1329 */       true, false, true);
/*      */     
/* 1331 */     SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
/* 1332 */       .getLodgingCountryCode(), formattedMessage, 3, false, false, 
/* 1333 */       true, false, true);
/* 1334 */     SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
/* 1335 */       .getLodgingCityName(), formattedMessage, 18, false, true, true, 
/* 1336 */       false, true);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1342 */     if (PropertyReader.getFileType().equalsIgnoreCase("FIXED")) {
/* 1343 */       SettlementMessageFormatter.characterSpacesFill(formattedMessage, 458);
/*      */     }
/* 1345 */     formatedMessageList.add(formattedMessage.toString());
/* 1346 */     if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
/* 1347 */       LOGGER.info("Formated Travel Cruise Industry Type Record :" + formattedMessage.toString());
/*      */     }
/*      */   }
/*      */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\formats\IndustryTAARecordFormatter.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */