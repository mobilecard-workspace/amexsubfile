/*     */ package com.americanexpress.ips.gfsg.formats;
/*     */ 
/*     */ import com.americanexpress.ips.gfsg.beans.common.taa.NonIndustrySpecificTAABean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.taa.nonindustrytype.CPSLevel2Bean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.taa.nonindustrytype.DeferredPaymentPlanBean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.taa.nonindustrytype.EMVBean;
/*     */ import com.americanexpress.ips.gfsg.connection.PropertyReader;
/*     */ import java.io.IOException;
/*     */ import java.util.List;
/*     */ import org.apache.log4j.Logger;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public final class NonIndustryTAARecordFormatter
/*     */ {
/*  25 */   private static final Logger LOGGER = Logger.getLogger(NonIndustryTAARecordFormatter.class);
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void formatNonIndustrySpecificTypeTAARecord(NonIndustrySpecificTAABean nonIndustrySpecificTAABean, List<String> formatedMessageList)
/*     */     throws IOException
/*     */   {
/*  40 */     if ((nonIndustrySpecificTAABean instanceof CPSLevel2Bean)) {
/*  41 */       CPSLevel2Bean cpsLevel2Bean = (CPSLevel2Bean)nonIndustrySpecificTAABean;
/*  42 */       formatCPSLevel2Record(cpsLevel2Bean, formatedMessageList);
/*     */ 
/*     */     }
/*  45 */     else if ((nonIndustrySpecificTAABean instanceof DeferredPaymentPlanBean)) {
/*  46 */       DeferredPaymentPlanBean deferredPaymentPlanBean = (DeferredPaymentPlanBean)nonIndustrySpecificTAABean;
/*  47 */       formatDeferredPaymentPlanRecord(deferredPaymentPlanBean, formatedMessageList);
/*     */ 
/*     */     }
/*  50 */     else if ((nonIndustrySpecificTAABean instanceof EMVBean)) {
/*  51 */       EMVBean emvBean = (EMVBean)nonIndustrySpecificTAABean;
/*  52 */       formatEMVRecord(emvBean, formatedMessageList);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void formatEMVRecord(EMVBean emvBean, List<String> formatedMessageList)
/*     */     throws IOException
/*     */   {
/*  69 */     StringBuffer formattedMessage = new StringBuffer();
/*     */     
/*  71 */     SettlementMessageFormatter.formatValue(emvBean.getRecordType(), 
/*  72 */       formattedMessage, 3, false, true, true, false, true);
/*  73 */     SettlementMessageFormatter.formatValue(emvBean.getRecordNumber(), formattedMessage, 8, true, false, false, true, false);
/*     */     
/*  75 */     SettlementMessageFormatter.formatValue(emvBean.getTransactionIdentifier(), 
/*  76 */       formattedMessage, 15, true, false, false, true, false);
/*     */     
/*  78 */     SettlementMessageFormatter.formatValue(emvBean.getEMVFormatType(), 
/*  79 */       formattedMessage, 2, true, false, false, true, false);
/*  80 */     SettlementMessageFormatter.formatValue(emvBean.getAddendaTypeCode(), 
/*  81 */       formattedMessage, 2, false, true, false, false, true);
/*     */     
/*  83 */     SettlementMessageFormatter.formatValue(emvBean.getICCSystemRelatedData(), formattedMessage, 256, false, true, false, false, true);
/*     */     
/*     */ 
/*  86 */     if (PropertyReader.getFileType().equalsIgnoreCase("FIXED")) {
/*  87 */       SettlementMessageFormatter.characterSpacesFill(formattedMessage, 414);
/*     */     }
/*     */     
/*  90 */     formatedMessageList.add(formattedMessage.toString());
/*     */     
/*  92 */     if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
/*  93 */       LOGGER.info("Formated EMV NonIndustry Type Record :" + formattedMessage.toString());
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void formatCPSLevel2Record(CPSLevel2Bean cPSLevel2Bean, List<String> formatedMessageList)
/*     */     throws IOException
/*     */   {
/* 109 */     StringBuffer formattedMessage = new StringBuffer();
/*     */     
/* 111 */     SettlementMessageFormatter.formatValue(cPSLevel2Bean.getRecordType(), 
/* 112 */       formattedMessage, 3, false, true, true, false, true);
/* 113 */     SettlementMessageFormatter.formatValue(cPSLevel2Bean.getRecordNumber(), formattedMessage, 8, true, false, false, true, false);
/*     */     
/* 115 */     SettlementMessageFormatter.formatValue(cPSLevel2Bean.getTransactionIdentifier(), 
/* 116 */       formattedMessage, 15, true, false, false, true, false);
/*     */     
/*     */ 
/*     */ 
/* 120 */     SettlementMessageFormatter.zeroFill(formattedMessage, 2);
/*     */     
/*     */ 
/*     */ 
/* 124 */     SettlementMessageFormatter.formatValue(cPSLevel2Bean.getAddendaTypeCode(), 
/* 125 */       formattedMessage, 2, false, true, false, false, true);
/*     */     
/*     */ 
/* 128 */     SettlementMessageFormatter.formatValue(cPSLevel2Bean.getRequesterName(), formattedMessage, 38, false, true, true, false, true);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 133 */     SettlementMessageFormatter.characterSpacesFill(formattedMessage, 2);
/*     */     
/*     */ 
/* 136 */     SettlementMessageFormatter.formatValue(cPSLevel2Bean.getChargeDescription1(), formattedMessage, 40, false, true, true, false, true);
/*     */     
/*     */ 
/*     */ 
/* 140 */     SettlementMessageFormatter.formatValue(cPSLevel2Bean.getChargeItemQuantity1(), formattedMessage, 3, true, false, false, true, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 145 */     SettlementMessageFormatter.zeroFill(formattedMessage, 3);
/*     */     
/* 147 */     SettlementMessageFormatter.formatValue(cPSLevel2Bean.getChargeItemAmount1(), formattedMessage, 12, true, false, false, true, false);
/*     */     
/*     */ 
/*     */ 
/* 151 */     SettlementMessageFormatter.formatValue(cPSLevel2Bean.getChargeDescription2(), formattedMessage, 40, false, true, true, false, true);
/*     */     
/*     */ 
/*     */ 
/* 155 */     SettlementMessageFormatter.formatValue(cPSLevel2Bean.getChargeItemQuantity2(), formattedMessage, 3, true, false, false, true, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 160 */     SettlementMessageFormatter.zeroFill(formattedMessage, 3);
/*     */     
/*     */ 
/* 163 */     SettlementMessageFormatter.formatValue(cPSLevel2Bean.getChargeItemAmount2(), formattedMessage, 12, true, false, false, true, false);
/*     */     
/*     */ 
/*     */ 
/* 167 */     SettlementMessageFormatter.formatValue(cPSLevel2Bean.getChargeDescription3(), formattedMessage, 40, false, true, true, false, true);
/*     */     
/*     */ 
/*     */ 
/* 171 */     SettlementMessageFormatter.formatValue(cPSLevel2Bean.getChargeItemQuantity3(), formattedMessage, 3, true, false, false, true, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 176 */     SettlementMessageFormatter.zeroFill(formattedMessage, 3);
/*     */     
/*     */ 
/* 179 */     SettlementMessageFormatter.formatValue(cPSLevel2Bean.getChargeItemAmount3(), formattedMessage, 12, true, false, false, true, false);
/*     */     
/*     */ 
/*     */ 
/* 183 */     SettlementMessageFormatter.formatValue(cPSLevel2Bean.getChargeDescription4(), formattedMessage, 40, false, true, true, false, true);
/*     */     
/*     */ 
/*     */ 
/* 187 */     SettlementMessageFormatter.formatValue(cPSLevel2Bean.getChargeItemQuantity4(), formattedMessage, 3, true, false, false, true, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 192 */     SettlementMessageFormatter.zeroFill(formattedMessage, 3);
/*     */     
/*     */ 
/* 195 */     SettlementMessageFormatter.formatValue(cPSLevel2Bean.getChargeItemAmount4(), formattedMessage, 12, true, false, false, true, false);
/*     */     
/*     */ 
/*     */ 
/* 199 */     SettlementMessageFormatter.formatValue(cPSLevel2Bean.getCardMemberReferenceNumber(), formattedMessage, 20, false, true, true, false, true);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 206 */     SettlementMessageFormatter.formatValue(cPSLevel2Bean.getShipToPostalCode(), formattedMessage, 15, false, true, true, false, true);
/*     */     
/*     */ 
/*     */ 
/* 210 */     SettlementMessageFormatter.characterSpacesFill(formattedMessage, 13);
/*     */     
/*     */ 
/*     */ 
/* 214 */     SettlementMessageFormatter.zeroFill(formattedMessage, 3);
/*     */     
/* 216 */     SettlementMessageFormatter.formatValue(cPSLevel2Bean.getTotalTaxAmount(), formattedMessage, 12, true, false, false, true, false);
/*     */     
/*     */ 
/*     */ 
/* 220 */     SettlementMessageFormatter.characterSpacesFill(formattedMessage, 20);
/*     */     
/* 222 */     SettlementMessageFormatter.formatValue(cPSLevel2Bean.getTaxTypeCode(), formattedMessage, 3, false, true, true, false, true);
/*     */     
/*     */ 
/*     */ 
/* 226 */     SettlementMessageFormatter.zeroFill(formattedMessage, 7);
/*     */     
/*     */ 
/* 229 */     if (PropertyReader.getFileType().equalsIgnoreCase("FIXED")) {
/* 230 */       SettlementMessageFormatter.characterSpacesFill(formattedMessage, 305);
/*     */     }
/* 232 */     formatedMessageList.add(formattedMessage.toString());
/* 233 */     if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
/* 234 */       LOGGER.info("Formated CPSLevel2 NonIndustry Type Record :" + formattedMessage.toString());
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void formatDeferredPaymentPlanRecord(DeferredPaymentPlanBean deferredPaymentPlanBean, List<String> formatedMessageList)
/*     */     throws IOException
/*     */   {
/* 250 */     StringBuffer formattedMessage = new StringBuffer();
/*     */     
/* 252 */     SettlementMessageFormatter.formatValue(deferredPaymentPlanBean.getRecordType(), 
/* 253 */       formattedMessage, 3, false, true, true, false, true);
/* 254 */     SettlementMessageFormatter.formatValue(deferredPaymentPlanBean.getRecordNumber(), formattedMessage, 8, true, false, false, true, false);
/*     */     
/* 256 */     SettlementMessageFormatter.formatValue(deferredPaymentPlanBean.getTransactionIdentifier(), 
/* 257 */       formattedMessage, 15, true, false, false, true, false);
/*     */     
/*     */ 
/*     */ 
/* 261 */     SettlementMessageFormatter.zeroFill(formattedMessage, 2);
/*     */     
/*     */ 
/*     */ 
/* 265 */     SettlementMessageFormatter.formatValue(deferredPaymentPlanBean.getAddendaTypeCode(), 
/* 266 */       formattedMessage, 2, false, true, false, false, true);
/*     */     
/*     */ 
/*     */ 
/* 270 */     SettlementMessageFormatter.zeroFill(formattedMessage, 3);
/*     */     
/*     */ 
/*     */ 
/* 274 */     SettlementMessageFormatter.formatValue(deferredPaymentPlanBean.getFullTransactionAmount(), 
/* 275 */       formattedMessage, 12, true, false, false, true, false);
/*     */     
/*     */ 
/* 278 */     SettlementMessageFormatter.formatValue(deferredPaymentPlanBean.getTypeOfPlanCode(), 
/* 279 */       formattedMessage, 4, false, true, false, false, true);
/*     */     
/* 281 */     SettlementMessageFormatter.formatValue(deferredPaymentPlanBean.getNoOfInstallments(), 
/* 282 */       formattedMessage, 4, true, false, false, true, false);
/*     */     
/*     */ 
/*     */ 
/* 286 */     SettlementMessageFormatter.zeroFill(formattedMessage, 3);
/*     */     
/*     */ 
/*     */ 
/* 290 */     SettlementMessageFormatter.formatValue(deferredPaymentPlanBean.getAmountOfInstallment(), 
/* 291 */       formattedMessage, 12, true, false, false, true, false);
/*     */     
/* 293 */     SettlementMessageFormatter.formatValue(deferredPaymentPlanBean.getInstallmentNumber(), 
/* 294 */       formattedMessage, 4, true, false, false, true, false);
/*     */     
/*     */ 
/* 297 */     SettlementMessageFormatter.formatValue(deferredPaymentPlanBean.getContractNumber(), 
/* 298 */       formattedMessage, 14, false, true, false, false, true);
/*     */     
/*     */ 
/*     */ 
/* 302 */     SettlementMessageFormatter.formatValue(deferredPaymentPlanBean.getPaymentTypeCode1(), 
/* 303 */       formattedMessage, 2, false, true, false, false, true);
/*     */     
/* 305 */     SettlementMessageFormatter.zeroFill(formattedMessage, 3);
/*     */     
/* 307 */     SettlementMessageFormatter.formatValue(deferredPaymentPlanBean.getPaymentTypeAmount1(), 
/* 308 */       formattedMessage, 12, true, false, false, true, false);
/*     */     
/* 310 */     SettlementMessageFormatter.zeroFill(formattedMessage, 15);
/*     */     
/*     */ 
/* 313 */     SettlementMessageFormatter.formatValue(deferredPaymentPlanBean.getPaymentTypeCode2(), 
/* 314 */       formattedMessage, 2, false, true, false, false, true);
/*     */     
/* 316 */     SettlementMessageFormatter.zeroFill(formattedMessage, 3);
/*     */     
/* 318 */     SettlementMessageFormatter.formatValue(deferredPaymentPlanBean.getPaymentTypeAmount2(), 
/* 319 */       formattedMessage, 12, true, false, false, true, false);
/*     */     
/* 321 */     SettlementMessageFormatter.zeroFill(formattedMessage, 15);
/*     */     
/*     */ 
/* 324 */     SettlementMessageFormatter.formatValue(deferredPaymentPlanBean.getPaymentTypeCode3(), 
/* 325 */       formattedMessage, 2, false, true, false, false, true);
/*     */     
/* 327 */     SettlementMessageFormatter.zeroFill(formattedMessage, 3);
/*     */     
/* 329 */     SettlementMessageFormatter.formatValue(deferredPaymentPlanBean.getPaymentTypeAmount3(), 
/* 330 */       formattedMessage, 12, true, false, false, true, false);
/*     */     
/* 332 */     SettlementMessageFormatter.zeroFill(formattedMessage, 15);
/*     */     
/* 334 */     SettlementMessageFormatter.formatValue(deferredPaymentPlanBean.getPaymentTypeCode4(), 
/* 335 */       formattedMessage, 2, false, true, false, false, true);
/*     */     
/* 337 */     SettlementMessageFormatter.zeroFill(formattedMessage, 3);
/*     */     
/* 339 */     SettlementMessageFormatter.formatValue(deferredPaymentPlanBean.getPaymentTypeAmount4(), 
/* 340 */       formattedMessage, 12, true, false, false, true, false);
/*     */     
/* 342 */     SettlementMessageFormatter.zeroFill(formattedMessage, 15);
/*     */     
/* 344 */     SettlementMessageFormatter.formatValue(deferredPaymentPlanBean.getPaymentTypeCode5(), 
/* 345 */       formattedMessage, 2, false, true, false, false, true);
/*     */     
/* 347 */     SettlementMessageFormatter.zeroFill(formattedMessage, 3);
/*     */     
/* 349 */     SettlementMessageFormatter.formatValue(deferredPaymentPlanBean.getPaymentTypeAmount5(), 
/* 350 */       formattedMessage, 12, true, false, false, true, false);
/*     */     
/* 352 */     SettlementMessageFormatter.zeroFill(formattedMessage, 15);
/* 353 */     if (PropertyReader.getFileType().equalsIgnoreCase("FIXED")) {
/* 354 */       SettlementMessageFormatter.characterSpacesFill(formattedMessage, 454);
/*     */     }
/* 356 */     formatedMessageList.add(formattedMessage.toString());
/* 357 */     if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
/* 358 */       LOGGER.info("Formated DPP NonIndustry Type Record :" + formattedMessage.toString());
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\formats\NonIndustryTAARecordFormatter.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */