/*    */ package com.americanexpress.ips.gfsg.formats;
/*    */ 
/*    */ import com.americanexpress.ips.gfsg.beans.common.TransactionFileSummaryBean;
/*    */ import com.americanexpress.ips.gfsg.connection.PropertyReader;
/*    */ import java.io.IOException;
/*    */ import java.util.List;
/*    */ import org.apache.log4j.Logger;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public final class TFSRecordFormatter
/*    */   extends SettlementMessageFormatter
/*    */ {
/* 22 */   private static final Logger LOGGER = Logger.getLogger(TFSRecordFormatter.class);
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public static void formatTFSRecord(TransactionFileSummaryBean transactionFileSummaryBean, List<String> formatedMessageList)
/*    */     throws IOException
/*    */   {
/* 36 */     StringBuffer formattedMessage = new StringBuffer();
/*    */     
/* 38 */     formatValue(transactionFileSummaryBean.getRecordType(), formattedMessage, 3, false, true, true, false, true);
/* 39 */     formatValue(transactionFileSummaryBean.getRecordNumber(), formattedMessage, 8, true, false, false, true, false);
/*    */     
/* 41 */     formatValue(transactionFileSummaryBean.getNumberOfDebits(), formattedMessage, 8, true, false, false, true, false);
/*    */     
/* 43 */     zeroFill(formattedMessage, 3);
/*    */     
/* 45 */     formatValue(transactionFileSummaryBean.getHashTotalDebitAmount(), formattedMessage, 20, true, false, false, true, false);
/* 46 */     formatValue(transactionFileSummaryBean.getNumberOfCredits(), formattedMessage, 8, true, false, false, true, false);
/*    */     
/*    */ 
/* 49 */     zeroFill(formattedMessage, 3);
/* 50 */     formatValue(transactionFileSummaryBean.getHashTotalCreditAmount(), formattedMessage, 20, true, false, false, true, false);
/*    */     
/*    */ 
/*    */ 
/* 54 */     zeroFill(formattedMessage, 3);
/*    */     
/* 56 */     formatValue(transactionFileSummaryBean.getHashTotalAmount(), formattedMessage, 20, true, false, false, true, false);
/*    */     
/*    */ 
/*    */ 
/* 60 */     if (PropertyReader.getFileType().equalsIgnoreCase("FIXED")) {
/* 61 */       characterSpacesFill(formattedMessage, 604);
/*    */     }
/* 63 */     formatedMessageList.add(formattedMessage.toString());
/* 64 */     LOGGER.info("After Formatting TFS Rec:" + formattedMessage.toString());
/* 65 */     LOGGER.info("Formatted TFS Rec Length:" + formattedMessage.toString().length());
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\formats\TFSRecordFormatter.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */