/*     */ package com.americanexpress.ips.gfsg.formats;
/*     */ 
/*     */ import com.americanexpress.ips.gfsg.beans.common.TransactionAdviceDetailBean;
/*     */ import com.americanexpress.ips.gfsg.connection.PropertyReader;
/*     */ import java.io.IOException;
/*     */ import java.util.List;
/*     */ import org.apache.log4j.Logger;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public final class TADRecordFormatter
/*     */ {
/*  23 */   private static final Logger LOGGER = Logger.getLogger(TADRecordFormatter.class);
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void formatTADRecord(TransactionAdviceDetailBean transactionAdviceDetailBean, List<String> formatedMessageList)
/*     */     throws IOException
/*     */   {
/*  39 */     StringBuffer formattedMessage = new StringBuffer();
/*     */     
/*  41 */     SettlementMessageFormatter.formatValue(transactionAdviceDetailBean.getRecordType(), 
/*  42 */       formattedMessage, 3, false, true, true, false, true);
/*  43 */     SettlementMessageFormatter.formatValue(transactionAdviceDetailBean.getRecordNumber(), 
/*  44 */       formattedMessage, 8, true, false, false, true, false);
/*     */     
/*  46 */     SettlementMessageFormatter.formatValue(transactionAdviceDetailBean.getTransactionIdentifier(), 
/*  47 */       formattedMessage, 15, true, false, false, true, false);
/*     */     
/*  49 */     SettlementMessageFormatter.formatValue(transactionAdviceDetailBean.getAdditionalAmountType1(), 
/*  50 */       formattedMessage, 3, true, false, false, true, false);
/*     */     
/*     */ 
/*     */ 
/*  54 */     SettlementMessageFormatter.zeroFill(formattedMessage, 3);
/*     */     
/*  56 */     SettlementMessageFormatter.formatValue(transactionAdviceDetailBean.getAdditionalAmount1(), 
/*  57 */       formattedMessage, 12, true, false, false, true, false);
/*     */     
/*  59 */     SettlementMessageFormatter.formatValue(transactionAdviceDetailBean.getAdditionalAmountSign1(), 
/*  60 */       formattedMessage, 1, false, true, false, false, true);
/*     */     
/*  62 */     SettlementMessageFormatter.formatValue(transactionAdviceDetailBean.getAdditionalAmountType2(), 
/*  63 */       formattedMessage, 3, true, false, false, true, false);
/*     */     
/*     */ 
/*     */ 
/*  67 */     SettlementMessageFormatter.zeroFill(formattedMessage, 3);
/*     */     
/*  69 */     SettlementMessageFormatter.formatValue(transactionAdviceDetailBean.getAdditionalAmount2(), 
/*  70 */       formattedMessage, 12, true, false, false, true, false);
/*     */     
/*  72 */     SettlementMessageFormatter.formatValue(transactionAdviceDetailBean.getAdditionalAmountSign2(), 
/*  73 */       formattedMessage, 1, false, true, false, false, true);
/*     */     
/*  75 */     SettlementMessageFormatter.formatValue(transactionAdviceDetailBean.getAdditionalAmountType3(), 
/*  76 */       formattedMessage, 3, true, false, false, true, false);
/*     */     
/*     */ 
/*     */ 
/*  80 */     SettlementMessageFormatter.zeroFill(formattedMessage, 3);
/*     */     
/*  82 */     SettlementMessageFormatter.formatValue(transactionAdviceDetailBean.getAdditionalAmount3(), 
/*  83 */       formattedMessage, 12, true, false, false, true, false);
/*     */     
/*  85 */     SettlementMessageFormatter.formatValue(transactionAdviceDetailBean.getAdditionalAmountSign3(), 
/*  86 */       formattedMessage, 1, false, true, false, false, true);
/*  87 */     SettlementMessageFormatter.formatValue(transactionAdviceDetailBean.getAdditionalAmountType4(), 
/*  88 */       formattedMessage, 3, true, false, false, true, false);
/*     */     
/*     */ 
/*     */ 
/*  92 */     SettlementMessageFormatter.zeroFill(formattedMessage, 3);
/*     */     
/*  94 */     SettlementMessageFormatter.formatValue(transactionAdviceDetailBean.getAdditionalAmount4(), 
/*  95 */       formattedMessage, 12, true, false, false, true, false);
/*     */     
/*  97 */     SettlementMessageFormatter.formatValue(transactionAdviceDetailBean.getAdditionalAmountSign4(), 
/*  98 */       formattedMessage, 1, false, true, false, false, true);
/*  99 */     SettlementMessageFormatter.formatValue(transactionAdviceDetailBean.getAdditionalAmountType5(), 
/* 100 */       formattedMessage, 3, true, false, false, true, false);
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 105 */     SettlementMessageFormatter.zeroFill(formattedMessage, 3);
/*     */     
/* 107 */     SettlementMessageFormatter.formatValue(transactionAdviceDetailBean.getAdditionalAmount5(), 
/* 108 */       formattedMessage, 12, true, false, false, true, false);
/*     */     
/* 110 */     SettlementMessageFormatter.formatValue(transactionAdviceDetailBean.getAdditionalAmountSign5(), 
/* 111 */       formattedMessage, 1, false, true, false, false, true);
/*     */     
/* 113 */     if (PropertyReader.getFileType().equalsIgnoreCase("FIXED")) {
/* 114 */       SettlementMessageFormatter.characterSpacesFill(formattedMessage, 579);
/*     */     }
/* 116 */     formatedMessageList.add(formattedMessage.toString());
/* 117 */     if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
/* 118 */       LOGGER.info("Formated TAD Record Type :" + formattedMessage.toString());
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\formats\TADRecordFormatter.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */