/*    */ package com.americanexpress.ips.gfsg.formats;
/*    */ 
/*    */ import com.americanexpress.ips.gfsg.beans.common.TransactionBatchTrailerBean;
/*    */ import com.americanexpress.ips.gfsg.connection.PropertyReader;
/*    */ import java.io.IOException;
/*    */ import java.util.List;
/*    */ import org.apache.log4j.Logger;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public final class TBTRecordFormatter
/*    */ {
/* 23 */   private static final Logger LOGGER = Logger.getLogger(TBTRecordFormatter.class);
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public static void formatTBTRecord(TransactionBatchTrailerBean transactionBatchTrailerBean, List<String> formatedMessageList)
/*    */     throws IOException
/*    */   {
/* 38 */     StringBuffer formattedMessage = new StringBuffer();
/*    */     
/*    */ 
/*    */ 
/* 42 */     SettlementMessageFormatter.formatValue(transactionBatchTrailerBean.getRecordType(), formattedMessage, 3, false, true, true, false, true);
/*    */     
/* 44 */     SettlementMessageFormatter.formatValue(transactionBatchTrailerBean.getRecordNumber(), formattedMessage, 8, true, false, false, true, false);
/*    */     
/* 46 */     SettlementMessageFormatter.formatValue(transactionBatchTrailerBean.getMerchantId(), formattedMessage, 15, false, true, false, false, true);
/*    */     
/*    */ 
/* 49 */     SettlementMessageFormatter.characterSpacesFill(formattedMessage, 15);
/*    */     
/* 51 */     SettlementMessageFormatter.formatValue(transactionBatchTrailerBean.getTbtIdentificationNumber(), formattedMessage, 15, true, false, false, true, false);
/*    */     
/* 53 */     SettlementMessageFormatter.formatValue(transactionBatchTrailerBean.getTbtCreationDate(), formattedMessage, 8, true, false, false, true, false);
/*    */     
/* 55 */     SettlementMessageFormatter.formatValue(transactionBatchTrailerBean.getTotalNoOfTabs(), formattedMessage, 8, true, false, false, true, false);
/*    */     
/*    */ 
/* 58 */     SettlementMessageFormatter.zeroFill(formattedMessage, 3);
/*    */     
/* 60 */     SettlementMessageFormatter.formatValue(transactionBatchTrailerBean.getTbtAmount(), formattedMessage, 20, true, false, false, true, false);
/*    */     
/* 62 */     SettlementMessageFormatter.formatValue(transactionBatchTrailerBean.getTbtAmountSign(), formattedMessage, 1, false, true, false, false, true);
/*    */     
/* 64 */     SettlementMessageFormatter.formatValue(transactionBatchTrailerBean.getTbtCurrencyCode(), formattedMessage, 3, true, false, true, false, false);
/*    */     
/*    */ 
/*    */ 
/* 68 */     SettlementMessageFormatter.zeroFill(formattedMessage, 3);
/*    */     
/* 70 */     SettlementMessageFormatter.zeroFill(formattedMessage, 20);
/*    */     
/* 72 */     SettlementMessageFormatter.characterSpacesFill(formattedMessage, 3);
/*    */     
/* 74 */     SettlementMessageFormatter.formatValue(transactionBatchTrailerBean.getTbtImageSequenceNumber(), formattedMessage, 8, false, true, false, false, true);
/*    */     
/*    */ 
/* 77 */     if (PropertyReader.getFileType().equalsIgnoreCase("FIXED")) {
/* 78 */       SettlementMessageFormatter.characterSpacesFill(formattedMessage, 567);
/*    */     }
/* 80 */     formatedMessageList.add(formattedMessage.toString());
/*    */     
/* 82 */     if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
/* 83 */       LOGGER.info("Formated TBT Record Type :" + formattedMessage.toString());
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\formats\TBTRecordFormatter.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */