/*    */ package com.americanexpress.ips.gfsg.formats;
/*    */ 
/*    */ import com.americanexpress.ips.gfsg.beans.common.taa.LocationDetailTAABean;
/*    */ import com.americanexpress.ips.gfsg.connection.PropertyReader;
/*    */ import java.io.IOException;
/*    */ import java.util.List;
/*    */ import org.apache.log4j.Logger;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public final class LocationTAAFormatter
/*    */ {
/* 21 */   private static final Logger LOGGER = Logger.getLogger(LocationTAAFormatter.class);
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public static void formatLocationTAARecord(LocationDetailTAABean locationDetailTAABean, List<String> formatedMessageList)
/*    */     throws IOException
/*    */   {
/* 36 */     StringBuffer formattedMessage = new StringBuffer();
/*    */     
/* 38 */     SettlementMessageFormatter.formatValue(locationDetailTAABean
/* 39 */       .getRecordType(), formattedMessage, 3, false, true, true, 
/* 40 */       false, true);
/*    */     
/* 42 */     SettlementMessageFormatter.formatValue(locationDetailTAABean
/* 43 */       .getRecordNumber(), formattedMessage, 8, true, false, false, 
/* 44 */       true, false);
/*    */     
/* 46 */     SettlementMessageFormatter.formatValue(locationDetailTAABean
/* 47 */       .getTransactionIdentifier(), formattedMessage, 15, true, false, 
/* 48 */       false, true, false);
/*    */     
/* 50 */     SettlementMessageFormatter.zeroFill(formattedMessage, 2);
/*    */     
/* 52 */     SettlementMessageFormatter.formatValue(locationDetailTAABean
/* 53 */       .getAddendaTypeCode(), formattedMessage, 2, false, true, false, 
/* 54 */       false, true);
/*    */     
/* 56 */     SettlementMessageFormatter.formatValue(locationDetailTAABean
/* 57 */       .getLocationName(), formattedMessage, 38, false, true, true, 
/* 58 */       false, true);
/*    */     
/* 60 */     SettlementMessageFormatter.formatValue(locationDetailTAABean
/* 61 */       .getLocationAddress(), formattedMessage, 38, false, true, true, 
/* 62 */       false, true);
/*    */     
/* 64 */     SettlementMessageFormatter.formatValue(locationDetailTAABean
/* 65 */       .getLocationCity(), formattedMessage, 21, false, true, true, 
/* 66 */       false, true);
/*    */     
/* 68 */     SettlementMessageFormatter.formatValue(locationDetailTAABean
/* 69 */       .getLocationRegion(), formattedMessage, 3, false, true, true, 
/* 70 */       false, true);
/*    */     
/* 72 */     SettlementMessageFormatter.formatValue(locationDetailTAABean
/* 73 */       .getLocationCountryCode(), formattedMessage, 3, false, true, 
/* 74 */       true, false, true);
/*    */     
/* 76 */     SettlementMessageFormatter.formatValue(locationDetailTAABean
/* 77 */       .getLocationPostalCode(), formattedMessage, 15, false, true, 
/* 78 */       true, false, true);
/*    */     
/* 80 */     SettlementMessageFormatter.formatValue(locationDetailTAABean
/* 81 */       .getMerchantCategoryCode(), formattedMessage, 4, true, false, 
/* 82 */       false, true, false);
/*    */     
/* 84 */     SettlementMessageFormatter.formatValue(locationDetailTAABean
/* 85 */       .getSellerId(), formattedMessage, 20, false, true, true, false, 
/* 86 */       true);
/*    */     
/* 88 */     if (PropertyReader.getFileType().equalsIgnoreCase("FIXED")) {
/* 89 */       SettlementMessageFormatter.characterSpacesFill(formattedMessage, 
/* 90 */         528);
/*    */     }
/*    */     
/* 93 */     formatedMessageList.add(formattedMessage.toString());
/*    */     
/* 95 */     if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
/* 96 */       LOGGER.info("Formated TAALocation Type Record :" + formattedMessage.toString());
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\formats\LocationTAAFormatter.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */