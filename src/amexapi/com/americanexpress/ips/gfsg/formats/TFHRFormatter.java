/*    */ package com.americanexpress.ips.gfsg.formats;
/*    */ 
/*    */ import com.americanexpress.ips.gfsg.beans.common.TransactionFileHeaderBean;
/*    */ import com.americanexpress.ips.gfsg.connection.PropertyReader;
/*    */ import java.io.IOException;
/*    */ import java.util.List;
/*    */ import org.apache.log4j.Logger;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public final class TFHRFormatter
/*    */   extends SettlementMessageFormatter
/*    */ {
/* 23 */   private static final Logger LOGGER = Logger.getLogger(TFHRFormatter.class);
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public static void formatTFHRecord(TransactionFileHeaderBean transactionFileHeaderBean, List<String> formatedMessageList)
/*    */     throws IOException
/*    */   {
/* 37 */     StringBuffer formattedMessage = new StringBuffer();
/*    */     
/*    */ 
/*    */ 
/* 41 */     formatValue(transactionFileHeaderBean.getRecordType(), formattedMessage, 3, false, true, true, false, true);
/* 42 */     formatValue(transactionFileHeaderBean.getRecordNumber(), formattedMessage, 8, true, false, false, true, false);
/*    */     
/* 44 */     formatValue(transactionFileHeaderBean.getSubmitterId(), formattedMessage, 11, false, true, true, false, true);
/*    */     
/*    */ 
/* 47 */     characterSpacesFill(formattedMessage, 21);
/*    */     
/* 49 */     formatValue(transactionFileHeaderBean.getSubmitterFileReferenceNumber(), formattedMessage, 9, false, true, true, false, true);
/*    */     
/* 51 */     formatValue(transactionFileHeaderBean.getSubmitterFileSequenceNumber(), formattedMessage, 9, true, false, false, true, false);
/*    */     
/*    */ 
/* 54 */     formatValue(transactionFileHeaderBean.getFileCreationDate(), formattedMessage, 8, true, false, false, true, false);
/* 55 */     formatValue(transactionFileHeaderBean.getFileCreationTime(), formattedMessage, 6, true, false, false, true, false);
/* 56 */     formatValue(transactionFileHeaderBean.getFileVersionNumber(), formattedMessage, 8, false, true, false, false, true);
/*    */     
/* 58 */     if (PropertyReader.getFileType().equalsIgnoreCase("FIXED")) {
/* 59 */       characterSpacesFill(formattedMessage, 617);
/*    */     }
/*    */     
/* 62 */     formatedMessageList.add(formattedMessage.toString());
/*    */     
/* 64 */     if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
/* 65 */       LOGGER.info("Formated TFH Record Type :" + formattedMessage.toString());
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\formats\TFHRFormatter.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */