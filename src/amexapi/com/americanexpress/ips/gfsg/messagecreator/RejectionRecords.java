/*     */ package com.americanexpress.ips.gfsg.messagecreator;
/*     */ 
/*     */ import com.americanexpress.ips.gfsg.beans.common.TransactionAdviceDetailBean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.TransactionBatchTrailerBean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.taa.LocationDetailTAABean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.AirlineIndustryTAABean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.AutoRentalIndustryTAABean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.CommunicationServicesIndustryBean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.EntertainmentTicketingIndustryTAABean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.InsuranceIndustryTAABean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.LodgingIndustryTAABean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.RailIndustryTAABean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.RetailIndustryTAABean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.TravelCruiseIndustryTAABean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.taa.nonindustrytype.CPSLevel2Bean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.taa.nonindustrytype.DeferredPaymentPlanBean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.taa.nonindustrytype.EMVBean;
/*     */ import com.americanexpress.ips.gfsg.beans.conformation.ConfirmationRejectedRecordBean;
/*     */ import com.americanexpress.ips.gfsg.beans.conformation.RejectedRecordDataBean;
/*     */ import com.americanexpress.ips.gfsg.beans.conformation.RejectedTABRecord;
/*     */ import org.apache.log4j.Logger;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class RejectionRecords
/*     */ {
/*  34 */   private static final Logger LOGGER = Logger.getLogger(RejectionRecords.class);
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void setTABRecord(StringBuffer recordType, ConfirmationRejectedRecordBean rejectedTABRecord)
/*     */   {
/*  47 */     RejectedRecordDataBean rejRecTabBean = new RejectedRecordDataBean();
/*  48 */     RejectedTABRecord tabRecord = new RejectedTABRecord();
/*  49 */     rejectedTABRecord.setReturnedaRecordType(recordSubstring(recordType.toString(), 0, 3));
/*     */     
/*  51 */     rejectedTABRecord.setReturnedRecordNumber(recordSubstring(recordType.toString(), 3, 11));
/*  52 */     rejectedTABRecord.setReturnedRecordData(rejRecTabBean);
/*     */     
/*     */ 
/*  55 */     tabRecord.setRecordType(recordSubstring(recordType.toString(), 0, 3));
/*  56 */     tabRecord.setRecordNumber(recordSubstring(recordType.toString(), 3, 11));
/*  57 */     tabRecord.setTransactionIdentifier(recordSubstring(recordType.toString(), 11, 26));
/*  58 */     tabRecord.setFormatCode(recordSubstring(recordType.toString(), 26, 28));
/*     */     
/*  60 */     tabRecord.setMediaCode(recordSubstring(recordType.toString(), 28, 30));
/*  61 */     tabRecord.setSubmissionMethod(recordSubstring(recordType.toString(), 30, 32));
/*  62 */     tabRecord.setTabField7reserved(recordSubstring(recordType.toString(), 32, 42));
/*  63 */     tabRecord.setApprovalCode(recordSubstring(recordType.toString(), 42, 48));
/*     */     
/*  65 */     tabRecord.setPrimaryAccountNumber(recordSubstring(recordType.toString(), 48, 67));
/*  66 */     tabRecord.setCardExpiryDate(recordSubstring(recordType.toString(), 67, 71));
/*  67 */     tabRecord.setTransactionDate(recordSubstring(recordType.toString(), 71, 79));
/*  68 */     tabRecord.setTransactionTime(recordSubstring(recordType.toString(), 79, 85));
/*     */     
/*  70 */     tabRecord.setTransactionAmount(recordSubstring(recordType.toString(), 88, 100));
/*  71 */     tabRecord.setProcessingCode(recordSubstring(recordType.toString(), 100, 106));
/*  72 */     tabRecord.setTransactionCurrencyCode(recordSubstring(recordType.toString(), 106, 109));
/*  73 */     tabRecord.setExtendedPaymentData(recordSubstring(recordType.toString(), 109, 111));
/*  74 */     tabRecord.setMerchantId(recordSubstring(recordType.toString(), 111, 126));
/*  75 */     tabRecord.setMerchantLocationId(recordSubstring(recordType.toString(), 126, 141));
/*  76 */     tabRecord.setMerchantContactInfo(recordSubstring(recordType.toString(), 141, 181));
/*  77 */     tabRecord.setTerminalId(recordSubstring(recordType.toString(), 181, 189));
/*  78 */     tabRecord.setPosDataCode(recordSubstring(recordType.toString(), 189, 201));
/*  79 */     tabRecord.setInvoiceReferenceNumber(recordSubstring(recordType.toString(), 219, 249));
/*  80 */     tabRecord.setTabImageSequenceNumber(recordSubstring(recordType.toString(), 264, 272));
/*  81 */     tabRecord.setMatchingKeyType(recordSubstring(recordType.toString(), 272, 274));
/*  82 */     tabRecord.setMatchingKey(recordSubstring(recordType.toString(), 274, 295));
/*  83 */     tabRecord.setElectronicCommerceIndicator(recordSubstring(recordType.toString(), 295, 297));
/*  84 */     rejRecTabBean.setTabRecord(tabRecord);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void setTADRecord(StringBuffer recordType, ConfirmationRejectedRecordBean rejectedTADRecord)
/*     */   {
/*  97 */     RejectedRecordDataBean rejRecTadBean = new RejectedRecordDataBean();
/*     */     
/*  99 */     rejectedTADRecord.setReturnedaRecordType(recordSubstring(recordType.toString(), 0, 3));
/*     */     
/* 101 */     rejectedTADRecord.setReturnedRecordNumber(recordSubstring(recordType.toString(), 3, 11));
/*     */     
/* 103 */     TransactionAdviceDetailBean tadBean = new TransactionAdviceDetailBean();
/* 104 */     tadBean.setRecordType(recordSubstring(recordType.toString(), 0, 3));
/* 105 */     tadBean.setRecordNumber(recordSubstring(recordType.toString(), 3, 11));
/* 106 */     tadBean.setTransactionIdentifier(recordSubstring(recordType.toString(), 11, 26));
/* 107 */     tadBean.setAdditionalAmountType1(recordSubstring(recordType.toString(), 26, 29));
/* 108 */     tadBean.setAdditionalAmount1(recordSubstring(recordType.toString(), 32, 44));
/* 109 */     tadBean.setAdditionalAmountSign1(recordSubstring(recordType.toString(), 44, 45));
/* 110 */     tadBean.setAdditionalAmountType2(recordSubstring(recordType.toString(), 45, 48));
/* 111 */     tadBean.setAdditionalAmount2(recordSubstring(recordType.toString(), 51, 63));
/* 112 */     tadBean.setAdditionalAmountSign2(recordSubstring(recordType.toString(), 63, 64));
/* 113 */     tadBean.setAdditionalAmountType3(recordSubstring(recordType.toString(), 64, 67));
/* 114 */     tadBean.setAdditionalAmount3(recordSubstring(recordType.toString(), 70, 82));
/*     */     
/* 116 */     tadBean.setAdditionalAmountSign3(recordSubstring(recordType.toString(), 82, 83));
/* 117 */     tadBean.setAdditionalAmountType4(recordSubstring(recordType.toString(), 83, 86));
/* 118 */     tadBean.setAdditionalAmount4(recordSubstring(recordType.toString(), 89, 101));
/* 119 */     tadBean.setAdditionalAmountSign4(recordSubstring(recordType.toString(), 101, 102));
/* 120 */     tadBean.setAdditionalAmountType5(recordSubstring(recordType.toString(), 102, 105));
/* 121 */     tadBean.setAdditionalAmount5(recordSubstring(recordType.toString(), 108, 120));
/* 122 */     tadBean.setAdditionalAmountSign5(recordSubstring(recordType.toString(), 120, 121));
/* 123 */     rejRecTadBean.setTadRecordBean(tadBean);
/* 124 */     rejectedTADRecord.setReturnedRecordData(rejRecTadBean);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void setTBTRecord(StringBuffer recordType, ConfirmationRejectedRecordBean rejectedTBTRecord)
/*     */   {
/* 138 */     rejectedTBTRecord.setReturnedaRecordType(recordSubstring(recordType.toString(), 0, 3));
/* 139 */     rejectedTBTRecord.setReturnedRecordNumber(recordSubstring(recordType.toString(), 3, 11));
/* 140 */     RejectedRecordDataBean rejRecTBTBean = new RejectedRecordDataBean();
/* 141 */     TransactionBatchTrailerBean tbtBean = new TransactionBatchTrailerBean();
/* 142 */     tbtBean.setRecordType(recordSubstring(recordType.toString(), 0, 3));
/* 143 */     tbtBean.setRecordNumber(recordSubstring(recordType.toString(), 3, 11));
/* 144 */     tbtBean.setMerchantId(recordSubstring(recordType.toString(), 11, 26));
/* 145 */     tbtBean.setTbtIdentificationNumber(recordSubstring(recordType.toString(), 41, 56));
/* 146 */     tbtBean.setTbtCreationDate(recordSubstring(recordType.toString(), 56, 64));
/* 147 */     tbtBean.setTotalNoOfTabs(recordSubstring(recordType.toString(), 64, 72));
/* 148 */     tbtBean.setTbtAmount(recordSubstring(recordType.toString(), 75, 95));
/* 149 */     tbtBean.setTbtAmountSign(recordSubstring(recordType.toString(), 95, 96));
/* 150 */     tbtBean.setTbtCurrencyCode(recordSubstring(recordType.toString(), 96, 99));
/* 151 */     tbtBean.setTbtImageSequenceNumber(recordSubstring(recordType.toString(), 125, 133));
/* 152 */     rejRecTBTBean.setTbtRecord(tbtBean);
/* 153 */     rejectedTBTRecord.setReturnedRecordData(rejRecTBTBean);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void setTAAAirRecord(StringBuffer recordType, ConfirmationRejectedRecordBean rejectedTAAAirecord)
/*     */   {
/* 167 */     rejectedTAAAirecord.setReturnedaRecordType(recordSubstring(recordType.toString(), 0, 3));
/* 168 */     rejectedTAAAirecord.setReturnedRecordNumber(recordSubstring(recordType.toString(), 3, 11));
/* 169 */     RejectedRecordDataBean rejRecAirlineBean = new RejectedRecordDataBean();
/* 170 */     AirlineIndustryTAABean airlineBean = new AirlineIndustryTAABean();
/*     */     
/* 172 */     airlineBean.setRecordType(recordSubstring(recordType.toString(), 0, 3));
/*     */     
/* 174 */     airlineBean.setRecordNumber(recordSubstring(recordType.toString(), 3, 11));
/*     */     
/* 176 */     airlineBean.setTransactionIdentifier(recordSubstring(recordType.toString(), 11, 26));
/*     */     
/* 178 */     airlineBean.setFormatCode(recordSubstring(recordType.toString(), 26, 28));
/* 179 */     airlineBean.setAddendaTypeCode(recordSubstring(recordType.toString(), 28, 30));
/* 180 */     airlineBean.setTransactionType(recordSubstring(recordType.toString(), 30, 33));
/* 181 */     airlineBean.setTicketNumber(recordSubstring(recordType.toString(), 33, 47));
/* 182 */     airlineBean.setDocumentType(recordSubstring(recordType.toString(), 47, 49));
/* 183 */     airlineBean.setAirlineProcessIdentifier(recordSubstring(recordType.toString(), 49, 52));
/* 184 */     airlineBean.setIataNumericCode(recordSubstring(recordType.toString(), 52, 60));
/* 185 */     airlineBean.setTicketingCarrierName(recordSubstring(recordType.toString(), 60, 85));
/* 186 */     airlineBean.setTicketIssueCity(recordSubstring(recordType.toString(), 85, 103));
/* 187 */     airlineBean.setTicketIssueDate(recordSubstring(recordType.toString(), 103, 111));
/* 188 */     airlineBean.setNumberInParty(recordSubstring(recordType.toString(), 111, 114));
/* 189 */     airlineBean.setPassengerName(recordSubstring(recordType.toString(), 114, 139));
/* 190 */     airlineBean.setConjunctionTicketIndicator(recordSubstring(recordType.toString(), 139, 140));
/*     */     
/* 192 */     airlineBean.setOriginalTransactionAmount(recordSubstring(recordType.toString(), 143, 155));
/* 193 */     airlineBean.setOriginalCurrencyCode(recordSubstring(recordType.toString(), 155, 158));
/* 194 */     airlineBean.setElectronicTicketIndicator(recordSubstring(recordType.toString(), 158, 159));
/* 195 */     airlineBean.setTotalNumberOfAirSegments(recordSubstring(recordType.toString(), 159, 160));
/* 196 */     airlineBean.setStopoverIndicator1(recordSubstring(recordType.toString(), 106, 161));
/* 197 */     airlineBean.setDepartureLocationCodeSegment1(recordSubstring(recordType.toString(), 161, 164));
/* 198 */     airlineBean.setDepartureDateSegment1(recordSubstring(recordType.toString(), 164, 172));
/* 199 */     airlineBean.setArrivalLocationCodeSegment1(recordSubstring(recordType.toString(), 172, 175));
/* 200 */     airlineBean.setSegmentCarrierCode1(recordSubstring(recordType.toString(), 175, 177));
/* 201 */     airlineBean.setSegment1FareBasis(recordSubstring(recordType.toString(), 177, 192));
/* 202 */     airlineBean.setClassOfServiceCodeSegment1(recordSubstring(recordType.toString(), 192, 194));
/* 203 */     airlineBean.setFlightNumberSegment1(recordSubstring(recordType.toString(), 194, 198));
/* 204 */     airlineBean.setSegment1Fare(recordSubstring(recordType.toString(), 201, 213));
/* 205 */     airlineBean.setStopoverIndicator2(recordSubstring(recordType.toString(), 213, 214));
/* 206 */     airlineBean.setDepartureLocationCodeSegment2(recordSubstring(recordType.toString(), 214, 217));
/* 207 */     airlineBean.setDepartureDateSegment2(recordSubstring(recordType.toString(), 217, 225));
/* 208 */     airlineBean.setArrivalLocationCodeSegment2(recordSubstring(recordType.toString(), 225, 228));
/* 209 */     airlineBean.setSegmentCarrierCode2(recordSubstring(recordType.toString(), 228, 230));
/* 210 */     airlineBean.setSegment2FareBasis(recordSubstring(recordType.toString(), 230, 245));
/* 211 */     airlineBean.setClassOfServiceCodeSegment2(recordSubstring(recordType.toString(), 245, 247));
/* 212 */     airlineBean.setFlightNumberSegment2(recordSubstring(recordType.toString(), 247, 251));
/*     */     
/* 214 */     airlineBean.setSegment2Fare(recordSubstring(recordType.toString(), 254, 266));
/* 215 */     airlineBean.setStopoverIndicator3(recordSubstring(recordType.toString(), 266, 267));
/* 216 */     airlineBean.setDepartureLocationCodeSegment3(recordSubstring(recordType.toString(), 267, 270));
/* 217 */     airlineBean.setDepartureDateSegment3(recordSubstring(recordType.toString(), 270, 278));
/* 218 */     airlineBean.setArrivalLocationCodeSegment3(recordSubstring(recordType.toString(), 278, 281));
/* 219 */     airlineBean.setSegmentCarrierCode3(recordSubstring(recordType.toString(), 281, 283));
/* 220 */     airlineBean.setSegment3FareBasis(recordSubstring(recordType.toString(), 283, 298));
/* 221 */     airlineBean.setClassOfServiceCodeSegment3(recordSubstring(recordType.toString(), 298, 300));
/* 222 */     airlineBean.setFlightNumberSegment3(recordSubstring(recordType.toString(), 300, 304));
/* 223 */     airlineBean.setSegment3Fare(recordSubstring(recordType.toString(), 307, 319));
/* 224 */     airlineBean.setStopoverIndicator4(recordSubstring(recordType.toString(), 319, 320));
/* 225 */     airlineBean.setDepartureLocationCodeSegment4(recordSubstring(recordType.toString(), 320, 323));
/* 226 */     airlineBean.setDepartureDateSegment4(recordSubstring(recordType.toString(), 323, 331));
/* 227 */     airlineBean.setArrivalLocationCodeSegment4(recordSubstring(recordType.toString(), 331, 334));
/* 228 */     airlineBean.setSegmentCarrierCode4(recordSubstring(recordType.toString(), 334, 336));
/* 229 */     airlineBean.setSegment4FareBasis(recordSubstring(recordType.toString(), 336, 351));
/* 230 */     airlineBean.setClassOfServiceCodeSegment4(recordSubstring(recordType.toString(), 351, 353));
/* 231 */     airlineBean.setFlightNumberSegment4(recordSubstring(recordType.toString(), 353, 357));
/* 232 */     airlineBean.setSegment4Fare(recordSubstring(recordType.toString(), 360, 372));
/* 233 */     airlineBean.setStopoverIndicator5(recordSubstring(recordType.toString(), 372, 373));
/* 234 */     airlineBean.setExchangedOrOriginalTicketNumber(recordSubstring(recordType.toString(), 373, 387));
/* 235 */     rejRecAirlineBean.setAirlineIndustry(airlineBean);
/* 236 */     rejectedTAAAirecord.setReturnedRecordData(rejRecAirlineBean);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void setTAALOCRecord(StringBuffer recordType, ConfirmationRejectedRecordBean rejectedLocRecord)
/*     */   {
/* 249 */     rejectedLocRecord.setReturnedaRecordType(recordSubstring(recordType.toString(), 0, 3));
/* 250 */     rejectedLocRecord.setReturnedRecordNumber(recordSubstring(recordType.toString(), 3, 11));
/* 251 */     RejectedRecordDataBean rejRecLocaBean = new RejectedRecordDataBean();
/* 252 */     LocationDetailTAABean locationTAABean = new LocationDetailTAABean();
/* 253 */     locationTAABean.setRecordType(recordSubstring(recordType.toString(), 0, 3));
/* 254 */     locationTAABean.setRecordNumber(recordSubstring(recordType.toString(), 3, 11));
/* 255 */     locationTAABean.setTransactionIdentifier(recordSubstring(recordType.toString(), 11, 26));
/* 256 */     locationTAABean.setAddendaTypeCode(recordSubstring(recordType.toString(), 28, 30));
/* 257 */     locationTAABean.setLocationName(recordSubstring(recordType.toString(), 30, 68));
/* 258 */     locationTAABean.setLocationAddress(recordSubstring(recordType.toString(), 68, 106));
/* 259 */     locationTAABean.setLocationCity(recordSubstring(recordType.toString(), 106, 127));
/* 260 */     locationTAABean.setLocationRegion(recordSubstring(recordType.toString(), 127, 130));
/* 261 */     locationTAABean.setLocationCountryCode(recordSubstring(recordType.toString(), 130, 133));
/* 262 */     locationTAABean.setLocationPostalCode(recordSubstring(recordType.toString(), 133, 148));
/* 263 */     locationTAABean.setMerchantCategoryCode(recordSubstring(recordType.toString(), 148, 152));
/* 264 */     locationTAABean.setSellerId(recordSubstring(recordType.toString(), 152, 172));
/* 265 */     rejRecLocaBean.setLocationDetailTAABean(locationTAABean);
/* 266 */     rejectedLocRecord.setReturnedRecordData(rejRecLocaBean);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void setTAACommRecord(StringBuffer recordType, ConfirmationRejectedRecordBean rejectedTAACOMMRecord)
/*     */   {
/* 280 */     rejectedTAACOMMRecord.setReturnedaRecordType(recordSubstring(recordType.toString(), 0, 3));
/* 281 */     rejectedTAACOMMRecord.setReturnedRecordNumber(recordSubstring(recordType.toString(), 3, 11));
/* 282 */     RejectedRecordDataBean rejRecTAACOMMBean = new RejectedRecordDataBean();
/*     */     
/* 284 */     CommunicationServicesIndustryBean communicationBean = new CommunicationServicesIndustryBean();
/* 285 */     communicationBean.setRecordType(recordSubstring(recordType.toString(), 0, 3));
/* 286 */     communicationBean.setRecordNumber(recordSubstring(recordType.toString(), 3, 11));
/* 287 */     communicationBean.setTransactionIdentifier(recordSubstring(recordType.toString(), 11, 26));
/* 288 */     communicationBean.setFormatCode(recordSubstring(recordType.toString(), 26, 28));
/* 289 */     communicationBean.setAddendaTypeCode(recordSubstring(recordType.toString(), 28, 30));
/* 290 */     communicationBean.setCallDate(recordSubstring(recordType.toString(), 30, 38));
/* 291 */     communicationBean.setCallTime(recordSubstring(recordType.toString(), 38, 44));
/* 292 */     communicationBean.setCallDurationTime(recordSubstring(recordType.toString(), 44, 50));
/* 293 */     communicationBean.setCallFromLocationName(recordSubstring(recordType.toString(), 50, 68));
/* 294 */     communicationBean.setCallFromRegionCode(recordSubstring(recordType.toString(), 68, 71));
/* 295 */     communicationBean.setCallFromCountryCode(recordSubstring(recordType.toString(), 71, 74));
/* 296 */     communicationBean.setCallFromPhoneNumber(recordSubstring(recordType.toString(), 74, 90));
/* 297 */     communicationBean.setCallToLocationName(recordSubstring(recordType.toString(), 90, 108));
/* 298 */     communicationBean.setCallToRegionCode(recordSubstring(recordType.toString(), 108, 111));
/* 299 */     communicationBean.setCallToCountryCode(recordSubstring(recordType.toString(), 111, 114));
/* 300 */     communicationBean.setCallToPhoneNumber(recordSubstring(recordType.toString(), 114, 130));
/* 301 */     communicationBean.setPhoneCardId(recordSubstring(recordType.toString(), 130, 138));
/* 302 */     communicationBean.setServiceDescription(recordSubstring(recordType.toString(), 138, 158));
/* 303 */     communicationBean.setBillingPeriod(recordSubstring(recordType.toString(), 158, 176));
/* 304 */     communicationBean.setCommunicationsCallTypeCode(recordSubstring(recordType.toString(), 176, 181));
/* 305 */     communicationBean.setCommunicationsRateClass(recordSubstring(recordType.toString(), 181, 182));
/*     */     
/* 307 */     rejRecTAACOMMBean.setCommunicationServicesIndustry(communicationBean);
/* 308 */     rejectedTAACOMMRecord.setReturnedRecordData(rejRecTAACOMMBean);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void setTAACPSL2Record(StringBuffer recordType, ConfirmationRejectedRecordBean rejectedTAACPSRecord)
/*     */   {
/* 321 */     rejectedTAACPSRecord.setReturnedaRecordType(recordSubstring(recordType.toString(), 0, 3));
/* 322 */     rejectedTAACPSRecord.setReturnedRecordNumber(recordSubstring(recordType.toString(), 3, 11));
/* 323 */     RejectedRecordDataBean rejRecTAACPSBean = new RejectedRecordDataBean();
/*     */     
/* 325 */     CPSLevel2Bean corporatePurchasingBean = new CPSLevel2Bean();
/* 326 */     corporatePurchasingBean.setRecordType(recordSubstring(recordType.toString(), 0, 3));
/* 327 */     corporatePurchasingBean.setRecordNumber(recordSubstring(recordType.toString(), 3, 11));
/* 328 */     corporatePurchasingBean.setTransactionIdentifier(recordSubstring(recordType.toString(), 11, 26));
/* 329 */     corporatePurchasingBean.setAddendaTypeCode(recordSubstring(recordType.toString(), 28, 30));
/* 330 */     corporatePurchasingBean.setRequesterName(recordSubstring(recordType.toString(), 30, 68));
/* 331 */     corporatePurchasingBean.setChargeDescription1(recordSubstring(recordType.toString(), 70, 110));
/* 332 */     corporatePurchasingBean.setChargeItemQuantity1(recordSubstring(recordType.toString(), 110, 113));
/* 333 */     corporatePurchasingBean.setChargeItemAmount1(recordSubstring(recordType.toString(), 116, 128));
/* 334 */     corporatePurchasingBean.setChargeDescription2(recordSubstring(recordType.toString(), 128, 168));
/* 335 */     corporatePurchasingBean.setChargeItemQuantity2(recordSubstring(recordType.toString(), 168, 171));
/* 336 */     corporatePurchasingBean.setChargeItemAmount2(recordSubstring(recordType.toString(), 174, 186));
/* 337 */     corporatePurchasingBean.setChargeDescription3(recordSubstring(recordType.toString(), 186, 226));
/* 338 */     corporatePurchasingBean.setChargeItemQuantity3(recordSubstring(recordType.toString(), 226, 229));
/* 339 */     corporatePurchasingBean.setChargeItemAmount3(recordSubstring(recordType.toString(), 233, 244));
/* 340 */     corporatePurchasingBean.setChargeDescription4(recordSubstring(recordType.toString(), 244, 284));
/* 341 */     corporatePurchasingBean.setChargeItemQuantity4(recordSubstring(recordType.toString(), 284, 287));
/* 342 */     corporatePurchasingBean.setChargeItemAmount4(recordSubstring(recordType.toString(), 290, 302));
/* 343 */     corporatePurchasingBean.setCardMemberReferenceNumber(recordSubstring(recordType.toString(), 302, 319));
/* 344 */     corporatePurchasingBean.setShipToPostalCode(recordSubstring(recordType.toString(), 322, 337));
/* 345 */     corporatePurchasingBean.setTotalTaxAmount(recordSubstring(recordType.toString(), 353, 365));
/* 346 */     corporatePurchasingBean.setTaxTypeCode(recordSubstring(recordType.toString(), 385, 388));
/* 347 */     rejRecTAACPSBean.setCpsLevel2Bean(corporatePurchasingBean);
/* 348 */     rejectedTAACPSRecord.setReturnedRecordData(rejRecTAACPSBean);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void setTAADPPRecord(StringBuffer recordType, ConfirmationRejectedRecordBean rejectedTAADPPRecord)
/*     */   {
/* 361 */     rejectedTAADPPRecord.setReturnedaRecordType(recordSubstring(recordType.toString(), 0, 3));
/* 362 */     rejectedTAADPPRecord.setReturnedRecordNumber(recordSubstring(recordType.toString(), 3, 11));
/* 363 */     RejectedRecordDataBean rejRecTAADPPBean = new RejectedRecordDataBean();
/*     */     
/* 365 */     DeferredPaymentPlanBean dppBean = new DeferredPaymentPlanBean();
/* 366 */     dppBean.setRecordType(recordSubstring(recordType.toString(), 0, 3));
/* 367 */     dppBean.setRecordNumber(recordSubstring(recordType.toString(), 3, 11));
/* 368 */     dppBean.setTransactionIdentifier(recordSubstring(recordType.toString(), 11, 26));
/* 369 */     dppBean.setAddendaTypeCode(recordSubstring(recordType.toString(), 28, 30));
/* 370 */     dppBean.setFullTransactionAmount(recordSubstring(recordType.toString(), 33, 45));
/* 371 */     dppBean.setTypeOfPlanCode(recordSubstring(recordType.toString(), 45, 49));
/* 372 */     dppBean.setNoOfInstallments(recordSubstring(recordType.toString(), 49, 53));
/* 373 */     dppBean.setAmountOfInstallment(recordSubstring(recordType.toString(), 56, 68));
/* 374 */     dppBean.setInstallmentNumber(recordSubstring(recordType.toString(), 68, 72));
/* 375 */     dppBean.setContractNumber(recordSubstring(recordType.toString(), 72, 86));
/* 376 */     dppBean.setPaymentTypeCode1(recordSubstring(recordType.toString(), 86, 88));
/* 377 */     dppBean.setPaymentTypeAmount1(recordSubstring(recordType.toString(), 91, 103));
/* 378 */     dppBean.setPaymentTypeCode2(recordSubstring(recordType.toString(), 119, 120));
/* 379 */     dppBean.setPaymentTypeAmount2(recordSubstring(recordType.toString(), 123, 135));
/* 380 */     dppBean.setPaymentTypeCode3(recordSubstring(recordType.toString(), 150, 152));
/* 381 */     dppBean.setPaymentTypeAmount3(recordSubstring(recordType.toString(), 155, 167));
/* 382 */     dppBean.setPaymentTypeCode4(recordSubstring(recordType.toString(), 182, 184));
/* 383 */     dppBean.setPaymentTypeAmount4(recordSubstring(recordType.toString(), 187, 199));
/* 384 */     dppBean.setPaymentTypeCode5(recordSubstring(recordType.toString(), 214, 216));
/* 385 */     dppBean.setPaymentTypeAmount5(recordSubstring(recordType.toString(), 219, 231));
/*     */     
/* 387 */     rejRecTAADPPBean.setDppNonIndustry(dppBean);
/* 388 */     rejectedTAADPPRecord.setReturnedRecordData(rejRecTAADPPBean);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void setTAAEMVRecord(StringBuffer recordType, ConfirmationRejectedRecordBean rejectedTAAEMVRecord)
/*     */   {
/* 400 */     rejectedTAAEMVRecord.setReturnedaRecordType(recordSubstring(recordType.toString(), 0, 3));
/* 401 */     rejectedTAAEMVRecord.setReturnedRecordNumber(recordSubstring(recordType.toString(), 3, 11));
/* 402 */     RejectedRecordDataBean rejRecTAADPPBean = new RejectedRecordDataBean();
/* 403 */     EMVBean emvBean = new EMVBean();
/* 404 */     emvBean.setRecordType(recordSubstring(recordType.toString(), 0, 3));
/* 405 */     emvBean.setRecordNumber(recordSubstring(recordType.toString(), 3, 11));
/* 406 */     emvBean.setTransactionIdentifier(recordSubstring(recordType.toString(), 11, 26));
/* 407 */     emvBean.setEMVFormatType(recordSubstring(recordType.toString(), 26, 28));
/* 408 */     emvBean.setAddendaTypeCode(recordSubstring(recordType.toString(), 28, 30));
/* 409 */     emvBean.setICCSystemRelatedData(recordSubstring(recordType.toString(), 30, 286));
/* 410 */     rejRecTAADPPBean.setEmvBean(emvBean);
/* 411 */     rejectedTAAEMVRecord.setReturnedRecordData(rejRecTAADPPBean);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void setTAAEntetainRecord(StringBuffer recordType, ConfirmationRejectedRecordBean rejectedTAAEntRecord)
/*     */   {
/* 424 */     rejectedTAAEntRecord.setReturnedaRecordType(recordSubstring(recordType.toString(), 0, 3));
/* 425 */     rejectedTAAEntRecord.setReturnedRecordNumber(recordSubstring(recordType.toString(), 3, 11));
/* 426 */     RejectedRecordDataBean rejRecTAAEntBean = new RejectedRecordDataBean();
/* 427 */     EntertainmentTicketingIndustryTAABean entertainmentBean = new EntertainmentTicketingIndustryTAABean();
/* 428 */     entertainmentBean.setRecordType(recordSubstring(recordType.toString(), 0, 3));
/* 429 */     entertainmentBean.setRecordNumber(recordSubstring(recordType.toString(), 3, 11));
/* 430 */     entertainmentBean.setTransactionIdentifier(recordSubstring(recordType.toString(), 11, 26));
/* 431 */     entertainmentBean.setFormatCode(recordSubstring(recordType.toString(), 26, 28));
/* 432 */     entertainmentBean.setAddendaTypeCode(recordSubstring(recordType.toString(), 28, 30));
/* 433 */     entertainmentBean.setEventName(recordSubstring(recordType.toString(), 30, 60));
/* 434 */     entertainmentBean.setEventDate(recordSubstring(recordType.toString(), 60, 68));
/* 435 */     entertainmentBean.setEventIndividualTicketPriceAmount(recordSubstring(recordType.toString(), 71, 83));
/* 436 */     entertainmentBean.setEventTicketQuantity(recordSubstring(recordType.toString(), 83, 87));
/* 437 */     entertainmentBean.setEventLocation(recordSubstring(recordType.toString(), 87, 127));
/* 438 */     entertainmentBean.setEventRegionCode(recordSubstring(recordType.toString(), 127, 130));
/* 439 */     entertainmentBean.setEventCountryCode(recordSubstring(recordType.toString(), 130, 133));
/* 440 */     rejRecTAAEntBean.setEntertainmentTicketingIndustry(entertainmentBean);
/* 441 */     rejectedTAAEntRecord.setReturnedRecordData(rejRecTAAEntBean);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void setTAAInsuRecord(StringBuffer recordType, ConfirmationRejectedRecordBean rejectedTAAInsuRecord)
/*     */   {
/* 454 */     rejectedTAAInsuRecord.setReturnedaRecordType(recordSubstring(recordType.toString(), 0, 3));
/* 455 */     rejectedTAAInsuRecord.setReturnedRecordNumber(recordSubstring(recordType.toString(), 3, 11));
/* 456 */     RejectedRecordDataBean rejRecTAAInsuBean = new RejectedRecordDataBean();
/*     */     
/* 458 */     InsuranceIndustryTAABean insuranceBean = new InsuranceIndustryTAABean();
/* 459 */     insuranceBean.setRecordType(recordSubstring(recordType.toString(), 0, 3));
/* 460 */     insuranceBean.setRecordNumber(recordSubstring(recordType.toString(), 3, 11));
/* 461 */     insuranceBean.setTransactionIdentifier(recordSubstring(recordType.toString(), 11, 26));
/* 462 */     insuranceBean.setFormatCode(recordSubstring(recordType.toString(), 26, 28));
/* 463 */     insuranceBean.setAddendaTypeCode(recordSubstring(recordType.toString(), 28, 30));
/* 464 */     insuranceBean.setInsurancePolicyNumber(recordSubstring(recordType.toString(), 30, 53));
/* 465 */     insuranceBean.setInsuranceCoverageStartDate(recordSubstring(recordType.toString(), 53, 61));
/* 466 */     insuranceBean.setInsuranceCoverageEndDate(recordSubstring(recordType.toString(), 61, 69));
/* 467 */     insuranceBean.setInsurancePolicyPremiumFrequency(recordSubstring(recordType.toString(), 69, 76));
/* 468 */     insuranceBean.setAdditionalInsurancePolicyNumber(recordSubstring(recordType.toString(), 76, 99));
/* 469 */     insuranceBean.setTypeOfPolicy(recordSubstring(recordType.toString(), 99, 124));
/* 470 */     insuranceBean.setNameOfInsured(recordSubstring(recordType.toString(), 124, 154));
/* 471 */     rejRecTAAInsuBean.setInsuranceIndustry(insuranceBean);
/* 472 */     rejectedTAAInsuRecord.setReturnedRecordData(rejRecTAAInsuBean);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void setTAALodgRecord(StringBuffer recordType, ConfirmationRejectedRecordBean rejectedTAALodRecord)
/*     */   {
/* 487 */     rejectedTAALodRecord.setReturnedaRecordType(recordSubstring(recordType.toString(), 0, 3));
/* 488 */     rejectedTAALodRecord.setReturnedRecordNumber(recordSubstring(recordType.toString(), 3, 11));
/* 489 */     RejectedRecordDataBean rejRecTAAInsuBean = new RejectedRecordDataBean();
/*     */     
/* 491 */     LodgingIndustryTAABean lodgingBean = new LodgingIndustryTAABean();
/* 492 */     lodgingBean.setRecordType(recordSubstring(recordType.toString(), 0, 3));
/* 493 */     lodgingBean.setRecordNumber(recordSubstring(recordType.toString(), 3, 11));
/* 494 */     lodgingBean.setTransactionIdentifier(recordSubstring(recordType.toString(), 11, 26));
/* 495 */     lodgingBean.setFormatCode(recordSubstring(recordType.toString(), 26, 28));
/* 496 */     lodgingBean.setAddendaTypeCode(recordSubstring(recordType.toString(), 28, 30));
/* 497 */     lodgingBean.setLodgingSpecialProgramCode(recordSubstring(recordType.toString(), 30, 31));
/* 498 */     lodgingBean.setLodgingCheckInDate(recordSubstring(recordType.toString(), 31, 39));
/* 499 */     lodgingBean.setLodgingCheckOutDate(recordSubstring(recordType.toString(), 39, 47));
/* 500 */     lodgingBean.setLodgingRoomRate1(recordSubstring(recordType.toString(), 50, 62));
/* 501 */     lodgingBean.setNumberOfNightsAtRoomRate1(recordSubstring(recordType.toString(), 62, 64));
/* 502 */     lodgingBean.setLodgingRoomRate2(recordSubstring(recordType.toString(), 67, 79));
/* 503 */     lodgingBean.setNumberOfNightsAtRoomRate2(recordSubstring(recordType.toString(), 79, 81));
/* 504 */     lodgingBean.setLodgingRoomRate3(recordSubstring(recordType.toString(), 84, 96));
/* 505 */     lodgingBean.setNumberOfNightsAtRoomRate3(recordSubstring(recordType.toString(), 96, 98));
/* 506 */     lodgingBean.setLodgingRenterName(recordSubstring(recordType.toString(), 98, 124));
/* 507 */     lodgingBean.setLodgingFolioNumber(recordSubstring(recordType.toString(), 124, 136));
/* 508 */     rejRecTAAInsuBean.setLodgingIndustry(lodgingBean);
/* 509 */     rejectedTAALodRecord.setReturnedRecordData(rejRecTAAInsuBean);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void setTAARailRecord(StringBuffer recordType, ConfirmationRejectedRecordBean rejectedTAARailRecord)
/*     */   {
/* 522 */     rejectedTAARailRecord.setReturnedaRecordType(recordSubstring(recordType.toString(), 0, 3));
/* 523 */     rejectedTAARailRecord.setReturnedRecordNumber(recordSubstring(recordType.toString(), 3, 11));
/* 524 */     RejectedRecordDataBean rejRecTAARailBean = new RejectedRecordDataBean();
/*     */     
/*     */ 
/* 527 */     RailIndustryTAABean railBean = new RailIndustryTAABean();
/* 528 */     railBean.setRecordType(recordSubstring(recordType.toString(), 0, 3));
/* 529 */     railBean.setRecordNumber(recordSubstring(recordType.toString(), 3, 11));
/* 530 */     railBean.setTransactionIdentifier(recordSubstring(recordType.toString(), 11, 26));
/* 531 */     railBean.setFormatCode(recordSubstring(recordType.toString(), 26, 28));
/* 532 */     railBean.setAddendaTypeCode(recordSubstring(recordType.toString(), 28, 30));
/* 533 */     railBean.setTransactionType(recordSubstring(recordType.toString(), 30, 33));
/* 534 */     railBean.setTicketNumber(recordSubstring(recordType.toString(), 33, 47));
/* 535 */     railBean.setPassengerName(recordSubstring(recordType.toString(), 47, 72));
/* 536 */     railBean.setIataCarrierCode(recordSubstring(recordType.toString(), 72, 75));
/* 537 */     railBean.setTicketIssuerName(recordSubstring(recordType.toString(), 75, 107));
/* 538 */     railBean.setTicketIssueCity(recordSubstring(recordType.toString(), 107, 125));
/* 539 */     railBean.setDepartureStation1(recordSubstring(recordType.toString(), 125, 128));
/* 540 */     railBean.setDepartureDate1(recordSubstring(recordType.toString(), 128, 136));
/* 541 */     railBean.setArrivalStation1(recordSubstring(recordType.toString(), 136, 139));
/* 542 */     railBean.setDepartureStation2(recordSubstring(recordType.toString(), 139, 142));
/* 543 */     railBean.setDepartureDate2(recordSubstring(recordType.toString(), 142, 150));
/* 544 */     railBean.setArrivalStation2(recordSubstring(recordType.toString(), 150, 153));
/* 545 */     railBean.setDepartureStation3(recordSubstring(recordType.toString(), 153, 156));
/* 546 */     railBean.setDepartureDate3(recordSubstring(recordType.toString(), 156, 164));
/* 547 */     railBean.setArrivalStation3(recordSubstring(recordType.toString(), 164, 167));
/* 548 */     railBean.setDepartureStation4(recordSubstring(recordType.toString(), 167, 170));
/* 549 */     railBean.setDepartureDate4(recordSubstring(recordType.toString(), 170, 178));
/* 550 */     railBean.setArrivalStation4(recordSubstring(recordType.toString(), 178, 181));
/* 551 */     rejRecTAARailBean.setRailIndustry(railBean);
/* 552 */     rejectedTAARailRecord.setReturnedRecordData(rejRecTAARailBean);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void setTAAReatilRecord(StringBuffer recordType, ConfirmationRejectedRecordBean rejectedTAAReatilRecord)
/*     */   {
/* 566 */     rejectedTAAReatilRecord.setReturnedaRecordType(recordSubstring(recordType.toString(), 0, 3));
/* 567 */     rejectedTAAReatilRecord.setReturnedRecordNumber(recordSubstring(recordType.toString(), 3, 11));
/* 568 */     RejectedRecordDataBean rejRecTAAReatilBean = new RejectedRecordDataBean();
/*     */     
/* 570 */     RetailIndustryTAABean retailBean = new RetailIndustryTAABean();
/* 571 */     retailBean.setRecordType(recordSubstring(recordType.toString(), 0, 3));
/* 572 */     retailBean.setRecordNumber(recordSubstring(recordType.toString(), 3, 11));
/* 573 */     retailBean.setTransactionIdentifier(recordSubstring(recordType.toString(), 11, 26));
/* 574 */     retailBean.setFormatCode(recordSubstring(recordType.toString(), 26, 28));
/* 575 */     retailBean.setAddendaTypeCode(recordSubstring(recordType.toString(), 28, 30));
/* 576 */     retailBean.setRetailDepartmentName(recordSubstring(recordType.toString(), 30, 70));
/*     */     
/* 578 */     retailBean.setRetailItemDescription1(recordSubstring(recordType.toString(), 70, 81));
/* 579 */     retailBean.setRetailItemQuantity1(recordSubstring(recordType.toString(), 81, 89));
/* 580 */     retailBean.setRetailItemAmount1(recordSubstring(recordType.toString(), 97, 107));
/* 581 */     retailBean.setRetailItemDescription2(recordSubstring(recordType.toString(), 107, 126));
/* 582 */     retailBean.setRetailItemQuantity2(recordSubstring(recordType.toString(), 126, 129));
/*     */     
/* 584 */     retailBean.setRetailItemAmount2(recordSubstring(recordType.toString(), 132, 144));
/* 585 */     retailBean.setRetailItemDescription3(recordSubstring(recordType.toString(), 144, 163));
/* 586 */     retailBean.setRetailItemQuantity3(recordSubstring(recordType.toString(), 163, 166));
/* 587 */     retailBean.setRetailItemAmount3(recordSubstring(recordType.toString(), 170, 181));
/* 588 */     retailBean.setRetailItemDescription4(recordSubstring(recordType.toString(), 181, 200));
/* 589 */     retailBean.setRetailItemQuantity4(recordSubstring(recordType.toString(), 200, 203));
/* 590 */     retailBean.setRetailItemAmount4(recordSubstring(recordType.toString(), 206, 218));
/* 591 */     retailBean.setRetailItemDescription5(recordSubstring(recordType.toString(), 218, 237));
/* 592 */     retailBean.setRetailItemQuantity5(recordSubstring(recordType.toString(), 237, 240));
/* 593 */     retailBean.setRetailItemAmount5(recordSubstring(recordType.toString(), 243, 255));
/* 594 */     retailBean.setRetailItemDescription6(recordSubstring(recordType.toString(), 255, 274));
/* 595 */     retailBean.setRetailItemQuantity6(recordSubstring(recordType.toString(), 274, 277));
/* 596 */     retailBean.setRetailItemAmount6(recordSubstring(recordType.toString(), 280, 292));
/* 597 */     rejRecTAAReatilBean.setRetailIndustry(retailBean);
/* 598 */     rejectedTAAReatilRecord.setReturnedRecordData(rejRecTAAReatilBean);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void setTAAAutoRecord(StringBuffer recordType, ConfirmationRejectedRecordBean rejectedTAAAutoRecord)
/*     */   {
/* 610 */     rejectedTAAAutoRecord.setReturnedaRecordType(recordSubstring(recordType.toString(), 0, 3));
/* 611 */     rejectedTAAAutoRecord.setReturnedRecordNumber(recordSubstring(recordType.toString(), 3, 11));
/* 612 */     RejectedRecordDataBean rejRecTAAAutoBean = new RejectedRecordDataBean();
/* 613 */     AutoRentalIndustryTAABean autoRentalBean = new AutoRentalIndustryTAABean();
/* 614 */     autoRentalBean.setRecordType(recordSubstring(recordType.toString(), 0, 3));
/* 615 */     autoRentalBean.setRecordNumber(recordSubstring(recordType.toString(), 3, 11));
/* 616 */     autoRentalBean.setTransactionIdentifier(recordSubstring(recordType.toString(), 11, 26));
/* 617 */     autoRentalBean.setFormatCode(recordSubstring(recordType.toString(), 26, 28));
/* 618 */     autoRentalBean.setAddendaTypeCode(recordSubstring(recordType.toString(), 28, 30));
/* 619 */     autoRentalBean.setAutoRentalAgreementNumber(recordSubstring(recordType.toString(), 30, 44));
/* 620 */     autoRentalBean.setAutoRentalPickupLocation(recordSubstring(recordType.toString(), 44, 82));
/* 621 */     autoRentalBean.setAutoRentalPickupCityName(recordSubstring(recordType.toString(), 82, 100));
/* 622 */     autoRentalBean.setAutoRentalPickupRegionCode(recordSubstring(recordType.toString(), 103, 106));
/* 623 */     autoRentalBean.setAutoRentalPickupCountryCode(recordSubstring(recordType.toString(), 106, 109));
/* 624 */     autoRentalBean.setAutoRentalPickupDate(recordSubstring(recordType.toString(), 109, 117));
/* 625 */     autoRentalBean.setAutoRentalPickupTime(recordSubstring(recordType.toString(), 117, 123));
/* 626 */     autoRentalBean.setAutoRentalReturnCityName(recordSubstring(recordType.toString(), 123, 141));
/* 627 */     autoRentalBean.setAutoRentalReturnRegionCode(recordSubstring(recordType.toString(), 144, 147));
/* 628 */     autoRentalBean.setAutoRentalReturnCountryCode(recordSubstring(recordType.toString(), 147, 150));
/* 629 */     autoRentalBean.setAutoRentalReturnDate(recordSubstring(recordType.toString(), 150, 158));
/* 630 */     autoRentalBean.setAutoRentalReturnTime(recordSubstring(recordType.toString(), 158, 164));
/* 631 */     autoRentalBean.setAutoRentalRenterName(recordSubstring(recordType.toString(), 164, 190));
/* 632 */     autoRentalBean.setAutoRentalVehicleClassId(recordSubstring(recordType.toString(), 190, 194));
/* 633 */     autoRentalBean.setAutoRentalDistance(recordSubstring(recordType.toString(), 194, 199));
/* 634 */     autoRentalBean.setAutoRentalDistanceUnitOfMeasure(recordSubstring(recordType.toString(), 199, 200));
/* 635 */     autoRentalBean.setAutoRentalAuditAdjustmentIndicator(recordSubstring(recordType.toString(), 200, 201));
/* 636 */     autoRentalBean.setAutoRentalAuditAdjustmentAmount(recordSubstring(recordType.toString(), 204, 216));
/*     */     
/* 638 */     rejRecTAAAutoBean.setAutoRentalIndustry(autoRentalBean);
/* 639 */     rejectedTAAAutoRecord.setReturnedRecordData(rejRecTAAAutoBean);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void setTAACruiesRecord(StringBuffer recordType, ConfirmationRejectedRecordBean rejectedTAATCruRecord)
/*     */   {
/* 653 */     rejectedTAATCruRecord.setReturnedaRecordType(recordSubstring(recordType.toString(), 0, 3));
/* 654 */     rejectedTAATCruRecord.setReturnedRecordNumber(recordSubstring(recordType.toString(), 3, 11));
/* 655 */     RejectedRecordDataBean rejRecTAATCrusBean = new RejectedRecordDataBean();
/* 656 */     TravelCruiseIndustryTAABean travelCruiseBean = new TravelCruiseIndustryTAABean();
/*     */     
/* 658 */     travelCruiseBean.setRecordType(recordSubstring(recordType.toString(), 0, 3));
/* 659 */     travelCruiseBean.setRecordNumber(recordSubstring(recordType.toString(), 3, 11));
/* 660 */     travelCruiseBean.setTransactionIdentifier(recordSubstring(recordType.toString(), 11, 26));
/* 661 */     travelCruiseBean.setFormatCode(recordSubstring(recordType.toString(), 26, 28));
/* 662 */     travelCruiseBean.setAddendaTypeCode(recordSubstring(recordType.toString(), 28, 30));
/* 663 */     travelCruiseBean.setIataCarrierCode(recordSubstring(recordType.toString(), 30, 33));
/* 664 */     travelCruiseBean.setIataAgencyNumber(recordSubstring(recordType.toString(), 33, 41));
/* 665 */     travelCruiseBean.setTravelPackageIndicator(recordSubstring(recordType.toString(), 41, 42));
/* 666 */     travelCruiseBean.setPassengerName(recordSubstring(recordType.toString(), 42, 67));
/* 667 */     travelCruiseBean.setTravelTicketNumber(recordSubstring(recordType.toString(), 67, 81));
/* 668 */     travelCruiseBean.setTravelDepartureDateSegment1(recordSubstring(recordType.toString(), 81, 89));
/* 669 */     travelCruiseBean.setTravelDestinationCodeSegment1(recordSubstring(recordType.toString(), 89, 92));
/* 670 */     travelCruiseBean.setTravelDepartureAirportSegment1(recordSubstring(recordType.toString(), 92, 95));
/* 671 */     travelCruiseBean.setAirCarrierCodeSegment1(recordSubstring(recordType.toString(), 95, 97));
/* 672 */     travelCruiseBean.setFlightNumberSegment1(recordSubstring(recordType.toString(), 97, 101));
/* 673 */     travelCruiseBean.setClassCodeSegment1(recordSubstring(recordType.toString(), 101, 102));
/* 674 */     travelCruiseBean.setTravelDepartureDateSegment2(recordSubstring(recordType.toString(), 102, 110));
/* 675 */     travelCruiseBean.setTravelDestinationCodeSegment2(recordSubstring(recordType.toString(), 110, 113));
/* 676 */     travelCruiseBean.setTravelDepartureAirportSegment2(recordSubstring(recordType.toString(), 113, 116));
/* 677 */     travelCruiseBean.setAirCarrierCodeSegment2(recordSubstring(recordType.toString(), 116, 118));
/* 678 */     travelCruiseBean.setFlightNumberSegment2(recordSubstring(recordType.toString(), 118, 122));
/* 679 */     travelCruiseBean.setClassCodeSegment2(recordSubstring(recordType.toString(), 122, 123));
/* 680 */     travelCruiseBean.setTravelDepartureDateSegment3(recordSubstring(recordType.toString(), 123, 131));
/* 681 */     travelCruiseBean.setTravelDestinationCodeSegment3(recordSubstring(recordType.toString(), 131, 134));
/* 682 */     travelCruiseBean.setTravelDepartureAirportSegment3(recordSubstring(recordType.toString(), 134, 137));
/* 683 */     travelCruiseBean.setAirCarrierCodeSegment3(recordSubstring(recordType.toString(), 137, 139));
/* 684 */     travelCruiseBean.setFlightNumberSegment3(recordSubstring(recordType.toString(), 139, 143));
/* 685 */     travelCruiseBean.setClassCodeSegment3(recordSubstring(recordType.toString(), 143, 144));
/* 686 */     travelCruiseBean.setTravelDepartureDateSegment4(recordSubstring(recordType.toString(), 144, 152));
/* 687 */     travelCruiseBean.setTravelDestinationCodeSegment4(recordSubstring(recordType.toString(), 152, 155));
/* 688 */     travelCruiseBean.setTravelDepartureAirportSegment4(recordSubstring(recordType.toString(), 155, 158));
/* 689 */     travelCruiseBean.setAirCarrierCodeSegment4(recordSubstring(recordType.toString(), 158, 160));
/* 690 */     travelCruiseBean.setFlightNumberSegment4(recordSubstring(recordType.toString(), 160, 164));
/* 691 */     travelCruiseBean.setClassCodeSegment4(recordSubstring(recordType.toString(), 164, 165));
/* 692 */     travelCruiseBean.setLodgingCheckInDate(recordSubstring(recordType.toString(), 165, 173));
/* 693 */     travelCruiseBean.setLodgingCheckOutDate(recordSubstring(recordType.toString(), 173, 181));
/* 694 */     travelCruiseBean.setLodgingRoomRate1(recordSubstring(recordType.toString(), 184, 196));
/* 695 */     travelCruiseBean.setNumberOfNightsAtRoomRate1(recordSubstring(recordType.toString(), 196, 198));
/* 696 */     travelCruiseBean.setLodgingName(recordSubstring(recordType.toString(), 198, 218));
/* 697 */     travelCruiseBean.setLodgingRegionCode(recordSubstring(recordType.toString(), 218, 221));
/* 698 */     travelCruiseBean.setLodgingCountryCode(recordSubstring(recordType.toString(), 221, 224));
/* 699 */     travelCruiseBean.setLodgingCityName(recordSubstring(recordType.toString(), 224, 242));
/* 700 */     rejRecTAATCrusBean.setTravelCruiseIndustry(travelCruiseBean);
/* 701 */     rejectedTAATCruRecord.setReturnedRecordData(rejRecTAATCrusBean);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static String recordSubstring(String s, int start, int end)
/*     */   {
/* 717 */     String s1 = s.substring(start, end);
/* 718 */     return s1.replaceAll("~", " ");
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\messagecreator\RejectionRecords.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */