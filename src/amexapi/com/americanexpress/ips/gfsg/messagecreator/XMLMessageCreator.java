/*      */ package com.americanexpress.ips.gfsg.messagecreator;
/*      */ 
/*      */ import com.americanexpress.ips.gfsg.beans.xml.batchadmin.BatchAdminRequestBean;
/*      */ import com.americanexpress.ips.gfsg.beans.xml.batchadmin.request.BatchAdminRequest;
/*      */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.AirlineSegInfoBean;
/*      */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.ChargeItemInfoBean;
/*      */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.DataCaptureRequestBean;
/*      */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.DataCaptureResponseBean;
/*      */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.InvoiceMassageInfoBean;
/*      */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.LodgeRoomInformationBean;
/*      */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.RailSegInfoBean;
/*      */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.RetailItemInfoBean;
/*      */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.TAAAirIndustryBean;
/*      */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.TAAAutoRentalIndustryBean;
/*      */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.TAACommunServIndustryBean;
/*      */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.TAACorpPurchSolLvl2Bean;
/*      */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.TAAEntertainmentTktIndustryBean;
/*      */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.TAAInsuranceIndustryBean;
/*      */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.TAALodgingIndustryBean;
/*      */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.TAARailIndustryBean;
/*      */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.TAARetailIndustryBean;
/*      */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.TAATravCruiseIndustryBean;
/*      */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.TravelSegInfoBean;
/*      */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.request.AirlineSegInfoType;
/*      */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.request.ChrgItemInfoType;
/*      */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.request.DataCaptureRequest;
/*      */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.request.DescriptionType;
/*      */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.request.InvoiceMsgInfoType;
/*      */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.request.LdgRoomInfoType;
/*      */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.request.RailSegInfoType;
/*      */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.request.RetailItemInfoType;
/*      */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.request.TAAAirIndusType;
/*      */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.request.TAAAutoRentalIndusType;
/*      */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.request.TAACommunServIndusType;
/*      */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.request.TAACorpPurchSolLvl2Type;
/*      */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.request.TAAEntTktIndusType;
/*      */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.request.TAAInsrIndusType;
/*      */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.request.TAALdgIndusType;
/*      */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.request.TAARailIndusType;
/*      */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.request.TAARetailIndusType;
/*      */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.request.TAATravCruiseIndusType;
/*      */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.request.TravPackTypeCdType;
/*      */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.request.TravTransTypeCdValueType;
/*      */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.request.TravelSegInfoType;
/*      */ import com.americanexpress.ips.gfsg.connection.PropertyReader;
/*      */ import com.americanexpress.ips.gfsg.exceptions.SettlementException;
/*      */ import com.americanexpress.ips.gfsg.utils.SettlementXMLConverter;
/*      */ import com.americanexpress.ips.gfsg.validator.CommonValidator;
/*      */ import java.io.StringWriter;
/*      */ import java.math.BigInteger;
/*      */ import java.util.Iterator;
/*      */ import java.util.List;
/*      */ import org.apache.log4j.Logger;
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ public class XMLMessageCreator
/*      */ {
/*   68 */   private static final Logger LOGGER = Logger.getLogger(XMLMessageCreator.class);
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static String createtXMLBatchAdminRequest(BatchAdminRequestBean batchAdminRequestBean)
/*      */     throws SettlementException
/*      */   {
/*   92 */     BatchAdminRequest batchAdmin = new com.americanexpress.ips.gfsg.beans.xml.batchadmin.request.ObjectFactory()
/*   93 */       .createBatchAdminRequest();
/*   94 */     batchAdmin.setBatchID(batchAdminRequestBean.getBatchId());
/*   95 */     batchAdmin.setBatchOperation(batchAdminRequestBean.getBatchOperation());
/*   96 */     batchAdmin.setMerId(batchAdminRequestBean.getMerId());
/*   97 */     batchAdmin.setMerTrmnlId(batchAdminRequestBean.getMerTrmnlId());
/*   98 */     if (!CommonValidator.isNullOrEmpty(batchAdminRequestBean
/*   99 */       .getReturnBatchSummary())) {
/*  100 */       batchAdmin.setReturnBatchSummary(
/*  101 */         batchAdminRequestBean.getReturnBatchSummary());
/*      */     }
/*  103 */     if (!CommonValidator.isNullOrEmpty(batchAdminRequestBean
/*  104 */       .getSubmitterCode())) {
/*  105 */       batchAdmin.setSubmitterCode(
/*  106 */         batchAdminRequestBean.getSubmitterCode());
/*      */     }
/*  108 */     batchAdmin.setVersion(batchAdminRequestBean.getVersion());
/*  109 */     if (!CommonValidator.isNullOrEmpty(batchAdminRequestBean
/*  110 */       .getBatchImageSeqNbr())) {
/*  111 */       batchAdmin.setBatchImageSeqNbr(
/*  112 */         batchAdminRequestBean.getBatchImageSeqNbr());
/*      */     }
/*  114 */     if (!CommonValidator.isNullOrEmpty(batchAdminRequestBean
/*  115 */       .getInvoiceFormatType())) {
/*  116 */       batchAdmin.setInvoiceFormatType(
/*  117 */         batchAdminRequestBean.getInvoiceFormatType());
/*      */     }
/*  119 */     if (!CommonValidator.isNullOrEmpty(batchAdminRequestBean
/*  120 */       .getServAgtMerId())) {
/*  121 */       batchAdmin.setServAgtMerId(batchAdminRequestBean.getServAgtMerId());
/*      */     }
/*  123 */     if (!CommonValidator.isNullOrEmpty(batchAdminRequestBean
/*  124 */       .getInvoiceNumber())) {
/*  125 */       batchAdmin.setInvoiceNumber(
/*  126 */         batchAdminRequestBean.getInvoiceNumber());
/*      */     }
/*  128 */     if (!CommonValidator.isNullOrEmpty(batchAdminRequestBean
/*  129 */       .getReturnInvoiceStatus())) {
/*  130 */       batchAdmin.setReturnInvoiceStatus(
/*  131 */         batchAdminRequestBean.getReturnInvoiceStatus());
/*      */     }
/*      */     
/*      */ 
/*  135 */     if (batchAdminRequestBean.getBatchOperation().equals("01")) {
/*  136 */       batchAdmin.setCardAcceptorDetail(new com.americanexpress.ips.gfsg.beans.xml.batchadmin.request.CardAcceptorDetail());
/*      */       
/*  138 */       batchAdmin.getCardAcceptorDetail().setCardAcptCityNm(
/*  139 */         batchAdminRequestBean.getCardAcceptorDetail()
/*  140 */         .getCardAcptCityNm());
/*  141 */       batchAdmin.getCardAcceptorDetail().setCardAcptCtryCd(
/*  142 */         batchAdminRequestBean.getCardAcceptorDetail()
/*  143 */         .getCardAcptCtryCd());
/*  144 */       batchAdmin.getCardAcceptorDetail().setCardAcptNm(
/*  145 */         batchAdminRequestBean.getCardAcceptorDetail()
/*  146 */         .getCardAcptNm());
/*  147 */       batchAdmin.getCardAcceptorDetail().setCardAcptPostCd(
/*  148 */         batchAdminRequestBean.getCardAcceptorDetail()
/*  149 */         .getCardAcptPostCd());
/*  150 */       batchAdmin.getCardAcceptorDetail().setCardAcptRgnCd(
/*  151 */         batchAdminRequestBean.getCardAcceptorDetail()
/*  152 */         .getCardAcptRegCd());
/*  153 */       batchAdmin.getCardAcceptorDetail().setCardAcptStreetNm(
/*  154 */         batchAdminRequestBean.getCardAcceptorDetail()
/*  155 */         .getCardAcptStreetNm());
/*      */     }
/*  157 */     String xmlMessage = 
/*  158 */       SettlementXMLConverter.createXMLMessage(batchAdmin, 
/*  159 */       "com.americanexpress.ips.gfsg.beans.xml.batchadmin.request");
/*  160 */     return xmlMessage;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static DataCaptureResponseBean createtXMLDataCaptureReqAndSend(DataCaptureRequestBean dataCaptureRequestBean)
/*      */     throws SettlementException
/*      */   {
/*  180 */     DataCaptureResponseBean dataCaptureResponseBean = null;
/*      */     
/*      */ 
/*  183 */     String xmlString = convertDataCaptureReqBeanToXML(dataCaptureRequestBean);
/*      */     
/*  185 */     return dataCaptureResponseBean;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static String convertDataCaptureReqBeanToXML(DataCaptureRequestBean dataCaptureRequestBean)
/*      */     throws SettlementException
/*      */   {
/*  224 */     StringWriter sw = new StringWriter();
/*  225 */     String XMLString = null;
/*      */     
/*      */ 
/*  228 */     DataCaptureRequest dataRequest = new com.americanexpress.ips.gfsg.beans.xml.datacapture.request.ObjectFactory()
/*  229 */       .createDataCaptureRequest();
/*      */     
/*      */ 
/*  232 */     if (PropertyReader.getDebuggerFlag().equalsIgnoreCase(
/*  233 */       "ON")) {
/*  234 */       LOGGER.info("DataCaptureRequest JXB is created :" + 
/*  235 */         dataCaptureRequestBean.getTransAddCd());
/*      */     }
/*      */     
/*      */ 
/*  239 */     if (dataCaptureRequestBean.getTransAddCd().equals("01"))
/*      */     {
/*      */ 
/*  242 */       dataRequest.setTAAAirIndus(convertAirlineIndustryToJAXBBean(dataCaptureRequestBean
/*  243 */         .getTaaAirIndustry()));
/*      */ 
/*      */     }
/*  246 */     else if (dataCaptureRequestBean.getTransAddCd().equals("04"))
/*      */     {
/*      */ 
/*  249 */       dataRequest.setTAAInsrIndus(convertInsuranceIndustryToJAXBBean(dataCaptureRequestBean
/*  250 */         .getTaaInsuranceIndustry()));
/*      */     }
/*  252 */     else if (dataCaptureRequestBean.getTransAddCd().equals("05"))
/*      */     {
/*      */ 
/*  255 */       dataRequest.setTAAAutoRentalIndus(convertAutoRentalIndustryToJAXBBean(dataCaptureRequestBean
/*  256 */         .getTaaAutoRentalIndustry()));
/*  257 */     } else if (dataCaptureRequestBean.getTransAddCd().equals("06"))
/*      */     {
/*      */ 
/*  260 */       dataRequest.setTAARailIndus(convertRailIndustryToJAXBBean(dataCaptureRequestBean
/*  261 */         .getTaaRailIndustry()));
/*  262 */     } else if (dataCaptureRequestBean.getTransAddCd().equals("11"))
/*      */     {
/*      */ 
/*  265 */       dataRequest.setTAALdgIndus(convertLodgingIndustryToJAXBBean(dataCaptureRequestBean
/*  266 */         .getTaaLodgingIndustry()));
/*  267 */     } else if (dataCaptureRequestBean.getTransAddCd().equals("13"))
/*      */     {
/*      */ 
/*  270 */       dataRequest.setTAACommunServIndus(convertCommunicationIndustryToJAXBBean(dataCaptureRequestBean
/*  271 */         .getTaaCommunServIndustry()));
/*  272 */     } else if (dataCaptureRequestBean.getTransAddCd().equals("14"))
/*      */     {
/*      */ 
/*  275 */       dataRequest.setTAATravCruiseIndus(convertTravelCruiseIndustryToJAXBBean(dataCaptureRequestBean
/*  276 */         .getTaaTravCruiseIndustry()));
/*  277 */     } else if (dataCaptureRequestBean.getTransAddCd().equals("20"))
/*      */     {
/*  279 */       dataRequest.setTAARetailIndus(convertRetailIndustryToJAXBBean(dataCaptureRequestBean
/*  280 */         .getTaaRetailIndustry()));
/*  281 */     } else if (dataCaptureRequestBean.getTransAddCd().equals("22"))
/*      */     {
/*  283 */       dataRequest.setTAAEntTktIndus(convertEntTktIndustryToJAXBBean(dataCaptureRequestBean
/*  284 */         .getTaaEntertainmentTktIndustry()));
/*      */ 
/*      */ 
/*      */ 
/*      */     }
/*  289 */     else if (dataCaptureRequestBean.getTaaCorpPurchSolLvl2() != null)
/*      */     {
/*      */ 
/*  292 */       dataRequest.setTAACorpPurchSolLvl2(convertCorpPurchSolToJAXBBean(dataCaptureRequestBean
/*  293 */         .getTaaCorpPurchSolLvl2()));
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*  298 */     if (dataCaptureRequestBean.getInvoiceMassageInfoBean() != null)
/*      */     {
/*      */ 
/*  301 */       dataRequest.setInvoiceMsgInfo(convertInvoiceMsgInfoToJAXBBean(dataCaptureRequestBean
/*  302 */         .getInvoiceMassageInfoBean()));
/*      */     }
/*      */     
/*  305 */     XMLString = 
/*  306 */       SettlementXMLConverter.convertDataCaptureReqBeanToJAXBSettlementType(
/*  307 */       dataCaptureRequestBean, dataRequest);
/*      */     
/*  309 */     return XMLString;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static TAAInsrIndusType convertInsuranceIndustryToJAXBBean(TAAInsuranceIndustryBean taaInsuranceIndustry)
/*      */   {
/*  326 */     TAAInsrIndusType insrIndusType = new com.americanexpress.ips.gfsg.beans.xml.datacapture.request.ObjectFactory()
/*  327 */       .createTAAInsrIndusType();
/*  328 */     if (!CommonValidator.isNullOrEmpty(taaInsuranceIndustry
/*  329 */       .getInsrPlcyNbr())) {
/*  330 */       insrIndusType.setInsrPlcyNbr(taaInsuranceIndustry.getInsrPlcyNbr());
/*      */     }
/*  332 */     if (!CommonValidator.isNullOrEmpty(taaInsuranceIndustry
/*  333 */       .getInsrCovStartDt())) {
/*  334 */       insrIndusType.setInsrCovStartDt(
/*  335 */         taaInsuranceIndustry.getInsrCovStartDt());
/*      */     }
/*  337 */     if (!CommonValidator.isNullOrEmpty(taaInsuranceIndustry
/*  338 */       .getInsrCovEndDt())) {
/*  339 */       insrIndusType.setInsrCovEndDt(
/*  340 */         taaInsuranceIndustry.getInsrCovEndDt());
/*      */     }
/*  342 */     if (!CommonValidator.isNullOrEmpty(taaInsuranceIndustry
/*  343 */       .getInsrPlcyPremFreqTxt())) {
/*  344 */       insrIndusType.setInsrPlcyPremFreqTxt(
/*  345 */         taaInsuranceIndustry.getInsrPlcyPremFreqTxt());
/*      */     }
/*  347 */     if (!CommonValidator.isNullOrEmpty(taaInsuranceIndustry
/*  348 */       .getAddInsrPlcyNo())) {
/*  349 */       insrIndusType.setAddInsrPlcyNo(
/*  350 */         taaInsuranceIndustry.getAddInsrPlcyNo());
/*      */     }
/*  352 */     if (!CommonValidator.isNullOrEmpty(taaInsuranceIndustry
/*  353 */       .getPlcyTypeTxt())) {
/*  354 */       insrIndusType.setPlcyTypeTxt(taaInsuranceIndustry.getPlcyTypeTxt());
/*      */     }
/*  356 */     if (!CommonValidator.isNullOrEmpty(taaInsuranceIndustry.getInsrNm())) {
/*  357 */       insrIndusType.setInsrNm(taaInsuranceIndustry.getInsrNm());
/*      */     }
/*  359 */     return insrIndusType;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static TAAAirIndusType convertAirlineIndustryToJAXBBean(TAAAirIndustryBean taaAirIndustry)
/*      */   {
/*  375 */     TAAAirIndusType airIndusType = new com.americanexpress.ips.gfsg.beans.xml.datacapture.request.ObjectFactory()
/*  376 */       .createTAAAirIndusType();
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*  381 */     if (!CommonValidator.isNullOrEmpty(taaAirIndustry.getTravTransTypeCd())) {
/*  382 */       airIndusType.setTravTransTypeCd(
/*  383 */         TravTransTypeCdValueType.fromValue(taaAirIndustry.getTravTransTypeCd()));
/*      */     }
/*      */     
/*  386 */     if (!CommonValidator.isNullOrEmpty(taaAirIndustry.getTktNbr())) {
/*  387 */       airIndusType.setTktNbr(taaAirIndustry.getTktNbr());
/*      */     }
/*  389 */     if (!CommonValidator.isNullOrEmpty(taaAirIndustry.getDocTypeCd())) {
/*  390 */       airIndusType.setDocTypeCd(taaAirIndustry.getDocTypeCd());
/*      */     }
/*  392 */     if (!CommonValidator.isNullOrEmpty(taaAirIndustry.getIATACarrierCd())) {
/*  393 */       airIndusType.setIATACarrierCd(taaAirIndustry.getIATACarrierCd());
/*      */     }
/*  395 */     if (!CommonValidator.isNullOrEmpty(taaAirIndustry.getIATAAgcyNbr())) {
/*  396 */       airIndusType.setIATAAgcyNbr(taaAirIndustry.getIATAAgcyNbr());
/*      */     }
/*  398 */     if (!CommonValidator.isNullOrEmpty(taaAirIndustry.getTktCarrierNm())) {
/*  399 */       airIndusType.setTktCarrierNm(taaAirIndustry.getTktCarrierNm());
/*      */     }
/*  401 */     if (!CommonValidator.isNullOrEmpty(taaAirIndustry.getTktIssCityNm())) {
/*  402 */       airIndusType.setTktIssCityNm(taaAirIndustry.getTktIssCityNm());
/*      */     }
/*  404 */     if (!CommonValidator.isNullOrEmpty(taaAirIndustry.getTktIssDt())) {
/*  405 */       airIndusType.setTktIssDt(taaAirIndustry.getTktIssDt());
/*      */     }
/*  407 */     if (!CommonValidator.isNullOrEmpty(taaAirIndustry.getPassCnt())) {
/*  408 */       airIndusType.setPassCnt(taaAirIndustry.getPassCnt());
/*      */     }
/*  410 */     if (!CommonValidator.isNullOrEmpty(taaAirIndustry.getPassNm())) {
/*  411 */       airIndusType.setPassNm(taaAirIndustry.getPassNm());
/*      */     }
/*  413 */     if (!CommonValidator.isNullOrEmpty(taaAirIndustry.getConjTktInd())) {
/*  414 */       airIndusType.setConjTktInd(taaAirIndustry.getConjTktInd());
/*      */     }
/*  416 */     if (!CommonValidator.isNullOrEmpty(taaAirIndustry.getElecTktInd())) {
/*  417 */       airIndusType.setElecTktInd(taaAirIndustry.getElecTktInd());
/*      */     }
/*  419 */     if (!CommonValidator.isNullOrEmpty(taaAirIndustry.getExchTktNbr())) {
/*  420 */       airIndusType.setExchTktNbr(taaAirIndustry.getExchTktNbr());
/*      */     }
/*      */     
/*  423 */     if ((taaAirIndustry.getAirlineSegInfo() != null) && 
/*  424 */       (!taaAirIndustry.getAirlineSegInfo().isEmpty()))
/*      */     {
/*  426 */       Iterator localIterator = taaAirIndustry.getAirlineSegInfo().iterator();
/*  425 */       while (localIterator.hasNext()) {
/*  426 */         AirlineSegInfoBean airlineSegInfoBean = (AirlineSegInfoBean)localIterator.next();
/*  427 */         AirlineSegInfoType airSegInfo = new com.americanexpress.ips.gfsg.beans.xml.datacapture.request.ObjectFactory()
/*  428 */           .createAirlineSegInfoType();
/*      */         
/*  430 */         if (!CommonValidator.isNullOrEmpty(airlineSegInfoBean
/*  431 */           .getStopoverCd()))
/*      */         {
/*  433 */           airSegInfo.setStopoverCd(airlineSegInfoBean.getStopoverCd());
/*      */         }
/*  435 */         if (!CommonValidator.isNullOrEmpty(airlineSegInfoBean
/*  436 */           .getDprtLocCd())) {
/*  437 */           airSegInfo.setDprtLocCd(airlineSegInfoBean.getDprtLocCd());
/*      */         }
/*  439 */         if (!CommonValidator.isNullOrEmpty(airlineSegInfoBean
/*  440 */           .getDprtDt())) {
/*  441 */           airSegInfo.setDprtDt(airlineSegInfoBean.getDprtDt());
/*      */         }
/*  443 */         if (!CommonValidator.isNullOrEmpty(airlineSegInfoBean
/*  444 */           .getArrLocCd())) {
/*  445 */           airSegInfo.setArrLocCd(airlineSegInfoBean.getArrLocCd());
/*      */         }
/*  447 */         if (!CommonValidator.isNullOrEmpty(airlineSegInfoBean
/*  448 */           .getSegIATACarrierCd())) {
/*  449 */           airSegInfo.setSegIATACarrierCd(
/*  450 */             airlineSegInfoBean.getSegIATACarrierCd());
/*      */         }
/*  452 */         if (!CommonValidator.isNullOrEmpty(airlineSegInfoBean
/*  453 */           .getFareBasisTxt())) {
/*  454 */           airSegInfo.setFareBasisTxt(
/*  455 */             airlineSegInfoBean.getFareBasisTxt());
/*      */         }
/*  457 */         if (!CommonValidator.isNullOrEmpty(airlineSegInfoBean
/*  458 */           .getServClassCd())) {
/*  459 */           airSegInfo.setServClassCd(
/*  460 */             airlineSegInfoBean.getServClassCd());
/*      */         }
/*  462 */         if (!CommonValidator.isNullOrEmpty(airlineSegInfoBean
/*  463 */           .getFlghtNbr())) {
/*  464 */           airSegInfo.setFlghtNbr(airlineSegInfoBean.getFlghtNbr());
/*      */         }
/*  466 */         if (!CommonValidator.isNullOrEmpty(airlineSegInfoBean
/*  467 */           .getSegTotFareAmt())) {
/*  468 */           airSegInfo.setSegTotFareAmt(
/*  469 */             airlineSegInfoBean.getSegTotFareAmt());
/*      */         }
/*  471 */         airIndusType.getAirlineSegInfo().add(airSegInfo);
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  487 */     if (!CommonValidator.isNullOrEmpty(taaAirIndustry.getOrigTransAmt())) {
/*  488 */       airIndusType.setOrigTransAmt(taaAirIndustry.getOrigTransAmt());
/*      */     }
/*  490 */     if (!CommonValidator.isNullOrEmpty(taaAirIndustry.getOrigCurrCd())) {
/*  491 */       airIndusType.setOrigCurrCd(taaAirIndustry.getOrigCurrCd());
/*      */     }
/*      */     
/*  494 */     return airIndusType;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static TAAAutoRentalIndusType convertAutoRentalIndustryToJAXBBean(TAAAutoRentalIndustryBean taaAutoRentalIndustry)
/*      */   {
/*  510 */     TAAAutoRentalIndusType autoRentalIndus = new com.americanexpress.ips.gfsg.beans.xml.datacapture.request.ObjectFactory()
/*  511 */       .createTAAAutoRentalIndusType();
/*  512 */     if (!CommonValidator.isNullOrEmpty(taaAutoRentalIndustry.getAgrNbr())) {
/*  513 */       autoRentalIndus.setAgrNbr(taaAutoRentalIndustry.getAgrNbr());
/*      */     }
/*  515 */     if (!CommonValidator.isNullOrEmpty(taaAutoRentalIndustry
/*  516 */       .getPickupLocNm())) {
/*  517 */       autoRentalIndus.setPickupLocNm(
/*  518 */         taaAutoRentalIndustry.getPickupLocNm());
/*      */     }
/*  520 */     if (!CommonValidator.isNullOrEmpty(taaAutoRentalIndustry
/*  521 */       .getPickupCityNm())) {
/*  522 */       autoRentalIndus.setPickupCityNm(
/*  523 */         taaAutoRentalIndustry.getPickupCityNm());
/*      */     }
/*  525 */     if (!CommonValidator.isNullOrEmpty(taaAutoRentalIndustry
/*  526 */       .getPickupRgnCd())) {
/*  527 */       autoRentalIndus.setPickupRgnCd(
/*  528 */         taaAutoRentalIndustry.getPickupRgnCd());
/*      */     }
/*  530 */     if (!CommonValidator.isNullOrEmpty(taaAutoRentalIndustry
/*  531 */       .getPickupCtryCd()))
/*      */     {
/*      */ 
/*  534 */       autoRentalIndus.setPickupCtryCd(
/*  535 */         taaAutoRentalIndustry.getPickupCtryCd());
/*      */     }
/*  537 */     if (!CommonValidator.isNullOrEmpty(taaAutoRentalIndustry.getPickupDt())) {
/*  538 */       autoRentalIndus.setPickupDt(taaAutoRentalIndustry.getPickupDt());
/*      */     }
/*  540 */     if (!CommonValidator.isNullOrEmpty(taaAutoRentalIndustry.getPickupTm())) {
/*  541 */       autoRentalIndus.setPickupTm(taaAutoRentalIndustry.getPickupTm());
/*      */     }
/*  543 */     if (!CommonValidator.isNullOrEmpty(taaAutoRentalIndustry
/*  544 */       .getRtrnCityNm()))
/*      */     {
/*  546 */       autoRentalIndus.setRtrnCityNm(taaAutoRentalIndustry.getRtrnCityNm());
/*      */     }
/*      */     
/*  549 */     if (!CommonValidator.isNullOrEmpty(taaAutoRentalIndustry.getRtrnRgnCd())) {
/*  550 */       autoRentalIndus.setRtrnRgnCd(taaAutoRentalIndustry.getRtrnRgnCd());
/*      */     }
/*  552 */     if (!CommonValidator.isNullOrEmpty(taaAutoRentalIndustry
/*  553 */       .getRtrnCtryCd()))
/*      */     {
/*      */ 
/*      */ 
/*  557 */       autoRentalIndus.setRtrnCtryCd(taaAutoRentalIndustry.getRtrnCtryCd());
/*      */     }
/*  559 */     if (!CommonValidator.isNullOrEmpty(taaAutoRentalIndustry.getRtrnDt())) {
/*  560 */       autoRentalIndus.setRtrnDt(taaAutoRentalIndustry.getRtrnDt());
/*      */     }
/*  562 */     if (!CommonValidator.isNullOrEmpty(taaAutoRentalIndustry.getRenterNm())) {
/*  563 */       autoRentalIndus.setRenterNm(taaAutoRentalIndustry.getRenterNm());
/*      */     }
/*  565 */     if (!CommonValidator.isNullOrEmpty(taaAutoRentalIndustry
/*  566 */       .getVehClassId()))
/*      */     {
/*  568 */       autoRentalIndus.setVehClassId(taaAutoRentalIndustry.getVehClassId());
/*      */     }
/*  570 */     if (!CommonValidator.isNullOrEmpty(taaAutoRentalIndustry
/*  571 */       .getTravDisCnt()))
/*      */     {
/*  573 */       autoRentalIndus.setTravDisCnt(taaAutoRentalIndustry.getTravDisCnt());
/*      */     }
/*  575 */     if (!CommonValidator.isNullOrEmpty(taaAutoRentalIndustry
/*  576 */       .getTravDisUnit())) {
/*  577 */       autoRentalIndus.setTravDisUnit(
/*  578 */         taaAutoRentalIndustry.getTravDisUnit());
/*      */     }
/*  580 */     if (!CommonValidator.isNullOrEmpty(taaAutoRentalIndustry
/*  581 */       .getAuditAdjInd()))
/*      */     {
/*  583 */       autoRentalIndus.setAuditAdjInd(
/*  584 */         taaAutoRentalIndustry.getAuditAdjInd());
/*      */     }
/*      */     
/*  587 */     if (!CommonValidator.isNullOrEmpty(taaAutoRentalIndustry
/*  588 */       .getAuditAdjAmt())) {
/*  589 */       autoRentalIndus.setAuditAdjAmt(
/*  590 */         taaAutoRentalIndustry.getAuditAdjAmt());
/*      */     }
/*  592 */     if (!CommonValidator.isNullOrEmpty(taaAutoRentalIndustry.getRtrnLocNm())) {
/*  593 */       autoRentalIndus.setRtrnLocNm(taaAutoRentalIndustry.getRtrnLocNm());
/*      */     }
/*  595 */     if (!CommonValidator.isNullOrEmpty(taaAutoRentalIndustry.getVehIdNbr())) {
/*  596 */       autoRentalIndus.setVehIdNbr(taaAutoRentalIndustry.getVehIdNbr());
/*      */     }
/*  598 */     if (!CommonValidator.isNullOrEmpty(taaAutoRentalIndustry.getDrvIdNbr())) {
/*  599 */       autoRentalIndus.setDrvIdNbr(taaAutoRentalIndustry.getDrvIdNbr());
/*      */     }
/*  601 */     if (!CommonValidator.isNullOrEmpty(taaAutoRentalIndustry.getDrvTaxIdNbr())) {
/*  602 */       autoRentalIndus.setDrvTaxIdNbr(taaAutoRentalIndustry.getDrvTaxIdNbr());
/*      */     }
/*      */     
/*      */ 
/*  606 */     return autoRentalIndus;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static TAACommunServIndusType convertCommunicationIndustryToJAXBBean(TAACommunServIndustryBean taaCommServIndustryBean)
/*      */   {
/*  622 */     TAACommunServIndusType communServIndus = new com.americanexpress.ips.gfsg.beans.xml.datacapture.request.ObjectFactory()
/*  623 */       .createTAACommunServIndusType();
/*  624 */     if (!CommonValidator.isNullOrEmpty(taaCommServIndustryBean.getCallDt())) {
/*  625 */       communServIndus.setCallDt(taaCommServIndustryBean.getCallDt());
/*      */     }
/*  627 */     if (!CommonValidator.isNullOrEmpty(taaCommServIndustryBean.getCallTm())) {
/*  628 */       communServIndus.setCallTm(taaCommServIndustryBean.getCallTm());
/*      */     }
/*  630 */     if (!CommonValidator.isNullOrEmpty(taaCommServIndustryBean
/*  631 */       .getCallDurTm()))
/*      */     {
/*  633 */       communServIndus.setCallDurTm(taaCommServIndustryBean.getCallDurTm());
/*      */     }
/*  635 */     if (!CommonValidator.isNullOrEmpty(taaCommServIndustryBean
/*  636 */       .getCallFromLocNm())) {
/*  637 */       communServIndus.setCallFromLocNm(
/*  638 */         taaCommServIndustryBean.getCallFromLocNm());
/*      */     }
/*  640 */     if (!CommonValidator.isNullOrEmpty(taaCommServIndustryBean
/*  641 */       .getCallFromRgnCd())) {
/*  642 */       communServIndus.setCallFromRgnCd(
/*  643 */         taaCommServIndustryBean.getCallFromRgnCd());
/*      */     }
/*  645 */     if (!CommonValidator.isNullOrEmpty(taaCommServIndustryBean
/*  646 */       .getCallFromCtryCd())) {
/*  647 */       communServIndus.setCallFromCtryCd(
/*  648 */         taaCommServIndustryBean.getCallFromCtryCd());
/*      */     }
/*  650 */     if (!CommonValidator.isNullOrEmpty(taaCommServIndustryBean
/*  651 */       .getCallFromPhoneNbr())) {
/*  652 */       communServIndus.setCallFromPhoneNbr(
/*  653 */         taaCommServIndustryBean.getCallFromPhoneNbr());
/*      */     }
/*  655 */     if (!CommonValidator.isNullOrEmpty(taaCommServIndustryBean
/*  656 */       .getCallToLocNm())) {
/*  657 */       communServIndus.setCallToLocNm(
/*  658 */         taaCommServIndustryBean.getCallToLocNm());
/*      */     }
/*  660 */     if (!CommonValidator.isNullOrEmpty(taaCommServIndustryBean
/*  661 */       .getCallToRgnCd())) {
/*  662 */       communServIndus.setCallToRgnCd(
/*  663 */         taaCommServIndustryBean.getCallToRgnCd());
/*      */     }
/*  665 */     if (!CommonValidator.isNullOrEmpty(taaCommServIndustryBean
/*  666 */       .getCallToCtryCd())) {
/*  667 */       communServIndus.setCallToCtryCd(
/*  668 */         taaCommServIndustryBean.getCallToCtryCd());
/*      */     }
/*  670 */     if (!CommonValidator.isNullOrEmpty(taaCommServIndustryBean
/*  671 */       .getCallToPhoneNbr())) {
/*  672 */       communServIndus.setCallToPhoneNbr(
/*  673 */         taaCommServIndustryBean.getCallToPhoneNbr());
/*      */     }
/*  675 */     if (!CommonValidator.isNullOrEmpty(taaCommServIndustryBean
/*  676 */       .getCallingCardId())) {
/*  677 */       communServIndus.setCallingCardId(
/*  678 */         taaCommServIndustryBean.getCallingCardId());
/*      */     }
/*  680 */     if (!CommonValidator.isNullOrEmpty(taaCommServIndustryBean
/*  681 */       .getServTypeDescTxt())) {
/*  682 */       communServIndus.setServTypeDescTxt(
/*  683 */         taaCommServIndustryBean.getServTypeDescTxt());
/*      */     }
/*  685 */     if (!CommonValidator.isNullOrEmpty(taaCommServIndustryBean
/*  686 */       .getBillPerTxt())) {
/*  687 */       communServIndus.setBillPerTxt(
/*  688 */         taaCommServIndustryBean.getBillPerTxt());
/*      */     }
/*  690 */     if (!CommonValidator.isNullOrEmpty(taaCommServIndustryBean
/*  691 */       .getCallTypeCd())) {
/*  692 */       communServIndus.setCallTypeCd(
/*  693 */         taaCommServIndustryBean.getCallTypeCd());
/*      */     }
/*  695 */     if (!CommonValidator.isNullOrEmpty(taaCommServIndustryBean
/*  696 */       .getRateClassCd())) {
/*  697 */       communServIndus.setRateClassCd(
/*  698 */         taaCommServIndustryBean.getRateClassCd());
/*      */     }
/*      */     
/*  701 */     return communServIndus;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static TAARailIndusType convertRailIndustryToJAXBBean(TAARailIndustryBean taaRailIndustryBean)
/*      */   {
/*  717 */     TAARailIndusType railIndus = new com.americanexpress.ips.gfsg.beans.xml.datacapture.request.ObjectFactory()
/*  718 */       .createTAARailIndusType();
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  724 */     railIndus.setTravTransTypeCd(
/*  725 */       TravTransTypeCdValueType.fromValue(taaRailIndustryBean.getTravTransTypeCd()));
/*  726 */     if (!CommonValidator.isNullOrEmpty(taaRailIndustryBean.getTktNbr())) {
/*  727 */       railIndus.setTktNbr(taaRailIndustryBean.getTktNbr());
/*      */     }
/*  729 */     if (!CommonValidator.isNullOrEmpty(taaRailIndustryBean.getPassNm())) {
/*  730 */       railIndus.setPassNm(taaRailIndustryBean.getPassNm());
/*      */     }
/*  732 */     if (!CommonValidator.isNullOrEmpty(taaRailIndustryBean
/*  733 */       .getIATACarrierCd()))
/*      */     {
/*  735 */       railIndus.setIATACarrierCd(taaRailIndustryBean.getIATACarrierCd());
/*      */     }
/*  737 */     if (!CommonValidator.isNullOrEmpty(taaRailIndustryBean.getTktIssNm())) {
/*  738 */       railIndus.setTktIssNm(taaRailIndustryBean.getTktIssNm());
/*      */     }
/*  740 */     if (!CommonValidator.isNullOrEmpty(taaRailIndustryBean
/*  741 */       .getTktIssCityNm())) {
/*  742 */       railIndus.setTktIssCityNm(taaRailIndustryBean.getTktIssCityNm());
/*      */     }
/*      */     
/*      */ 
/*  746 */     if ((taaRailIndustryBean.getRailSegInfoBean() != null) && 
/*  747 */       (!taaRailIndustryBean.getRailSegInfoBean().isEmpty()))
/*      */     {
/*  749 */       Iterator localIterator = taaRailIndustryBean.getRailSegInfoBean().iterator();
/*  748 */       while (localIterator.hasNext()) {
/*  749 */         RailSegInfoBean railSegInfoType = (RailSegInfoBean)localIterator.next();
/*  750 */         RailSegInfoType railSegInfo = new com.americanexpress.ips.gfsg.beans.xml.datacapture.request.ObjectFactory()
/*  751 */           .createRailSegInfoType();
/*      */         
/*  753 */         if (!CommonValidator.isNullOrEmpty(railSegInfoType
/*  754 */           .getArrStnCd())) {
/*  755 */           railSegInfo.setArrStnCd(railSegInfoType.getArrStnCd());
/*      */         }
/*      */         
/*  758 */         if (!CommonValidator.isNullOrEmpty(railSegInfoType.getDprtDt())) {
/*  759 */           railSegInfo.setDprtDt(railSegInfoType.getDprtDt());
/*      */         }
/*  761 */         if (!CommonValidator.isNullOrEmpty(railSegInfoType
/*  762 */           .getDprtStnCd())) {
/*  763 */           railSegInfo.setDprtStnCd(railSegInfoType.getDprtStnCd());
/*      */         }
/*  765 */         railIndus.getRailSegInfo().add(railSegInfo);
/*      */       }
/*      */     }
/*      */     
/*  769 */     return railIndus;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static TAALdgIndusType convertLodgingIndustryToJAXBBean(TAALodgingIndustryBean taaLodgingIndustryBean)
/*      */   {
/*  785 */     TAALdgIndusType LdgIndus = new com.americanexpress.ips.gfsg.beans.xml.datacapture.request.ObjectFactory()
/*  786 */       .createTAALdgIndusType();
/*      */     
/*  788 */     if (!CommonValidator.isNullOrEmpty(taaLodgingIndustryBean
/*  789 */       .getLdgSpecProgCd()))
/*      */     {
/*  791 */       LdgIndus.setLdgSpecProgCd(taaLodgingIndustryBean.getLdgSpecProgCd());
/*      */     }
/*  793 */     if (!CommonValidator.isNullOrEmpty(taaLodgingIndustryBean
/*  794 */       .getLdgCheckInDt())) {
/*  795 */       LdgIndus.setLdgCheckInDt(taaLodgingIndustryBean.getLdgCheckInDt());
/*      */     }
/*  797 */     if (!CommonValidator.isNullOrEmpty(taaLodgingIndustryBean
/*  798 */       .getLdgCheckOutDt()))
/*      */     {
/*  800 */       LdgIndus.setLdgCheckOutDt(taaLodgingIndustryBean.getLdgCheckOutDt());
/*      */     }
/*  802 */     if (!CommonValidator.isNullOrEmpty(taaLodgingIndustryBean
/*  803 */       .getLdgRentNm())) {
/*  804 */       LdgIndus.setLdgRentNm(taaLodgingIndustryBean.getLdgRentNm());
/*      */     }
/*  806 */     if (!CommonValidator.isNullOrEmpty(taaLodgingIndustryBean
/*  807 */       .getLdgFolioNbr())) {
/*  808 */       LdgIndus.setLdgFolioNbr(taaLodgingIndustryBean.getLdgFolioNbr());
/*      */     }
/*      */     
/*  811 */     if ((taaLodgingIndustryBean.getLodgeRoomInformationBean() != null) && 
/*  812 */       (!taaLodgingIndustryBean.getLodgeRoomInformationBean().isEmpty()))
/*      */     {
/*  814 */       Iterator localIterator = taaLodgingIndustryBean.getLodgeRoomInformationBean().iterator();
/*  813 */       while (localIterator.hasNext()) {
/*  814 */         LodgeRoomInformationBean ldgRoomInfoBean = (LodgeRoomInformationBean)localIterator.next();
/*  815 */         LdgRoomInfoType ldgRommInfo = new com.americanexpress.ips.gfsg.beans.xml.datacapture.request.ObjectFactory()
/*  816 */           .createLdgRoomInfoType();
/*      */         
/*  818 */         if (!CommonValidator.isNullOrEmpty(ldgRoomInfoBean
/*  819 */           .getLdgRmRate())) {
/*  820 */           ldgRommInfo.setLdgRmRate(ldgRoomInfoBean.getLdgRmRate());
/*      */         }
/*  822 */         if (!CommonValidator.isNullOrEmpty(ldgRoomInfoBean
/*  823 */           .getNbrOfNgtCnt()))
/*      */         {
/*  825 */           ldgRommInfo.setNbrOfNgtCnt(ldgRoomInfoBean.getNbrOfNgtCnt());
/*      */         }
/*  827 */         LdgIndus.getLdgRoomInfo().add(ldgRommInfo);
/*      */       }
/*      */     }
/*      */     
/*  831 */     return LdgIndus;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static TAATravCruiseIndusType convertTravelCruiseIndustryToJAXBBean(TAATravCruiseIndustryBean taaTravCruiseIndustryBean)
/*      */   {
/*  847 */     TAATravCruiseIndusType travCruiseIndus = new com.americanexpress.ips.gfsg.beans.xml.datacapture.request.ObjectFactory()
/*  848 */       .createTAATravCruiseIndusType();
/*  849 */     if (!CommonValidator.isNullOrEmpty(taaTravCruiseIndustryBean
/*  850 */       .getIATACarrierCd())) {
/*  851 */       travCruiseIndus.setIATACarrierCd(
/*  852 */         taaTravCruiseIndustryBean.getIATACarrierCd());
/*      */     }
/*  854 */     if (!CommonValidator.isNullOrEmpty(taaTravCruiseIndustryBean
/*  855 */       .getIATAAgcyNbr())) {
/*  856 */       travCruiseIndus.setIATAAgcyNbr(
/*  857 */         taaTravCruiseIndustryBean.getIATAAgcyNbr());
/*      */     }
/*      */     
/*  860 */     if (!CommonValidator.isNullOrEmpty(taaTravCruiseIndustryBean
/*  861 */       .getTravPackTypeCd())) {
/*  862 */       travCruiseIndus.setTravPackTypeCd(
/*  863 */         TravPackTypeCdType.fromValue(taaTravCruiseIndustryBean.getTravPackTypeCd()));
/*      */     }
/*  865 */     if (!CommonValidator.isNullOrEmpty(taaTravCruiseIndustryBean
/*  866 */       .getPassNm())) {
/*  867 */       travCruiseIndus.setPassNm(taaTravCruiseIndustryBean.getPassNm());
/*      */     }
/*  869 */     if (!CommonValidator.isNullOrEmpty(taaTravCruiseIndustryBean
/*  870 */       .getLdgCheckInDt())) {
/*  871 */       travCruiseIndus.setLdgCheckInDt(
/*  872 */         taaTravCruiseIndustryBean.getLdgCheckInDt());
/*      */     }
/*  874 */     if (!CommonValidator.isNullOrEmpty(taaTravCruiseIndustryBean
/*  875 */       .getLdgCheckOutDt())) {
/*  876 */       travCruiseIndus.setLdgCheckOutDt(
/*  877 */         taaTravCruiseIndustryBean.getLdgCheckOutDt());
/*      */     }
/*  879 */     if (!CommonValidator.isNullOrEmpty(taaTravCruiseIndustryBean
/*  880 */       .getLdgRmRate())) {
/*  881 */       travCruiseIndus.setLdgRmRate(
/*  882 */         taaTravCruiseIndustryBean.getLdgRmRate());
/*      */     }
/*  884 */     if (!CommonValidator.isNullOrEmpty(taaTravCruiseIndustryBean
/*  885 */       .getNbrOfNgtCnt())) {
/*  886 */       travCruiseIndus.setNbrOfNgtCnt(
/*  887 */         taaTravCruiseIndustryBean.getNbrOfNgtCnt());
/*      */     }
/*      */     
/*  890 */     if (!CommonValidator.isNullOrEmpty(taaTravCruiseIndustryBean.getLdgNm())) {
/*  891 */       travCruiseIndus.setLdgNm(taaTravCruiseIndustryBean.getLdgNm());
/*      */     }
/*  893 */     if (!CommonValidator.isNullOrEmpty(taaTravCruiseIndustryBean
/*  894 */       .getLdgCityNm())) {
/*  895 */       travCruiseIndus.setLdgCityNm(
/*  896 */         taaTravCruiseIndustryBean.getLdgCityNm());
/*      */     }
/*  898 */     if (!CommonValidator.isNullOrEmpty(taaTravCruiseIndustryBean
/*  899 */       .getLdgRgnCd()))
/*      */     {
/*  901 */       travCruiseIndus.setLdgRgnCd(taaTravCruiseIndustryBean.getLdgRgnCd());
/*      */     }
/*      */     
/*  904 */     if (!CommonValidator.isNullOrEmpty(taaTravCruiseIndustryBean
/*  905 */       .getLdgCtryCd())) {
/*  906 */       travCruiseIndus.setLdgCtryCd(
/*  907 */         taaTravCruiseIndustryBean.getLdgCtryCd());
/*      */     }
/*  909 */     if ((taaTravCruiseIndustryBean.getTravelSegInfoBean() != null) && 
/*  910 */       (!taaTravCruiseIndustryBean.getTravelSegInfoBean().isEmpty()))
/*      */     {
/*      */ 
/*  913 */       Iterator localIterator = taaTravCruiseIndustryBean.getTravelSegInfoBean().iterator();
/*  912 */       while (localIterator.hasNext()) {
/*  913 */         TravelSegInfoBean travelSegInfoBean = (TravelSegInfoBean)localIterator.next();
/*  914 */         TravelSegInfoType travelSegInfo = new com.americanexpress.ips.gfsg.beans.xml.datacapture.request.ObjectFactory()
/*  915 */           .createTravelSegInfoType();
/*      */         
/*  917 */         if (!CommonValidator.isNullOrEmpty(travelSegInfoBean
/*  918 */           .getDprtDt())) {
/*  919 */           travelSegInfo.setDprtDt(travelSegInfoBean.getDprtDt());
/*      */         }
/*  921 */         if (!CommonValidator.isNullOrEmpty(travelSegInfoBean
/*  922 */           .getDestCd())) {
/*  923 */           travelSegInfo.setDestCd(travelSegInfoBean.getDestCd());
/*      */         }
/*  925 */         if (!CommonValidator.isNullOrEmpty(travelSegInfoBean
/*  926 */           .getDprtAirCd()))
/*      */         {
/*  928 */           travelSegInfo.setDprtAirCd(travelSegInfoBean.getDprtAirCd());
/*      */         }
/*  930 */         if (!CommonValidator.isNullOrEmpty(travelSegInfoBean
/*  931 */           .getFlghtNbr())) {
/*  932 */           travelSegInfo.setFlghtNbr(travelSegInfoBean.getFlghtNbr());
/*      */         }
/*  934 */         if (!CommonValidator.isNullOrEmpty(travelSegInfoBean
/*  935 */           .getSegIATACarrierCd())) {
/*  936 */           travelSegInfo.setSegIATACarrierCd(
/*  937 */             travelSegInfoBean.getSegIATACarrierCd());
/*      */         }
/*  939 */         if (!CommonValidator.isNullOrEmpty(travelSegInfoBean
/*  940 */           .getServClassCd())) {
/*  941 */           travelSegInfo.setServClassCd(
/*  942 */             travelSegInfoBean.getServClassCd());
/*      */         }
/*  944 */         travCruiseIndus.getTravelSegInfo().add(travelSegInfo);
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*  949 */     return travCruiseIndus;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static TAARetailIndusType convertRetailIndustryToJAXBBean(TAARetailIndustryBean taaRetailIndustryBean)
/*      */   {
/*  965 */     TAARetailIndusType retailIndus = new com.americanexpress.ips.gfsg.beans.xml.datacapture.request.ObjectFactory()
/*  966 */       .createTAARetailIndusType();
/*  967 */     if (!CommonValidator.isNullOrEmpty(taaRetailIndustryBean
/*  968 */       .getRetailDeptNm()))
/*      */     {
/*  970 */       retailIndus.setRetailDeptNm(taaRetailIndustryBean.getRetailDeptNm());
/*      */     }
/*  972 */     if (!taaRetailIndustryBean.getRetailItemInfoBean().isEmpty())
/*      */     {
/*  974 */       Iterator localIterator = taaRetailIndustryBean.getRetailItemInfoBean().iterator();
/*  973 */       while (localIterator.hasNext()) {
/*  974 */         RetailItemInfoBean retailItemInfoBean = (RetailItemInfoBean)localIterator.next();
/*  975 */         RetailItemInfoType retailItemInfo = new com.americanexpress.ips.gfsg.beans.xml.datacapture.request.ObjectFactory()
/*  976 */           .createRetailItemInfoType();
/*  977 */         if (!CommonValidator.isNullOrEmpty(retailItemInfoBean
/*  978 */           .getRetailItemDesc())) {
/*  979 */           retailItemInfo.setRetailItemDesc(
/*  980 */             retailItemInfoBean.getRetailItemDesc());
/*      */         }
/*  982 */         if (!CommonValidator.isNullOrEmpty(retailItemInfoBean
/*  983 */           .getRetailItemQty())) {
/*  984 */           retailItemInfo.setRetailItemQty(
/*  985 */             retailItemInfoBean.getRetailItemQty());
/*      */         }
/*      */         
/*  988 */         if (!CommonValidator.isNullOrEmpty(retailItemInfoBean
/*  989 */           .getRetailItemAmt())) {
/*  990 */           retailItemInfo.setRetailItemAmt(
/*  991 */             retailItemInfoBean.getRetailItemAmt());
/*      */         }
/*      */         
/*  994 */         retailIndus.getRetailItemInfo().add(retailItemInfo);
/*      */       }
/*      */     }
/*      */     
/*  998 */     return retailIndus;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static TAAEntTktIndusType convertEntTktIndustryToJAXBBean(TAAEntertainmentTktIndustryBean taaEntTktIndustryBean)
/*      */   {
/* 1014 */     TAAEntTktIndusType entTktIndus = new com.americanexpress.ips.gfsg.beans.xml.datacapture.request.ObjectFactory()
/* 1015 */       .createTAAEntTktIndusType();
/* 1016 */     if (!CommonValidator.isNullOrEmpty(taaEntTktIndustryBean.getEventNm())) {
/* 1017 */       entTktIndus.setEventNm(taaEntTktIndustryBean.getEventNm());
/*      */     }
/* 1019 */     if (!CommonValidator.isNullOrEmpty(taaEntTktIndustryBean.getEventDt())) {
/* 1020 */       entTktIndus.setEventDt(taaEntTktIndustryBean.getEventDt());
/*      */     }
/* 1022 */     if (!CommonValidator.isNullOrEmpty(taaEntTktIndustryBean
/* 1023 */       .getEventTktPrcAmt())) {
/* 1024 */       entTktIndus.setEventTktPrcAmt(
/* 1025 */         taaEntTktIndustryBean.getEventTktPrcAmt());
/*      */     }
/* 1027 */     if (!CommonValidator.isNullOrEmpty(taaEntTktIndustryBean
/* 1028 */       .getEventTktCnt())) {
/* 1029 */       entTktIndus.setEventTktCnt(taaEntTktIndustryBean.getEventTktCnt());
/*      */     }
/* 1031 */     if (!CommonValidator.isNullOrEmpty(taaEntTktIndustryBean
/* 1032 */       .getEventLocNm())) {
/* 1033 */       entTktIndus.setEventLocNm(taaEntTktIndustryBean.getEventLocNm());
/*      */     }
/* 1035 */     if (!CommonValidator.isNullOrEmpty(taaEntTktIndustryBean
/* 1036 */       .getEventCtryCd())) {
/* 1037 */       entTktIndus.setEventCtryCd(taaEntTktIndustryBean.getEventCtryCd());
/*      */     }
/* 1039 */     if (!CommonValidator.isNullOrEmpty(taaEntTktIndustryBean
/* 1040 */       .getEventRgnCd())) {
/* 1041 */       entTktIndus.setEventRgnCd(taaEntTktIndustryBean.getEventRgnCd());
/*      */     }
/*      */     
/* 1044 */     return entTktIndus;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static TAACorpPurchSolLvl2Type convertCorpPurchSolToJAXBBean(TAACorpPurchSolLvl2Bean taaCorpPurchSolLvl2Bean)
/*      */   {
/* 1060 */     TAACorpPurchSolLvl2Type corpPurchSolLvl2 = new com.americanexpress.ips.gfsg.beans.xml.datacapture.request.ObjectFactory()
/* 1061 */       .createTAACorpPurchSolLvl2Type();
/* 1062 */     if (!CommonValidator.isNullOrEmpty(taaCorpPurchSolLvl2Bean.getReqNm())) {
/* 1063 */       corpPurchSolLvl2.setReqNm(taaCorpPurchSolLvl2Bean.getReqNm());
/*      */     }
/* 1065 */     if (!CommonValidator.isNullOrEmpty(taaCorpPurchSolLvl2Bean
/* 1066 */       .getCmRefNbr())) {
/* 1067 */       corpPurchSolLvl2.setCMRefNbr(taaCorpPurchSolLvl2Bean.getCmRefNbr());
/*      */     }
/* 1069 */     if (!CommonValidator.isNullOrEmpty(taaCorpPurchSolLvl2Bean
/* 1070 */       .getShipToPostCd())) {
/* 1071 */       corpPurchSolLvl2.setShipToPostCd(
/* 1072 */         taaCorpPurchSolLvl2Bean.getShipToPostCd());
/*      */     }
/* 1074 */     if (!CommonValidator.isNullOrEmpty(taaCorpPurchSolLvl2Bean
/* 1075 */       .getTaxTotAmt())) {
/* 1076 */       BigInteger bigTaxTotAmt = new BigInteger(
/* 1077 */         taaCorpPurchSolLvl2Bean.getTaxTotAmt());
/* 1078 */       corpPurchSolLvl2.setTaxTotAmt(bigTaxTotAmt);
/*      */     }
/* 1080 */     if (!CommonValidator.isNullOrEmpty(taaCorpPurchSolLvl2Bean
/* 1081 */       .getTaxTypeCd())) {
/* 1082 */       corpPurchSolLvl2.setTaxTypeCd(
/* 1083 */         taaCorpPurchSolLvl2Bean.getTaxTypeCd());
/*      */     }
/* 1085 */     if ((taaCorpPurchSolLvl2Bean.getChargeItemInfoBean() != null) && 
/* 1086 */       (!taaCorpPurchSolLvl2Bean.getChargeItemInfoBean().isEmpty()))
/*      */     {
/* 1088 */       Iterator localIterator = taaCorpPurchSolLvl2Bean.getChargeItemInfoBean().iterator();
/* 1087 */       while (localIterator.hasNext()) {
/* 1088 */         ChargeItemInfoBean chargeItemInfo = (ChargeItemInfoBean)localIterator.next();
/* 1089 */         ChrgItemInfoType chrgItemInfo = new com.americanexpress.ips.gfsg.beans.xml.datacapture.request.ObjectFactory()
/* 1090 */           .createChrgItemInfoType();
/* 1091 */         if (!CommonValidator.isNullOrEmpty(chargeItemInfo
/* 1092 */           .getChargeDesc()))
/*      */         {
/* 1094 */           chrgItemInfo.setChrgDesc(chargeItemInfo.getChargeDesc());
/*      */         }
/*      */         
/* 1097 */         if (!CommonValidator.isNullOrEmpty(chargeItemInfo
/* 1098 */           .getChargeItemQty()))
/*      */         {
/* 1100 */           chrgItemInfo.setChrgItemQty(
/* 1101 */             chargeItemInfo.getChargeItemQty());
/*      */         }
/* 1103 */         if (!CommonValidator.isNullOrEmpty(chargeItemInfo
/* 1104 */           .getChargeItemAmt()))
/*      */         {
/* 1106 */           chrgItemInfo.setChrgItemAmt(
/* 1107 */             chargeItemInfo.getChargeItemAmt());
/*      */         }
/* 1109 */         corpPurchSolLvl2.getChrgItemInfo().add(chrgItemInfo);
/*      */       }
/*      */     }
/*      */     
/* 1113 */     return corpPurchSolLvl2;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static InvoiceMsgInfoType convertInvoiceMsgInfoToJAXBBean(InvoiceMassageInfoBean invoiceMassageInfoBean)
/*      */   {
/* 1129 */     InvoiceMsgInfoType invoiceMsgInfo = new com.americanexpress.ips.gfsg.beans.xml.datacapture.request.ObjectFactory()
/* 1130 */       .createInvoiceMsgInfoType();
/* 1131 */     if (!CommonValidator.isNullOrEmpty(invoiceMassageInfoBean
/* 1132 */       .getInvoiceNumber())) {
/* 1133 */       invoiceMsgInfo.setInvoiceNumber(
/* 1134 */         invoiceMassageInfoBean.getInvoiceNumber());
/*      */     }
/*      */     
/* 1137 */     if (!CommonValidator.isNullOrEmpty(invoiceMassageInfoBean
/* 1138 */       .getReleaseNumber())) {
/* 1139 */       invoiceMsgInfo.setReleaseNumber(
/* 1140 */         invoiceMassageInfoBean.getReleaseNumber());
/*      */     }
/*      */     
/* 1143 */     DescriptionType descriptionType = new com.americanexpress.ips.gfsg.beans.xml.datacapture.request.ObjectFactory()
/* 1144 */       .createDescriptionType();
/* 1145 */     if (!CommonValidator.isNullOrEmpty(invoiceMassageInfoBean
/* 1146 */       .getDescription1()))
/*      */     {
/* 1148 */       descriptionType.setDesc1(invoiceMassageInfoBean.getDescription1());
/*      */     }
/*      */     
/* 1151 */     if (!CommonValidator.isNullOrEmpty(invoiceMassageInfoBean
/* 1152 */       .getDescription2())) {
/* 1153 */       descriptionType.setDesc2(invoiceMassageInfoBean.getDescription2());
/*      */     }
/*      */     
/* 1156 */     if (!CommonValidator.isNullOrEmpty(invoiceMassageInfoBean
/* 1157 */       .getDescription3())) {
/* 1158 */       descriptionType.setDesc3(invoiceMassageInfoBean.getDescription3());
/*      */     }
/* 1160 */     if (!CommonValidator.isNullOrEmpty(invoiceMassageInfoBean
/* 1161 */       .getDescription4())) {
/* 1162 */       descriptionType.setDesc4(invoiceMassageInfoBean.getDescription4());
/*      */     }
/*      */     
/* 1165 */     if (descriptionType != null)
/*      */     {
/* 1167 */       invoiceMsgInfo.setDescription(descriptionType);
/*      */     }
/*      */     
/*      */ 
/* 1171 */     return invoiceMsgInfo;
/*      */   }
/*      */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\messagecreator\XMLMessageCreator.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */