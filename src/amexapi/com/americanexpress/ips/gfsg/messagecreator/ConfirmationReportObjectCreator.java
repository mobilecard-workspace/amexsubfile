/*     */ package com.americanexpress.ips.gfsg.messagecreator;
/*     */ 
/*     */ import com.americanexpress.ips.gfsg.beans.conformation.ConfirmationErrorReportBean;
/*     */ import com.americanexpress.ips.gfsg.beans.conformation.ConfirmationReportBodyBean;
/*     */ import com.americanexpress.ips.gfsg.beans.conformation.ConfirmationReportHeaderBean;
/*     */ import com.americanexpress.ips.gfsg.beans.conformation.ConfirmationReportObject;
/*     */ import com.americanexpress.ips.gfsg.beans.conformation.ConfirmationReportSummaryBean;
/*     */ import org.apache.log4j.Logger;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class ConfirmationReportObjectCreator
/*     */ {
/*  24 */   private static final Logger LOGGER = Logger.getLogger(ConfirmationReportObjectCreator.class);
/*     */   
/*     */   /* Error */
/*     */   public static java.util.List<ConfirmationReportObject> convertRawFiletoResponse(java.util.List<java.io.File> confirmationFile)
/*     */   {
/*     */     // Byte code:
/*     */     //   0: new 29	java/util/ArrayList
/*     */     //   3: dup
/*     */     //   4: invokespecial 31	java/util/ArrayList:<init>	()V
/*     */     //   7: astore_1
/*     */     //   8: aconst_null
/*     */     //   9: astore_2
/*     */     //   10: sipush 700
/*     */     //   13: istore_3
/*     */     //   14: aload_0
/*     */     //   15: invokeinterface 32 1 0
/*     */     //   20: ifne +1265 -> 1285
/*     */     //   23: aload_0
/*     */     //   24: invokeinterface 38 1 0
/*     */     //   29: astore 5
/*     */     //   31: goto +1145 -> 1176
/*     */     //   34: aload 5
/*     */     //   36: invokeinterface 42 1 0
/*     */     //   41: checkcast 48	java/io/File
/*     */     //   44: astore 4
/*     */     //   46: new 50	java/io/BufferedReader
/*     */     //   49: dup
/*     */     //   50: new 52	java/io/FileReader
/*     */     //   53: dup
/*     */     //   54: aload 4
/*     */     //   56: invokespecial 54	java/io/FileReader:<init>	(Ljava/io/File;)V
/*     */     //   59: invokespecial 57	java/io/BufferedReader:<init>	(Ljava/io/Reader;)V
/*     */     //   62: astore_2
/*     */     //   63: new 60	com/americanexpress/ips/gfsg/beans/conformation/ConfirmationReportObject
/*     */     //   66: dup
/*     */     //   67: invokespecial 62	com/americanexpress/ips/gfsg/beans/conformation/ConfirmationReportObject:<init>	()V
/*     */     //   70: astore 6
/*     */     //   72: new 63	java/lang/StringBuffer
/*     */     //   75: dup
/*     */     //   76: invokespecial 65	java/lang/StringBuffer:<init>	()V
/*     */     //   79: astore 8
/*     */     //   81: goto +1076 -> 1157
/*     */     //   84: aload 8
/*     */     //   86: aload 7
/*     */     //   88: invokevirtual 66	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
/*     */     //   91: pop
/*     */     //   92: iload_3
/*     */     //   93: aload 8
/*     */     //   95: invokevirtual 70	java/lang/StringBuffer:length	()I
/*     */     //   98: if_icmpne +1059 -> 1157
/*     */     //   101: aload 8
/*     */     //   103: iconst_0
/*     */     //   104: iconst_3
/*     */     //   105: invokevirtual 74	java/lang/StringBuffer:substring	(II)Ljava/lang/String;
/*     */     //   108: astore 9
/*     */     //   110: aload 9
/*     */     //   112: ldc 78
/*     */     //   114: invokevirtual 80	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
/*     */     //   117: ifeq +50 -> 167
/*     */     //   120: invokestatic 86	com/americanexpress/ips/gfsg/connection/PropertyReader:getDebuggerFlag	()Ljava/lang/String;
/*     */     //   123: ldc 92
/*     */     //   125: invokevirtual 80	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
/*     */     //   128: ifeq +29 -> 157
/*     */     //   131: getstatic 16	com/americanexpress/ips/gfsg/messagecreator/ConfirmationReportObjectCreator:LOGGER	Lorg/apache/log4j/Logger;
/*     */     //   134: new 94	java/lang/StringBuilder
/*     */     //   137: dup
/*     */     //   138: ldc 96
/*     */     //   140: invokespecial 98	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
/*     */     //   143: aload 8
/*     */     //   145: invokevirtual 101	java/lang/StringBuffer:toString	()Ljava/lang/String;
/*     */     //   148: invokevirtual 104	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
/*     */     //   151: invokevirtual 107	java/lang/StringBuilder:toString	()Ljava/lang/String;
/*     */     //   154: invokevirtual 108	org/apache/log4j/Logger:info	(Ljava/lang/Object;)V
/*     */     //   157: aload 8
/*     */     //   159: aload 6
/*     */     //   161: invokestatic 112	com/americanexpress/ips/gfsg/messagecreator/ConfirmationReportObjectCreator:parseReportHeader	(Ljava/lang/StringBuffer;Lcom/americanexpress/ips/gfsg/beans/conformation/ConfirmationReportObject;)V
/*     */     //   164: goto +983 -> 1147
/*     */     //   167: aload 9
/*     */     //   169: ldc 116
/*     */     //   171: invokevirtual 80	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
/*     */     //   174: ifeq +39 -> 213
/*     */     //   177: getstatic 16	com/americanexpress/ips/gfsg/messagecreator/ConfirmationReportObjectCreator:LOGGER	Lorg/apache/log4j/Logger;
/*     */     //   180: new 94	java/lang/StringBuilder
/*     */     //   183: dup
/*     */     //   184: ldc 118
/*     */     //   186: invokespecial 98	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
/*     */     //   189: aload 8
/*     */     //   191: invokevirtual 101	java/lang/StringBuffer:toString	()Ljava/lang/String;
/*     */     //   194: invokevirtual 104	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
/*     */     //   197: invokevirtual 107	java/lang/StringBuilder:toString	()Ljava/lang/String;
/*     */     //   200: invokevirtual 108	org/apache/log4j/Logger:info	(Ljava/lang/Object;)V
/*     */     //   203: aload 8
/*     */     //   205: aload 6
/*     */     //   207: invokestatic 120	com/americanexpress/ips/gfsg/messagecreator/ConfirmationReportObjectCreator:parseReportBody	(Ljava/lang/StringBuffer;Lcom/americanexpress/ips/gfsg/beans/conformation/ConfirmationReportObject;)V
/*     */     //   210: goto +937 -> 1147
/*     */     //   213: aload 9
/*     */     //   215: ldc 123
/*     */     //   217: invokevirtual 80	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
/*     */     //   220: ifeq +39 -> 259
/*     */     //   223: getstatic 16	com/americanexpress/ips/gfsg/messagecreator/ConfirmationReportObjectCreator:LOGGER	Lorg/apache/log4j/Logger;
/*     */     //   226: new 94	java/lang/StringBuilder
/*     */     //   229: dup
/*     */     //   230: ldc 125
/*     */     //   232: invokespecial 98	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
/*     */     //   235: aload 8
/*     */     //   237: invokevirtual 101	java/lang/StringBuffer:toString	()Ljava/lang/String;
/*     */     //   240: invokevirtual 104	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
/*     */     //   243: invokevirtual 107	java/lang/StringBuilder:toString	()Ljava/lang/String;
/*     */     //   246: invokevirtual 108	org/apache/log4j/Logger:info	(Ljava/lang/Object;)V
/*     */     //   249: aload 8
/*     */     //   251: aload 6
/*     */     //   253: invokestatic 127	com/americanexpress/ips/gfsg/messagecreator/ConfirmationReportObjectCreator:parseReportSummary	(Ljava/lang/StringBuffer;Lcom/americanexpress/ips/gfsg/beans/conformation/ConfirmationReportObject;)V
/*     */     //   256: goto +891 -> 1147
/*     */     //   259: aload 9
/*     */     //   261: ldc -126
/*     */     //   263: invokevirtual 80	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
/*     */     //   266: ifeq +13 -> 279
/*     */     //   269: aload 8
/*     */     //   271: aload 6
/*     */     //   273: invokestatic 132	com/americanexpress/ips/gfsg/messagecreator/ConfirmationReportObjectCreator:parseReportRequiringRejection	(Ljava/lang/StringBuffer;Lcom/americanexpress/ips/gfsg/beans/conformation/ConfirmationReportObject;)V
/*     */     //   276: goto +871 -> 1147
/*     */     //   279: aload 9
/*     */     //   281: ldc -121
/*     */     //   283: invokevirtual 80	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
/*     */     //   286: ifeq +39 -> 325
/*     */     //   289: getstatic 16	com/americanexpress/ips/gfsg/messagecreator/ConfirmationReportObjectCreator:LOGGER	Lorg/apache/log4j/Logger;
/*     */     //   292: new 94	java/lang/StringBuilder
/*     */     //   295: dup
/*     */     //   296: ldc -119
/*     */     //   298: invokespecial 98	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
/*     */     //   301: aload 8
/*     */     //   303: invokevirtual 101	java/lang/StringBuffer:toString	()Ljava/lang/String;
/*     */     //   306: invokevirtual 104	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
/*     */     //   309: invokevirtual 107	java/lang/StringBuilder:toString	()Ljava/lang/String;
/*     */     //   312: invokevirtual 108	org/apache/log4j/Logger:info	(Ljava/lang/Object;)V
/*     */     //   315: aload 8
/*     */     //   317: aload 6
/*     */     //   319: invokestatic 139	com/americanexpress/ips/gfsg/messagecreator/ConfirmationReportObjectCreator:parseReportNotRequiringRejection	(Ljava/lang/StringBuffer;Lcom/americanexpress/ips/gfsg/beans/conformation/ConfirmationReportObject;)V
/*     */     //   322: goto +825 -> 1147
/*     */     //   325: aload 9
/*     */     //   327: ldc -114
/*     */     //   329: invokevirtual 80	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
/*     */     //   332: ifeq +39 -> 371
/*     */     //   335: getstatic 16	com/americanexpress/ips/gfsg/messagecreator/ConfirmationReportObjectCreator:LOGGER	Lorg/apache/log4j/Logger;
/*     */     //   338: new 94	java/lang/StringBuilder
/*     */     //   341: dup
/*     */     //   342: ldc -112
/*     */     //   344: invokespecial 98	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
/*     */     //   347: aload 8
/*     */     //   349: invokevirtual 101	java/lang/StringBuffer:toString	()Ljava/lang/String;
/*     */     //   352: invokevirtual 104	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
/*     */     //   355: invokevirtual 107	java/lang/StringBuilder:toString	()Ljava/lang/String;
/*     */     //   358: invokevirtual 108	org/apache/log4j/Logger:info	(Ljava/lang/Object;)V
/*     */     //   361: aload 8
/*     */     //   363: aload 6
/*     */     //   365: invokestatic 146	com/americanexpress/ips/gfsg/messagecreator/ConfirmationReportObjectCreator:parseReportBatchSuspension	(Ljava/lang/StringBuffer;Lcom/americanexpress/ips/gfsg/beans/conformation/ConfirmationReportObject;)V
/*     */     //   368: goto +779 -> 1147
/*     */     //   371: aload 9
/*     */     //   373: ldc -107
/*     */     //   375: invokevirtual 80	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
/*     */     //   378: ifeq +39 -> 417
/*     */     //   381: getstatic 16	com/americanexpress/ips/gfsg/messagecreator/ConfirmationReportObjectCreator:LOGGER	Lorg/apache/log4j/Logger;
/*     */     //   384: new 94	java/lang/StringBuilder
/*     */     //   387: dup
/*     */     //   388: ldc -105
/*     */     //   390: invokespecial 98	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
/*     */     //   393: aload 8
/*     */     //   395: invokevirtual 101	java/lang/StringBuffer:toString	()Ljava/lang/String;
/*     */     //   398: invokevirtual 104	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
/*     */     //   401: invokevirtual 107	java/lang/StringBuilder:toString	()Ljava/lang/String;
/*     */     //   404: invokevirtual 108	org/apache/log4j/Logger:info	(Ljava/lang/Object;)V
/*     */     //   407: aload 8
/*     */     //   409: aload 6
/*     */     //   411: invokestatic 153	com/americanexpress/ips/gfsg/messagecreator/ConfirmationReportObjectCreator:parseReportRejectedBatch	(Ljava/lang/StringBuffer;Lcom/americanexpress/ips/gfsg/beans/conformation/ConfirmationReportObject;)V
/*     */     //   414: goto +733 -> 1147
/*     */     //   417: aload 9
/*     */     //   419: ldc -100
/*     */     //   421: invokevirtual 80	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
/*     */     //   424: ifeq +39 -> 463
/*     */     //   427: getstatic 16	com/americanexpress/ips/gfsg/messagecreator/ConfirmationReportObjectCreator:LOGGER	Lorg/apache/log4j/Logger;
/*     */     //   430: new 94	java/lang/StringBuilder
/*     */     //   433: dup
/*     */     //   434: ldc -98
/*     */     //   436: invokespecial 98	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
/*     */     //   439: aload 8
/*     */     //   441: invokevirtual 101	java/lang/StringBuffer:toString	()Ljava/lang/String;
/*     */     //   444: invokevirtual 104	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
/*     */     //   447: invokevirtual 107	java/lang/StringBuilder:toString	()Ljava/lang/String;
/*     */     //   450: invokevirtual 108	org/apache/log4j/Logger:info	(Ljava/lang/Object;)V
/*     */     //   453: aload 8
/*     */     //   455: aload 6
/*     */     //   457: invokestatic 160	com/americanexpress/ips/gfsg/messagecreator/ConfirmationReportObjectCreator:parseReportProcessedBatch	(Ljava/lang/StringBuffer;Lcom/americanexpress/ips/gfsg/beans/conformation/ConfirmationReportObject;)V
/*     */     //   460: goto +687 -> 1147
/*     */     //   463: aload 9
/*     */     //   465: ldc -93
/*     */     //   467: invokevirtual 80	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
/*     */     //   470: ifeq +61 -> 531
/*     */     //   473: getstatic 16	com/americanexpress/ips/gfsg/messagecreator/ConfirmationReportObjectCreator:LOGGER	Lorg/apache/log4j/Logger;
/*     */     //   476: new 94	java/lang/StringBuilder
/*     */     //   479: dup
/*     */     //   480: ldc -91
/*     */     //   482: invokespecial 98	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
/*     */     //   485: aload 8
/*     */     //   487: invokevirtual 101	java/lang/StringBuffer:toString	()Ljava/lang/String;
/*     */     //   490: invokevirtual 104	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
/*     */     //   493: invokevirtual 107	java/lang/StringBuilder:toString	()Ljava/lang/String;
/*     */     //   496: invokevirtual 108	org/apache/log4j/Logger:info	(Ljava/lang/Object;)V
/*     */     //   499: new 167	com/americanexpress/ips/gfsg/beans/conformation/ConfirmationRejectedRecordBean
/*     */     //   502: dup
/*     */     //   503: invokespecial 169	com/americanexpress/ips/gfsg/beans/conformation/ConfirmationRejectedRecordBean:<init>	()V
/*     */     //   506: astore 10
/*     */     //   508: aload 8
/*     */     //   510: aload 10
/*     */     //   512: invokestatic 170	com/americanexpress/ips/gfsg/messagecreator/RejectionRecords:setTABRecord	(Ljava/lang/StringBuffer;Lcom/americanexpress/ips/gfsg/beans/conformation/ConfirmationRejectedRecordBean;)V
/*     */     //   515: aload 6
/*     */     //   517: invokevirtual 176	com/americanexpress/ips/gfsg/beans/conformation/ConfirmationReportObject:getRejectionRecords	()Ljava/util/List;
/*     */     //   520: aload 10
/*     */     //   522: invokeinterface 180 2 0
/*     */     //   527: pop
/*     */     //   528: goto +619 -> 1147
/*     */     //   531: aload 9
/*     */     //   533: ldc -72
/*     */     //   535: invokevirtual 80	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
/*     */     //   538: ifeq +61 -> 599
/*     */     //   541: getstatic 16	com/americanexpress/ips/gfsg/messagecreator/ConfirmationReportObjectCreator:LOGGER	Lorg/apache/log4j/Logger;
/*     */     //   544: new 94	java/lang/StringBuilder
/*     */     //   547: dup
/*     */     //   548: ldc -70
/*     */     //   550: invokespecial 98	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
/*     */     //   553: aload 8
/*     */     //   555: invokevirtual 101	java/lang/StringBuffer:toString	()Ljava/lang/String;
/*     */     //   558: invokevirtual 104	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
/*     */     //   561: invokevirtual 107	java/lang/StringBuilder:toString	()Ljava/lang/String;
/*     */     //   564: invokevirtual 108	org/apache/log4j/Logger:info	(Ljava/lang/Object;)V
/*     */     //   567: new 167	com/americanexpress/ips/gfsg/beans/conformation/ConfirmationRejectedRecordBean
/*     */     //   570: dup
/*     */     //   571: invokespecial 169	com/americanexpress/ips/gfsg/beans/conformation/ConfirmationRejectedRecordBean:<init>	()V
/*     */     //   574: astore 10
/*     */     //   576: aload 8
/*     */     //   578: aload 10
/*     */     //   580: invokestatic 188	com/americanexpress/ips/gfsg/messagecreator/RejectionRecords:setTADRecord	(Ljava/lang/StringBuffer;Lcom/americanexpress/ips/gfsg/beans/conformation/ConfirmationRejectedRecordBean;)V
/*     */     //   583: aload 6
/*     */     //   585: invokevirtual 176	com/americanexpress/ips/gfsg/beans/conformation/ConfirmationReportObject:getRejectionRecords	()Ljava/util/List;
/*     */     //   588: aload 10
/*     */     //   590: invokeinterface 180 2 0
/*     */     //   595: pop
/*     */     //   596: goto +551 -> 1147
/*     */     //   599: aload 9
/*     */     //   601: ldc -65
/*     */     //   603: invokevirtual 80	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
/*     */     //   606: ifeq +68 -> 674
/*     */     //   609: new 167	com/americanexpress/ips/gfsg/beans/conformation/ConfirmationRejectedRecordBean
/*     */     //   612: dup
/*     */     //   613: invokespecial 169	com/americanexpress/ips/gfsg/beans/conformation/ConfirmationRejectedRecordBean:<init>	()V
/*     */     //   616: astore 10
/*     */     //   618: aload 8
/*     */     //   620: aload 10
/*     */     //   622: invokestatic 193	com/americanexpress/ips/gfsg/messagecreator/RejectionRecords:setTBTRecord	(Ljava/lang/StringBuffer;Lcom/americanexpress/ips/gfsg/beans/conformation/ConfirmationRejectedRecordBean;)V
/*     */     //   625: getstatic 16	com/americanexpress/ips/gfsg/messagecreator/ConfirmationReportObjectCreator:LOGGER	Lorg/apache/log4j/Logger;
/*     */     //   628: new 94	java/lang/StringBuilder
/*     */     //   631: dup
/*     */     //   632: ldc -60
/*     */     //   634: invokespecial 98	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
/*     */     //   637: aload 8
/*     */     //   639: invokevirtual 101	java/lang/StringBuffer:toString	()Ljava/lang/String;
/*     */     //   642: invokevirtual 104	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
/*     */     //   645: invokevirtual 107	java/lang/StringBuilder:toString	()Ljava/lang/String;
/*     */     //   648: invokevirtual 108	org/apache/log4j/Logger:info	(Ljava/lang/Object;)V
/*     */     //   651: aload 8
/*     */     //   653: aload 6
/*     */     //   655: invokestatic 160	com/americanexpress/ips/gfsg/messagecreator/ConfirmationReportObjectCreator:parseReportProcessedBatch	(Ljava/lang/StringBuffer;Lcom/americanexpress/ips/gfsg/beans/conformation/ConfirmationReportObject;)V
/*     */     //   658: aload 6
/*     */     //   660: invokevirtual 176	com/americanexpress/ips/gfsg/beans/conformation/ConfirmationReportObject:getRejectionRecords	()Ljava/util/List;
/*     */     //   663: aload 10
/*     */     //   665: invokeinterface 180 2 0
/*     */     //   670: pop
/*     */     //   671: goto +476 -> 1147
/*     */     //   674: aload 9
/*     */     //   676: ldc -58
/*     */     //   678: invokevirtual 80	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
/*     */     //   681: ifeq +466 -> 1147
/*     */     //   684: getstatic 16	com/americanexpress/ips/gfsg/messagecreator/ConfirmationReportObjectCreator:LOGGER	Lorg/apache/log4j/Logger;
/*     */     //   687: new 94	java/lang/StringBuilder
/*     */     //   690: dup
/*     */     //   691: ldc -56
/*     */     //   693: invokespecial 98	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
/*     */     //   696: aload 8
/*     */     //   698: invokevirtual 101	java/lang/StringBuffer:toString	()Ljava/lang/String;
/*     */     //   701: invokevirtual 104	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
/*     */     //   704: invokevirtual 107	java/lang/StringBuilder:toString	()Ljava/lang/String;
/*     */     //   707: invokevirtual 108	org/apache/log4j/Logger:info	(Ljava/lang/Object;)V
/*     */     //   710: new 167	com/americanexpress/ips/gfsg/beans/conformation/ConfirmationRejectedRecordBean
/*     */     //   713: dup
/*     */     //   714: invokespecial 169	com/americanexpress/ips/gfsg/beans/conformation/ConfirmationRejectedRecordBean:<init>	()V
/*     */     //   717: astore 10
/*     */     //   719: aload 8
/*     */     //   721: bipush 26
/*     */     //   723: bipush 28
/*     */     //   725: invokevirtual 74	java/lang/StringBuffer:substring	(II)Ljava/lang/String;
/*     */     //   728: astore 11
/*     */     //   730: aload 11
/*     */     //   732: ldc -54
/*     */     //   734: invokevirtual 204	java/lang/String:equals	(Ljava/lang/Object;)Z
/*     */     //   737: ifne +13 -> 750
/*     */     //   740: aload 11
/*     */     //   742: ldc -49
/*     */     //   744: invokevirtual 204	java/lang/String:equals	(Ljava/lang/Object;)Z
/*     */     //   747: ifeq +124 -> 871
/*     */     //   750: aload 8
/*     */     //   752: bipush 28
/*     */     //   754: bipush 30
/*     */     //   756: invokevirtual 74	java/lang/StringBuffer:substring	(II)Ljava/lang/String;
/*     */     //   759: astore 12
/*     */     //   761: aload 12
/*     */     //   763: ldc -47
/*     */     //   765: invokevirtual 204	java/lang/String:equals	(Ljava/lang/Object;)Z
/*     */     //   768: ifne +13 -> 781
/*     */     //   771: aload 12
/*     */     //   773: ldc -45
/*     */     //   775: invokevirtual 204	java/lang/String:equals	(Ljava/lang/Object;)Z
/*     */     //   778: ifeq +13 -> 791
/*     */     //   781: aload 8
/*     */     //   783: aload 10
/*     */     //   785: invokestatic 213	com/americanexpress/ips/gfsg/messagecreator/RejectionRecords:setTAADPPRecord	(Ljava/lang/StringBuffer;Lcom/americanexpress/ips/gfsg/beans/conformation/ConfirmationRejectedRecordBean;)V
/*     */     //   788: goto +346 -> 1134
/*     */     //   791: aload 12
/*     */     //   793: ldc -40
/*     */     //   795: invokevirtual 204	java/lang/String:equals	(Ljava/lang/Object;)Z
/*     */     //   798: ifne +13 -> 811
/*     */     //   801: aload 12
/*     */     //   803: ldc -38
/*     */     //   805: invokevirtual 204	java/lang/String:equals	(Ljava/lang/Object;)Z
/*     */     //   808: ifeq +13 -> 821
/*     */     //   811: aload 8
/*     */     //   813: aload 10
/*     */     //   815: invokestatic 220	com/americanexpress/ips/gfsg/messagecreator/RejectionRecords:setTAACPSL2Record	(Ljava/lang/StringBuffer;Lcom/americanexpress/ips/gfsg/beans/conformation/ConfirmationRejectedRecordBean;)V
/*     */     //   818: goto +316 -> 1134
/*     */     //   821: aload 12
/*     */     //   823: ldc -33
/*     */     //   825: invokevirtual 204	java/lang/String:equals	(Ljava/lang/Object;)Z
/*     */     //   828: ifne +13 -> 841
/*     */     //   831: aload 12
/*     */     //   833: ldc -31
/*     */     //   835: invokevirtual 204	java/lang/String:equals	(Ljava/lang/Object;)Z
/*     */     //   838: ifeq +13 -> 851
/*     */     //   841: aload 8
/*     */     //   843: aload 10
/*     */     //   845: invokestatic 227	com/americanexpress/ips/gfsg/messagecreator/RejectionRecords:setTAAEMVRecord	(Ljava/lang/StringBuffer;Lcom/americanexpress/ips/gfsg/beans/conformation/ConfirmationRejectedRecordBean;)V
/*     */     //   848: goto +286 -> 1134
/*     */     //   851: aload 12
/*     */     //   853: ldc -26
/*     */     //   855: invokevirtual 204	java/lang/String:equals	(Ljava/lang/Object;)Z
/*     */     //   858: ifeq +276 -> 1134
/*     */     //   861: aload 8
/*     */     //   863: aload 10
/*     */     //   865: invokestatic 232	com/americanexpress/ips/gfsg/messagecreator/RejectionRecords:setTAALOCRecord	(Ljava/lang/StringBuffer;Lcom/americanexpress/ips/gfsg/beans/conformation/ConfirmationRejectedRecordBean;)V
/*     */     //   868: goto +266 -> 1134
/*     */     //   871: aload 11
/*     */     //   873: ldc -21
/*     */     //   875: invokevirtual 204	java/lang/String:equals	(Ljava/lang/Object;)Z
/*     */     //   878: ifne +13 -> 891
/*     */     //   881: aload 11
/*     */     //   883: ldc -19
/*     */     //   885: invokevirtual 204	java/lang/String:equals	(Ljava/lang/Object;)Z
/*     */     //   888: ifeq +54 -> 942
/*     */     //   891: aload 8
/*     */     //   893: bipush 28
/*     */     //   895: bipush 30
/*     */     //   897: invokevirtual 74	java/lang/StringBuffer:substring	(II)Ljava/lang/String;
/*     */     //   900: astore 12
/*     */     //   902: aload 12
/*     */     //   904: ldc -33
/*     */     //   906: invokevirtual 204	java/lang/String:equals	(Ljava/lang/Object;)Z
/*     */     //   909: ifne +13 -> 922
/*     */     //   912: aload 12
/*     */     //   914: ldc -31
/*     */     //   916: invokevirtual 204	java/lang/String:equals	(Ljava/lang/Object;)Z
/*     */     //   919: ifeq +13 -> 932
/*     */     //   922: aload 8
/*     */     //   924: aload 10
/*     */     //   926: invokestatic 227	com/americanexpress/ips/gfsg/messagecreator/RejectionRecords:setTAAEMVRecord	(Ljava/lang/StringBuffer;Lcom/americanexpress/ips/gfsg/beans/conformation/ConfirmationRejectedRecordBean;)V
/*     */     //   929: goto +205 -> 1134
/*     */     //   932: aload 8
/*     */     //   934: aload 10
/*     */     //   936: invokestatic 239	com/americanexpress/ips/gfsg/messagecreator/RejectionRecords:setTAAAirRecord	(Ljava/lang/StringBuffer;Lcom/americanexpress/ips/gfsg/beans/conformation/ConfirmationRejectedRecordBean;)V
/*     */     //   939: goto +195 -> 1134
/*     */     //   942: aload 11
/*     */     //   944: ldc -14
/*     */     //   946: invokevirtual 204	java/lang/String:equals	(Ljava/lang/Object;)Z
/*     */     //   949: ifne +13 -> 962
/*     */     //   952: aload 11
/*     */     //   954: ldc -12
/*     */     //   956: invokevirtual 204	java/lang/String:equals	(Ljava/lang/Object;)Z
/*     */     //   959: ifeq +13 -> 972
/*     */     //   962: aload 8
/*     */     //   964: aload 10
/*     */     //   966: invokestatic 246	com/americanexpress/ips/gfsg/messagecreator/RejectionRecords:setTAAInsuRecord	(Ljava/lang/StringBuffer;Lcom/americanexpress/ips/gfsg/beans/conformation/ConfirmationRejectedRecordBean;)V
/*     */     //   969: goto +165 -> 1134
/*     */     //   972: aload 11
/*     */     //   974: ldc -40
/*     */     //   976: invokevirtual 204	java/lang/String:equals	(Ljava/lang/Object;)Z
/*     */     //   979: ifne +13 -> 992
/*     */     //   982: aload 11
/*     */     //   984: ldc -38
/*     */     //   986: invokevirtual 204	java/lang/String:equals	(Ljava/lang/Object;)Z
/*     */     //   989: ifeq +13 -> 1002
/*     */     //   992: aload 8
/*     */     //   994: aload 10
/*     */     //   996: invokestatic 249	com/americanexpress/ips/gfsg/messagecreator/RejectionRecords:setTAAAutoRecord	(Ljava/lang/StringBuffer;Lcom/americanexpress/ips/gfsg/beans/conformation/ConfirmationRejectedRecordBean;)V
/*     */     //   999: goto +135 -> 1134
/*     */     //   1002: aload 11
/*     */     //   1004: ldc -4
/*     */     //   1006: invokevirtual 204	java/lang/String:equals	(Ljava/lang/Object;)Z
/*     */     //   1009: ifne +13 -> 1022
/*     */     //   1012: aload 11
/*     */     //   1014: ldc -2
/*     */     //   1016: invokevirtual 204	java/lang/String:equals	(Ljava/lang/Object;)Z
/*     */     //   1019: ifeq +13 -> 1032
/*     */     //   1022: aload 8
/*     */     //   1024: aload 10
/*     */     //   1026: invokestatic 256	com/americanexpress/ips/gfsg/messagecreator/RejectionRecords:setTAARailRecord	(Ljava/lang/StringBuffer;Lcom/americanexpress/ips/gfsg/beans/conformation/ConfirmationRejectedRecordBean;)V
/*     */     //   1029: goto +105 -> 1134
/*     */     //   1032: aload 11
/*     */     //   1034: ldc_w 259
/*     */     //   1037: invokevirtual 204	java/lang/String:equals	(Ljava/lang/Object;)Z
/*     */     //   1040: ifeq +13 -> 1053
/*     */     //   1043: aload 8
/*     */     //   1045: aload 10
/*     */     //   1047: invokestatic 261	com/americanexpress/ips/gfsg/messagecreator/RejectionRecords:setTAALodgRecord	(Ljava/lang/StringBuffer;Lcom/americanexpress/ips/gfsg/beans/conformation/ConfirmationRejectedRecordBean;)V
/*     */     //   1050: goto +84 -> 1134
/*     */     //   1053: aload 11
/*     */     //   1055: ldc_w 264
/*     */     //   1058: invokevirtual 204	java/lang/String:equals	(Ljava/lang/Object;)Z
/*     */     //   1061: ifeq +13 -> 1074
/*     */     //   1064: aload 8
/*     */     //   1066: aload 10
/*     */     //   1068: invokestatic 266	com/americanexpress/ips/gfsg/messagecreator/RejectionRecords:setTAACommRecord	(Ljava/lang/StringBuffer;Lcom/americanexpress/ips/gfsg/beans/conformation/ConfirmationRejectedRecordBean;)V
/*     */     //   1071: goto +63 -> 1134
/*     */     //   1074: aload 11
/*     */     //   1076: ldc_w 269
/*     */     //   1079: invokevirtual 204	java/lang/String:equals	(Ljava/lang/Object;)Z
/*     */     //   1082: ifeq +13 -> 1095
/*     */     //   1085: aload 8
/*     */     //   1087: aload 10
/*     */     //   1089: invokestatic 271	com/americanexpress/ips/gfsg/messagecreator/RejectionRecords:setTAACruiesRecord	(Ljava/lang/StringBuffer;Lcom/americanexpress/ips/gfsg/beans/conformation/ConfirmationRejectedRecordBean;)V
/*     */     //   1092: goto +42 -> 1134
/*     */     //   1095: aload 11
/*     */     //   1097: ldc_w 274
/*     */     //   1100: invokevirtual 204	java/lang/String:equals	(Ljava/lang/Object;)Z
/*     */     //   1103: ifeq +13 -> 1116
/*     */     //   1106: aload 8
/*     */     //   1108: aload 10
/*     */     //   1110: invokestatic 276	com/americanexpress/ips/gfsg/messagecreator/RejectionRecords:setTAAReatilRecord	(Ljava/lang/StringBuffer;Lcom/americanexpress/ips/gfsg/beans/conformation/ConfirmationRejectedRecordBean;)V
/*     */     //   1113: goto +21 -> 1134
/*     */     //   1116: aload 11
/*     */     //   1118: ldc_w 279
/*     */     //   1121: invokevirtual 204	java/lang/String:equals	(Ljava/lang/Object;)Z
/*     */     //   1124: ifeq +10 -> 1134
/*     */     //   1127: aload 8
/*     */     //   1129: aload 10
/*     */     //   1131: invokestatic 281	com/americanexpress/ips/gfsg/messagecreator/RejectionRecords:setTAAEntetainRecord	(Ljava/lang/StringBuffer;Lcom/americanexpress/ips/gfsg/beans/conformation/ConfirmationRejectedRecordBean;)V
/*     */     //   1134: aload 6
/*     */     //   1136: invokevirtual 176	com/americanexpress/ips/gfsg/beans/conformation/ConfirmationReportObject:getRejectionRecords	()Ljava/util/List;
/*     */     //   1139: aload 10
/*     */     //   1141: invokeinterface 180 2 0
/*     */     //   1146: pop
/*     */     //   1147: aload 8
/*     */     //   1149: iconst_0
/*     */     //   1150: sipush 700
/*     */     //   1153: invokevirtual 284	java/lang/StringBuffer:delete	(II)Ljava/lang/StringBuffer;
/*     */     //   1156: pop
/*     */     //   1157: aload_2
/*     */     //   1158: invokevirtual 288	java/io/BufferedReader:readLine	()Ljava/lang/String;
/*     */     //   1161: dup
/*     */     //   1162: astore 7
/*     */     //   1164: ifnonnull -1080 -> 84
/*     */     //   1167: aload_1
/*     */     //   1168: aload 6
/*     */     //   1170: invokeinterface 180 2 0
/*     */     //   1175: pop
/*     */     //   1176: aload 5
/*     */     //   1178: invokeinterface 291 1 0
/*     */     //   1183: ifne -1149 -> 34
/*     */     //   1186: getstatic 16	com/americanexpress/ips/gfsg/messagecreator/ConfirmationReportObjectCreator:LOGGER	Lorg/apache/log4j/Logger;
/*     */     //   1189: new 94	java/lang/StringBuilder
/*     */     //   1192: dup
/*     */     //   1193: ldc_w 294
/*     */     //   1196: invokespecial 98	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
/*     */     //   1199: aload_1
/*     */     //   1200: invokeinterface 296 1 0
/*     */     //   1205: invokevirtual 299	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
/*     */     //   1208: invokevirtual 107	java/lang/StringBuilder:toString	()Ljava/lang/String;
/*     */     //   1211: invokevirtual 108	org/apache/log4j/Logger:info	(Ljava/lang/Object;)V
/*     */     //   1214: goto +71 -> 1285
/*     */     //   1217: astore_3
/*     */     //   1218: getstatic 16	com/americanexpress/ips/gfsg/messagecreator/ConfirmationReportObjectCreator:LOGGER	Lorg/apache/log4j/Logger;
/*     */     //   1221: new 94	java/lang/StringBuilder
/*     */     //   1224: dup
/*     */     //   1225: ldc_w 302
/*     */     //   1228: invokespecial 98	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
/*     */     //   1231: aload_3
/*     */     //   1232: invokevirtual 304	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
/*     */     //   1235: invokevirtual 107	java/lang/StringBuilder:toString	()Ljava/lang/String;
/*     */     //   1238: invokevirtual 307	org/apache/log4j/Logger:fatal	(Ljava/lang/Object;)V
/*     */     //   1241: aload_2
/*     */     //   1242: invokevirtual 310	java/io/BufferedReader:close	()V
/*     */     //   1245: goto +58 -> 1303
/*     */     //   1248: astore 14
/*     */     //   1250: getstatic 16	com/americanexpress/ips/gfsg/messagecreator/ConfirmationReportObjectCreator:LOGGER	Lorg/apache/log4j/Logger;
/*     */     //   1253: ldc_w 313
/*     */     //   1256: invokevirtual 315	org/apache/log4j/Logger:error	(Ljava/lang/Object;)V
/*     */     //   1259: goto +44 -> 1303
/*     */     //   1262: astore 13
/*     */     //   1264: aload_2
/*     */     //   1265: invokevirtual 310	java/io/BufferedReader:close	()V
/*     */     //   1268: goto +14 -> 1282
/*     */     //   1271: astore 14
/*     */     //   1273: getstatic 16	com/americanexpress/ips/gfsg/messagecreator/ConfirmationReportObjectCreator:LOGGER	Lorg/apache/log4j/Logger;
/*     */     //   1276: ldc_w 313
/*     */     //   1279: invokevirtual 315	org/apache/log4j/Logger:error	(Ljava/lang/Object;)V
/*     */     //   1282: aload 13
/*     */     //   1284: athrow
/*     */     //   1285: aload_2
/*     */     //   1286: invokevirtual 310	java/io/BufferedReader:close	()V
/*     */     //   1289: goto +14 -> 1303
/*     */     //   1292: astore 14
/*     */     //   1294: getstatic 16	com/americanexpress/ips/gfsg/messagecreator/ConfirmationReportObjectCreator:LOGGER	Lorg/apache/log4j/Logger;
/*     */     //   1297: ldc_w 313
/*     */     //   1300: invokevirtual 315	org/apache/log4j/Logger:error	(Ljava/lang/Object;)V
/*     */     //   1303: aload_1
/*     */     //   1304: areturn
/*     */     // Line number table:
/*     */     //   Java source line #28	-> byte code offset #0
/*     */     //   Java source line #29	-> byte code offset #8
/*     */     //   Java source line #32	-> byte code offset #10
/*     */     //   Java source line #33	-> byte code offset #14
/*     */     //   Java source line #34	-> byte code offset #23
/*     */     //   Java source line #36	-> byte code offset #46
/*     */     //   Java source line #37	-> byte code offset #50
/*     */     //   Java source line #36	-> byte code offset #59
/*     */     //   Java source line #38	-> byte code offset #63
/*     */     //   Java source line #41	-> byte code offset #72
/*     */     //   Java source line #44	-> byte code offset #81
/*     */     //   Java source line #46	-> byte code offset #84
/*     */     //   Java source line #48	-> byte code offset #92
/*     */     //   Java source line #51	-> byte code offset #101
/*     */     //   Java source line #52	-> byte code offset #110
/*     */     //   Java source line #53	-> byte code offset #112
/*     */     //   Java source line #52	-> byte code offset #114
/*     */     //   Java source line #54	-> byte code offset #120
/*     */     //   Java source line #56	-> byte code offset #123
/*     */     //   Java source line #55	-> byte code offset #125
/*     */     //   Java source line #57	-> byte code offset #131
/*     */     //   Java source line #58	-> byte code offset #134
/*     */     //   Java source line #59	-> byte code offset #143
/*     */     //   Java source line #58	-> byte code offset #151
/*     */     //   Java source line #62	-> byte code offset #157
/*     */     //   Java source line #63	-> byte code offset #159
/*     */     //   Java source line #62	-> byte code offset #161
/*     */     //   Java source line #64	-> byte code offset #167
/*     */     //   Java source line #66	-> byte code offset #177
/*     */     //   Java source line #67	-> byte code offset #189
/*     */     //   Java source line #66	-> byte code offset #200
/*     */     //   Java source line #68	-> byte code offset #203
/*     */     //   Java source line #69	-> byte code offset #205
/*     */     //   Java source line #68	-> byte code offset #207
/*     */     //   Java source line #71	-> byte code offset #213
/*     */     //   Java source line #72	-> byte code offset #223
/*     */     //   Java source line #73	-> byte code offset #235
/*     */     //   Java source line #72	-> byte code offset #246
/*     */     //   Java source line #75	-> byte code offset #249
/*     */     //   Java source line #76	-> byte code offset #251
/*     */     //   Java source line #75	-> byte code offset #253
/*     */     //   Java source line #78	-> byte code offset #259
/*     */     //   Java source line #79	-> byte code offset #269
/*     */     //   Java source line #80	-> byte code offset #271
/*     */     //   Java source line #79	-> byte code offset #273
/*     */     //   Java source line #81	-> byte code offset #279
/*     */     //   Java source line #82	-> byte code offset #289
/*     */     //   Java source line #83	-> byte code offset #292
/*     */     //   Java source line #84	-> byte code offset #301
/*     */     //   Java source line #83	-> byte code offset #309
/*     */     //   Java source line #85	-> byte code offset #315
/*     */     //   Java source line #86	-> byte code offset #317
/*     */     //   Java source line #85	-> byte code offset #319
/*     */     //   Java source line #87	-> byte code offset #325
/*     */     //   Java source line #88	-> byte code offset #335
/*     */     //   Java source line #89	-> byte code offset #338
/*     */     //   Java source line #90	-> byte code offset #347
/*     */     //   Java source line #89	-> byte code offset #355
/*     */     //   Java source line #91	-> byte code offset #361
/*     */     //   Java source line #92	-> byte code offset #363
/*     */     //   Java source line #91	-> byte code offset #365
/*     */     //   Java source line #94	-> byte code offset #371
/*     */     //   Java source line #95	-> byte code offset #381
/*     */     //   Java source line #96	-> byte code offset #384
/*     */     //   Java source line #97	-> byte code offset #393
/*     */     //   Java source line #96	-> byte code offset #401
/*     */     //   Java source line #98	-> byte code offset #407
/*     */     //   Java source line #99	-> byte code offset #409
/*     */     //   Java source line #98	-> byte code offset #411
/*     */     //   Java source line #101	-> byte code offset #417
/*     */     //   Java source line #102	-> byte code offset #427
/*     */     //   Java source line #103	-> byte code offset #430
/*     */     //   Java source line #104	-> byte code offset #439
/*     */     //   Java source line #103	-> byte code offset #447
/*     */     //   Java source line #106	-> byte code offset #453
/*     */     //   Java source line #107	-> byte code offset #455
/*     */     //   Java source line #106	-> byte code offset #457
/*     */     //   Java source line #109	-> byte code offset #463
/*     */     //   Java source line #110	-> byte code offset #473
/*     */     //   Java source line #111	-> byte code offset #485
/*     */     //   Java source line #110	-> byte code offset #496
/*     */     //   Java source line #113	-> byte code offset #499
/*     */     //   Java source line #115	-> byte code offset #508
/*     */     //   Java source line #116	-> byte code offset #510
/*     */     //   Java source line #115	-> byte code offset #512
/*     */     //   Java source line #118	-> byte code offset #515
/*     */     //   Java source line #119	-> byte code offset #520
/*     */     //   Java source line #121	-> byte code offset #531
/*     */     //   Java source line #123	-> byte code offset #541
/*     */     //   Java source line #124	-> byte code offset #553
/*     */     //   Java source line #123	-> byte code offset #564
/*     */     //   Java source line #126	-> byte code offset #567
/*     */     //   Java source line #128	-> byte code offset #576
/*     */     //   Java source line #129	-> byte code offset #578
/*     */     //   Java source line #128	-> byte code offset #580
/*     */     //   Java source line #131	-> byte code offset #583
/*     */     //   Java source line #132	-> byte code offset #588
/*     */     //   Java source line #134	-> byte code offset #599
/*     */     //   Java source line #135	-> byte code offset #609
/*     */     //   Java source line #137	-> byte code offset #618
/*     */     //   Java source line #138	-> byte code offset #620
/*     */     //   Java source line #137	-> byte code offset #622
/*     */     //   Java source line #140	-> byte code offset #625
/*     */     //   Java source line #141	-> byte code offset #637
/*     */     //   Java source line #140	-> byte code offset #648
/*     */     //   Java source line #143	-> byte code offset #651
/*     */     //   Java source line #144	-> byte code offset #653
/*     */     //   Java source line #143	-> byte code offset #655
/*     */     //   Java source line #146	-> byte code offset #658
/*     */     //   Java source line #147	-> byte code offset #663
/*     */     //   Java source line #149	-> byte code offset #674
/*     */     //   Java source line #151	-> byte code offset #684
/*     */     //   Java source line #152	-> byte code offset #696
/*     */     //   Java source line #151	-> byte code offset #707
/*     */     //   Java source line #154	-> byte code offset #710
/*     */     //   Java source line #155	-> byte code offset #719
/*     */     //   Java source line #156	-> byte code offset #723
/*     */     //   Java source line #155	-> byte code offset #725
/*     */     //   Java source line #159	-> byte code offset #730
/*     */     //   Java source line #160	-> byte code offset #740
/*     */     //   Java source line #161	-> byte code offset #750
/*     */     //   Java source line #162	-> byte code offset #752
/*     */     //   Java source line #161	-> byte code offset #759
/*     */     //   Java source line #164	-> byte code offset #761
/*     */     //   Java source line #165	-> byte code offset #771
/*     */     //   Java source line #166	-> byte code offset #773
/*     */     //   Java source line #169	-> byte code offset #781
/*     */     //   Java source line #170	-> byte code offset #783
/*     */     //   Java source line #169	-> byte code offset #785
/*     */     //   Java source line #171	-> byte code offset #791
/*     */     //   Java source line #172	-> byte code offset #793
/*     */     //   Java source line #173	-> byte code offset #801
/*     */     //   Java source line #174	-> byte code offset #803
/*     */     //   Java source line #178	-> byte code offset #811
/*     */     //   Java source line #179	-> byte code offset #813
/*     */     //   Java source line #177	-> byte code offset #815
/*     */     //   Java source line #180	-> byte code offset #821
/*     */     //   Java source line #181	-> byte code offset #823
/*     */     //   Java source line #182	-> byte code offset #831
/*     */     //   Java source line #183	-> byte code offset #833
/*     */     //   Java source line #186	-> byte code offset #841
/*     */     //   Java source line #187	-> byte code offset #843
/*     */     //   Java source line #186	-> byte code offset #845
/*     */     //   Java source line #188	-> byte code offset #851
/*     */     //   Java source line #189	-> byte code offset #853
/*     */     //   Java source line #192	-> byte code offset #861
/*     */     //   Java source line #193	-> byte code offset #863
/*     */     //   Java source line #192	-> byte code offset #865
/*     */     //   Java source line #197	-> byte code offset #871
/*     */     //   Java source line #198	-> byte code offset #881
/*     */     //   Java source line #200	-> byte code offset #891
/*     */     //   Java source line #201	-> byte code offset #893
/*     */     //   Java source line #200	-> byte code offset #900
/*     */     //   Java source line #202	-> byte code offset #902
/*     */     //   Java source line #203	-> byte code offset #904
/*     */     //   Java source line #204	-> byte code offset #912
/*     */     //   Java source line #205	-> byte code offset #914
/*     */     //   Java source line #208	-> byte code offset #922
/*     */     //   Java source line #209	-> byte code offset #924
/*     */     //   Java source line #207	-> byte code offset #926
/*     */     //   Java source line #212	-> byte code offset #932
/*     */     //   Java source line #213	-> byte code offset #934
/*     */     //   Java source line #211	-> byte code offset #936
/*     */     //   Java source line #216	-> byte code offset #942
/*     */     //   Java source line #217	-> byte code offset #944
/*     */     //   Java source line #218	-> byte code offset #952
/*     */     //   Java source line #222	-> byte code offset #962
/*     */     //   Java source line #223	-> byte code offset #964
/*     */     //   Java source line #222	-> byte code offset #966
/*     */     //   Java source line #225	-> byte code offset #972
/*     */     //   Java source line #226	-> byte code offset #974
/*     */     //   Java source line #227	-> byte code offset #982
/*     */     //   Java source line #231	-> byte code offset #992
/*     */     //   Java source line #232	-> byte code offset #994
/*     */     //   Java source line #231	-> byte code offset #996
/*     */     //   Java source line #234	-> byte code offset #1002
/*     */     //   Java source line #235	-> byte code offset #1004
/*     */     //   Java source line #236	-> byte code offset #1012
/*     */     //   Java source line #240	-> byte code offset #1022
/*     */     //   Java source line #241	-> byte code offset #1024
/*     */     //   Java source line #240	-> byte code offset #1026
/*     */     //   Java source line #243	-> byte code offset #1032
/*     */     //   Java source line #244	-> byte code offset #1034
/*     */     //   Java source line #248	-> byte code offset #1043
/*     */     //   Java source line #249	-> byte code offset #1045
/*     */     //   Java source line #248	-> byte code offset #1047
/*     */     //   Java source line #251	-> byte code offset #1053
/*     */     //   Java source line #252	-> byte code offset #1055
/*     */     //   Java source line #255	-> byte code offset #1064
/*     */     //   Java source line #256	-> byte code offset #1066
/*     */     //   Java source line #255	-> byte code offset #1068
/*     */     //   Java source line #258	-> byte code offset #1074
/*     */     //   Java source line #259	-> byte code offset #1076
/*     */     //   Java source line #263	-> byte code offset #1085
/*     */     //   Java source line #264	-> byte code offset #1087
/*     */     //   Java source line #262	-> byte code offset #1089
/*     */     //   Java source line #266	-> byte code offset #1095
/*     */     //   Java source line #267	-> byte code offset #1097
/*     */     //   Java source line #271	-> byte code offset #1106
/*     */     //   Java source line #272	-> byte code offset #1108
/*     */     //   Java source line #270	-> byte code offset #1110
/*     */     //   Java source line #274	-> byte code offset #1116
/*     */     //   Java source line #275	-> byte code offset #1118
/*     */     //   Java source line #280	-> byte code offset #1127
/*     */     //   Java source line #281	-> byte code offset #1129
/*     */     //   Java source line #279	-> byte code offset #1131
/*     */     //   Java source line #286	-> byte code offset #1134
/*     */     //   Java source line #287	-> byte code offset #1139
/*     */     //   Java source line #291	-> byte code offset #1147
/*     */     //   Java source line #44	-> byte code offset #1157
/*     */     //   Java source line #296	-> byte code offset #1167
/*     */     //   Java source line #34	-> byte code offset #1176
/*     */     //   Java source line #298	-> byte code offset #1186
/*     */     //   Java source line #299	-> byte code offset #1199
/*     */     //   Java source line #298	-> byte code offset #1211
/*     */     //   Java source line #302	-> byte code offset #1217
/*     */     //   Java source line #303	-> byte code offset #1218
/*     */     //   Java source line #310	-> byte code offset #1241
/*     */     //   Java source line #312	-> byte code offset #1248
/*     */     //   Java source line #313	-> byte code offset #1250
/*     */     //   Java source line #308	-> byte code offset #1262
/*     */     //   Java source line #310	-> byte code offset #1264
/*     */     //   Java source line #312	-> byte code offset #1271
/*     */     //   Java source line #313	-> byte code offset #1273
/*     */     //   Java source line #315	-> byte code offset #1282
/*     */     //   Java source line #310	-> byte code offset #1285
/*     */     //   Java source line #312	-> byte code offset #1292
/*     */     //   Java source line #313	-> byte code offset #1294
/*     */     //   Java source line #316	-> byte code offset #1303
/*     */     // Local variable table:
/*     */     //   start	length	slot	name	signature
/*     */     //   0	1305	0	confirmationFile	java.util.List<java.io.File>
/*     */     //   7	1297	1	confirmationReportObjectList	java.util.List<ConfirmationReportObject>
/*     */     //   9	1277	2	bufferedReader	java.io.BufferedReader
/*     */     //   13	80	3	length	int
/*     */     //   1217	15	3	e	Exception
/*     */     //   44	11	4	file	java.io.File
/*     */     //   29	1148	5	localIterator	java.util.Iterator
/*     */     //   70	1099	6	confirmationReportObject	ConfirmationReportObject
/*     */     //   84	3	7	line	String
/*     */     //   1162	3	7	line	String
/*     */     //   79	1069	8	stringBuffer	StringBuffer
/*     */     //   108	567	9	reportPart	String
/*     */     //   506	15	10	rejectedTABRecord	com.americanexpress.ips.gfsg.beans.conformation.ConfirmationRejectedRecordBean
/*     */     //   574	15	10	rejectedTAdRecord	com.americanexpress.ips.gfsg.beans.conformation.ConfirmationRejectedRecordBean
/*     */     //   616	48	10	rejectedTBTRecord	com.americanexpress.ips.gfsg.beans.conformation.ConfirmationRejectedRecordBean
/*     */     //   717	423	10	rejectedTAARecord	com.americanexpress.ips.gfsg.beans.conformation.ConfirmationRejectedRecordBean
/*     */     //   728	389	11	formatCode	String
/*     */     //   759	93	12	addendTypeCode	String
/*     */     //   900	13	12	addendTypeCode	String
/*     */     //   1262	21	13	localObject	Object
/*     */     //   1248	3	14	ioe	java.io.IOException
/*     */     //   1271	3	14	ioe	java.io.IOException
/*     */     //   1292	3	14	ioe	java.io.IOException
/*     */     // Exception table:
/*     */     //   from	to	target	type
/*     */     //   10	1214	1217	java/lang/Exception
/*     */     //   1241	1245	1248	java/io/IOException
/*     */     //   10	1241	1262	finally
/*     */     //   1264	1268	1271	java/io/IOException
/*     */     //   1285	1289	1292	java/io/IOException
/*     */   }
/*     */   
/*     */   private static void parseReportHeader(StringBuffer recordType, ConfirmationReportObject confirmationReportObject)
/*     */   {
/* 333 */     ConfirmationReportHeaderBean reportHeader = new ConfirmationReportHeaderBean();
/*     */     
/* 335 */     reportHeader
/* 336 */       .setReocrdType(recordSubstring(recordType.toString(), 0, 3));
/*     */     
/* 338 */     reportHeader.setReportDateAndTime(recordSubstring(
/* 339 */       recordType.toString(), 3, 29));
/*     */     
/* 341 */     reportHeader
/* 342 */       .setReportID(recordSubstring(recordType.toString(), 29, 35));
/*     */     
/* 344 */     confirmationReportObject.setConfirmationReportHeaderBean(reportHeader);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void parseReportBody(StringBuffer recordType, ConfirmationReportObject confirmationReportObject)
/*     */   {
/* 361 */     ConfirmationReportBodyBean reportBodyBean = new ConfirmationReportBodyBean();
/*     */     
/* 363 */     reportBodyBean.setRecordType(
/* 364 */       recordSubstring(recordType.toString(), 0, 3));
/*     */     
/* 366 */     reportBodyBean
/* 367 */       .setReportID(recordSubstring(recordType.toString(), 3, 9));
/*     */     
/* 369 */     reportBodyBean.setReportName(
/* 370 */       recordSubstring(recordType.toString(), 9, 69));
/* 371 */     reportBodyBean.setSubmitterName(
/* 372 */       recordSubstring(recordType.toString(), 69, 109));
/* 373 */     reportBodyBean.setReportDateTime(
/* 374 */       recordSubstring(recordType.toString(), 109, 135));
/* 375 */     reportBodyBean.setSubmitterID(
/* 376 */       recordSubstring(recordType.toString(), 135, 146));
/* 377 */     reportBodyBean.setSubmitterFileRefNumber(recordSubstring(
/* 378 */       recordType.toString(), 146, 155));
/* 379 */     reportBodyBean.setFileStatus(
/* 380 */       recordSubstring(recordType.toString(), 155, 195));
/* 381 */     reportBodyBean.setFileCreationDateTime(recordSubstring(
/* 382 */       recordType.toString(), 195, 217));
/* 383 */     reportBodyBean.setFileTrackingNumber(recordSubstring(
/* 384 */       recordType.toString(), 217, 231));
/* 385 */     reportBodyBean.setFileReceiptDateTime(recordSubstring(
/* 386 */       recordType.toString(), 231, 257));
/* 387 */     reportBodyBean.setTotalNumberOfRecords(recordSubstring(
/* 388 */       recordType.toString(), 257, 265));
/* 389 */     reportBodyBean.setHashTotalAmount(recordSubstring(
/* 390 */       recordType.toString(), 265, 285));
/* 391 */     reportBodyBean.setTotalNumberOfDebits(recordSubstring(
/* 392 */       recordType.toString(), 285, 293));
/* 393 */     reportBodyBean.setDebitHashTotalAmount(recordSubstring(
/* 394 */       recordType.toString(), 293, 313));
/* 395 */     reportBodyBean.setTotalNumberOfCredits(recordSubstring(
/* 396 */       recordType.toString(), 313, 321));
/* 397 */     reportBodyBean.setCreditHashTotalAmount(recordSubstring(
/* 398 */       recordType.toString(), 321, 341));
/* 399 */     reportBodyBean.setTotalRejectedRecords(recordSubstring(
/* 400 */       recordType.toString(), 341, 349));
/* 401 */     reportBodyBean.setTotalSuspendedBatches(recordSubstring(
/* 402 */       recordType.toString(), 349, 357));
/* 403 */     reportBodyBean.setTotalSuspendedRecords(recordSubstring(
/* 404 */       recordType.toString(), 357, 365));
/*     */     
/* 406 */     confirmationReportObject.setConfirmationReportBodyBean(reportBodyBean);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void parseReportSummary(StringBuffer recordType, ConfirmationReportObject confirmationReportObject)
/*     */   {
/* 423 */     ConfirmationReportSummaryBean reportSummaryBean = new ConfirmationReportSummaryBean();
/*     */     
/* 425 */     reportSummaryBean.setRecordType(
/* 426 */       recordSubstring(recordType.toString(), 0, 3));
/*     */     
/* 428 */     reportSummaryBean.setErrorCode(
/* 429 */       recordSubstring(recordType.toString(), 3, 7));
/*     */     
/* 431 */     reportSummaryBean.setNumberOfOccurrences(recordSubstring(
/* 432 */       recordType.toString(), 7, 15));
/*     */     
/* 434 */     reportSummaryBean.setErrorText(
/* 435 */       recordSubstring(recordType.toString(), 15, 115));
/*     */     
/* 437 */     confirmationReportObject.addErrorSummary(reportSummaryBean);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void parseReportRequiringRejection(StringBuffer recordType, ConfirmationReportObject confirmationReportObject)
/*     */   {
/* 454 */     ConfirmationErrorReportBean errorReportBean = new ConfirmationErrorReportBean();
/*     */     
/* 456 */     errorReportBean.setRecordType(
/* 457 */       recordSubstring(recordType.toString(), 0, 3));
/*     */     
/* 459 */     errorReportBean.setErrorCode(
/* 460 */       recordSubstring(recordType.toString(), 3, 7));
/*     */     
/* 462 */     recordNumSubstring(recordType.substring(7, 695), errorReportBean);
/*     */     
/* 464 */     confirmationReportObject
/* 465 */       .addErrorsRequiringRejectionRecord(errorReportBean);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void parseReportNotRequiringRejection(StringBuffer recordType, ConfirmationReportObject confirmationReportObject)
/*     */   {
/* 484 */     ConfirmationErrorReportBean errorReportBean = new ConfirmationErrorReportBean();
/*     */     
/* 486 */     errorReportBean.setRecordType(
/* 487 */       recordSubstring(recordType.toString(), 0, 3));
/*     */     
/* 489 */     errorReportBean.setErrorCode(
/* 490 */       recordSubstring(recordType.toString(), 3, 7));
/*     */     
/* 492 */     recordNumSubstring(recordType.substring(7, 695), errorReportBean);
/*     */     
/* 494 */     confirmationReportObject
/* 495 */       .addErrorsNotRequiringRejectionRecord(errorReportBean);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void parseReportBatchSuspension(StringBuffer recordType, ConfirmationReportObject confirmationReportObject)
/*     */   {
/* 513 */     ConfirmationErrorReportBean errorReportBean = new ConfirmationErrorReportBean();
/*     */     
/* 515 */     errorReportBean.setRecordType(
/* 516 */       recordSubstring(recordType.toString(), 0, 3));
/*     */     
/* 518 */     errorReportBean.setErrorCode(
/* 519 */       recordSubstring(recordType.toString(), 3, 7));
/*     */     
/* 521 */     recordNumSubstring(recordType.substring(7, 695), errorReportBean);
/*     */     
/* 523 */     confirmationReportObject
/* 524 */       .addErrorsRequiringBatchSuspensionRecord(errorReportBean);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void parseReportRejectedBatch(StringBuffer recordType, ConfirmationReportObject confirmationReportObject)
/*     */   {
/* 542 */     ConfirmationErrorReportBean errorReportBean = new ConfirmationErrorReportBean();
/*     */     
/* 544 */     errorReportBean.setRecordType(
/* 545 */       recordSubstring(recordType.toString(), 0, 3));
/*     */     
/* 547 */     errorReportBean.setErrorCode(
/* 548 */       recordSubstring(recordType.toString(), 3, 7));
/*     */     
/* 550 */     recordNumSubstring(recordType.substring(7, 695), errorReportBean);
/*     */     
/* 552 */     confirmationReportObject
/* 553 */       .addPreSuspendedRejectedBatchRecord(errorReportBean);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void parseReportProcessedBatch(StringBuffer recordType, ConfirmationReportObject confirmationReportObject)
/*     */   {
/* 572 */     ConfirmationErrorReportBean errorReportBean = new ConfirmationErrorReportBean();
/*     */     
/* 574 */     errorReportBean.setRecordType(
/* 575 */       recordSubstring(recordType.toString(), 0, 3));
/*     */     
/* 577 */     errorReportBean.setErrorCode(
/* 578 */       recordSubstring(recordType.toString(), 3, 7));
/*     */     
/* 580 */     recordNumSubstring(recordType.substring(7, 695), errorReportBean);
/*     */     
/* 582 */     confirmationReportObject
/* 583 */       .addPreSuspendedProcessedRejectedBatchRecord(errorReportBean);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private static String recordSubstring(String s, int start, int end)
/*     */   {
/* 590 */     String s1 = s.substring(start, end);
/* 591 */     return s1.replaceAll("~", " ");
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private static void recordNumSubstring(String s, ConfirmationErrorReportBean errorReportBean)
/*     */   {
/* 598 */     for (int i = 0; i < s.length();) {
/* 599 */       if (!s.substring(i, i + 8).equals("00000000")) {
/* 600 */         errorReportBean.addRecordNumber(s.substring(i, i + 8));
/*     */       }
/*     */       
/*     */ 
/* 604 */       i += 8;
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\messagecreator\ConfirmationReportObjectCreator.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */