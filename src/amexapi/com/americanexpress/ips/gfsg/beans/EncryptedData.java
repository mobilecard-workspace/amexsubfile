/*    */ package com.americanexpress.ips.gfsg.beans;
/*    */ 
/*    */ import java.security.AlgorithmParameters;
/*    */ import java.security.Key;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class EncryptedData
/*    */ {
/* 16 */   String encryptedUserID = null;
/* 17 */   String encryptedPwd = null;
/*    */   
/* 19 */   String decryptedUserID = null;
/* 20 */   String decryptedPwd = null;
/*    */   
/* 22 */   Key encKey = null;
/*    */   
/* 24 */   AlgorithmParameters param = null;
/*    */   
/*    */ 
/*    */   public AlgorithmParameters getParam()
/*    */   {
/* 29 */     return this.param;
/*    */   }
/*    */   
/*    */ 
/*    */   public void setParam(AlgorithmParameters param)
/*    */   {
/* 35 */     this.param = param;
/*    */   }
/*    */   
/*    */ 
/*    */   public Key getEncKey()
/*    */   {
/* 41 */     return this.encKey;
/*    */   }
/*    */   
/*    */ 
/*    */   public void setEncKey(Key encKey)
/*    */   {
/* 47 */     this.encKey = encKey;
/*    */   }
/*    */   
/*    */ 
/*    */   public String getEncryptedUserID()
/*    */   {
/* 53 */     return this.encryptedUserID;
/*    */   }
/*    */   
/*    */ 
/*    */   public String getDecryptedUserID()
/*    */   {
/* 59 */     return this.decryptedUserID;
/*    */   }
/*    */   
/*    */ 
/*    */   public void setDecryptedUserID(String decryptedUserID)
/*    */   {
/* 65 */     this.decryptedUserID = decryptedUserID;
/*    */   }
/*    */   
/*    */ 
/*    */   public String getDecryptedPwd()
/*    */   {
/* 71 */     return this.decryptedPwd;
/*    */   }
/*    */   
/*    */ 
/*    */   public void setDecryptedPwd(String decryptedPwd)
/*    */   {
/* 77 */     this.decryptedPwd = decryptedPwd;
/*    */   }
/*    */   
/*    */ 
/*    */   public void setEncryptedUserID(String encryptedUserID)
/*    */   {
/* 83 */     this.encryptedUserID = encryptedUserID;
/*    */   }
/*    */   
/*    */ 
/*    */   public String getEncryptedPwd()
/*    */   {
/* 89 */     return this.encryptedPwd;
/*    */   }
/*    */   
/*    */ 
/*    */   public void setEncryptedPwd(String setEncryptedPwd)
/*    */   {
/* 95 */     this.encryptedPwd = setEncryptedPwd;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\EncryptedData.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */