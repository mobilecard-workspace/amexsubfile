/*     */ package com.americanexpress.ips.gfsg.beans;
/*     */ 
/*     */ import java.util.List;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class SettlementResponseDataObject
/*     */ {
/*  14 */   private String merchantId = "";
/*  15 */   private String numberOfDebits = "";
/*  16 */   private String hashTotalDebitAmount = "";
/*  17 */   private String numberOfCredits = "";
/*  18 */   private String hashTotalCreditAmount = "";
/*  19 */   private String hashTotalAmount = "";
/*  20 */   private String errorCode = "";
/*  21 */   private String fileStatus = "";
/*  22 */   private String submitterId = "";
/*  23 */   private String submitterFileReferenceNumber = "";
/*  24 */   private String submitterFileSequenceNumber = "";
/*  25 */   private String fileCreationDateTime = "";
/*  26 */   private String fileReceiptDateTime = "";
/*     */   
/*     */   private List<ErrorObject> actionCodeDescription;
/*     */   
/*     */ 
/*     */   public String getMerchantId()
/*     */   {
/*  33 */     return this.merchantId;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getNumberOfDebits()
/*     */   {
/*  39 */     return this.numberOfDebits;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getHashTotalDebitAmount()
/*     */   {
/*  45 */     return this.hashTotalDebitAmount;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getNumberOfCredits()
/*     */   {
/*  51 */     return this.numberOfCredits;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getHashTotalCreditAmount()
/*     */   {
/*  57 */     return this.hashTotalCreditAmount;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getHashTotalAmount()
/*     */   {
/*  63 */     return this.hashTotalAmount;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getErrorCode()
/*     */   {
/*  69 */     return this.errorCode;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getFileStatus()
/*     */   {
/*  75 */     return this.fileStatus;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getSubmitterId()
/*     */   {
/*  81 */     return this.submitterId;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getSubmitterFileReferenceNumber()
/*     */   {
/*  87 */     return this.submitterFileReferenceNumber;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getSubmitterFileSequenceNumber()
/*     */   {
/*  93 */     return this.submitterFileSequenceNumber;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getFileCreationDateTime()
/*     */   {
/*  99 */     return this.fileCreationDateTime;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getFileReceiptDateTime()
/*     */   {
/* 105 */     return this.fileReceiptDateTime;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setMerchantId(String merchantId)
/*     */   {
/* 111 */     this.merchantId = merchantId;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setNumberOfDebits(String numberOfDebits)
/*     */   {
/* 117 */     this.numberOfDebits = numberOfDebits;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setHashTotalDebitAmount(String hashTotalDebitAmount)
/*     */   {
/* 123 */     this.hashTotalDebitAmount = hashTotalDebitAmount;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setNumberOfCredits(String numberOfCredits)
/*     */   {
/* 129 */     this.numberOfCredits = numberOfCredits;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setHashTotalCreditAmount(String hashTotalCreditAmount)
/*     */   {
/* 135 */     this.hashTotalCreditAmount = hashTotalCreditAmount;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setHashTotalAmount(String hashTotalAmount)
/*     */   {
/* 141 */     this.hashTotalAmount = hashTotalAmount;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setErrorCode(String errorCode)
/*     */   {
/* 147 */     this.errorCode = errorCode;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setFileStatus(String fileStatus)
/*     */   {
/* 153 */     this.fileStatus = fileStatus;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setSubmitterId(String submitterId)
/*     */   {
/* 159 */     this.submitterId = submitterId;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setSubmitterFileReferenceNumber(String submitterFileReferenceNumber)
/*     */   {
/* 165 */     this.submitterFileReferenceNumber = submitterFileReferenceNumber;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setSubmitterFileSequenceNumber(String submitterFileSequenceNumber)
/*     */   {
/* 171 */     this.submitterFileSequenceNumber = submitterFileSequenceNumber;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setFileCreationDateTime(String fileCreationDateTime)
/*     */   {
/* 177 */     this.fileCreationDateTime = fileCreationDateTime;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setFileReceiptDateTime(String fileReceiptDateTime)
/*     */   {
/* 183 */     this.fileReceiptDateTime = fileReceiptDateTime;
/*     */   }
/*     */   
/*     */ 
/*     */   public List<ErrorObject> getActionCodeDescription()
/*     */   {
/* 189 */     return this.actionCodeDescription;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setActionCodeDescription(List<ErrorObject> actionCodeDescription)
/*     */   {
/* 195 */     this.actionCodeDescription = actionCodeDescription;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\SettlementResponseDataObject.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */