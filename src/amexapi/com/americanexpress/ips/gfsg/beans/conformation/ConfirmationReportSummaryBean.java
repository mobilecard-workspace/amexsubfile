/*    */ package com.americanexpress.ips.gfsg.beans.conformation;
/*    */ 
/*    */ 
/*    */ 
/*    */ public class ConfirmationReportSummaryBean
/*    */ {
/*    */   private String recordType;
/*    */   
/*    */ 
/*    */   private String numberOfOccurrences;
/*    */   
/*    */ 
/*    */   private String errorCode;
/*    */   
/*    */   private String errorText;
/*    */   
/*    */   private String sumReserved;
/*    */   
/*    */ 
/*    */   public String getRecordType()
/*    */   {
/* 22 */     return this.recordType;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public void setRecordType(String recordType)
/*    */   {
/* 29 */     this.recordType = recordType;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public String getErrorCode()
/*    */   {
/* 36 */     return this.errorCode;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public void setErrorCode(String errorCode)
/*    */   {
/* 43 */     this.errorCode = errorCode;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public String getErrorText()
/*    */   {
/* 50 */     return this.errorText;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public void setErrorText(String errorText)
/*    */   {
/* 57 */     this.errorText = errorText;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public String getSumReserved()
/*    */   {
/* 64 */     return this.sumReserved;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public void setSumReserved(String sumReserved)
/*    */   {
/* 71 */     this.sumReserved = sumReserved;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public String getNumberOfOccurrences()
/*    */   {
/* 78 */     return this.numberOfOccurrences;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public void setNumberOfOccurrences(String numberOfOccurrences)
/*    */   {
/* 85 */     this.numberOfOccurrences = numberOfOccurrences;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\conformation\ConfirmationReportSummaryBean.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */