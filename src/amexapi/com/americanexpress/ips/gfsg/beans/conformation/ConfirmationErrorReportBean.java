/*    */ package com.americanexpress.ips.gfsg.beans.conformation;
/*    */ 
/*    */ import java.util.ArrayList;
/*    */ import java.util.List;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class ConfirmationErrorReportBean
/*    */ {
/*    */   private String recordType;
/*    */   private String errorCode;
/* 18 */   private List<String> recordNumber = new ArrayList();
/*    */   
/*    */ 
/*    */   private String cerReserved;
/*    */   
/*    */ 
/*    */   public List<String> getRecordNumber()
/*    */   {
/* 26 */     return this.recordNumber;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public void addRecordNumber(String recNumber)
/*    */   {
/* 34 */     this.recordNumber.add(recNumber);
/*    */   }
/*    */   
/*    */ 
/*    */   public String getRecordType()
/*    */   {
/* 40 */     return this.recordType;
/*    */   }
/*    */   
/*    */ 
/*    */   public void setRecordType(String recordType)
/*    */   {
/* 46 */     this.recordType = recordType;
/*    */   }
/*    */   
/*    */ 
/*    */   public String getErrorCode()
/*    */   {
/* 52 */     return this.errorCode;
/*    */   }
/*    */   
/*    */ 
/*    */   public void setErrorCode(String errorCode)
/*    */   {
/* 58 */     this.errorCode = errorCode;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public String getCerReserved()
/*    */   {
/* 65 */     return this.cerReserved;
/*    */   }
/*    */   
/*    */ 
/*    */   public void setCerReserved(String cerReserved)
/*    */   {
/* 71 */     this.cerReserved = cerReserved;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\conformation\ConfirmationErrorReportBean.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */