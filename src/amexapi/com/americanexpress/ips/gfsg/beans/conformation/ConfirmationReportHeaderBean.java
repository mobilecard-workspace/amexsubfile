/*    */ package com.americanexpress.ips.gfsg.beans.conformation;
/*    */ 
/*    */ 
/*    */ 
/*    */ public class ConfirmationReportHeaderBean
/*    */ {
/*    */   private String reocrdType;
/*    */   
/*    */ 
/*    */   private String reportDateAndTime;
/*    */   
/*    */ 
/*    */   private String reportID;
/*    */   
/*    */ 
/*    */   private String hdrReserved;
/*    */   
/*    */ 
/*    */   public String getReocrdType()
/*    */   {
/* 21 */     return this.reocrdType;
/*    */   }
/*    */   
/*    */ 
/*    */   public void setReocrdType(String reocrdType)
/*    */   {
/* 27 */     this.reocrdType = reocrdType;
/*    */   }
/*    */   
/*    */ 
/*    */   public String getReportDateAndTime()
/*    */   {
/* 33 */     return this.reportDateAndTime;
/*    */   }
/*    */   
/*    */ 
/*    */   public void setReportDateAndTime(String reportDateAndTime)
/*    */   {
/* 39 */     this.reportDateAndTime = reportDateAndTime;
/*    */   }
/*    */   
/*    */ 
/*    */   public String getReportID()
/*    */   {
/* 45 */     return this.reportID;
/*    */   }
/*    */   
/*    */ 
/*    */   public void setReportID(String reportID)
/*    */   {
/* 51 */     this.reportID = reportID;
/*    */   }
/*    */   
/*    */ 
/*    */   public String getHdrReserved()
/*    */   {
/* 57 */     return this.hdrReserved;
/*    */   }
/*    */   
/*    */ 
/*    */   public void setHdrReserved(String hdrReserved)
/*    */   {
/* 63 */     this.hdrReserved = hdrReserved;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\conformation\ConfirmationReportHeaderBean.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */