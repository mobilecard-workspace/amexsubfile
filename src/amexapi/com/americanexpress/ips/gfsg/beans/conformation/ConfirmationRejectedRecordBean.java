/*    */ package com.americanexpress.ips.gfsg.beans.conformation;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class ConfirmationRejectedRecordBean
/*    */ {
/*    */   private String returnedaRecordType;
/*    */   
/*    */ 
/*    */ 
/*    */   private String returnedRecordNumber;
/*    */   
/*    */ 
/*    */ 
/*    */   private RejectedRecordDataBean returnedRecordData;
/*    */   
/*    */ 
/*    */ 
/*    */   public String getReturnedaRecordType()
/*    */   {
/* 22 */     return this.returnedaRecordType;
/*    */   }
/*    */   
/*    */ 
/*    */   public void setReturnedaRecordType(String returnedaRecordType)
/*    */   {
/* 28 */     this.returnedaRecordType = returnedaRecordType;
/*    */   }
/*    */   
/*    */ 
/*    */   public String getReturnedRecordNumber()
/*    */   {
/* 34 */     return this.returnedRecordNumber;
/*    */   }
/*    */   
/*    */ 
/*    */   public void setReturnedRecordNumber(String returnedRecordNumber)
/*    */   {
/* 40 */     this.returnedRecordNumber = returnedRecordNumber;
/*    */   }
/*    */   
/*    */ 
/*    */   public RejectedRecordDataBean getReturnedRecordData()
/*    */   {
/* 46 */     return this.returnedRecordData;
/*    */   }
/*    */   
/*    */ 
/*    */   public void setReturnedRecordData(RejectedRecordDataBean returnedRecordData)
/*    */   {
/* 52 */     this.returnedRecordData = returnedRecordData;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\conformation\ConfirmationRejectedRecordBean.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */