/*      */ package com.americanexpress.ips.gfsg.beans.conformation;
/*      */ 
/*      */ import com.americanexpress.ips.gfsg.enumerations.RecordType;
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ public class RejectedTABRecord
/*      */ {
/*   22 */   private String recordType = RecordType.Transaction_Advice_Basic.getRecordType();
/*      */   
/*      */   private String recordNumber;
/*      */   
/*      */   private String transactionIdentifier;
/*      */   
/*      */   private String formatCode;
/*      */   
/*      */   private String mediaCode;
/*      */   
/*      */   private String submissionMethod;
/*      */   
/*      */   private String approvalCode;
/*      */   
/*      */   private String primaryAccountNumber;
/*      */   
/*      */   private String cardExpiryDate;
/*      */   
/*      */   private String transactionDate;
/*      */   
/*      */   private String transactionTime;
/*      */   private String transactionAmount;
/*      */   private String processingCode;
/*      */   private String transactionCurrencyCode;
/*      */   private String extendedPaymentData;
/*      */   private String merchantId;
/*      */   private String merchantLocationId;
/*      */   private String merchantContactInfo;
/*      */   private String terminalId;
/*      */   private String posDataCode;
/*      */   private String invoiceReferenceNumber;
/*      */   private String tabImageSequenceNumber;
/*      */   private String matchingKeyType;
/*      */   private String matchingKey;
/*      */   private String electronicCommerceIndicator;
/*      */   private String tabField7reserved;
/*      */   private String tabField13reserved;
/*      */   private String tabField23reserved;
/*      */   private String tabField24reserved;
/*      */   private String tabField25reserved;
/*      */   private String tabField27reserved;
/*      */   private String tabField32reserved;
/*      */   
/*      */   public String getRecordType()
/*      */   {
/*   67 */     return this.recordType;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setRecordType(String recordType)
/*      */   {
/*   83 */     this.recordType = recordType;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getRecordNumber()
/*      */   {
/*   97 */     return this.recordNumber;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setRecordNumber(String recordNumber)
/*      */   {
/*  112 */     this.recordNumber = recordNumber;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTransactionIdentifier()
/*      */   {
/*  128 */     return this.transactionIdentifier;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTransactionIdentifier(String transactionIdentifier)
/*      */   {
/*  145 */     this.transactionIdentifier = transactionIdentifier;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getFormatCode()
/*      */   {
/*  174 */     return this.formatCode;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setFormatCode(String formatCode)
/*      */   {
/*  204 */     this.formatCode = formatCode;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getMediaCode()
/*      */   {
/*  225 */     return this.mediaCode;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setMediaCode(String mediaCode)
/*      */   {
/*  247 */     this.mediaCode = mediaCode;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getSubmissionMethod()
/*      */   {
/*  272 */     return this.submissionMethod;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSubmissionMethod(String submissionMethod)
/*      */   {
/*  297 */     this.submissionMethod = submissionMethod;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getApprovalCode()
/*      */   {
/*  312 */     return this.approvalCode;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setApprovalCode(String approvalCode)
/*      */   {
/*  327 */     this.approvalCode = approvalCode;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getPrimaryAccountNumber()
/*      */   {
/*  340 */     return this.primaryAccountNumber;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setPrimaryAccountNumber(String primaryAccountNumber)
/*      */   {
/*  354 */     this.primaryAccountNumber = primaryAccountNumber;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getCardExpiryDate()
/*      */   {
/*  373 */     return this.cardExpiryDate;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setCardExpiryDate(String cardExpiryDate)
/*      */   {
/*  392 */     this.cardExpiryDate = cardExpiryDate;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTransactionDate()
/*      */   {
/*  413 */     return this.transactionDate;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTransactionDate(String transactionDate)
/*      */   {
/*  435 */     this.transactionDate = transactionDate;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTransactionTime()
/*      */   {
/*  455 */     return this.transactionTime;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTransactionTime(String transactionTime)
/*      */   {
/*  476 */     this.transactionTime = transactionTime;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTransactionAmount()
/*      */   {
/*  491 */     return this.transactionAmount;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTransactionAmount(String transactionAmount)
/*      */   {
/*  507 */     this.transactionAmount = transactionAmount;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getProcessingCode()
/*      */   {
/*  531 */     return this.processingCode;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setProcessingCode(String processingCode)
/*      */   {
/*  556 */     this.processingCode = processingCode;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTransactionCurrencyCode()
/*      */   {
/*  571 */     return this.transactionCurrencyCode;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTransactionCurrencyCode(String transactionCurrencyCode)
/*      */   {
/*  587 */     this.transactionCurrencyCode = transactionCurrencyCode;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getExtendedPaymentData()
/*      */   {
/*  602 */     return this.extendedPaymentData;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setExtendedPaymentData(String extendedPaymentData)
/*      */   {
/*  618 */     this.extendedPaymentData = extendedPaymentData;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getMerchantId()
/*      */   {
/*  633 */     return this.merchantId;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setMerchantId(String merchantId)
/*      */   {
/*  649 */     this.merchantId = merchantId;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getMerchantLocationId()
/*      */   {
/*  663 */     return this.merchantLocationId;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setMerchantLocationId(String merchantLocationId)
/*      */   {
/*  678 */     this.merchantLocationId = merchantLocationId;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getMerchantContactInfo()
/*      */   {
/*  691 */     return this.merchantContactInfo;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setMerchantContactInfo(String merchantContactInfo)
/*      */   {
/*  705 */     this.merchantContactInfo = merchantContactInfo;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTerminalId()
/*      */   {
/*  719 */     return this.terminalId;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTerminalId(String terminalId)
/*      */   {
/*  734 */     this.terminalId = terminalId;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getPosDataCode()
/*      */   {
/*  748 */     return this.posDataCode;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setPosDataCode(String posDataCode)
/*      */   {
/*  763 */     this.posDataCode = posDataCode;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getInvoiceReferenceNumber()
/*      */   {
/*  777 */     return this.invoiceReferenceNumber;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setInvoiceReferenceNumber(String invoiceReferenceNumber)
/*      */   {
/*  792 */     this.invoiceReferenceNumber = invoiceReferenceNumber;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTabImageSequenceNumber()
/*      */   {
/*  806 */     return this.tabImageSequenceNumber;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTabImageSequenceNumber(String tabImageSequenceNumber)
/*      */   {
/*  821 */     this.tabImageSequenceNumber = tabImageSequenceNumber;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getMatchingKeyType()
/*      */   {
/*  841 */     return this.matchingKeyType;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setMatchingKeyType(String matchingKeyType)
/*      */   {
/*  862 */     this.matchingKeyType = matchingKeyType;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getMatchingKey()
/*      */   {
/*  878 */     return this.matchingKey;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setMatchingKey(String matchingKey)
/*      */   {
/*  895 */     this.matchingKey = matchingKey;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getElectronicCommerceIndicator()
/*      */   {
/*  915 */     return this.electronicCommerceIndicator;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setElectronicCommerceIndicator(String electronicCommerceIndicator)
/*      */   {
/*  937 */     this.electronicCommerceIndicator = electronicCommerceIndicator;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTabField7reserved()
/*      */   {
/*  950 */     return this.tabField7reserved;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTabField7reserved(String tabField7reserved)
/*      */   {
/*  964 */     this.tabField7reserved = tabField7reserved;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTabField13reserved()
/*      */   {
/*  977 */     return this.tabField13reserved;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTabField13reserved(String tabField13reserved)
/*      */   {
/*  991 */     this.tabField13reserved = tabField13reserved;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTabField23reserved()
/*      */   {
/* 1004 */     return this.tabField23reserved;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTabField23reserved(String tabField23reserved)
/*      */   {
/* 1018 */     this.tabField23reserved = tabField23reserved;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTabField24reserved()
/*      */   {
/* 1031 */     return this.tabField24reserved;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTabField24reserved(String tabField24reserved)
/*      */   {
/* 1045 */     this.tabField24reserved = tabField24reserved;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTabField25reserved()
/*      */   {
/* 1058 */     return this.tabField25reserved;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTabField25reserved(String tabField25reserved)
/*      */   {
/* 1072 */     this.tabField25reserved = tabField25reserved;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTabField27reserved()
/*      */   {
/* 1085 */     return this.tabField27reserved;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTabField27reserved(String tabField27reserved)
/*      */   {
/* 1099 */     this.tabField27reserved = tabField27reserved;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTabField32reserved()
/*      */   {
/* 1116 */     return this.tabField32reserved;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTabField32reserved(String tabField32reserved)
/*      */   {
/* 1134 */     this.tabField32reserved = tabField32reserved;
/*      */   }
/*      */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\conformation\RejectedTABRecord.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */