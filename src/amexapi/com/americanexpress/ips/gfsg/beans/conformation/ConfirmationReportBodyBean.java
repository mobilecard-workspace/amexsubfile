/*     */ package com.americanexpress.ips.gfsg.beans.conformation;
/*     */ 
/*     */ 
/*     */ public class ConfirmationReportBodyBean
/*     */ {
/*     */   private String recordType;
/*     */   
/*     */   private String reportID;
/*     */   
/*     */   private String reportName;
/*     */   
/*     */   private String submitterName;
/*     */   
/*     */   private String reportDateTime;
/*     */   
/*     */   private String submitterID;
/*     */   
/*     */   private String submitterFileRefNumber;
/*     */   
/*     */   private String fileStatus;
/*     */   
/*     */   private String fileCreationDateTime;
/*     */   private String fileTrackingNumber;
/*     */   private String fileReceiptDateTime;
/*     */   private String totalNumberOfRecords;
/*     */   private String hashTotalAmount;
/*     */   private String totalNumberOfDebits;
/*     */   private String debitHashTotalAmount;
/*     */   private String totalNumberOfCredits;
/*     */   private String creditHashTotalAmount;
/*     */   private String totalRejectedRecords;
/*     */   private String totalSuspendedBatches;
/*     */   private String totalSuspendedRecords;
/*     */   private String bdyReserved;
/*     */   
/*     */   public String getRecordType()
/*     */   {
/*  38 */     return this.recordType;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setRecordType(String recordType)
/*     */   {
/*  44 */     this.recordType = recordType;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getReportID()
/*     */   {
/*  50 */     return this.reportID;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setReportID(String reportID)
/*     */   {
/*  56 */     this.reportID = reportID;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getReportName()
/*     */   {
/*  62 */     return this.reportName;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setReportName(String reportName)
/*     */   {
/*  68 */     this.reportName = reportName;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getSubmitterName()
/*     */   {
/*  74 */     return this.submitterName;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setSubmitterName(String submitterName)
/*     */   {
/*  80 */     this.submitterName = submitterName;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getReportDateTime()
/*     */   {
/*  86 */     return this.reportDateTime;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setReportDateTime(String reportDateTime)
/*     */   {
/*  92 */     this.reportDateTime = reportDateTime;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getSubmitterID()
/*     */   {
/*  98 */     return this.submitterID;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setSubmitterID(String submitterID)
/*     */   {
/* 104 */     this.submitterID = submitterID;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getSubmitterFileRefNumber()
/*     */   {
/* 110 */     return this.submitterFileRefNumber;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setSubmitterFileRefNumber(String submitterFileRefNumber)
/*     */   {
/* 116 */     this.submitterFileRefNumber = submitterFileRefNumber;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getFileStatus()
/*     */   {
/* 122 */     return this.fileStatus;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setFileStatus(String fileStatus)
/*     */   {
/* 128 */     this.fileStatus = fileStatus;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getFileCreationDateTime()
/*     */   {
/* 134 */     return this.fileCreationDateTime;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setFileCreationDateTime(String fileCreationDateTime)
/*     */   {
/* 140 */     this.fileCreationDateTime = fileCreationDateTime;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getFileTrackingNumber()
/*     */   {
/* 146 */     return this.fileTrackingNumber;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setFileTrackingNumber(String fileTrackingNumber)
/*     */   {
/* 152 */     this.fileTrackingNumber = fileTrackingNumber;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getFileReceiptDateTime()
/*     */   {
/* 158 */     return this.fileReceiptDateTime;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setFileReceiptDateTime(String fileReceiptDateTime)
/*     */   {
/* 164 */     this.fileReceiptDateTime = fileReceiptDateTime;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getTotalNumberOfRecords()
/*     */   {
/* 170 */     return this.totalNumberOfRecords;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setTotalNumberOfRecords(String totalNumberOfRecords)
/*     */   {
/* 176 */     this.totalNumberOfRecords = totalNumberOfRecords;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getHashTotalAmount()
/*     */   {
/* 182 */     return this.hashTotalAmount;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setHashTotalAmount(String hashTotalAmount)
/*     */   {
/* 188 */     this.hashTotalAmount = hashTotalAmount;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getTotalNumberOfDebits()
/*     */   {
/* 194 */     return this.totalNumberOfDebits;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setTotalNumberOfDebits(String totalNumberOfDebits)
/*     */   {
/* 200 */     this.totalNumberOfDebits = totalNumberOfDebits;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getDebitHashTotalAmount()
/*     */   {
/* 206 */     return this.debitHashTotalAmount;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setDebitHashTotalAmount(String debitHashTotalAmount)
/*     */   {
/* 212 */     this.debitHashTotalAmount = debitHashTotalAmount;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getTotalNumberOfCredits()
/*     */   {
/* 218 */     return this.totalNumberOfCredits;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setTotalNumberOfCredits(String totalNumberOfCredits)
/*     */   {
/* 224 */     this.totalNumberOfCredits = totalNumberOfCredits;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getCreditHashTotalAmount()
/*     */   {
/* 230 */     return this.creditHashTotalAmount;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setCreditHashTotalAmount(String creditHashTotalAmount)
/*     */   {
/* 236 */     this.creditHashTotalAmount = creditHashTotalAmount;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getTotalRejectedRecords()
/*     */   {
/* 242 */     return this.totalRejectedRecords;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setTotalRejectedRecords(String totalRejectedRecords)
/*     */   {
/* 248 */     this.totalRejectedRecords = totalRejectedRecords;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getTotalSuspendedBatches()
/*     */   {
/* 254 */     return this.totalSuspendedBatches;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setTotalSuspendedBatches(String totalSuspendedBatches)
/*     */   {
/* 260 */     this.totalSuspendedBatches = totalSuspendedBatches;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getTotalSuspendedRecords()
/*     */   {
/* 266 */     return this.totalSuspendedRecords;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setTotalSuspendedRecords(String totalSuspendedRecords)
/*     */   {
/* 272 */     this.totalSuspendedRecords = totalSuspendedRecords;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getBdyReserved()
/*     */   {
/* 278 */     return this.bdyReserved;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setBdyReserved(String bdyReserved)
/*     */   {
/* 284 */     this.bdyReserved = bdyReserved;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\conformation\ConfirmationReportBodyBean.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */