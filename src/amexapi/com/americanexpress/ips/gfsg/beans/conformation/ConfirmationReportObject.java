/*     */ package com.americanexpress.ips.gfsg.beans.conformation;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class ConfirmationReportObject
/*     */ {
/*     */   private ConfirmationReportBodyBean confirmationReportBodyBean;
/*  16 */   private List<ConfirmationErrorReportBean> errorsRequiringRejectionRecord = new ArrayList();
/*  17 */   private List<ConfirmationErrorReportBean> errorsNotRequiringRejectionRecord = new ArrayList();
/*  18 */   private List<ConfirmationErrorReportBean> errorsRequiringBatchSuspensionRecord = new ArrayList();
/*  19 */   private List<ConfirmationErrorReportBean> preSuspendedRejectedBatchRecord = new ArrayList();
/*  20 */   private List<ConfirmationErrorReportBean> preSuspendedProcessedRejectedBatchRecord = new ArrayList();
/*  21 */   private List<ConfirmationRejectedRecordBean> rejectionRecords = new ArrayList();
/*  22 */   private List<ConfirmationReportSummaryBean> errorSummary = new ArrayList();
/*     */   
/*     */   private ConfirmationReportHeaderBean confirmationReportHeaderBean;
/*     */   
/*     */   public ConfirmationReportBodyBean getConfirmationReportBodyBean()
/*     */   {
/*  28 */     return this.confirmationReportBodyBean;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void setConfirmationReportBodyBean(ConfirmationReportBodyBean confirmationReportBodyBean)
/*     */   {
/*  35 */     this.confirmationReportBodyBean = confirmationReportBodyBean;
/*     */   }
/*     */   
/*     */ 
/*     */   public List<ConfirmationErrorReportBean> getErrorsRequiringRejectionRecord()
/*     */   {
/*  41 */     return this.errorsRequiringRejectionRecord;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void addErrorsRequiringRejectionRecord(ConfirmationErrorReportBean errorsRequiringRejectionRecord)
/*     */   {
/*  48 */     this.errorsRequiringRejectionRecord.add(errorsRequiringRejectionRecord);
/*     */   }
/*     */   
/*     */ 
/*     */   public List<ConfirmationErrorReportBean> getErrorsNotRequiringRejectionRecord()
/*     */   {
/*  54 */     return this.errorsNotRequiringRejectionRecord;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void addErrorsNotRequiringRejectionRecord(ConfirmationErrorReportBean errorsNotRequiringRejectionRecord)
/*     */   {
/*  61 */     this.errorsNotRequiringRejectionRecord.add(errorsNotRequiringRejectionRecord);
/*     */   }
/*     */   
/*     */ 
/*     */   public List<ConfirmationErrorReportBean> getErrorsRequiringBatchSuspensionRecord()
/*     */   {
/*  67 */     return this.errorsRequiringBatchSuspensionRecord;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void addErrorsRequiringBatchSuspensionRecord(ConfirmationErrorReportBean errorsRequiringBatchSuspensionRecord)
/*     */   {
/*  74 */     this.errorsRequiringBatchSuspensionRecord.add(errorsRequiringBatchSuspensionRecord);
/*     */   }
/*     */   
/*     */ 
/*     */   public List<ConfirmationErrorReportBean> getPreSuspendedRejectedBatchRecord()
/*     */   {
/*  80 */     return this.preSuspendedRejectedBatchRecord;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void addPreSuspendedRejectedBatchRecord(ConfirmationErrorReportBean preSuspendedRejectBatchRec)
/*     */   {
/*  88 */     this.preSuspendedRejectedBatchRecord.add(preSuspendedRejectBatchRec);
/*     */   }
/*     */   
/*     */ 
/*     */   public List<ConfirmationErrorReportBean> getPreSuspendedProcessedRejectedBatchRecord()
/*     */   {
/*  94 */     return this.preSuspendedProcessedRejectedBatchRecord;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void addPreSuspendedProcessedRejectedBatchRecord(ConfirmationErrorReportBean preSuspendedProcessedRejectBatchRec)
/*     */   {
/* 102 */     this.preSuspendedProcessedRejectedBatchRecord.add(preSuspendedProcessedRejectBatchRec);
/*     */   }
/*     */   
/*     */ 
/*     */   public List<ConfirmationRejectedRecordBean> getRejectionRecords()
/*     */   {
/* 108 */     return this.rejectionRecords;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void addRejectionRecords(ConfirmationRejectedRecordBean rejectionRec)
/*     */   {
/* 116 */     this.rejectionRecords.add(rejectionRec);
/*     */   }
/*     */   
/*     */ 
/*     */   public List<ConfirmationReportSummaryBean> getErrorSummary()
/*     */   {
/* 122 */     return this.errorSummary;
/*     */   }
/*     */   
/*     */ 
/*     */   public void addErrorSummary(ConfirmationReportSummaryBean errorSummary)
/*     */   {
/* 128 */     this.errorSummary.add(errorSummary);
/*     */   }
/*     */   
/*     */ 
/*     */   public ConfirmationReportHeaderBean getConfirmationReportHeaderBean()
/*     */   {
/* 134 */     return this.confirmationReportHeaderBean;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void setConfirmationReportHeaderBean(ConfirmationReportHeaderBean confirmationReportHeaderBean)
/*     */   {
/* 141 */     this.confirmationReportHeaderBean = confirmationReportHeaderBean;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\conformation\ConfirmationReportObject.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */