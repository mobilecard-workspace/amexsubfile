/*     */ package com.americanexpress.ips.gfsg.beans.conformation;
/*     */ 
/*     */ import com.americanexpress.ips.gfsg.beans.common.TransactionAdviceDetailBean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.TransactionBatchTrailerBean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.taa.LocationDetailTAABean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.AirlineIndustryTAABean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.AutoRentalIndustryTAABean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.CommunicationServicesIndustryBean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.EntertainmentTicketingIndustryTAABean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.InsuranceIndustryTAABean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.LodgingIndustryTAABean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.RailIndustryTAABean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.RetailIndustryTAABean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.TravelCruiseIndustryTAABean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.taa.nonindustrytype.CPSLevel2Bean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.taa.nonindustrytype.DeferredPaymentPlanBean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.taa.nonindustrytype.EMVBean;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class RejectedRecordDataBean
/*     */ {
/*  28 */   private LocationDetailTAABean locationDetailTAABean = new LocationDetailTAABean();
/*  29 */   private TransactionAdviceDetailBean tadRecordBean = new TransactionAdviceDetailBean();
/*     */   
/*  31 */   private TransactionBatchTrailerBean tbtRecord = new TransactionBatchTrailerBean();
/*  32 */   private RejectedTABRecord tabRecord = new RejectedTABRecord();
/*  33 */   private AirlineIndustryTAABean airlineIndustry = new AirlineIndustryTAABean();
/*  34 */   private AutoRentalIndustryTAABean autoRentalIndustry = new AutoRentalIndustryTAABean();
/*  35 */   private CommunicationServicesIndustryBean communicationServicesIndustry = new CommunicationServicesIndustryBean();
/*  36 */   private EntertainmentTicketingIndustryTAABean entertainmentTicketingIndustry = new EntertainmentTicketingIndustryTAABean();
/*  37 */   private InsuranceIndustryTAABean insuranceIndustry = new InsuranceIndustryTAABean();
/*  38 */   private LodgingIndustryTAABean lodgingIndustry = new LodgingIndustryTAABean();
/*  39 */   private RailIndustryTAABean railIndustry = new RailIndustryTAABean();
/*  40 */   private DeferredPaymentPlanBean dppNonIndustry = new DeferredPaymentPlanBean();
/*  41 */   private RetailIndustryTAABean retailIndustry = new RetailIndustryTAABean();
/*  42 */   private TravelCruiseIndustryTAABean travelCruiseIndustry = new TravelCruiseIndustryTAABean();
/*  43 */   private CPSLevel2Bean cpsLevel2Bean = new CPSLevel2Bean();
/*  44 */   private EMVBean emvBean = new EMVBean();
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public DeferredPaymentPlanBean getDppNonIndustry()
/*     */   {
/*  52 */     return this.dppNonIndustry;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setDppNonIndustry(DeferredPaymentPlanBean dppNonIndustry)
/*     */   {
/*  58 */     this.dppNonIndustry = dppNonIndustry;
/*     */   }
/*     */   
/*     */ 
/*     */   public LocationDetailTAABean getLocationDetailTAABean()
/*     */   {
/*  64 */     return this.locationDetailTAABean;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setLocationDetailTAABean(LocationDetailTAABean locationDetailTAABean)
/*     */   {
/*  70 */     this.locationDetailTAABean = locationDetailTAABean;
/*     */   }
/*     */   
/*     */ 
/*     */   public TransactionAdviceDetailBean getTadRecordBean()
/*     */   {
/*  76 */     return this.tadRecordBean;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setTadRecordBean(TransactionAdviceDetailBean tadRecordBean)
/*     */   {
/*  82 */     this.tadRecordBean = tadRecordBean;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public TransactionBatchTrailerBean getTbtRecord()
/*     */   {
/* 101 */     return this.tbtRecord;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setTbtRecord(TransactionBatchTrailerBean tbtRecord)
/*     */   {
/* 107 */     this.tbtRecord = tbtRecord;
/*     */   }
/*     */   
/*     */ 
/*     */   public RejectedTABRecord getTabRecord()
/*     */   {
/* 113 */     return this.tabRecord;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setTabRecord(RejectedTABRecord tabRecord)
/*     */   {
/* 119 */     this.tabRecord = tabRecord;
/*     */   }
/*     */   
/*     */ 
/*     */   public AirlineIndustryTAABean getAirlineIndustry()
/*     */   {
/* 125 */     return this.airlineIndustry;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setAirlineIndustry(AirlineIndustryTAABean airlineIndustry)
/*     */   {
/* 131 */     this.airlineIndustry = airlineIndustry;
/*     */   }
/*     */   
/*     */ 
/*     */   public AutoRentalIndustryTAABean getAutoRentalIndustry()
/*     */   {
/* 137 */     return this.autoRentalIndustry;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setAutoRentalIndustry(AutoRentalIndustryTAABean autoRentalIndustry)
/*     */   {
/* 143 */     this.autoRentalIndustry = autoRentalIndustry;
/*     */   }
/*     */   
/*     */ 
/*     */   public CommunicationServicesIndustryBean getCommunicationServicesIndustry()
/*     */   {
/* 149 */     return this.communicationServicesIndustry;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void setCommunicationServicesIndustry(CommunicationServicesIndustryBean communicationServicesIndustry)
/*     */   {
/* 156 */     this.communicationServicesIndustry = communicationServicesIndustry;
/*     */   }
/*     */   
/*     */ 
/*     */   public EntertainmentTicketingIndustryTAABean getEntertainmentTicketingIndustry()
/*     */   {
/* 162 */     return this.entertainmentTicketingIndustry;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void setEntertainmentTicketingIndustry(EntertainmentTicketingIndustryTAABean entertainmentTicketingIndustry)
/*     */   {
/* 169 */     this.entertainmentTicketingIndustry = entertainmentTicketingIndustry;
/*     */   }
/*     */   
/*     */ 
/*     */   public InsuranceIndustryTAABean getInsuranceIndustry()
/*     */   {
/* 175 */     return this.insuranceIndustry;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setInsuranceIndustry(InsuranceIndustryTAABean insuranceIndustry)
/*     */   {
/* 181 */     this.insuranceIndustry = insuranceIndustry;
/*     */   }
/*     */   
/*     */ 
/*     */   public LodgingIndustryTAABean getLodgingIndustry()
/*     */   {
/* 187 */     return this.lodgingIndustry;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setLodgingIndustry(LodgingIndustryTAABean lodgingIndustry)
/*     */   {
/* 193 */     this.lodgingIndustry = lodgingIndustry;
/*     */   }
/*     */   
/*     */ 
/*     */   public RailIndustryTAABean getRailIndustry()
/*     */   {
/* 199 */     return this.railIndustry;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setRailIndustry(RailIndustryTAABean railIndustry)
/*     */   {
/* 205 */     this.railIndustry = railIndustry;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public RetailIndustryTAABean getRetailIndustry()
/*     */   {
/* 212 */     return this.retailIndustry;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setRetailIndustry(RetailIndustryTAABean retailIndustry)
/*     */   {
/* 218 */     this.retailIndustry = retailIndustry;
/*     */   }
/*     */   
/*     */ 
/*     */   public TravelCruiseIndustryTAABean getTravelCruiseIndustry()
/*     */   {
/* 224 */     return this.travelCruiseIndustry;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void setTravelCruiseIndustry(TravelCruiseIndustryTAABean travelCruiseIndustry)
/*     */   {
/* 231 */     this.travelCruiseIndustry = travelCruiseIndustry;
/*     */   }
/*     */   
/*     */ 
/*     */   public CPSLevel2Bean getCpsLevel2Bean()
/*     */   {
/* 237 */     return this.cpsLevel2Bean;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setCpsLevel2Bean(CPSLevel2Bean cpsLevel2Bean)
/*     */   {
/* 243 */     this.cpsLevel2Bean = cpsLevel2Bean;
/*     */   }
/*     */   
/*     */ 
/*     */   public EMVBean getEmvBean()
/*     */   {
/* 249 */     return this.emvBean;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setEmvBean(EMVBean emvBean)
/*     */   {
/* 255 */     this.emvBean = emvBean;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\conformation\RejectedRecordDataBean.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */