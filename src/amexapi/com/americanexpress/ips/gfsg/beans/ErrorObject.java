/*    */ package com.americanexpress.ips.gfsg.beans;
/*    */ 
/*    */ import com.americanexpress.ips.gfsg.enumerations.SubmissionErrorCodes;
/*    */ import com.americanexpress.ips.gfsg.enumerations.XMLSubmissionErrorCodes;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class ErrorObject
/*    */ {
/*    */   private String errorCode;
/*    */   private String errorDescription;
/*    */   
/*    */   public ErrorObject(String errorCode, String errorDescription)
/*    */   {
/* 24 */     this.errorCode = errorCode;
/* 25 */     this.errorDescription = errorDescription;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public ErrorObject(SubmissionErrorCodes code)
/*    */   {
/* 33 */     this(code.getErrorCode(), code.getErrorDescription());
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public ErrorObject(XMLSubmissionErrorCodes code)
/*    */   {
/* 40 */     this(code.getErrorCode(), code.getErrorDescription());
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public String getErrorCode()
/*    */   {
/* 49 */     return this.errorCode;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public String getErrorDescription()
/*    */   {
/* 69 */     return this.errorDescription;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\ErrorObject.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */