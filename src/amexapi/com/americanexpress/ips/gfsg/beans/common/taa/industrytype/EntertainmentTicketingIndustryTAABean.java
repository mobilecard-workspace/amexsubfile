/*     */ package com.americanexpress.ips.gfsg.beans.common.taa.industrytype;
/*     */ 
/*     */ import com.americanexpress.ips.gfsg.beans.common.taa.IndustryTypeTAABean;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class EntertainmentTicketingIndustryTAABean
/*     */   extends IndustryTypeTAABean
/*     */ {
/*     */   private String eventName;
/*     */   private String eventDate;
/*     */   private String eventIndividualTicketPriceAmount;
/*     */   private String eventTicketQuantity;
/*     */   private String eventLocation;
/*     */   private String eventRegionCode;
/*     */   private String eventCountryCode;
/*     */   
/*     */   public String getEventName()
/*     */   {
/*  32 */     return this.eventName;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setEventName(String eventName)
/*     */   {
/*  46 */     this.eventName = eventName;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getEventDate()
/*     */   {
/*  69 */     return this.eventDate;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setEventDate(String eventDate)
/*     */   {
/*  93 */     this.eventDate = eventDate;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getEventIndividualTicketPriceAmount()
/*     */   {
/* 106 */     return this.eventIndividualTicketPriceAmount;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setEventIndividualTicketPriceAmount(String eventIndividualTicketPriceAmount)
/*     */   {
/* 121 */     this.eventIndividualTicketPriceAmount = eventIndividualTicketPriceAmount;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getEventTicketQuantity()
/*     */   {
/* 136 */     return this.eventTicketQuantity;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setEventTicketQuantity(String eventTicketQuantity)
/*     */   {
/* 152 */     this.eventTicketQuantity = eventTicketQuantity;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getEventLocation()
/*     */   {
/* 166 */     return this.eventLocation;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setEventLocation(String eventLocation)
/*     */   {
/* 181 */     this.eventLocation = eventLocation;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getEventRegionCode()
/*     */   {
/* 195 */     return this.eventRegionCode;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setEventRegionCode(String eventRegionCode)
/*     */   {
/* 210 */     this.eventRegionCode = eventRegionCode;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getEventCountryCode()
/*     */   {
/* 224 */     return this.eventCountryCode;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setEventCountryCode(String eventCountryCode)
/*     */   {
/* 239 */     this.eventCountryCode = eventCountryCode;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\common\taa\industrytype\EntertainmentTicketingIndustryTAABean.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */