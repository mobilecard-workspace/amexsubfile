/*      */ package com.americanexpress.ips.gfsg.beans.common;
/*      */ 
/*      */ import com.americanexpress.ips.gfsg.beans.common.taa.LocationDetailTAABean;
/*      */ import com.americanexpress.ips.gfsg.enumerations.RecordType;
/*      */ import java.util.ArrayList;
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ public class TransactionAdviceBasicBean
/*      */ {
/*      */   private LocationDetailTAABean locationDetailTAABean;
/*      */   private TransactionAdviceDetailBean transactionAdviceDetailBean;
/*      */   private TransactionAdviceAddendumBean transactionAdviceAddendumBean;
/*   21 */   private String recordType = RecordType.Transaction_Advice_Basic.getRecordType();
/*      */   
/*      */   private String recordNumber;
/*      */   
/*      */   private String transactionIdentifier;
/*      */   
/*      */   private String formatCode;
/*      */   
/*      */   private String mediaCode;
/*      */   
/*      */   private String submissionMethod;
/*      */   private String approvalCode;
/*      */   private String primaryAccountNumber;
/*      */   private String cardExpiryDate;
/*      */   private String transactionDate;
/*      */   private String transactionTime;
/*      */   private String transactionAmount;
/*      */   private String processingCode;
/*      */   private String transactionCurrencyCode;
/*      */   private String extendedPaymentData;
/*      */   private String merchantId;
/*      */   private String merchantLocationId;
/*      */   private String merchantContactInfo;
/*      */   private String terminalId;
/*      */   private String posDataCode;
/*      */   private String invoiceReferenceNumber;
/*      */   private String tabImageSequenceNumber;
/*      */   private String matchingKeyType;
/*      */   private String matchingKey;
/*      */   private String electronicCommerceIndicator;
/*      */   private String tabField7reserved;
/*      */   private String tabField13reserved;
/*      */   private String tabField23reserved;
/*      */   private String tabField24reserved;
/*      */   private String tabField25reserved;
/*      */   private String tabField27reserved;
/*      */   private String tabField32reserved;
/*      */   private ArrayList<TransactionAdviceAddendumBean> arrayTAABean;
/*      */   
/*      */   public ArrayList<TransactionAdviceAddendumBean> getArrayTAABean()
/*      */   {
/*   62 */     return this.arrayTAABean;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setArrayTAABean(ArrayList<TransactionAdviceAddendumBean> arrayTAABean)
/*      */   {
/*   72 */     this.arrayTAABean = arrayTAABean;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public LocationDetailTAABean getLocationDetailTAABean()
/*      */   {
/*   81 */     return this.locationDetailTAABean;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setLocationDetailTAABean(LocationDetailTAABean locationDetailTAABean)
/*      */   {
/*   92 */     this.locationDetailTAABean = locationDetailTAABean;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public TransactionAdviceDetailBean getTransactionAdviceDetailBean()
/*      */   {
/*  101 */     return this.transactionAdviceDetailBean;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTransactionAdviceDetailBean(TransactionAdviceDetailBean transactionAdviceDetailBean)
/*      */   {
/*  112 */     this.transactionAdviceDetailBean = transactionAdviceDetailBean;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public TransactionAdviceAddendumBean getTransactionAdviceAddendumBean()
/*      */   {
/*  121 */     return this.transactionAdviceAddendumBean;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTransactionAdviceAddendumBean(TransactionAdviceAddendumBean transactionAdviceAddendumBean)
/*      */   {
/*  132 */     this.transactionAdviceAddendumBean = transactionAdviceAddendumBean;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getRecordType()
/*      */   {
/*  147 */     return this.recordType;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setRecordType(String recordType)
/*      */   {
/*  163 */     this.recordType = recordType;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getRecordNumber()
/*      */   {
/*  177 */     return this.recordNumber;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setRecordNumber(String recordNumber)
/*      */   {
/*  192 */     this.recordNumber = recordNumber;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTransactionIdentifier()
/*      */   {
/*  208 */     return this.transactionIdentifier;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTransactionIdentifier(String transactionIdentifier)
/*      */   {
/*  225 */     this.transactionIdentifier = transactionIdentifier;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getFormatCode()
/*      */   {
/*  254 */     return this.formatCode;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setFormatCode(String formatCode)
/*      */   {
/*  284 */     this.formatCode = formatCode;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getMediaCode()
/*      */   {
/*  305 */     return this.mediaCode;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setMediaCode(String mediaCode)
/*      */   {
/*  327 */     this.mediaCode = mediaCode;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getSubmissionMethod()
/*      */   {
/*  352 */     return this.submissionMethod;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSubmissionMethod(String submissionMethod)
/*      */   {
/*  377 */     this.submissionMethod = submissionMethod;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getApprovalCode()
/*      */   {
/*  392 */     return this.approvalCode;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setApprovalCode(String approvalCode)
/*      */   {
/*  407 */     this.approvalCode = approvalCode;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getPrimaryAccountNumber()
/*      */   {
/*  420 */     return this.primaryAccountNumber;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setPrimaryAccountNumber(String primaryAccountNumber)
/*      */   {
/*  434 */     this.primaryAccountNumber = primaryAccountNumber;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getCardExpiryDate()
/*      */   {
/*  453 */     return this.cardExpiryDate;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setCardExpiryDate(String cardExpiryDate)
/*      */   {
/*  472 */     this.cardExpiryDate = cardExpiryDate;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTransactionDate()
/*      */   {
/*  493 */     return this.transactionDate;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTransactionDate(String transactionDate)
/*      */   {
/*  515 */     this.transactionDate = transactionDate;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTransactionTime()
/*      */   {
/*  535 */     return this.transactionTime;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTransactionTime(String transactionTime)
/*      */   {
/*  556 */     this.transactionTime = transactionTime;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTransactionAmount()
/*      */   {
/*  571 */     return this.transactionAmount;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTransactionAmount(String transactionAmount)
/*      */   {
/*  587 */     this.transactionAmount = transactionAmount;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getProcessingCode()
/*      */   {
/*  611 */     return this.processingCode;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setProcessingCode(String processingCode)
/*      */   {
/*  636 */     this.processingCode = processingCode;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTransactionCurrencyCode()
/*      */   {
/*  651 */     return this.transactionCurrencyCode;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTransactionCurrencyCode(String transactionCurrencyCode)
/*      */   {
/*  667 */     this.transactionCurrencyCode = transactionCurrencyCode;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getExtendedPaymentData()
/*      */   {
/*  682 */     return this.extendedPaymentData;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setExtendedPaymentData(String extendedPaymentData)
/*      */   {
/*  698 */     this.extendedPaymentData = extendedPaymentData;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getMerchantId()
/*      */   {
/*  713 */     return this.merchantId;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setMerchantId(String merchantId)
/*      */   {
/*  729 */     this.merchantId = merchantId;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getMerchantLocationId()
/*      */   {
/*  743 */     return this.merchantLocationId;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setMerchantLocationId(String merchantLocationId)
/*      */   {
/*  758 */     this.merchantLocationId = merchantLocationId;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getMerchantContactInfo()
/*      */   {
/*  771 */     return this.merchantContactInfo;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setMerchantContactInfo(String merchantContactInfo)
/*      */   {
/*  785 */     this.merchantContactInfo = merchantContactInfo;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTerminalId()
/*      */   {
/*  799 */     return this.terminalId;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTerminalId(String terminalId)
/*      */   {
/*  814 */     this.terminalId = terminalId;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getPosDataCode()
/*      */   {
/*  828 */     return this.posDataCode;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setPosDataCode(String posDataCode)
/*      */   {
/*  843 */     this.posDataCode = posDataCode;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getInvoiceReferenceNumber()
/*      */   {
/*  857 */     return this.invoiceReferenceNumber;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setInvoiceReferenceNumber(String invoiceReferenceNumber)
/*      */   {
/*  872 */     this.invoiceReferenceNumber = invoiceReferenceNumber;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTabImageSequenceNumber()
/*      */   {
/*  886 */     return this.tabImageSequenceNumber;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTabImageSequenceNumber(String tabImageSequenceNumber)
/*      */   {
/*  901 */     this.tabImageSequenceNumber = tabImageSequenceNumber;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getMatchingKeyType()
/*      */   {
/*  921 */     return this.matchingKeyType;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setMatchingKeyType(String matchingKeyType)
/*      */   {
/*  942 */     this.matchingKeyType = matchingKeyType;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getMatchingKey()
/*      */   {
/*  958 */     return this.matchingKey;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setMatchingKey(String matchingKey)
/*      */   {
/*  975 */     this.matchingKey = matchingKey;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getElectronicCommerceIndicator()
/*      */   {
/*  995 */     return this.electronicCommerceIndicator;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setElectronicCommerceIndicator(String electronicCommerceIndicator)
/*      */   {
/* 1017 */     this.electronicCommerceIndicator = electronicCommerceIndicator;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTabField7reserved()
/*      */   {
/* 1030 */     return this.tabField7reserved;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTabField7reserved(String tabField7reserved)
/*      */   {
/* 1044 */     this.tabField7reserved = tabField7reserved;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTabField13reserved()
/*      */   {
/* 1057 */     return this.tabField13reserved;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTabField13reserved(String tabField13reserved)
/*      */   {
/* 1071 */     this.tabField13reserved = tabField13reserved;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTabField23reserved()
/*      */   {
/* 1084 */     return this.tabField23reserved;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTabField23reserved(String tabField23reserved)
/*      */   {
/* 1098 */     this.tabField23reserved = tabField23reserved;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTabField24reserved()
/*      */   {
/* 1111 */     return this.tabField24reserved;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTabField24reserved(String tabField24reserved)
/*      */   {
/* 1125 */     this.tabField24reserved = tabField24reserved;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTabField25reserved()
/*      */   {
/* 1138 */     return this.tabField25reserved;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTabField25reserved(String tabField25reserved)
/*      */   {
/* 1152 */     this.tabField25reserved = tabField25reserved;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTabField27reserved()
/*      */   {
/* 1165 */     return this.tabField27reserved;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTabField27reserved(String tabField27reserved)
/*      */   {
/* 1179 */     this.tabField27reserved = tabField27reserved;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTabField32reserved()
/*      */   {
/* 1196 */     return this.tabField32reserved;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTabField32reserved(String tabField32reserved)
/*      */   {
/* 1214 */     this.tabField32reserved = tabField32reserved;
/*      */   }
/*      */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\common\TransactionAdviceBasicBean.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */