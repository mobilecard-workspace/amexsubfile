/*     */ package com.americanexpress.ips.gfsg.beans.common;
/*     */ 
/*     */ import com.americanexpress.ips.gfsg.enumerations.RecordType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class TransactionBatchTrailerBean
/*     */ {
/*  14 */   private String recordType = RecordType.Transaction_Batch_Trailer.getRecordType();
/*     */   
/*     */   private String recordNumber;
/*     */   
/*     */   private String merchantId;
/*     */   
/*     */   private String tbtIdentificationNumber;
/*     */   
/*     */   private String tbtCreationDate;
/*     */   
/*     */   private String totalNoOfTabs;
/*     */   
/*     */   private String tbtAmount;
/*     */   
/*     */   private String tbtAmountSign;
/*     */   
/*     */   private String tbtCurrencyCode;
/*     */   
/*     */   private String tbtImageSequenceNumber;
/*     */   
/*     */   private String tbtField4Reserved;
/*     */   private String tbtField8Reserved;
/*     */   private String tbtField12Reserved;
/*     */   private String tbtField13Reserved;
/*     */   private String tbtField14Reserved;
/*     */   private String tbtField16Reserved;
/*     */   
/*     */   public String getRecordType()
/*     */   {
/*  43 */     return this.recordType;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setRecordType(String recordType)
/*     */   {
/*  59 */     this.recordType = recordType;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getRecordNumber()
/*     */   {
/*  73 */     return this.recordNumber;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setRecordNumber(String recordNumber)
/*     */   {
/*  88 */     this.recordNumber = recordNumber;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getMerchantId()
/*     */   {
/* 102 */     return this.merchantId;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setMerchantId(String merchantId)
/*     */   {
/* 117 */     this.merchantId = merchantId;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTbtIdentificationNumber()
/*     */   {
/* 131 */     return this.tbtIdentificationNumber;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTbtIdentificationNumber(String tbtIdentificationNumber)
/*     */   {
/* 146 */     this.tbtIdentificationNumber = tbtIdentificationNumber;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTbtCreationDate()
/*     */   {
/* 168 */     return this.tbtCreationDate;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTbtCreationDate(String tbtCreationDate)
/*     */   {
/* 191 */     this.tbtCreationDate = tbtCreationDate;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTotalNoOfTabs()
/*     */   {
/* 206 */     return this.totalNoOfTabs;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTotalNoOfTabs(String totalNoOfTabs)
/*     */   {
/* 222 */     this.totalNoOfTabs = totalNoOfTabs;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTbtAmount()
/*     */   {
/* 237 */     return this.tbtAmount;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTbtAmount(String tbtAmount)
/*     */   {
/* 253 */     this.tbtAmount = tbtAmount;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTbtAmountSign()
/*     */   {
/* 268 */     return this.tbtAmountSign;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTbtAmountSign(String tbtAmountSign)
/*     */   {
/* 284 */     this.tbtAmountSign = tbtAmountSign;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTbtCurrencyCode()
/*     */   {
/* 299 */     return this.tbtCurrencyCode;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTbtCurrencyCode(String tbtCurrencyCode)
/*     */   {
/* 315 */     this.tbtCurrencyCode = tbtCurrencyCode;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTbtImageSequenceNumber()
/*     */   {
/* 329 */     return this.tbtImageSequenceNumber;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTbtImageSequenceNumber(String tbtImageSequenceNumber)
/*     */   {
/* 344 */     this.tbtImageSequenceNumber = tbtImageSequenceNumber;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTbtField4Reserved()
/*     */   {
/* 357 */     return this.tbtField4Reserved;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTbtField4Reserved(String tbtField4Reserved)
/*     */   {
/* 371 */     this.tbtField4Reserved = tbtField4Reserved;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTbtField8Reserved()
/*     */   {
/* 384 */     return this.tbtField8Reserved;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTbtField8Reserved(String tbtField8Reserved)
/*     */   {
/* 398 */     this.tbtField8Reserved = tbtField8Reserved;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTbtField12Reserved()
/*     */   {
/* 411 */     return this.tbtField12Reserved;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTbtField12Reserved(String tbtField12Reserved)
/*     */   {
/* 425 */     this.tbtField12Reserved = tbtField12Reserved;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTbtField13Reserved()
/*     */   {
/* 438 */     return this.tbtField13Reserved;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTbtField13Reserved(String tbtField13Reserved)
/*     */   {
/* 452 */     this.tbtField13Reserved = tbtField13Reserved;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTbtField14Reserved()
/*     */   {
/* 465 */     return this.tbtField14Reserved;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTbtField14Reserved(String tbtField14Reserved)
/*     */   {
/* 479 */     this.tbtField14Reserved = tbtField14Reserved;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTbtField16Reserved()
/*     */   {
/* 492 */     return this.tbtField16Reserved;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTbtField16Reserved(String tbtField16Reserved)
/*     */   {
/* 506 */     this.tbtField16Reserved = tbtField16Reserved;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\common\TransactionBatchTrailerBean.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */