/*     */ package com.americanexpress.ips.gfsg.beans.common.taa.nonindustrytype;
/*     */ 
/*     */ import com.americanexpress.ips.gfsg.beans.common.taa.NonIndustrySpecificTAABean;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class CPSLevel2Bean
/*     */   extends NonIndustrySpecificTAABean
/*     */ {
/*     */   private String requesterName;
/*     */   private String chargeDescription1;
/*     */   private String chargeItemQuantity1;
/*     */   private String chargeItemAmount1;
/*     */   private String chargeDescription2;
/*     */   private String chargeItemQuantity2;
/*     */   private String chargeItemAmount2;
/*     */   private String chargeDescription3;
/*     */   private String chargeItemQuantity3;
/*     */   private String chargeItemAmount3;
/*     */   private String chargeDescription4;
/*     */   private String chargeItemQuantity4;
/*     */   private String chargeItemAmount4;
/*     */   private String cardMemberReferenceNumber;
/*     */   private String shipToPostalCode;
/*     */   private String totalTaxAmount;
/*     */   private String taxTypeCode;
/*     */   
/*     */   public String getRequesterName()
/*     */   {
/*  43 */     return this.requesterName;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setRequesterName(String requesterName)
/*     */   {
/*  58 */     this.requesterName = requesterName;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getChargeDescription1()
/*     */   {
/*  71 */     return this.chargeDescription1;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setChargeDescription1(String chargeDescription1)
/*     */   {
/*  85 */     this.chargeDescription1 = chargeDescription1;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getChargeItemQuantity1()
/*     */   {
/*  99 */     return this.chargeItemQuantity1;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setChargeItemQuantity1(String chargeItemQuantity1)
/*     */   {
/* 114 */     this.chargeItemQuantity1 = chargeItemQuantity1;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getChargeItemAmount1()
/*     */   {
/* 128 */     return this.chargeItemAmount1;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setChargeItemAmount1(String chargeItemAmount1)
/*     */   {
/* 143 */     this.chargeItemAmount1 = chargeItemAmount1;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getChargeDescription2()
/*     */   {
/* 156 */     return this.chargeDescription2;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setChargeDescription2(String chargeDescription2)
/*     */   {
/* 170 */     this.chargeDescription2 = chargeDescription2;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getChargeItemQuantity2()
/*     */   {
/* 184 */     return this.chargeItemQuantity2;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setChargeItemQuantity2(String chargeItemQuantity2)
/*     */   {
/* 199 */     this.chargeItemQuantity2 = chargeItemQuantity2;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getChargeItemAmount2()
/*     */   {
/* 213 */     return this.chargeItemAmount2;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setChargeItemAmount2(String chargeItemAmount2)
/*     */   {
/* 228 */     this.chargeItemAmount2 = chargeItemAmount2;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getChargeDescription3()
/*     */   {
/* 241 */     return this.chargeDescription3;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setChargeDescription3(String chargeDescription3)
/*     */   {
/* 255 */     this.chargeDescription3 = chargeDescription3;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getChargeItemQuantity3()
/*     */   {
/* 269 */     return this.chargeItemQuantity3;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setChargeItemQuantity3(String chargeItemQuantity3)
/*     */   {
/* 284 */     this.chargeItemQuantity3 = chargeItemQuantity3;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getChargeItemAmount3()
/*     */   {
/* 298 */     return this.chargeItemAmount3;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setChargeItemAmount3(String chargeItemAmount3)
/*     */   {
/* 313 */     this.chargeItemAmount3 = chargeItemAmount3;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getChargeDescription4()
/*     */   {
/* 326 */     return this.chargeDescription4;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setChargeDescription4(String chargeDescription4)
/*     */   {
/* 340 */     this.chargeDescription4 = chargeDescription4;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getChargeItemQuantity4()
/*     */   {
/* 354 */     return this.chargeItemQuantity4;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setChargeItemQuantity4(String chargeItemQuantity4)
/*     */   {
/* 369 */     this.chargeItemQuantity4 = chargeItemQuantity4;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getChargeItemAmount4()
/*     */   {
/* 383 */     return this.chargeItemAmount4;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setChargeItemAmount4(String chargeItemAmount4)
/*     */   {
/* 398 */     this.chargeItemAmount4 = chargeItemAmount4;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCardMemberReferenceNumber()
/*     */   {
/* 413 */     return this.cardMemberReferenceNumber;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCardMemberReferenceNumber(String cardMemberReferenceNumber)
/*     */   {
/* 429 */     this.cardMemberReferenceNumber = cardMemberReferenceNumber;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getShipToPostalCode()
/*     */   {
/* 443 */     return this.shipToPostalCode;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setShipToPostalCode(String shipToPostalCode)
/*     */   {
/* 458 */     this.shipToPostalCode = shipToPostalCode;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTotalTaxAmount()
/*     */   {
/* 472 */     return this.totalTaxAmount;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTotalTaxAmount(String totalTaxAmount)
/*     */   {
/* 487 */     this.totalTaxAmount = totalTaxAmount;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTaxTypeCode()
/*     */   {
/* 501 */     return this.taxTypeCode;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTaxTypeCode(String taxTypeCode)
/*     */   {
/* 516 */     this.taxTypeCode = taxTypeCode;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\common\taa\nonindustrytype\CPSLevel2Bean.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */