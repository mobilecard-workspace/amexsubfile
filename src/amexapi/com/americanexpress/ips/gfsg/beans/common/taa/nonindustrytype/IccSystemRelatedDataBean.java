/*     */ package com.americanexpress.ips.gfsg.beans.common.taa.nonindustrytype;
/*     */ 
/*     */ 
/*     */ public class IccSystemRelatedDataBean
/*     */ {
/*     */   private String headerVersionName;
/*     */   
/*     */   private String headerVersionNumber;
/*     */   
/*     */   private String applicationCryptogram;
/*     */   
/*     */   private String issuerApplicationData;
/*     */   
/*     */   private String unPredictableNumber;
/*     */   
/*     */   private String applicationTransactionCounter;
/*     */   
/*     */   private String terminalVerificationResults;
/*     */   
/*     */   private String transactionDate;
/*     */   
/*     */   private String transactionType;
/*     */   private String amountAuthorized;
/*     */   private String terminalTransactionCurrencyCode;
/*     */   private String terminalCountryCode;
/*     */   private String applicationInterCahngeProfile;
/*     */   private String amountOther;
/*     */   private String applicationPanSequenceNumber;
/*     */   private String cryptogramInformationData;
/*     */   
/*     */   public String getHeaderVersionName()
/*     */   {
/*  33 */     return this.headerVersionName;
/*     */   }
/*     */   
/*  36 */   public void setHeaderVersionName(String headerVersionName) { this.headerVersionName = headerVersionName; }
/*     */   
/*     */   public String getHeaderVersionNumber() {
/*  39 */     return this.headerVersionNumber;
/*     */   }
/*     */   
/*  42 */   public void setHeaderVersionNumber(String headerVersionNumber) { this.headerVersionNumber = headerVersionNumber; }
/*     */   
/*     */   public String getApplicationCryptogram() {
/*  45 */     return this.applicationCryptogram;
/*     */   }
/*     */   
/*  48 */   public void setApplicationCryptogram(String applicationCryptogram) { this.applicationCryptogram = applicationCryptogram; }
/*     */   
/*     */   public String getIssuerApplicationData() {
/*  51 */     return this.issuerApplicationData;
/*     */   }
/*     */   
/*  54 */   public void setIssuerApplicationData(String issuerApplicationData) { this.issuerApplicationData = issuerApplicationData; }
/*     */   
/*     */   public String getUnPredictableNumber() {
/*  57 */     return this.unPredictableNumber;
/*     */   }
/*     */   
/*  60 */   public void setUnPredictableNumber(String unPredictableNumber) { this.unPredictableNumber = unPredictableNumber; }
/*     */   
/*     */   public String getApplicationTransactionCounter() {
/*  63 */     return this.applicationTransactionCounter;
/*     */   }
/*     */   
/*     */   public void setApplicationTransactionCounter(String applicationTransactionCounter) {
/*  67 */     this.applicationTransactionCounter = applicationTransactionCounter;
/*     */   }
/*     */   
/*  70 */   public String getTerminalVerificationResults() { return this.terminalVerificationResults; }
/*     */   
/*     */   public void setTerminalVerificationResults(String terminalVerificationResults) {
/*  73 */     this.terminalVerificationResults = terminalVerificationResults;
/*     */   }
/*     */   
/*  76 */   public String getTransactionDate() { return this.transactionDate; }
/*     */   
/*     */   public void setTransactionDate(String transactionDate) {
/*  79 */     this.transactionDate = transactionDate;
/*     */   }
/*     */   
/*     */   public String getTransactionType() {
/*  83 */     return this.transactionType;
/*     */   }
/*     */   
/*  86 */   public void setTransactionType(String transactionType) { this.transactionType = transactionType; }
/*     */   
/*     */   public String getAmountAuthorized() {
/*  89 */     return this.amountAuthorized;
/*     */   }
/*     */   
/*  92 */   public void setAmountAuthorized(String amountAuthorized) { this.amountAuthorized = amountAuthorized; }
/*     */   
/*     */   public String getTerminalTransactionCurrencyCode() {
/*  95 */     return this.terminalTransactionCurrencyCode;
/*     */   }
/*     */   
/*     */   public void setTerminalTransactionCurrencyCode(String terminalTransactionCurrencyCode) {
/*  99 */     this.terminalTransactionCurrencyCode = terminalTransactionCurrencyCode;
/*     */   }
/*     */   
/* 102 */   public String getTerminalCountryCode() { return this.terminalCountryCode; }
/*     */   
/*     */   public void setTerminalCountryCode(String terminalCountryCode) {
/* 105 */     this.terminalCountryCode = terminalCountryCode;
/*     */   }
/*     */   
/* 108 */   public String getApplicationInterCahngeProfile() { return this.applicationInterCahngeProfile; }
/*     */   
/*     */   public void setApplicationInterCahngeProfile(String applicationInterCahngeProfile)
/*     */   {
/* 112 */     this.applicationInterCahngeProfile = applicationInterCahngeProfile;
/*     */   }
/*     */   
/* 115 */   public String getAmountOther() { return this.amountOther; }
/*     */   
/*     */   public void setAmountOther(String amountOther) {
/* 118 */     this.amountOther = amountOther;
/*     */   }
/*     */   
/* 121 */   public String getApplicationPanSequenceNumber() { return this.applicationPanSequenceNumber; }
/*     */   
/*     */   public void setApplicationPanSequenceNumber(String applicationPanSequenceNumber) {
/* 124 */     this.applicationPanSequenceNumber = applicationPanSequenceNumber;
/*     */   }
/*     */   
/* 127 */   public String getCryptogramInformationData() { return this.cryptogramInformationData; }
/*     */   
/*     */   public void setCryptogramInformationData(String cryptogramInformationData) {
/* 130 */     this.cryptogramInformationData = cryptogramInformationData;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\common\taa\nonindustrytype\IccSystemRelatedDataBean.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */