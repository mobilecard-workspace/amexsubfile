/*     */ package com.americanexpress.ips.gfsg.beans.common.taa.nonindustrytype;
/*     */ 
/*     */ import com.americanexpress.ips.gfsg.beans.common.taa.NonIndustrySpecificTAABean;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class EMVBean
/*     */   extends NonIndustrySpecificTAABean
/*     */ {
/*     */   private String eMVFormatType;
/*     */   private String iCCSystemRelatedData;
/*     */   private String emvField7Reserved;
/*     */   private IccSystemRelatedDataBean iccSystemRelatedDataBean;
/*     */   
/*     */   public String getEMVFormatType()
/*     */   {
/*  34 */     return this.eMVFormatType;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setEMVFormatType(String formatType)
/*     */   {
/*  53 */     this.eMVFormatType = formatType;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getICCSystemRelatedData()
/*     */   {
/*  66 */     return this.iCCSystemRelatedData;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setICCSystemRelatedData(String systemRelatedData)
/*     */   {
/*  80 */     this.iCCSystemRelatedData = systemRelatedData;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getEmvField7Reserved()
/*     */   {
/*  98 */     return this.emvField7Reserved;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setEmvField7Reserved(String emvField7Reserved)
/*     */   {
/* 117 */     this.emvField7Reserved = emvField7Reserved;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public IccSystemRelatedDataBean getIccSystemRelatedDataBean()
/*     */   {
/* 127 */     return this.iccSystemRelatedDataBean;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setIccSystemRelatedDataBean(IccSystemRelatedDataBean iccSystemRelatedDataBean)
/*     */   {
/* 139 */     this.iccSystemRelatedDataBean = iccSystemRelatedDataBean;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\common\taa\nonindustrytype\EMVBean.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */