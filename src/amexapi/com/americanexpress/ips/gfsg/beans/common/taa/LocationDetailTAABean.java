/*     */ package com.americanexpress.ips.gfsg.beans.common.taa;
/*     */ 
/*     */ import com.americanexpress.ips.gfsg.enumerations.RecordType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class LocationDetailTAABean
/*     */ {
/*  14 */   private String recordType = RecordType.Transaction_Advice_Addendum.getRecordType();
/*     */   
/*     */   private String recordNumber;
/*     */   
/*     */   private String transactionIdentifier;
/*     */   
/*     */   private String ltaaDataField4Reserved;
/*     */   
/*     */   private String addendaTypeCode;
/*     */   
/*     */   private String locationName;
/*     */   
/*     */   private String locationAddress;
/*     */   
/*     */   private String locationCity;
/*     */   
/*     */   private String locationRegion;
/*     */   
/*     */   private String locationCountryCode;
/*     */   
/*     */   private String locationPostalCode;
/*     */   private String merchantCategoryCode;
/*     */   private String sellerId;
/*     */   private String ltaaDataField14Reserved;
/*     */   
/*     */   public String getRecordType()
/*     */   {
/*  41 */     return this.recordType;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setRecordType(String recordType)
/*     */   {
/*  57 */     this.recordType = recordType;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getRecordNumber()
/*     */   {
/*  71 */     return this.recordNumber;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setRecordNumber(String recordNumber)
/*     */   {
/*  86 */     this.recordNumber = recordNumber;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTransactionIdentifier()
/*     */   {
/* 102 */     return this.transactionIdentifier;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTransactionIdentifier(String transactionIdentifier)
/*     */   {
/* 119 */     this.transactionIdentifier = transactionIdentifier;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getLtaaDataField4Reserved()
/*     */   {
/* 132 */     return this.ltaaDataField4Reserved;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLtaaDataField4Reserved(String ltaaDataField4Reserved)
/*     */   {
/* 146 */     this.ltaaDataField4Reserved = ltaaDataField4Reserved;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getAddendaTypeCode()
/*     */   {
/* 160 */     return this.addendaTypeCode;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setAddendaTypeCode(String addendaTypeCode)
/*     */   {
/* 175 */     this.addendaTypeCode = addendaTypeCode;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getLocationName()
/*     */   {
/* 190 */     return this.locationName;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLocationName(String locationName)
/*     */   {
/* 206 */     this.locationName = locationName;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getLocationAddress()
/*     */   {
/* 220 */     return this.locationAddress;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLocationAddress(String locationAddress)
/*     */   {
/* 235 */     this.locationAddress = locationAddress;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getLocationCity()
/*     */   {
/* 249 */     return this.locationCity;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLocationCity(String locationCity)
/*     */   {
/* 264 */     this.locationCity = locationCity;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getLocationRegion()
/*     */   {
/* 279 */     return this.locationRegion;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLocationRegion(String locationRegion)
/*     */   {
/* 295 */     this.locationRegion = locationRegion;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getLocationCountryCode()
/*     */   {
/* 309 */     return this.locationCountryCode;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLocationCountryCode(String locationCountryCode)
/*     */   {
/* 324 */     this.locationCountryCode = locationCountryCode;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getLocationPostalCode()
/*     */   {
/* 338 */     return this.locationPostalCode;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLocationPostalCode(String locationPostalCode)
/*     */   {
/* 353 */     this.locationPostalCode = locationPostalCode;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getMerchantCategoryCode()
/*     */   {
/* 368 */     return this.merchantCategoryCode;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setMerchantCategoryCode(String merchantCategoryCode)
/*     */   {
/* 384 */     this.merchantCategoryCode = merchantCategoryCode;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getSellerId()
/*     */   {
/* 399 */     return this.sellerId;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setSellerId(String sellerId)
/*     */   {
/* 415 */     this.sellerId = sellerId;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getLtaaDataField14Reserved()
/*     */   {
/* 433 */     return this.ltaaDataField14Reserved;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLtaaDataField14Reserved(String ltaaDataField14Reserved)
/*     */   {
/* 452 */     this.ltaaDataField14Reserved = ltaaDataField14Reserved;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\common\taa\LocationDetailTAABean.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */