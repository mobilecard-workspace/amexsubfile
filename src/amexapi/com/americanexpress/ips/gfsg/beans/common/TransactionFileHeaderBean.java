/*     */ package com.americanexpress.ips.gfsg.beans.common;
/*     */ 
/*     */ import com.americanexpress.ips.gfsg.enumerations.RecordType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class TransactionFileHeaderBean
/*     */ {
/*  15 */   private String recordType = RecordType.Transaction_File_Header.getRecordType();
/*  16 */   private String recordNumber = "00000001";
/*     */   private String submitterId;
/*     */   private String submitterFileReferenceNumber;
/*     */   private String submitterFileSequenceNumber;
/*     */   private String fileCreationDate;
/*     */   private String fileCreationTime;
/*  22 */   private String fileVersionNumber = "12010000";
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private String tfhField4reserved;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   private String tfhField10reserved;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTfhField10reserved()
/*     */   {
/*  41 */     return this.tfhField10reserved;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTfhField10reserved(String tfhField10reserved)
/*     */   {
/*  58 */     this.tfhField10reserved = tfhField10reserved;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTfhField4reserved()
/*     */   {
/*  71 */     return this.tfhField4reserved;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTfhField4reserved(String tfhField4reserved)
/*     */   {
/*  84 */     this.tfhField4reserved = tfhField4reserved;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getRecordType()
/*     */   {
/* 100 */     return this.recordType;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getRecordNumber()
/*     */   {
/* 119 */     return this.recordNumber;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getSubmitterId()
/*     */   {
/* 136 */     return this.submitterId;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getSubmitterFileReferenceNumber()
/*     */   {
/* 151 */     return this.submitterFileReferenceNumber;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getSubmitterFileSequenceNumber()
/*     */   {
/* 166 */     return this.submitterFileSequenceNumber;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getFileCreationDate()
/*     */   {
/* 187 */     return this.fileCreationDate;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getFileCreationTime()
/*     */   {
/* 208 */     return this.fileCreationTime;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getFileVersionNumber()
/*     */   {
/* 223 */     return this.fileVersionNumber;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setRecordType(String recordType)
/*     */   {
/* 239 */     this.recordType = recordType;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setRecordNumber(String recordNumber)
/*     */   {
/* 259 */     this.recordNumber = recordNumber;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setSubmitterId(String submitterId)
/*     */   {
/* 276 */     this.submitterId = submitterId;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setSubmitterFileReferenceNumber(String submitterFileReferenceNumber)
/*     */   {
/* 292 */     this.submitterFileReferenceNumber = submitterFileReferenceNumber;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setSubmitterFileSequenceNumber(String submitterFileSequenceNumber)
/*     */   {
/* 307 */     this.submitterFileSequenceNumber = submitterFileSequenceNumber;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setFileCreationDate(String fileCreationDate)
/*     */   {
/* 329 */     this.fileCreationDate = fileCreationDate;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setFileCreationTime(String fileCreationTime)
/*     */   {
/* 349 */     this.fileCreationTime = fileCreationTime;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setFileVersionNumber(String fileVersionNumber)
/*     */   {
/* 364 */     this.fileVersionNumber = fileVersionNumber;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\common\TransactionFileHeaderBean.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */