/*      */ package com.americanexpress.ips.gfsg.beans.common.taa.industrytype;
/*      */ 
/*      */ import com.americanexpress.ips.gfsg.beans.common.taa.IndustryTypeTAABean;
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ public class AirlineIndustryTAABean
/*      */   extends IndustryTypeTAABean
/*      */ {
/*      */   private String transactionType;
/*      */   private String ticketNumber;
/*      */   private String documentType;
/*      */   private String airlineProcessIdentifier;
/*      */   private String iataNumericCode;
/*      */   private String ticketingCarrierName;
/*      */   private String ticketIssueCity;
/*      */   private String ticketIssueDate;
/*      */   private String numberInParty;
/*      */   private String passengerName;
/*      */   private String conjunctionTicketIndicator;
/*      */   private String originalTransactionAmount;
/*      */   private String originalCurrencyCode;
/*      */   private String electronicTicketIndicator;
/*      */   private String totalNumberOfAirSegments;
/*      */   private String stopoverIndicator1;
/*      */   private String departureLocationCodeSegment1;
/*      */   private String departureDateSegment1;
/*      */   private String arrivalLocationCodeSegment1;
/*      */   private String segmentCarrierCode1;
/*      */   private String segment1FareBasis;
/*      */   private String classOfServiceCodeSegment1;
/*      */   private String flightNumberSegment1;
/*      */   private String reserved;
/*      */   private String segment1Fare;
/*      */   private String stopoverIndicator2;
/*      */   private String departureLocationCodeSegment2;
/*      */   private String departureDateSegment2;
/*      */   private String arrivalLocationCodeSegment2;
/*      */   private String segmentCarrierCode2;
/*      */   private String segment2FareBasis;
/*      */   private String classOfServiceCodeSegment2;
/*      */   private String flightNumberSegment2;
/*      */   private String segment2Fare;
/*      */   private String stopoverIndicator3;
/*      */   private String departureLocationCodeSegment3;
/*      */   private String departureDateSegment3;
/*      */   private String arrivalLocationCodeSegment3;
/*      */   private String segmentCarrierCode3;
/*      */   private String segment3FareBasis;
/*      */   private String classOfServiceCodeSegment3;
/*      */   private String flightNumberSegment3;
/*      */   private String segment3Fare;
/*      */   private String stopoverIndicator4;
/*      */   private String departureLocationCodeSegment4;
/*      */   private String departureDateSegment4;
/*      */   private String arrivalLocationCodeSegment4;
/*      */   private String segmentCarrierCode4;
/*      */   private String segment4FareBasis;
/*      */   private String classOfServiceCodeSegment4;
/*      */   private String flightNumberSegment4;
/*      */   private String segment4Fare;
/*      */   private String stopoverIndicator5;
/*      */   private String exchangedOrOriginalTicketNumber;
/*      */   
/*      */   public String getTransactionType()
/*      */   {
/*   98 */     return this.transactionType;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTransactionType(String transactionType)
/*      */   {
/*  120 */     this.transactionType = transactionType;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTicketNumber()
/*      */   {
/*  134 */     return this.ticketNumber;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTicketNumber(String ticketNumber)
/*      */   {
/*  149 */     this.ticketNumber = ticketNumber;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getDocumentType()
/*      */   {
/*  165 */     return this.documentType;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setDocumentType(String documentType)
/*      */   {
/*  182 */     this.documentType = documentType;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getAirlineProcessIdentifier()
/*      */   {
/*  198 */     return this.airlineProcessIdentifier;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setAirlineProcessIdentifier(String airlineProcessIdentifier)
/*      */   {
/*  215 */     this.airlineProcessIdentifier = airlineProcessIdentifier;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getIataNumericCode()
/*      */   {
/*  230 */     return this.iataNumericCode;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setIataNumericCode(String iataNumericCode)
/*      */   {
/*  246 */     this.iataNumericCode = iataNumericCode;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTicketingCarrierName()
/*      */   {
/*  260 */     return this.ticketingCarrierName;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTicketingCarrierName(String ticketingCarrierName)
/*      */   {
/*  275 */     this.ticketingCarrierName = ticketingCarrierName;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTicketIssueCity()
/*      */   {
/*  289 */     return this.ticketIssueCity;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTicketIssueCity(String ticketIssueCity)
/*      */   {
/*  304 */     this.ticketIssueCity = ticketIssueCity;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTicketIssueDate()
/*      */   {
/*  325 */     return this.ticketIssueDate;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTicketIssueDate(String ticketIssueDate)
/*      */   {
/*  347 */     this.ticketIssueDate = ticketIssueDate;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getNumberInParty()
/*      */   {
/*  361 */     return this.numberInParty;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setNumberInParty(String numberInParty)
/*      */   {
/*  376 */     this.numberInParty = numberInParty;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getPassengerName()
/*      */   {
/*  389 */     return this.passengerName;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setPassengerName(String passengerName)
/*      */   {
/*  403 */     this.passengerName = passengerName;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getConjunctionTicketIndicator()
/*      */   {
/*  422 */     return this.conjunctionTicketIndicator;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setConjunctionTicketIndicator(String conjunctionTicketIndicator)
/*      */   {
/*  442 */     this.conjunctionTicketIndicator = conjunctionTicketIndicator;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getOriginalTransactionAmount()
/*      */   {
/*  458 */     return this.originalTransactionAmount;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setOriginalTransactionAmount(String originalTransactionAmount)
/*      */   {
/*  475 */     this.originalTransactionAmount = originalTransactionAmount;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getOriginalCurrencyCode()
/*      */   {
/*  491 */     return this.originalCurrencyCode;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setOriginalCurrencyCode(String originalCurrencyCode)
/*      */   {
/*  508 */     this.originalCurrencyCode = originalCurrencyCode;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getElectronicTicketIndicator()
/*      */   {
/*  526 */     return this.electronicTicketIndicator;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setElectronicTicketIndicator(String electronicTicketIndicator)
/*      */   {
/*  545 */     this.electronicTicketIndicator = electronicTicketIndicator;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTotalNumberOfAirSegments()
/*      */   {
/*  560 */     return this.totalNumberOfAirSegments;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTotalNumberOfAirSegments(String totalNumberOfAirSegments)
/*      */   {
/*  576 */     this.totalNumberOfAirSegments = totalNumberOfAirSegments;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getStopoverIndicator1()
/*      */   {
/*  595 */     return this.stopoverIndicator1;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setStopoverIndicator1(String stopoverIndicator1)
/*      */   {
/*  615 */     this.stopoverIndicator1 = stopoverIndicator1;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getDepartureLocationCodeSegment1()
/*      */   {
/*  630 */     return this.departureLocationCodeSegment1;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setDepartureLocationCodeSegment1(String departureLocationCodeSegment1)
/*      */   {
/*  647 */     this.departureLocationCodeSegment1 = departureLocationCodeSegment1;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getDepartureDateSegment1()
/*      */   {
/*  668 */     return this.departureDateSegment1;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setDepartureDateSegment1(String departureDateSegment1)
/*      */   {
/*  690 */     this.departureDateSegment1 = departureDateSegment1;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getArrivalLocationCodeSegment1()
/*      */   {
/*  705 */     return this.arrivalLocationCodeSegment1;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setArrivalLocationCodeSegment1(String arrivalLocationCodeSegment1)
/*      */   {
/*  722 */     this.arrivalLocationCodeSegment1 = arrivalLocationCodeSegment1;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getSegmentCarrierCode1()
/*      */   {
/*  737 */     return this.segmentCarrierCode1;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSegmentCarrierCode1(String segmentCarrierCode1)
/*      */   {
/*  753 */     this.segmentCarrierCode1 = segmentCarrierCode1;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getSegment1FareBasis()
/*      */   {
/*  768 */     return this.segment1FareBasis;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSegment1FareBasis(String segment1FareBasis)
/*      */   {
/*  784 */     this.segment1FareBasis = segment1FareBasis;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getClassOfServiceCodeSegment1()
/*      */   {
/*  800 */     return this.classOfServiceCodeSegment1;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setClassOfServiceCodeSegment1(String classOfServiceCodeSegment1)
/*      */   {
/*  817 */     this.classOfServiceCodeSegment1 = classOfServiceCodeSegment1;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getFlightNumberSegment1()
/*      */   {
/*  833 */     return this.flightNumberSegment1;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setFlightNumberSegment1(String flightNumberSegment1)
/*      */   {
/*  850 */     this.flightNumberSegment1 = flightNumberSegment1;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getReserved()
/*      */   {
/*  863 */     return this.reserved;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setReserved(String reserved)
/*      */   {
/*  877 */     this.reserved = reserved;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getSegment1Fare()
/*      */   {
/*  890 */     return this.segment1Fare;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSegment1Fare(String segment1Fare)
/*      */   {
/*  904 */     this.segment1Fare = segment1Fare;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getStopoverIndicator2()
/*      */   {
/*  923 */     return this.stopoverIndicator2;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setStopoverIndicator2(String stopoverIndicator2)
/*      */   {
/*  943 */     this.stopoverIndicator2 = stopoverIndicator2;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getDepartureLocationCodeSegment2()
/*      */   {
/*  958 */     return this.departureLocationCodeSegment2;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setDepartureLocationCodeSegment2(String departureLocationCodeSegment2)
/*      */   {
/*  975 */     this.departureLocationCodeSegment2 = departureLocationCodeSegment2;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getDepartureDateSegment2()
/*      */   {
/*  996 */     return this.departureDateSegment2;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setDepartureDateSegment2(String departureDateSegment2)
/*      */   {
/* 1018 */     this.departureDateSegment2 = departureDateSegment2;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getArrivalLocationCodeSegment2()
/*      */   {
/* 1033 */     return this.arrivalLocationCodeSegment2;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setArrivalLocationCodeSegment2(String arrivalLocationCodeSegment2)
/*      */   {
/* 1050 */     this.arrivalLocationCodeSegment2 = arrivalLocationCodeSegment2;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getSegmentCarrierCode2()
/*      */   {
/* 1065 */     return this.segmentCarrierCode2;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSegmentCarrierCode2(String segmentCarrierCode2)
/*      */   {
/* 1081 */     this.segmentCarrierCode2 = segmentCarrierCode2;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getSegment2FareBasis()
/*      */   {
/* 1096 */     return this.segment2FareBasis;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSegment2FareBasis(String segment2FareBasis)
/*      */   {
/* 1112 */     this.segment2FareBasis = segment2FareBasis;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getClassOfServiceCodeSegment2()
/*      */   {
/* 1128 */     return this.classOfServiceCodeSegment2;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setClassOfServiceCodeSegment2(String classOfServiceCodeSegment2)
/*      */   {
/* 1145 */     this.classOfServiceCodeSegment2 = classOfServiceCodeSegment2;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getFlightNumberSegment2()
/*      */   {
/* 1161 */     return this.flightNumberSegment2;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setFlightNumberSegment2(String flightNumberSegment2)
/*      */   {
/* 1178 */     this.flightNumberSegment2 = flightNumberSegment2;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getSegment2Fare()
/*      */   {
/* 1191 */     return this.segment2Fare;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSegment2Fare(String segment2Fare)
/*      */   {
/* 1205 */     this.segment2Fare = segment2Fare;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getStopoverIndicator3()
/*      */   {
/* 1224 */     return this.stopoverIndicator3;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setStopoverIndicator3(String stopoverIndicator3)
/*      */   {
/* 1244 */     this.stopoverIndicator3 = stopoverIndicator3;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getDepartureLocationCodeSegment3()
/*      */   {
/* 1259 */     return this.departureLocationCodeSegment3;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setDepartureLocationCodeSegment3(String departureLocationCodeSegment3)
/*      */   {
/* 1276 */     this.departureLocationCodeSegment3 = departureLocationCodeSegment3;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getDepartureDateSegment3()
/*      */   {
/* 1297 */     return this.departureDateSegment3;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setDepartureDateSegment3(String departureDateSegment3)
/*      */   {
/* 1319 */     this.departureDateSegment3 = departureDateSegment3;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getArrivalLocationCodeSegment3()
/*      */   {
/* 1334 */     return this.arrivalLocationCodeSegment3;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setArrivalLocationCodeSegment3(String arrivalLocationCodeSegment3)
/*      */   {
/* 1351 */     this.arrivalLocationCodeSegment3 = arrivalLocationCodeSegment3;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getSegmentCarrierCode3()
/*      */   {
/* 1366 */     return this.segmentCarrierCode3;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSegmentCarrierCode3(String segmentCarrierCode3)
/*      */   {
/* 1382 */     this.segmentCarrierCode3 = segmentCarrierCode3;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getSegment3FareBasis()
/*      */   {
/* 1397 */     return this.segment3FareBasis;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSegment3FareBasis(String segment3FareBasis)
/*      */   {
/* 1413 */     this.segment3FareBasis = segment3FareBasis;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getClassOfServiceCodeSegment3()
/*      */   {
/* 1429 */     return this.classOfServiceCodeSegment3;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setClassOfServiceCodeSegment3(String classOfServiceCodeSegment3)
/*      */   {
/* 1446 */     this.classOfServiceCodeSegment3 = classOfServiceCodeSegment3;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getFlightNumberSegment3()
/*      */   {
/* 1462 */     return this.flightNumberSegment3;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setFlightNumberSegment3(String flightNumberSegment3)
/*      */   {
/* 1479 */     this.flightNumberSegment3 = flightNumberSegment3;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getSegment3Fare()
/*      */   {
/* 1492 */     return this.segment3Fare;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSegment3Fare(String segment3Fare)
/*      */   {
/* 1506 */     this.segment3Fare = segment3Fare;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getStopoverIndicator4()
/*      */   {
/* 1525 */     return this.stopoverIndicator4;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setStopoverIndicator4(String stopoverIndicator4)
/*      */   {
/* 1545 */     this.stopoverIndicator4 = stopoverIndicator4;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getDepartureLocationCodeSegment4()
/*      */   {
/* 1560 */     return this.departureLocationCodeSegment4;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setDepartureLocationCodeSegment4(String departureLocationCodeSegment4)
/*      */   {
/* 1577 */     this.departureLocationCodeSegment4 = departureLocationCodeSegment4;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getDepartureDateSegment4()
/*      */   {
/* 1598 */     return this.departureDateSegment4;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setDepartureDateSegment4(String departureDateSegment4)
/*      */   {
/* 1620 */     this.departureDateSegment4 = departureDateSegment4;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getArrivalLocationCodeSegment4()
/*      */   {
/* 1635 */     return this.arrivalLocationCodeSegment4;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setArrivalLocationCodeSegment4(String arrivalLocationCodeSegment4)
/*      */   {
/* 1652 */     this.arrivalLocationCodeSegment4 = arrivalLocationCodeSegment4;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getSegmentCarrierCode4()
/*      */   {
/* 1667 */     return this.segmentCarrierCode4;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSegmentCarrierCode4(String segmentCarrierCode4)
/*      */   {
/* 1683 */     this.segmentCarrierCode4 = segmentCarrierCode4;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getSegment4FareBasis()
/*      */   {
/* 1698 */     return this.segment4FareBasis;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSegment4FareBasis(String segment4FareBasis)
/*      */   {
/* 1714 */     this.segment4FareBasis = segment4FareBasis;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getClassOfServiceCodeSegment4()
/*      */   {
/* 1730 */     return this.classOfServiceCodeSegment4;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setClassOfServiceCodeSegment4(String classOfServiceCodeSegment4)
/*      */   {
/* 1747 */     this.classOfServiceCodeSegment4 = classOfServiceCodeSegment4;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getFlightNumberSegment4()
/*      */   {
/* 1763 */     return this.flightNumberSegment4;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setFlightNumberSegment4(String flightNumberSegment4)
/*      */   {
/* 1780 */     this.flightNumberSegment4 = flightNumberSegment4;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getSegment4Fare()
/*      */   {
/* 1793 */     return this.segment4Fare;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSegment4Fare(String segment4Fare)
/*      */   {
/* 1807 */     this.segment4Fare = segment4Fare;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getStopoverIndicator5()
/*      */   {
/* 1826 */     return this.stopoverIndicator5;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setStopoverIndicator5(String stopoverIndicator5)
/*      */   {
/* 1846 */     this.stopoverIndicator5 = stopoverIndicator5;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getExchangedOrOriginalTicketNumber()
/*      */   {
/* 1860 */     return this.exchangedOrOriginalTicketNumber;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setExchangedOrOriginalTicketNumber(String exchangedOrOriginalTicketNumber)
/*      */   {
/* 1876 */     this.exchangedOrOriginalTicketNumber = exchangedOrOriginalTicketNumber;
/*      */   }
/*      */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\common\taa\industrytype\AirlineIndustryTAABean.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */