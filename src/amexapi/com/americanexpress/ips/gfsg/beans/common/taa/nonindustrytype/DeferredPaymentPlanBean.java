/*     */ package com.americanexpress.ips.gfsg.beans.common.taa.nonindustrytype;
/*     */ 
/*     */ import com.americanexpress.ips.gfsg.beans.common.taa.NonIndustrySpecificTAABean;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class DeferredPaymentPlanBean
/*     */   extends NonIndustrySpecificTAABean
/*     */ {
/*     */   private String fullTransactionAmount;
/*     */   private String typeOfPlanCode;
/*     */   private String noOfInstallments;
/*     */   private String amountOfInstallment;
/*  19 */   private String installmentNumber = "0001";
/*     */   
/*     */   private String contractNumber;
/*     */   
/*     */   private String paymentTypeCode1;
/*     */   
/*     */   private String paymentTypeAmount1;
/*     */   
/*     */   private String paymentTypeCode2;
/*     */   
/*     */   private String paymentTypeAmount2;
/*     */   
/*     */   private String paymentTypeCode3;
/*     */   
/*     */   private String paymentTypeAmount3;
/*     */   
/*     */   private String paymentTypeCode4;
/*     */   
/*     */   private String paymentTypeAmount4;
/*     */   private String paymentTypeCode5;
/*     */   private String paymentTypeAmount5;
/*     */   
/*     */   public String getFullTransactionAmount()
/*     */   {
/*  43 */     return this.fullTransactionAmount;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setFullTransactionAmount(String fullTransactionAmount)
/*     */   {
/*  58 */     this.fullTransactionAmount = fullTransactionAmount;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTypeOfPlanCode()
/*     */   {
/*  78 */     return this.typeOfPlanCode;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTypeOfPlanCode(String typeOfPlanCode)
/*     */   {
/*  99 */     this.typeOfPlanCode = typeOfPlanCode;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getNoOfInstallments()
/*     */   {
/* 113 */     return this.noOfInstallments;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setNoOfInstallments(String noOfInstallments)
/*     */   {
/* 128 */     this.noOfInstallments = noOfInstallments;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getAmountOfInstallment()
/*     */   {
/* 142 */     return this.amountOfInstallment;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setAmountOfInstallment(String amountOfInstallment)
/*     */   {
/* 157 */     this.amountOfInstallment = amountOfInstallment;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getInstallmentNumber()
/*     */   {
/* 172 */     return this.installmentNumber;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setInstallmentNumber(String installmentNumber)
/*     */   {
/* 188 */     this.installmentNumber = installmentNumber;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getContractNumber()
/*     */   {
/* 202 */     return this.contractNumber;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setContractNumber(String contractNumber)
/*     */   {
/* 217 */     this.contractNumber = contractNumber;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getPaymentTypeCode1()
/*     */   {
/* 231 */     return this.paymentTypeCode1;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPaymentTypeCode1(String paymentTypeCode1)
/*     */   {
/* 246 */     this.paymentTypeCode1 = paymentTypeCode1;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getPaymentTypeAmount1()
/*     */   {
/* 260 */     return this.paymentTypeAmount1;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPaymentTypeAmount1(String paymentTypeAmount1)
/*     */   {
/* 275 */     this.paymentTypeAmount1 = paymentTypeAmount1;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getPaymentTypeCode2()
/*     */   {
/* 289 */     return this.paymentTypeCode2;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPaymentTypeCode2(String paymentTypeCode2)
/*     */   {
/* 304 */     this.paymentTypeCode2 = paymentTypeCode2;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getPaymentTypeAmount2()
/*     */   {
/* 318 */     return this.paymentTypeAmount2;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPaymentTypeAmount2(String paymentTypeAmount2)
/*     */   {
/* 333 */     this.paymentTypeAmount2 = paymentTypeAmount2;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getPaymentTypeCode3()
/*     */   {
/* 347 */     return this.paymentTypeCode3;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPaymentTypeCode3(String paymentTypeCode3)
/*     */   {
/* 362 */     this.paymentTypeCode3 = paymentTypeCode3;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getPaymentTypeAmount3()
/*     */   {
/* 376 */     return this.paymentTypeAmount3;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPaymentTypeAmount3(String paymentTypeAmount3)
/*     */   {
/* 391 */     this.paymentTypeAmount3 = paymentTypeAmount3;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getPaymentTypeCode4()
/*     */   {
/* 405 */     return this.paymentTypeCode4;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPaymentTypeCode4(String paymentTypeCode4)
/*     */   {
/* 420 */     this.paymentTypeCode4 = paymentTypeCode4;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getPaymentTypeAmount4()
/*     */   {
/* 434 */     return this.paymentTypeAmount4;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPaymentTypeAmount4(String paymentTypeAmount4)
/*     */   {
/* 449 */     this.paymentTypeAmount4 = paymentTypeAmount4;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getPaymentTypeCode5()
/*     */   {
/* 463 */     return this.paymentTypeCode5;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPaymentTypeCode5(String paymentTypeCode5)
/*     */   {
/* 478 */     this.paymentTypeCode5 = paymentTypeCode5;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getPaymentTypeAmount5()
/*     */   {
/* 492 */     return this.paymentTypeAmount5;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPaymentTypeAmount5(String paymentTypeAmount5)
/*     */   {
/* 507 */     this.paymentTypeAmount5 = paymentTypeAmount5;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\common\taa\nonindustrytype\DeferredPaymentPlanBean.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */