/*     */ package com.americanexpress.ips.gfsg.beans.common;
/*     */ 
/*     */ import com.americanexpress.ips.gfsg.enumerations.RecordType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class TransactionAdviceAddendumBean
/*     */ {
/*  14 */   private String recordType = RecordType.Transaction_Advice_Addendum.getRecordType();
/*     */   
/*     */ 
/*     */   private String recordNumber;
/*     */   
/*     */ 
/*     */   private String transactionIdentifier;
/*     */   
/*     */ 
/*     */   private String formatCode;
/*     */   
/*     */ 
/*     */   private String addendaTypeCode;
/*     */   
/*     */   private String reserved;
/*     */   
/*     */ 
/*     */   public String getRecordType()
/*     */   {
/*  33 */     return this.recordType;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setRecordType(String recordType)
/*     */   {
/*  49 */     this.recordType = recordType;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getRecordNumber()
/*     */   {
/*  63 */     return this.recordNumber;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setRecordNumber(String recordNumber)
/*     */   {
/*  78 */     this.recordNumber = recordNumber;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTransactionIdentifier()
/*     */   {
/*  94 */     return this.transactionIdentifier;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTransactionIdentifier(String transactionIdentifier)
/*     */   {
/* 111 */     this.transactionIdentifier = transactionIdentifier;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getFormatCode()
/*     */   {
/* 126 */     return this.formatCode;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setFormatCode(String formatCode)
/*     */   {
/* 142 */     this.formatCode = formatCode;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getAddendaTypeCode()
/*     */   {
/* 156 */     return this.addendaTypeCode;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setAddendaTypeCode(String addendaTypeCode)
/*     */   {
/* 171 */     this.addendaTypeCode = addendaTypeCode;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getReserved()
/*     */   {
/* 184 */     return this.reserved;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setReserved(String reserved)
/*     */   {
/* 198 */     this.reserved = reserved;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\common\TransactionAdviceAddendumBean.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */