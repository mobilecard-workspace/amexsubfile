/*     */ package com.americanexpress.ips.gfsg.beans.common.taa.industrytype;
/*     */ 
/*     */ import com.americanexpress.ips.gfsg.beans.common.taa.IndustryTypeTAABean;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class LodgingIndustryTAABean
/*     */   extends IndustryTypeTAABean
/*     */ {
/*     */   private String lodgingSpecialProgramCode;
/*     */   private String lodgingCheckInDate;
/*     */   private String lodgingCheckOutDate;
/*     */   private String lodgingRoomRate1;
/*     */   private String numberOfNightsAtRoomRate1;
/*     */   private String lodgingRoomRate2;
/*     */   private String numberOfNightsAtRoomRate2;
/*     */   private String lodgingRoomRate3;
/*     */   private String numberOfNightsAtRoomRate3;
/*     */   private String lodgingRenterName;
/*     */   private String lodgingFolioNumber;
/*     */   private String lodgingIndustryDataField9Reserved;
/*     */   private String lodgingIndustryDataField12Reserved;
/*     */   private String lodgingIndustryDataField15Reserved;
/*     */   private String lodgingIndustryDataField20Reserved;
/*     */   
/*     */   public String getLodgingSpecialProgramCode()
/*     */   {
/*  54 */     return this.lodgingSpecialProgramCode;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLodgingSpecialProgramCode(String lodgingSpecialProgramCode)
/*     */   {
/*  74 */     this.lodgingSpecialProgramCode = lodgingSpecialProgramCode;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getLodgingCheckInDate()
/*     */   {
/*  96 */     return this.lodgingCheckInDate;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLodgingCheckInDate(String lodgingCheckInDate)
/*     */   {
/* 119 */     this.lodgingCheckInDate = lodgingCheckInDate;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getLodgingCheckOutDate()
/*     */   {
/* 141 */     return this.lodgingCheckOutDate;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLodgingCheckOutDate(String lodgingCheckOutDate)
/*     */   {
/* 164 */     this.lodgingCheckOutDate = lodgingCheckOutDate;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getLodgingRoomRate1()
/*     */   {
/* 178 */     return this.lodgingRoomRate1;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLodgingRoomRate1(String lodgingRoomRate1)
/*     */   {
/* 193 */     this.lodgingRoomRate1 = lodgingRoomRate1;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getNumberOfNightsAtRoomRate1()
/*     */   {
/* 207 */     return this.numberOfNightsAtRoomRate1;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setNumberOfNightsAtRoomRate1(String numberOfNightsAtRoomRate1)
/*     */   {
/* 222 */     this.numberOfNightsAtRoomRate1 = numberOfNightsAtRoomRate1;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getLodgingRoomRate2()
/*     */   {
/* 236 */     return this.lodgingRoomRate2;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLodgingRoomRate2(String lodgingRoomRate2)
/*     */   {
/* 251 */     this.lodgingRoomRate2 = lodgingRoomRate2;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getNumberOfNightsAtRoomRate2()
/*     */   {
/* 265 */     return this.numberOfNightsAtRoomRate2;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setNumberOfNightsAtRoomRate2(String numberOfNightsAtRoomRate2)
/*     */   {
/* 280 */     this.numberOfNightsAtRoomRate2 = numberOfNightsAtRoomRate2;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getLodgingRoomRate3()
/*     */   {
/* 294 */     return this.lodgingRoomRate3;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLodgingRoomRate3(String lodgingRoomRate3)
/*     */   {
/* 309 */     this.lodgingRoomRate3 = lodgingRoomRate3;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getNumberOfNightsAtRoomRate3()
/*     */   {
/* 323 */     return this.numberOfNightsAtRoomRate3;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setNumberOfNightsAtRoomRate3(String numberOfNightsAtRoomRate3)
/*     */   {
/* 338 */     this.numberOfNightsAtRoomRate3 = numberOfNightsAtRoomRate3;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getLodgingRenterName()
/*     */   {
/* 353 */     return this.lodgingRenterName;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLodgingRenterName(String lodgingRenterName)
/*     */   {
/* 369 */     this.lodgingRenterName = lodgingRenterName;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getLodgingFolioNumber()
/*     */   {
/* 385 */     return this.lodgingFolioNumber;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLodgingFolioNumber(String lodgingFolioNumber)
/*     */   {
/* 402 */     this.lodgingFolioNumber = lodgingFolioNumber;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getLodgingIndustryDataField9Reserved()
/*     */   {
/* 415 */     return this.lodgingIndustryDataField9Reserved;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLodgingIndustryDataField9Reserved(String lodgingIndustryDataField9Reserved)
/*     */   {
/* 430 */     this.lodgingIndustryDataField9Reserved = lodgingIndustryDataField9Reserved;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getLodgingIndustryDataField12Reserved()
/*     */   {
/* 443 */     return this.lodgingIndustryDataField12Reserved;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLodgingIndustryDataField12Reserved(String lodgingIndustryDataField12Reserved)
/*     */   {
/* 458 */     this.lodgingIndustryDataField12Reserved = lodgingIndustryDataField12Reserved;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getLodgingIndustryDataField15Reserved()
/*     */   {
/* 471 */     return this.lodgingIndustryDataField15Reserved;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLodgingIndustryDataField15Reserved(String lodgingIndustryDataField15Reserved)
/*     */   {
/* 486 */     this.lodgingIndustryDataField15Reserved = lodgingIndustryDataField15Reserved;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getLodgingIndustryDataField20Reserved()
/*     */   {
/* 504 */     return this.lodgingIndustryDataField20Reserved;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLodgingIndustryDataField20Reserved(String lodgingIndustryDataField20Reserved)
/*     */   {
/* 524 */     this.lodgingIndustryDataField20Reserved = lodgingIndustryDataField20Reserved;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\common\taa\industrytype\LodgingIndustryTAABean.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */