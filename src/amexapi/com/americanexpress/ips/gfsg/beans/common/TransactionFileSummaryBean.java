/*     */ package com.americanexpress.ips.gfsg.beans.common;
/*     */ 
/*     */ import com.americanexpress.ips.gfsg.enumerations.RecordType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class TransactionFileSummaryBean
/*     */ {
/*  14 */   private String recordType = RecordType.Transaction_File_Summary.getRecordType();
/*     */   
/*     */   private String recordNumber;
/*     */   
/*     */   private String numberOfDebits;
/*     */   
/*     */   private String hashTotalDebitAmount;
/*     */   
/*     */   private String numberOfCredits;
/*     */   
/*     */   private String hashTotalCreditAmount;
/*     */   
/*     */   private String hashTotalAmount;
/*     */   
/*     */   private String tfsField4reserved;
/*     */   
/*     */   private String tfsField7reserved;
/*     */   
/*     */   private String tfsField9reserved;
/*     */   
/*     */   private String tfsField11reserved;
/*     */   
/*     */   public String getRecordType()
/*     */   {
/*  38 */     return this.recordType;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setRecordType(String recordType)
/*     */   {
/*  54 */     this.recordType = recordType;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getRecordNumber()
/*     */   {
/*  68 */     return this.recordNumber;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setRecordNumber(String recordNumber)
/*     */   {
/*  83 */     this.recordNumber = recordNumber;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getNumberOfDebits()
/*     */   {
/*  98 */     return this.numberOfDebits;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setNumberOfDebits(String numberOfDebits)
/*     */   {
/* 114 */     this.numberOfDebits = numberOfDebits;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getHashTotalDebitAmount()
/*     */   {
/* 131 */     return this.hashTotalDebitAmount;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setHashTotalDebitAmount(String hashTotalDebitAmount)
/*     */   {
/* 149 */     this.hashTotalDebitAmount = hashTotalDebitAmount;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getNumberOfCredits()
/*     */   {
/* 164 */     return this.numberOfCredits;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setNumberOfCredits(String numberOfCredits)
/*     */   {
/* 180 */     this.numberOfCredits = numberOfCredits;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getHashTotalCreditAmount()
/*     */   {
/* 197 */     return this.hashTotalCreditAmount;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setHashTotalCreditAmount(String hashTotalCreditAmount)
/*     */   {
/* 215 */     this.hashTotalCreditAmount = hashTotalCreditAmount;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getHashTotalAmount()
/*     */   {
/* 231 */     return this.hashTotalAmount;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setHashTotalAmount(String hashTotalAmount)
/*     */   {
/* 248 */     this.hashTotalAmount = hashTotalAmount;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTfsField4reserved()
/*     */   {
/* 261 */     return this.tfsField4reserved;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTfsField4reserved(String tfsField4reserved)
/*     */   {
/* 275 */     this.tfsField4reserved = tfsField4reserved;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTfsField7reserved()
/*     */   {
/* 288 */     return this.tfsField7reserved;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTfsField7reserved(String tfsField7reserved)
/*     */   {
/* 302 */     this.tfsField7reserved = tfsField7reserved;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTfsField9reserved()
/*     */   {
/* 315 */     return this.tfsField9reserved;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTfsField9reserved(String tfsField9reserved)
/*     */   {
/* 329 */     this.tfsField9reserved = tfsField9reserved;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTfsField11reserved()
/*     */   {
/* 342 */     return this.tfsField11reserved;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTfsField11reserved(String tfsField11reserved)
/*     */   {
/* 356 */     this.tfsField11reserved = tfsField11reserved;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\common\TransactionFileSummaryBean.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */