/*      */ package com.americanexpress.ips.gfsg.beans.common.taa.industrytype;
/*      */ 
/*      */ import com.americanexpress.ips.gfsg.beans.common.taa.IndustryTypeTAABean;
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ public class TravelCruiseIndustryTAABean
/*      */   extends IndustryTypeTAABean
/*      */ {
/*      */   private String iataCarrierCode;
/*      */   private String iataAgencyNumber;
/*      */   private String travelPackageIndicator;
/*      */   private String passengerName;
/*      */   private String travelTicketNumber;
/*      */   private String travelDepartureDateSegment1;
/*      */   private String travelDestinationCodeSegment1;
/*      */   private String travelDepartureAirportSegment1;
/*      */   private String airCarrierCodeSegment1;
/*      */   private String flightNumberSegment1;
/*      */   private String classCodeSegment1;
/*      */   private String travelDepartureDateSegment2;
/*      */   private String travelDestinationCodeSegment2;
/*      */   private String travelDepartureAirportSegment2;
/*      */   private String airCarrierCodeSegment2;
/*      */   private String flightNumberSegment2;
/*      */   private String classCodeSegment2;
/*      */   private String travelDepartureDateSegment3;
/*      */   private String travelDestinationCodeSegment3;
/*      */   private String travelDepartureAirportSegment3;
/*      */   private String airCarrierCodeSegment3;
/*      */   private String flightNumberSegment3;
/*      */   private String classCodeSegment3;
/*      */   private String travelDepartureDateSegment4;
/*      */   private String travelDestinationCodeSegment4;
/*      */   private String travelDepartureAirportSegment4;
/*      */   private String airCarrierCodeSegment4;
/*      */   private String flightNumberSegment4;
/*      */   private String classCodeSegment4;
/*      */   private String lodgingCheckInDate;
/*      */   private String lodgingCheckOutDate;
/*      */   private String lodgingRoomRate1;
/*      */   private String numberOfNightsAtRoomRate1;
/*      */   private String lodgingName;
/*      */   private String lodgingRegionCode;
/*      */   private String lodgingCountryCode;
/*      */   private String lodgingCityName;
/*      */   private String reserved;
/*      */   
/*      */   public String getIataCarrierCode()
/*      */   {
/*   76 */     return this.iataCarrierCode;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setIataCarrierCode(String iataCarrierCode)
/*      */   {
/*   92 */     this.iataCarrierCode = iataCarrierCode;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getIataAgencyNumber()
/*      */   {
/*  106 */     return this.iataAgencyNumber;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setIataAgencyNumber(String iataAgencyNumber)
/*      */   {
/*  121 */     this.iataAgencyNumber = iataAgencyNumber;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTravelPackageIndicator()
/*      */   {
/*  143 */     return this.travelPackageIndicator;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTravelPackageIndicator(String travelPackageIndicator)
/*      */   {
/*  166 */     this.travelPackageIndicator = travelPackageIndicator;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getPassengerName()
/*      */   {
/*  179 */     return this.passengerName;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setPassengerName(String passengerName)
/*      */   {
/*  193 */     this.passengerName = passengerName;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTravelTicketNumber()
/*      */   {
/*  207 */     return this.travelTicketNumber;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTravelTicketNumber(String travelTicketNumber)
/*      */   {
/*  222 */     this.travelTicketNumber = travelTicketNumber;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTravelDepartureDateSegment1()
/*      */   {
/*  243 */     return this.travelDepartureDateSegment1;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTravelDepartureDateSegment1(String travelDepartureDateSegment1)
/*      */   {
/*  266 */     this.travelDepartureDateSegment1 = travelDepartureDateSegment1;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTravelDestinationCodeSegment1()
/*      */   {
/*  282 */     return this.travelDestinationCodeSegment1;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTravelDestinationCodeSegment1(String travelDestinationCodeSegment1)
/*      */   {
/*  300 */     this.travelDestinationCodeSegment1 = travelDestinationCodeSegment1;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTravelDepartureAirportSegment1()
/*      */   {
/*  316 */     return this.travelDepartureAirportSegment1;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTravelDepartureAirportSegment1(String travelDepartureAirportSegment1)
/*      */   {
/*  334 */     this.travelDepartureAirportSegment1 = travelDepartureAirportSegment1;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getAirCarrierCodeSegment1()
/*      */   {
/*  349 */     return this.airCarrierCodeSegment1;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setAirCarrierCodeSegment1(String airCarrierCodeSegment1)
/*      */   {
/*  365 */     this.airCarrierCodeSegment1 = airCarrierCodeSegment1;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getFlightNumberSegment1()
/*      */   {
/*  379 */     return this.flightNumberSegment1;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setFlightNumberSegment1(String flightNumberSegment1)
/*      */   {
/*  394 */     this.flightNumberSegment1 = flightNumberSegment1;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getClassCodeSegment1()
/*      */   {
/*  410 */     return this.classCodeSegment1;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setClassCodeSegment1(String classCodeSegment1)
/*      */   {
/*  427 */     this.classCodeSegment1 = classCodeSegment1;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTravelDepartureDateSegment2()
/*      */   {
/*  448 */     return this.travelDepartureDateSegment2;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTravelDepartureDateSegment2(String travelDepartureDateSegment2)
/*      */   {
/*  471 */     this.travelDepartureDateSegment2 = travelDepartureDateSegment2;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTravelDestinationCodeSegment2()
/*      */   {
/*  487 */     return this.travelDestinationCodeSegment2;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTravelDestinationCodeSegment2(String travelDestinationCodeSegment2)
/*      */   {
/*  505 */     this.travelDestinationCodeSegment2 = travelDestinationCodeSegment2;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTravelDepartureAirportSegment2()
/*      */   {
/*  521 */     return this.travelDepartureAirportSegment2;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTravelDepartureAirportSegment2(String travelDepartureAirportSegment2)
/*      */   {
/*  539 */     this.travelDepartureAirportSegment2 = travelDepartureAirportSegment2;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getAirCarrierCodeSegment2()
/*      */   {
/*  554 */     return this.airCarrierCodeSegment2;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setAirCarrierCodeSegment2(String airCarrierCodeSegment2)
/*      */   {
/*  570 */     this.airCarrierCodeSegment2 = airCarrierCodeSegment2;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getFlightNumberSegment2()
/*      */   {
/*  584 */     return this.flightNumberSegment2;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setFlightNumberSegment2(String flightNumberSegment2)
/*      */   {
/*  599 */     this.flightNumberSegment2 = flightNumberSegment2;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getClassCodeSegment2()
/*      */   {
/*  615 */     return this.classCodeSegment2;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setClassCodeSegment2(String classCodeSegment2)
/*      */   {
/*  632 */     this.classCodeSegment2 = classCodeSegment2;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTravelDepartureDateSegment3()
/*      */   {
/*  653 */     return this.travelDepartureDateSegment3;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTravelDepartureDateSegment3(String travelDepartureDateSegment3)
/*      */   {
/*  676 */     this.travelDepartureDateSegment3 = travelDepartureDateSegment3;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTravelDestinationCodeSegment3()
/*      */   {
/*  692 */     return this.travelDestinationCodeSegment3;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTravelDestinationCodeSegment3(String travelDestinationCodeSegment3)
/*      */   {
/*  710 */     this.travelDestinationCodeSegment3 = travelDestinationCodeSegment3;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTravelDepartureAirportSegment3()
/*      */   {
/*  726 */     return this.travelDepartureAirportSegment3;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTravelDepartureAirportSegment3(String travelDepartureAirportSegment3)
/*      */   {
/*  744 */     this.travelDepartureAirportSegment3 = travelDepartureAirportSegment3;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getAirCarrierCodeSegment3()
/*      */   {
/*  759 */     return this.airCarrierCodeSegment3;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setAirCarrierCodeSegment3(String airCarrierCodeSegment3)
/*      */   {
/*  775 */     this.airCarrierCodeSegment3 = airCarrierCodeSegment3;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getFlightNumberSegment3()
/*      */   {
/*  789 */     return this.flightNumberSegment3;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setFlightNumberSegment3(String flightNumberSegment3)
/*      */   {
/*  804 */     this.flightNumberSegment3 = flightNumberSegment3;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getClassCodeSegment3()
/*      */   {
/*  820 */     return this.classCodeSegment3;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setClassCodeSegment3(String classCodeSegment3)
/*      */   {
/*  837 */     this.classCodeSegment3 = classCodeSegment3;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTravelDepartureDateSegment4()
/*      */   {
/*  858 */     return this.travelDepartureDateSegment4;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTravelDepartureDateSegment4(String travelDepartureDateSegment4)
/*      */   {
/*  881 */     this.travelDepartureDateSegment4 = travelDepartureDateSegment4;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTravelDestinationCodeSegment4()
/*      */   {
/*  897 */     return this.travelDestinationCodeSegment4;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTravelDestinationCodeSegment4(String travelDestinationCodeSegment4)
/*      */   {
/*  915 */     this.travelDestinationCodeSegment4 = travelDestinationCodeSegment4;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTravelDepartureAirportSegment4()
/*      */   {
/*  931 */     return this.travelDepartureAirportSegment4;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTravelDepartureAirportSegment4(String travelDepartureAirportSegment4)
/*      */   {
/*  949 */     this.travelDepartureAirportSegment4 = travelDepartureAirportSegment4;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getAirCarrierCodeSegment4()
/*      */   {
/*  964 */     return this.airCarrierCodeSegment4;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setAirCarrierCodeSegment4(String airCarrierCodeSegment4)
/*      */   {
/*  980 */     this.airCarrierCodeSegment4 = airCarrierCodeSegment4;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getFlightNumberSegment4()
/*      */   {
/*  994 */     return this.flightNumberSegment4;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setFlightNumberSegment4(String flightNumberSegment4)
/*      */   {
/* 1009 */     this.flightNumberSegment4 = flightNumberSegment4;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getClassCodeSegment4()
/*      */   {
/* 1025 */     return this.classCodeSegment4;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setClassCodeSegment4(String classCodeSegment4)
/*      */   {
/* 1042 */     this.classCodeSegment4 = classCodeSegment4;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getLodgingCheckInDate()
/*      */   {
/* 1064 */     return this.lodgingCheckInDate;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setLodgingCheckInDate(String lodgingCheckInDate)
/*      */   {
/* 1087 */     this.lodgingCheckInDate = lodgingCheckInDate;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getLodgingCheckOutDate()
/*      */   {
/* 1109 */     return this.lodgingCheckOutDate;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setLodgingCheckOutDate(String lodgingCheckOutDate)
/*      */   {
/* 1132 */     this.lodgingCheckOutDate = lodgingCheckOutDate;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getLodgingRoomRate1()
/*      */   {
/* 1146 */     return this.lodgingRoomRate1;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setLodgingRoomRate1(String lodgingRoomRate1)
/*      */   {
/* 1161 */     this.lodgingRoomRate1 = lodgingRoomRate1;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getNumberOfNightsAtRoomRate1()
/*      */   {
/* 1175 */     return this.numberOfNightsAtRoomRate1;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setNumberOfNightsAtRoomRate1(String numberOfNightsAtRoomRate1)
/*      */   {
/* 1190 */     this.numberOfNightsAtRoomRate1 = numberOfNightsAtRoomRate1;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getLodgingName()
/*      */   {
/* 1205 */     return this.lodgingName;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setLodgingName(String lodgingName)
/*      */   {
/* 1221 */     this.lodgingName = lodgingName;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getLodgingRegionCode()
/*      */   {
/* 1235 */     return this.lodgingRegionCode;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setLodgingRegionCode(String lodgingRegionCode)
/*      */   {
/* 1250 */     this.lodgingRegionCode = lodgingRegionCode;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getLodgingCountryCode()
/*      */   {
/* 1263 */     return this.lodgingCountryCode;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setLodgingCountryCode(String lodgingCountryCode)
/*      */   {
/* 1277 */     this.lodgingCountryCode = lodgingCountryCode;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getLodgingCityName()
/*      */   {
/* 1291 */     return this.lodgingCityName;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setLodgingCityName(String lodgingCityName)
/*      */   {
/* 1306 */     this.lodgingCityName = lodgingCityName;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getReserved()
/*      */   {
/* 1324 */     return this.reserved;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setReserved(String reserved)
/*      */   {
/* 1343 */     this.reserved = reserved;
/*      */   }
/*      */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\common\taa\industrytype\TravelCruiseIndustryTAABean.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */