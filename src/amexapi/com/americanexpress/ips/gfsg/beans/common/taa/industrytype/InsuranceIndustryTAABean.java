/*     */ package com.americanexpress.ips.gfsg.beans.common.taa.industrytype;
/*     */ 
/*     */ import com.americanexpress.ips.gfsg.beans.common.taa.IndustryTypeTAABean;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class InsuranceIndustryTAABean
/*     */   extends IndustryTypeTAABean
/*     */ {
/*     */   private String insurancePolicyNumber;
/*     */   private String insuranceCoverageStartDate;
/*     */   private String insuranceCoverageEndDate;
/*     */   private String insurancePolicyPremiumFrequency;
/*     */   private String additionalInsurancePolicyNumber;
/*     */   private String typeOfPolicy;
/*     */   private String nameOfInsured;
/*     */   
/*     */   public String getInsurancePolicyNumber()
/*     */   {
/*  33 */     return this.insurancePolicyNumber;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setInsurancePolicyNumber(String insurancePolicyNumber)
/*     */   {
/*  48 */     this.insurancePolicyNumber = insurancePolicyNumber;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getInsuranceCoverageStartDate()
/*     */   {
/*  70 */     return this.insuranceCoverageStartDate;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setInsuranceCoverageStartDate(String insuranceCoverageStartDate)
/*     */   {
/*  93 */     this.insuranceCoverageStartDate = insuranceCoverageStartDate;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getInsuranceCoverageEndDate()
/*     */   {
/* 115 */     return this.insuranceCoverageEndDate;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setInsuranceCoverageEndDate(String insuranceCoverageEndDate)
/*     */   {
/* 138 */     this.insuranceCoverageEndDate = insuranceCoverageEndDate;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getInsurancePolicyPremiumFrequency()
/*     */   {
/* 161 */     return this.insurancePolicyPremiumFrequency;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setInsurancePolicyPremiumFrequency(String insurancePolicyPremiumFrequency)
/*     */   {
/* 186 */     this.insurancePolicyPremiumFrequency = insurancePolicyPremiumFrequency;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getAdditionalInsurancePolicyNumber()
/*     */   {
/* 200 */     return this.additionalInsurancePolicyNumber;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setAdditionalInsurancePolicyNumber(String additionalInsurancePolicyNumber)
/*     */   {
/* 216 */     this.additionalInsurancePolicyNumber = additionalInsurancePolicyNumber;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTypeOfPolicy()
/*     */   {
/* 231 */     return this.typeOfPolicy;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTypeOfPolicy(String typeOfPolicy)
/*     */   {
/* 247 */     this.typeOfPolicy = typeOfPolicy;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getNameOfInsured()
/*     */   {
/* 262 */     return this.nameOfInsured;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setNameOfInsured(String nameOfInsured)
/*     */   {
/* 278 */     this.nameOfInsured = nameOfInsured;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\common\taa\industrytype\InsuranceIndustryTAABean.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */