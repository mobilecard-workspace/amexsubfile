/*     */ package com.americanexpress.ips.gfsg.beans.common.taa.industrytype;
/*     */ 
/*     */ import com.americanexpress.ips.gfsg.beans.common.taa.IndustryTypeTAABean;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class AutoRentalIndustryTAABean
/*     */   extends IndustryTypeTAABean
/*     */ {
/*     */   private String autoRentalAgreementNumber;
/*     */   private String autoRentalPickupLocation;
/*     */   private String autoRentalPickupCityName;
/*     */   private String autoRentalPickupRegionCode;
/*     */   private String autoRentalPickupCountryCode;
/*     */   private String autoRentalPickupDate;
/*     */   private String autoRentalPickupTime;
/*     */   private String autoRentalReturnCityName;
/*     */   private String autoRentalReturnRegionCode;
/*     */   private String autoRentalReturnCountryCode;
/*     */   private String autoRentalReturnDate;
/*     */   private String autoRentalReturnTime;
/*     */   private String autoRentalRenterName;
/*     */   private String autoRentalVehicleClassId;
/*     */   private String autoRentalDistance;
/*     */   private String autoRentalDistanceUnitOfMeasure;
/*     */   private String autoRentalAuditAdjustmentIndicator;
/*     */   private String autoRentalAuditAdjustmentAmount;
/*     */   private String autoreserved;
/*     */   private String returnDropoffLocation;
/*     */   private String vehicleIdentificationNumber;
/*     */   private String driverIdentificationNumber;
/*     */   private String driverTaxNumber;
/*     */   
/*     */   public String getReturnDropoffLocation()
/*     */   {
/*  49 */     return this.returnDropoffLocation;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setReturnDropoffLocation(String returnDropoffLocation)
/*     */   {
/*  63 */     this.returnDropoffLocation = returnDropoffLocation;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getVehicleIdentificationNumber()
/*     */   {
/*  76 */     return this.vehicleIdentificationNumber;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setVehicleIdentificationNumber(String vehicleIdentificationNumber)
/*     */   {
/*  90 */     this.vehicleIdentificationNumber = vehicleIdentificationNumber;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getDriverIdentificationNumber()
/*     */   {
/* 103 */     return this.driverIdentificationNumber;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setDriverIdentificationNumber(String driverIdentificationNumber)
/*     */   {
/* 117 */     this.driverIdentificationNumber = driverIdentificationNumber;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getDriverTaxNumber()
/*     */   {
/* 130 */     return this.driverTaxNumber;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setDriverTaxNumber(String driverTaxNumber)
/*     */   {
/* 144 */     this.driverTaxNumber = driverTaxNumber;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getAutoRentalAgreementNumber()
/*     */   {
/* 159 */     return this.autoRentalAgreementNumber;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setAutoRentalAgreementNumber(String autoRentalAgreementNumber)
/*     */   {
/* 175 */     this.autoRentalAgreementNumber = autoRentalAgreementNumber;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getAutoRentalPickupLocation()
/*     */   {
/* 189 */     return this.autoRentalPickupLocation;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setAutoRentalPickupLocation(String autoRentalPickupLocation)
/*     */   {
/* 204 */     this.autoRentalPickupLocation = autoRentalPickupLocation;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getAutoRentalPickupCityName()
/*     */   {
/* 218 */     return this.autoRentalPickupCityName;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setAutoRentalPickupCityName(String autoRentalPickupCityName)
/*     */   {
/* 233 */     this.autoRentalPickupCityName = autoRentalPickupCityName;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getAutoRentalPickupRegionCode()
/*     */   {
/* 248 */     return this.autoRentalPickupRegionCode;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setAutoRentalPickupRegionCode(String autoRentalPickupRegionCode)
/*     */   {
/* 264 */     this.autoRentalPickupRegionCode = autoRentalPickupRegionCode;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getAutoRentalPickupCountryCode()
/*     */   {
/* 278 */     return this.autoRentalPickupCountryCode;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setAutoRentalPickupCountryCode(String autoRentalPickupCountryCode)
/*     */   {
/* 294 */     this.autoRentalPickupCountryCode = autoRentalPickupCountryCode;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getAutoRentalPickupDate()
/*     */   {
/* 316 */     return this.autoRentalPickupDate;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setAutoRentalPickupDate(String autoRentalPickupDate)
/*     */   {
/* 339 */     this.autoRentalPickupDate = autoRentalPickupDate;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getAutoRentalPickupTime()
/*     */   {
/* 360 */     return this.autoRentalPickupTime;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setAutoRentalPickupTime(String autoRentalPickupTime)
/*     */   {
/* 382 */     this.autoRentalPickupTime = autoRentalPickupTime;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getAutoRentalReturnCityName()
/*     */   {
/* 396 */     return this.autoRentalReturnCityName;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setAutoRentalReturnCityName(String autoRentalReturnCityName)
/*     */   {
/* 411 */     this.autoRentalReturnCityName = autoRentalReturnCityName;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getAutoRentalReturnRegionCode()
/*     */   {
/* 425 */     return this.autoRentalReturnRegionCode;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setAutoRentalReturnRegionCode(String autoRentalReturnRegionCode)
/*     */   {
/* 440 */     this.autoRentalReturnRegionCode = autoRentalReturnRegionCode;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getAutoRentalReturnCountryCode()
/*     */   {
/* 454 */     return this.autoRentalReturnCountryCode;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setAutoRentalReturnCountryCode(String autoRentalReturnCountryCode)
/*     */   {
/* 470 */     this.autoRentalReturnCountryCode = autoRentalReturnCountryCode;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getAutoRentalReturnDate()
/*     */   {
/* 491 */     return this.autoRentalReturnDate;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setAutoRentalReturnDate(String autoRentalReturnDate)
/*     */   {
/* 513 */     this.autoRentalReturnDate = autoRentalReturnDate;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getAutoRentalReturnTime()
/*     */   {
/* 534 */     return this.autoRentalReturnTime;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setAutoRentalReturnTime(String autoRentalReturnTime)
/*     */   {
/* 556 */     this.autoRentalReturnTime = autoRentalReturnTime;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getAutoRentalRenterName()
/*     */   {
/* 571 */     return this.autoRentalRenterName;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setAutoRentalRenterName(String autoRentalRenterName)
/*     */   {
/* 587 */     this.autoRentalRenterName = autoRentalRenterName;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getAutoRentalVehicleClassId()
/*     */   {
/* 601 */     return this.autoRentalVehicleClassId;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setAutoRentalVehicleClassId(String autoRentalVehicleClassId)
/*     */   {
/* 616 */     this.autoRentalVehicleClassId = autoRentalVehicleClassId;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getAutoRentalDistance()
/*     */   {
/* 631 */     return this.autoRentalDistance;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setAutoRentalDistance(String autoRentalDistance)
/*     */   {
/* 647 */     this.autoRentalDistance = autoRentalDistance;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getAutoRentalDistanceUnitOfMeasure()
/*     */   {
/* 666 */     return this.autoRentalDistanceUnitOfMeasure;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setAutoRentalDistanceUnitOfMeasure(String autoRentalDistanceUnitOfMeasure)
/*     */   {
/* 687 */     this.autoRentalDistanceUnitOfMeasure = autoRentalDistanceUnitOfMeasure;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getAutoRentalAuditAdjustmentIndicator()
/*     */   {
/* 719 */     return this.autoRentalAuditAdjustmentIndicator;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setAutoRentalAuditAdjustmentIndicator(String autoRentalAuditAdjustmentIndicator)
/*     */   {
/* 753 */     this.autoRentalAuditAdjustmentIndicator = autoRentalAuditAdjustmentIndicator;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getAutoRentalAuditAdjustmentAmount()
/*     */   {
/* 766 */     return this.autoRentalAuditAdjustmentAmount;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setAutoRentalAuditAdjustmentAmount(String autoRentalAuditAdjustmentAmount)
/*     */   {
/* 781 */     this.autoRentalAuditAdjustmentAmount = autoRentalAuditAdjustmentAmount;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getAutoreserved()
/*     */   {
/* 794 */     return this.autoreserved;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setAutoreserved(String autoreserved)
/*     */   {
/* 808 */     this.autoreserved = autoreserved;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\common\taa\industrytype\AutoRentalIndustryTAABean.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */