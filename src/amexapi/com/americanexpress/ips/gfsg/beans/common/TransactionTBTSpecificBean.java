/*    */ package com.americanexpress.ips.gfsg.beans.common;
/*    */ 
/*    */ import java.util.List;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class TransactionTBTSpecificBean
/*    */ {
/*    */   private TransactionBatchTrailerBean transactionBatchTrailerBean;
/*    */   private List<TransactionAdviceBasicBean> transactionAdviceBasicType;
/*    */   
/*    */   public TransactionBatchTrailerBean getTransactionBatchTrailerBean()
/*    */   {
/* 23 */     return this.transactionBatchTrailerBean;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public List<TransactionAdviceBasicBean> getTransactionAdviceBasicType()
/*    */   {
/* 33 */     return this.transactionAdviceBasicType;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setTransactionBatchTrailerBean(TransactionBatchTrailerBean transactionBatchTrailerBean)
/*    */   {
/* 44 */     this.transactionBatchTrailerBean = transactionBatchTrailerBean;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setTransactionAdviceBasicType(List<TransactionAdviceBasicBean> transactionAdviceBasicType)
/*    */   {
/* 55 */     this.transactionAdviceBasicType = transactionAdviceBasicType;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\common\TransactionTBTSpecificBean.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */