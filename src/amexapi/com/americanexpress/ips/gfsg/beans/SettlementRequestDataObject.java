/*     */ package com.americanexpress.ips.gfsg.beans;
/*     */ 
/*     */ import com.americanexpress.ips.gfsg.beans.common.TransactionFileHeaderBean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.TransactionFileSummaryBean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.TransactionTBTSpecificBean;
/*     */ import java.util.List;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class SettlementRequestDataObject
/*     */ {
/*     */   private TransactionFileHeaderBean transactionFileHeaderType;
/*     */   private List<TransactionTBTSpecificBean> transactionTBTSpecificType;
/*     */   private TransactionFileSummaryBean transactionFileSummaryType;
/*     */   private String formatCode;
/*     */   private String transactionIdentifier;
/*     */   private String merchantId;
/*     */   
/*     */   public TransactionFileHeaderBean getTransactionFileHeaderType()
/*     */   {
/*  33 */     return this.transactionFileHeaderType;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public TransactionFileSummaryBean getTransactionFileSummaryType()
/*     */   {
/*  40 */     return this.transactionFileSummaryType;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getFormatCode()
/*     */   {
/*  46 */     return this.formatCode;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getTransactionIdentifier()
/*     */   {
/*  52 */     return this.transactionIdentifier;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getMerchantId()
/*     */   {
/*  58 */     return this.merchantId;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void setTransactionFileHeaderType(TransactionFileHeaderBean transactionFileHeaderType)
/*     */   {
/*  65 */     this.transactionFileHeaderType = transactionFileHeaderType;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTransactionFileSummaryType(TransactionFileSummaryBean transactionFileSummaryType)
/*     */   {
/*  73 */     this.transactionFileSummaryType = transactionFileSummaryType;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setFormatCode(String formatCode)
/*     */   {
/*  79 */     this.formatCode = formatCode;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setTransactionIdentifier(String transactionIdentifier)
/*     */   {
/*  85 */     this.transactionIdentifier = transactionIdentifier;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setMerchantId(String merchantId)
/*     */   {
/*  91 */     this.merchantId = merchantId;
/*     */   }
/*     */   
/*     */ 
/*     */   public List<TransactionTBTSpecificBean> getTransactionTBTSpecificType()
/*     */   {
/*  97 */     return this.transactionTBTSpecificType;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTransactionTBTSpecificType(List<TransactionTBTSpecificBean> transactionTBTSpecificType)
/*     */   {
/* 105 */     this.transactionTBTSpecificType = transactionTBTSpecificType;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\SettlementRequestDataObject.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */