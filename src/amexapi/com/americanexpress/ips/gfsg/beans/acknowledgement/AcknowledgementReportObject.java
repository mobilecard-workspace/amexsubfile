/*    */ package com.americanexpress.ips.gfsg.beans.acknowledgement;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class AcknowledgementReportObject
/*    */ {
/* 15 */   private String subFileDate = null;
/* 16 */   private String refNumber = null;
/* 17 */   private String seqNumber = null;
/* 18 */   private String fileTrackNumber = null;
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public String getSubFileDate()
/*    */   {
/* 26 */     return this.subFileDate;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setSubFileDate(String subFileDate)
/*    */   {
/* 34 */     this.subFileDate = subFileDate;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public String getRefNumber()
/*    */   {
/* 41 */     return this.refNumber;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setRefNumber(String refNumber)
/*    */   {
/* 49 */     this.refNumber = refNumber;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public String getSeqNumber()
/*    */   {
/* 56 */     return this.seqNumber;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setSeqNumber(String seqNumber)
/*    */   {
/* 64 */     this.seqNumber = seqNumber;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public String getFileTrackNumber()
/*    */   {
/* 71 */     return this.fileTrackNumber;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setFileTrackNumber(String fileTrackNumber)
/*    */   {
/* 79 */     this.fileTrackNumber = fileTrackNumber;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\acknowledgement\AcknowledgementReportObject.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */