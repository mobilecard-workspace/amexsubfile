/*     */ package com.americanexpress.ips.gfsg.beans.xml.datacapture.request;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="TAAAirIndusType", propOrder={"travTransTypeCd", "tktNbr", "docTypeCd", "iataCarrierCd", "iataAgcyNbr", "tktCarrierNm", "tktIssCityNm", "tktIssDt", "passCnt", "passNm", "conjTktInd", "origTransAmt", "origCurrCd", "elecTktInd", "airlineSegInfo", "exchTktNbr"})
/*     */ public class TAAAirIndusType
/*     */ {
/*     */   @XmlElement(name="TravTransTypeCd", required=true)
/*     */   protected TravTransTypeCdValueType travTransTypeCd;
/*     */   @XmlElement(name="TktNbr")
/*     */   protected String tktNbr;
/*     */   @XmlElement(name="DocTypeCd", required=true)
/*     */   protected String docTypeCd;
/*     */   @XmlElement(name="IATACarrierCd", required=true)
/*     */   protected String iataCarrierCd;
/*     */   @XmlElement(name="IATAAgcyNbr")
/*     */   protected String iataAgcyNbr;
/*     */   @XmlElement(name="TktCarrierNm")
/*     */   protected String tktCarrierNm;
/*     */   @XmlElement(name="TktIssCityNm")
/*     */   protected String tktIssCityNm;
/*     */   @XmlElement(name="TktIssDt", required=true)
/*     */   protected String tktIssDt;
/*     */   @XmlElement(name="PassCnt")
/*     */   protected String passCnt;
/*     */   @XmlElement(name="PassNm")
/*     */   protected String passNm;
/*     */   @XmlElement(name="ConjTktInd")
/*     */   protected String conjTktInd;
/*     */   @XmlElement(name="OrigTransAmt")
/*     */   protected String origTransAmt;
/*     */   @XmlElement(name="OrigCurrCd")
/*     */   protected String origCurrCd;
/*     */   @XmlElement(name="ElecTktInd")
/*     */   protected String elecTktInd;
/*     */   @XmlElement(name="AirlineSegInfo")
/*     */   protected List<AirlineSegInfoType> airlineSegInfo;
/*     */   @XmlElement(name="ExchTktNbr")
/*     */   protected String exchTktNbr;
/*     */   
/*     */   public TravTransTypeCdValueType getTravTransTypeCd()
/*     */   {
/* 141 */     return this.travTransTypeCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTravTransTypeCd(TravTransTypeCdValueType value)
/*     */   {
/* 153 */     this.travTransTypeCd = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTktNbr()
/*     */   {
/* 165 */     return this.tktNbr;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTktNbr(String value)
/*     */   {
/* 177 */     this.tktNbr = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getDocTypeCd()
/*     */   {
/* 189 */     return this.docTypeCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setDocTypeCd(String value)
/*     */   {
/* 201 */     this.docTypeCd = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getIATACarrierCd()
/*     */   {
/* 213 */     return this.iataCarrierCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setIATACarrierCd(String value)
/*     */   {
/* 225 */     this.iataCarrierCd = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getIATAAgcyNbr()
/*     */   {
/* 237 */     return this.iataAgcyNbr;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setIATAAgcyNbr(String value)
/*     */   {
/* 249 */     this.iataAgcyNbr = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTktCarrierNm()
/*     */   {
/* 261 */     return this.tktCarrierNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTktCarrierNm(String value)
/*     */   {
/* 273 */     this.tktCarrierNm = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTktIssCityNm()
/*     */   {
/* 285 */     return this.tktIssCityNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTktIssCityNm(String value)
/*     */   {
/* 297 */     this.tktIssCityNm = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTktIssDt()
/*     */   {
/* 309 */     return this.tktIssDt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTktIssDt(String value)
/*     */   {
/* 321 */     this.tktIssDt = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getPassCnt()
/*     */   {
/* 333 */     return this.passCnt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPassCnt(String value)
/*     */   {
/* 345 */     this.passCnt = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getPassNm()
/*     */   {
/* 357 */     return this.passNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPassNm(String value)
/*     */   {
/* 369 */     this.passNm = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getConjTktInd()
/*     */   {
/* 381 */     return this.conjTktInd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setConjTktInd(String value)
/*     */   {
/* 393 */     this.conjTktInd = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getOrigTransAmt()
/*     */   {
/* 405 */     return this.origTransAmt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setOrigTransAmt(String value)
/*     */   {
/* 417 */     this.origTransAmt = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getOrigCurrCd()
/*     */   {
/* 429 */     return this.origCurrCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setOrigCurrCd(String value)
/*     */   {
/* 441 */     this.origCurrCd = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getElecTktInd()
/*     */   {
/* 453 */     return this.elecTktInd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setElecTktInd(String value)
/*     */   {
/* 465 */     this.elecTktInd = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<AirlineSegInfoType> getAirlineSegInfo()
/*     */   {
/* 491 */     if (this.airlineSegInfo == null) {
/* 492 */       this.airlineSegInfo = new ArrayList();
/*     */     }
/* 494 */     return this.airlineSegInfo;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getExchTktNbr()
/*     */   {
/* 506 */     return this.exchTktNbr;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setExchTktNbr(String value)
/*     */   {
/* 518 */     this.exchTktNbr = value;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\xml\datacapture\request\TAAAirIndusType.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */