/*    */ package com.americanexpress.ips.gfsg.beans.xml.datacapture;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class ChargeItemInfoBean
/*    */ {
/*    */   private String chargeDesc;
/*    */   
/*    */ 
/*    */ 
/*    */   private String chargeItemQty;
/*    */   
/*    */ 
/*    */ 
/*    */   private String chargeItemAmt;
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public String getChargeDesc()
/*    */   {
/* 24 */     return this.chargeDesc;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setChargeDesc(String chargeDesc)
/*    */   {
/* 34 */     this.chargeDesc = chargeDesc;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public String getChargeItemQty()
/*    */   {
/* 44 */     return this.chargeItemQty;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setChargeItemQty(String chargeItemQty)
/*    */   {
/* 54 */     this.chargeItemQty = chargeItemQty;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public String getChargeItemAmt()
/*    */   {
/* 64 */     return this.chargeItemAmt;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setChargeItemAmt(String chargeItemAmt)
/*    */   {
/* 74 */     this.chargeItemAmt = chargeItemAmt;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\xml\datacapture\ChargeItemInfoBean.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */