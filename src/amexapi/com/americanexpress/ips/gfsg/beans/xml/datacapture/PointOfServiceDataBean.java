/*     */ package com.americanexpress.ips.gfsg.beans.xml.datacapture;
/*     */ 
/*     */ 
/*     */ public class PointOfServiceDataBean
/*     */ {
/*     */   private String cardDataInpCpblCd;
/*     */   
/*     */   private String cmAuthnCpblCd;
/*     */   
/*     */   private String cardCptrCpblCd;
/*     */   
/*     */   private String oprEnvirCd;
/*     */   
/*     */   private String cmPresentCd;
/*     */   
/*     */   private String cardPresentCd;
/*     */   
/*     */   private String cardDataInpModeCd;
/*     */   
/*     */   private String cmAuthnMthdCd;
/*     */   
/*     */   private String cmAuthnEnttyCd;
/*     */   
/*     */   private String cardDataOpCpblCd;
/*     */   private String trmnlOpCpblCd;
/*     */   private String pinCptrCpblCd;
/*     */   
/*     */   public String getCardDataInpCpblCd()
/*     */   {
/*  30 */     return this.cardDataInpCpblCd;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setCardDataInpCpblCd(String cardDataInpCpblCd)
/*     */   {
/*  36 */     this.cardDataInpCpblCd = cardDataInpCpblCd;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getCmAuthnCpblCd()
/*     */   {
/*  42 */     return this.cmAuthnCpblCd;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setCmAuthnCpblCd(String cmAuthnCpblCd)
/*     */   {
/*  48 */     this.cmAuthnCpblCd = cmAuthnCpblCd;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getCardCptrCpblCd()
/*     */   {
/*  54 */     return this.cardCptrCpblCd;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setCardCptrCpblCd(String cardCptrCpblCd)
/*     */   {
/*  60 */     this.cardCptrCpblCd = cardCptrCpblCd;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getOprEnvirCd()
/*     */   {
/*  66 */     return this.oprEnvirCd;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setOprEnvirCd(String oprEnvirCd)
/*     */   {
/*  72 */     this.oprEnvirCd = oprEnvirCd;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getCmPresentCd()
/*     */   {
/*  78 */     return this.cmPresentCd;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setCmPresentCd(String cmPresentCd)
/*     */   {
/*  84 */     this.cmPresentCd = cmPresentCd;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getCardPresentCd()
/*     */   {
/*  90 */     return this.cardPresentCd;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setCardPresentCd(String cardPresentCd)
/*     */   {
/*  96 */     this.cardPresentCd = cardPresentCd;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getCardDataInpModeCd()
/*     */   {
/* 102 */     return this.cardDataInpModeCd;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setCardDataInpModeCd(String cardDataInpModeCd)
/*     */   {
/* 108 */     this.cardDataInpModeCd = cardDataInpModeCd;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getCmAuthnMthdCd()
/*     */   {
/* 114 */     return this.cmAuthnMthdCd;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setCmAuthnMthdCd(String cmAuthnMthdCd)
/*     */   {
/* 120 */     this.cmAuthnMthdCd = cmAuthnMthdCd;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getCmAuthnEnttyCd()
/*     */   {
/* 126 */     return this.cmAuthnEnttyCd;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setCmAuthnEnttyCd(String cmAuthnEnttyCd)
/*     */   {
/* 132 */     this.cmAuthnEnttyCd = cmAuthnEnttyCd;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getCardDataOpCpblCd()
/*     */   {
/* 138 */     return this.cardDataOpCpblCd;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setCardDataOpCpblCd(String cardDataOpCpblCd)
/*     */   {
/* 144 */     this.cardDataOpCpblCd = cardDataOpCpblCd;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getTrmnlOpCpblCd()
/*     */   {
/* 150 */     return this.trmnlOpCpblCd;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setTrmnlOpCpblCd(String trmnlOpCpblCd)
/*     */   {
/* 156 */     this.trmnlOpCpblCd = trmnlOpCpblCd;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getPinCptrCpblCd()
/*     */   {
/* 162 */     return this.pinCptrCpblCd;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setPinCptrCpblCd(String pinCptrCpblCd)
/*     */   {
/* 168 */     this.pinCptrCpblCd = pinCptrCpblCd;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\xml\datacapture\PointOfServiceDataBean.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */