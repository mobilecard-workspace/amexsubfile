/*     */ package com.americanexpress.ips.gfsg.beans.xml.datacapture.request;
/*     */ 
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="TravelSegInfoType", propOrder={"dprtDt", "destCd", "dprtAirCd", "segIATACarrierCd", "flghtNbr", "servClassCd"})
/*     */ public class TravelSegInfoType
/*     */ {
/*     */   @XmlElement(name="DprtDt")
/*     */   protected String dprtDt;
/*     */   @XmlElement(name="DestCd")
/*     */   protected String destCd;
/*     */   @XmlElement(name="DprtAirCd")
/*     */   protected String dprtAirCd;
/*     */   @XmlElement(name="SegIATACarrierCd")
/*     */   protected String segIATACarrierCd;
/*     */   @XmlElement(name="FlghtNbr")
/*     */   protected String flghtNbr;
/*     */   @XmlElement(name="ServClassCd")
/*     */   protected String servClassCd;
/*     */   
/*     */   public String getDprtDt()
/*     */   {
/*  94 */     return this.dprtDt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setDprtDt(String value)
/*     */   {
/* 106 */     this.dprtDt = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getDestCd()
/*     */   {
/* 118 */     return this.destCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setDestCd(String value)
/*     */   {
/* 130 */     this.destCd = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getDprtAirCd()
/*     */   {
/* 142 */     return this.dprtAirCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setDprtAirCd(String value)
/*     */   {
/* 154 */     this.dprtAirCd = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getSegIATACarrierCd()
/*     */   {
/* 166 */     return this.segIATACarrierCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setSegIATACarrierCd(String value)
/*     */   {
/* 178 */     this.segIATACarrierCd = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getFlghtNbr()
/*     */   {
/* 190 */     return this.flghtNbr;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setFlghtNbr(String value)
/*     */   {
/* 202 */     this.flghtNbr = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getServClassCd()
/*     */   {
/* 214 */     return this.servClassCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setServClassCd(String value)
/*     */   {
/* 226 */     this.servClassCd = value;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\xml\datacapture\request\TravelSegInfoType.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */