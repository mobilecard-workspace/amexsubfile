/*     */ package com.americanexpress.ips.gfsg.beans.xml.datacapture.request;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="TAARailIndusType", propOrder={"travTransTypeCd", "tktNbr", "passNm", "iataCarrierCd", "tktIssNm", "tktIssCityNm", "railSegInfo"})
/*     */ public class TAARailIndusType
/*     */ {
/*     */   @XmlElement(name="TravTransTypeCd", required=true)
/*     */   protected TravTransTypeCdValueType travTransTypeCd;
/*     */   @XmlElement(name="TktNbr", required=true)
/*     */   protected String tktNbr;
/*     */   @XmlElement(name="PassNm", required=true)
/*     */   protected String passNm;
/*     */   @XmlElement(name="IATACarrierCd")
/*     */   protected String iataCarrierCd;
/*     */   @XmlElement(name="TktIssNm")
/*     */   protected String tktIssNm;
/*     */   @XmlElement(name="TktIssCityNm")
/*     */   protected String tktIssCityNm;
/*     */   @XmlElement(name="RailSegInfo")
/*     */   protected List<RailSegInfoType> railSegInfo;
/*     */   
/*     */   public TravTransTypeCdValueType getTravTransTypeCd()
/*     */   {
/*  97 */     return this.travTransTypeCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTravTransTypeCd(TravTransTypeCdValueType value)
/*     */   {
/* 109 */     this.travTransTypeCd = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTktNbr()
/*     */   {
/* 121 */     return this.tktNbr;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTktNbr(String value)
/*     */   {
/* 133 */     this.tktNbr = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getPassNm()
/*     */   {
/* 145 */     return this.passNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPassNm(String value)
/*     */   {
/* 157 */     this.passNm = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getIATACarrierCd()
/*     */   {
/* 169 */     return this.iataCarrierCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setIATACarrierCd(String value)
/*     */   {
/* 181 */     this.iataCarrierCd = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTktIssNm()
/*     */   {
/* 193 */     return this.tktIssNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTktIssNm(String value)
/*     */   {
/* 205 */     this.tktIssNm = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTktIssCityNm()
/*     */   {
/* 217 */     return this.tktIssCityNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTktIssCityNm(String value)
/*     */   {
/* 229 */     this.tktIssCityNm = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<RailSegInfoType> getRailSegInfo()
/*     */   {
/* 255 */     if (this.railSegInfo == null) {
/* 256 */       this.railSegInfo = new ArrayList();
/*     */     }
/* 258 */     return this.railSegInfo;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\xml\datacapture\request\TAARailIndusType.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */