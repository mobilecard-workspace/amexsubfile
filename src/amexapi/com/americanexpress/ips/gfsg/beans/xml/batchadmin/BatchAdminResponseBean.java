/*     */ package com.americanexpress.ips.gfsg.beans.xml.batchadmin;
/*     */ 
/*     */ import com.americanexpress.ips.gfsg.beans.ErrorObject;
/*     */ import java.util.List;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class BatchAdminResponseBean
/*     */ {
/*     */   private String version;
/*     */   private String merId;
/*     */   private String merTrmnlId;
/*     */   private String batchId;
/*     */   private String batchStatus;
/*     */   private String batchStatusText;
/*     */   private String submitterCode;
/*     */   private String openedDate;
/*     */   private String OpenedTm;
/*     */   private String transAmountCredit;
/*     */   private String transCountCredit;
/*     */   private String transAmountDebit;
/*     */   private String transAmountTotal;
/*     */   private String transCountDebit;
/*     */   private String transCountTotal;
/*     */   private String lastRefNumber;
/*     */   private String closedDt;
/*     */   private String closedTm;
/*     */   private String processedDt;
/*     */   private String processedTm;
/*     */   private String transDt;
/*     */   private String transTm;
/*     */   private String transCurrCd;
/*     */   private String requestxml;
/*     */   private String responsexml;
/*     */   private List<ErrorObject> actionCodeDescription;
/*     */   
/*     */   public String getVersion()
/*     */   {
/*  49 */     return this.version;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getMerId()
/*     */   {
/*  55 */     return this.merId;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getMerTrmnlId()
/*     */   {
/*  61 */     return this.merTrmnlId;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getBatchId()
/*     */   {
/*  67 */     return this.batchId;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getBatchStatus()
/*     */   {
/*  73 */     return this.batchStatus;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getBatchStatusText()
/*     */   {
/*  79 */     return this.batchStatusText;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getSubmitterCode()
/*     */   {
/*  85 */     return this.submitterCode;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setVersion(String version)
/*     */   {
/*  91 */     this.version = version;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setMerId(String merId)
/*     */   {
/*  97 */     this.merId = merId;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setMerTrmnlId(String merTrmnlId)
/*     */   {
/* 103 */     this.merTrmnlId = merTrmnlId;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setBatchId(String batchId)
/*     */   {
/* 109 */     this.batchId = batchId;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setBatchStatus(String batchStatus)
/*     */   {
/* 115 */     this.batchStatus = batchStatus;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setBatchStatusText(String batchStatusText)
/*     */   {
/* 121 */     this.batchStatusText = batchStatusText;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setSubmitterCode(String submitterCode)
/*     */   {
/* 127 */     this.submitterCode = submitterCode;
/*     */   }
/*     */   
/* 130 */   public List<ErrorObject> getActionCodeDescription() { return this.actionCodeDescription; }
/*     */   
/*     */   public void setActionCodeDescription(List<ErrorObject> actionCodeDescription) {
/* 133 */     this.actionCodeDescription = actionCodeDescription;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getRequestxml()
/*     */   {
/* 139 */     return this.requestxml;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setRequestxml(String requestxml)
/*     */   {
/* 145 */     this.requestxml = requestxml;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getResponsexml()
/*     */   {
/* 151 */     return this.responsexml;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setResponsexml(String responsexml)
/*     */   {
/* 157 */     this.responsexml = responsexml;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getOpenedDate()
/*     */   {
/* 163 */     return this.openedDate;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setOpenedDate(String openedDate)
/*     */   {
/* 169 */     this.openedDate = openedDate;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getOpenedTm()
/*     */   {
/* 175 */     return this.OpenedTm;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setOpenedTm(String openedTm)
/*     */   {
/* 181 */     this.OpenedTm = openedTm;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getTransAmountCredit()
/*     */   {
/* 187 */     return this.transAmountCredit;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setTransAmountCredit(String transAmountCredit)
/*     */   {
/* 193 */     this.transAmountCredit = transAmountCredit;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getTransCountCredit()
/*     */   {
/* 199 */     return this.transCountCredit;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setTransCountCredit(String transCountCredit)
/*     */   {
/* 205 */     this.transCountCredit = transCountCredit;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getTransAmountDebit()
/*     */   {
/* 211 */     return this.transAmountDebit;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setTransAmountDebit(String transAmountDebit)
/*     */   {
/* 217 */     this.transAmountDebit = transAmountDebit;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getTransAmountTotal()
/*     */   {
/* 223 */     return this.transAmountTotal;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setTransAmountTotal(String transAmountTotal)
/*     */   {
/* 229 */     this.transAmountTotal = transAmountTotal;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getTransCountDebit()
/*     */   {
/* 235 */     return this.transCountDebit;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setTransCountDebit(String transCountDebit)
/*     */   {
/* 241 */     this.transCountDebit = transCountDebit;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getTransCountTotal()
/*     */   {
/* 247 */     return this.transCountTotal;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setTransCountTotal(String transCountTotal)
/*     */   {
/* 253 */     this.transCountTotal = transCountTotal;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getLastRefNumber()
/*     */   {
/* 259 */     return this.lastRefNumber;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setLastRefNumber(String lastRefNumber)
/*     */   {
/* 265 */     this.lastRefNumber = lastRefNumber;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getClosedDt()
/*     */   {
/* 271 */     return this.closedDt;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setClosedDt(String closedDt)
/*     */   {
/* 277 */     this.closedDt = closedDt;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getClosedTm()
/*     */   {
/* 283 */     return this.closedTm;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setClosedTm(String closedTm)
/*     */   {
/* 289 */     this.closedTm = closedTm;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getProcessedDt()
/*     */   {
/* 295 */     return this.processedDt;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setProcessedDt(String processedDt)
/*     */   {
/* 301 */     this.processedDt = processedDt;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getProcessedTm()
/*     */   {
/* 307 */     return this.processedTm;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setProcessedTm(String processedTm)
/*     */   {
/* 313 */     this.processedTm = processedTm;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getTransDt()
/*     */   {
/* 319 */     return this.transDt;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setTransDt(String transDt)
/*     */   {
/* 325 */     this.transDt = transDt;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getTransTm()
/*     */   {
/* 331 */     return this.transTm;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setTransTm(String transTm)
/*     */   {
/* 337 */     this.transTm = transTm;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getTransCurrCd()
/*     */   {
/* 343 */     return this.transCurrCd;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setTransCurrCd(String transCurrCd)
/*     */   {
/* 349 */     this.transCurrCd = transCurrCd;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\xml\batchadmin\BatchAdminResponseBean.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */