/*     */ package com.americanexpress.ips.gfsg.beans.xml.datacapture;
/*     */ 
/*     */ import java.util.List;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class TAAAirIndustryBean
/*     */ {
/*     */   private String travTransTypeCd;
/*     */   private String tktNbr;
/*     */   private String docTypeCd;
/*     */   private String iATACarrierCd;
/*     */   private String iATAAgcyNbr;
/*     */   private String tktCarrierNm;
/*     */   private String tktIssCityNm;
/*     */   private String tktIssDt;
/*     */   private String passCnt;
/*     */   private String passNm;
/*     */   private String conjTktInd;
/*     */   private String elecTktInd;
/*     */   private String exchTktNbr;
/*     */   private List<AirlineSegInfoBean> airlineSegInfo;
/*     */   private String origTransAmt;
/*     */   private String origCurrCd;
/*     */   
/*     */   public String getTravTransTypeCd()
/*     */   {
/*  40 */     return this.travTransTypeCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTravTransTypeCd(String travTransTypeCd)
/*     */   {
/*  52 */     this.travTransTypeCd = travTransTypeCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTktNbr()
/*     */   {
/*  64 */     return this.tktNbr;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTktNbr(String tktNbr)
/*     */   {
/*  76 */     this.tktNbr = tktNbr;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getDocTypeCd()
/*     */   {
/*  88 */     return this.docTypeCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setDocTypeCd(String docTypeCd)
/*     */   {
/* 100 */     this.docTypeCd = docTypeCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getIATACarrierCd()
/*     */   {
/* 112 */     return this.iATACarrierCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setIATACarrierCd(String carrierCd)
/*     */   {
/* 124 */     this.iATACarrierCd = carrierCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getIATAAgcyNbr()
/*     */   {
/* 136 */     return this.iATAAgcyNbr;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setIATAAgcyNbr(String agcyNbr)
/*     */   {
/* 148 */     this.iATAAgcyNbr = agcyNbr;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTktCarrierNm()
/*     */   {
/* 160 */     return this.tktCarrierNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTktCarrierNm(String tktCarrierNm)
/*     */   {
/* 172 */     this.tktCarrierNm = tktCarrierNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTktIssCityNm()
/*     */   {
/* 184 */     return this.tktIssCityNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTktIssCityNm(String tktIssCityNm)
/*     */   {
/* 196 */     this.tktIssCityNm = tktIssCityNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTktIssDt()
/*     */   {
/* 208 */     return this.tktIssDt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTktIssDt(String tktIssDt)
/*     */   {
/* 220 */     this.tktIssDt = tktIssDt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getPassCnt()
/*     */   {
/* 232 */     return this.passCnt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPassCnt(String passCnt)
/*     */   {
/* 244 */     this.passCnt = passCnt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getPassNm()
/*     */   {
/* 256 */     return this.passNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPassNm(String passNm)
/*     */   {
/* 268 */     this.passNm = passNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getConjTktInd()
/*     */   {
/* 280 */     return this.conjTktInd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setConjTktInd(String conjTktInd)
/*     */   {
/* 292 */     this.conjTktInd = conjTktInd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getElecTktInd()
/*     */   {
/* 304 */     return this.elecTktInd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setElecTktInd(String elecTktInd)
/*     */   {
/* 316 */     this.elecTktInd = elecTktInd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getExchTktNbr()
/*     */   {
/* 328 */     return this.exchTktNbr;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setExchTktNbr(String exchTktNbr)
/*     */   {
/* 340 */     this.exchTktNbr = exchTktNbr;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<AirlineSegInfoBean> getAirlineSegInfo()
/*     */   {
/* 352 */     return this.airlineSegInfo;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setAirlineSegInfo(List<AirlineSegInfoBean> airlineSegInfo)
/*     */   {
/* 364 */     this.airlineSegInfo = airlineSegInfo;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getOrigTransAmt()
/*     */   {
/* 381 */     return this.origTransAmt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setOrigTransAmt(String origTransAmt)
/*     */   {
/* 398 */     this.origTransAmt = origTransAmt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getOrigCurrCd()
/*     */   {
/* 414 */     return this.origCurrCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setOrigCurrCd(String origCurrCd)
/*     */   {
/* 430 */     this.origCurrCd = origCurrCd;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\xml\datacapture\TAAAirIndustryBean.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */