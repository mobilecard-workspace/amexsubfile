/*     */ package com.americanexpress.ips.gfsg.beans.xml.datacapture;
/*     */ 
/*     */ import com.americanexpress.ips.gfsg.beans.ErrorObject;
/*     */ import java.util.List;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class DataCaptureResponseBean
/*     */ {
/*     */   private String version;
/*     */   private String merId;
/*     */   private String merTrmnlId;
/*     */   private String batchId;
/*     */   private String batchStatus;
/*     */   private String batchStatusText;
/*     */   private String submitterCode;
/*     */   private String refNumber;
/*     */   private String requestXML;
/*     */   private String responseXML;
/*     */   private List<ErrorObject> actionCodeDescription;
/*     */   
/*     */   public String getVersion()
/*     */   {
/*  33 */     return this.version;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getMerId()
/*     */   {
/*  39 */     return this.merId;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getMerTrmnlId()
/*     */   {
/*  45 */     return this.merTrmnlId;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getBatchId()
/*     */   {
/*  51 */     return this.batchId;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getBatchStatus()
/*     */   {
/*  57 */     return this.batchStatus;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getBatchStatusText()
/*     */   {
/*  63 */     return this.batchStatusText;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getSubmitterCode()
/*     */   {
/*  69 */     return this.submitterCode;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getRefNumber()
/*     */   {
/*  75 */     return this.refNumber;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setVersion(String version)
/*     */   {
/*  81 */     this.version = version;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setMerId(String merId)
/*     */   {
/*  87 */     this.merId = merId;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setMerTrmnlId(String merTrmnlId)
/*     */   {
/*  93 */     this.merTrmnlId = merTrmnlId;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setBatchId(String batchId)
/*     */   {
/*  99 */     this.batchId = batchId;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setBatchStatus(String batchStatus)
/*     */   {
/* 105 */     this.batchStatus = batchStatus;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setBatchStatusText(String batchStatusText)
/*     */   {
/* 111 */     this.batchStatusText = batchStatusText;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setSubmitterCode(String submitterCode)
/*     */   {
/* 117 */     this.submitterCode = submitterCode;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setRefNumber(String refNumber)
/*     */   {
/* 123 */     this.refNumber = refNumber;
/*     */   }
/*     */   
/* 126 */   public List<ErrorObject> getActionCodeDescription() { return this.actionCodeDescription; }
/*     */   
/*     */   public void setActionCodeDescription(List<ErrorObject> actionCodeDescription) {
/* 129 */     this.actionCodeDescription = actionCodeDescription;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getRequestXML()
/*     */   {
/* 135 */     return this.requestXML;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setRequestXML(String requestXML)
/*     */   {
/* 141 */     this.requestXML = requestXML;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getResponseXML()
/*     */   {
/* 147 */     return this.responseXML;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setResponseXML(String responseXML)
/*     */   {
/* 153 */     this.responseXML = responseXML;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\xml\datacapture\DataCaptureResponseBean.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */