/*     */ package com.americanexpress.ips.gfsg.beans.xml.datacapture;
/*     */ 
/*     */ 
/*     */ public class TAAAutoRentalIndustryBean
/*     */ {
/*     */   private String agrNbr;
/*     */   
/*     */   private String pickupLocNm;
/*     */   
/*     */   private String pickupCityNm;
/*     */   
/*     */   private String pickupRgnCd;
/*     */   
/*     */   private String pickupCtryCd;
/*     */   
/*     */   private String pickupDt;
/*     */   
/*     */   private String pickupTm;
/*     */   private String rtrnCityNm;
/*     */   private String rtrnRgnCd;
/*     */   private String rtrnCtryCd;
/*     */   private String rtrnDt;
/*     */   private String rtrnTm;
/*     */   private String renterNm;
/*     */   private String vehClassId;
/*     */   private String travDisCnt;
/*     */   private String travDisUnit;
/*     */   private String auditAdjInd;
/*     */   private String auditAdjAmt;
/*     */   private String rtrnLocNm;
/*     */   private String vehIdNbr;
/*     */   private String drvIdNbr;
/*     */   private String drvTaxIdNbr;
/*     */   
/*     */   public String getRtrnLocNm()
/*     */   {
/*  37 */     return this.rtrnLocNm;
/*     */   }
/*     */   
/*  40 */   public void setRtrnLocNm(String rtrnLocNm) { this.rtrnLocNm = rtrnLocNm; }
/*     */   
/*     */   public String getVehIdNbr() {
/*  43 */     return this.vehIdNbr;
/*     */   }
/*     */   
/*  46 */   public void setVehIdNbr(String vehIdNbr) { this.vehIdNbr = vehIdNbr; }
/*     */   
/*     */   public String getDrvIdNbr() {
/*  49 */     return this.drvIdNbr;
/*     */   }
/*     */   
/*  52 */   public void setDrvIdNbr(String drvIdNbr) { this.drvIdNbr = drvIdNbr; }
/*     */   
/*     */   public String getDrvTaxIdNbr() {
/*  55 */     return this.drvTaxIdNbr;
/*     */   }
/*     */   
/*  58 */   public void setDrvTaxIdNbr(String drvTaxIdNbr) { this.drvTaxIdNbr = drvTaxIdNbr; }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getAgrNbr()
/*     */   {
/*  68 */     return this.agrNbr;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setAgrNbr(String agrNbr)
/*     */   {
/*  78 */     this.agrNbr = agrNbr;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getPickupLocNm()
/*     */   {
/*  88 */     return this.pickupLocNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPickupLocNm(String pickupLocNm)
/*     */   {
/*  98 */     this.pickupLocNm = pickupLocNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getPickupCityNm()
/*     */   {
/* 108 */     return this.pickupCityNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPickupCityNm(String pickupCityNm)
/*     */   {
/* 118 */     this.pickupCityNm = pickupCityNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getPickupRgnCd()
/*     */   {
/* 128 */     return this.pickupRgnCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPickupRgnCd(String pickupRgnCd)
/*     */   {
/* 138 */     this.pickupRgnCd = pickupRgnCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getPickupCtryCd()
/*     */   {
/* 148 */     return this.pickupCtryCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPickupCtryCd(String pickupCtryCd)
/*     */   {
/* 158 */     this.pickupCtryCd = pickupCtryCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getPickupDt()
/*     */   {
/* 168 */     return this.pickupDt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPickupDt(String pickupDt)
/*     */   {
/* 178 */     this.pickupDt = pickupDt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getPickupTm()
/*     */   {
/* 188 */     return this.pickupTm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPickupTm(String pickupTm)
/*     */   {
/* 198 */     this.pickupTm = pickupTm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getRtrnCityNm()
/*     */   {
/* 208 */     return this.rtrnCityNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setRtrnCityNm(String rtrnCityNm)
/*     */   {
/* 218 */     this.rtrnCityNm = rtrnCityNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getRtrnRgnCd()
/*     */   {
/* 228 */     return this.rtrnRgnCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setRtrnRgnCd(String rtrnRgnCd)
/*     */   {
/* 238 */     this.rtrnRgnCd = rtrnRgnCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getRtrnCtryCd()
/*     */   {
/* 248 */     return this.rtrnCtryCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setRtrnCtryCd(String rtrnCtryCd)
/*     */   {
/* 258 */     this.rtrnCtryCd = rtrnCtryCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getRtrnDt()
/*     */   {
/* 268 */     return this.rtrnDt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setRtrnDt(String rtrnDt)
/*     */   {
/* 278 */     this.rtrnDt = rtrnDt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getRtrnTm()
/*     */   {
/* 288 */     return this.rtrnTm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setRtrnTm(String rtrnTm)
/*     */   {
/* 298 */     this.rtrnTm = rtrnTm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getRenterNm()
/*     */   {
/* 308 */     return this.renterNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setRenterNm(String renterNm)
/*     */   {
/* 318 */     this.renterNm = renterNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getVehClassId()
/*     */   {
/* 328 */     return this.vehClassId;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setVehClassId(String vehClassId)
/*     */   {
/* 338 */     this.vehClassId = vehClassId;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTravDisCnt()
/*     */   {
/* 348 */     return this.travDisCnt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTravDisCnt(String travDisCnt)
/*     */   {
/* 358 */     this.travDisCnt = travDisCnt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTravDisUnit()
/*     */   {
/* 368 */     return this.travDisUnit;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTravDisUnit(String travDisUnit)
/*     */   {
/* 378 */     this.travDisUnit = travDisUnit;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getAuditAdjInd()
/*     */   {
/* 388 */     return this.auditAdjInd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setAuditAdjInd(String auditAdjInd)
/*     */   {
/* 398 */     this.auditAdjInd = auditAdjInd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getAuditAdjAmt()
/*     */   {
/* 408 */     return this.auditAdjAmt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setAuditAdjAmt(String auditAdjAmt)
/*     */   {
/* 418 */     this.auditAdjAmt = auditAdjAmt;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\xml\datacapture\TAAAutoRentalIndustryBean.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */