/*     */ package com.americanexpress.ips.gfsg.beans.xml.batchadmin;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class CardAcceptorDetail
/*     */ {
/*     */   private String cardAcptNm;
/*     */   
/*     */ 
/*     */ 
/*     */   private String cardAcptStreetNm;
/*     */   
/*     */ 
/*     */   private String cardAcptCityNm;
/*     */   
/*     */ 
/*     */   private String cardAcptRegCd;
/*     */   
/*     */ 
/*     */   private String cardAcptCtryCd;
/*     */   
/*     */ 
/*     */   private String cardAcptPostCd;
/*     */   
/*     */ 
/*     */ 
/*     */   public String getCardAcptNm()
/*     */   {
/*  30 */     return this.cardAcptNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCardAcptNm(String cardAcptNm)
/*     */   {
/*  42 */     this.cardAcptNm = cardAcptNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCardAcptStreetNm()
/*     */   {
/*  54 */     return this.cardAcptStreetNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCardAcptStreetNm(String cardAcptStreetNm)
/*     */   {
/*  66 */     this.cardAcptStreetNm = cardAcptStreetNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCardAcptCityNm()
/*     */   {
/*  78 */     return this.cardAcptCityNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCardAcptCityNm(String cardAcptCityNm)
/*     */   {
/*  90 */     this.cardAcptCityNm = cardAcptCityNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCardAcptRegCd()
/*     */   {
/* 104 */     return this.cardAcptRegCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCardAcptRegCd(String cardAcptRegCd)
/*     */   {
/* 118 */     this.cardAcptRegCd = cardAcptRegCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCardAcptCtryCd()
/*     */   {
/* 130 */     return this.cardAcptCtryCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCardAcptCtryCd(String cardAcptCtryCd)
/*     */   {
/* 142 */     this.cardAcptCtryCd = cardAcptCtryCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCardAcptPostCd()
/*     */   {
/* 154 */     return this.cardAcptPostCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCardAcptPostCd(String cardAcptPostCd)
/*     */   {
/* 166 */     this.cardAcptPostCd = cardAcptPostCd;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\xml\batchadmin\CardAcceptorDetail.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */