/*     */ package com.americanexpress.ips.gfsg.beans.xml.datacapture.request;
/*     */ 
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="ChrgItemInfoType", propOrder={"chrgDesc", "chrgItemQty", "chrgItemAmt"})
/*     */ public class ChrgItemInfoType
/*     */ {
/*     */   @XmlElement(name="ChrgDesc")
/*     */   protected String chrgDesc;
/*     */   @XmlElement(name="ChrgItemQty")
/*     */   protected String chrgItemQty;
/*     */   @XmlElement(name="ChrgItemAmt")
/*     */   protected String chrgItemAmt;
/*     */   
/*     */   public String getChrgDesc()
/*     */   {
/*  72 */     return this.chrgDesc;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setChrgDesc(String value)
/*     */   {
/*  84 */     this.chrgDesc = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getChrgItemQty()
/*     */   {
/*  96 */     return this.chrgItemQty;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setChrgItemQty(String value)
/*     */   {
/* 108 */     this.chrgItemQty = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getChrgItemAmt()
/*     */   {
/* 120 */     return this.chrgItemAmt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setChrgItemAmt(String value)
/*     */   {
/* 132 */     this.chrgItemAmt = value;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\xml\datacapture\request\ChrgItemInfoType.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */