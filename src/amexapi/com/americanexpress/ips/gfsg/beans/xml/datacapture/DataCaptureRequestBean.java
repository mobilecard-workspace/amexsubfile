/*      */ package com.americanexpress.ips.gfsg.beans.xml.datacapture;
/*      */ 
/*      */ import java.util.List;
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ public class DataCaptureRequestBean
/*      */ {
/*   17 */   private String version = "12010000";
/*      */   
/*      */   private String merId;
/*      */   
/*      */   private String merTrmnlId;
/*      */   
/*      */   private String batchId;
/*      */   
/*      */   private String refNumber;
/*      */   
/*      */   private String cardNbr;
/*      */   
/*      */   private String cardExprDt;
/*      */   
/*      */   private String transDt;
/*      */   private String transTm;
/*      */   private String transAmt;
/*      */   private String transCurrCd;
/*      */   private String transProcCd;
/*      */   private String transId;
/*      */   private String transAprvCd;
/*      */   private String elecComrceInd;
/*      */   private String mediaCd;
/*      */   private String submMthdCd;
/*      */   private String merLocId;
/*      */   private String merCtcInfoTxt;
/*      */   private PointOfServiceDataBean pointOfServiceData;
/*      */   private String transImageSeqNbr;
/*      */   private String matchKeyTypeCd;
/*      */   private String matchKeyId;
/*      */   private String defPaymentPlan;
/*      */   private String numDefPayments;
/*      */   private String emvData;
/*      */   private String merCtgyCd;
/*      */   private String sellId;
/*      */   private List<CardTransactionDetailBean> cardTransDetail;
/*      */   private String transAddCd;
/*      */   private TAAAirIndustryBean taaAirIndustry;
/*      */   private TAAAutoRentalIndustryBean taaAutoRentalIndustry;
/*      */   private TAACommunServIndustryBean taaCommunServIndustry;
/*      */   private TAAEntertainmentTktIndustryBean taaEntertainmentTktIndustry;
/*      */   private TAAInsuranceIndustryBean taaInsuranceIndustry;
/*      */   private TAALodgingIndustryBean taaLodgingIndustry;
/*      */   private TAARailIndustryBean taaRailIndustry;
/*      */   private TAARetailIndustryBean taaRetailIndustry;
/*      */   private TAATravCruiseIndustryBean taaTravCruiseIndustry;
/*      */   private TAACorpPurchSolLvl2Bean taaCorpPurchSolLvl2;
/*      */   private InvoiceMassageInfoBean invoiceMassageInfoBean;
/*      */   private String batchImageSeqNbr;
/*      */   private String servAgtMerId;
/*      */   
/*      */   public String getVersion()
/*      */   {
/*   70 */     return this.version;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setVersion(String version)
/*      */   {
/*   81 */     this.version = version;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getMerId()
/*      */   {
/*   92 */     return this.merId;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setMerId(String merId)
/*      */   {
/*  104 */     this.merId = merId;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getMerTrmnlId()
/*      */   {
/*  116 */     return this.merTrmnlId;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setMerTrmnlId(String merTrmnlId)
/*      */   {
/*  128 */     this.merTrmnlId = merTrmnlId;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getBatchId()
/*      */   {
/*  141 */     return this.batchId;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setBatchId(String batchId)
/*      */   {
/*  154 */     this.batchId = batchId;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getRefNumber()
/*      */   {
/*  167 */     return this.refNumber;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setRefNumber(String refNumber)
/*      */   {
/*  180 */     this.refNumber = refNumber;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getCardNbr()
/*      */   {
/*  191 */     return this.cardNbr;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setCardNbr(String cardNbr)
/*      */   {
/*  203 */     this.cardNbr = cardNbr;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getCardExprDt()
/*      */   {
/*  214 */     return this.cardExprDt;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setCardExprDt(String cardExprDt)
/*      */   {
/*  226 */     this.cardExprDt = cardExprDt;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTransDt()
/*      */   {
/*  237 */     return this.transDt;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTransDt(String transDt)
/*      */   {
/*  248 */     this.transDt = transDt;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTransTm()
/*      */   {
/*  259 */     return this.transTm;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTransTm(String transTm)
/*      */   {
/*  271 */     this.transTm = transTm;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTransAmt()
/*      */   {
/*  283 */     return this.transAmt;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTransAmt(String transAmt)
/*      */   {
/*  295 */     this.transAmt = transAmt;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTransCurrCd()
/*      */   {
/*  310 */     return this.transCurrCd;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTransCurrCd(String transCurrCd)
/*      */   {
/*  325 */     this.transCurrCd = transCurrCd;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTransProcCd()
/*      */   {
/*  337 */     return this.transProcCd;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTransProcCd(String transProcCd)
/*      */   {
/*  349 */     this.transProcCd = transProcCd;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTransId()
/*      */   {
/*  362 */     return this.transId;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTransId(String transId)
/*      */   {
/*  375 */     this.transId = transId;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTransAprvCd()
/*      */   {
/*  387 */     return this.transAprvCd;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTransAprvCd(String transAprvCd)
/*      */   {
/*  399 */     this.transAprvCd = transAprvCd;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getElecComrceInd()
/*      */   {
/*  410 */     return this.elecComrceInd;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setElecComrceInd(String elecComrceInd)
/*      */   {
/*  421 */     this.elecComrceInd = elecComrceInd;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getMediaCd()
/*      */   {
/*  432 */     return this.mediaCd;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setMediaCd(String mediaCd)
/*      */   {
/*  443 */     this.mediaCd = mediaCd;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getSubmMthdCd()
/*      */   {
/*  454 */     return this.submMthdCd;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSubmMthdCd(String submMthdCd)
/*      */   {
/*  465 */     this.submMthdCd = submMthdCd;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getMerLocId()
/*      */   {
/*  476 */     return this.merLocId;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setMerLocId(String merLocId)
/*      */   {
/*  487 */     this.merLocId = merLocId;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getMerCtcInfoTxt()
/*      */   {
/*  498 */     return this.merCtcInfoTxt;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setMerCtcInfoTxt(String merCtcInfoTxt)
/*      */   {
/*  509 */     this.merCtcInfoTxt = merCtcInfoTxt;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public PointOfServiceDataBean getPointOfServiceData()
/*      */   {
/*  520 */     return this.pointOfServiceData;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setPointOfServiceData(PointOfServiceDataBean pointOfServiceData)
/*      */   {
/*  531 */     this.pointOfServiceData = pointOfServiceData;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTransImageSeqNbr()
/*      */   {
/*  542 */     return this.transImageSeqNbr;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTransImageSeqNbr(String transImageSeqNbr)
/*      */   {
/*  553 */     this.transImageSeqNbr = transImageSeqNbr;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getMatchKeyTypeCd()
/*      */   {
/*  566 */     return this.matchKeyTypeCd;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setMatchKeyTypeCd(String matchKeyTypeCd)
/*      */   {
/*  579 */     this.matchKeyTypeCd = matchKeyTypeCd;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getMatchKeyId()
/*      */   {
/*  593 */     return this.matchKeyId;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setMatchKeyId(String matchKeyId)
/*      */   {
/*  607 */     this.matchKeyId = matchKeyId;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getDefPaymentPlan()
/*      */   {
/*  621 */     return this.defPaymentPlan;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setDefPaymentPlan(String defPaymentPlan)
/*      */   {
/*  635 */     this.defPaymentPlan = defPaymentPlan;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getNumDefPayments()
/*      */   {
/*  648 */     return this.numDefPayments;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setNumDefPayments(String numDefPayments)
/*      */   {
/*  661 */     this.numDefPayments = numDefPayments;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getEmvData()
/*      */   {
/*  675 */     return this.emvData;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setEmvData(String emvData)
/*      */   {
/*  690 */     this.emvData = emvData;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getMerCtgyCd()
/*      */   {
/*  704 */     return this.merCtgyCd;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setMerCtgyCd(String merCtgyCd)
/*      */   {
/*  718 */     this.merCtgyCd = merCtgyCd;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getSellId()
/*      */   {
/*  732 */     return this.sellId;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSellId(String sellId)
/*      */   {
/*  746 */     this.sellId = sellId;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public List<CardTransactionDetailBean> getCardTransDetail()
/*      */   {
/*  757 */     return this.cardTransDetail;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setCardTransDetail(List<CardTransactionDetailBean> cardTransDetail)
/*      */   {
/*  768 */     this.cardTransDetail = cardTransDetail;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTransAddCd()
/*      */   {
/*  785 */     return this.transAddCd;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTransAddCd(String transAddCd)
/*      */   {
/*  802 */     this.transAddCd = transAddCd;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public TAAAirIndustryBean getTaaAirIndustry()
/*      */   {
/*  815 */     return this.taaAirIndustry;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTaaAirIndustry(TAAAirIndustryBean taaAirIndustry)
/*      */   {
/*  828 */     this.taaAirIndustry = taaAirIndustry;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public TAAAutoRentalIndustryBean getTaaAutoRentalIndustry()
/*      */   {
/*  841 */     return this.taaAutoRentalIndustry;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTaaAutoRentalIndustry(TAAAutoRentalIndustryBean taaAutoRentalIndustry)
/*      */   {
/*  855 */     this.taaAutoRentalIndustry = taaAutoRentalIndustry;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public TAACommunServIndustryBean getTaaCommunServIndustry()
/*      */   {
/*  868 */     return this.taaCommunServIndustry;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTaaCommunServIndustry(TAACommunServIndustryBean taaCommunServIndustry)
/*      */   {
/*  882 */     this.taaCommunServIndustry = taaCommunServIndustry;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public TAAEntertainmentTktIndustryBean getTaaEntertainmentTktIndustry()
/*      */   {
/*  895 */     return this.taaEntertainmentTktIndustry;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTaaEntertainmentTktIndustry(TAAEntertainmentTktIndustryBean taaEntertainmentTktIndustry)
/*      */   {
/*  909 */     this.taaEntertainmentTktIndustry = taaEntertainmentTktIndustry;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public TAAInsuranceIndustryBean getTaaInsuranceIndustry()
/*      */   {
/*  922 */     return this.taaInsuranceIndustry;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTaaInsuranceIndustry(TAAInsuranceIndustryBean taaInsuranceIndustry)
/*      */   {
/*  936 */     this.taaInsuranceIndustry = taaInsuranceIndustry;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public TAALodgingIndustryBean getTaaLodgingIndustry()
/*      */   {
/*  949 */     return this.taaLodgingIndustry;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTaaLodgingIndustry(TAALodgingIndustryBean taaLodgingIndustry)
/*      */   {
/*  962 */     this.taaLodgingIndustry = taaLodgingIndustry;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public TAARailIndustryBean getTaaRailIndustry()
/*      */   {
/*  975 */     return this.taaRailIndustry;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTaaRailIndustry(TAARailIndustryBean taaRailIndustry)
/*      */   {
/*  988 */     this.taaRailIndustry = taaRailIndustry;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public TAARetailIndustryBean getTaaRetailIndustry()
/*      */   {
/* 1001 */     return this.taaRetailIndustry;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTaaRetailIndustry(TAARetailIndustryBean taaRetailIndustry)
/*      */   {
/* 1014 */     this.taaRetailIndustry = taaRetailIndustry;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public TAATravCruiseIndustryBean getTaaTravCruiseIndustry()
/*      */   {
/* 1027 */     return this.taaTravCruiseIndustry;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTaaTravCruiseIndustry(TAATravCruiseIndustryBean taaTravCruiseIndustry)
/*      */   {
/* 1041 */     this.taaTravCruiseIndustry = taaTravCruiseIndustry;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public TAACorpPurchSolLvl2Bean getTaaCorpPurchSolLvl2()
/*      */   {
/* 1051 */     return this.taaCorpPurchSolLvl2;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTaaCorpPurchSolLvl2(TAACorpPurchSolLvl2Bean taaCorpPurchSolLvl2)
/*      */   {
/* 1064 */     this.taaCorpPurchSolLvl2 = taaCorpPurchSolLvl2;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public InvoiceMassageInfoBean getInvoiceMassageInfoBean()
/*      */   {
/* 1075 */     return this.invoiceMassageInfoBean;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setInvoiceMassageInfoBean(InvoiceMassageInfoBean invoiceMassageInfoBean)
/*      */   {
/* 1087 */     this.invoiceMassageInfoBean = invoiceMassageInfoBean;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getBatchImageSeqNbr()
/*      */   {
/* 1098 */     return this.batchImageSeqNbr;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setBatchImageSeqNbr(String batchImageSeqNbr)
/*      */   {
/* 1109 */     this.batchImageSeqNbr = batchImageSeqNbr;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getServAgtMerId()
/*      */   {
/* 1120 */     return this.servAgtMerId;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setServAgtMerId(String servAgtMerId)
/*      */   {
/* 1131 */     this.servAgtMerId = servAgtMerId;
/*      */   }
/*      */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\xml\datacapture\DataCaptureRequestBean.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */