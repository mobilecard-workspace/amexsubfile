/*     */ package com.americanexpress.ips.gfsg.beans.xml.datacapture.request;
/*     */ 
/*     */ import java.math.BigInteger;
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="TAACorpPurchSolLvl2Type", propOrder={"reqNm", "chrgItemInfo", "cmRefNbr", "shipToPostCd", "taxTotAmt", "taxTypeCd"})
/*     */ public class TAACorpPurchSolLvl2Type
/*     */ {
/*     */   @XmlElement(name="ReqNm")
/*     */   protected String reqNm;
/*     */   @XmlElement(name="ChrgItemInfo")
/*     */   protected List<ChrgItemInfoType> chrgItemInfo;
/*     */   @XmlElement(name="CMRefNbr")
/*     */   protected String cmRefNbr;
/*     */   @XmlElement(name="ShipToPostCd", required=true)
/*     */   protected String shipToPostCd;
/*     */   @XmlElement(name="TaxTotAmt", required=true)
/*     */   protected BigInteger taxTotAmt;
/*     */   @XmlElement(name="TaxTypeCd", required=true)
/*     */   protected String taxTypeCd;
/*     */   
/*     */   public String getReqNm()
/*     */   {
/*  83 */     return this.reqNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setReqNm(String value)
/*     */   {
/*  95 */     this.reqNm = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<ChrgItemInfoType> getChrgItemInfo()
/*     */   {
/* 121 */     if (this.chrgItemInfo == null) {
/* 122 */       this.chrgItemInfo = new ArrayList();
/*     */     }
/* 124 */     return this.chrgItemInfo;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCMRefNbr()
/*     */   {
/* 136 */     return this.cmRefNbr;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCMRefNbr(String value)
/*     */   {
/* 148 */     this.cmRefNbr = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getShipToPostCd()
/*     */   {
/* 160 */     return this.shipToPostCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setShipToPostCd(String value)
/*     */   {
/* 172 */     this.shipToPostCd = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public BigInteger getTaxTotAmt()
/*     */   {
/* 184 */     return this.taxTotAmt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTaxTotAmt(BigInteger value)
/*     */   {
/* 196 */     this.taxTotAmt = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTaxTypeCd()
/*     */   {
/* 208 */     return this.taxTypeCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTaxTypeCd(String value)
/*     */   {
/* 220 */     this.taxTypeCd = value;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\xml\datacapture\request\TAACorpPurchSolLvl2Type.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */