/*     */ package com.americanexpress.ips.gfsg.beans.xml.datacapture.request;
/*     */ 
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="RailSegInfoType", propOrder={"dprtStnCd", "dprtDt", "arrStnCd"})
/*     */ public class RailSegInfoType
/*     */ {
/*     */   @XmlElement(name="DprtStnCd")
/*     */   protected String dprtStnCd;
/*     */   @XmlElement(name="DprtDt")
/*     */   protected String dprtDt;
/*     */   @XmlElement(name="ArrStnCd")
/*     */   protected String arrStnCd;
/*     */   
/*     */   public String getDprtStnCd()
/*     */   {
/*  72 */     return this.dprtStnCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setDprtStnCd(String value)
/*     */   {
/*  84 */     this.dprtStnCd = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getDprtDt()
/*     */   {
/*  96 */     return this.dprtDt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setDprtDt(String value)
/*     */   {
/* 108 */     this.dprtDt = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getArrStnCd()
/*     */   {
/* 120 */     return this.arrStnCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setArrStnCd(String value)
/*     */   {
/* 132 */     this.arrStnCd = value;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\xml\datacapture\request\RailSegInfoType.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */