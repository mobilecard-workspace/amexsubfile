/*     */ package com.americanexpress.ips.gfsg.beans.xml.datacapture.request;
/*     */ 
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="AirlineSegInfoType", propOrder={})
/*     */ public class AirlineSegInfoType
/*     */ {
/*     */   @XmlElement(name="StopoverCd")
/*     */   protected String stopoverCd;
/*     */   @XmlElement(name="DprtLocCd")
/*     */   protected String dprtLocCd;
/*     */   @XmlElement(name="DprtDt")
/*     */   protected String dprtDt;
/*     */   @XmlElement(name="ArrLocCd")
/*     */   protected String arrLocCd;
/*     */   @XmlElement(name="SegIATACarrierCd")
/*     */   protected String segIATACarrierCd;
/*     */   @XmlElement(name="FareBasisTxt")
/*     */   protected String fareBasisTxt;
/*     */   @XmlElement(name="ServClassCd")
/*     */   protected String servClassCd;
/*     */   @XmlElement(name="FlghtNbr")
/*     */   protected String flghtNbr;
/*     */   @XmlElement(name="SegTotFareAmt")
/*     */   protected String segTotFareAmt;
/*     */   
/*     */   public String getStopoverCd()
/*     */   {
/* 113 */     return this.stopoverCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setStopoverCd(String value)
/*     */   {
/* 125 */     this.stopoverCd = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getDprtLocCd()
/*     */   {
/* 137 */     return this.dprtLocCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setDprtLocCd(String value)
/*     */   {
/* 149 */     this.dprtLocCd = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getDprtDt()
/*     */   {
/* 161 */     return this.dprtDt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setDprtDt(String value)
/*     */   {
/* 173 */     this.dprtDt = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getArrLocCd()
/*     */   {
/* 185 */     return this.arrLocCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setArrLocCd(String value)
/*     */   {
/* 197 */     this.arrLocCd = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getSegIATACarrierCd()
/*     */   {
/* 209 */     return this.segIATACarrierCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setSegIATACarrierCd(String value)
/*     */   {
/* 221 */     this.segIATACarrierCd = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getFareBasisTxt()
/*     */   {
/* 233 */     return this.fareBasisTxt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setFareBasisTxt(String value)
/*     */   {
/* 245 */     this.fareBasisTxt = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getServClassCd()
/*     */   {
/* 257 */     return this.servClassCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setServClassCd(String value)
/*     */   {
/* 269 */     this.servClassCd = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getFlghtNbr()
/*     */   {
/* 281 */     return this.flghtNbr;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setFlghtNbr(String value)
/*     */   {
/* 293 */     this.flghtNbr = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getSegTotFareAmt()
/*     */   {
/* 305 */     return this.segTotFareAmt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setSegTotFareAmt(String value)
/*     */   {
/* 317 */     this.segTotFareAmt = value;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\xml\datacapture\request\AirlineSegInfoType.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */