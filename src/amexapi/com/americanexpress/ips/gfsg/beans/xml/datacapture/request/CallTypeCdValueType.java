/*    */ package com.americanexpress.ips.gfsg.beans.xml.datacapture.request;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlEnum;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlEnum
/*    */ @XmlType(name="CallTypeCdValueType")
/*    */ public enum CallTypeCdValueType
/*    */ {
/* 36 */   ISD, 
/* 37 */   ITF, 
/* 38 */   LOCAL, 
/* 39 */   TDS;
/*    */   
/*    */   public String value() {
/* 42 */     return name();
/*    */   }
/*    */   
/*    */   public static CallTypeCdValueType fromValue(String v) {
/* 46 */     return valueOf(v);
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\xml\datacapture\request\CallTypeCdValueType.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */