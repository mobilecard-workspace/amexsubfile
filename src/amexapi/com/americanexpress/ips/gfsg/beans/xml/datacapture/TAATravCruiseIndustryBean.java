/*     */ package com.americanexpress.ips.gfsg.beans.xml.datacapture;
/*     */ 
/*     */ import java.util.List;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class TAATravCruiseIndustryBean
/*     */ {
/*     */   private String iATACarrierCd;
/*     */   private String iATAAgcyNbr;
/*     */   private String travPackTypeCd;
/*     */   private String tktNbr;
/*     */   private String passNm;
/*     */   private String ldgCheckInDt;
/*     */   private String ldgCheckOutDt;
/*     */   private String ldgRmRate;
/*     */   private String nbrOfNgtCnt;
/*     */   private String ldgNm;
/*     */   private String ldgCityNm;
/*     */   private String ldgRgnCd;
/*     */   private String ldgCtryCd;
/*     */   private List<TravelSegInfoBean> travelSegInfoBean;
/*     */   
/*     */   public List<TravelSegInfoBean> getTravelSegInfoBean()
/*     */   {
/*  36 */     return this.travelSegInfoBean;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTravelSegInfoBean(List<TravelSegInfoBean> travelSegInfoBean)
/*     */   {
/*  46 */     this.travelSegInfoBean = travelSegInfoBean;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getIATACarrierCd()
/*     */   {
/*  56 */     return this.iATACarrierCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setIATACarrierCd(String carrierCd)
/*     */   {
/*  66 */     this.iATACarrierCd = carrierCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getIATAAgcyNbr()
/*     */   {
/*  76 */     return this.iATAAgcyNbr;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setIATAAgcyNbr(String agcyNbr)
/*     */   {
/*  86 */     this.iATAAgcyNbr = agcyNbr;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTravPackTypeCd()
/*     */   {
/*  96 */     return this.travPackTypeCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTravPackTypeCd(String travPackTypeCd)
/*     */   {
/* 106 */     this.travPackTypeCd = travPackTypeCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTktNbr()
/*     */   {
/* 116 */     return this.tktNbr;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTktNbr(String tktNbr)
/*     */   {
/* 126 */     this.tktNbr = tktNbr;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getPassNm()
/*     */   {
/* 136 */     return this.passNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPassNm(String passNm)
/*     */   {
/* 146 */     this.passNm = passNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getLdgCheckInDt()
/*     */   {
/* 156 */     return this.ldgCheckInDt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLdgCheckInDt(String ldgCheckInDt)
/*     */   {
/* 166 */     this.ldgCheckInDt = ldgCheckInDt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getLdgCheckOutDt()
/*     */   {
/* 176 */     return this.ldgCheckOutDt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLdgCheckOutDt(String ldgCheckOutDt)
/*     */   {
/* 186 */     this.ldgCheckOutDt = ldgCheckOutDt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getLdgRmRate()
/*     */   {
/* 196 */     return this.ldgRmRate;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLdgRmRate(String ldgRmRate)
/*     */   {
/* 206 */     this.ldgRmRate = ldgRmRate;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getNbrOfNgtCnt()
/*     */   {
/* 216 */     return this.nbrOfNgtCnt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setNbrOfNgtCnt(String nbrOfNgtCnt)
/*     */   {
/* 226 */     this.nbrOfNgtCnt = nbrOfNgtCnt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getLdgNm()
/*     */   {
/* 236 */     return this.ldgNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLdgNm(String ldgNm)
/*     */   {
/* 246 */     this.ldgNm = ldgNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getLdgCityNm()
/*     */   {
/* 256 */     return this.ldgCityNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLdgCityNm(String ldgCityNm)
/*     */   {
/* 266 */     this.ldgCityNm = ldgCityNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getLdgRgnCd()
/*     */   {
/* 276 */     return this.ldgRgnCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLdgRgnCd(String ldgRgnCd)
/*     */   {
/* 286 */     this.ldgRgnCd = ldgRgnCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getLdgCtryCd()
/*     */   {
/* 296 */     return this.ldgCtryCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLdgCtryCd(String ldgCtryCd)
/*     */   {
/* 306 */     this.ldgCtryCd = ldgCtryCd;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\xml\datacapture\TAATravCruiseIndustryBean.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */