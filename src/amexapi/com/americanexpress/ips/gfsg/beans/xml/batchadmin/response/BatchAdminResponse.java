/*     */ package com.americanexpress.ips.gfsg.beans.xml.batchadmin.response;
/*     */ 
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlRootElement;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="", propOrder={"version", "merId", "merTrmnlId", "batchID", "batchStatus", "batchStatusText", "openedDt", "openedTm", "closedDt", "closedTm", "processedDt", "processedTm", "transDt", "transTm", "transCurrCd", "transAmountCredit", "transCountCredit", "transAmountDebit", "transCountDebit", "transAmountTotal", "transCountTotal", "lastRefNumber"})
/*     */ @XmlRootElement(name="BatchAdminResponse")
/*     */ public class BatchAdminResponse
/*     */ {
/*     */   @XmlElement(name="Version")
/*     */   protected long version;
/*     */   @XmlElement(name="MerId")
/*     */   protected long merId;
/*     */   @XmlElement(name="MerTrmnlId", required=true)
/*     */   protected String merTrmnlId;
/*     */   @XmlElement(name="BatchID")
/*     */   protected int batchID;
/*     */   @XmlElement(name="BatchStatus", required=true)
/*     */   protected String batchStatus;
/*     */   @XmlElement(name="BatchStatusText", required=true)
/*     */   protected String batchStatusText;
/*     */   @XmlElement(name="OpenedDt")
/*     */   protected String openedDt;
/*     */   @XmlElement(name="OpenedTm")
/*     */   protected String openedTm;
/*     */   @XmlElement(name="ClosedDt")
/*     */   protected String closedDt;
/*     */   @XmlElement(name="ClosedTm")
/*     */   protected String closedTm;
/*     */   @XmlElement(name="ProcessedDt")
/*     */   protected String processedDt;
/*     */   @XmlElement(name="ProcessedTm")
/*     */   protected String processedTm;
/*     */   @XmlElement(name="TransDt")
/*     */   protected String transDt;
/*     */   @XmlElement(name="TransTm")
/*     */   protected String transTm;
/*     */   @XmlElement(name="TransCurrCd")
/*     */   protected String transCurrCd;
/*     */   @XmlElement(name="TransAmountCredit")
/*     */   protected Long transAmountCredit;
/*     */   @XmlElement(name="TransCountCredit")
/*     */   protected Integer transCountCredit;
/*     */   @XmlElement(name="TransAmountDebit")
/*     */   protected Long transAmountDebit;
/*     */   @XmlElement(name="TransCountDebit")
/*     */   protected Integer transCountDebit;
/*     */   @XmlElement(name="TransAmountTotal")
/*     */   protected Long transAmountTotal;
/*     */   @XmlElement(name="TransCountTotal")
/*     */   protected Integer transCountTotal;
/*     */   @XmlElement(name="LastRefNumber")
/*     */   protected Long lastRefNumber;
/*     */   
/*     */   public long getVersion()
/*     */   {
/* 267 */     return this.version;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setVersion(long value)
/*     */   {
/* 275 */     this.version = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public long getMerId()
/*     */   {
/* 283 */     return this.merId;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setMerId(long value)
/*     */   {
/* 291 */     this.merId = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getMerTrmnlId()
/*     */   {
/* 303 */     return this.merTrmnlId;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setMerTrmnlId(String value)
/*     */   {
/* 315 */     this.merTrmnlId = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getBatchID()
/*     */   {
/* 323 */     return this.batchID;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setBatchID(int value)
/*     */   {
/* 331 */     this.batchID = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getBatchStatus()
/*     */   {
/* 343 */     return this.batchStatus;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setBatchStatus(String value)
/*     */   {
/* 355 */     this.batchStatus = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getBatchStatusText()
/*     */   {
/* 367 */     return this.batchStatusText;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setBatchStatusText(String value)
/*     */   {
/* 379 */     this.batchStatusText = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getOpenedDt()
/*     */   {
/* 391 */     return this.openedDt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setOpenedDt(String value)
/*     */   {
/* 403 */     this.openedDt = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getOpenedTm()
/*     */   {
/* 415 */     return this.openedTm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setOpenedTm(String value)
/*     */   {
/* 427 */     this.openedTm = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getClosedDt()
/*     */   {
/* 439 */     return this.closedDt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setClosedDt(String value)
/*     */   {
/* 451 */     this.closedDt = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getClosedTm()
/*     */   {
/* 463 */     return this.closedTm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setClosedTm(String value)
/*     */   {
/* 475 */     this.closedTm = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getProcessedDt()
/*     */   {
/* 487 */     return this.processedDt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setProcessedDt(String value)
/*     */   {
/* 499 */     this.processedDt = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getProcessedTm()
/*     */   {
/* 511 */     return this.processedTm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setProcessedTm(String value)
/*     */   {
/* 523 */     this.processedTm = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTransDt()
/*     */   {
/* 535 */     return this.transDt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTransDt(String value)
/*     */   {
/* 547 */     this.transDt = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTransTm()
/*     */   {
/* 559 */     return this.transTm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTransTm(String value)
/*     */   {
/* 571 */     this.transTm = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTransCurrCd()
/*     */   {
/* 583 */     return this.transCurrCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTransCurrCd(String value)
/*     */   {
/* 595 */     this.transCurrCd = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Long getTransAmountCredit()
/*     */   {
/* 607 */     return this.transAmountCredit;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTransAmountCredit(Long value)
/*     */   {
/* 619 */     this.transAmountCredit = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Integer getTransCountCredit()
/*     */   {
/* 631 */     return this.transCountCredit;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTransCountCredit(Integer value)
/*     */   {
/* 643 */     this.transCountCredit = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Long getTransAmountDebit()
/*     */   {
/* 655 */     return this.transAmountDebit;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTransAmountDebit(Long value)
/*     */   {
/* 667 */     this.transAmountDebit = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Integer getTransCountDebit()
/*     */   {
/* 679 */     return this.transCountDebit;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTransCountDebit(Integer value)
/*     */   {
/* 691 */     this.transCountDebit = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Long getTransAmountTotal()
/*     */   {
/* 703 */     return this.transAmountTotal;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTransAmountTotal(Long value)
/*     */   {
/* 715 */     this.transAmountTotal = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Integer getTransCountTotal()
/*     */   {
/* 727 */     return this.transCountTotal;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTransCountTotal(Integer value)
/*     */   {
/* 739 */     this.transCountTotal = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Long getLastRefNumber()
/*     */   {
/* 751 */     return this.lastRefNumber;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLastRefNumber(Long value)
/*     */   {
/* 763 */     this.lastRefNumber = value;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\xml\batchadmin\response\BatchAdminResponse.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */