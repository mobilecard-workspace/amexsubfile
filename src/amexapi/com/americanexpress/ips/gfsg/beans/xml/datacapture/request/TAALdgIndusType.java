/*     */ package com.americanexpress.ips.gfsg.beans.xml.datacapture.request;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="TAALdgIndusType", propOrder={"ldgSpecProgCd", "ldgCheckInDt", "ldgCheckOutDt", "ldgRoomInfo", "ldgRentNm", "ldgFolioNbr"})
/*     */ public class TAALdgIndusType
/*     */ {
/*     */   @XmlElement(name="LdgSpecProgCd", required=true)
/*     */   protected String ldgSpecProgCd;
/*     */   @XmlElement(name="LdgCheckInDt", required=true)
/*     */   protected String ldgCheckInDt;
/*     */   @XmlElement(name="LdgCheckOutDt", required=true)
/*     */   protected String ldgCheckOutDt;
/*     */   @XmlElement(name="LdgRoomInfo")
/*     */   protected List<LdgRoomInfoType> ldgRoomInfo;
/*     */   @XmlElement(name="LdgRentNm")
/*     */   protected String ldgRentNm;
/*     */   @XmlElement(name="LdgFolioNbr", required=true)
/*     */   protected String ldgFolioNbr;
/*     */   
/*     */   public String getLdgSpecProgCd()
/*     */   {
/*  82 */     return this.ldgSpecProgCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLdgSpecProgCd(String value)
/*     */   {
/*  94 */     this.ldgSpecProgCd = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getLdgCheckInDt()
/*     */   {
/* 106 */     return this.ldgCheckInDt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLdgCheckInDt(String value)
/*     */   {
/* 118 */     this.ldgCheckInDt = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getLdgCheckOutDt()
/*     */   {
/* 130 */     return this.ldgCheckOutDt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLdgCheckOutDt(String value)
/*     */   {
/* 142 */     this.ldgCheckOutDt = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<LdgRoomInfoType> getLdgRoomInfo()
/*     */   {
/* 168 */     if (this.ldgRoomInfo == null) {
/* 169 */       this.ldgRoomInfo = new ArrayList();
/*     */     }
/* 171 */     return this.ldgRoomInfo;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getLdgRentNm()
/*     */   {
/* 183 */     return this.ldgRentNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLdgRentNm(String value)
/*     */   {
/* 195 */     this.ldgRentNm = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getLdgFolioNbr()
/*     */   {
/* 207 */     return this.ldgFolioNbr;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLdgFolioNbr(String value)
/*     */   {
/* 219 */     this.ldgFolioNbr = value;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\xml\datacapture\request\TAALdgIndusType.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */