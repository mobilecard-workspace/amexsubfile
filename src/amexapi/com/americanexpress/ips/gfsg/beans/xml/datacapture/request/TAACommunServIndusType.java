/*     */ package com.americanexpress.ips.gfsg.beans.xml.datacapture.request;
/*     */ 
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="TAACommunServIndusType", propOrder={})
/*     */ public class TAACommunServIndusType
/*     */ {
/*     */   @XmlElement(name="CallDt", required=true)
/*     */   protected String callDt;
/*     */   @XmlElement(name="CallTm")
/*     */   protected String callTm;
/*     */   @XmlElement(name="CallDurTm", required=true)
/*     */   protected String callDurTm;
/*     */   @XmlElement(name="CallFromLocNm", required=true)
/*     */   protected String callFromLocNm;
/*     */   @XmlElement(name="CallFromRgnCd")
/*     */   protected String callFromRgnCd;
/*     */   @XmlElement(name="CallFromCtryCd")
/*     */   protected String callFromCtryCd;
/*     */   @XmlElement(name="CallFromPhoneNbr", required=true)
/*     */   protected String callFromPhoneNbr;
/*     */   @XmlElement(name="CallToLocNm", required=true)
/*     */   protected String callToLocNm;
/*     */   @XmlElement(name="CallToRgnCd")
/*     */   protected String callToRgnCd;
/*     */   @XmlElement(name="CallToCtryCd")
/*     */   protected String callToCtryCd;
/*     */   @XmlElement(name="CallToPhoneNbr", required=true)
/*     */   protected String callToPhoneNbr;
/*     */   @XmlElement(name="CallingCardId")
/*     */   protected String callingCardId;
/*     */   @XmlElement(name="ServTypeDescTxt")
/*     */   protected String servTypeDescTxt;
/*     */   @XmlElement(name="BillPerTxt")
/*     */   protected String billPerTxt;
/*     */   @XmlElement(name="CallTypeCd")
/*     */   protected String callTypeCd;
/*     */   @XmlElement(name="RateClassCd")
/*     */   protected String rateClassCd;
/*     */   
/*     */   public String getCallDt()
/*     */   {
/* 152 */     return this.callDt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCallDt(String value)
/*     */   {
/* 164 */     this.callDt = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCallTm()
/*     */   {
/* 176 */     return this.callTm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCallTm(String value)
/*     */   {
/* 188 */     this.callTm = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCallDurTm()
/*     */   {
/* 200 */     return this.callDurTm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCallDurTm(String value)
/*     */   {
/* 212 */     this.callDurTm = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCallFromLocNm()
/*     */   {
/* 224 */     return this.callFromLocNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCallFromLocNm(String value)
/*     */   {
/* 236 */     this.callFromLocNm = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCallFromRgnCd()
/*     */   {
/* 248 */     return this.callFromRgnCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCallFromRgnCd(String value)
/*     */   {
/* 260 */     this.callFromRgnCd = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCallFromCtryCd()
/*     */   {
/* 272 */     return this.callFromCtryCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCallFromCtryCd(String value)
/*     */   {
/* 284 */     this.callFromCtryCd = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCallFromPhoneNbr()
/*     */   {
/* 296 */     return this.callFromPhoneNbr;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCallFromPhoneNbr(String value)
/*     */   {
/* 308 */     this.callFromPhoneNbr = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCallToLocNm()
/*     */   {
/* 320 */     return this.callToLocNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCallToLocNm(String value)
/*     */   {
/* 332 */     this.callToLocNm = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCallToRgnCd()
/*     */   {
/* 344 */     return this.callToRgnCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCallToRgnCd(String value)
/*     */   {
/* 356 */     this.callToRgnCd = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCallToCtryCd()
/*     */   {
/* 368 */     return this.callToCtryCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCallToCtryCd(String value)
/*     */   {
/* 380 */     this.callToCtryCd = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCallToPhoneNbr()
/*     */   {
/* 392 */     return this.callToPhoneNbr;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCallToPhoneNbr(String value)
/*     */   {
/* 404 */     this.callToPhoneNbr = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCallingCardId()
/*     */   {
/* 416 */     return this.callingCardId;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCallingCardId(String value)
/*     */   {
/* 428 */     this.callingCardId = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getServTypeDescTxt()
/*     */   {
/* 440 */     return this.servTypeDescTxt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setServTypeDescTxt(String value)
/*     */   {
/* 452 */     this.servTypeDescTxt = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getBillPerTxt()
/*     */   {
/* 464 */     return this.billPerTxt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setBillPerTxt(String value)
/*     */   {
/* 476 */     this.billPerTxt = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCallTypeCd()
/*     */   {
/* 488 */     return this.callTypeCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCallTypeCd(String value)
/*     */   {
/* 500 */     this.callTypeCd = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getRateClassCd()
/*     */   {
/* 512 */     return this.rateClassCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setRateClassCd(String value)
/*     */   {
/* 524 */     this.rateClassCd = value;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\xml\datacapture\request\TAACommunServIndusType.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */