/*     */ package com.americanexpress.ips.gfsg.beans.xml.datacapture;
/*     */ 
/*     */ 
/*     */ 
/*     */ public class TAAInsuranceIndustryBean
/*     */ {
/*     */   private String insrPlcyNbr;
/*     */   
/*     */ 
/*     */   private String insrCovStartDt;
/*     */   
/*     */ 
/*     */   private String insrCovEndDt;
/*     */   
/*     */ 
/*     */   private String insrPlcyPremFreqTxt;
/*     */   
/*     */ 
/*     */   private String addInsrPlcyNo;
/*     */   
/*     */ 
/*     */   private String plcyTypeTxt;
/*     */   
/*     */ 
/*     */   private String insrNm;
/*     */   
/*     */ 
/*     */   public String getInsrPlcyNbr()
/*     */   {
/*  30 */     return this.insrPlcyNbr;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setInsrPlcyNbr(String insrPlcyNbr)
/*     */   {
/*  40 */     this.insrPlcyNbr = insrPlcyNbr;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getInsrCovStartDt()
/*     */   {
/*  50 */     return this.insrCovStartDt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setInsrCovStartDt(String insrCovStartDt)
/*     */   {
/*  60 */     this.insrCovStartDt = insrCovStartDt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getInsrCovEndDt()
/*     */   {
/*  72 */     return this.insrCovEndDt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setInsrCovEndDt(String insrCovEndDt)
/*     */   {
/*  84 */     this.insrCovEndDt = insrCovEndDt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getInsrPlcyPremFreqTxt()
/*     */   {
/*  94 */     return this.insrPlcyPremFreqTxt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setInsrPlcyPremFreqTxt(String insrPlcyPremFreqTxt)
/*     */   {
/* 104 */     this.insrPlcyPremFreqTxt = insrPlcyPremFreqTxt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getAddInsrPlcyNo()
/*     */   {
/* 114 */     return this.addInsrPlcyNo;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setAddInsrPlcyNo(String addInsrPlcyNo)
/*     */   {
/* 124 */     this.addInsrPlcyNo = addInsrPlcyNo;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getPlcyTypeTxt()
/*     */   {
/* 134 */     return this.plcyTypeTxt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPlcyTypeTxt(String plcyTypeTxt)
/*     */   {
/* 144 */     this.plcyTypeTxt = plcyTypeTxt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getInsrNm()
/*     */   {
/* 154 */     return this.insrNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setInsrNm(String insrNm)
/*     */   {
/* 164 */     this.insrNm = insrNm;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\xml\datacapture\TAAInsuranceIndustryBean.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */