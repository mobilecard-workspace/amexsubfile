/*     */ package com.americanexpress.ips.gfsg.beans.xml.datacapture.request;
/*     */ 
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="TAAInsrIndusType", propOrder={})
/*     */ public class TAAInsrIndusType
/*     */ {
/*     */   @XmlElement(name="InsrPlcyNbr", required=true)
/*     */   protected String insrPlcyNbr;
/*     */   @XmlElement(name="InsrCovStartDt", required=true)
/*     */   protected String insrCovStartDt;
/*     */   @XmlElement(name="InsrCovEndDt", required=true)
/*     */   protected String insrCovEndDt;
/*     */   @XmlElement(name="InsrPlcyPremFreqTxt")
/*     */   protected String insrPlcyPremFreqTxt;
/*     */   @XmlElement(name="AddInsrPlcyNo")
/*     */   protected String addInsrPlcyNo;
/*     */   @XmlElement(name="PlcyTypeTxt", required=true)
/*     */   protected String plcyTypeTxt;
/*     */   @XmlElement(name="InsrNm")
/*     */   protected String insrNm;
/*     */   
/*     */   public String getInsrPlcyNbr()
/*     */   {
/*  84 */     return this.insrPlcyNbr;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setInsrPlcyNbr(String value)
/*     */   {
/*  96 */     this.insrPlcyNbr = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getInsrCovStartDt()
/*     */   {
/* 108 */     return this.insrCovStartDt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setInsrCovStartDt(String value)
/*     */   {
/* 120 */     this.insrCovStartDt = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getInsrCovEndDt()
/*     */   {
/* 132 */     return this.insrCovEndDt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setInsrCovEndDt(String value)
/*     */   {
/* 144 */     this.insrCovEndDt = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getInsrPlcyPremFreqTxt()
/*     */   {
/* 156 */     return this.insrPlcyPremFreqTxt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setInsrPlcyPremFreqTxt(String value)
/*     */   {
/* 168 */     this.insrPlcyPremFreqTxt = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getAddInsrPlcyNo()
/*     */   {
/* 180 */     return this.addInsrPlcyNo;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setAddInsrPlcyNo(String value)
/*     */   {
/* 192 */     this.addInsrPlcyNo = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getPlcyTypeTxt()
/*     */   {
/* 204 */     return this.plcyTypeTxt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPlcyTypeTxt(String value)
/*     */   {
/* 216 */     this.plcyTypeTxt = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getInsrNm()
/*     */   {
/* 228 */     return this.insrNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setInsrNm(String value)
/*     */   {
/* 240 */     this.insrNm = value;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\xml\datacapture\request\TAAInsrIndusType.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */