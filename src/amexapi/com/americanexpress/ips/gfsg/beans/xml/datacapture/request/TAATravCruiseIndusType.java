/*     */ package com.americanexpress.ips.gfsg.beans.xml.datacapture.request;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="TAATravCruiseIndusType", propOrder={"iataCarrierCd", "iataAgcyNbr", "travPackTypeCd", "passNm", "tktNbr", "travelSegInfo", "ldgCheckInDt", "ldgCheckOutDt", "ldgRmRate", "nbrOfNgtCnt", "ldgNm", "ldgRgnCd", "ldgCtryCd", "ldgCityNm"})
/*     */ public class TAATravCruiseIndusType
/*     */ {
/*     */   @XmlElement(name="IATACarrierCd")
/*     */   protected String iataCarrierCd;
/*     */   @XmlElement(name="IATAAgcyNbr")
/*     */   protected String iataAgcyNbr;
/*     */   @XmlElement(name="TravPackTypeCd", required=true)
/*     */   protected TravPackTypeCdType travPackTypeCd;
/*     */   @XmlElement(name="PassNm", required=true)
/*     */   protected String passNm;
/*     */   @XmlElement(name="TktNbr", required=true)
/*     */   protected String tktNbr;
/*     */   @XmlElement(name="TravelSegInfo")
/*     */   protected List<TravelSegInfoType> travelSegInfo;
/*     */   @XmlElement(name="LdgCheckInDt")
/*     */   protected String ldgCheckInDt;
/*     */   @XmlElement(name="LdgCheckOutDt")
/*     */   protected String ldgCheckOutDt;
/*     */   @XmlElement(name="LdgRmRate")
/*     */   protected String ldgRmRate;
/*     */   @XmlElement(name="NbrOfNgtCnt")
/*     */   protected String nbrOfNgtCnt;
/*     */   @XmlElement(name="LdgNm")
/*     */   protected String ldgNm;
/*     */   @XmlElement(name="LdgRgnCd")
/*     */   protected String ldgRgnCd;
/*     */   @XmlElement(name="LdgCtryCd")
/*     */   protected String ldgCtryCd;
/*     */   @XmlElement(name="LdgCityNm")
/*     */   protected String ldgCityNm;
/*     */   
/*     */   public String getIATACarrierCd()
/*     */   {
/* 155 */     return this.iataCarrierCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setIATACarrierCd(String value)
/*     */   {
/* 167 */     this.iataCarrierCd = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getIATAAgcyNbr()
/*     */   {
/* 179 */     return this.iataAgcyNbr;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setIATAAgcyNbr(String value)
/*     */   {
/* 191 */     this.iataAgcyNbr = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public TravPackTypeCdType getTravPackTypeCd()
/*     */   {
/* 203 */     return this.travPackTypeCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTravPackTypeCd(TravPackTypeCdType value)
/*     */   {
/* 215 */     this.travPackTypeCd = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getPassNm()
/*     */   {
/* 227 */     return this.passNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPassNm(String value)
/*     */   {
/* 239 */     this.passNm = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTktNbr()
/*     */   {
/* 251 */     return this.tktNbr;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTktNbr(String value)
/*     */   {
/* 263 */     this.tktNbr = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<TravelSegInfoType> getTravelSegInfo()
/*     */   {
/* 289 */     if (this.travelSegInfo == null) {
/* 290 */       this.travelSegInfo = new ArrayList();
/*     */     }
/* 292 */     return this.travelSegInfo;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getLdgCheckInDt()
/*     */   {
/* 304 */     return this.ldgCheckInDt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLdgCheckInDt(String value)
/*     */   {
/* 316 */     this.ldgCheckInDt = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getLdgCheckOutDt()
/*     */   {
/* 328 */     return this.ldgCheckOutDt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLdgCheckOutDt(String value)
/*     */   {
/* 340 */     this.ldgCheckOutDt = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getLdgRmRate()
/*     */   {
/* 352 */     return this.ldgRmRate;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLdgRmRate(String value)
/*     */   {
/* 364 */     this.ldgRmRate = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getNbrOfNgtCnt()
/*     */   {
/* 376 */     return this.nbrOfNgtCnt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setNbrOfNgtCnt(String value)
/*     */   {
/* 388 */     this.nbrOfNgtCnt = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getLdgNm()
/*     */   {
/* 400 */     return this.ldgNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLdgNm(String value)
/*     */   {
/* 412 */     this.ldgNm = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getLdgRgnCd()
/*     */   {
/* 424 */     return this.ldgRgnCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLdgRgnCd(String value)
/*     */   {
/* 436 */     this.ldgRgnCd = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getLdgCtryCd()
/*     */   {
/* 448 */     return this.ldgCtryCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLdgCtryCd(String value)
/*     */   {
/* 460 */     this.ldgCtryCd = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getLdgCityNm()
/*     */   {
/* 472 */     return this.ldgCityNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLdgCityNm(String value)
/*     */   {
/* 484 */     this.ldgCityNm = value;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\xml\datacapture\request\TAATravCruiseIndusType.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */