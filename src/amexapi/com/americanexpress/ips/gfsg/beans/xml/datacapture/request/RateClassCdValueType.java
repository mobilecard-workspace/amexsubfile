/*    */ package com.americanexpress.ips.gfsg.beans.xml.datacapture.request;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlEnum;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlEnum
/*    */ @XmlType(name="RateClassCdValueType")
/*    */ public enum RateClassCdValueType
/*    */ {
/* 37 */   D, 
/* 38 */   E, 
/* 39 */   N, 
/* 40 */   S;
/*    */   
/*    */   public String value() {
/* 43 */     return name();
/*    */   }
/*    */   
/*    */   public static RateClassCdValueType fromValue(String v) {
/* 47 */     return valueOf(v);
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\xml\datacapture\request\RateClassCdValueType.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */