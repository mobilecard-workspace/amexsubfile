/*    */ package com.americanexpress.ips.gfsg.beans.xml.datacapture.request;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlEnum;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlEnum
/*    */ @XmlType(name="AuditAdjIndCdValueType")
/*    */ public enum AuditAdjIndCdValueType
/*    */ {
/* 36 */   X, 
/* 37 */   Y, 
/* 38 */   Z;
/*    */   
/*    */   public String value() {
/* 41 */     return name();
/*    */   }
/*    */   
/*    */   public static AuditAdjIndCdValueType fromValue(String v) {
/* 45 */     return valueOf(v);
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\xml\datacapture\request\AuditAdjIndCdValueType.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */