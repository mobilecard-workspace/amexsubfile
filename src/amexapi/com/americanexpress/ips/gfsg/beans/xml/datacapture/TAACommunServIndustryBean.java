/*     */ package com.americanexpress.ips.gfsg.beans.xml.datacapture;
/*     */ 
/*     */ 
/*     */ public class TAACommunServIndustryBean
/*     */ {
/*     */   private String callDt;
/*     */   
/*     */   private String callTm;
/*     */   
/*     */   private String callDurTm;
/*     */   
/*     */   private String callFromLocNm;
/*     */   
/*     */   private String callFromRgnCd;
/*     */   
/*     */   private String callFromCtryCd;
/*     */   
/*     */   private String callFromPhoneNbr;
/*     */   
/*     */   private String callToLocNm;
/*     */   
/*     */   private String callToRgnCd;
/*     */   
/*     */   private String callToCtryCd;
/*     */   
/*     */   private String callToPhoneNbr;
/*     */   
/*     */   private String callingCardId;
/*     */   
/*     */   private String servTypeDescTxt;
/*     */   private String billPerTxt;
/*     */   private String callTypeCd;
/*     */   private String rateClassCd;
/*     */   
/*     */   public String getCallDt()
/*     */   {
/*  37 */     return this.callDt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCallDt(String callDt)
/*     */   {
/*  47 */     this.callDt = callDt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCallTm()
/*     */   {
/*  57 */     return this.callTm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCallTm(String callTm)
/*     */   {
/*  67 */     this.callTm = callTm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCallDurTm()
/*     */   {
/*  79 */     return this.callDurTm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCallDurTm(String callDurTm)
/*     */   {
/*  91 */     this.callDurTm = callDurTm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCallFromLocNm()
/*     */   {
/* 101 */     return this.callFromLocNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCallFromLocNm(String callFromLocNm)
/*     */   {
/* 111 */     this.callFromLocNm = callFromLocNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCallFromRgnCd()
/*     */   {
/* 123 */     return this.callFromRgnCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCallFromRgnCd(String callFromRgnCd)
/*     */   {
/* 135 */     this.callFromRgnCd = callFromRgnCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCallFromCtryCd()
/*     */   {
/* 147 */     return this.callFromCtryCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCallFromCtryCd(String callFromCtryCd)
/*     */   {
/* 159 */     this.callFromCtryCd = callFromCtryCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCallFromPhoneNbr()
/*     */   {
/* 169 */     return this.callFromPhoneNbr;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCallFromPhoneNbr(String callFromPhoneNbr)
/*     */   {
/* 179 */     this.callFromPhoneNbr = callFromPhoneNbr;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCallToLocNm()
/*     */   {
/* 189 */     return this.callToLocNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCallToLocNm(String callToLocNm)
/*     */   {
/* 199 */     this.callToLocNm = callToLocNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCallToRgnCd()
/*     */   {
/* 211 */     return this.callToRgnCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCallToRgnCd(String callToRgnCd)
/*     */   {
/* 223 */     this.callToRgnCd = callToRgnCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCallToCtryCd()
/*     */   {
/* 235 */     return this.callToCtryCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCallToCtryCd(String callToCtryCd)
/*     */   {
/* 247 */     this.callToCtryCd = callToCtryCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCallToPhoneNbr()
/*     */   {
/* 257 */     return this.callToPhoneNbr;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCallToPhoneNbr(String callToPhoneNbr)
/*     */   {
/* 267 */     this.callToPhoneNbr = callToPhoneNbr;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCallingCardId()
/*     */   {
/* 277 */     return this.callingCardId;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCallingCardId(String callingCardId)
/*     */   {
/* 287 */     this.callingCardId = callingCardId;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getServTypeDescTxt()
/*     */   {
/* 297 */     return this.servTypeDescTxt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setServTypeDescTxt(String servTypeDescTxt)
/*     */   {
/* 307 */     this.servTypeDescTxt = servTypeDescTxt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getBillPerTxt()
/*     */   {
/* 317 */     return this.billPerTxt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setBillPerTxt(String billPerTxt)
/*     */   {
/* 327 */     this.billPerTxt = billPerTxt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCallTypeCd()
/*     */   {
/* 337 */     return this.callTypeCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCallTypeCd(String callTypeCd)
/*     */   {
/* 347 */     this.callTypeCd = callTypeCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getRateClassCd()
/*     */   {
/* 357 */     return this.rateClassCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setRateClassCd(String rateClassCd)
/*     */   {
/* 367 */     this.rateClassCd = rateClassCd;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\xml\datacapture\TAACommunServIndustryBean.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */