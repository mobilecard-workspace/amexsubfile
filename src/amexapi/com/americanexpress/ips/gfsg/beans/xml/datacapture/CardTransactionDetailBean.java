/*    */ package com.americanexpress.ips.gfsg.beans.xml.datacapture;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class CardTransactionDetailBean
/*    */ {
/*    */   private String addAmtTypeCd;
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   private String addAmt;
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   private String signInd;
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public String getAddAmtTypeCd()
/*    */   {
/* 26 */     return this.addAmtTypeCd;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setAddAmtTypeCd(String addAmtTypeCd)
/*    */   {
/* 36 */     this.addAmtTypeCd = addAmtTypeCd;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public String getAddAmt()
/*    */   {
/* 46 */     return this.addAmt;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setAddAmt(String addAmt)
/*    */   {
/* 56 */     this.addAmt = addAmt;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public String getSignInd()
/*    */   {
/* 66 */     return this.signInd;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setSignInd(String signInd)
/*    */   {
/* 76 */     this.signInd = signInd;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\xml\datacapture\CardTransactionDetailBean.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */