/*     */ package com.americanexpress.ips.gfsg.beans.xml.datacapture;
/*     */ 
/*     */ import java.util.List;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class TAACorpPurchSolLvl2Bean
/*     */ {
/*     */   private String reqNm;
/*     */   private List<ChargeItemInfoBean> chargeItemInfoBean;
/*     */   private String cmRefNbr;
/*     */   private String shipToPostCd;
/*     */   private String taxTotAmt;
/*     */   private String taxTypeCd;
/*     */   
/*     */   public String getReqNm()
/*     */   {
/*  28 */     return this.reqNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setReqNm(String reqNm)
/*     */   {
/*  39 */     this.reqNm = reqNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<ChargeItemInfoBean> getChargeItemInfoBean()
/*     */   {
/*  53 */     return this.chargeItemInfoBean;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setChargeItemInfoBean(List<ChargeItemInfoBean> chargeItemInfoBean)
/*     */   {
/*  65 */     this.chargeItemInfoBean = chargeItemInfoBean;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCmRefNbr()
/*     */   {
/*  75 */     return this.cmRefNbr;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCmRefNbr(String cmRefNbr)
/*     */   {
/*  85 */     this.cmRefNbr = cmRefNbr;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getShipToPostCd()
/*     */   {
/*  95 */     return this.shipToPostCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setShipToPostCd(String shipToPostCd)
/*     */   {
/* 105 */     this.shipToPostCd = shipToPostCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTaxTotAmt()
/*     */   {
/* 115 */     return this.taxTotAmt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTaxTotAmt(String taxTotAmt)
/*     */   {
/* 125 */     this.taxTotAmt = taxTotAmt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTaxTypeCd()
/*     */   {
/* 135 */     return this.taxTypeCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTaxTypeCd(String taxTypeCd)
/*     */   {
/* 145 */     this.taxTypeCd = taxTypeCd;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\xml\datacapture\TAACorpPurchSolLvl2Bean.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */