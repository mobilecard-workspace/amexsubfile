/*     */ package com.americanexpress.ips.gfsg.beans.xml.batchadmin;
/*     */ 
/*     */ 
/*     */ public class BatchSummaryResponseBean
/*     */   extends BatchAdminResponseBean
/*     */ {
/*     */   private String openedDt;
/*     */   
/*     */   private String openedTm;
/*     */   
/*     */   private String closedDt;
/*     */   
/*     */   private String closedTm;
/*     */   
/*     */   private String processedDt;
/*     */   
/*     */   private String processedTm;
/*     */   private String transDt;
/*     */   private String transTm;
/*     */   private String transCurrCd;
/*     */   private String transAmountCredit;
/*     */   private String transCountCredit;
/*     */   private String transAmountDebit;
/*     */   private String transCountDebit;
/*     */   private String transAmountTotal;
/*     */   private String transCountTotal;
/*     */   private String lastRefNumber;
/*     */   
/*     */   public String getOpenedDt()
/*     */   {
/*  31 */     return this.openedDt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void setOpenedDt(String openedDt)
/*     */   {
/*  38 */     this.openedDt = openedDt;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getOpenedTm()
/*     */   {
/*  44 */     return this.openedTm;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setOpenedTm(String openedTm)
/*     */   {
/*  50 */     this.openedTm = openedTm;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getClosedDt()
/*     */   {
/*  56 */     return this.closedDt;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setClosedDt(String closedDt)
/*     */   {
/*  62 */     this.closedDt = closedDt;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getClosedTm()
/*     */   {
/*  68 */     return this.closedTm;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setClosedTm(String closedTm)
/*     */   {
/*  74 */     this.closedTm = closedTm;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getProcessedDt()
/*     */   {
/*  80 */     return this.processedDt;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setProcessedDt(String processedDt)
/*     */   {
/*  86 */     this.processedDt = processedDt;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getProcessedTm()
/*     */   {
/*  92 */     return this.processedTm;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setProcessedTm(String processedTm)
/*     */   {
/*  98 */     this.processedTm = processedTm;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getTransDt()
/*     */   {
/* 104 */     return this.transDt;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setTransDt(String transDt)
/*     */   {
/* 110 */     this.transDt = transDt;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getTransTm()
/*     */   {
/* 116 */     return this.transTm;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setTransTm(String transTm)
/*     */   {
/* 122 */     this.transTm = transTm;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getTransCurrCd()
/*     */   {
/* 128 */     return this.transCurrCd;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setTransCurrCd(String transCurrCd)
/*     */   {
/* 134 */     this.transCurrCd = transCurrCd;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getTransAmountCredit()
/*     */   {
/* 140 */     return this.transAmountCredit;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setTransAmountCredit(String transAmountCredit)
/*     */   {
/* 146 */     this.transAmountCredit = transAmountCredit;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getTransCountCredit()
/*     */   {
/* 152 */     return this.transCountCredit;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setTransCountCredit(String transCountCredit)
/*     */   {
/* 158 */     this.transCountCredit = transCountCredit;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getTransAmountDebit()
/*     */   {
/* 164 */     return this.transAmountDebit;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setTransAmountDebit(String transAmountDebit)
/*     */   {
/* 170 */     this.transAmountDebit = transAmountDebit;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getTransCountDebit()
/*     */   {
/* 176 */     return this.transCountDebit;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setTransCountDebit(String transCountDebit)
/*     */   {
/* 182 */     this.transCountDebit = transCountDebit;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getTransAmountTotal()
/*     */   {
/* 188 */     return this.transAmountTotal;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setTransAmountTotal(String transAmountTotal)
/*     */   {
/* 194 */     this.transAmountTotal = transAmountTotal;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getTransCountTotal()
/*     */   {
/* 200 */     return this.transCountTotal;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setTransCountTotal(String transCountTotal)
/*     */   {
/* 206 */     this.transCountTotal = transCountTotal;
/*     */   }
/*     */   
/*     */ 
/*     */   public String getLastRefNumber()
/*     */   {
/* 212 */     return this.lastRefNumber;
/*     */   }
/*     */   
/*     */ 
/*     */   public void setLastRefNumber(String lastRefNumber)
/*     */   {
/* 218 */     this.lastRefNumber = lastRefNumber;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\xml\batchadmin\BatchSummaryResponseBean.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */