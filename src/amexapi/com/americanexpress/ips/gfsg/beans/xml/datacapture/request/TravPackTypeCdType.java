/*    */ package com.americanexpress.ips.gfsg.beans.xml.datacapture.request;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlEnum;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlEnum
/*    */ @XmlType(name="TravPackTypeCdType")
/*    */ public enum TravPackTypeCdType
/*    */ {
/* 37 */   C, 
/* 38 */   A, 
/* 39 */   B, 
/* 40 */   N;
/*    */   
/*    */   public String value() {
/* 43 */     return name();
/*    */   }
/*    */   
/*    */   public static TravPackTypeCdType fromValue(String v) {
/* 47 */     return valueOf(v);
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\xml\datacapture\request\TravPackTypeCdType.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */