/*    */ package com.americanexpress.ips.gfsg.beans.xml.datacapture.request;
/*    */ 
/*    */ import java.util.ArrayList;
/*    */ import java.util.List;
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlElement;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name="CardTransDetailType", propOrder={"addAmtInfo"})
/*    */ public class CardTransDetailType
/*    */ {
/*    */   @XmlElement(name="AddAmtInfo", required=true)
/*    */   protected List<AddAmtInfoType> addAmtInfo;
/*    */   
/*    */   public List<AddAmtInfoType> getAddAmtInfo()
/*    */   {
/* 70 */     if (this.addAmtInfo == null) {
/* 71 */       this.addAmtInfo = new ArrayList();
/*    */     }
/* 73 */     return this.addAmtInfo;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\xml\datacapture\request\CardTransDetailType.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */