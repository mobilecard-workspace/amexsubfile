/*     */ package com.americanexpress.ips.gfsg.beans.xml.datacapture;
/*     */ 
/*     */ 
/*     */ 
/*     */ public class TravelSegInfoBean
/*     */ {
/*     */   private String dprtDt;
/*     */   
/*     */ 
/*     */   private String destCd;
/*     */   
/*     */ 
/*     */   private String dprtAirCd;
/*     */   
/*     */ 
/*     */   private String servClassCd;
/*     */   
/*     */ 
/*     */   private String flghtNbr;
/*     */   
/*     */   private String segIATACarrierCd;
/*     */   
/*     */ 
/*     */   public String getDestCd()
/*     */   {
/*  26 */     return this.destCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setDestCd(String destCd)
/*     */   {
/*  36 */     this.destCd = destCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getDprtAirCd()
/*     */   {
/*  46 */     return this.dprtAirCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setDprtAirCd(String dprtAirCd)
/*     */   {
/*  56 */     this.dprtAirCd = dprtAirCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getDprtDt()
/*     */   {
/*  66 */     return this.dprtDt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setDprtDt(String dprtDt)
/*     */   {
/*  76 */     this.dprtDt = dprtDt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getServClassCd()
/*     */   {
/*  86 */     return this.servClassCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setServClassCd(String servClassCd)
/*     */   {
/*  96 */     this.servClassCd = servClassCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getFlghtNbr()
/*     */   {
/* 106 */     return this.flghtNbr;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setFlghtNbr(String flghtNbr)
/*     */   {
/* 116 */     this.flghtNbr = flghtNbr;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getSegIATACarrierCd()
/*     */   {
/* 126 */     return this.segIATACarrierCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setSegIATACarrierCd(String segIATACarrierCd)
/*     */   {
/* 136 */     this.segIATACarrierCd = segIATACarrierCd;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\xml\datacapture\TravelSegInfoBean.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */