/*    */ package com.americanexpress.ips.gfsg.beans.xml.datacapture.request;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlEnum;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlEnum
/*    */ @XmlType(name="TravTransTypeCdValueType")
/*    */ public enum TravTransTypeCdValueType
/*    */ {
/* 36 */   TKT, 
/* 37 */   EXC, 
/* 38 */   REF, 
/* 39 */   MSC;
/*    */   
/*    */   public String value() {
/* 42 */     return name();
/*    */   }
/*    */   
/*    */   public static TravTransTypeCdValueType fromValue(String v) {
/* 46 */     return valueOf(v);
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\xml\datacapture\request\TravTransTypeCdValueType.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */