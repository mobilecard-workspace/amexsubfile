/*     */ package com.americanexpress.ips.gfsg.beans.xml.datacapture.request;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="TAARetailIndusType", propOrder={"retailDeptNm", "retailItemInfo"})
/*     */ public class TAARetailIndusType
/*     */ {
/*     */   @XmlElement(name="RetailDeptNm")
/*     */   protected String retailDeptNm;
/*     */   @XmlElement(name="RetailItemInfo")
/*     */   protected List<RetailItemInfoType> retailItemInfo;
/*     */   
/*     */   public String getRetailDeptNm()
/*     */   {
/*  60 */     return this.retailDeptNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setRetailDeptNm(String value)
/*     */   {
/*  72 */     this.retailDeptNm = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<RetailItemInfoType> getRetailItemInfo()
/*     */   {
/*  98 */     if (this.retailItemInfo == null) {
/*  99 */       this.retailItemInfo = new ArrayList();
/*     */     }
/* 101 */     return this.retailItemInfo;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\xml\datacapture\request\TAARetailIndusType.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */