/*      */ package com.americanexpress.ips.gfsg.beans.xml.datacapture.request;
/*      */ 
/*      */ import java.math.BigInteger;
/*      */ import javax.xml.bind.annotation.XmlAccessType;
/*      */ import javax.xml.bind.annotation.XmlAccessorType;
/*      */ import javax.xml.bind.annotation.XmlElement;
/*      */ import javax.xml.bind.annotation.XmlRootElement;
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ @XmlAccessorType(XmlAccessType.FIELD)
/*      */ @XmlRootElement(name="DataCaptureRequest")
/*      */ public class DataCaptureRequest
/*      */ {
/*      */   @XmlElement(name="Version", required=true)
/*      */   protected String version;
/*      */   @XmlElement(name="MerId")
/*      */   protected long merId;
/*      */   @XmlElement(name="MerTrmnlId", required=true)
/*      */   protected String merTrmnlId;
/*      */   @XmlElement(name="BatchID", required=true)
/*      */   protected BigInteger batchID;
/*      */   @XmlElement(name="RefNumber", required=true)
/*      */   protected String refNumber;
/*      */   @XmlElement(name="CardNbr", required=true)
/*      */   protected String cardNbr;
/*      */   @XmlElement(name="CardExprDt")
/*      */   protected String cardExprDt;
/*      */   @XmlElement(name="TransDt", required=true)
/*      */   protected String transDt;
/*      */   @XmlElement(name="TransTm")
/*      */   protected String transTm;
/*      */   @XmlElement(name="TransAmt", required=true)
/*      */   protected BigInteger transAmt;
/*      */   @XmlElement(name="TransCurrCd", required=true)
/*      */   protected String transCurrCd;
/*      */   @XmlElement(name="TransProcCd", required=true)
/*      */   protected String transProcCd;
/*      */   @XmlElement(name="TransId")
/*      */   protected String transId;
/*      */   @XmlElement(name="TransAprvCd")
/*      */   protected String transAprvCd;
/*      */   @XmlElement(name="PointOfServiceData", required=true)
/*      */   protected PointOfServiceDataType pointOfServiceData;
/*      */   @XmlElement(name="ElecComrceInd")
/*      */   protected String elecComrceInd;
/*      */   @XmlElement(name="MediaCd")
/*      */   protected String mediaCd;
/*      */   @XmlElement(name="SubmMthdCd")
/*      */   protected String submMthdCd;
/*      */   @XmlElement(name="MerLocId")
/*      */   protected String merLocId;
/*      */   @XmlElement(name="MerCtcInfoTxt")
/*      */   protected String merCtcInfoTxt;
/*      */   @XmlElement(name="TransImageSeqNbr")
/*      */   protected String transImageSeqNbr;
/*      */   @XmlElement(name="MatchKeyTypeCd")
/*      */   protected String matchKeyTypeCd;
/*      */   @XmlElement(name="MatchKeyId")
/*      */   protected String matchKeyId;
/*      */   @XmlElement(name="DefPaymentPlan")
/*      */   protected String defPaymentPlan;
/*      */   @XmlElement(name="NumDefPayments")
/*      */   protected String numDefPayments;
/*      */   @XmlElement(name="EMVData")
/*      */   protected String emvData;
/*      */   @XmlElement(name="MerCtgyCd")
/*      */   protected String merCtgyCd;
/*      */   @XmlElement(name="SellId")
/*      */   protected String sellId;
/*      */   @XmlElement(name="CardTransDetail")
/*      */   protected CardTransDetailType cardTransDetail;
/*      */   @XmlElement(name="TransAddCd")
/*      */   protected String transAddCd;
/*      */   @XmlElement(name="TAAAirIndus")
/*      */   protected TAAAirIndusType taaAirIndus;
/*      */   @XmlElement(name="TAAAutoRentalIndus")
/*      */   protected TAAAutoRentalIndusType taaAutoRentalIndus;
/*      */   @XmlElement(name="TAACommunServIndus")
/*      */   protected TAACommunServIndusType taaCommunServIndus;
/*      */   @XmlElement(name="TAAEntTktIndus")
/*      */   protected TAAEntTktIndusType taaEntTktIndus;
/*      */   @XmlElement(name="TAAInsrIndus")
/*      */   protected TAAInsrIndusType taaInsrIndus;
/*      */   @XmlElement(name="TAALdgIndus")
/*      */   protected TAALdgIndusType taaLdgIndus;
/*      */   @XmlElement(name="TAARailIndus")
/*      */   protected TAARailIndusType taaRailIndus;
/*      */   @XmlElement(name="TAARetailIndus")
/*      */   protected TAARetailIndusType taaRetailIndus;
/*      */   @XmlElement(name="TAATravCruiseIndus")
/*      */   protected TAATravCruiseIndusType taaTravCruiseIndus;
/*      */   @XmlElement(name="TAACorpPurchSolLvl2")
/*      */   protected TAACorpPurchSolLvl2Type taaCorpPurchSolLvl2;
/*      */   @XmlElement(name="InvoiceMsgInfo")
/*      */   protected InvoiceMsgInfoType invoiceMsgInfo;
/*      */   @XmlElement(name="BatchImageSeqNbr")
/*      */   protected String batchImageSeqNbr;
/*      */   @XmlElement(name="ServAgtMerId")
/*      */   protected String servAgtMerId;
/*      */   
/*      */   public String getVersion()
/*      */   {
/*  275 */     return this.version;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setVersion(String value)
/*      */   {
/*  287 */     this.version = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public long getMerId()
/*      */   {
/*  295 */     return this.merId;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setMerId(long value)
/*      */   {
/*  303 */     this.merId = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getMerTrmnlId()
/*      */   {
/*  315 */     return this.merTrmnlId;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setMerTrmnlId(String value)
/*      */   {
/*  327 */     this.merTrmnlId = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public BigInteger getBatchID()
/*      */   {
/*  339 */     return this.batchID;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setBatchID(BigInteger value)
/*      */   {
/*  351 */     this.batchID = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getRefNumber()
/*      */   {
/*  363 */     return this.refNumber;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setRefNumber(String value)
/*      */   {
/*  375 */     this.refNumber = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getCardNbr()
/*      */   {
/*  387 */     return this.cardNbr;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setCardNbr(String value)
/*      */   {
/*  399 */     this.cardNbr = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getCardExprDt()
/*      */   {
/*  411 */     return this.cardExprDt;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setCardExprDt(String value)
/*      */   {
/*  423 */     this.cardExprDt = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTransDt()
/*      */   {
/*  435 */     return this.transDt;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTransDt(String value)
/*      */   {
/*  447 */     this.transDt = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTransTm()
/*      */   {
/*  459 */     return this.transTm;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTransTm(String value)
/*      */   {
/*  471 */     this.transTm = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public BigInteger getTransAmt()
/*      */   {
/*  483 */     return this.transAmt;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTransAmt(BigInteger value)
/*      */   {
/*  495 */     this.transAmt = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTransCurrCd()
/*      */   {
/*  507 */     return this.transCurrCd;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTransCurrCd(String value)
/*      */   {
/*  519 */     this.transCurrCd = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTransProcCd()
/*      */   {
/*  531 */     return this.transProcCd;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTransProcCd(String value)
/*      */   {
/*  543 */     this.transProcCd = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTransId()
/*      */   {
/*  555 */     return this.transId;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTransId(String value)
/*      */   {
/*  567 */     this.transId = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTransAprvCd()
/*      */   {
/*  579 */     return this.transAprvCd;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTransAprvCd(String value)
/*      */   {
/*  591 */     this.transAprvCd = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public PointOfServiceDataType getPointOfServiceData()
/*      */   {
/*  603 */     return this.pointOfServiceData;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setPointOfServiceData(PointOfServiceDataType value)
/*      */   {
/*  615 */     this.pointOfServiceData = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getElecComrceInd()
/*      */   {
/*  627 */     return this.elecComrceInd;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setElecComrceInd(String value)
/*      */   {
/*  639 */     this.elecComrceInd = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getMediaCd()
/*      */   {
/*  651 */     return this.mediaCd;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setMediaCd(String value)
/*      */   {
/*  663 */     this.mediaCd = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getSubmMthdCd()
/*      */   {
/*  675 */     return this.submMthdCd;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSubmMthdCd(String value)
/*      */   {
/*  687 */     this.submMthdCd = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getMerLocId()
/*      */   {
/*  699 */     return this.merLocId;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setMerLocId(String value)
/*      */   {
/*  711 */     this.merLocId = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getMerCtcInfoTxt()
/*      */   {
/*  723 */     return this.merCtcInfoTxt;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setMerCtcInfoTxt(String value)
/*      */   {
/*  735 */     this.merCtcInfoTxt = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTransImageSeqNbr()
/*      */   {
/*  747 */     return this.transImageSeqNbr;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTransImageSeqNbr(String value)
/*      */   {
/*  759 */     this.transImageSeqNbr = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getMatchKeyTypeCd()
/*      */   {
/*  771 */     return this.matchKeyTypeCd;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setMatchKeyTypeCd(String value)
/*      */   {
/*  783 */     this.matchKeyTypeCd = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getMatchKeyId()
/*      */   {
/*  795 */     return this.matchKeyId;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setMatchKeyId(String value)
/*      */   {
/*  807 */     this.matchKeyId = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getDefPaymentPlan()
/*      */   {
/*  819 */     return this.defPaymentPlan;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setDefPaymentPlan(String value)
/*      */   {
/*  831 */     this.defPaymentPlan = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getNumDefPayments()
/*      */   {
/*  843 */     return this.numDefPayments;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setNumDefPayments(String value)
/*      */   {
/*  855 */     this.numDefPayments = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getEMVData()
/*      */   {
/*  867 */     return this.emvData;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setEMVData(String value)
/*      */   {
/*  879 */     this.emvData = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getMerCtgyCd()
/*      */   {
/*  891 */     return this.merCtgyCd;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setMerCtgyCd(String value)
/*      */   {
/*  903 */     this.merCtgyCd = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getSellId()
/*      */   {
/*  915 */     return this.sellId;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setSellId(String value)
/*      */   {
/*  927 */     this.sellId = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public CardTransDetailType getCardTransDetail()
/*      */   {
/*  939 */     return this.cardTransDetail;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setCardTransDetail(CardTransDetailType value)
/*      */   {
/*  951 */     this.cardTransDetail = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getTransAddCd()
/*      */   {
/*  963 */     return this.transAddCd;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTransAddCd(String value)
/*      */   {
/*  975 */     this.transAddCd = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public TAAAirIndusType getTAAAirIndus()
/*      */   {
/*  987 */     return this.taaAirIndus;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTAAAirIndus(TAAAirIndusType value)
/*      */   {
/*  999 */     this.taaAirIndus = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public TAAAutoRentalIndusType getTAAAutoRentalIndus()
/*      */   {
/* 1011 */     return this.taaAutoRentalIndus;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTAAAutoRentalIndus(TAAAutoRentalIndusType value)
/*      */   {
/* 1023 */     this.taaAutoRentalIndus = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public TAACommunServIndusType getTAACommunServIndus()
/*      */   {
/* 1035 */     return this.taaCommunServIndus;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTAACommunServIndus(TAACommunServIndusType value)
/*      */   {
/* 1047 */     this.taaCommunServIndus = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public TAAEntTktIndusType getTAAEntTktIndus()
/*      */   {
/* 1059 */     return this.taaEntTktIndus;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTAAEntTktIndus(TAAEntTktIndusType value)
/*      */   {
/* 1071 */     this.taaEntTktIndus = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public TAAInsrIndusType getTAAInsrIndus()
/*      */   {
/* 1083 */     return this.taaInsrIndus;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTAAInsrIndus(TAAInsrIndusType value)
/*      */   {
/* 1095 */     this.taaInsrIndus = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public TAALdgIndusType getTAALdgIndus()
/*      */   {
/* 1107 */     return this.taaLdgIndus;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTAALdgIndus(TAALdgIndusType value)
/*      */   {
/* 1119 */     this.taaLdgIndus = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public TAARailIndusType getTAARailIndus()
/*      */   {
/* 1131 */     return this.taaRailIndus;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTAARailIndus(TAARailIndusType value)
/*      */   {
/* 1143 */     this.taaRailIndus = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public TAARetailIndusType getTAARetailIndus()
/*      */   {
/* 1155 */     return this.taaRetailIndus;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTAARetailIndus(TAARetailIndusType value)
/*      */   {
/* 1167 */     this.taaRetailIndus = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public TAATravCruiseIndusType getTAATravCruiseIndus()
/*      */   {
/* 1179 */     return this.taaTravCruiseIndus;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTAATravCruiseIndus(TAATravCruiseIndusType value)
/*      */   {
/* 1191 */     this.taaTravCruiseIndus = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public TAACorpPurchSolLvl2Type getTAACorpPurchSolLvl2()
/*      */   {
/* 1203 */     return this.taaCorpPurchSolLvl2;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setTAACorpPurchSolLvl2(TAACorpPurchSolLvl2Type value)
/*      */   {
/* 1215 */     this.taaCorpPurchSolLvl2 = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public InvoiceMsgInfoType getInvoiceMsgInfo()
/*      */   {
/* 1227 */     return this.invoiceMsgInfo;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setInvoiceMsgInfo(InvoiceMsgInfoType value)
/*      */   {
/* 1239 */     this.invoiceMsgInfo = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getBatchImageSeqNbr()
/*      */   {
/* 1251 */     return this.batchImageSeqNbr;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setBatchImageSeqNbr(String value)
/*      */   {
/* 1263 */     this.batchImageSeqNbr = value;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getServAgtMerId()
/*      */   {
/* 1275 */     return this.servAgtMerId;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void setServAgtMerId(String value)
/*      */   {
/* 1287 */     this.servAgtMerId = value;
/*      */   }
/*      */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\xml\datacapture\request\DataCaptureRequest.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */