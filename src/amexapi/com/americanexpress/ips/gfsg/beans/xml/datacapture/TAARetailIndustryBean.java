/*    */ package com.americanexpress.ips.gfsg.beans.xml.datacapture;
/*    */ 
/*    */ import java.util.List;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class TAARetailIndustryBean
/*    */ {
/*    */   private String retailDeptNm;
/*    */   private List<RetailItemInfoBean> retailItemInfoBean;
/*    */   
/*    */   public String getRetailDeptNm()
/*    */   {
/* 24 */     return this.retailDeptNm;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setRetailDeptNm(String retailDeptNm)
/*    */   {
/* 34 */     this.retailDeptNm = retailDeptNm;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public List<RetailItemInfoBean> getRetailItemInfoBean()
/*    */   {
/* 46 */     return this.retailItemInfoBean;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setRetailItemInfoBean(List<RetailItemInfoBean> retailItemInfoBean)
/*    */   {
/* 58 */     this.retailItemInfoBean = retailItemInfoBean;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\xml\datacapture\TAARetailIndustryBean.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */