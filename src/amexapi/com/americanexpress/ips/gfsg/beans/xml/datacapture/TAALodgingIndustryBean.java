/*     */ package com.americanexpress.ips.gfsg.beans.xml.datacapture;
/*     */ 
/*     */ import java.util.List;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class TAALodgingIndustryBean
/*     */ {
/*     */   private String ldgSpecProgCd;
/*     */   private String ldgCheckInDt;
/*     */   private String ldgCheckOutDt;
/*     */   private List<LodgeRoomInformationBean> lodgeRoomInformationBean;
/*     */   private String ldgRentNm;
/*     */   private String ldgFolioNbr;
/*     */   
/*     */   public String getLdgSpecProgCd()
/*     */   {
/*  26 */     return this.ldgSpecProgCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLdgSpecProgCd(String ldgSpecProgCd)
/*     */   {
/*  36 */     this.ldgSpecProgCd = ldgSpecProgCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getLdgCheckInDt()
/*     */   {
/*  46 */     return this.ldgCheckInDt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLdgCheckInDt(String ldgCheckInDt)
/*     */   {
/*  56 */     this.ldgCheckInDt = ldgCheckInDt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getLdgCheckOutDt()
/*     */   {
/*  66 */     return this.ldgCheckOutDt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLdgCheckOutDt(String ldgCheckOutDt)
/*     */   {
/*  76 */     this.ldgCheckOutDt = ldgCheckOutDt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getLdgRentNm()
/*     */   {
/*  87 */     return this.ldgRentNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLdgRentNm(String ldgRentNm)
/*     */   {
/*  97 */     this.ldgRentNm = ldgRentNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getLdgFolioNbr()
/*     */   {
/* 109 */     return this.ldgFolioNbr;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLdgFolioNbr(String ldgFolioNbr)
/*     */   {
/* 121 */     this.ldgFolioNbr = ldgFolioNbr;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<LodgeRoomInformationBean> getLodgeRoomInformationBean()
/*     */   {
/* 131 */     return this.lodgeRoomInformationBean;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setLodgeRoomInformationBean(List<LodgeRoomInformationBean> lodgeRoomInformationBean)
/*     */   {
/* 142 */     this.lodgeRoomInformationBean = lodgeRoomInformationBean;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\xml\datacapture\TAALodgingIndustryBean.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */