/*     */ package com.americanexpress.ips.gfsg.beans.xml.datacapture.request;
/*     */ 
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="TAAAutoRentalIndusType", propOrder={})
/*     */ public class TAAAutoRentalIndusType
/*     */ {
/*     */   @XmlElement(name="AgrNbr", required=true)
/*     */   protected String agrNbr;
/*     */   @XmlElement(name="PickupLocNm")
/*     */   protected String pickupLocNm;
/*     */   @XmlElement(name="PickupCityNm", required=true)
/*     */   protected String pickupCityNm;
/*     */   @XmlElement(name="PickupRgnCd", required=true)
/*     */   protected String pickupRgnCd;
/*     */   @XmlElement(name="PickupCtryCd", required=true)
/*     */   protected String pickupCtryCd;
/*     */   @XmlElement(name="PickupDt", required=true)
/*     */   protected String pickupDt;
/*     */   @XmlElement(name="PickupTm")
/*     */   protected String pickupTm;
/*     */   @XmlElement(name="RtrnCityNm", required=true)
/*     */   protected String rtrnCityNm;
/*     */   @XmlElement(name="RtrnRgnCd", required=true)
/*     */   protected String rtrnRgnCd;
/*     */   @XmlElement(name="RtrnCtryCd", required=true)
/*     */   protected String rtrnCtryCd;
/*     */   @XmlElement(name="RtrnDt", required=true)
/*     */   protected String rtrnDt;
/*     */   @XmlElement(name="RtrnTm")
/*     */   protected String rtrnTm;
/*     */   @XmlElement(name="RenterNm", required=true)
/*     */   protected String renterNm;
/*     */   @XmlElement(name="VehClassId")
/*     */   protected String vehClassId;
/*     */   @XmlElement(name="TravDisCnt")
/*     */   protected String travDisCnt;
/*     */   @XmlElement(name="TravDisUnit")
/*     */   protected String travDisUnit;
/*     */   @XmlElement(name="AuditAdjInd")
/*     */   protected String auditAdjInd;
/*     */   @XmlElement(name="AuditAdjAmt")
/*     */   protected String auditAdjAmt;
/*     */   @XmlElement(name="RtrnLocNm")
/*     */   protected String rtrnLocNm;
/*     */   @XmlElement(name="VehIdNbr")
/*     */   protected String vehIdNbr;
/*     */   @XmlElement(name="DrvIdNbr")
/*     */   protected String drvIdNbr;
/*     */   @XmlElement(name="DrvTaxIdNbr")
/*     */   protected String drvTaxIdNbr;
/*     */   
/*     */   public String getRtrnLocNm()
/*     */   {
/* 188 */     return this.rtrnLocNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setRtrnLocNm(String rtrnLocNm)
/*     */   {
/* 199 */     this.rtrnLocNm = rtrnLocNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getVehIdNbr()
/*     */   {
/* 210 */     return this.vehIdNbr;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setVehIdNbr(String vehIdNbr)
/*     */   {
/* 221 */     this.vehIdNbr = vehIdNbr;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getDrvIdNbr()
/*     */   {
/* 232 */     return this.drvIdNbr;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setDrvIdNbr(String drvIdNbr)
/*     */   {
/* 243 */     this.drvIdNbr = drvIdNbr;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getDrvTaxIdNbr()
/*     */   {
/* 254 */     return this.drvTaxIdNbr;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setDrvTaxIdNbr(String drvTaxIdNbr)
/*     */   {
/* 265 */     this.drvTaxIdNbr = drvTaxIdNbr;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getAgrNbr()
/*     */   {
/* 278 */     return this.agrNbr;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setAgrNbr(String value)
/*     */   {
/* 290 */     this.agrNbr = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getPickupLocNm()
/*     */   {
/* 302 */     return this.pickupLocNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPickupLocNm(String value)
/*     */   {
/* 314 */     this.pickupLocNm = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getPickupCityNm()
/*     */   {
/* 326 */     return this.pickupCityNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPickupCityNm(String value)
/*     */   {
/* 338 */     this.pickupCityNm = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getPickupRgnCd()
/*     */   {
/* 350 */     return this.pickupRgnCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPickupRgnCd(String value)
/*     */   {
/* 362 */     this.pickupRgnCd = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getPickupCtryCd()
/*     */   {
/* 374 */     return this.pickupCtryCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPickupCtryCd(String value)
/*     */   {
/* 386 */     this.pickupCtryCd = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getPickupDt()
/*     */   {
/* 398 */     return this.pickupDt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPickupDt(String value)
/*     */   {
/* 410 */     this.pickupDt = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getPickupTm()
/*     */   {
/* 422 */     return this.pickupTm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPickupTm(String value)
/*     */   {
/* 434 */     this.pickupTm = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getRtrnCityNm()
/*     */   {
/* 446 */     return this.rtrnCityNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setRtrnCityNm(String value)
/*     */   {
/* 458 */     this.rtrnCityNm = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getRtrnRgnCd()
/*     */   {
/* 470 */     return this.rtrnRgnCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setRtrnRgnCd(String value)
/*     */   {
/* 482 */     this.rtrnRgnCd = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getRtrnCtryCd()
/*     */   {
/* 494 */     return this.rtrnCtryCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setRtrnCtryCd(String value)
/*     */   {
/* 506 */     this.rtrnCtryCd = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getRtrnDt()
/*     */   {
/* 518 */     return this.rtrnDt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setRtrnDt(String value)
/*     */   {
/* 530 */     this.rtrnDt = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getRtrnTm()
/*     */   {
/* 542 */     return this.rtrnTm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setRtrnTm(String value)
/*     */   {
/* 554 */     this.rtrnTm = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getRenterNm()
/*     */   {
/* 566 */     return this.renterNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setRenterNm(String value)
/*     */   {
/* 578 */     this.renterNm = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getVehClassId()
/*     */   {
/* 590 */     return this.vehClassId;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setVehClassId(String value)
/*     */   {
/* 602 */     this.vehClassId = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTravDisCnt()
/*     */   {
/* 614 */     return this.travDisCnt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTravDisCnt(String value)
/*     */   {
/* 626 */     this.travDisCnt = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTravDisUnit()
/*     */   {
/* 638 */     return this.travDisUnit;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTravDisUnit(String value)
/*     */   {
/* 650 */     this.travDisUnit = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getAuditAdjInd()
/*     */   {
/* 662 */     return this.auditAdjInd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setAuditAdjInd(String value)
/*     */   {
/* 674 */     this.auditAdjInd = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getAuditAdjAmt()
/*     */   {
/* 686 */     return this.auditAdjAmt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setAuditAdjAmt(String value)
/*     */   {
/* 698 */     this.auditAdjAmt = value;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\xml\datacapture\request\TAAAutoRentalIndusType.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */