/*     */ package com.americanexpress.ips.gfsg.beans.xml.datacapture;
/*     */ 
/*     */ 
/*     */ 
/*     */ public class TAAEntertainmentTktIndustryBean
/*     */ {
/*     */   private String eventNm;
/*     */   
/*     */ 
/*     */   private String eventDt;
/*     */   
/*     */ 
/*     */   private String eventTktPrcAmt;
/*     */   
/*     */ 
/*     */   private String eventTktCnt;
/*     */   
/*     */   private String eventLocNm;
/*     */   
/*     */   private String eventRgnCd;
/*     */   
/*     */   private String eventCtryCd;
/*     */   
/*     */ 
/*     */   public String getEventNm()
/*     */   {
/*  27 */     return this.eventNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setEventNm(String eventNm)
/*     */   {
/*  37 */     this.eventNm = eventNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getEventDt()
/*     */   {
/*  47 */     return this.eventDt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setEventDt(String eventDt)
/*     */   {
/*  57 */     this.eventDt = eventDt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getEventTktPrcAmt()
/*     */   {
/*  67 */     return this.eventTktPrcAmt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setEventTktPrcAmt(String eventTktPrcAmt)
/*     */   {
/*  77 */     this.eventTktPrcAmt = eventTktPrcAmt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getEventTktCnt()
/*     */   {
/*  87 */     return this.eventTktCnt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setEventTktCnt(String eventTktCnt)
/*     */   {
/*  97 */     this.eventTktCnt = eventTktCnt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getEventLocNm()
/*     */   {
/* 107 */     return this.eventLocNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setEventLocNm(String eventLocNm)
/*     */   {
/* 117 */     this.eventLocNm = eventLocNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getEventRgnCd()
/*     */   {
/* 129 */     return this.eventRgnCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setEventRgnCd(String eventRgnCd)
/*     */   {
/* 141 */     this.eventRgnCd = eventRgnCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getEventCtryCd()
/*     */   {
/* 153 */     return this.eventCtryCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setEventCtryCd(String eventCtryCd)
/*     */   {
/* 165 */     this.eventCtryCd = eventCtryCd;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\xml\datacapture\TAAEntertainmentTktIndustryBean.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */