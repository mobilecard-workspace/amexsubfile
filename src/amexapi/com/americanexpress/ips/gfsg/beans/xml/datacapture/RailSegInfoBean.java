/*    */ package com.americanexpress.ips.gfsg.beans.xml.datacapture;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class RailSegInfoBean
/*    */ {
/*    */   private String dprtStnCd;
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   private String dprtDt;
/*    */   
/*    */ 
/*    */ 
/*    */   private String arrStnCd;
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public String getDprtStnCd()
/*    */   {
/* 25 */     return this.dprtStnCd;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setDprtStnCd(String dprtStnCd)
/*    */   {
/* 36 */     this.dprtStnCd = dprtStnCd;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public String getDprtDt()
/*    */   {
/* 47 */     return this.dprtDt;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setDprtDt(String dprtDt)
/*    */   {
/* 58 */     this.dprtDt = dprtDt;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public String getArrStnCd()
/*    */   {
/* 69 */     return this.arrStnCd;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setArrStnCd(String arrStnCd)
/*    */   {
/* 80 */     this.arrStnCd = arrStnCd;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\xml\datacapture\RailSegInfoBean.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */