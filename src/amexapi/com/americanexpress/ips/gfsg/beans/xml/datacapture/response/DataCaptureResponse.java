/*     */ package com.americanexpress.ips.gfsg.beans.xml.datacapture.response;
/*     */ 
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlRootElement;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="", propOrder={"version", "merId", "merTrmnlId", "batchID", "refNumber", "statusCode", "statusText"})
/*     */ @XmlRootElement(name="DataCaptureResponse")
/*     */ public class DataCaptureResponse
/*     */ {
/*     */   @XmlElement(name="Version")
/*     */   protected long version;
/*     */   @XmlElement(name="MerId")
/*     */   protected long merId;
/*     */   @XmlElement(name="MerTrmnlId", required=true)
/*     */   protected String merTrmnlId;
/*     */   @XmlElement(name="BatchID")
/*     */   protected int batchID;
/*     */   @XmlElement(name="RefNumber", required=true)
/*     */   protected String refNumber;
/*     */   @XmlElement(name="StatusCode")
/*     */   protected int statusCode;
/*     */   @XmlElement(name="StatusText", required=true)
/*     */   protected String statusText;
/*     */   
/*     */   public long getVersion()
/*     */   {
/* 121 */     return this.version;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setVersion(long value)
/*     */   {
/* 129 */     this.version = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public long getMerId()
/*     */   {
/* 137 */     return this.merId;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setMerId(long value)
/*     */   {
/* 145 */     this.merId = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getMerTrmnlId()
/*     */   {
/* 157 */     return this.merTrmnlId;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setMerTrmnlId(String value)
/*     */   {
/* 169 */     this.merTrmnlId = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getBatchID()
/*     */   {
/* 177 */     return this.batchID;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setBatchID(int value)
/*     */   {
/* 185 */     this.batchID = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getRefNumber()
/*     */   {
/* 197 */     return this.refNumber;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setRefNumber(String value)
/*     */   {
/* 209 */     this.refNumber = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getStatusCode()
/*     */   {
/* 217 */     return this.statusCode;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setStatusCode(int value)
/*     */   {
/* 225 */     this.statusCode = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getStatusText()
/*     */   {
/* 237 */     return this.statusText;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setStatusText(String value)
/*     */   {
/* 249 */     this.statusText = value;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\xml\datacapture\response\DataCaptureResponse.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */