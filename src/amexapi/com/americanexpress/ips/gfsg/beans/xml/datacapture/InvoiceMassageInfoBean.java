/*    */ package com.americanexpress.ips.gfsg.beans.xml.datacapture;
/*    */ 
/*    */ 
/*    */ 
/*    */ public class InvoiceMassageInfoBean
/*    */ {
/*    */   private String invoiceNumber;
/*    */   
/*    */ 
/*    */   private String releaseNumber;
/*    */   
/*    */ 
/*    */   private String description1;
/*    */   
/*    */ 
/*    */   private String description2;
/*    */   
/*    */   private String description3;
/*    */   
/*    */   private String description4;
/*    */   
/*    */ 
/*    */   public String getInvoiceNumber()
/*    */   {
/* 25 */     return this.invoiceNumber;
/*    */   }
/*    */   
/*    */ 
/*    */   public void setInvoiceNumber(String invoiceNumber)
/*    */   {
/* 31 */     this.invoiceNumber = invoiceNumber;
/*    */   }
/*    */   
/*    */ 
/*    */   public String getReleaseNumber()
/*    */   {
/* 37 */     return this.releaseNumber;
/*    */   }
/*    */   
/*    */ 
/*    */   public void setReleaseNumber(String releaseNumber)
/*    */   {
/* 43 */     this.releaseNumber = releaseNumber;
/*    */   }
/*    */   
/*    */ 
/*    */   public String getDescription1()
/*    */   {
/* 49 */     return this.description1;
/*    */   }
/*    */   
/*    */ 
/*    */   public void setDescription1(String description1)
/*    */   {
/* 55 */     this.description1 = description1;
/*    */   }
/*    */   
/*    */ 
/*    */   public String getDescription2()
/*    */   {
/* 61 */     return this.description2;
/*    */   }
/*    */   
/*    */ 
/*    */   public void setDescription2(String description2)
/*    */   {
/* 67 */     this.description2 = description2;
/*    */   }
/*    */   
/*    */ 
/*    */   public String getDescription3()
/*    */   {
/* 73 */     return this.description3;
/*    */   }
/*    */   
/*    */ 
/*    */   public void setDescription3(String description3)
/*    */   {
/* 79 */     this.description3 = description3;
/*    */   }
/*    */   
/*    */ 
/*    */   public String getDescription4()
/*    */   {
/* 85 */     return this.description4;
/*    */   }
/*    */   
/*    */ 
/*    */   public void setDescription4(String description4)
/*    */   {
/* 91 */     this.description4 = description4;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\xml\datacapture\InvoiceMassageInfoBean.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */