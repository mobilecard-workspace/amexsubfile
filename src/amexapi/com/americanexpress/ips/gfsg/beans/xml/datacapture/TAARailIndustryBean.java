/*     */ package com.americanexpress.ips.gfsg.beans.xml.datacapture;
/*     */ 
/*     */ import java.util.List;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class TAARailIndustryBean
/*     */ {
/*     */   private String travTransTypeCd;
/*     */   private String tktNbr;
/*     */   private String passNm;
/*     */   private String iATACarrierCd;
/*     */   private String tktIssNm;
/*     */   private String tktIssCityNm;
/*     */   private List<RailSegInfoBean> railSegInfoBean;
/*     */   
/*     */   public List<RailSegInfoBean> getRailSegInfoBean()
/*     */   {
/*  31 */     return this.railSegInfoBean;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setRailSegInfoBean(List<RailSegInfoBean> railSegInfoBean)
/*     */   {
/*  43 */     this.railSegInfoBean = railSegInfoBean;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTravTransTypeCd()
/*     */   {
/*  55 */     return this.travTransTypeCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTravTransTypeCd(String travTransTypeCd)
/*     */   {
/*  67 */     this.travTransTypeCd = travTransTypeCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTktNbr()
/*     */   {
/*  77 */     return this.tktNbr;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTktNbr(String tktNbr)
/*     */   {
/*  87 */     this.tktNbr = tktNbr;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getPassNm()
/*     */   {
/*  97 */     return this.passNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPassNm(String passNm)
/*     */   {
/* 107 */     this.passNm = passNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getIATACarrierCd()
/*     */   {
/* 117 */     return this.iATACarrierCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setIATACarrierCd(String carrierCd)
/*     */   {
/* 127 */     this.iATACarrierCd = carrierCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTktIssNm()
/*     */   {
/* 137 */     return this.tktIssNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTktIssNm(String tktIssNm)
/*     */   {
/* 147 */     this.tktIssNm = tktIssNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTktIssCityNm()
/*     */   {
/* 157 */     return this.tktIssCityNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTktIssCityNm(String tktIssCityNm)
/*     */   {
/* 167 */     this.tktIssCityNm = tktIssCityNm;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\xml\datacapture\TAARailIndustryBean.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */