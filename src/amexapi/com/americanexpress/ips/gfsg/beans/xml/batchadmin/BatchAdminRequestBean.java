/*     */ package com.americanexpress.ips.gfsg.beans.xml.batchadmin;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class BatchAdminRequestBean
/*     */ {
/*  16 */   private String version = "12010000";
/*     */   
/*     */   private String merId;
/*     */   
/*     */   private String merTrmnlId;
/*     */   
/*     */   private String batchId;
/*     */   
/*     */   private String batchOperation;
/*     */   
/*     */   private String returnBatchSummary;
/*     */   
/*     */   private String submitterCode;
/*     */   
/*     */   private String batchImageSeqNbr;
/*     */   
/*     */   private String servAgtMerId;
/*     */   
/*     */   private String invoiceFormatType;
/*     */   private String returnInvoiceStatus;
/*     */   private String invoiceNumber;
/*     */   private CardAcceptorDetail cardAcceptorDetail;
/*     */   
/*     */   public String getVersion()
/*     */   {
/*  41 */     return this.version;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setVersion(String version)
/*     */   {
/*  51 */     this.version = version;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getMerId()
/*     */   {
/*  62 */     return this.merId;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setMerId(String merId)
/*     */   {
/*  73 */     this.merId = merId;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getMerTrmnlId()
/*     */   {
/*  84 */     return this.merTrmnlId;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setMerTrmnlId(String merTrmnlId)
/*     */   {
/*  95 */     this.merTrmnlId = merTrmnlId;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getBatchId()
/*     */   {
/* 107 */     return this.batchId;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setBatchId(String batchId)
/*     */   {
/* 119 */     this.batchId = batchId;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getBatchOperation()
/*     */   {
/* 131 */     return this.batchOperation;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setBatchOperation(String batchOperation)
/*     */   {
/* 142 */     this.batchOperation = batchOperation;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getReturnBatchSummary()
/*     */   {
/* 152 */     return this.returnBatchSummary;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setReturnBatchSummary(String returnBatchSummary)
/*     */   {
/* 162 */     this.returnBatchSummary = returnBatchSummary;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getSubmitterCode()
/*     */   {
/* 172 */     return this.submitterCode;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setSubmitterCode(String submitterCode)
/*     */   {
/* 182 */     this.submitterCode = submitterCode;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public CardAcceptorDetail getCardAcceptorDetail()
/*     */   {
/* 192 */     return this.cardAcceptorDetail;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCardAcceptorDetail(CardAcceptorDetail cardAcceptorDetail)
/*     */   {
/* 202 */     this.cardAcceptorDetail = cardAcceptorDetail;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getBatchImageSeqNbr()
/*     */   {
/* 213 */     return this.batchImageSeqNbr;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setBatchImageSeqNbr(String batchImageSeqNbr)
/*     */   {
/* 223 */     this.batchImageSeqNbr = batchImageSeqNbr;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getServAgtMerId()
/*     */   {
/* 233 */     return this.servAgtMerId;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setServAgtMerId(String servAgtMerId)
/*     */   {
/* 243 */     this.servAgtMerId = servAgtMerId;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getInvoiceFormatType()
/*     */   {
/* 253 */     return this.invoiceFormatType;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setInvoiceFormatType(String invoiceFormatType)
/*     */   {
/* 263 */     this.invoiceFormatType = invoiceFormatType;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getReturnInvoiceStatus()
/*     */   {
/* 273 */     return this.returnInvoiceStatus;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setReturnInvoiceStatus(String returnInvoiceStatus)
/*     */   {
/* 283 */     this.returnInvoiceStatus = returnInvoiceStatus;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getInvoiceNumber()
/*     */   {
/* 293 */     return this.invoiceNumber;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setInvoiceNumber(String invoiceNumber)
/*     */   {
/* 303 */     this.invoiceNumber = invoiceNumber;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\xml\batchadmin\BatchAdminRequestBean.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */