/*    */ package com.americanexpress.ips.gfsg.beans.xml.datacapture;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class LodgeRoomInformationBean
/*    */ {
/*    */   private String ldgRmRate;
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   private String nbrOfNgtCnt;
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public String getLdgRmRate()
/*    */   {
/* 21 */     return this.ldgRmRate;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setLdgRmRate(String ldgRmRate)
/*    */   {
/* 31 */     this.ldgRmRate = ldgRmRate;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public String getNbrOfNgtCnt()
/*    */   {
/* 41 */     return this.nbrOfNgtCnt;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setNbrOfNgtCnt(String nbrOfNgtCnt)
/*    */   {
/* 51 */     this.nbrOfNgtCnt = nbrOfNgtCnt;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\xml\datacapture\LodgeRoomInformationBean.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */