/*     */ package com.americanexpress.ips.gfsg.beans.xml.datacapture;
/*     */ 
/*     */ 
/*     */ 
/*     */ public class AirlineSegInfoBean
/*     */ {
/*     */   private String stopoverCd;
/*     */   
/*     */ 
/*     */   private String dprtLocCd;
/*     */   
/*     */ 
/*     */   private String dprtDt;
/*     */   
/*     */ 
/*     */   private String arrLocCd;
/*     */   
/*     */ 
/*     */   private String segIATACarrierCd;
/*     */   
/*     */ 
/*     */   private String fareBasisTxt;
/*     */   
/*     */   private String servClassCd;
/*     */   
/*     */   private String flghtNbr;
/*     */   
/*     */   private String segTotFareAmt;
/*     */   
/*     */ 
/*     */   public String getStopoverCd()
/*     */   {
/*  33 */     return this.stopoverCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setStopoverCd(String stopoverCd)
/*     */   {
/*  45 */     this.stopoverCd = stopoverCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getDprtLocCd()
/*     */   {
/*  57 */     return this.dprtLocCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setDprtLocCd(String dprtLocCd)
/*     */   {
/*  69 */     this.dprtLocCd = dprtLocCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getDprtDt()
/*     */   {
/*  81 */     return this.dprtDt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setDprtDt(String dprtDt)
/*     */   {
/*  93 */     this.dprtDt = dprtDt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getArrLocCd()
/*     */   {
/* 105 */     return this.arrLocCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setArrLocCd(String arrLocCd)
/*     */   {
/* 117 */     this.arrLocCd = arrLocCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getSegIATACarrierCd()
/*     */   {
/* 129 */     return this.segIATACarrierCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setSegIATACarrierCd(String segIATACarrierCd)
/*     */   {
/* 141 */     this.segIATACarrierCd = segIATACarrierCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getFareBasisTxt()
/*     */   {
/* 153 */     return this.fareBasisTxt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setFareBasisTxt(String fareBasisTxt)
/*     */   {
/* 165 */     this.fareBasisTxt = fareBasisTxt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getServClassCd()
/*     */   {
/* 177 */     return this.servClassCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setServClassCd(String servClassCd)
/*     */   {
/* 189 */     this.servClassCd = servClassCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getFlghtNbr()
/*     */   {
/* 201 */     return this.flghtNbr;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setFlghtNbr(String flghtNbr)
/*     */   {
/* 213 */     this.flghtNbr = flghtNbr;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getSegTotFareAmt()
/*     */   {
/* 225 */     return this.segTotFareAmt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setSegTotFareAmt(String segTotFareAmt)
/*     */   {
/* 237 */     this.segTotFareAmt = segTotFareAmt;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\xml\datacapture\AirlineSegInfoBean.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */