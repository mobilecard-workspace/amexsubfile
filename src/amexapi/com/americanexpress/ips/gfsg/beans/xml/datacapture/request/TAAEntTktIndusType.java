/*     */ package com.americanexpress.ips.gfsg.beans.xml.datacapture.request;
/*     */ 
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="TAAEntTktIndusType", propOrder={})
/*     */ public class TAAEntTktIndusType
/*     */ {
/*     */   @XmlElement(name="EventNm")
/*     */   protected String eventNm;
/*     */   @XmlElement(name="EventDt", required=true)
/*     */   protected String eventDt;
/*     */   @XmlElement(name="EventTktPrcAmt")
/*     */   protected String eventTktPrcAmt;
/*     */   @XmlElement(name="EventTktCnt")
/*     */   protected String eventTktCnt;
/*     */   @XmlElement(name="EventLocNm")
/*     */   protected String eventLocNm;
/*     */   @XmlElement(name="EventRgnCd")
/*     */   protected String eventRgnCd;
/*     */   @XmlElement(name="EventCtryCd")
/*     */   protected String eventCtryCd;
/*     */   
/*     */   public String getEventNm()
/*     */   {
/*  92 */     return this.eventNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setEventNm(String value)
/*     */   {
/* 104 */     this.eventNm = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getEventDt()
/*     */   {
/* 116 */     return this.eventDt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setEventDt(String value)
/*     */   {
/* 128 */     this.eventDt = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getEventTktPrcAmt()
/*     */   {
/* 140 */     return this.eventTktPrcAmt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setEventTktPrcAmt(String value)
/*     */   {
/* 152 */     this.eventTktPrcAmt = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getEventTktCnt()
/*     */   {
/* 164 */     return this.eventTktCnt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setEventTktCnt(String value)
/*     */   {
/* 176 */     this.eventTktCnt = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getEventLocNm()
/*     */   {
/* 188 */     return this.eventLocNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setEventLocNm(String value)
/*     */   {
/* 200 */     this.eventLocNm = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getEventRgnCd()
/*     */   {
/* 212 */     return this.eventRgnCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setEventRgnCd(String value)
/*     */   {
/* 224 */     this.eventRgnCd = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getEventCtryCd()
/*     */   {
/* 236 */     return this.eventCtryCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setEventCtryCd(String value)
/*     */   {
/* 248 */     this.eventCtryCd = value;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\xml\datacapture\request\TAAEntTktIndusType.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */