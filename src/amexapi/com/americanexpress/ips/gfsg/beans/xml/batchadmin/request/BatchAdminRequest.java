/*     */ package com.americanexpress.ips.gfsg.beans.xml.batchadmin.request;
/*     */ 
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlRootElement;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="", propOrder={"version", "merId", "merTrmnlId", "batchID", "batchOperation", "returnBatchSummary", "cardAcceptorDetail", "submitterCode", "batchImageSeqNbr", "servAgtMerId", "invoiceFormatType", "returnInvoiceStatus", "invoiceNumber"})
/*     */ @XmlRootElement(name="BatchAdminRequest")
/*     */ public class BatchAdminRequest
/*     */ {
/*     */   @XmlElement(name="Version", required=true)
/*     */   protected String version;
/*     */   @XmlElement(name="MerId", required=true)
/*     */   protected String merId;
/*     */   @XmlElement(name="MerTrmnlId", required=true)
/*     */   protected String merTrmnlId;
/*     */   @XmlElement(name="BatchID", required=true)
/*     */   protected String batchID;
/*     */   @XmlElement(name="BatchOperation", required=true)
/*     */   protected String batchOperation;
/*     */   @XmlElement(name="ReturnBatchSummary")
/*     */   protected String returnBatchSummary;
/*     */   @XmlElement(name="CardAcceptorDetail")
/*     */   protected CardAcceptorDetail cardAcceptorDetail;
/*     */   @XmlElement(name="SubmitterCode")
/*     */   protected String submitterCode;
/*     */   @XmlElement(name="BatchImageSeqNbr")
/*     */   protected String batchImageSeqNbr;
/*     */   @XmlElement(name="ServAgtMerId")
/*     */   protected String servAgtMerId;
/*     */   @XmlElement(name="InvoiceFormatType")
/*     */   protected String invoiceFormatType;
/*     */   @XmlElement(name="ReturnInvoiceStatus")
/*     */   protected String returnInvoiceStatus;
/*     */   @XmlElement(name="InvoiceNumber")
/*     */   protected String invoiceNumber;
/*     */   
/*     */   public String getVersion()
/*     */   {
/* 126 */     return this.version;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setVersion(String value)
/*     */   {
/* 138 */     this.version = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getMerId()
/*     */   {
/* 150 */     return this.merId;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setMerId(String value)
/*     */   {
/* 162 */     this.merId = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getMerTrmnlId()
/*     */   {
/* 174 */     return this.merTrmnlId;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setMerTrmnlId(String value)
/*     */   {
/* 186 */     this.merTrmnlId = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getBatchID()
/*     */   {
/* 198 */     return this.batchID;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setBatchID(String value)
/*     */   {
/* 210 */     this.batchID = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getBatchOperation()
/*     */   {
/* 222 */     return this.batchOperation;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setBatchOperation(String value)
/*     */   {
/* 234 */     this.batchOperation = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getReturnBatchSummary()
/*     */   {
/* 246 */     return this.returnBatchSummary;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setReturnBatchSummary(String value)
/*     */   {
/* 258 */     this.returnBatchSummary = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public CardAcceptorDetail getCardAcceptorDetail()
/*     */   {
/* 270 */     return this.cardAcceptorDetail;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCardAcceptorDetail(CardAcceptorDetail value)
/*     */   {
/* 282 */     this.cardAcceptorDetail = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getSubmitterCode()
/*     */   {
/* 294 */     return this.submitterCode;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setSubmitterCode(String value)
/*     */   {
/* 306 */     this.submitterCode = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getBatchImageSeqNbr()
/*     */   {
/* 318 */     return this.batchImageSeqNbr;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setBatchImageSeqNbr(String value)
/*     */   {
/* 330 */     this.batchImageSeqNbr = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getServAgtMerId()
/*     */   {
/* 342 */     return this.servAgtMerId;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setServAgtMerId(String value)
/*     */   {
/* 354 */     this.servAgtMerId = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getInvoiceFormatType()
/*     */   {
/* 366 */     return this.invoiceFormatType;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setInvoiceFormatType(String value)
/*     */   {
/* 378 */     this.invoiceFormatType = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getReturnInvoiceStatus()
/*     */   {
/* 390 */     return this.returnInvoiceStatus;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setReturnInvoiceStatus(String value)
/*     */   {
/* 402 */     this.returnInvoiceStatus = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getInvoiceNumber()
/*     */   {
/* 414 */     return this.invoiceNumber;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setInvoiceNumber(String value)
/*     */   {
/* 426 */     this.invoiceNumber = value;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\xml\batchadmin\request\BatchAdminRequest.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */