/*     */ package com.americanexpress.ips.gfsg.beans.xml.batchadmin.request;
/*     */ 
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlRootElement;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="", propOrder={"cardAcptNm", "cardAcptStreetNm", "cardAcptCityNm", "cardAcptRgnCd", "cardAcptCtryCd", "cardAcptPostCd"})
/*     */ @XmlRootElement(name="CardAcceptorDetail")
/*     */ public class CardAcceptorDetail
/*     */ {
/*     */   @XmlElement(name="CardAcptNm", required=true)
/*     */   protected String cardAcptNm;
/*     */   @XmlElement(name="CardAcptStreetNm")
/*     */   protected String cardAcptStreetNm;
/*     */   @XmlElement(name="CardAcptCityNm", required=true)
/*     */   protected String cardAcptCityNm;
/*     */   @XmlElement(name="CardAcptRgnCd", required=true)
/*     */   protected String cardAcptRgnCd;
/*     */   @XmlElement(name="CardAcptCtryCd", required=true)
/*     */   protected String cardAcptCtryCd;
/*     */   @XmlElement(name="CardAcptPostCd")
/*     */   protected String cardAcptPostCd;
/*     */   
/*     */   public String getCardAcptNm()
/*     */   {
/*  98 */     return this.cardAcptNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCardAcptNm(String value)
/*     */   {
/* 110 */     this.cardAcptNm = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCardAcptStreetNm()
/*     */   {
/* 122 */     return this.cardAcptStreetNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCardAcptStreetNm(String value)
/*     */   {
/* 134 */     this.cardAcptStreetNm = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCardAcptCityNm()
/*     */   {
/* 146 */     return this.cardAcptCityNm;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCardAcptCityNm(String value)
/*     */   {
/* 158 */     this.cardAcptCityNm = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCardAcptRgnCd()
/*     */   {
/* 170 */     return this.cardAcptRgnCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCardAcptRgnCd(String value)
/*     */   {
/* 182 */     this.cardAcptRgnCd = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCardAcptCtryCd()
/*     */   {
/* 194 */     return this.cardAcptCtryCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCardAcptCtryCd(String value)
/*     */   {
/* 206 */     this.cardAcptCtryCd = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCardAcptPostCd()
/*     */   {
/* 218 */     return this.cardAcptPostCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCardAcptPostCd(String value)
/*     */   {
/* 230 */     this.cardAcptPostCd = value;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\xml\batchadmin\request\CardAcceptorDetail.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */