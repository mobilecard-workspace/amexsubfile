/*    */ package com.americanexpress.ips.gfsg.beans.xml.datacapture;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class RetailItemInfoBean
/*    */ {
/*    */   private String retailItemDesc;
/*    */   
/*    */ 
/*    */ 
/*    */   private String retailItemQty;
/*    */   
/*    */ 
/*    */   private String retailItemAmt;
/*    */   
/*    */ 
/*    */ 
/*    */   public String getRetailItemDesc()
/*    */   {
/* 21 */     return this.retailItemDesc;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setRetailItemDesc(String retailItemDesc)
/*    */   {
/* 32 */     this.retailItemDesc = retailItemDesc;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public String getRetailItemQty()
/*    */   {
/* 43 */     return this.retailItemQty;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setRetailItemQty(String retailItemQty)
/*    */   {
/* 54 */     this.retailItemQty = retailItemQty;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public String getRetailItemAmt()
/*    */   {
/* 65 */     return this.retailItemAmt;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setRetailItemAmt(String retailItemAmt)
/*    */   {
/* 76 */     this.retailItemAmt = retailItemAmt;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\xml\datacapture\RetailItemInfoBean.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */