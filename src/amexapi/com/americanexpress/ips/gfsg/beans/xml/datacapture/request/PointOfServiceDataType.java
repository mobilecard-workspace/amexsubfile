/*     */ package com.americanexpress.ips.gfsg.beans.xml.datacapture.request;
/*     */ 
/*     */ import javax.xml.bind.annotation.XmlAccessType;
/*     */ import javax.xml.bind.annotation.XmlAccessorType;
/*     */ import javax.xml.bind.annotation.XmlElement;
/*     */ import javax.xml.bind.annotation.XmlType;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlAccessorType(XmlAccessType.FIELD)
/*     */ @XmlType(name="PointOfServiceDataType", propOrder={})
/*     */ public class PointOfServiceDataType
/*     */ {
/*     */   @XmlElement(name="CardDataInpCpblCd", required=true)
/*     */   protected String cardDataInpCpblCd;
/*     */   @XmlElement(name="CMAuthnCpblCd", required=true)
/*     */   protected String cmAuthnCpblCd;
/*     */   @XmlElement(name="CardCptrCpblCd", required=true)
/*     */   protected String cardCptrCpblCd;
/*     */   @XmlElement(name="OprEnvirCd", required=true)
/*     */   protected String oprEnvirCd;
/*     */   @XmlElement(name="CMPresentCd", required=true)
/*     */   protected String cmPresentCd;
/*     */   @XmlElement(name="CardPresentCd", required=true)
/*     */   protected String cardPresentCd;
/*     */   @XmlElement(name="CardDataInpModeCd", required=true)
/*     */   protected String cardDataInpModeCd;
/*     */   @XmlElement(name="CMAuthnMthdCd", required=true)
/*     */   protected String cmAuthnMthdCd;
/*     */   @XmlElement(name="CMAuthnEnttyCd", required=true)
/*     */   protected String cmAuthnEnttyCd;
/*     */   @XmlElement(name="CardDataOpCpblCd", required=true)
/*     */   protected String cardDataOpCpblCd;
/*     */   @XmlElement(name="TrmnlOpCpblCd", required=true)
/*     */   protected String trmnlOpCpblCd;
/*     */   @XmlElement(name="PINCptrCpblCd", required=true)
/*     */   protected String pinCptrCpblCd;
/*     */   
/*     */   public String getCardDataInpCpblCd()
/*     */   {
/*  92 */     return this.cardDataInpCpblCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCardDataInpCpblCd(String value)
/*     */   {
/* 104 */     this.cardDataInpCpblCd = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCMAuthnCpblCd()
/*     */   {
/* 116 */     return this.cmAuthnCpblCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCMAuthnCpblCd(String value)
/*     */   {
/* 128 */     this.cmAuthnCpblCd = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCardCptrCpblCd()
/*     */   {
/* 140 */     return this.cardCptrCpblCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCardCptrCpblCd(String value)
/*     */   {
/* 152 */     this.cardCptrCpblCd = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getOprEnvirCd()
/*     */   {
/* 164 */     return this.oprEnvirCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setOprEnvirCd(String value)
/*     */   {
/* 176 */     this.oprEnvirCd = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCMPresentCd()
/*     */   {
/* 188 */     return this.cmPresentCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCMPresentCd(String value)
/*     */   {
/* 200 */     this.cmPresentCd = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCardPresentCd()
/*     */   {
/* 212 */     return this.cardPresentCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCardPresentCd(String value)
/*     */   {
/* 224 */     this.cardPresentCd = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCardDataInpModeCd()
/*     */   {
/* 236 */     return this.cardDataInpModeCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCardDataInpModeCd(String value)
/*     */   {
/* 248 */     this.cardDataInpModeCd = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCMAuthnMthdCd()
/*     */   {
/* 260 */     return this.cmAuthnMthdCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCMAuthnMthdCd(String value)
/*     */   {
/* 272 */     this.cmAuthnMthdCd = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCMAuthnEnttyCd()
/*     */   {
/* 284 */     return this.cmAuthnEnttyCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCMAuthnEnttyCd(String value)
/*     */   {
/* 296 */     this.cmAuthnEnttyCd = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getCardDataOpCpblCd()
/*     */   {
/* 308 */     return this.cardDataOpCpblCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setCardDataOpCpblCd(String value)
/*     */   {
/* 320 */     this.cardDataOpCpblCd = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTrmnlOpCpblCd()
/*     */   {
/* 332 */     return this.trmnlOpCpblCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setTrmnlOpCpblCd(String value)
/*     */   {
/* 344 */     this.trmnlOpCpblCd = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getPINCptrCpblCd()
/*     */   {
/* 356 */     return this.pinCptrCpblCd;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPINCptrCpblCd(String value)
/*     */   {
/* 368 */     this.pinCptrCpblCd = value;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\xml\datacapture\request\PointOfServiceDataType.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */