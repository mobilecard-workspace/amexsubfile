/*     */ package com.americanexpress.ips.gfsg.beans.xml.datacapture.request;
/*     */ 
/*     */ import javax.xml.bind.annotation.XmlRegistry;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @XmlRegistry
/*     */ public class ObjectFactory
/*     */ {
/*     */   public TAAEntTktIndusType createTAAEntTktIndusType()
/*     */   {
/*  43 */     return new TAAEntTktIndusType();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public TAATravCruiseIndusType createTAATravCruiseIndusType()
/*     */   {
/*  51 */     return new TAATravCruiseIndusType();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public TAACommunServIndusType createTAACommunServIndusType()
/*     */   {
/*  59 */     return new TAACommunServIndusType();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public TAAInsrIndusType createTAAInsrIndusType()
/*     */   {
/*  67 */     return new TAAInsrIndusType();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public ChrgItemInfoType createChrgItemInfoType()
/*     */   {
/*  75 */     return new ChrgItemInfoType();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public LdgRoomInfoType createLdgRoomInfoType()
/*     */   {
/*  83 */     return new LdgRoomInfoType();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public TAAAutoRentalIndusType createTAAAutoRentalIndusType()
/*     */   {
/*  91 */     return new TAAAutoRentalIndusType();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public TAARetailIndusType createTAARetailIndusType()
/*     */   {
/*  99 */     return new TAARetailIndusType();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public TravelSegInfoType createTravelSegInfoType()
/*     */   {
/* 107 */     return new TravelSegInfoType();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public AddAmtInfoType createAddAmtInfoType()
/*     */   {
/* 115 */     return new AddAmtInfoType();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public DataCaptureRequest createDataCaptureRequest()
/*     */   {
/* 123 */     return new DataCaptureRequest();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public InvoiceMsgInfoType createInvoiceMsgInfoType()
/*     */   {
/* 131 */     return new InvoiceMsgInfoType();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public CardTransDetailType createCardTransDetailType()
/*     */   {
/* 139 */     return new CardTransDetailType();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public AirlineSegInfoType createAirlineSegInfoType()
/*     */   {
/* 147 */     return new AirlineSegInfoType();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public RailSegInfoType createRailSegInfoType()
/*     */   {
/* 155 */     return new RailSegInfoType();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public DescriptionType createDescriptionType()
/*     */   {
/* 163 */     return new DescriptionType();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public TAACorpPurchSolLvl2Type createTAACorpPurchSolLvl2Type()
/*     */   {
/* 171 */     return new TAACorpPurchSolLvl2Type();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public TAAAirIndusType createTAAAirIndusType()
/*     */   {
/* 179 */     return new TAAAirIndusType();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public RetailItemInfoType createRetailItemInfoType()
/*     */   {
/* 187 */     return new RetailItemInfoType();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public PointOfServiceDataType createPointOfServiceDataType()
/*     */   {
/* 195 */     return new PointOfServiceDataType();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public TAARailIndusType createTAARailIndusType()
/*     */   {
/* 203 */     return new TAARailIndusType();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public TAALdgIndusType createTAALdgIndusType()
/*     */   {
/* 211 */     return new TAALdgIndusType();
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\beans\xml\datacapture\request\ObjectFactory.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */