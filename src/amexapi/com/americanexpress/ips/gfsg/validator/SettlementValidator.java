/*     */ package com.americanexpress.ips.gfsg.validator;
/*     */ 
/*     */ import com.americanexpress.ips.gfsg.beans.ErrorObject;
/*     */ import com.americanexpress.ips.gfsg.beans.SettlementRequestDataObject;
/*     */ import com.americanexpress.ips.gfsg.beans.common.TransactionAdviceBasicBean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.TransactionTBTSpecificBean;
/*     */ import com.americanexpress.ips.gfsg.connection.PropertyReader;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Iterator;
/*     */ import java.util.List;
/*     */ import org.apache.log4j.Logger;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class SettlementValidator
/*     */ {
/*  23 */   private static final Logger LOGGER = Logger.getLogger(SettlementValidator.class);
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static boolean debugFlag;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static List<ErrorObject> validateISOSettlementRecords(SettlementRequestDataObject settlementReqBean)
/*     */     throws Exception
/*     */   {
/*  39 */     if (PropertyReader.getDebuggerFlag().equalsIgnoreCase(
/*  40 */       "ON")) {
/*  41 */       debugFlag = true;
/*  42 */       LOGGER.info("Entered into validateISOSettlementRecords()");
/*     */     }
/*  44 */     List<ErrorObject> validateErrorList = new ArrayList();
/*     */     try
/*     */     {
/*  47 */       if (!settlementReqBean.getTransactionTBTSpecificType().isEmpty())
/*     */       {
/*  49 */         Iterator localIterator1 = settlementReqBean.getTransactionTBTSpecificType().iterator();
/*  48 */         while (localIterator1.hasNext()) {
/*  49 */           TransactionTBTSpecificBean transactionTBTSpecifictType = (TransactionTBTSpecificBean)localIterator1.next();
/*     */           
/*  51 */           if (transactionTBTSpecifictType
/*  52 */             .getTransactionAdviceBasicType() != null)
/*     */           {
/*  54 */             if (!transactionTBTSpecifictType.getTransactionAdviceBasicType().isEmpty())
/*     */             {
/*  56 */               Iterator localIterator2 = transactionTBTSpecifictType.getTransactionAdviceBasicType().iterator();
/*  55 */               while (localIterator2.hasNext()) {
/*  56 */                 TransactionAdviceBasicBean transactionAdviceBasicType = (TransactionAdviceBasicBean)localIterator2.next();
/*     */                 
/*  58 */                 TABRecValidator.validateTABRecordType(
/*  59 */                   transactionAdviceBasicType, 
/*  60 */                   validateErrorList);
/*     */               }
/*     */             }
/*     */           }
/*     */           
/*  65 */           TBTRecValidator.validateTBTRecord(
/*  66 */             transactionTBTSpecifictType, validateErrorList);
/*     */         }
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*  72 */       TFHRecValidator.validateTFHRecord(
/*  73 */         settlementReqBean.getTransactionFileHeaderType(), validateErrorList);
/*     */       
/*     */ 
/*  76 */       TFSRecValidator.validateTFSRecord(settlementReqBean
/*  77 */         .getTransactionFileSummaryType(), settlementReqBean, 
/*  78 */         validateErrorList);
/*     */     }
/*     */     catch (Exception e) {
/*  81 */       if (debugFlag) {
/*  82 */         LOGGER.fatal("Error" + e);
/*     */       }
/*     */     }
/*     */     
/*  86 */     if (debugFlag) {
/*  87 */       LOGGER.info("Exiting from validateISOSettlementRecords()");
/*     */     }
/*     */     
/*  90 */     return validateErrorList;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static List<ErrorObject> validateConnectionInfo(PropertyReader propertyReader)
/*     */   {
/* 105 */     List<ErrorObject> connectionInfoErrorList = new ArrayList();
/*     */     
/*     */ 
/*     */ 
/* 109 */     if (CommonValidator.isNullOrEmpty(PropertyReader.getFtpURL())) {
/* 110 */       ErrorObject connectionErrorObj = new ErrorObject("2", "Invalid FTP URL");
/* 111 */       connectionInfoErrorList.add(connectionErrorObj);
/* 112 */       if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
/* 113 */         LOGGER.error("Invalid FTP URL");
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 118 */     if (CommonValidator.isNullOrEmpty(PropertyReader.getFtpUserID())) {
/* 119 */       ErrorObject connectionErrorObj = new ErrorObject("3", "Invalid FTP User ID");
/* 120 */       connectionInfoErrorList.add(connectionErrorObj);
/* 121 */       if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
/* 122 */         LOGGER.error("Invalid FTP User ID");
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 127 */     if (CommonValidator.isNullOrEmpty(PropertyReader.getFtpPassword())) {
/* 128 */       ErrorObject connectionErrorObj = new ErrorObject("4", "Invalid FTP Password");
/* 129 */       connectionInfoErrorList.add(connectionErrorObj);
/* 130 */       if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
/* 131 */         LOGGER.error("Invalid FTP Password");
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/* 137 */     if ((CommonValidator.isNullOrEmpty(PropertyReader.getFileStorage())) || ((!PropertyReader.getFileStorage().equalsIgnoreCase("ON")) && (!PropertyReader.getFileStorage().equalsIgnoreCase("OFF")))) {
/* 138 */       ErrorObject connectionErrorObj = new ErrorObject("5", "Invalid File Storage Flag");
/* 139 */       connectionInfoErrorList.add(connectionErrorObj);
/* 140 */       if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
/* 141 */         LOGGER.error("Invalid File Storage Flag");
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 146 */     if ((!CommonValidator.isNullOrEmpty(PropertyReader.getFileStorage())) && (!PropertyReader.getFileStorage().equalsIgnoreCase("ON"))) {
/* 147 */       if (CommonValidator.isNullOrEmpty(PropertyReader.getFtpLocalUploadFilePath())) {
/* 148 */         ErrorObject connectionErrorObj = new ErrorObject("6", "Invalid FTP Local Upload File Path");
/* 149 */         connectionInfoErrorList.add(connectionErrorObj);
/* 150 */         if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
/* 151 */           LOGGER.error("Invalid FTP Local Upload File Path");
/*     */         }
/*     */       }
/*     */       
/*     */ 
/* 156 */       if (CommonValidator.isNullOrEmpty(PropertyReader.getFtpLocalDownloadFilePath())) {
/* 157 */         ErrorObject connectionErrorObj = new ErrorObject("7", "Invalid FTP Local Download File Path");
/* 158 */         connectionInfoErrorList.add(connectionErrorObj);
/* 159 */         if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
/* 160 */           LOGGER.error("Invalid FTP Local Download File Path");
/*     */         }
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 166 */     if (CommonValidator.isNullOrEmpty(PropertyReader.getFtpUploadFileName())) {
/* 167 */       ErrorObject connectionErrorObj = new ErrorObject("8", "Invalid FTP Upload File Name");
/* 168 */       connectionInfoErrorList.add(connectionErrorObj);
/* 169 */       if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
/* 170 */         LOGGER.error("Invalid FTP Upload File Name");
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 175 */     if (CommonValidator.isNullOrEmpty(PropertyReader.getFtpDownloadAcknowledgeFileName())) {
/* 176 */       ErrorObject connectionErrorObj = new ErrorObject("9", "Invalid FTP Download Acknowledge File Name");
/* 177 */       connectionInfoErrorList.add(connectionErrorObj);
/* 178 */       if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
/* 179 */         LOGGER.error("Invalid FTP Download Acknowledge File Name");
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 184 */     if (CommonValidator.isNullOrEmpty(PropertyReader.getFtpDownloadPrintedFileName())) {
/* 185 */       ErrorObject connectionErrorObj = new ErrorObject("10", "Invalid FTP Download Printed File Name");
/* 186 */       connectionInfoErrorList.add(connectionErrorObj);
/* 187 */       if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
/* 188 */         LOGGER.error("Invalid FTP Download Printed File Name");
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 193 */     if (CommonValidator.isNullOrEmpty(PropertyReader.getFtpDownloadRawFileName())) {
/* 194 */       ErrorObject connectionErrorObj = new ErrorObject("11", "Invalid FTP Download Raw File Name");
/* 195 */       connectionInfoErrorList.add(connectionErrorObj);
/* 196 */       if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
/* 197 */         LOGGER.error("Invalid FTP Download Raw File Name");
/*     */       }
/*     */     }
/*     */     
/* 201 */     return connectionInfoErrorList;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\validator\SettlementValidator.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */