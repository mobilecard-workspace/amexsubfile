/*      */ package com.americanexpress.ips.gfsg.validator;
/*      */ 
/*      */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.DataCaptureRequestBean;
/*      */ import com.americanexpress.ips.gfsg.enumerations.AddAmtTypeCd;
/*      */ import com.americanexpress.ips.gfsg.enumerations.AddendaTypeCode;
/*      */ import com.americanexpress.ips.gfsg.enumerations.AuditAdjInd;
/*      */ import com.americanexpress.ips.gfsg.enumerations.AutoRentalDistanceUnitOfMeasure;
/*      */ import com.americanexpress.ips.gfsg.enumerations.BatchOperation;
/*      */ import com.americanexpress.ips.gfsg.enumerations.CallTypeCd;
/*      */ import com.americanexpress.ips.gfsg.enumerations.CommunicationsCallTypeCode;
/*      */ import com.americanexpress.ips.gfsg.enumerations.CountryCode;
/*      */ import com.americanexpress.ips.gfsg.enumerations.CurrencyCode;
/*      */ import com.americanexpress.ips.gfsg.enumerations.CurrencyCodeAndAmount;
/*      */ import com.americanexpress.ips.gfsg.enumerations.ElectronicCommerceIndicator;
/*      */ import com.americanexpress.ips.gfsg.enumerations.FormatCode;
/*      */ import com.americanexpress.ips.gfsg.enumerations.InvoiceFormatType;
/*      */ import com.americanexpress.ips.gfsg.enumerations.LdgSpecProgCd;
/*      */ import com.americanexpress.ips.gfsg.enumerations.MediaCode;
/*      */ import com.americanexpress.ips.gfsg.enumerations.ProcessingCode;
/*      */ import com.americanexpress.ips.gfsg.enumerations.ProhibitedCountryCodes;
/*      */ import com.americanexpress.ips.gfsg.enumerations.RateClassCd;
/*      */ import com.americanexpress.ips.gfsg.enumerations.RegionCode1;
/*      */ import com.americanexpress.ips.gfsg.enumerations.RegionCode2;
/*      */ import com.americanexpress.ips.gfsg.enumerations.RegionCode3;
/*      */ import com.americanexpress.ips.gfsg.enumerations.ReturnBatchOperationSummary;
/*      */ import com.americanexpress.ips.gfsg.enumerations.ReturnInvoiceStatus;
/*      */ import com.americanexpress.ips.gfsg.enumerations.StopOverIndicator;
/*      */ import com.americanexpress.ips.gfsg.enumerations.SubmissionMethod;
/*      */ import com.americanexpress.ips.gfsg.enumerations.TaxTypeCd;
/*      */ import com.americanexpress.ips.gfsg.enumerations.TransProcCd;
/*      */ import com.americanexpress.ips.gfsg.enumerations.TravDisUnit;
/*      */ import com.americanexpress.ips.gfsg.enumerations.TravPackTypeCd;
/*      */ import com.americanexpress.ips.gfsg.enumerations.TravTransTypeCd;
/*      */ import com.americanexpress.ips.gfsg.enumerations.VehicleClassCodes;
/*      */ import com.americanexpress.ips.gfsg.enumerations.xml.XmlCountryCode;
/*      */ import com.americanexpress.ips.gfsg.enumerations.xml.XmlCurrencyCode;
/*      */ import java.util.Calendar;
/*      */ import java.util.regex.Matcher;
/*      */ import java.util.regex.Pattern;
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ public class CommonValidator
/*      */ {
/*      */   public static String validateLength(String value, int maxLength, int minLength)
/*      */   {
/*   69 */     String reqLength = "";
/*   70 */     if (value.length() < minLength) {
/*   71 */       reqLength = "lessThanMin";
/*   72 */     } else if (maxLength < value.length()) {
/*   73 */       reqLength = "greaterThanMax";
/*      */     } else {
/*   75 */       reqLength = "equal";
/*      */     }
/*   77 */     return reqLength;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static boolean validateData(String value, String expression)
/*      */   {
/*   90 */     boolean flag = validateCharacters(value, expression);
/*   91 */     return flag;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static boolean validateCharacters(String value, String expression)
/*      */   {
/*  102 */     boolean isValid = false;
/*  103 */     CharSequence inputStr = value;
/*      */     
/*  105 */     Pattern pattern = Pattern.compile(expression);
/*      */     
/*  107 */     Matcher matcher = pattern.matcher(inputStr);
/*      */     
/*  109 */     if (matcher.matches()) {
/*  110 */       isValid = true;
/*      */     }
/*  112 */     return isValid;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static boolean isNullOrEmpty(String token)
/*      */   {
/*  122 */     boolean isNull = false;
/*  123 */     if ((token == null) || ("".equals(token))) {
/*  124 */       isNull = true;
/*      */     }
/*  126 */     return isNull;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static String validateDate(String fileCreationDate)
/*      */   {
/*  140 */     String status = "";
/*  141 */     int year = Integer.parseInt(fileCreationDate.substring(0, 4));
/*  142 */     int month = Integer.parseInt(fileCreationDate.substring(4, 6)) - 1;
/*  143 */     int day = Integer.parseInt(fileCreationDate.substring(6));
/*  144 */     Calendar enteredDate = Calendar.getInstance();
/*  145 */     enteredDate.set(year, month, day);
/*  146 */     Calendar currentDate = Calendar.getInstance();
/*  147 */     Calendar pastDate = Calendar.getInstance();
/*  148 */     pastDate.set(5, 
/*  149 */       currentDate.get(5) - 364);
/*      */     
/*  151 */     if (enteredDate.before(pastDate)) {
/*  152 */       status = "expired";
/*  153 */     } else if (enteredDate.after(currentDate)) {
/*  154 */       status = "futureDate";
/*      */     }
/*  156 */     return status;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static boolean isValidDate(String inDate, String dateFormat)
/*      */   {
/*  168 */     boolean validDate = true;
/*  169 */     String year = "0";
/*      */     
/*  171 */     String day = "0";
/*      */     
/*  173 */     if (dateFormat.equalsIgnoreCase("YYMM")) {
/*  174 */       String month = inDate.substring(2);
/*      */       
/*  176 */       if (!validateMonth(month)) {
/*  177 */         validDate = false;
/*      */       }
/*      */     } else {
/*  180 */       year = inDate.substring(0, 4);
/*  181 */       String month = inDate.substring(4, 6);
/*  182 */       day = inDate.substring(6, 8);
/*  183 */       if ((!validateMonth(month)) || (!validateDay(year, month, day))) {
/*  184 */         validDate = false;
/*      */       }
/*      */     }
/*  187 */     return validDate;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static boolean validateMonth(String month)
/*      */   {
/*  198 */     boolean isValidMonth = true;
/*      */     
/*  200 */     if ((Integer.parseInt(month) <= 0) || (Integer.parseInt(month) > 12)) {
/*  201 */       isValidMonth = false;
/*      */     }
/*  203 */     return isValidMonth;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static boolean validateDay(String year, String month, String day)
/*      */   {
/*  217 */     boolean isvalidaDay = true;
/*      */     
/*  219 */     if ((Integer.parseInt(month) == 2) && (Integer.parseInt(year) % 4 == 0) && 
/*  220 */       (Integer.parseInt(day) > 29))
/*      */     {
/*  222 */       isvalidaDay = false;
/*  223 */     } else if ((Integer.parseInt(month) == 2) && 
/*  224 */       (Integer.parseInt(year) % 4 != 0) && 
/*  225 */       (Integer.parseInt(day) > 28))
/*      */     {
/*  227 */       isvalidaDay = false;
/*      */     }
/*  229 */     else if (((Integer.parseInt(month) == 4) || (Integer.parseInt(month) == 6) || 
/*  230 */       (Integer.parseInt(month) == 9) || (Integer.parseInt(month) == 11)) && 
/*  231 */       (Integer.parseInt(day) > 30))
/*      */     {
/*  233 */       isvalidaDay = false;
/*  234 */     } else if (Integer.parseInt(day) > 31)
/*      */     {
/*  236 */       isvalidaDay = false;
/*      */     }
/*  238 */     return isvalidaDay;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static boolean validateTime(String time)
/*      */   {
/*  250 */     boolean status = true;
/*  251 */     if (time.length() == 6)
/*      */     {
/*  253 */       if (Integer.parseInt(time) >= 235960)
/*      */       {
/*  255 */         status = false;
/*      */       }
/*      */     } else {
/*  258 */       status = false;
/*      */     }
/*  260 */     return status;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static boolean isValidStopOverIndicator(String stopOverIndicator)
/*      */   {
/*  271 */     boolean isValidStopOverIndicator = false;
/*      */     StopOverIndicator[] arrayOfStopOverIndicator;
/*  273 */     int j = (arrayOfStopOverIndicator = StopOverIndicator.values()).length; for (int i = 0; i < j; i++) {
/*  273 */       StopOverIndicator stopOverIndicatorvalues = arrayOfStopOverIndicator[i];
/*  274 */       if (stopOverIndicator.equalsIgnoreCase(
/*  275 */         stopOverIndicatorvalues.getStopOverIndicator())) {
/*  276 */         isValidStopOverIndicator = true;
/*  277 */         break;
/*      */       }
/*      */     }
/*  280 */     return isValidStopOverIndicator;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static boolean isValidFormatCode(String formatCode)
/*      */   {
/*  291 */     boolean isValidFormatCode = false;
/*  292 */     FormatCode[] arrayOfFormatCode; int j = (arrayOfFormatCode = FormatCode.values()).length; for (int i = 0; i < j; i++) { FormatCode formatCodevalues = arrayOfFormatCode[i];
/*  293 */       if (formatCode.equalsIgnoreCase(formatCodevalues.getFormatCode())) {
/*  294 */         isValidFormatCode = true;
/*  295 */         break;
/*      */       }
/*      */     }
/*  298 */     return isValidFormatCode;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static boolean isValidMediaCode(String mediaCode)
/*      */   {
/*  309 */     boolean isValidMediaCode = false;
/*  310 */     MediaCode[] arrayOfMediaCode; int j = (arrayOfMediaCode = MediaCode.values()).length; for (int i = 0; i < j; i++) { MediaCode mediaCodes = arrayOfMediaCode[i];
/*  311 */       if (mediaCode.equalsIgnoreCase(mediaCodes.getMediaCode())) {
/*  312 */         isValidMediaCode = true;
/*  313 */         break;
/*      */       }
/*      */     }
/*      */     
/*  317 */     return isValidMediaCode;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static boolean isValidSubCode(String subCode)
/*      */   {
/*  328 */     boolean isValidCode = false;
/*  329 */     SubmissionMethod[] arrayOfSubmissionMethod; int j = (arrayOfSubmissionMethod = SubmissionMethod.values()).length; for (int i = 0; i < j; i++) { SubmissionMethod subCodes = arrayOfSubmissionMethod[i];
/*  330 */       if (subCode.equalsIgnoreCase(subCodes.getSubmissionMethod())) {
/*  331 */         isValidCode = true;
/*  332 */         break;
/*      */       }
/*      */     }
/*      */     
/*  336 */     return isValidCode;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static boolean isValidProcessingCode(String processingCode)
/*      */   {
/*  347 */     boolean isValidProcessingCode = false;
/*  348 */     ProcessingCode[] arrayOfProcessingCode; int j = (arrayOfProcessingCode = ProcessingCode.values()).length; for (int i = 0; i < j; i++) { ProcessingCode posCodes = arrayOfProcessingCode[i];
/*  349 */       if (processingCode.equalsIgnoreCase(posCodes.getProcessingCode())) {
/*  350 */         isValidProcessingCode = true;
/*  351 */         break;
/*      */       }
/*      */     }
/*      */     
/*  355 */     return isValidProcessingCode;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static boolean isValidAddendaTypeCode(String addendaTypeCode)
/*      */   {
/*  366 */     boolean isValidCode = false;
/*  367 */     AddendaTypeCode[] arrayOfAddendaTypeCode; int j = (arrayOfAddendaTypeCode = AddendaTypeCode.values()).length; for (int i = 0; i < j; i++) { AddendaTypeCode addCodes = arrayOfAddendaTypeCode[i];
/*  368 */       if (addendaTypeCode.equalsIgnoreCase(addCodes.getAddendaTypeCode())) {
/*  369 */         isValidCode = true;
/*  370 */         break;
/*      */       }
/*      */     }
/*  373 */     return isValidCode;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static boolean isValidCommunicationsCallTypeCode(String communicationsCallTypeCode)
/*      */   {
/*  385 */     boolean isValidCode = false;
/*      */     CommunicationsCallTypeCode[] arrayOfCommunicationsCallTypeCode;
/*  387 */     int j = (arrayOfCommunicationsCallTypeCode = CommunicationsCallTypeCode.values()).length; for (int i = 0; i < j; i++) {
/*  387 */       CommunicationsCallTypeCode communicationsCallTypeCodes = arrayOfCommunicationsCallTypeCode[i];
/*      */       
/*  389 */       if (communicationsCallTypeCode.equalsIgnoreCase(communicationsCallTypeCodes
/*  390 */         .getCommunicationsCallTypeCode())) {
/*  391 */         isValidCode = true;
/*  392 */         break;
/*      */       }
/*      */     }
/*  395 */     return isValidCode;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static boolean isValidModulusNineCheck(String merchantId)
/*      */   {
/*  406 */     boolean isValid = true;
/*  407 */     if (!isNullOrEmpty(merchantId)) {
/*  408 */       isValid = CheckDigitValidator.modulusNineCheck(merchantId);
/*      */     }
/*  410 */     return isValid;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static boolean isValidBatchOperation(String batchOperation)
/*      */   {
/*  422 */     boolean isValidBatchOperation = false;
/*  423 */     BatchOperation[] arrayOfBatchOperation; int j = (arrayOfBatchOperation = BatchOperation.values()).length; for (int i = 0; i < j; i++) { BatchOperation batchOperations = arrayOfBatchOperation[i];
/*      */       
/*  425 */       if (batchOperation.equalsIgnoreCase(
/*  426 */         batchOperations.getBatchOperation()))
/*      */       {
/*  428 */         isValidBatchOperation = true;
/*  429 */         break;
/*      */       }
/*      */     }
/*  432 */     return isValidBatchOperation;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static boolean isValidReturnBatchOperationSummary(String returnBatchOperationSummary)
/*      */   {
/*  445 */     boolean isValidReturnBatchOperationSummary = false;
/*      */     ReturnBatchOperationSummary[] arrayOfReturnBatchOperationSummary;
/*  447 */     int j = (arrayOfReturnBatchOperationSummary = ReturnBatchOperationSummary.values()).length; for (int i = 0; i < j; i++) {
/*  447 */       ReturnBatchOperationSummary returnBatchSummary = arrayOfReturnBatchOperationSummary[i];
/*      */       
/*  449 */       if (returnBatchOperationSummary.equalsIgnoreCase(
/*  450 */         returnBatchSummary.getReturnBatchOperationSummary()))
/*      */       {
/*  452 */         isValidReturnBatchOperationSummary = true;
/*  453 */         break;
/*      */       }
/*      */     }
/*  456 */     return isValidReturnBatchOperationSummary;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static boolean isValidCountryCode(String countryCode)
/*      */   {
/*  468 */     boolean isValidCountryCode = false;
/*  469 */     CountryCode[] arrayOfCountryCode; int j = (arrayOfCountryCode = CountryCode.values()).length; for (int i = 0; i < j; i++) { CountryCode countryCodes = arrayOfCountryCode[i];
/*      */       
/*  471 */       if (countryCode.equalsIgnoreCase(countryCodes.getCountryCode()))
/*      */       {
/*  473 */         isValidCountryCode = true;
/*  474 */         break;
/*      */       }
/*      */     }
/*  477 */     return isValidCountryCode;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static boolean isValidLdgSpecProgCd(String ldgSpecProgCd)
/*      */   {
/*  489 */     boolean isValidLdgSpecProgCd = false;
/*  490 */     LdgSpecProgCd[] arrayOfLdgSpecProgCd; int j = (arrayOfLdgSpecProgCd = LdgSpecProgCd.values()).length; for (int i = 0; i < j; i++) { LdgSpecProgCd ldgSpecProgCds = arrayOfLdgSpecProgCd[i];
/*      */       
/*  492 */       if (ldgSpecProgCd.equalsIgnoreCase(
/*  493 */         ldgSpecProgCds.getLdgSpecProgCd()))
/*      */       {
/*  495 */         isValidLdgSpecProgCd = true;
/*  496 */         break;
/*      */       }
/*      */     }
/*  499 */     return isValidLdgSpecProgCd;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static boolean isValidTravTransTypeCd(String travTransTypeCd)
/*      */   {
/*  510 */     boolean isValidTravTransTypeCd = false;
/*  511 */     TravTransTypeCd[] arrayOfTravTransTypeCd; int j = (arrayOfTravTransTypeCd = TravTransTypeCd.values()).length; for (int i = 0; i < j; i++) { TravTransTypeCd travTransTypeCds = arrayOfTravTransTypeCd[i];
/*  512 */       if (travTransTypeCd.equalsIgnoreCase(
/*  513 */         travTransTypeCds.getTravTransTypeCd())) {
/*  514 */         isValidTravTransTypeCd = true;
/*  515 */         break;
/*      */       }
/*      */     }
/*  518 */     return isValidTravTransTypeCd;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static boolean isVehClassId(String vehClassId)
/*      */   {
/*  530 */     boolean isVehClassId = false;
/*  531 */     VehicleClassCodes[] arrayOfVehicleClassCodes; int j = (arrayOfVehicleClassCodes = VehicleClassCodes.values()).length; for (int i = 0; i < j; i++) { VehicleClassCodes vehicleClassCodes = arrayOfVehicleClassCodes[i];
/*  532 */       if (vehClassId.equalsIgnoreCase(
/*  533 */         vehicleClassCodes.getVehicleClassCodes())) {
/*  534 */         isVehClassId = true;
/*  535 */         break;
/*      */       }
/*      */     }
/*  538 */     return isVehClassId;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static boolean isValidTravDisUnit(String travDisUnit)
/*      */   {
/*  551 */     boolean isValidTravDisUnit = false;
/*  552 */     TravDisUnit[] arrayOfTravDisUnit; int j = (arrayOfTravDisUnit = TravDisUnit.values()).length; for (int i = 0; i < j; i++) { TravDisUnit travDisUnits = arrayOfTravDisUnit[i];
/*      */       
/*  554 */       if (travDisUnit.equalsIgnoreCase(travDisUnits.getTravDisUnit()))
/*      */       {
/*  556 */         isValidTravDisUnit = true;
/*  557 */         break;
/*      */       }
/*      */     }
/*  560 */     return isValidTravDisUnit;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static boolean isValidAuditAdjInd(String auditAdjInd)
/*      */   {
/*  573 */     boolean isValidAuditAdjInd = false;
/*  574 */     AuditAdjInd[] arrayOfAuditAdjInd; int j = (arrayOfAuditAdjInd = AuditAdjInd.values()).length; for (int i = 0; i < j; i++) { AuditAdjInd auditAdjInds = arrayOfAuditAdjInd[i];
/*      */       
/*  576 */       if (auditAdjInd.equalsIgnoreCase(auditAdjInds.getAuditAdjInd()))
/*      */       {
/*  578 */         isValidAuditAdjInd = true;
/*  579 */         break;
/*      */       }
/*      */     }
/*  582 */     return isValidAuditAdjInd;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static boolean isValidCallTypeCd(String callTypeCd)
/*      */   {
/*  595 */     boolean isValidCallTypeCd = false;
/*  596 */     CallTypeCd[] arrayOfCallTypeCd; int j = (arrayOfCallTypeCd = CallTypeCd.values()).length; for (int i = 0; i < j; i++) { CallTypeCd callTypeCds = arrayOfCallTypeCd[i];
/*      */       
/*  598 */       if (callTypeCd.equalsIgnoreCase(callTypeCds.getCallTypeCd()))
/*      */       {
/*  600 */         isValidCallTypeCd = true;
/*  601 */         break;
/*      */       }
/*      */     }
/*  604 */     return isValidCallTypeCd;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static boolean isValidRateClassCd(String rateClassCd)
/*      */   {
/*  617 */     boolean isValidRateClassCd = false;
/*  618 */     RateClassCd[] arrayOfRateClassCd; int j = (arrayOfRateClassCd = RateClassCd.values()).length; for (int i = 0; i < j; i++) { RateClassCd rateClassCds = arrayOfRateClassCd[i];
/*      */       
/*  620 */       if (rateClassCd.equalsIgnoreCase(rateClassCds.getRateClassCd()))
/*      */       {
/*  622 */         isValidRateClassCd = true;
/*  623 */         break;
/*      */       }
/*      */     }
/*      */     
/*  627 */     return isValidRateClassCd;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static boolean isValidTravPackTypeCd(String travPackTypeCd)
/*      */   {
/*  640 */     boolean isValidTravPackTypeCd = false;
/*  641 */     TravPackTypeCd[] arrayOfTravPackTypeCd; int j = (arrayOfTravPackTypeCd = TravPackTypeCd.values()).length; for (int i = 0; i < j; i++) { TravPackTypeCd travPackTypeCds = arrayOfTravPackTypeCd[i];
/*      */       
/*  643 */       if (travPackTypeCd.equalsIgnoreCase(
/*  644 */         travPackTypeCds.getTravPackTypeCd()))
/*      */       {
/*  646 */         isValidTravPackTypeCd = true;
/*  647 */         break;
/*      */       }
/*      */     }
/*  650 */     return isValidTravPackTypeCd;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static boolean isAutoRentalDistanceUnitOfMessure(String autoRentalDistanceUnitOfMeasure)
/*      */   {
/*  662 */     boolean isValidCode = false;
/*      */     AutoRentalDistanceUnitOfMeasure[] arrayOfAutoRentalDistanceUnitOfMeasure;
/*  664 */     int j = (arrayOfAutoRentalDistanceUnitOfMeasure = AutoRentalDistanceUnitOfMeasure.values()).length; for (int i = 0; i < j; i++) {
/*  664 */       AutoRentalDistanceUnitOfMeasure autoRentalDistanceUnitOfMessures = arrayOfAutoRentalDistanceUnitOfMeasure[i];
/*      */       
/*  666 */       if (autoRentalDistanceUnitOfMeasure.equalsIgnoreCase(autoRentalDistanceUnitOfMessures
/*  667 */         .getAutoRentalDistanceUnitOfMeasure())) {
/*  668 */         isValidCode = true;
/*  669 */         break;
/*      */       }
/*      */     }
/*  672 */     return isValidCode;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static boolean isValidTaxTypeCd(String taxTypeCd)
/*      */   {
/*  685 */     boolean isValidTaxTypeCd = false;
/*  686 */     TaxTypeCd[] arrayOfTaxTypeCd; int j = (arrayOfTaxTypeCd = TaxTypeCd.values()).length; for (int i = 0; i < j; i++) { TaxTypeCd taxTypeCds = arrayOfTaxTypeCd[i];
/*      */       
/*  688 */       if (taxTypeCd.equalsIgnoreCase(taxTypeCds.getTaxTypeCd()))
/*      */       {
/*  690 */         isValidTaxTypeCd = true;
/*  691 */         break;
/*      */       }
/*      */     }
/*  694 */     return isValidTaxTypeCd;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static boolean isValidInvoiceFormatType(String invoiceFormatType)
/*      */   {
/*  707 */     boolean isValidInvoiceFormatType = false;
/*  708 */     InvoiceFormatType[] arrayOfInvoiceFormatType; int j = (arrayOfInvoiceFormatType = InvoiceFormatType.values()).length; for (int i = 0; i < j; i++) { InvoiceFormatType invoiceFormatTypes = arrayOfInvoiceFormatType[i];
/*      */       
/*  710 */       if (invoiceFormatType.equalsIgnoreCase(
/*  711 */         invoiceFormatTypes.getInvoiceFormatType()))
/*      */       {
/*  713 */         isValidInvoiceFormatType = true;
/*  714 */         break;
/*      */       }
/*      */     }
/*  717 */     return isValidInvoiceFormatType;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static boolean isValidReturnInvoiceStatus(String returnInvoiceStatus)
/*      */   {
/*  730 */     boolean isValidReturnInvoiceStatus = false;
/*      */     ReturnInvoiceStatus[] arrayOfReturnInvoiceStatus;
/*  732 */     int j = (arrayOfReturnInvoiceStatus = ReturnInvoiceStatus.values()).length; for (int i = 0; i < j; i++) {
/*  732 */       ReturnInvoiceStatus returnInvoiceStatusValue = arrayOfReturnInvoiceStatus[i];
/*      */       
/*  734 */       if (returnInvoiceStatus.equalsIgnoreCase(
/*  735 */         returnInvoiceStatusValue.getReturnInvoiceStatus()))
/*      */       {
/*  737 */         isValidReturnInvoiceStatus = true;
/*  738 */         break;
/*      */       }
/*      */     }
/*  741 */     return isValidReturnInvoiceStatus;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static boolean isValidElecComrceInd(String elecComrceInd)
/*      */   {
/*  753 */     boolean isValidElecComrceInd = false;
/*  754 */     ElectronicCommerceIndicator[] arrayOfElectronicCommerceIndicator; int j = (arrayOfElectronicCommerceIndicator = ElectronicCommerceIndicator.values()).length; for (int i = 0; i < j; i++) { ElectronicCommerceIndicator elecComrceInds = arrayOfElectronicCommerceIndicator[i];
/*      */       
/*  756 */       if (elecComrceInd.equalsIgnoreCase(
/*  757 */         elecComrceInds.getElectronicCommerceIndicator()))
/*      */       {
/*  759 */         isValidElecComrceInd = true;
/*  760 */         break;
/*      */       }
/*      */     }
/*  763 */     return isValidElecComrceInd;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static boolean isValidAmount(String countryCode, String amount)
/*      */   {
/*  776 */     boolean isValid = false;
/*      */     CurrencyCodeAndAmount[] arrayOfCurrencyCodeAndAmount;
/*  778 */     int j = (arrayOfCurrencyCodeAndAmount = CurrencyCodeAndAmount.values()).length; for (int i = 0; i < j; i++) {
/*  778 */       CurrencyCodeAndAmount curCodesAndAmounts = arrayOfCurrencyCodeAndAmount[i];
/*      */       
/*  780 */       if (countryCode.equalsIgnoreCase(
/*  781 */         curCodesAndAmounts.getCurrencyCode()))
/*  782 */         if (compareAmounts(amount, curCodesAndAmounts.getAmount()))
/*      */         {
/*  784 */           isValid = true;
/*  785 */           break;
/*      */         }
/*      */     }
/*  788 */     return isValid;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static boolean compareAmounts(String transactionAmount, String currencySpecificAmount)
/*      */   {
/*  803 */     boolean amountStatus = true;
/*      */     
/*  805 */     if ((!isNullOrEmpty(transactionAmount)) && (!isNullOrEmpty(currencySpecificAmount)))
/*      */     {
/*  807 */       if (Long.parseLong(currencySpecificAmount) < Long.parseLong(transactionAmount))
/*      */       {
/*  809 */         amountStatus = false;
/*      */       }
/*      */     }
/*      */     
/*  813 */     return amountStatus;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static boolean isValidModulusTenCheck(String cardNumber)
/*      */   {
/*  824 */     boolean isValid = true;
/*  825 */     if (!isNullOrEmpty(cardNumber)) {
/*  826 */       isValid = CheckDigitValidator.modulusTenCheck(cardNumber);
/*      */     }
/*  828 */     return isValid;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static boolean isValidCurrencyCode(String currencyCode)
/*      */   {
/*  840 */     boolean isValidCurrencyCode = false;
/*  841 */     CurrencyCode[] arrayOfCurrencyCode; int j = (arrayOfCurrencyCode = CurrencyCode.values()).length; for (int i = 0; i < j; i++) { CurrencyCode currencyCodes = arrayOfCurrencyCode[i];
/*      */       
/*  843 */       if (currencyCode.equalsIgnoreCase(currencyCodes.getCurrencyCode()))
/*      */       {
/*  845 */         isValidCurrencyCode = true;
/*  846 */         break;
/*      */       }
/*      */     }
/*  849 */     return isValidCurrencyCode;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static boolean isValidRegionCode(String regionCode, String countryCode)
/*      */   {
/*  865 */     boolean isValidRegionCode = false;
/*  866 */     if (isValidXmlCountryCode(countryCode)) {
/*      */       XmlCountryCode[] arrayOfXmlCountryCode;
/*  868 */       int j = (arrayOfXmlCountryCode = XmlCountryCode.values()).length; for (int i = 0; i < j; i++) { XmlCountryCode countryCodes = arrayOfXmlCountryCode[i];
/*      */         
/*  870 */         if ((countryCode.equalsIgnoreCase(
/*  871 */           countryCodes.getNumCountryCode())) || 
/*  872 */           (countryCode.equalsIgnoreCase(
/*  873 */           countryCodes.getAlphaCountryCode()))) {
/*  874 */           String numCountryCode = countryCodes.getNumCountryCode();
/*  875 */           if (validateRegionCode1(regionCode, numCountryCode))
/*      */           {
/*  877 */             isValidRegionCode = true; break;
/*      */           }
/*  879 */           if (validateRegionCode2(regionCode, numCountryCode))
/*      */           {
/*  881 */             isValidRegionCode = true; break;
/*      */           }
/*  883 */           if (!validateRegionCode3(regionCode, numCountryCode))
/*      */             break;
/*  885 */           isValidRegionCode = true;
/*      */           
/*      */ 
/*      */ 
/*  889 */           break;
/*      */         }
/*      */         
/*      */       }
/*      */     }
/*      */     else
/*      */     {
/*  896 */       isValidRegionCode = false;
/*      */     }
/*      */     
/*  899 */     return isValidRegionCode;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static boolean validateRegionCode1(String regionCode, String countryCode)
/*      */   {
/*  913 */     boolean isValidRegionCode = false;
/*  914 */     RegionCode1[] arrayOfRegionCode1; int j = (arrayOfRegionCode1 = RegionCode1.values()).length; for (int i = 0; i < j; i++) { RegionCode1 regionCodes = arrayOfRegionCode1[i];
/*  915 */       if (countryCode.equalsIgnoreCase(regionCodes.getCountryCode()))
/*      */       {
/*  917 */         if (regionCode.equalsIgnoreCase(regionCodes.getRegionCode())) {
/*  918 */           isValidRegionCode = true;
/*  919 */           break;
/*      */         } }
/*      */     }
/*  922 */     return isValidRegionCode;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static boolean validateRegionCode2(String regionCode, String countryCode)
/*      */   {
/*  936 */     boolean isValidRegionCode = false;
/*  937 */     RegionCode2[] arrayOfRegionCode2; int j = (arrayOfRegionCode2 = RegionCode2.values()).length; for (int i = 0; i < j; i++) { RegionCode2 regionCodes = arrayOfRegionCode2[i];
/*      */       
/*  939 */       if (countryCode.equalsIgnoreCase(regionCodes.getCountryCode()))
/*      */       {
/*  941 */         if (regionCode.equalsIgnoreCase(regionCodes.getRegionCode()))
/*      */         {
/*  943 */           isValidRegionCode = true;
/*  944 */           break;
/*      */         } }
/*      */     }
/*  947 */     return isValidRegionCode;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static boolean validateRegionCode3(String regionCode, String countryCode)
/*      */   {
/*  961 */     boolean isValidRegionCode = false;
/*  962 */     RegionCode3[] arrayOfRegionCode3; int j = (arrayOfRegionCode3 = RegionCode3.values()).length; for (int i = 0; i < j; i++) { RegionCode3 regionCodes = arrayOfRegionCode3[i];
/*      */       
/*  964 */       if (countryCode.equalsIgnoreCase(regionCodes.getCountryCode()))
/*      */       {
/*  966 */         if (regionCode.equalsIgnoreCase(regionCodes.getRegionCode()))
/*      */         {
/*  968 */           isValidRegionCode = true;
/*  969 */           break;
/*      */         } }
/*      */     }
/*  972 */     return isValidRegionCode;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static boolean isProhibitedCurrencyCode(String currencyCode)
/*      */   {
/*  984 */     boolean isProhibitedCode = false;
/*      */     ProhibitedCountryCodes[] arrayOfProhibitedCountryCodes;
/*  986 */     int j = (arrayOfProhibitedCountryCodes = ProhibitedCountryCodes.values()).length; for (int i = 0; i < j; i++) {
/*  986 */       ProhibitedCountryCodes prohibitedCountryCodes = arrayOfProhibitedCountryCodes[i];
/*      */       
/*  988 */       if (currencyCode.equalsIgnoreCase(
/*  989 */         prohibitedCountryCodes.getCountryCode()))
/*      */       {
/*  991 */         isProhibitedCode = true;
/*  992 */         break;
/*      */       }
/*      */     }
/*  995 */     return isProhibitedCode;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static boolean isValidXmlDate(String inDate, String format)
/*      */   {
/* 1007 */     boolean validDate = true;
/* 1008 */     String year = "0";
/* 1009 */     String month = "";
/* 1010 */     String day = "0";
/*      */     
/* 1012 */     if (format.equalsIgnoreCase("YYMM")) {
/* 1013 */       if (inDate.length() == 4) {
/* 1014 */         month = inDate.substring(2);
/*      */         
/* 1016 */         if (!validateXmlMonth(month))
/*      */         {
/* 1018 */           validDate = false;
/*      */         }
/*      */       }
/*      */       else
/*      */       {
/* 1023 */         validDate = false;
/*      */       }
/*      */     }
/* 1026 */     else if (inDate.length() == 8)
/*      */     {
/* 1028 */       year = inDate.substring(0, 4);
/* 1029 */       month = inDate.substring(4, 6);
/* 1030 */       day = inDate.substring(6, 8);
/* 1031 */       if ((!validateXmlMonth(month)) || 
/* 1032 */         (!validateXmlDay(year, month, day)))
/*      */       {
/* 1034 */         validDate = false;
/*      */       }
/*      */     }
/*      */     else {
/* 1038 */       validDate = false;
/*      */     }
/*      */     
/*      */ 
/* 1042 */     return validDate;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static boolean validateXmlMonth(String month)
/*      */   {
/* 1052 */     boolean isValidMonth = true;
/*      */     
/* 1054 */     if ((Integer.parseInt(month) <= 0) || (Integer.parseInt(month) > 12)) {
/* 1055 */       isValidMonth = false;
/*      */     }
/* 1057 */     return isValidMonth;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static boolean validateXmlDay(String yy, String mm, String dd)
/*      */   {
/* 1069 */     boolean isValid = true;
/* 1070 */     int month = Integer.parseInt(mm);
/* 1071 */     int date = Integer.parseInt(dd);
/* 1072 */     int year = Integer.parseInt(yy);
/* 1073 */     if ((month == 1) || (month == 3) || (month == 5) || (month == 7) || (month == 8) || 
/* 1074 */       (month == 10) || (month == 12)) {
/* 1075 */       if ((date > 31) || (date <= 0)) {
/* 1076 */         isValid = false;
/*      */       }
/* 1078 */     } else if ((month == 4) || (month == 6) || (month == 9) || (month == 11)) {
/* 1079 */       if ((date > 30) || (date <= 0)) {
/* 1080 */         isValid = false;
/*      */       }
/* 1082 */     } else if (month == 2)
/*      */     {
/* 1084 */       boolean isLeapYear = year % 4 == 0;
/* 1085 */       if (isLeapYear) {
/* 1086 */         if ((date <= 0) || (date > 29)) {
/* 1087 */           isValid = false;
/*      */         } else {
/* 1089 */           isValid = true;
/*      */         }
/*      */       }
/* 1092 */       else if ((date <= 0) || (date > 28)) {
/* 1093 */         isValid = false;
/*      */       }
/*      */     }
/* 1096 */     return isValid;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static boolean isValidAddAmtTypeCd(String addAmtTypeCd)
/*      */   {
/* 1108 */     boolean isValidAddAmtTypeCd = false;
/* 1109 */     AddAmtTypeCd[] arrayOfAddAmtTypeCd; int j = (arrayOfAddAmtTypeCd = AddAmtTypeCd.values()).length; for (int i = 0; i < j; i++) { AddAmtTypeCd addAmtTypeCds = arrayOfAddAmtTypeCd[i];
/*      */       
/* 1111 */       if (addAmtTypeCd.equalsIgnoreCase(addAmtTypeCds.getAddAmtTypeCd()))
/*      */       {
/* 1113 */         isValidAddAmtTypeCd = true;
/* 1114 */         break;
/*      */       }
/*      */     }
/* 1117 */     return isValidAddAmtTypeCd;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static boolean isValidXmlTransProcCd(String transProcCd)
/*      */   {
/* 1128 */     boolean isValidTransProcCd = false;
/* 1129 */     TransProcCd[] arrayOfTransProcCd; int j = (arrayOfTransProcCd = TransProcCd.values()).length; for (int i = 0; i < j; i++) { TransProcCd posCodes = arrayOfTransProcCd[i];
/* 1130 */       if (transProcCd.equalsIgnoreCase(posCodes.getTransProcCd())) {
/* 1131 */         isValidTransProcCd = true;
/* 1132 */         break;
/*      */       }
/*      */     }
/* 1135 */     return isValidTransProcCd;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static boolean isValidXmlAmount(String currencyCode, String amount)
/*      */   {
/* 1148 */     boolean isValid = false;
/* 1149 */     XmlCurrencyCode[] arrayOfXmlCurrencyCode; int j = (arrayOfXmlCurrencyCode = XmlCurrencyCode.values()).length; for (int i = 0; i < j; i++) { XmlCurrencyCode xmlCurrencyCode = arrayOfXmlCurrencyCode[i];
/*      */       
/* 1151 */       if ((currencyCode.equalsIgnoreCase(
/* 1152 */         xmlCurrencyCode.getAlphaCurrencyCode())) || 
/* 1153 */         (currencyCode.equalsIgnoreCase(xmlCurrencyCode.getNumCurrencyCode())))
/*      */       {
/* 1155 */         if (compareAmounts(amount, 
/* 1156 */           xmlCurrencyCode.getMaxTransAmt())) {
/* 1157 */           isValid = true;
/* 1158 */           break;
/*      */         }
/*      */       }
/*      */     }
/* 1162 */     return isValid;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static boolean isValidXmlCurrencyCode(String currencyCode)
/*      */   {
/* 1173 */     boolean isValidCurrencyCode = false;
/* 1174 */     XmlCurrencyCode[] arrayOfXmlCurrencyCode; int j = (arrayOfXmlCurrencyCode = XmlCurrencyCode.values()).length; for (int i = 0; i < j; i++) { XmlCurrencyCode currencyCodes = arrayOfXmlCurrencyCode[i];
/*      */       
/* 1176 */       if ((currencyCode.equalsIgnoreCase(
/* 1177 */         currencyCodes.getAlphaCurrencyCode())) || 
/* 1178 */         (currencyCode.equalsIgnoreCase(
/* 1179 */         currencyCodes.getNumCurrencyCode())))
/*      */       {
/* 1181 */         isValidCurrencyCode = true;
/* 1182 */         break;
/*      */       }
/*      */     }
/* 1185 */     return isValidCurrencyCode;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static boolean isValidXmlCountryCode(String countryCode)
/*      */   {
/* 1196 */     boolean isValidCountryCode = false;
/* 1197 */     XmlCountryCode[] arrayOfXmlCountryCode; int j = (arrayOfXmlCountryCode = XmlCountryCode.values()).length; for (int i = 0; i < j; i++) { XmlCountryCode countryCodes = arrayOfXmlCountryCode[i];
/* 1198 */       if (!countryCode.equalsIgnoreCase(countryCodes.getNumCountryCode())) {
/* 1199 */         if (countryCode.equalsIgnoreCase(
/* 1200 */           countryCodes.getAlphaCountryCode()))
/* 1201 */           if (countryCodes.getUsage().equalsIgnoreCase(
/* 1202 */             "X")) {}
/* 1203 */       } else { isValidCountryCode = true;
/* 1204 */         break;
/*      */       }
/*      */     }
/* 1207 */     return isValidCountryCode;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static String validateTransactionDate(String transactionDate)
/*      */   {
/* 1220 */     String status = "";
/* 1221 */     int year = Integer.parseInt(transactionDate.substring(0, 4));
/* 1222 */     int month = Integer.parseInt(transactionDate.substring(4, 6)) - 1;
/* 1223 */     int day = Integer.parseInt(transactionDate.substring(6));
/* 1224 */     Calendar enteredDate = Calendar.getInstance();
/* 1225 */     enteredDate.set(year, month, day);
/* 1226 */     Calendar currentDate = Calendar.getInstance();
/* 1227 */     Calendar pastDate = Calendar.getInstance();
/* 1228 */     pastDate.set(5, 
/* 1229 */       currentDate.get(5) - 364);
/* 1230 */     Calendar futureDate = Calendar.getInstance();
/* 1231 */     futureDate.set(5, 
/* 1232 */       currentDate.get(5) + 1);
/* 1233 */     if (enteredDate.before(pastDate)) {
/* 1234 */       status = "expired";
/* 1235 */     } else if (enteredDate.after(futureDate)) {
/* 1236 */       status = "futureDate";
/*      */     }
/* 1238 */     return status;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static boolean isIndustryRecordPresent(DataCaptureRequestBean dataCaptureRequestBean)
/*      */   {
/* 1251 */     boolean industryPresentflag = false;
/* 1252 */     if (dataCaptureRequestBean.getTaaAirIndustry() != null) {
/* 1253 */       industryPresentflag = true;
/* 1254 */     } else if (dataCaptureRequestBean.getTaaAutoRentalIndustry() != null) {
/* 1255 */       industryPresentflag = true;
/* 1256 */     } else if (dataCaptureRequestBean.getTaaCommunServIndustry() != null) {
/* 1257 */       industryPresentflag = true;
/* 1258 */     } else if (dataCaptureRequestBean.getTaaEntertainmentTktIndustry() != null) {
/* 1259 */       industryPresentflag = true;
/* 1260 */     } else if (dataCaptureRequestBean.getTaaInsuranceIndustry() != null) {
/* 1261 */       industryPresentflag = true;
/* 1262 */     } else if (dataCaptureRequestBean.getTaaLodgingIndustry() != null) {
/* 1263 */       industryPresentflag = true;
/* 1264 */     } else if (dataCaptureRequestBean.getTaaRailIndustry() != null) {
/* 1265 */       industryPresentflag = true;
/* 1266 */     } else if (dataCaptureRequestBean.getTaaRetailIndustry() != null) {
/* 1267 */       industryPresentflag = true;
/* 1268 */     } else if (dataCaptureRequestBean.getTaaTravCruiseIndustry() != null) {
/* 1269 */       industryPresentflag = true;
/*      */     }
/* 1271 */     return industryPresentflag;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static String getMasked(int length, String symbol)
/*      */   {
/* 1285 */     String temp = "";
/* 1286 */     for (int i = 0; i < length; i++) {
/* 1287 */       temp = symbol + temp;
/*      */     }
/* 1289 */     return temp;
/*      */   }
/*      */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\validator\CommonValidator.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */