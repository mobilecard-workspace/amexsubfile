/*     */ package com.americanexpress.ips.gfsg.validator.industry;
/*     */ 
/*     */ import com.americanexpress.ips.gfsg.beans.ErrorObject;
/*     */ import com.americanexpress.ips.gfsg.beans.common.TransactionAdviceAddendumBean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.TransactionAdviceBasicBean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.LodgingIndustryTAABean;
/*     */ import com.americanexpress.ips.gfsg.enumerations.SubmissionErrorCodes;
/*     */ import com.americanexpress.ips.gfsg.exceptions.SettlementException;
/*     */ import com.americanexpress.ips.gfsg.validator.CommonValidator;
/*     */ import java.util.List;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class LodgingRecordValidator
/*     */ {
/*     */   public static void validateLodgingTypeRec(TransactionAdviceBasicBean transactionAdviceBasicType, TransactionAdviceAddendumBean transactionAddendumType, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/*  37 */     LodgingIndustryTAABean lodgingIndustryTAABean = (LodgingIndustryTAABean)transactionAddendumType;
/*     */     
/*     */ 
/*  40 */     TAADBRecordValidator.validateRecordType(lodgingIndustryTAABean
/*  41 */       .getRecordType(), errorCodes);
/*  42 */     TAADBRecordValidator.validateRecordNumber(lodgingIndustryTAABean
/*  43 */       .getRecordNumber(), errorCodes);
/*  44 */     TAADBRecordValidator.validateTransactionIdentifier(
/*  45 */       lodgingIndustryTAABean.getTransactionIdentifier(), 
/*  46 */       transactionAdviceBasicType, errorCodes);
/*  47 */     TAADBRecordValidator.validateAddendaTypeCode(lodgingIndustryTAABean
/*  48 */       .getAddendaTypeCode(), errorCodes);
/*  49 */     TAADBRecordValidator.validateFormatCode(lodgingIndustryTAABean
/*  50 */       .getFormatCode(), transactionAdviceBasicType, errorCodes);
/*     */     
/*  52 */     validateLodgingSpecialProgramCode(lodgingIndustryTAABean
/*  53 */       .getLodgingSpecialProgramCode(), errorCodes);
/*  54 */     validateLodgingCheckInDate(lodgingIndustryTAABean
/*  55 */       .getLodgingCheckInDate(), errorCodes);
/*  56 */     validateLodgingCheckOutDate(lodgingIndustryTAABean, errorCodes);
/*  57 */     validateLodgingRoomRate1(lodgingIndustryTAABean.getLodgingRoomRate1(), 
/*  58 */       errorCodes);
/*  59 */     validateNumberOfNightsAtRoomRate1(lodgingIndustryTAABean
/*  60 */       .getNumberOfNightsAtRoomRate1(), errorCodes);
/*  61 */     validateLodgingRoomRate2(lodgingIndustryTAABean.getLodgingRoomRate2(), 
/*  62 */       errorCodes);
/*  63 */     validateNumberOfNightsAtRoomRate2(lodgingIndustryTAABean
/*  64 */       .getNumberOfNightsAtRoomRate2(), errorCodes);
/*  65 */     validateLodgingRoomRate3(lodgingIndustryTAABean.getLodgingRoomRate3(), 
/*  66 */       errorCodes);
/*  67 */     validateNumberOfNightsAtRoomRate3(lodgingIndustryTAABean
/*  68 */       .getNumberOfNightsAtRoomRate3(), errorCodes);
/*  69 */     validateLodgingRenterName(
/*  70 */       lodgingIndustryTAABean.getLodgingRenterName(), errorCodes);
/*  71 */     validateLodgingFolioNumber(lodgingIndustryTAABean
/*  72 */       .getLodgingFolioNumber(), errorCodes);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateLodgingSpecialProgramCode(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/*  88 */     ErrorObject errorObj = null;
/*     */     
/*  90 */     if (CommonValidator.isNullOrEmpty(value))
/*     */     {
/*     */ 
/*  93 */       errorObj = new ErrorObject(
/*  94 */         SubmissionErrorCodes.ERROR_DATAFIELD71_LODGINGSPECIALPROGRAM_CODE);
/*  95 */       errorCodes.add(errorObj);
/*     */     }
/*  97 */     else if (CommonValidator.validateData(value, 
/*  98 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[72]))
/*     */     {
/* 100 */       String reqLength = CommonValidator.validateLength(value, 1, 1);
/*     */       
/* 102 */       if (reqLength.equals("greaterThanMax")) {
/* 103 */         errorObj = new ErrorObject(
/* 104 */           SubmissionErrorCodes.ERROR_DATAFIELD71_LODGINGSPECIALPROGRAM_CODE);
/* 105 */         errorCodes.add(errorObj);
/*     */ 
/*     */       }
/* 108 */       else if ((Integer.parseInt(value) != 1) && 
/* 109 */         (Integer.parseInt(value) != 2) && 
/* 110 */         (Integer.parseInt(value) != 3)) {
/* 111 */         errorObj = new ErrorObject(
/* 112 */           SubmissionErrorCodes.ERROR_DATAFIELD71_LODGINGSPECIALPROGRAM_CODE);
/* 113 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */     else {
/* 117 */       errorObj = new ErrorObject(
/* 118 */         SubmissionErrorCodes.ERROR_DATAFIELD71_LODGINGSPECIALPROGRAM_CODE);
/* 119 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateLodgingCheckInDate(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 136 */     ErrorObject errorObj = null;
/*     */     
/* 138 */     if (CommonValidator.isNullOrEmpty(value)) {
/* 139 */       errorObj = new ErrorObject(
/* 140 */         SubmissionErrorCodes.ERROR_DATAFIELD72_LODGING_CHECK_INDATE);
/* 141 */       errorCodes.add(errorObj);
/*     */     }
/* 143 */     else if (CommonValidator.validateData(value, 
/* 144 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[73]))
/*     */     {
/* 146 */       String reqLength = CommonValidator.validateLength(value, 8, 8);
/*     */       
/* 148 */       if ((!reqLength.equals("equal")) || 
/* 149 */         (!CommonValidator.isValidDate(value, 
/* 150 */         "CCYYMMDD")))
/*     */       {
/* 152 */         errorObj = new ErrorObject(
/* 153 */           SubmissionErrorCodes.ERROR_DATAFIELD72_LODGING_CHECK_INDATE);
/* 154 */         errorCodes.add(errorObj);
/*     */       }
/*     */     } else {
/* 157 */       errorObj = new ErrorObject(
/* 158 */         SubmissionErrorCodes.ERROR_DATAFIELD72_LODGING_CHECK_INDATE);
/* 159 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateLodgingCheckOutDate(LodgingIndustryTAABean lodgingIndustryTAABean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 177 */     ErrorObject errorObj = null;
/*     */     
/* 179 */     if (CommonValidator.isNullOrEmpty(lodgingIndustryTAABean
/* 180 */       .getLodgingCheckOutDate()))
/*     */     {
/* 182 */       errorObj = new ErrorObject(
/* 183 */         SubmissionErrorCodes.ERROR_DATAFIELD73_LODGING_CHECK_OUTDATE);
/* 184 */       errorCodes.add(errorObj);
/*     */     }
/* 186 */     else if (CommonValidator.validateData(lodgingIndustryTAABean
/* 187 */       .getLodgingCheckOutDate(), 
/* 188 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[74]))
/*     */     {
/* 190 */       String reqLength = CommonValidator.validateLength(
/* 191 */         lodgingIndustryTAABean.getLodgingCheckOutDate(), 8, 8);
/*     */       
/* 193 */       if ((reqLength.equals("equal")) && 
/* 194 */         (CommonValidator.isValidDate(lodgingIndustryTAABean
/* 195 */         .getLodgingCheckOutDate(), 
/* 196 */         "CCYYMMDD")))
/*     */       {
/*     */ 
/*     */ 
/* 200 */         if (Integer.parseInt(lodgingIndustryTAABean.getLodgingCheckOutDate()) < Integer.parseInt(lodgingIndustryTAABean
/* 201 */           .getLodgingCheckInDate()))
/*     */         {
/* 203 */           errorObj = new ErrorObject(
/* 204 */             SubmissionErrorCodes.ERROR_DATAFIELD74_LODGING_CHECK_OUTDATE);
/* 205 */           errorCodes.add(errorObj);
/*     */         }
/*     */       } else {
/* 208 */         errorObj = new ErrorObject(
/* 209 */           SubmissionErrorCodes.ERROR_DATAFIELD73_LODGING_CHECK_OUTDATE);
/* 210 */         errorCodes.add(errorObj);
/*     */       }
/*     */     } else {
/* 213 */       errorObj = new ErrorObject(
/* 214 */         SubmissionErrorCodes.ERROR_DATAFIELD73_LODGING_CHECK_OUTDATE);
/* 215 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateLodgingRoomRate1(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 232 */     ErrorObject errorObj = null;
/*     */     
/* 234 */     if (!CommonValidator.isNullOrEmpty(value)) {
/* 235 */       if (CommonValidator.validateData(value, 
/* 236 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[76]))
/*     */       {
/* 238 */         String reqLength = 
/* 239 */           CommonValidator.validateLength(value, 12, 12);
/*     */         
/* 241 */         if (reqLength.equals("greaterThanMax"))
/*     */         {
/* 243 */           errorObj = new ErrorObject(
/* 244 */             SubmissionErrorCodes.ERROR_DATAFIELD76_LODGING_ROOMRATE1);
/* 245 */           errorCodes.add(errorObj);
/*     */         }
/*     */       } else {
/* 248 */         errorObj = new ErrorObject(
/* 249 */           SubmissionErrorCodes.ERROR_DATAFIELD75_LODGING_ROOMRATE1);
/* 250 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateNumberOfNightsAtRoomRate1(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 267 */     ErrorObject errorObj = null;
/*     */     
/* 269 */     if (!CommonValidator.isNullOrEmpty(value)) {
/* 270 */       if (CommonValidator.validateData(value, 
/* 271 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[77]))
/*     */       {
/* 273 */         String reqLength = CommonValidator.validateLength(value, 2, 2);
/*     */         
/* 275 */         if (reqLength.equals("greaterThanMax")) {
/* 276 */           errorObj = new ErrorObject(
/* 277 */             SubmissionErrorCodes.ERROR_DATAFIELD77_NUMBEROF_NIGHTSATROOMRATE1);
/* 278 */           errorCodes.add(errorObj);
/*     */         }
/*     */       } else {
/* 281 */         errorObj = new ErrorObject(
/* 282 */           SubmissionErrorCodes.ERROR_DATAFIELD77_NUMBEROF_NIGHTSATROOMRATE1);
/* 283 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateLodgingRoomRate2(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 300 */     ErrorObject errorObj = null;
/*     */     
/* 302 */     if (!CommonValidator.isNullOrEmpty(value)) {
/* 303 */       if (CommonValidator.validateData(value, 
/* 304 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[79]))
/*     */       {
/* 306 */         String reqLength = 
/* 307 */           CommonValidator.validateLength(value, 12, 12);
/*     */         
/* 309 */         if (reqLength.equals("greaterThanMax"))
/*     */         {
/* 311 */           errorObj = new ErrorObject(
/* 312 */             SubmissionErrorCodes.ERROR_DATAFIELD78_LODGING_ROOMRATE2);
/* 313 */           errorCodes.add(errorObj);
/*     */         }
/*     */       } else {
/* 316 */         errorObj = new ErrorObject(
/* 317 */           SubmissionErrorCodes.ERROR_DATAFIELD79_LODGING_ROOMRATE2);
/* 318 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateLodgingRoomRate3(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 335 */     ErrorObject errorObj = null;
/*     */     
/* 337 */     if (!CommonValidator.isNullOrEmpty(value)) {
/* 338 */       if (CommonValidator.validateData(value, 
/* 339 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[80]))
/*     */       {
/* 341 */         String reqLength = 
/* 342 */           CommonValidator.validateLength(value, 12, 12);
/*     */         
/* 344 */         if (reqLength.equals("greaterThanMax"))
/*     */         {
/* 346 */           errorObj = new ErrorObject(
/* 347 */             SubmissionErrorCodes.ERROR_DATAFIELD81_LODGING_ROOMRATE3);
/* 348 */           errorCodes.add(errorObj);
/*     */         }
/*     */       } else {
/* 351 */         errorObj = new ErrorObject(
/* 352 */           SubmissionErrorCodes.ERROR_DATAFIELD82_LODGING_ROOMRATE3);
/* 353 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateNumberOfNightsAtRoomRate2(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 370 */     ErrorObject errorObj = null;
/*     */     
/* 372 */     if (!CommonValidator.isNullOrEmpty(value)) {
/* 373 */       if (CommonValidator.validateData(value, 
/* 374 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[82]))
/*     */       {
/* 376 */         String reqLength = CommonValidator.validateLength(value, 2, 2);
/*     */         
/* 378 */         if (reqLength.equals("greaterThanMax")) {
/* 379 */           errorObj = new ErrorObject(
/* 380 */             SubmissionErrorCodes.ERROR_DATAFIELD80_NUMBEROF_NIGHTSATROOMRATE2);
/* 381 */           errorCodes.add(errorObj);
/*     */         }
/*     */       } else {
/* 384 */         errorObj = new ErrorObject(
/* 385 */           SubmissionErrorCodes.ERROR_DATAFIELD80_NUMBEROF_NIGHTSATROOMRATE2);
/* 386 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateNumberOfNightsAtRoomRate3(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 403 */     ErrorObject errorObj = null;
/*     */     
/* 405 */     if (!CommonValidator.isNullOrEmpty(value)) {
/* 406 */       if (CommonValidator.validateData(value, 
/* 407 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[83]))
/*     */       {
/* 409 */         String reqLength = CommonValidator.validateLength(value, 2, 2);
/*     */         
/* 411 */         if (reqLength.equals("greaterThanMax")) {
/* 412 */           errorObj = new ErrorObject(
/* 413 */             SubmissionErrorCodes.ERROR_DATAFIELD83_NUMBEROF_NIGHTSATROOMRATE3);
/* 414 */           errorCodes.add(errorObj);
/*     */         }
/*     */       } else {
/* 417 */         errorObj = new ErrorObject(
/* 418 */           SubmissionErrorCodes.ERROR_DATAFIELD83_NUMBEROF_NIGHTSATROOMRATE3);
/* 419 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateLodgingRenterName(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 436 */     ErrorObject errorObj = null;
/*     */     
/* 438 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/* 443 */       String reqLength = 
/* 444 */         CommonValidator.validateLength(value, 26, 26);
/*     */       
/* 446 */       if (reqLength.equals("greaterThanMax")) {
/* 447 */         errorObj = new ErrorObject(
/* 448 */           SubmissionErrorCodes.ERROR_DATAFIELD84_LODGINGRENTER_NAME);
/* 449 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateLodgingFolioNumber(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 471 */     ErrorObject errorObj = null;
/*     */     
/* 473 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/* 478 */       String reqLength = 
/* 479 */         CommonValidator.validateLength(value, 12, 12);
/*     */       
/* 481 */       if (reqLength.equals("greaterThanMax")) {
/* 482 */         errorObj = new ErrorObject(
/* 483 */           SubmissionErrorCodes.ERROR_DATAFIELD85_LODGINGFOLIO_NUMBER);
/* 484 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\validator\industry\LodgingRecordValidator.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */