/*     */ package com.americanexpress.ips.gfsg.validator.industry;
/*     */ 
/*     */ import com.americanexpress.ips.gfsg.beans.ErrorObject;
/*     */ import com.americanexpress.ips.gfsg.beans.common.TransactionAdviceBasicBean;
/*     */ import com.americanexpress.ips.gfsg.enumerations.RecordType;
/*     */ import com.americanexpress.ips.gfsg.enumerations.SubmissionErrorCodes;
/*     */ import com.americanexpress.ips.gfsg.exceptions.SettlementException;
/*     */ import com.americanexpress.ips.gfsg.validator.CommonValidator;
/*     */ import java.util.List;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class TAADBRecordValidator
/*     */ {
/*     */   public static void validateRecordType(String recordType, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/*  36 */     ErrorObject errorObj = null;
/*     */     
/*  38 */     if (CommonValidator.isNullOrEmpty(recordType))
/*     */     {
/*     */ 
/*  41 */       errorObj = new ErrorObject(
/*  42 */         SubmissionErrorCodes.ERROR_DATAFIELD1_RECORDTYPE);
/*  43 */       errorCodes.add(errorObj);
/*     */     }
/*  45 */     else if (!RecordType.Transaction_Advice_Addendum.getRecordType().equalsIgnoreCase(recordType)) {
/*  46 */       errorObj = new ErrorObject(
/*  47 */         SubmissionErrorCodes.ERROR_DATAFIELD1_RECORDTYPE);
/*  48 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void validateRecordNumber(String recordNumber, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/*  65 */     ErrorObject errorObj = null;
/*     */     
/*  67 */     if (CommonValidator.isNullOrEmpty(recordNumber))
/*     */     {
/*  69 */       errorObj = new ErrorObject(
/*  70 */         SubmissionErrorCodes.ERROR_DATAFIELD3_RECORDNUMBER);
/*  71 */       errorCodes.add(errorObj);
/*     */     }
/*  73 */     else if (CommonValidator.validateData(recordNumber, 
/*  74 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['']))
/*     */     {
/*  76 */       String reqLength = CommonValidator.validateLength(recordNumber, 8, 8);
/*     */       
/*  78 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/*  80 */         errorObj = new ErrorObject(
/*  81 */           SubmissionErrorCodes.ERROR_DATAFIELD3_RECORDNUMBER);
/*  82 */         errorCodes.add(errorObj);
/*     */       }
/*     */     } else {
/*  85 */       errorObj = new ErrorObject(
/*  86 */         SubmissionErrorCodes.ERROR_DATAFIELD2_RECORDNUMBER);
/*  87 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void validateTransactionIdentifier(String transactionIdentifier, TransactionAdviceBasicBean transactionAdviceBasicType, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 106 */     ErrorObject errorObj = null;
/*     */     
/* 108 */     if (CommonValidator.isNullOrEmpty(transactionIdentifier))
/*     */     {
/* 110 */       errorObj = new ErrorObject(
/* 111 */         SubmissionErrorCodes.ERROR_DATAFIELD25_TRANSACTION_IDENTIFIER);
/* 112 */       errorCodes.add(errorObj);
/*     */     }
/* 114 */     else if (CommonValidator.validateData(transactionIdentifier, 
/* 115 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['']))
/*     */     {
/* 117 */       String reqLength = 
/* 118 */         CommonValidator.validateLength(transactionIdentifier, 15, 15);
/*     */       
/* 120 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/* 122 */         errorObj = new ErrorObject(
/* 123 */           SubmissionErrorCodes.ERROR_DATAFIELD25_TRANSACTION_IDENTIFIER);
/* 124 */         errorCodes.add(errorObj);
/*     */       }
/* 126 */       else if (!transactionIdentifier.equalsIgnoreCase(
/* 127 */         transactionAdviceBasicType.getTransactionIdentifier())) {
/* 128 */         errorObj = new ErrorObject(
/* 129 */           SubmissionErrorCodes.ERROR_DATAFIELD67_TRANSACTION_IDENTIFIER);
/* 130 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */     else {
/* 134 */       errorObj = new ErrorObject(
/* 135 */         SubmissionErrorCodes.ERROR_DATAFIELD25_TRANSACTION_IDENTIFIER);
/* 136 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void validateFormatCode(String formatCode, TransactionAdviceBasicBean transactionAdviceBasicType, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 155 */     ErrorObject errorObj = null;
/* 156 */     if (CommonValidator.isNullOrEmpty(formatCode))
/*     */     {
/* 158 */       errorObj = new ErrorObject(
/* 159 */         SubmissionErrorCodes.ERROR_DATAFIELD28_FORMAT_CODE);
/* 160 */       errorCodes.add(errorObj);
/*     */     }
/* 162 */     else if (CommonValidator.validateData(formatCode, 
/* 163 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['']))
/*     */     {
/* 165 */       String reqLength = CommonValidator.validateLength(formatCode, 2, 2);
/*     */       
/* 167 */       if (reqLength.equals("greaterThanMax")) {
/* 168 */         errorObj = new ErrorObject(
/* 169 */           SubmissionErrorCodes.ERROR_DATAFIELD28_FORMAT_CODE);
/* 170 */         errorCodes.add(errorObj);
/*     */       }
/* 172 */       else if (!formatCode.equalsIgnoreCase(
/* 173 */         transactionAdviceBasicType.getFormatCode())) {
/* 174 */         errorObj = new ErrorObject(
/* 175 */           SubmissionErrorCodes.ERROR_DATAFIELD138_FORMAT_CODE);
/* 176 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */     else {
/* 180 */       errorObj = new ErrorObject(
/* 181 */         SubmissionErrorCodes.ERROR_DATAFIELD28_FORMAT_CODE);
/* 182 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void validateAddendaTypeCode(String addendaTypeCode, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 199 */     ErrorObject errorObj = null;
/* 200 */     if (CommonValidator.isNullOrEmpty(addendaTypeCode))
/*     */     {
/* 202 */       errorObj = new ErrorObject(
/* 203 */         SubmissionErrorCodes.ERROR_DATAFIELD68_ADDENDA_TYPECODE);
/* 204 */       errorCodes.add(errorObj);
/*     */     }
/* 206 */     else if (CommonValidator.validateData(addendaTypeCode, 
/* 207 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['']))
/*     */     {
/* 209 */       String reqLength = CommonValidator.validateLength(addendaTypeCode, 2, 2);
/*     */       
/* 211 */       if (reqLength.equals("greaterThanMax")) {
/* 212 */         errorObj = new ErrorObject(
/* 213 */           SubmissionErrorCodes.ERROR_DATAFIELD68_ADDENDA_TYPECODE);
/* 214 */         errorCodes.add(errorObj);
/*     */ 
/*     */       }
/* 217 */       else if (!CommonValidator.isValidAddendaTypeCode(addendaTypeCode))
/*     */       {
/* 219 */         errorObj = new ErrorObject(
/* 220 */           SubmissionErrorCodes.ERROR_DATAFIELD70_ADDENDA_TYPECODE
/* 221 */           .getErrorCode(), 
/* 222 */           SubmissionErrorCodes.ERROR_DATAFIELD70_ADDENDA_TYPECODE
/* 223 */           .getErrorDescription() + 
/* 224 */           "\n" + 
/* 225 */           "RecordType:" + 
/* 226 */           "|" + 
/* 227 */           "RecordNumber:" + 
/* 228 */           "|" + 
/* 229 */           "AddendaTypeCode" + 
/* 230 */           "|" + 
/* 231 */           SubmissionErrorCodes.ERROR_DATAFIELD70_ADDENDA_TYPECODE
/* 232 */           .getErrorDescription());
/* 233 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */     else {
/* 237 */       errorObj = new ErrorObject(
/* 238 */         SubmissionErrorCodes.ERROR_DATAFIELD68_ADDENDA_TYPECODE);
/* 239 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\validator\industry\TAADBRecordValidator.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */