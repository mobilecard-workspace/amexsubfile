/*     */ package com.americanexpress.ips.gfsg.validator.industry;
/*     */ 
/*     */ import com.americanexpress.ips.gfsg.beans.ErrorObject;
/*     */ import com.americanexpress.ips.gfsg.beans.common.TransactionAdviceAddendumBean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.TransactionAdviceBasicBean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.RetailIndustryTAABean;
/*     */ import com.americanexpress.ips.gfsg.enumerations.SubmissionErrorCodes;
/*     */ import com.americanexpress.ips.gfsg.exceptions.SettlementException;
/*     */ import com.americanexpress.ips.gfsg.validator.CommonValidator;
/*     */ import java.util.List;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class RetailRecordValidator
/*     */ {
/*     */   public static void validateRetailIndRecord(TransactionAdviceBasicBean transactionAdviceBasicType, TransactionAdviceAddendumBean transactionAddendumType, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/*  37 */     RetailIndustryTAABean retailIndustryTAABean = (RetailIndustryTAABean)transactionAddendumType;
/*     */     
/*     */ 
/*  40 */     TAADBRecordValidator.validateRecordType(retailIndustryTAABean
/*  41 */       .getRecordType(), errorCodes);
/*  42 */     TAADBRecordValidator.validateRecordNumber(retailIndustryTAABean
/*  43 */       .getRecordNumber(), errorCodes);
/*  44 */     TAADBRecordValidator.validateTransactionIdentifier(
/*  45 */       retailIndustryTAABean.getTransactionIdentifier(), 
/*  46 */       transactionAdviceBasicType, errorCodes);
/*  47 */     TAADBRecordValidator.validateAddendaTypeCode(retailIndustryTAABean
/*  48 */       .getAddendaTypeCode(), errorCodes);
/*  49 */     TAADBRecordValidator.validateFormatCode(retailIndustryTAABean
/*  50 */       .getFormatCode(), transactionAdviceBasicType, errorCodes);
/*     */     
/*  52 */     validateRetailDepartmentName(retailIndustryTAABean
/*  53 */       .getRetailDepartmentName(), errorCodes);
/*  54 */     validateRetailItemDescription1(retailIndustryTAABean
/*  55 */       .getRetailItemDescription1(), errorCodes);
/*  56 */     validateRetailItemQuantity1(retailIndustryTAABean
/*  57 */       .getRetailItemQuantity1(), errorCodes);
/*  58 */     validateRetailRetailItemAmount1(retailIndustryTAABean
/*  59 */       .getRetailItemAmount1(), errorCodes);
/*     */     
/*  61 */     validateRetailItemDescription2(retailIndustryTAABean
/*  62 */       .getRetailItemDescription2(), errorCodes);
/*  63 */     validateRetailItemQuantity2(retailIndustryTAABean
/*  64 */       .getRetailItemQuantity2(), errorCodes);
/*  65 */     validateRetailRetailItemAmount2(retailIndustryTAABean
/*  66 */       .getRetailItemAmount2(), errorCodes);
/*     */     
/*  68 */     validateRetailItemDescription3(retailIndustryTAABean
/*  69 */       .getRetailItemDescription3(), errorCodes);
/*  70 */     validateRetailItemQuantity3(retailIndustryTAABean
/*  71 */       .getRetailItemQuantity3(), errorCodes);
/*  72 */     validateRetailRetailItemAmount3(retailIndustryTAABean
/*  73 */       .getRetailItemAmount3(), errorCodes);
/*     */     
/*  75 */     validateRetailItemDescription4(retailIndustryTAABean
/*  76 */       .getRetailItemDescription4(), errorCodes);
/*  77 */     validateRetailItemQuantity4(retailIndustryTAABean
/*  78 */       .getRetailItemQuantity4(), errorCodes);
/*  79 */     validateRetailRetailItemAmount4(retailIndustryTAABean
/*  80 */       .getRetailItemAmount4(), errorCodes);
/*     */     
/*  82 */     validateRetailItemDescription5(retailIndustryTAABean
/*  83 */       .getRetailItemDescription5(), errorCodes);
/*  84 */     validateRetailItemQuantity5(retailIndustryTAABean
/*  85 */       .getRetailItemQuantity5(), errorCodes);
/*  86 */     validateRetailRetailItemAmount5(retailIndustryTAABean
/*  87 */       .getRetailItemAmount5(), errorCodes);
/*     */     
/*  89 */     validateRetailItemDescription6(retailIndustryTAABean
/*  90 */       .getRetailItemDescription6(), errorCodes);
/*  91 */     validateRetailItemQuantity6(retailIndustryTAABean
/*  92 */       .getRetailItemQuantity6(), errorCodes);
/*  93 */     validateRetailRetailItemAmount6(retailIndustryTAABean
/*  94 */       .getRetailItemAmount6(), errorCodes);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateRetailDepartmentName(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 110 */     ErrorObject errorObj = null;
/*     */     
/* 112 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/* 117 */       String reqLength = 
/* 118 */         CommonValidator.validateLength(value, 40, 40);
/*     */       
/* 120 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/* 122 */         errorObj = new ErrorObject("RETAIL", 
/* 123 */           "RetailDepartmentName Length is out of Sequence");
/* 124 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateRetailItemDescription1(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 146 */     ErrorObject errorObj = null;
/*     */     
/* 148 */     if (CommonValidator.isNullOrEmpty(value))
/*     */     {
/*     */ 
/* 151 */       errorObj = new ErrorObject(
/* 152 */         SubmissionErrorCodes.ERROR_DATAFIELD220_RETAILITEM_DESCRIPTION1);
/* 153 */       errorCodes.add(errorObj);
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/* 159 */       String reqLength = 
/* 160 */         CommonValidator.validateLength(value, 19, 19);
/*     */       
/* 162 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/* 164 */         errorObj = new ErrorObject("RETAIL", 
/* 165 */           " RetailItemDescription1 Length is out of Sequence");
/* 166 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateRetailItemQuantity1(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 188 */     ErrorObject errorObj = null;
/*     */     
/* 190 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 192 */       if (CommonValidator.validateData(value, 
/* 193 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ý']))
/*     */       {
/* 195 */         String reqLength = CommonValidator.validateLength(value, 3, 3);
/*     */         
/* 197 */         if (reqLength.equals("greaterThanMax"))
/*     */         {
/* 199 */           errorObj = new ErrorObject("RETAIL", 
/* 200 */             "RetailItemQuantity1 Length is out of Sequence");
/* 201 */           errorCodes.add(errorObj);
/*     */         }
/*     */       } else {
/* 204 */         errorObj = new ErrorObject(
/* 205 */           SubmissionErrorCodes.ERROR_DATAFIELD221_RETAILITEM_QUANTITY1);
/* 206 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateRetailRetailItemAmount1(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 223 */     ErrorObject errorObj = null;
/*     */     
/* 225 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 227 */       if (CommonValidator.validateData(value, 
/* 228 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ß']))
/*     */       {
/* 230 */         String reqLength = 
/* 231 */           CommonValidator.validateLength(value, 12, 12);
/*     */         
/* 233 */         if (reqLength.equals("greaterThanMax"))
/*     */         {
/* 235 */           errorObj = new ErrorObject(
/* 236 */             SubmissionErrorCodes.ERROR_DATAFIELD223_RETAILITEM_AMOUNT1);
/* 237 */           errorCodes.add(errorObj);
/*     */         }
/*     */       } else {
/* 240 */         errorObj = new ErrorObject(
/* 241 */           SubmissionErrorCodes.ERROR_DATAFIELD222_RETAILITEM_AMOUNT1);
/* 242 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateRetailItemDescription2(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 259 */     ErrorObject errorObj = null;
/*     */     
/* 261 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 267 */       String reqLength = 
/* 268 */         CommonValidator.validateLength(value, 19, 19);
/*     */       
/* 270 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/* 272 */         errorObj = new ErrorObject("RETAIL", 
/* 273 */           " RetailItemDescription2 Length is out of Sequence");
/* 274 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateRetailItemQuantity2(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 296 */     ErrorObject errorObj = null;
/*     */     
/* 298 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 300 */       if (CommonValidator.validateData(value, 
/* 301 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['á']))
/*     */       {
/* 303 */         String reqLength = CommonValidator.validateLength(value, 3, 3);
/*     */         
/* 305 */         if (reqLength.equals("greaterThanMax"))
/*     */         {
/* 307 */           errorObj = new ErrorObject("RETAIL", 
/* 308 */             "RetailItemQuantity2 Length is out of Sequence");
/* 309 */           errorCodes.add(errorObj);
/*     */         }
/*     */       } else {
/* 312 */         errorObj = new ErrorObject(
/* 313 */           SubmissionErrorCodes.ERROR_DATAFIELD224_RETAILITEM_QUANTITY2);
/* 314 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateRetailRetailItemAmount2(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 331 */     ErrorObject errorObj = null;
/*     */     
/* 333 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 335 */       if (CommonValidator.validateData(value, 
/* 336 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ã']))
/*     */       {
/* 338 */         String reqLength = 
/* 339 */           CommonValidator.validateLength(value, 12, 12);
/*     */         
/* 341 */         if (reqLength.equals("greaterThanMax"))
/*     */         {
/* 343 */           errorObj = new ErrorObject(
/* 344 */             SubmissionErrorCodes.ERROR_DATAFIELD226_RETAILITEM_AMOUNT2);
/* 345 */           errorCodes.add(errorObj);
/*     */         }
/*     */       } else {
/* 348 */         errorObj = new ErrorObject(
/* 349 */           SubmissionErrorCodes.ERROR_DATAFIELD225_RETAILITEM_AMOUNT2);
/* 350 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateRetailItemDescription3(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 367 */     ErrorObject errorObj = null;
/*     */     
/* 369 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 375 */       String reqLength = 
/* 376 */         CommonValidator.validateLength(value, 19, 19);
/*     */       
/* 378 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/* 380 */         errorObj = new ErrorObject("RETAIL", 
/* 381 */           " RetailItemDescription3 Length is out of Sequence");
/* 382 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateRetailItemQuantity3(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 404 */     ErrorObject errorObj = null;
/*     */     
/* 406 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 408 */       if (CommonValidator.validateData(value, 
/* 409 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['å']))
/*     */       {
/* 411 */         String reqLength = CommonValidator.validateLength(value, 3, 3);
/*     */         
/* 413 */         if (reqLength.equals("greaterThanMax"))
/*     */         {
/* 415 */           errorObj = new ErrorObject("RETAIL", 
/* 416 */             "RetailItemQuantity3 Length is out of Sequence");
/* 417 */           errorCodes.add(errorObj);
/*     */         }
/*     */       } else {
/* 420 */         errorObj = new ErrorObject(
/* 421 */           SubmissionErrorCodes.ERROR_DATAFIELD227_RETAILITEM_QUANTITY3);
/* 422 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateRetailRetailItemAmount3(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 439 */     ErrorObject errorObj = null;
/*     */     
/* 441 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 443 */       if (CommonValidator.validateData(value, 
/* 444 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ç']))
/*     */       {
/* 446 */         String reqLength = 
/* 447 */           CommonValidator.validateLength(value, 12, 12);
/*     */         
/* 449 */         if (reqLength.equals("greaterThanMax"))
/*     */         {
/* 451 */           errorObj = new ErrorObject(
/* 452 */             SubmissionErrorCodes.ERROR_DATAFIELD229_RETAILITEM_AMOUNT3);
/* 453 */           errorCodes.add(errorObj);
/*     */         }
/*     */       } else {
/* 456 */         errorObj = new ErrorObject(
/* 457 */           SubmissionErrorCodes.ERROR_DATAFIELD228_RETAILITEM_AMOUNT3);
/* 458 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateRetailItemDescription4(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 475 */     ErrorObject errorObj = null;
/*     */     
/* 477 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 483 */       String reqLength = 
/* 484 */         CommonValidator.validateLength(value, 19, 19);
/*     */       
/* 486 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/* 488 */         errorObj = new ErrorObject("RETAIL", 
/* 489 */           " RetailItemDescription4 Length is out of Sequence");
/* 490 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateRetailItemQuantity4(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 512 */     ErrorObject errorObj = null;
/*     */     
/* 514 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 516 */       if (CommonValidator.validateData(value, 
/* 517 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['é']))
/*     */       {
/* 519 */         String reqLength = CommonValidator.validateLength(value, 3, 3);
/*     */         
/* 521 */         if (reqLength.equals("greaterThanMax"))
/*     */         {
/* 523 */           errorObj = new ErrorObject("RETAIL", 
/* 524 */             "RetailItemQuantity4 Length is out of Sequence");
/* 525 */           errorCodes.add(errorObj);
/*     */         }
/*     */       } else {
/* 528 */         errorObj = new ErrorObject(
/* 529 */           SubmissionErrorCodes.ERROR_DATAFIELD230_RETAILITEM_QUANTITY4);
/* 530 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateRetailRetailItemAmount4(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 547 */     ErrorObject errorObj = null;
/*     */     
/* 549 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 551 */       if (CommonValidator.validateData(value, 
/* 552 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ë']))
/*     */       {
/* 554 */         String reqLength = 
/* 555 */           CommonValidator.validateLength(value, 12, 12);
/*     */         
/* 557 */         if (reqLength.equals("greaterThanMax"))
/*     */         {
/* 559 */           errorObj = new ErrorObject(
/* 560 */             SubmissionErrorCodes.ERROR_DATAFIELD232_RETAILITEM_AMOUNT4);
/* 561 */           errorCodes.add(errorObj);
/*     */         }
/*     */       } else {
/* 564 */         errorObj = new ErrorObject(
/* 565 */           SubmissionErrorCodes.ERROR_DATAFIELD231_RETAILITEM_AMOUNT4);
/* 566 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateRetailItemDescription5(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 583 */     ErrorObject errorObj = null;
/*     */     
/* 585 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 591 */       String reqLength = 
/* 592 */         CommonValidator.validateLength(value, 19, 19);
/*     */       
/* 594 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/* 596 */         errorObj = new ErrorObject("RETAIL", 
/* 597 */           " RetailItemDescription5 Length is out of Sequence");
/* 598 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateRetailItemQuantity5(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 620 */     ErrorObject errorObj = null;
/*     */     
/* 622 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 624 */       if (CommonValidator.validateData(value, 
/* 625 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['í']))
/*     */       {
/* 627 */         String reqLength = CommonValidator.validateLength(value, 3, 3);
/*     */         
/* 629 */         if (reqLength.equals("greaterThanMax"))
/*     */         {
/* 631 */           errorObj = new ErrorObject("RETAIL", 
/* 632 */             "RetailItemQuantity5 Length is out of Sequence");
/* 633 */           errorCodes.add(errorObj);
/*     */         }
/*     */       } else {
/* 636 */         errorObj = new ErrorObject(
/* 637 */           SubmissionErrorCodes.ERROR_DATAFIELD233_RETAILITEM_QUANTITY5);
/* 638 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateRetailRetailItemAmount5(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 655 */     ErrorObject errorObj = null;
/*     */     
/* 657 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 659 */       if (CommonValidator.validateData(value, 
/* 660 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ï']))
/*     */       {
/* 662 */         String reqLength = 
/* 663 */           CommonValidator.validateLength(value, 12, 12);
/*     */         
/* 665 */         if (reqLength.equals("greaterThanMax"))
/*     */         {
/* 667 */           errorObj = new ErrorObject(
/* 668 */             SubmissionErrorCodes.ERROR_DATAFIELD235_RETAILITEM_AMOUNT5);
/* 669 */           errorCodes.add(errorObj);
/*     */         }
/*     */       } else {
/* 672 */         errorObj = new ErrorObject(
/* 673 */           SubmissionErrorCodes.ERROR_DATAFIELD234_RETAILITEM_AMOUNT5);
/* 674 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateRetailItemDescription6(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 691 */     ErrorObject errorObj = null;
/*     */     
/* 693 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 699 */       String reqLength = 
/* 700 */         CommonValidator.validateLength(value, 19, 19);
/*     */       
/* 702 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/* 704 */         errorObj = new ErrorObject("RETAIL", 
/* 705 */           " RetailItemDescription6 Length is out of Sequence");
/* 706 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateRetailItemQuantity6(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 728 */     ErrorObject errorObj = null;
/*     */     
/* 730 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 732 */       if (CommonValidator.validateData(value, 
/* 733 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ñ']))
/*     */       {
/* 735 */         String reqLength = CommonValidator.validateLength(value, 3, 3);
/*     */         
/* 737 */         if (reqLength.equals("greaterThanMax"))
/*     */         {
/* 739 */           errorObj = new ErrorObject("RETAIL", 
/* 740 */             "RetailItemQuantity6 Length is out of Sequence");
/* 741 */           errorCodes.add(errorObj);
/*     */         }
/*     */       } else {
/* 744 */         errorObj = new ErrorObject(
/* 745 */           SubmissionErrorCodes.ERROR_DATAFIELD236_RETAIL_ITEM_QUANTITY6);
/* 746 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateRetailRetailItemAmount6(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 763 */     ErrorObject errorObj = null;
/*     */     
/* 765 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 767 */       if (CommonValidator.validateData(value, 
/* 768 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ó']))
/*     */       {
/* 770 */         String reqLength = 
/* 771 */           CommonValidator.validateLength(value, 12, 12);
/*     */         
/* 773 */         if (reqLength.equals("greaterThanMax"))
/*     */         {
/* 775 */           errorObj = new ErrorObject(
/* 776 */             SubmissionErrorCodes.ERROR_DATAFIELD238_RETAILITEM_AMOUNT6);
/* 777 */           errorCodes.add(errorObj);
/*     */         }
/*     */       } else {
/* 780 */         errorObj = new ErrorObject(
/* 781 */           SubmissionErrorCodes.ERROR_DATAFIELD237_RETAILITEM_AMOUNT6);
/* 782 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\validator\industry\RetailRecordValidator.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */