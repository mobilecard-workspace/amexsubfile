/*      */ package com.americanexpress.ips.gfsg.validator.industry;
/*      */ 
/*      */ import com.americanexpress.ips.gfsg.beans.ErrorObject;
/*      */ import com.americanexpress.ips.gfsg.beans.common.TransactionAdviceAddendumBean;
/*      */ import com.americanexpress.ips.gfsg.beans.common.TransactionAdviceBasicBean;
/*      */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.AutoRentalIndustryTAABean;
/*      */ import com.americanexpress.ips.gfsg.enumerations.SubmissionErrorCodes;
/*      */ import com.americanexpress.ips.gfsg.exceptions.SettlementException;
/*      */ import com.americanexpress.ips.gfsg.validator.CommonValidator;
/*      */ import java.util.List;
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ public class AutoRentalRecordValidator
/*      */ {
/*      */   private static String recordNumber;
/*      */   private static String recordType;
/*      */   
/*      */   public static void validateAutoRentalRecord(TransactionAdviceBasicBean transactionAdviceBasicType, TransactionAdviceAddendumBean transactionAddendumType, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*   41 */     AutoRentalIndustryTAABean autoRentalIndustryTAABean = (AutoRentalIndustryTAABean)transactionAddendumType;
/*      */     
/*      */ 
/*   44 */     recordNumber = autoRentalIndustryTAABean.getRecordNumber();
/*   45 */     recordType = autoRentalIndustryTAABean.getRecordType();
/*   46 */     TAADBRecordValidator.validateRecordType(autoRentalIndustryTAABean
/*   47 */       .getRecordType(), errorCodes);
/*   48 */     TAADBRecordValidator.validateRecordNumber(autoRentalIndustryTAABean
/*   49 */       .getRecordNumber(), errorCodes);
/*   50 */     TAADBRecordValidator.validateTransactionIdentifier(
/*   51 */       autoRentalIndustryTAABean.getTransactionIdentifier(), 
/*   52 */       transactionAdviceBasicType, errorCodes);
/*   53 */     TAADBRecordValidator.validateAddendaTypeCode(autoRentalIndustryTAABean
/*   54 */       .getAddendaTypeCode(), errorCodes);
/*   55 */     TAADBRecordValidator.validateFormatCode(autoRentalIndustryTAABean
/*   56 */       .getFormatCode(), transactionAdviceBasicType, errorCodes);
/*   57 */     validateAutoRentalAgreementNumber(autoRentalIndustryTAABean
/*   58 */       .getAutoRentalAgreementNumber(), errorCodes);
/*   59 */     validateAutoRentalPickupLocation(autoRentalIndustryTAABean
/*   60 */       .getAutoRentalPickupLocation(), errorCodes);
/*   61 */     validateAutoRentalPickupCityName(autoRentalIndustryTAABean
/*   62 */       .getAutoRentalPickupCityName(), errorCodes);
/*   63 */     validateAutoRentalPickupRegionCode(autoRentalIndustryTAABean
/*   64 */       .getAutoRentalPickupRegionCode(), errorCodes);
/*   65 */     validateAutoRentalPickupCountryCode(autoRentalIndustryTAABean
/*   66 */       .getAutoRentalPickupCountryCode(), errorCodes);
/*   67 */     validateAutoRentalPickupDate(autoRentalIndustryTAABean
/*   68 */       .getAutoRentalPickupDate(), errorCodes);
/*   69 */     validateAutoRentalPickupTime(autoRentalIndustryTAABean
/*   70 */       .getAutoRentalPickupTime(), errorCodes);
/*   71 */     validateAutoRentalReturnCityName(autoRentalIndustryTAABean
/*   72 */       .getAutoRentalReturnCityName(), errorCodes);
/*   73 */     validateAutoRentalReturnRegionCode(autoRentalIndustryTAABean
/*   74 */       .getAutoRentalReturnRegionCode(), errorCodes);
/*   75 */     validateAutoRentalReturnCountryCode(autoRentalIndustryTAABean
/*   76 */       .getAutoRentalReturnCountryCode(), errorCodes);
/*   77 */     validateAutoRentalReturnDate(autoRentalIndustryTAABean, errorCodes);
/*   78 */     validateAutoRentalReturnTime(autoRentalIndustryTAABean
/*   79 */       .getAutoRentalReturnTime(), errorCodes);
/*   80 */     validateAutoRentalRenterName(autoRentalIndustryTAABean
/*   81 */       .getAutoRentalRenterName(), errorCodes);
/*   82 */     validateAutoRentalVehicleClassId(autoRentalIndustryTAABean
/*   83 */       .getAutoRentalVehicleClassId(), errorCodes);
/*   84 */     validateAutoRentalDistance(autoRentalIndustryTAABean, errorCodes);
/*   85 */     validateAutoRentalDistanceUnitOfMeasure(autoRentalIndustryTAABean, 
/*   86 */       errorCodes);
/*   87 */     validateAutoRentalAuditAdjustmentIndicator(autoRentalIndustryTAABean, 
/*   88 */       errorCodes);
/*   89 */     validateAutoRentalAuditAdjustmentAmount(autoRentalIndustryTAABean, 
/*   90 */       errorCodes);
/*   91 */     validateReturnDropoffLocation(autoRentalIndustryTAABean, 
/*   92 */       errorCodes);
/*   93 */     validateVehicleIdentificationNumber(autoRentalIndustryTAABean, 
/*   94 */       errorCodes);
/*   95 */     validateDriverIdentificationNumber(autoRentalIndustryTAABean, 
/*   96 */       errorCodes);
/*   97 */     validateDriverTaxNumber(autoRentalIndustryTAABean, 
/*   98 */       errorCodes);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateAutoRentalAgreementNumber(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  114 */     ErrorObject errorObj = null;
/*      */     
/*  116 */     if (CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  118 */       errorObj = new ErrorObject(
/*  119 */         SubmissionErrorCodes.ERROR_DATAFIELD239_AUTORENTAL_AGREEMENTNUMBER
/*  120 */         .getErrorCode(), 
/*  121 */         SubmissionErrorCodes.ERROR_DATAFIELD239_AUTORENTAL_AGREEMENTNUMBER
/*  122 */         .getErrorDescription() + 
/*  123 */         "\n" + 
/*  124 */         "RecordType:" + 
/*  125 */         recordType + 
/*  126 */         "|" + 
/*  127 */         "RecordNumber:" + 
/*  128 */         recordNumber + 
/*  129 */         "|" + 
/*  130 */         "AutoRentalAgreementNumber" + 
/*  131 */         "|" + 
/*  132 */         "This field is mandatory and cannot be empty");
/*  133 */       errorCodes.add(errorObj);
/*      */ 
/*      */ 
/*      */     }
/*      */     else
/*      */     {
/*      */ 
/*  140 */       String reqLength = 
/*  141 */         CommonValidator.validateLength(value, 14, 14);
/*      */       
/*  143 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  145 */         errorObj = new ErrorObject(
/*  146 */           SubmissionErrorCodes.ERROR_DATAFIELD239_AUTORENTAL_AGREEMENTNUMBER
/*  147 */           .getErrorCode(), 
/*  148 */           SubmissionErrorCodes.ERROR_DATAFIELD239_AUTORENTAL_AGREEMENTNUMBER
/*  149 */           .getErrorDescription() + 
/*  150 */           "\n" + 
/*  151 */           "RecordType:" + 
/*  152 */           recordType + 
/*  153 */           "|" + 
/*  154 */           "RecordNumber:" + 
/*  155 */           recordNumber + 
/*  156 */           "|" + 
/*  157 */           "AutoRentalAgreementNumber" + 
/*  158 */           "|" + 
/*  159 */           "This field length Cannot be greater than 14");
/*  160 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateAutoRentalPickupLocation(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  195 */     ErrorObject errorObj = null;
/*      */     
/*  197 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/*      */ 
/*      */ 
/*      */ 
/*  202 */       String reqLength = 
/*  203 */         CommonValidator.validateLength(value, 38, 38);
/*      */       
/*  205 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  207 */         errorObj = new ErrorObject("AUTO", 
/*  208 */           "AutoRentalPickupLocation Length is out of Sequence");
/*  209 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateAutoRentalPickupCityName(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  231 */     ErrorObject errorObj = null;
/*      */     
/*  233 */     if (CommonValidator.isNullOrEmpty(value))
/*      */     {
/*      */ 
/*  236 */       errorObj = new ErrorObject(
/*  237 */         SubmissionErrorCodes.ERROR_DATAFIELD240_AUTORENTAL_PICKUPCITYNAME);
/*  238 */       errorCodes.add(errorObj);
/*      */ 
/*      */     }
/*      */     else
/*      */     {
/*      */ 
/*  244 */       String reqLength = 
/*  245 */         CommonValidator.validateLength(value, 18, 18);
/*      */       
/*  247 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  249 */         errorObj = new ErrorObject("AUTO", 
/*  250 */           "AutoRentalPickupCityName Length is out of Sequence");
/*  251 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateAutoRentalPickupRegionCode(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  273 */     ErrorObject errorObj = null;
/*      */     
/*  275 */     if (CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  277 */       errorObj = new ErrorObject(
/*  278 */         SubmissionErrorCodes.ERROR_DATAFIELD241_AUTORENTAL_PICKUPREGIONCODE);
/*  279 */       errorCodes.add(errorObj);
/*      */     }
/*  281 */     else if (CommonValidator.validateData(value, 
/*  282 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['þ']))
/*      */     {
/*  284 */       String reqLength = CommonValidator.validateLength(value, 3, 3);
/*      */       
/*  286 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  288 */         errorObj = new ErrorObject("AUTO", 
/*  289 */           "AutoRentalPickupRegionCode Length is out of Sequence");
/*  290 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/*  293 */       errorObj = new ErrorObject("AUTO", 
/*  294 */         "AutoRentalPickupRegionCode Type is Missing");
/*  295 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateAutoRentalPickupCountryCode(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  312 */     ErrorObject errorObj = null;
/*      */     
/*  314 */     if (CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  316 */       errorObj = new ErrorObject(
/*  317 */         SubmissionErrorCodes.ERROR_DATAFIELD242_AUTORENTAL_PICKUPCOUNTRYCODE);
/*  318 */       errorCodes.add(errorObj);
/*      */     }
/*  320 */     else if (CommonValidator.validateData(value, 
/*  321 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ÿ']))
/*      */     {
/*  323 */       String reqLength = CommonValidator.validateLength(value, 3, 3);
/*      */       
/*  325 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  327 */         errorObj = new ErrorObject(
/*  328 */           SubmissionErrorCodes.ERROR_DATAFIELD242_AUTORENTAL_PICKUPCOUNTRYCODE);
/*  329 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */     else {
/*  333 */       errorObj = new ErrorObject(
/*  334 */         SubmissionErrorCodes.ERROR_DATAFIELD242_AUTORENTAL_PICKUPCOUNTRYCODE);
/*  335 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateAutoRentalPickupDate(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  352 */     ErrorObject errorObj = null;
/*      */     
/*  354 */     if (CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  356 */       errorObj = new ErrorObject(
/*  357 */         SubmissionErrorCodes.ERROR_DATAFIELD243_AUTORENTAL_PICKUPDATE);
/*  358 */       errorCodes.add(errorObj);
/*      */     }
/*  360 */     else if (CommonValidator.validateData(value, 
/*  361 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ā']))
/*      */     {
/*  363 */       String reqLength = CommonValidator.validateLength(value, 8, 8);
/*      */       
/*  365 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  367 */         errorObj = new ErrorObject(
/*  368 */           SubmissionErrorCodes.ERROR_DATAFIELD243_AUTORENTAL_PICKUPDATE);
/*  369 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */     else {
/*  373 */       errorObj = new ErrorObject(
/*  374 */         SubmissionErrorCodes.ERROR_DATAFIELD243_AUTORENTAL_PICKUPDATE);
/*  375 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateAutoRentalPickupTime(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  392 */     ErrorObject errorObj = null;
/*      */     
/*  394 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  396 */       if (CommonValidator.validateData(value, 
/*  397 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ā']))
/*      */       {
/*  399 */         String reqLength = CommonValidator.validateLength(value, 6, 6);
/*      */         
/*  401 */         if (reqLength.equals("greaterThanMax"))
/*      */         {
/*  403 */           errorObj = new ErrorObject(
/*  404 */             SubmissionErrorCodes.ERROR_DATAFIELD244_AUTORENTAL_PICKUPTIME);
/*  405 */           errorCodes.add(errorObj);
/*      */         }
/*      */       }
/*      */       else {
/*  409 */         errorObj = new ErrorObject(
/*  410 */           SubmissionErrorCodes.ERROR_DATAFIELD244_AUTORENTAL_PICKUPTIME);
/*  411 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateAutoRentalReturnCityName(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  428 */     ErrorObject errorObj = null;
/*      */     
/*  430 */     if (CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  432 */       errorObj = new ErrorObject(
/*  433 */         SubmissionErrorCodes.ERROR_DATAFIELD245_AUTORENTAL_RETURNCITYNAME);
/*  434 */       errorCodes.add(errorObj);
/*      */ 
/*      */     }
/*      */     else
/*      */     {
/*      */ 
/*  440 */       String reqLength = 
/*  441 */         CommonValidator.validateLength(value, 18, 18);
/*      */       
/*  443 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  445 */         errorObj = new ErrorObject("AUTO", 
/*  446 */           "AUTO RentalReturnCityName Length is Out Of Sequence");
/*  447 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateAutoRentalReturnRegionCode(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  470 */     ErrorObject errorObj = null;
/*      */     
/*  472 */     if (CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  474 */       errorObj = new ErrorObject(
/*  475 */         SubmissionErrorCodes.ERROR_DATAFIELD246_AUTORENTAL_RETURNREGIONCODE);
/*  476 */       errorCodes.add(errorObj);
/*      */     }
/*  478 */     else if (CommonValidator.validateData(value, 
/*  479 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ą']))
/*      */     {
/*  481 */       String reqLength = CommonValidator.validateLength(value, 3, 3);
/*      */       
/*  483 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  485 */         errorObj = new ErrorObject(
/*  486 */           SubmissionErrorCodes.ERROR_DATAFIELD246_AUTORENTAL_RETURNREGIONCODE);
/*  487 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/*  490 */       errorObj = new ErrorObject(
/*  491 */         SubmissionErrorCodes.ERROR_DATAFIELD246_AUTORENTAL_RETURNREGIONCODE);
/*  492 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateAutoRentalReturnCountryCode(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  509 */     ErrorObject errorObj = null;
/*      */     
/*  511 */     if (CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  513 */       errorObj = new ErrorObject(
/*  514 */         SubmissionErrorCodes.ERROR_DATAFIELD247_AUTORENTAL_RETURNCOUNTRYCODE);
/*  515 */       errorCodes.add(errorObj);
/*      */     }
/*  517 */     else if (CommonValidator.validateData(value, 
/*  518 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ą']))
/*      */     {
/*  520 */       String reqLength = CommonValidator.validateLength(value, 3, 3);
/*      */       
/*  522 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  524 */         errorObj = new ErrorObject(
/*  525 */           SubmissionErrorCodes.ERROR_DATAFIELD247_AUTORENTAL_RETURNCOUNTRYCODE);
/*  526 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */     else {
/*  530 */       errorObj = new ErrorObject(
/*  531 */         SubmissionErrorCodes.ERROR_DATAFIELD247_AUTORENTAL_RETURNCOUNTRYCODE);
/*  532 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateAutoRentalReturnDate(AutoRentalIndustryTAABean autoRentalIndustryTAABean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  550 */     ErrorObject errorObj = null;
/*      */     
/*  552 */     String returnDate = autoRentalIndustryTAABean.getAutoRentalReturnDate();
/*      */     
/*  554 */     if (CommonValidator.isNullOrEmpty(returnDate))
/*      */     {
/*  556 */       errorObj = new ErrorObject(
/*  557 */         SubmissionErrorCodes.ERROR_DATAFIELD248_AUTORENTAL_RETURNDATE);
/*  558 */       errorCodes.add(errorObj);
/*      */     }
/*  560 */     else if (CommonValidator.validateData(returnDate, 
/*  561 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ć']))
/*      */     {
/*  563 */       String reqLength = CommonValidator.validateLength(returnDate, 
/*  564 */         8, 8);
/*      */       
/*  566 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  568 */         errorObj = new ErrorObject(
/*  569 */           SubmissionErrorCodes.ERROR_DATAFIELD248_AUTORENTAL_RETURNDATE);
/*  570 */         errorCodes.add(errorObj);
/*      */ 
/*      */ 
/*      */ 
/*      */       }
/*  575 */       else if (Integer.parseInt(returnDate) < Integer.parseInt(autoRentalIndustryTAABean
/*  576 */         .getAutoRentalPickupDate()))
/*      */       {
/*  578 */         errorObj = new ErrorObject(
/*  579 */           SubmissionErrorCodes.ERROR_DATAFIELD249_AUTORENTAL_RETURNDATE);
/*  580 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */     else
/*      */     {
/*  585 */       errorObj = new ErrorObject(
/*  586 */         SubmissionErrorCodes.ERROR_DATAFIELD248_AUTORENTAL_RETURNDATE);
/*  587 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateAutoRentalReturnTime(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  604 */     ErrorObject errorObj = null;
/*      */     
/*  606 */     if (CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  608 */       errorObj = new ErrorObject("AUTO", 
/*  609 */         "AUTO RentalReturnTime is Missing");
/*  610 */       errorCodes.add(errorObj);
/*      */     }
/*  612 */     else if (CommonValidator.validateData(value, 
/*  613 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ć']))
/*      */     {
/*  615 */       String reqLength = CommonValidator.validateLength(value, 6, 6);
/*      */       
/*  617 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  619 */         errorObj = new ErrorObject(
/*  620 */           SubmissionErrorCodes.ERROR_DATAFIELD250_AUTO_RENTALRETURNTIME);
/*  621 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */     else {
/*  625 */       errorObj = new ErrorObject(
/*  626 */         SubmissionErrorCodes.ERROR_DATAFIELD250_AUTO_RENTALRETURNTIME);
/*  627 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateAutoRentalRenterName(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  644 */     ErrorObject errorObj = null;
/*      */     
/*  646 */     if (CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  648 */       errorObj = new ErrorObject(
/*  649 */         SubmissionErrorCodes.ERROR_DATAFIELD251_AUTO_RENTAL_RENTERNAME);
/*  650 */       errorCodes.add(errorObj);
/*      */ 
/*      */     }
/*      */     else
/*      */     {
/*      */ 
/*  656 */       String reqLength = 
/*  657 */         CommonValidator.validateLength(value, 26, 26);
/*      */       
/*  659 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  661 */         errorObj = new ErrorObject("AUTO", 
/*  662 */           "AUTO AutoRentalRenterName Length is Out Of Sequence ");
/*  663 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateAutoRentalVehicleClassId(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  686 */     ErrorObject errorObj = null;
/*      */     
/*  688 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  690 */       if (CommonValidator.validateData(value, 
/*  691 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ĉ']))
/*      */       {
/*  693 */         String reqLength = CommonValidator.validateLength(value, 4, 4);
/*      */         
/*  695 */         if (reqLength.equals("greaterThanMax"))
/*      */         {
/*  697 */           errorObj = new ErrorObject(
/*  698 */             SubmissionErrorCodes.ERROR_DATAFIELD252_AUTO_RENTAL_VEHICLECLASSID);
/*  699 */           errorCodes.add(errorObj);
/*      */         }
/*      */       }
/*      */       else {
/*  703 */         errorObj = new ErrorObject(
/*  704 */           SubmissionErrorCodes.ERROR_DATAFIELD252_AUTO_RENTAL_VEHICLECLASSID);
/*  705 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateAutoRentalDistance(AutoRentalIndustryTAABean autoRentalIndustryTAABean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  723 */     ErrorObject errorObj = null;
/*      */     
/*  725 */     if (CommonValidator.isNullOrEmpty(autoRentalIndustryTAABean
/*  726 */       .getAutoRentalDistance()))
/*  727 */       if (!CommonValidator.isNullOrEmpty(autoRentalIndustryTAABean
/*  728 */         .getAutoRentalDistanceUnitOfMeasure()))
/*      */       {
/*  730 */         errorObj = new ErrorObject(
/*  731 */           SubmissionErrorCodes.ERROR_DATAFIELD254_AUTORENTAL_DISTANCE
/*  732 */           .getErrorCode(), 
/*  733 */           SubmissionErrorCodes.ERROR_DATAFIELD254_AUTORENTAL_DISTANCE
/*  734 */           .getErrorDescription() + 
/*  735 */           "\n" + 
/*  736 */           "RecordType:" + 
/*  737 */           recordType + 
/*  738 */           "|" + 
/*  739 */           "RecordNumber:" + 
/*  740 */           recordNumber + 
/*  741 */           "|" + 
/*  742 */           "AutoRentalDistance" + 
/*  743 */           "|" + 
/*  744 */           SubmissionErrorCodes.ERROR_DATAFIELD254_AUTORENTAL_DISTANCE
/*  745 */           .getErrorDescription());
/*  746 */         errorCodes.add(errorObj); return;
/*      */       }
/*  748 */     if (!CommonValidator.isNullOrEmpty(autoRentalIndustryTAABean
/*  749 */       .getAutoRentalDistance()))
/*      */     {
/*  751 */       if (CommonValidator.validateData(autoRentalIndustryTAABean
/*  752 */         .getAutoRentalDistance(), 
/*  753 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ċ']))
/*      */       {
/*  755 */         String reqLength = 
/*  756 */           CommonValidator.validateLength(autoRentalIndustryTAABean
/*  757 */           .getAutoRentalDistance(), 5, 5);
/*      */         
/*  759 */         if (reqLength.equals("greaterThanMax"))
/*      */         {
/*  761 */           errorObj = new ErrorObject(
/*  762 */             SubmissionErrorCodes.ERROR_DATAFIELD0253_AUTORENTAL_DISTANCE
/*  763 */             .getErrorCode(), 
/*  764 */             SubmissionErrorCodes.ERROR_DATAFIELD0253_AUTORENTAL_DISTANCE
/*  765 */             .getErrorDescription() + 
/*  766 */             "\n" + 
/*  767 */             "RecordType:" + 
/*  768 */             recordType + 
/*  769 */             "|" + 
/*  770 */             "RecordNumber:" + 
/*  771 */             recordNumber + 
/*  772 */             "|" + 
/*  773 */             "AutoRentalDistance" + 
/*  774 */             "|" + 
/*  775 */             "This field length Cannot be greater than 5");
/*  776 */           errorCodes.add(errorObj);
/*      */         }
/*      */       }
/*      */       else {
/*  780 */         errorObj = new ErrorObject(
/*  781 */           SubmissionErrorCodes.ERROR_DATAFIELD253_AUTORENTAL_DISTANCE
/*  782 */           .getErrorCode(), 
/*  783 */           SubmissionErrorCodes.ERROR_DATAFIELD253_AUTORENTAL_DISTANCE
/*  784 */           .getErrorDescription() + 
/*  785 */           "\n" + 
/*  786 */           "RecordType:" + 
/*  787 */           recordType + 
/*  788 */           "|" + 
/*  789 */           "RecordNumber:" + 
/*  790 */           recordNumber + 
/*  791 */           "|" + 
/*  792 */           "AutoRentalDistance" + 
/*  793 */           "|" + 
/*  794 */           "This field can only be Numeric");
/*  795 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateAutoRentalDistanceUnitOfMeasure(AutoRentalIndustryTAABean autoRentalIndustryTAABean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  813 */     ErrorObject errorObj = null;
/*      */     
/*  815 */     if (CommonValidator.isNullOrEmpty(autoRentalIndustryTAABean
/*  816 */       .getAutoRentalDistanceUnitOfMeasure()))
/*  817 */       if (!CommonValidator.isNullOrEmpty(autoRentalIndustryTAABean
/*  818 */         .getAutoRentalDistance()))
/*      */       {
/*  820 */         errorObj = new ErrorObject(
/*  821 */           SubmissionErrorCodes.ERROR_DATAFIELD256_AUTORENTAL_DISTANCEUNITOFMEASURE
/*  822 */           .getErrorCode(), 
/*  823 */           SubmissionErrorCodes.ERROR_DATAFIELD256_AUTORENTAL_DISTANCEUNITOFMEASURE
/*  824 */           .getErrorDescription() + 
/*  825 */           "\n" + 
/*  826 */           "RecordType:" + 
/*  827 */           recordType + 
/*  828 */           "|" + 
/*  829 */           "RecordNumber:" + 
/*  830 */           recordNumber + 
/*  831 */           "|" + 
/*  832 */           "AutoRentalDistanceUnitOfMeasure" + 
/*  833 */           "|" + 
/*  834 */           SubmissionErrorCodes.ERROR_DATAFIELD256_AUTORENTAL_DISTANCEUNITOFMEASURE
/*  835 */           .getErrorDescription());
/*  836 */         errorCodes.add(errorObj); return;
/*      */       }
/*  838 */     if (!CommonValidator.isNullOrEmpty(autoRentalIndustryTAABean
/*  839 */       .getAutoRentalDistanceUnitOfMeasure()))
/*      */     {
/*      */ 
/*  842 */       if (!CommonValidator.isAutoRentalDistanceUnitOfMessure(autoRentalIndustryTAABean
/*  843 */         .getAutoRentalDistanceUnitOfMeasure())) {
/*  844 */         errorObj = new ErrorObject(
/*  845 */           SubmissionErrorCodes.ERROR_DATAFIELD255_AUTORENTAL_DISTANCEUNITOFMEASURE
/*  846 */           .getErrorCode(), 
/*  847 */           SubmissionErrorCodes.ERROR_DATAFIELD255_AUTORENTAL_DISTANCEUNITOFMEASURE
/*  848 */           .getErrorDescription() + 
/*  849 */           "\n" + 
/*  850 */           "RecordType:" + 
/*  851 */           recordType + 
/*  852 */           "|" + 
/*  853 */           "RecordNumber:" + 
/*  854 */           recordNumber + 
/*  855 */           "|" + 
/*  856 */           "AutoRentalDistanceUnitOfMeasure" + 
/*  857 */           "|" + 
/*  858 */           SubmissionErrorCodes.ERROR_DATAFIELD255_AUTORENTAL_DISTANCEUNITOFMEASURE
/*  859 */           .getErrorDescription());
/*  860 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateAutoRentalAuditAdjustmentIndicator(AutoRentalIndustryTAABean autoIndustryTAABean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  880 */     ErrorObject errorObj = null;
/*      */     
/*  882 */     if (!CommonValidator.isNullOrEmpty(autoIndustryTAABean
/*  883 */       .getAutoRentalAuditAdjustmentIndicator()))
/*      */     {
/*      */ 
/*  886 */       if (!autoIndustryTAABean.getAutoRentalAuditAdjustmentIndicator().equalsIgnoreCase("X"))
/*      */       {
/*      */ 
/*  889 */         if (!autoIndustryTAABean.getAutoRentalAuditAdjustmentIndicator().equalsIgnoreCase("Y"))
/*      */         {
/*      */ 
/*  892 */           if (!autoIndustryTAABean.getAutoRentalAuditAdjustmentIndicator().equalsIgnoreCase("Z"))
/*      */           {
/*  894 */             errorObj = new ErrorObject(
/*  895 */               SubmissionErrorCodes.ERROR_DATAFIELD257_AUTORENTAL_AUDITADJUSTMENTINDICATOR);
/*  896 */             errorCodes.add(errorObj); return;
/*      */           } } } }
/*  898 */     if (!CommonValidator.isNullOrEmpty(autoIndustryTAABean
/*  899 */       .getAutoRentalAuditAdjustmentIndicator()))
/*      */     {
/*  901 */       if (CommonValidator.validateData(autoIndustryTAABean
/*  902 */         .getAutoRentalAuditAdjustmentIndicator(), 
/*  903 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Č']))
/*      */       {
/*  905 */         String reqLength = CommonValidator.validateLength(
/*  906 */           autoIndustryTAABean
/*  907 */           .getAutoRentalAuditAdjustmentIndicator(), 1, 1);
/*      */         
/*  909 */         if (reqLength.equals("greaterThanMax"))
/*      */         {
/*  911 */           errorObj = new ErrorObject(
/*  912 */             SubmissionErrorCodes.ERROR_DATAFIELD257_AUTORENTAL_AUDITADJUSTMENTINDICATOR);
/*  913 */           errorCodes.add(errorObj);
/*      */ 
/*      */ 
/*      */         }
/*  917 */         else if (CommonValidator.isNullOrEmpty(autoIndustryTAABean
/*  918 */           .getAutoRentalAuditAdjustmentAmount()))
/*      */         {
/*  920 */           errorObj = new ErrorObject(
/*  921 */             SubmissionErrorCodes.ERROR_DATAFIELD261_AUTORENTAL_AUDITADJUSTMENTAMOUNT);
/*  922 */           errorCodes.add(errorObj);
/*      */         }
/*      */       }
/*      */       else
/*      */       {
/*  927 */         errorObj = new ErrorObject(
/*  928 */           SubmissionErrorCodes.ERROR_DATAFIELD257_AUTORENTAL_AUDITADJUSTMENTINDICATOR);
/*  929 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateAutoRentalAuditAdjustmentAmount(AutoRentalIndustryTAABean autoRentalIndustryTAABean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  947 */     ErrorObject errorObj = null;
/*      */     
/*  949 */     if (!CommonValidator.isNullOrEmpty(autoRentalIndustryTAABean
/*  950 */       .getAutoRentalAuditAdjustmentAmount()))
/*      */     {
/*  952 */       if (CommonValidator.validateData(autoRentalIndustryTAABean
/*  953 */         .getAutoRentalAuditAdjustmentAmount(), 
/*  954 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ď']))
/*      */       {
/*  956 */         String reqLength = CommonValidator.validateLength(
/*  957 */           autoRentalIndustryTAABean
/*  958 */           .getAutoRentalAuditAdjustmentAmount(), 12, 12);
/*      */         
/*  960 */         if (reqLength.equals("greaterThanMax"))
/*      */         {
/*  962 */           errorObj = new ErrorObject(
/*  963 */             SubmissionErrorCodes.ERROR_DATAFIELD260_AUTORENTAL_AUDITADJUSTMENTAMOUNT);
/*  964 */           errorCodes.add(errorObj);
/*      */ 
/*      */ 
/*      */         }
/*  968 */         else if (CommonValidator.isNullOrEmpty(autoRentalIndustryTAABean
/*  969 */           .getAutoRentalAuditAdjustmentIndicator()))
/*      */         {
/*  971 */           errorObj = new ErrorObject(
/*  972 */             SubmissionErrorCodes.ERROR_DATAFIELD258_AUTORENTAL_AUDITADJUSTMENTINDICATOR);
/*  973 */           errorCodes.add(errorObj);
/*      */         }
/*      */       }
/*      */       else
/*      */       {
/*  978 */         errorObj = new ErrorObject(
/*  979 */           SubmissionErrorCodes.ERROR_DATAFIELD259_AUTORENTAL_AUDITADJUSTMENTAMOUNT);
/*  980 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateReturnDropoffLocation(AutoRentalIndustryTAABean autoRentalIndustryTAABean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  998 */     ErrorObject errorObj = null;
/*      */     
/* 1000 */     if (!CommonValidator.isNullOrEmpty(autoRentalIndustryTAABean
/* 1001 */       .getReturnDropoffLocation()))
/*      */     {
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1007 */       String reqLength = CommonValidator.validateLength(
/* 1008 */         autoRentalIndustryTAABean
/* 1009 */         .getReturnDropoffLocation(), 38, 38);
/*      */       
/* 1011 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/* 1013 */         errorObj = new ErrorObject(
/* 1014 */           SubmissionErrorCodes.ERROR_DATAFIELD613_AUTORENTAL_RETURNDROPOFFLOCATION);
/* 1015 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateVehicleIdentificationNumber(AutoRentalIndustryTAABean autoRentalIndustryTAABean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1040 */     ErrorObject errorObj = null;
/*      */     
/* 1042 */     if (!CommonValidator.isNullOrEmpty(autoRentalIndustryTAABean
/* 1043 */       .getVehicleIdentificationNumber()))
/*      */     {
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1049 */       String reqLength = CommonValidator.validateLength(
/* 1050 */         autoRentalIndustryTAABean
/* 1051 */         .getVehicleIdentificationNumber(), 20, 20);
/*      */       
/* 1053 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/* 1055 */         errorObj = new ErrorObject(
/* 1056 */           SubmissionErrorCodes.ERROR_DATAFIELD614_AUTORENTAL_VEHICLEIDNUMBER);
/* 1057 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateDriverIdentificationNumber(AutoRentalIndustryTAABean autoRentalIndustryTAABean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1082 */     ErrorObject errorObj = null;
/*      */     
/* 1084 */     if (!CommonValidator.isNullOrEmpty(autoRentalIndustryTAABean
/* 1085 */       .getDriverIdentificationNumber()))
/*      */     {
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1091 */       String reqLength = CommonValidator.validateLength(
/* 1092 */         autoRentalIndustryTAABean
/* 1093 */         .getDriverIdentificationNumber(), 20, 20);
/*      */       
/* 1095 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/* 1097 */         errorObj = new ErrorObject(
/* 1098 */           SubmissionErrorCodes.ERROR_DATAFIELD615_AUTORENTAL_DRIVERIDNUMBER);
/* 1099 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateDriverTaxNumber(AutoRentalIndustryTAABean autoRentalIndustryTAABean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1124 */     ErrorObject errorObj = null;
/*      */     
/* 1126 */     if (!CommonValidator.isNullOrEmpty(autoRentalIndustryTAABean
/* 1127 */       .getDriverTaxNumber()))
/*      */     {
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1133 */       String reqLength = CommonValidator.validateLength(
/* 1134 */         autoRentalIndustryTAABean
/* 1135 */         .getDriverTaxNumber(), 20, 20);
/*      */       
/* 1137 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/* 1139 */         errorObj = new ErrorObject(
/* 1140 */           SubmissionErrorCodes.ERROR_DATAFIELD616_AUTORENTAL_DRIVERTAXNUMBER);
/* 1141 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\validator\industry\AutoRentalRecordValidator.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */