/*     */ package com.americanexpress.ips.gfsg.validator.industry;
/*     */ 
/*     */ import com.americanexpress.ips.gfsg.beans.ErrorObject;
/*     */ import com.americanexpress.ips.gfsg.beans.common.TransactionAdviceAddendumBean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.TransactionAdviceBasicBean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.CommunicationServicesIndustryBean;
/*     */ import com.americanexpress.ips.gfsg.enumerations.SubmissionErrorCodes;
/*     */ import com.americanexpress.ips.gfsg.exceptions.SettlementException;
/*     */ import com.americanexpress.ips.gfsg.validator.CommonValidator;
/*     */ import java.util.List;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class CMSIndRecordValidator
/*     */ {
/*     */   public static void validateCMSIndRecord(TransactionAdviceBasicBean transactionAdviceBasicType, TransactionAdviceAddendumBean transactionAddendumType, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/*  37 */     CommunicationServicesIndustryBean communicationServicesIndustryBean = (CommunicationServicesIndustryBean)transactionAddendumType;
/*     */     
/*     */ 
/*  40 */     TAADBRecordValidator.validateRecordType(
/*  41 */       communicationServicesIndustryBean.getRecordType(), errorCodes);
/*     */     
/*  43 */     TAADBRecordValidator.validateRecordNumber(communicationServicesIndustryBean
/*  44 */       .getRecordNumber(), errorCodes);
/*  45 */     TAADBRecordValidator.validateTransactionIdentifier(
/*  46 */       communicationServicesIndustryBean.getTransactionIdentifier(), 
/*  47 */       transactionAdviceBasicType, errorCodes);
/*  48 */     TAADBRecordValidator.validateAddendaTypeCode(
/*  49 */       communicationServicesIndustryBean.getAddendaTypeCode(), 
/*  50 */       errorCodes);
/*  51 */     TAADBRecordValidator.validateFormatCode(
/*  52 */       communicationServicesIndustryBean.getFormatCode(), 
/*  53 */       transactionAdviceBasicType, errorCodes);
/*     */     
/*  55 */     validateCallDate(communicationServicesIndustryBean.getCallDate(), 
/*  56 */       errorCodes);
/*  57 */     validateCallTime(communicationServicesIndustryBean.getCallTime(), 
/*  58 */       errorCodes);
/*  59 */     validateCallDurationTime(communicationServicesIndustryBean
/*  60 */       .getCallDurationTime(), errorCodes);
/*  61 */     validateCallFromLocationName(communicationServicesIndustryBean
/*  62 */       .getCallFromLocationName(), errorCodes);
/*  63 */     validateCallFromRegionCode(communicationServicesIndustryBean
/*  64 */       .getCallFromRegionCode(), errorCodes);
/*  65 */     validateCallFromCountryCOde(communicationServicesIndustryBean
/*  66 */       .getCallFromCountryCode(), errorCodes);
/*     */     
/*  68 */     validateCallFromPhoneNumber(communicationServicesIndustryBean
/*  69 */       .getCallFromPhoneNumber(), errorCodes);
/*  70 */     validateCallToLocationName(communicationServicesIndustryBean
/*  71 */       .getCallToLocationName(), errorCodes);
/*     */     
/*  73 */     validateCallToRegionCode(communicationServicesIndustryBean
/*  74 */       .getCallToRegionCode(), errorCodes);
/*  75 */     validateCallToCountryCode(communicationServicesIndustryBean
/*  76 */       .getCallToCountryCode(), errorCodes);
/*     */     
/*  78 */     validateCallToPhoneNumber(communicationServicesIndustryBean
/*  79 */       .getCallToPhoneNumber(), errorCodes);
/*     */     
/*  81 */     validatePhoneCardId(communicationServicesIndustryBean.getPhoneCardId(), 
/*  82 */       errorCodes);
/*  83 */     validateServiceDescription(communicationServicesIndustryBean
/*  84 */       .getServiceDescription(), errorCodes);
/*  85 */     validateBillingPeriod(communicationServicesIndustryBean
/*  86 */       .getBillingPeriod(), errorCodes);
/*     */     
/*  88 */     validateCommunicationsCallTypeCode(communicationServicesIndustryBean
/*  89 */       .getCommunicationsCallTypeCode(), errorCodes);
/*     */     
/*  91 */     validateCommunicationsRateClass(communicationServicesIndustryBean
/*  92 */       .getCommunicationsRateClass(), errorCodes);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateCallDate(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 108 */     ErrorObject errorObj = null;
/*     */     
/* 110 */     if (CommonValidator.isNullOrEmpty(value))
/*     */     {
/*     */ 
/* 113 */       errorObj = new ErrorObject(
/* 114 */         SubmissionErrorCodes.ERROR_DATAFIELD262_CALLDATE);
/* 115 */       errorCodes.add(errorObj);
/*     */     }
/* 117 */     else if (CommonValidator.validateData(value, 
/* 118 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ĕ']))
/*     */     {
/* 120 */       String reqLength = CommonValidator.validateLength(value, 8, 8);
/*     */       
/* 122 */       if ((!reqLength.equals("equal")) || 
/* 123 */         (!CommonValidator.isValidDate(value, 
/* 124 */         "CCYYMMDD")))
/*     */       {
/* 126 */         errorObj = new ErrorObject(
/* 127 */           SubmissionErrorCodes.ERROR_DATAFIELD262_CALLDATE);
/* 128 */         errorCodes.add(errorObj);
/*     */       }
/*     */     } else {
/* 131 */       errorObj = new ErrorObject(
/* 132 */         SubmissionErrorCodes.ERROR_DATAFIELD262_CALLDATE);
/* 133 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateCallTime(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 150 */     ErrorObject errorObj = null;
/*     */     
/* 152 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 154 */       if (CommonValidator.validateData(value, 
/* 155 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ĕ']))
/*     */       {
/* 157 */         String reqLength = CommonValidator.validateLength(value, 6, 6);
/*     */         
/* 159 */         if (reqLength.equals("greaterThanMax"))
/*     */         {
/* 161 */           errorObj = new ErrorObject(
/* 162 */             SubmissionErrorCodes.ERROR_DATAFIELD263_CALLDATECALL_TIME);
/* 163 */           errorCodes.add(errorObj);
/*     */         }
/*     */       } else {
/* 166 */         errorObj = new ErrorObject(
/* 167 */           SubmissionErrorCodes.ERROR_DATAFIELD263_CALLDATECALL_TIME);
/* 168 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateCallDurationTime(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 185 */     ErrorObject errorObj = null;
/*     */     
/* 187 */     if (CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 189 */       errorObj = new ErrorObject(
/* 190 */         SubmissionErrorCodes.ERROR_DATAFIELD264_CALL_DURATIONTIME);
/* 191 */       errorCodes.add(errorObj);
/*     */     }
/* 193 */     else if (CommonValidator.validateData(value, 
/* 194 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ė']))
/*     */     {
/* 196 */       String reqLength = CommonValidator.validateLength(value, 6, 6);
/*     */       
/* 198 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/* 200 */         errorObj = new ErrorObject(
/* 201 */           SubmissionErrorCodes.ERROR_DATAFIELD264_CALL_DURATIONTIME);
/* 202 */         errorCodes.add(errorObj);
/*     */       }
/*     */     } else {
/* 205 */       errorObj = new ErrorObject(
/* 206 */         SubmissionErrorCodes.ERROR_DATAFIELD264_CALL_DURATIONTIME);
/* 207 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateCallFromLocationName(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 224 */     ErrorObject errorObj = null;
/*     */     
/* 226 */     if (CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 228 */       errorObj = new ErrorObject(
/* 229 */         SubmissionErrorCodes.ERROR_DATAFIELD265_CALLFROM_LOCATIONNAME);
/* 230 */       errorCodes.add(errorObj);
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/* 236 */       String reqLength = 
/* 237 */         CommonValidator.validateLength(value, 18, 18);
/*     */       
/* 239 */       if (reqLength.equals("greaterThanMax")) {
/* 240 */         errorObj = new ErrorObject(
/* 241 */           SubmissionErrorCodes.ERROR_DATAFIELD266_CALLFROM_LOCATIONNAME);
/* 242 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateCallFromRegionCode(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 265 */     ErrorObject errorObj = null;
/*     */     
/* 267 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 269 */       if (CommonValidator.validateData(value, 
/* 270 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ę']))
/*     */       {
/* 272 */         String reqLength = CommonValidator.validateLength(value, 3, 3);
/*     */         
/* 274 */         if (reqLength.equals("greaterThanMax")) {
/* 275 */           errorObj = new ErrorObject(
/* 276 */             SubmissionErrorCodes.ERROR_DATAFIELD268_CALLFROM_REGIONCODE);
/* 277 */           errorCodes.add(errorObj);
/*     */         }
/*     */       } else {
/* 280 */         errorObj = new ErrorObject(
/* 281 */           SubmissionErrorCodes.ERROR_DATAFIELD269_CALLFROM_REGIONCODE);
/* 282 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateCallFromCountryCOde(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 299 */     ErrorObject errorObj = null;
/*     */     
/* 301 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 303 */       if (CommonValidator.validateData(value, 
/* 304 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ę']))
/*     */       {
/* 306 */         String reqLength = CommonValidator.validateLength(value, 3, 3);
/*     */         
/* 308 */         if (reqLength.equals("greaterThanMax")) {
/* 309 */           errorObj = new ErrorObject(
/* 310 */             SubmissionErrorCodes.ERROR_DATAFIELD271_CALLFROM_COUNTRYCODE);
/* 311 */           errorCodes.add(errorObj);
/*     */         }
/*     */       } else {
/* 314 */         errorObj = new ErrorObject(
/* 315 */           SubmissionErrorCodes.ERROR_DATAFIELD270_CALLFROM_COUNTRYCODE);
/* 316 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateCallFromPhoneNumber(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 333 */     ErrorObject errorObj = null;
/*     */     
/* 335 */     if (CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 337 */       errorObj = new ErrorObject(
/* 338 */         SubmissionErrorCodes.ERROR_DATAFIELD272_CALLFROM_PHONENUMBER);
/* 339 */       errorCodes.add(errorObj);
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/* 346 */       String reqLength = 
/* 347 */         CommonValidator.validateLength(value, 16, 16);
/*     */       
/* 349 */       if (reqLength.equals("greaterThanMax")) {
/* 350 */         errorObj = new ErrorObject(
/* 351 */           SubmissionErrorCodes.ERROR_DATAFIELD274_CALLFROM_PHONENUMBER);
/* 352 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateCallToLocationName(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 374 */     ErrorObject errorObj = null;
/*     */     
/* 376 */     if (CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 378 */       errorObj = new ErrorObject(
/* 379 */         SubmissionErrorCodes.ERROR_DATAFIELD275_CALLTO_LOCATIONNAME);
/* 380 */       errorCodes.add(errorObj);
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/* 387 */       String reqLength = 
/* 388 */         CommonValidator.validateLength(value, 18, 18);
/*     */       
/* 390 */       if (reqLength.equals("greaterThanMax")) {
/* 391 */         errorObj = new ErrorObject(
/* 392 */           SubmissionErrorCodes.ERROR_DATAFIELD277_CALLTO_LOCATIONNAME);
/* 393 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateCallToRegionCode(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 415 */     ErrorObject errorObj = null;
/*     */     
/* 417 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 419 */       if (CommonValidator.validateData(value, 
/* 420 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ĝ']))
/*     */       {
/* 422 */         String reqLength = CommonValidator.validateLength(value, 3, 3);
/*     */         
/* 424 */         if (reqLength.equals("greaterThanMax")) {
/* 425 */           errorObj = new ErrorObject(
/* 426 */             SubmissionErrorCodes.ERROR_DATAFIELD279_CALLTO_REGIONCODE);
/* 427 */           errorCodes.add(errorObj);
/*     */         }
/*     */       } else {
/* 430 */         errorObj = new ErrorObject(
/* 431 */           SubmissionErrorCodes.ERROR_DATAFIELD278_CALLTO_REGIONCODE);
/* 432 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateCallToCountryCode(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 449 */     ErrorObject errorObj = null;
/*     */     
/* 451 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 453 */       if (CommonValidator.validateData(value, 
/* 454 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ĝ']))
/*     */       {
/* 456 */         String reqLength = CommonValidator.validateLength(value, 3, 3);
/*     */         
/* 458 */         if (reqLength.equals("greaterThanMax")) {
/* 459 */           errorObj = new ErrorObject(
/* 460 */             SubmissionErrorCodes.ERROR_DATAFIELD280_CALLTO_COUNTRYCODE);
/* 461 */           errorCodes.add(errorObj);
/*     */         }
/*     */       } else {
/* 464 */         errorObj = new ErrorObject(
/* 465 */           SubmissionErrorCodes.ERROR_DATAFIELD279_CALLTO_COUNTRYCODE);
/* 466 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateCallToPhoneNumber(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 483 */     ErrorObject errorObj = null;
/*     */     
/* 485 */     if (CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 487 */       errorObj = new ErrorObject(
/* 488 */         SubmissionErrorCodes.ERROR_DATAFIELD281_CALLTO_PHONENUMBER);
/* 489 */       errorCodes.add(errorObj);
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/* 495 */       String reqLength = 
/* 496 */         CommonValidator.validateLength(value, 16, 16);
/*     */       
/* 498 */       if (reqLength.equals("greaterThanMax")) {
/* 499 */         errorObj = new ErrorObject(
/* 500 */           SubmissionErrorCodes.ERROR_DATAFIELD283_CALLTO_PHONENUMBER);
/* 501 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validatePhoneCardId(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 523 */     ErrorObject errorObj = null;
/*     */     
/* 525 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/* 530 */       String reqLength = CommonValidator.validateLength(value, 8, 8);
/*     */       
/* 532 */       if (reqLength.equals("greaterThanMax")) {
/* 533 */         errorObj = new ErrorObject(
/* 534 */           SubmissionErrorCodes.ERROR_DATAFIELD284_PHONECARDID);
/* 535 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateServiceDescription(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 557 */     ErrorObject errorObj = null;
/*     */     
/* 559 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 565 */       String reqLength = 
/* 566 */         CommonValidator.validateLength(value, 20, 20);
/*     */       
/* 568 */       if (reqLength.equals("greaterThanMax")) {
/* 569 */         errorObj = new ErrorObject(
/* 570 */           SubmissionErrorCodes.ERROR_DATAFIELD286_SERVICE_DESCRIPTION);
/* 571 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateBillingPeriod(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 593 */     ErrorObject errorObj = null;
/*     */     
/* 595 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 601 */       String reqLength = 
/* 602 */         CommonValidator.validateLength(value, 18, 18);
/*     */       
/* 604 */       if (reqLength.equals("greaterThanMax")) {
/* 605 */         errorObj = new ErrorObject(
/* 606 */           SubmissionErrorCodes.ERROR_DATAFIELD288_BILLING_PERIOD);
/* 607 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateCommunicationsCallTypeCode(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 629 */     if ((!CommonValidator.isNullOrEmpty(value)) && 
/* 630 */       (!CommonValidator.isValidCommunicationsCallTypeCode(value)))
/*     */     {
/* 632 */       ErrorObject errorObj = new ErrorObject(
/* 633 */         SubmissionErrorCodes.ERROR_DATAFIELD289_COMMUNICATIONS_CALLTYPECODE);
/* 634 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateCommunicationsRateClass(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 650 */     ErrorObject errorObj = null;
/*     */     
/* 652 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 654 */       if (CommonValidator.validateData(value, 
/* 655 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ģ'])) {
/* 656 */         String reqLength = CommonValidator.validateLength(value, 1, 1);
/*     */         
/* 658 */         if (reqLength.equals("greaterThanMax")) {
/* 659 */           errorObj = new ErrorObject(
/* 660 */             SubmissionErrorCodes.ERROR_DATAFIELD292_COMMUNICATIONS_RATECLASS);
/* 661 */           errorCodes.add(errorObj);
/*     */         }
/*     */       } else {
/* 664 */         errorObj = new ErrorObject(
/* 665 */           SubmissionErrorCodes.ERROR_DATAFIELD293_COMMUNICATIONS_RATECLASS);
/* 666 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\validator\industry\CMSIndRecordValidator.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */