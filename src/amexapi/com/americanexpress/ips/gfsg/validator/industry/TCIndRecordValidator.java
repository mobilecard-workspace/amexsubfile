/*      */ package com.americanexpress.ips.gfsg.validator.industry;
/*      */ 
/*      */ import com.americanexpress.ips.gfsg.beans.ErrorObject;
/*      */ import com.americanexpress.ips.gfsg.beans.common.TransactionAdviceAddendumBean;
/*      */ import com.americanexpress.ips.gfsg.beans.common.TransactionAdviceBasicBean;
/*      */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.TravelCruiseIndustryTAABean;
/*      */ import com.americanexpress.ips.gfsg.enumerations.SubmissionErrorCodes;
/*      */ import com.americanexpress.ips.gfsg.exceptions.SettlementException;
/*      */ import com.americanexpress.ips.gfsg.validator.CommonValidator;
/*      */ import java.util.List;
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ public class TCIndRecordValidator
/*      */ {
/*      */   public static void validateTCIndRecord(TransactionAdviceBasicBean transactionAdviceBasicType, TransactionAdviceAddendumBean transactionAddendumType, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*   38 */     TravelCruiseIndustryTAABean tIndustryTAABean = (TravelCruiseIndustryTAABean)transactionAddendumType;
/*      */     
/*      */ 
/*   41 */     TAADBRecordValidator.validateRecordType(tIndustryTAABean
/*   42 */       .getRecordType(), errorCodes);
/*   43 */     TAADBRecordValidator.validateRecordNumber(tIndustryTAABean
/*   44 */       .getRecordNumber(), errorCodes);
/*   45 */     TAADBRecordValidator.validateTransactionIdentifier(tIndustryTAABean
/*   46 */       .getTransactionIdentifier(), transactionAdviceBasicType, 
/*   47 */       errorCodes);
/*   48 */     TAADBRecordValidator.validateAddendaTypeCode(tIndustryTAABean
/*   49 */       .getAddendaTypeCode(), errorCodes);
/*   50 */     TAADBRecordValidator.validateFormatCode(tIndustryTAABean
/*   51 */       .getFormatCode(), transactionAdviceBasicType, errorCodes);
/*      */     
/*   53 */     validateIataCarrierCode(tIndustryTAABean.getIataCarrierCode(), 
/*   54 */       errorCodes);
/*   55 */     validateIataAgencyNumber(tIndustryTAABean.getIataAgencyNumber(), 
/*   56 */       errorCodes);
/*   57 */     validateTravelPackageIndicator(tIndustryTAABean
/*   58 */       .getTravelPackageIndicator(), errorCodes);
/*   59 */     validatePassengerName(tIndustryTAABean.getPassengerName(), errorCodes);
/*   60 */     validateTravelTicketNumber(tIndustryTAABean.getTravelTicketNumber(), 
/*   61 */       errorCodes);
/*   62 */     validateTravelDepartureDateSegment1(tIndustryTAABean
/*   63 */       .getTravelDepartureDateSegment1(), errorCodes);
/*   64 */     validateTravelDestinationCodeSegment1(tIndustryTAABean
/*   65 */       .getTravelDestinationCodeSegment1(), errorCodes);
/*   66 */     validateTravelDepartureAirportSegment1(tIndustryTAABean
/*   67 */       .getTravelDepartureAirportSegment1(), errorCodes);
/*   68 */     validateAirCarrierCodeSegment1(tIndustryTAABean
/*   69 */       .getAirCarrierCodeSegment1(), errorCodes);
/*   70 */     validateFlightNumberSegment1(
/*   71 */       tIndustryTAABean.getFlightNumberSegment1(), errorCodes);
/*   72 */     validateClassCodeSegment1(tIndustryTAABean.getClassCodeSegment1(), 
/*   73 */       errorCodes);
/*      */     
/*   75 */     validateClassCodeSegment2(tIndustryTAABean.getClassCodeSegment2(), 
/*   76 */       errorCodes);
/*   77 */     validateClassCodeSegment3(tIndustryTAABean.getClassCodeSegment3(), 
/*   78 */       errorCodes);
/*   79 */     validateClassCodeSegment4(tIndustryTAABean.getClassCodeSegment4(), 
/*   80 */       errorCodes);
/*      */     
/*   82 */     validateFlightNumberSegment2(
/*   83 */       tIndustryTAABean.getFlightNumberSegment2(), errorCodes);
/*   84 */     validateFlightNumberSegment3(
/*   85 */       tIndustryTAABean.getFlightNumberSegment3(), errorCodes);
/*   86 */     validateFlightNumberSegment4(
/*   87 */       tIndustryTAABean.getFlightNumberSegment4(), errorCodes);
/*      */     
/*   89 */     validateAirCarrierCodeSegment2(tIndustryTAABean
/*   90 */       .getAirCarrierCodeSegment2(), errorCodes);
/*   91 */     validateAirCarrierCodeSegment3(tIndustryTAABean
/*   92 */       .getAirCarrierCodeSegment3(), errorCodes);
/*   93 */     validateAirCarrierCodeSegment4(tIndustryTAABean
/*   94 */       .getAirCarrierCodeSegment4(), errorCodes);
/*      */     
/*   96 */     validateTravelDepartureAirportSegment2(tIndustryTAABean
/*   97 */       .getTravelDepartureAirportSegment2(), errorCodes);
/*   98 */     validateTravelDepartureAirportSegment3(tIndustryTAABean
/*   99 */       .getTravelDepartureAirportSegment3(), errorCodes);
/*  100 */     validateTravelDepartureAirportSegment4(tIndustryTAABean
/*  101 */       .getTravelDepartureAirportSegment4(), errorCodes);
/*      */     
/*  103 */     validateTravelDestinationCodeSegment2(tIndustryTAABean
/*  104 */       .getTravelDestinationCodeSegment2(), errorCodes);
/*  105 */     validateTravelDestinationCodeSegment3(tIndustryTAABean
/*  106 */       .getTravelDestinationCodeSegment3(), errorCodes);
/*  107 */     validateTravelDestinationCodeSegment4(tIndustryTAABean
/*  108 */       .getTravelDestinationCodeSegment4(), errorCodes);
/*      */     
/*  110 */     validateTravelDepartureDateSegment2(tIndustryTAABean
/*  111 */       .getTravelDepartureDateSegment2(), errorCodes);
/*  112 */     validateTravelDepartureDateSegment3(tIndustryTAABean
/*  113 */       .getTravelDepartureDateSegment3(), errorCodes);
/*  114 */     validateTravelDepartureDateSegment4(tIndustryTAABean
/*  115 */       .getTravelDepartureDateSegment4(), errorCodes);
/*      */     
/*  117 */     validateLodgingCheckInDate(tIndustryTAABean.getLodgingCheckInDate(), 
/*  118 */       errorCodes);
/*  119 */     validateLodgingCheckOutDate(tIndustryTAABean, errorCodes);
/*  120 */     validateLodgingRoomRate1(tIndustryTAABean.getLodgingRoomRate1(), 
/*  121 */       errorCodes);
/*  122 */     validateNumberOfNightsAtRoomRate1(tIndustryTAABean
/*  123 */       .getNumberOfNightsAtRoomRate1(), errorCodes);
/*  124 */     validateLodgingName(tIndustryTAABean.getLodgingName(), errorCodes);
/*  125 */     validateLodgingRegionCode(tIndustryTAABean.getLodgingRegionCode(), 
/*  126 */       errorCodes);
/*  127 */     validateLodgingCountryCode(tIndustryTAABean.getLodgingCountryCode(), 
/*  128 */       errorCodes);
/*  129 */     validateLodgingCityName(tIndustryTAABean.getLodgingCityName(), 
/*  130 */       errorCodes);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateIataCarrierCode(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  146 */     ErrorObject errorObj = null;
/*      */     
/*  148 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  150 */       if (CommonValidator.validateData(value, 
/*  151 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ĸ']))
/*      */       {
/*  153 */         String reqLength = CommonValidator.validateLength(value, 3, 3);
/*      */         
/*  155 */         if (reqLength.equals("greaterThanMax"))
/*      */         {
/*      */ 
/*  158 */           errorObj = new ErrorObject(
/*  159 */             SubmissionErrorCodes.ERROR_DATAFIELD311_IATA_CARRIER_CODE);
/*  160 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/*  163 */         errorObj = new ErrorObject(
/*  164 */           SubmissionErrorCodes.ERROR_DATAFIELD312_IATA_CARRIER_CODE);
/*  165 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateIataAgencyNumber(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  182 */     ErrorObject errorObj = null;
/*      */     
/*  184 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  186 */       if (CommonValidator.validateData(value, 
/*  187 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ĺ']))
/*      */       {
/*  189 */         String reqLength = CommonValidator.validateLength(value, 8, 8);
/*      */         
/*  191 */         if (reqLength.equals("greaterThanMax"))
/*      */         {
/*  193 */           errorObj = new ErrorObject(
/*  194 */             SubmissionErrorCodes.ERROR_DATAFIELD313_IATA_CARRIER_CODE);
/*  195 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/*  198 */         errorObj = new ErrorObject(
/*  199 */           SubmissionErrorCodes.ERROR_DATAFIELD314_IATA_CARRIER_CODE);
/*  200 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTravelPackageIndicator(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  217 */     ErrorObject errorObj = null;
/*      */     
/*  219 */     if (CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  221 */       errorObj = new ErrorObject(
/*  222 */         SubmissionErrorCodes.ERROR_DATAFIELD316_TRAVEL_PACKAGENDICATOR);
/*  223 */       errorCodes.add(errorObj);
/*      */     }
/*  225 */     else if (CommonValidator.validateData(value, 
/*  226 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ĺ']))
/*      */     {
/*  228 */       String reqLength = CommonValidator.validateLength(value, 1, 1);
/*      */       
/*  230 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  232 */         errorObj = new ErrorObject(
/*  233 */           SubmissionErrorCodes.ERROR_DATAFIELD317_TRAVEL_PACKAGENDICATOR);
/*  234 */         errorCodes.add(errorObj);
/*      */ 
/*      */ 
/*      */       }
/*  238 */       else if (!value.equalsIgnoreCase("C"))
/*      */       {
/*  240 */         if (!value.equalsIgnoreCase("A"))
/*      */         {
/*  242 */           if (!value.equalsIgnoreCase("B"))
/*      */           {
/*  244 */             if (!value.equalsIgnoreCase("N"))
/*      */             {
/*  246 */               errorObj = new ErrorObject(
/*  247 */                 SubmissionErrorCodes.ERROR_DATAFIELD315_TRAVEL_PACKAGENDICATOR);
/*  248 */               errorCodes.add(errorObj);
/*      */             } } }
/*      */       }
/*      */     } else {
/*  252 */       errorObj = new ErrorObject(
/*  253 */         SubmissionErrorCodes.ERROR_DATAFIELD318_TRAVEL_PACKAGENDICATOR);
/*  254 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validatePassengerName(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  271 */     ErrorObject errorObj = null;
/*      */     
/*  273 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/*      */ 
/*      */ 
/*      */ 
/*  278 */       String reqLength = 
/*  279 */         CommonValidator.validateLength(value, 25, 25);
/*      */       
/*  281 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  283 */         errorObj = new ErrorObject(
/*  284 */           SubmissionErrorCodes.ERROR_DATAFIELD318_PASSENGER_NAME);
/*  285 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTravelTicketNumber(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  307 */     ErrorObject errorObj = null;
/*      */     
/*  309 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/*      */ 
/*      */ 
/*      */ 
/*  314 */       String reqLength = 
/*  315 */         CommonValidator.validateLength(value, 14, 14);
/*      */       
/*  317 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  319 */         errorObj = new ErrorObject(
/*  320 */           SubmissionErrorCodes.ERROR_DATAFIELD320_TRAVEL_TICKET_NUMBER);
/*  321 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTravelDepartureDateSegment1(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  343 */     ErrorObject errorObj = null;
/*      */     
/*  345 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  347 */       if (CommonValidator.validateData(value, 
/*  348 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ľ']))
/*      */       {
/*  350 */         String reqLength = CommonValidator.validateLength(value, 8, 8);
/*      */         
/*  352 */         if (!reqLength.equals("equal"))
/*      */         {
/*  354 */           errorObj = new ErrorObject(
/*  355 */             SubmissionErrorCodes.ERROR_DATAFIELD323_TRAVELDEPARTURE_DATESEGMENT1);
/*  356 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/*  359 */         errorObj = new ErrorObject(
/*  360 */           SubmissionErrorCodes.ERROR_DATAFIELD322_TRAVELDEPARTURE_DATESEGMENT1);
/*  361 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTravelDepartureDateSegment2(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  378 */     ErrorObject errorObj = null;
/*      */     
/*  380 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  382 */       if (CommonValidator.validateData(value, 
/*  383 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ľ']))
/*      */       {
/*  385 */         String reqLength = CommonValidator.validateLength(value, 8, 8);
/*      */         
/*  387 */         if (!reqLength.equals("equal"))
/*      */         {
/*  389 */           errorObj = new ErrorObject(
/*  390 */             SubmissionErrorCodes.ERROR_DATAFIELD325_TRAVELDEPARTURE_DATESEGMENT2);
/*  391 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/*  394 */         errorObj = new ErrorObject(
/*  395 */           SubmissionErrorCodes.ERROR_DATAFIELD324_TRAVELDEPARTURE_DATESEGMENT2);
/*  396 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTravelDepartureDateSegment3(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  413 */     ErrorObject errorObj = null;
/*      */     
/*  415 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  417 */       if (CommonValidator.validateData(value, 
/*  418 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ŀ']))
/*      */       {
/*  420 */         String reqLength = CommonValidator.validateLength(value, 8, 8);
/*      */         
/*  422 */         if (!reqLength.equals("equal"))
/*      */         {
/*  424 */           errorObj = new ErrorObject(
/*  425 */             SubmissionErrorCodes.ERROR_DATAFIELD327_TRAVELDEPARTURE_DATESEGMENT3);
/*  426 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/*  429 */         errorObj = new ErrorObject(
/*  430 */           SubmissionErrorCodes.ERROR_DATAFIELD326_TRAVELDEPARTURE_DATESEGMENT3);
/*  431 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTravelDepartureDateSegment4(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  448 */     ErrorObject errorObj = null;
/*      */     
/*  450 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  452 */       if (CommonValidator.validateData(value, 
/*  453 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ŀ']))
/*      */       {
/*  455 */         String reqLength = CommonValidator.validateLength(value, 8, 8);
/*      */         
/*  457 */         if (!reqLength.equals("equal"))
/*      */         {
/*  459 */           errorObj = new ErrorObject(
/*  460 */             SubmissionErrorCodes.ERROR_DATAFIELD329_TRAVELDEPARTURE_DATESEGMENT4);
/*  461 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/*  464 */         errorObj = new ErrorObject(
/*  465 */           SubmissionErrorCodes.ERROR_DATAFIELD328_TRAVELDEPARTURE_DATESEGMENT4);
/*  466 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTravelDestinationCodeSegment1(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  483 */     ErrorObject errorObj = null;
/*      */     
/*  485 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  487 */       if (CommonValidator.validateData(value, 
/*  488 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ł']))
/*      */       {
/*  490 */         String reqLength = CommonValidator.validateLength(value, 3, 3);
/*      */         
/*  492 */         if (reqLength.equals("greaterThanMax"))
/*      */         {
/*  494 */           errorObj = new ErrorObject(
/*  495 */             SubmissionErrorCodes.ERROR_DATAFIELD331_TRAVEL_DESTINATIONCODESEGMENT1);
/*  496 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/*  499 */         errorObj = new ErrorObject(
/*  500 */           SubmissionErrorCodes.ERROR_DATAFIELD330_TRAVEL_DESTINATIONCODESEGMENT1);
/*  501 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTravelDestinationCodeSegment2(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  518 */     ErrorObject errorObj = null;
/*      */     
/*  520 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  522 */       if (CommonValidator.validateData(value, 
/*  523 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ł']))
/*      */       {
/*  525 */         String reqLength = CommonValidator.validateLength(value, 3, 3);
/*      */         
/*  527 */         if (reqLength.equals("greaterThanMax"))
/*      */         {
/*  529 */           errorObj = new ErrorObject(
/*  530 */             SubmissionErrorCodes.ERROR_DATAFIELD333_TRAVEL_DESTINATIONCODESEGMENT2);
/*  531 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/*  534 */         errorObj = new ErrorObject(
/*  535 */           SubmissionErrorCodes.ERROR_DATAFIELD332_TRAVEL_DESTINATIONCODESEGMENT2);
/*  536 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTravelDestinationCodeSegment3(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  553 */     ErrorObject errorObj = null;
/*      */     
/*  555 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  557 */       if (CommonValidator.validateData(value, 
/*  558 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ń']))
/*      */       {
/*  560 */         String reqLength = CommonValidator.validateLength(value, 3, 3);
/*      */         
/*  562 */         if (reqLength.equals("greaterThanMax"))
/*      */         {
/*  564 */           errorObj = new ErrorObject(
/*  565 */             SubmissionErrorCodes.ERROR_DATAFIELD335_TRAVEL_DESTINATIONCODESEGMENT3);
/*  566 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/*  569 */         errorObj = new ErrorObject(
/*  570 */           SubmissionErrorCodes.ERROR_DATAFIELD334_TRAVEL_DESTINATIONCODESEGMENT3);
/*  571 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTravelDestinationCodeSegment4(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  588 */     ErrorObject errorObj = null;
/*      */     
/*  590 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  592 */       if (CommonValidator.validateData(value, 
/*  593 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ń']))
/*      */       {
/*  595 */         String reqLength = CommonValidator.validateLength(value, 3, 3);
/*      */         
/*  597 */         if (reqLength.equals("greaterThanMax"))
/*      */         {
/*  599 */           errorObj = new ErrorObject(
/*  600 */             SubmissionErrorCodes.ERROR_DATAFIELD337_TRAVEL_DESTINATIONCODESEGMENT4);
/*  601 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/*  604 */         errorObj = new ErrorObject(
/*  605 */           SubmissionErrorCodes.ERROR_DATAFIELD336_TRAVEL_DESTINATIONCODESEGMENT4);
/*  606 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTravelDepartureAirportSegment1(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  623 */     ErrorObject errorObj = null;
/*      */     
/*  625 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  627 */       if (CommonValidator.validateData(value, 
/*  628 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ņ']))
/*      */       {
/*  630 */         String reqLength = CommonValidator.validateLength(value, 3, 3);
/*      */         
/*  632 */         if (reqLength.equals("greaterThanMax"))
/*      */         {
/*  634 */           errorObj = new ErrorObject(
/*  635 */             SubmissionErrorCodes.ERROR_DATAFIELD339_TRAVELDEPARTURE_AIRPORTSEGMENT1);
/*  636 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/*  639 */         errorObj = new ErrorObject(
/*  640 */           SubmissionErrorCodes.ERROR_DATAFIELD338_TRAVELDEPARTURE_AIRPORTSEGMENT1);
/*  641 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTravelDepartureAirportSegment2(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  658 */     ErrorObject errorObj = null;
/*      */     
/*  660 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  662 */       if (CommonValidator.validateData(value, 
/*  663 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ņ']))
/*      */       {
/*  665 */         String reqLength = CommonValidator.validateLength(value, 3, 3);
/*      */         
/*  667 */         if (reqLength.equals("greaterThanMax"))
/*      */         {
/*  669 */           errorObj = new ErrorObject(
/*  670 */             SubmissionErrorCodes.ERROR_DATAFIELD341_TRAVELDEPARTURE_AIRPORTSEGMENT2);
/*  671 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/*  674 */         errorObj = new ErrorObject(
/*  675 */           SubmissionErrorCodes.ERROR_DATAFIELD340_TRAVELDEPARTURE_AIRPORTSEGMENT2);
/*  676 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTravelDepartureAirportSegment3(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  693 */     ErrorObject errorObj = null;
/*      */     
/*  695 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  697 */       if (CommonValidator.validateData(value, 
/*  698 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ň']))
/*      */       {
/*  700 */         String reqLength = CommonValidator.validateLength(value, 3, 3);
/*      */         
/*  702 */         if (reqLength.equals("greaterThanMax"))
/*      */         {
/*  704 */           errorObj = new ErrorObject(
/*  705 */             SubmissionErrorCodes.ERROR_DATAFIELD343_TRAVELDEPARTURE_AIRPORTSEGMENT3);
/*  706 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/*  709 */         errorObj = new ErrorObject(
/*  710 */           SubmissionErrorCodes.ERROR_DATAFIELD342_TRAVELDEPARTURE_AIRPORTSEGMENT3);
/*  711 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTravelDepartureAirportSegment4(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  728 */     ErrorObject errorObj = null;
/*      */     
/*  730 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  732 */       if (CommonValidator.validateData(value, 
/*  733 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ň']))
/*      */       {
/*  735 */         String reqLength = CommonValidator.validateLength(value, 3, 3);
/*      */         
/*  737 */         if (reqLength.equals("greaterThanMax"))
/*      */         {
/*  739 */           errorObj = new ErrorObject(
/*  740 */             SubmissionErrorCodes.ERROR_DATAFIELD345_TRAVELDEPARTURE_AIRPORTSEGMENT4);
/*  741 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/*  744 */         errorObj = new ErrorObject(
/*  745 */           SubmissionErrorCodes.ERROR_DATAFIELD344_TRAVELDEPARTURE_AIRPORTSEGMENT4);
/*  746 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateAirCarrierCodeSegment1(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  763 */     ErrorObject errorObj = null;
/*      */     
/*  765 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  767 */       if (CommonValidator.validateData(value, 
/*  768 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ŉ']))
/*      */       {
/*  770 */         String reqLength = CommonValidator.validateLength(value, 2, 2);
/*      */         
/*  772 */         if (reqLength.equals("greaterThanMax"))
/*      */         {
/*  774 */           errorObj = new ErrorObject(
/*  775 */             SubmissionErrorCodes.ERROR_DATAFIELD347_AIRCARRIER_CODESEGMENT1);
/*  776 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/*  779 */         errorObj = new ErrorObject(
/*  780 */           SubmissionErrorCodes.ERROR_DATAFIELD346_AIRCARRIER_CODESEGMENT1);
/*  781 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateAirCarrierCodeSegment2(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  798 */     ErrorObject errorObj = null;
/*      */     
/*  800 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  802 */       if (CommonValidator.validateData(value, 
/*  803 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ŋ']))
/*      */       {
/*  805 */         String reqLength = CommonValidator.validateLength(value, 2, 2);
/*      */         
/*  807 */         if (reqLength.equals("greaterThanMax"))
/*      */         {
/*  809 */           errorObj = new ErrorObject(
/*  810 */             SubmissionErrorCodes.ERROR_DATAFIELD349_AIRCARRIER_CODESEGMENT2);
/*  811 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/*  814 */         errorObj = new ErrorObject(
/*  815 */           SubmissionErrorCodes.ERROR_DATAFIELD348_AIRCARRIER_CODESEGMENT2);
/*  816 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateAirCarrierCodeSegment3(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  833 */     ErrorObject errorObj = null;
/*      */     
/*  835 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  837 */       if (CommonValidator.validateData(value, 
/*  838 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ŋ']))
/*      */       {
/*  840 */         String reqLength = CommonValidator.validateLength(value, 2, 2);
/*      */         
/*  842 */         if (reqLength.equals("greaterThanMax"))
/*      */         {
/*  844 */           errorObj = new ErrorObject(
/*  845 */             SubmissionErrorCodes.ERROR_DATAFIELD351_AIRCARRIER_CODESEGMENT3);
/*  846 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/*  849 */         errorObj = new ErrorObject(
/*  850 */           SubmissionErrorCodes.ERROR_DATAFIELD350_AIRCARRIER_CODESEGMENT3);
/*  851 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateAirCarrierCodeSegment4(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  868 */     ErrorObject errorObj = null;
/*      */     
/*  870 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  872 */       if (CommonValidator.validateData(value, 
/*  873 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ō']))
/*      */       {
/*  875 */         String reqLength = CommonValidator.validateLength(value, 2, 2);
/*      */         
/*  877 */         if (reqLength.equals("greaterThanMax"))
/*      */         {
/*  879 */           errorObj = new ErrorObject(
/*  880 */             SubmissionErrorCodes.ERROR_DATAFIELD353_AIRCARRIER_CODESEGMENT4);
/*  881 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/*  884 */         errorObj = new ErrorObject(
/*  885 */           SubmissionErrorCodes.ERROR_DATAFIELD352_AIRCARRIER_CODESEGMENT4);
/*  886 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateFlightNumberSegment1(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  903 */     ErrorObject errorObj = null;
/*      */     
/*  905 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  907 */       if (CommonValidator.validateData(value, 
/*  908 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ō']))
/*      */       {
/*  910 */         String reqLength = CommonValidator.validateLength(value, 4, 4);
/*      */         
/*  912 */         if (reqLength.equals("greaterThanMax"))
/*      */         {
/*  914 */           errorObj = new ErrorObject(
/*  915 */             SubmissionErrorCodes.ERROR_DATAFIELD355_FLIGHTNUMBER_SEGMENT1);
/*  916 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/*  919 */         errorObj = new ErrorObject(
/*  920 */           SubmissionErrorCodes.ERROR_DATAFIELD354_FLIGHTNUMBER_SEGMENT1);
/*  921 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateFlightNumberSegment2(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  938 */     ErrorObject errorObj = null;
/*      */     
/*  940 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  942 */       if (CommonValidator.validateData(value, 
/*  943 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ŏ']))
/*      */       {
/*  945 */         String reqLength = CommonValidator.validateLength(value, 4, 4);
/*      */         
/*  947 */         if (reqLength.equals("greaterThanMax"))
/*      */         {
/*  949 */           errorObj = new ErrorObject(
/*  950 */             SubmissionErrorCodes.ERROR_DATAFIELD357_FLIGHTNUMBER_SEGMENT2);
/*  951 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/*  954 */         errorObj = new ErrorObject(
/*  955 */           SubmissionErrorCodes.ERROR_DATAFIELD356_FLIGHTNUMBER_SEGMENT2);
/*  956 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateFlightNumberSegment3(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  973 */     ErrorObject errorObj = null;
/*      */     
/*  975 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  977 */       if (CommonValidator.validateData(value, 
/*  978 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ŏ']))
/*      */       {
/*  980 */         String reqLength = CommonValidator.validateLength(value, 4, 4);
/*      */         
/*  982 */         if (reqLength.equals("greaterThanMax"))
/*      */         {
/*  984 */           errorObj = new ErrorObject(
/*  985 */             SubmissionErrorCodes.ERROR_DATAFIELD359_FLIGHTNUMBER_SEGMENT3);
/*  986 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/*  989 */         errorObj = new ErrorObject(
/*  990 */           SubmissionErrorCodes.ERROR_DATAFIELD358_FLIGHTNUMBER_SEGMENT3);
/*  991 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateFlightNumberSegment4(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1008 */     ErrorObject errorObj = null;
/*      */     
/* 1010 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/* 1012 */       if (CommonValidator.validateData(value, 
/* 1013 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ő']))
/*      */       {
/* 1015 */         String reqLength = CommonValidator.validateLength(value, 4, 4);
/*      */         
/* 1017 */         if (reqLength.equals("greaterThanMax"))
/*      */         {
/* 1019 */           errorObj = new ErrorObject(
/* 1020 */             SubmissionErrorCodes.ERROR_DATAFIELD361_FLIGHTNUMBER_SEGMENT4);
/* 1021 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/* 1024 */         errorObj = new ErrorObject(
/* 1025 */           SubmissionErrorCodes.ERROR_DATAFIELD360_FLIGHTNUMBER_SEGMENT4);
/* 1026 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateClassCodeSegment1(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1043 */     ErrorObject errorObj = null;
/*      */     
/* 1045 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/* 1047 */       if (CommonValidator.validateData(value, 
/* 1048 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ő']))
/*      */       {
/* 1050 */         String reqLength = CommonValidator.validateLength(value, 1, 1);
/*      */         
/* 1052 */         if (reqLength.equals("greaterThanMax"))
/*      */         {
/* 1054 */           errorObj = new ErrorObject(
/* 1055 */             SubmissionErrorCodes.ERROR_DATAFIELD363_CLASSCODE_SEGMENT1);
/* 1056 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/* 1059 */         errorObj = new ErrorObject(
/* 1060 */           SubmissionErrorCodes.ERROR_DATAFIELD362_CLASSCODE_SEGMENT1);
/* 1061 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateClassCodeSegment2(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1078 */     ErrorObject errorObj = null;
/*      */     
/* 1080 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/* 1082 */       if (CommonValidator.validateData(value, 
/* 1083 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Œ']))
/*      */       {
/* 1085 */         String reqLength = CommonValidator.validateLength(value, 1, 1);
/*      */         
/* 1087 */         if (reqLength.equals("greaterThanMax"))
/*      */         {
/* 1089 */           errorObj = new ErrorObject(
/* 1090 */             SubmissionErrorCodes.ERROR_DATAFIELD365_CLASSCODE_SEGMENT2);
/* 1091 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/* 1094 */         errorObj = new ErrorObject(
/* 1095 */           SubmissionErrorCodes.ERROR_DATAFIELD364_CLASSCODE_SEGMENT2);
/* 1096 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateClassCodeSegment3(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1113 */     ErrorObject errorObj = null;
/*      */     
/* 1115 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/* 1117 */       if (CommonValidator.validateData(value, 
/* 1118 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['œ']))
/*      */       {
/* 1120 */         String reqLength = CommonValidator.validateLength(value, 1, 1);
/*      */         
/* 1122 */         if (reqLength.equals("greaterThanMax"))
/*      */         {
/* 1124 */           errorObj = new ErrorObject(
/* 1125 */             SubmissionErrorCodes.ERROR_DATAFIELD367_CLASSCODE_SEGMENT3);
/* 1126 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/* 1129 */         errorObj = new ErrorObject(
/* 1130 */           SubmissionErrorCodes.ERROR_DATAFIELD366_CLASSCODE_SEGMENT3);
/* 1131 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateClassCodeSegment4(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1148 */     ErrorObject errorObj = null;
/*      */     
/* 1150 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/* 1152 */       if (CommonValidator.validateData(value, 
/* 1153 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ŕ']))
/*      */       {
/* 1155 */         String reqLength = CommonValidator.validateLength(value, 1, 1);
/*      */         
/* 1157 */         if (reqLength.equals("greaterThanMax"))
/*      */         {
/* 1159 */           errorObj = new ErrorObject(
/* 1160 */             SubmissionErrorCodes.ERROR_DATAFIELD369_CLASSCODE_SEGMENT4);
/* 1161 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/* 1164 */         errorObj = new ErrorObject(
/* 1165 */           SubmissionErrorCodes.ERROR_DATAFIELD368_CLASSCODE_SEGMENT4);
/* 1166 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateLodgingCheckInDate(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1183 */     ErrorObject errorObj = null;
/*      */     
/* 1185 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/* 1187 */       if (CommonValidator.validateData(value, 
/* 1188 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ŕ']))
/*      */       {
/* 1190 */         String reqLength = CommonValidator.validateLength(value, 8, 8);
/*      */         
/* 1192 */         if (!reqLength.equals("equal"))
/*      */         {
/* 1194 */           errorObj = new ErrorObject(
/* 1195 */             SubmissionErrorCodes.ERROR_DATAFIELD371_LODGINGCHECK_INDATE);
/* 1196 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/* 1199 */         errorObj = new ErrorObject(
/* 1200 */           SubmissionErrorCodes.ERROR_DATAFIELD370_LODGINGCHECK_INDATE);
/* 1201 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateLodgingCheckOutDate(TravelCruiseIndustryTAABean tIndustryTAABean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1219 */     ErrorObject errorObj = null;
/*      */     
/* 1221 */     if (!CommonValidator.isNullOrEmpty(tIndustryTAABean
/* 1222 */       .getLodgingCheckOutDate()))
/*      */     {
/* 1224 */       if (CommonValidator.validateData(tIndustryTAABean
/* 1225 */         .getLodgingCheckOutDate(), 
/* 1226 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ŗ']))
/*      */       {
/* 1228 */         String reqLength = CommonValidator.validateLength(
/* 1229 */           tIndustryTAABean.getLodgingCheckOutDate(), 8, 8);
/*      */         
/* 1231 */         if (reqLength.equals("equal"))
/*      */         {
/*      */ 
/*      */ 
/* 1235 */           if (Integer.parseInt(tIndustryTAABean.getLodgingCheckOutDate()) < Integer.parseInt(tIndustryTAABean.getLodgingCheckInDate()))
/*      */           {
/* 1237 */             errorObj = new ErrorObject(
/* 1238 */               SubmissionErrorCodes.ERROR_DATAFIELD374_LODGINGCHECK_OUTDATE);
/* 1239 */             errorCodes.add(errorObj);
/*      */           }
/*      */         } else {
/* 1242 */           errorObj = new ErrorObject(
/* 1243 */             SubmissionErrorCodes.ERROR_DATAFIELD373_LODGINGCHECK_OUTDATE);
/* 1244 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/* 1247 */         errorObj = new ErrorObject(
/* 1248 */           SubmissionErrorCodes.ERROR_DATAFIELD372_LODGINGCHECK_OUTDATE);
/* 1249 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateLodgingRoomRate1(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1266 */     ErrorObject errorObj = null;
/*      */     
/* 1268 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/* 1270 */       if (CommonValidator.validateData(value, 
/* 1271 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ř']))
/*      */       {
/* 1273 */         String reqLength = 
/* 1274 */           CommonValidator.validateLength(value, 12, 12);
/*      */         
/* 1276 */         if (reqLength.equals("greaterThanMax"))
/*      */         {
/* 1278 */           errorObj = new ErrorObject(
/* 1279 */             SubmissionErrorCodes.ERROR_DATAFIELD376_LODGING_ROOMRATE1);
/* 1280 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/* 1283 */         errorObj = new ErrorObject(
/* 1284 */           SubmissionErrorCodes.ERROR_DATAFIELD375_LODGING_ROOMRATE1);
/* 1285 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateNumberOfNightsAtRoomRate1(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1302 */     ErrorObject errorObj = null;
/*      */     
/* 1304 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/* 1306 */       if (CommonValidator.validateData(value, 
/* 1307 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ř']))
/*      */       {
/* 1309 */         String reqLength = CommonValidator.validateLength(value, 2, 2);
/*      */         
/* 1311 */         if (reqLength.equals("greaterThanMax"))
/*      */         {
/* 1313 */           errorObj = new ErrorObject(
/* 1314 */             SubmissionErrorCodes.ERROR_DATAFIELD378_NUMBEROFNIGHTS_ATROOMRATE1);
/* 1315 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/* 1318 */         errorObj = new ErrorObject(
/* 1319 */           SubmissionErrorCodes.ERROR_DATAFIELD377_NUMBEROFNIGHTS_ATROOMRATE1);
/* 1320 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateLodgingName(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1337 */     ErrorObject errorObj = null;
/*      */     
/* 1339 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/*      */ 
/*      */ 
/*      */ 
/* 1344 */       String reqLength = 
/* 1345 */         CommonValidator.validateLength(value, 20, 20);
/*      */       
/* 1347 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/* 1349 */         errorObj = new ErrorObject(
/* 1350 */           SubmissionErrorCodes.ERROR_DATAFIELD379_LODGING_NAME);
/* 1351 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateLodgingRegionCode(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1373 */     ErrorObject errorObj = null;
/*      */     
/* 1375 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/* 1377 */       if (CommonValidator.validateData(value, 
/* 1378 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ś']))
/*      */       {
/* 1380 */         String reqLength = CommonValidator.validateLength(value, 3, 3);
/*      */         
/* 1382 */         if (reqLength.equals("greaterThanMax"))
/*      */         {
/* 1384 */           errorObj = new ErrorObject(
/* 1385 */             SubmissionErrorCodes.ERROR_DATAFIELD381_LODGING_REGIONCODE);
/* 1386 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/* 1389 */         errorObj = new ErrorObject(
/* 1390 */           SubmissionErrorCodes.ERROR_DATAFIELD382_LODGING_REGIONCODE);
/* 1391 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateLodgingCountryCode(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1408 */     ErrorObject errorObj = null;
/*      */     
/* 1410 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/* 1412 */       if (CommonValidator.validateData(value, 
/* 1413 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ŝ']))
/*      */       {
/* 1415 */         String reqLength = CommonValidator.validateLength(value, 3, 3);
/*      */         
/* 1417 */         if (reqLength.equals("greaterThanMax"))
/*      */         {
/* 1419 */           errorObj = new ErrorObject(
/* 1420 */             SubmissionErrorCodes.ERROR_DATAFIELD383_LODGING_COUNTRYCODE);
/* 1421 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/* 1424 */         errorObj = new ErrorObject(
/* 1425 */           SubmissionErrorCodes.ERROR_DATAFIELD384_LODGING_COUNTRYCODE);
/* 1426 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateLodgingCityName(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1443 */     ErrorObject errorObj = null;
/*      */     
/* 1445 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1451 */       String reqLength = 
/* 1452 */         CommonValidator.validateLength(value, 18, 18);
/*      */       
/* 1454 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/* 1456 */         errorObj = new ErrorObject(
/* 1457 */           SubmissionErrorCodes.ERROR_DATAFIELD385_LODGING_CITYNAME);
/* 1458 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\validator\industry\TCIndRecordValidator.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */