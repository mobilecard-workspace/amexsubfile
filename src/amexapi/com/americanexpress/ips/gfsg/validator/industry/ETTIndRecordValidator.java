/*     */ package com.americanexpress.ips.gfsg.validator.industry;
/*     */ 
/*     */ import com.americanexpress.ips.gfsg.beans.ErrorObject;
/*     */ import com.americanexpress.ips.gfsg.beans.common.TransactionAdviceAddendumBean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.TransactionAdviceBasicBean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.EntertainmentTicketingIndustryTAABean;
/*     */ import com.americanexpress.ips.gfsg.enumerations.SubmissionErrorCodes;
/*     */ import com.americanexpress.ips.gfsg.exceptions.SettlementException;
/*     */ import com.americanexpress.ips.gfsg.validator.CommonValidator;
/*     */ import java.util.List;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class ETTIndRecordValidator
/*     */ {
/*     */   public static void validateETTIndRecord(TransactionAdviceBasicBean transactionAdviceBasicType, TransactionAdviceAddendumBean transactionAddendumType, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/*  37 */     EntertainmentTicketingIndustryTAABean eIndustryTAABean = (EntertainmentTicketingIndustryTAABean)transactionAddendumType;
/*     */     
/*     */ 
/*  40 */     TAADBRecordValidator.validateRecordType(eIndustryTAABean
/*  41 */       .getRecordType(), errorCodes);
/*  42 */     TAADBRecordValidator.validateRecordNumber(eIndustryTAABean
/*  43 */       .getRecordNumber(), errorCodes);
/*  44 */     TAADBRecordValidator.validateTransactionIdentifier(eIndustryTAABean
/*  45 */       .getTransactionIdentifier(), transactionAdviceBasicType, 
/*  46 */       errorCodes);
/*  47 */     TAADBRecordValidator.validateAddendaTypeCode(eIndustryTAABean
/*  48 */       .getAddendaTypeCode(), errorCodes);
/*  49 */     TAADBRecordValidator.validateFormatCode(eIndustryTAABean
/*  50 */       .getFormatCode(), transactionAdviceBasicType, errorCodes);
/*     */     
/*  52 */     validateEventName(eIndustryTAABean.getEventName(), errorCodes);
/*  53 */     validateEventDate(eIndustryTAABean.getEventDate(), errorCodes);
/*  54 */     validateEventIndividualTicketPriceAmount(eIndustryTAABean
/*  55 */       .getEventIndividualTicketPriceAmount(), errorCodes);
/*  56 */     validateEventTicketQuantity(eIndustryTAABean.getEventTicketQuantity(), 
/*  57 */       errorCodes);
/*     */     
/*  59 */     validateEventlocation(eIndustryTAABean.getEventLocation(), errorCodes);
/*  60 */     validateEventRegionCode(eIndustryTAABean, errorCodes);
/*  61 */     validateEventCountryCode(eIndustryTAABean, errorCodes);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateEventName(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/*  77 */     ErrorObject errorObj = null;
/*     */     
/*  79 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/*  84 */       String reqLength = 
/*  85 */         CommonValidator.validateLength(value, 30, 30);
/*     */       
/*  87 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/*     */ 
/*  90 */         errorObj = new ErrorObject(
/*  91 */           SubmissionErrorCodes.ERROR_DATAFIELD294_EVENT_NAME);
/*  92 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateEventDate(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 114 */     ErrorObject errorObj = null;
/*     */     
/* 116 */     if (CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 118 */       errorObj = new ErrorObject(
/* 119 */         SubmissionErrorCodes.ERROR_DATAFIELD296_EVENT_DATE);
/* 120 */       errorCodes.add(errorObj);
/*     */ 
/*     */     }
/* 123 */     else if (CommonValidator.validateData(value, 
/* 124 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ī']))
/*     */     {
/* 126 */       String reqLength = CommonValidator.validateLength(value, 8, 8);
/*     */       
/* 128 */       if ((!reqLength.equals("equal")) || 
/* 129 */         (!CommonValidator.isValidDate(value, 
/* 130 */         "CCYYMMDD")))
/*     */       {
/* 132 */         errorObj = new ErrorObject(
/* 133 */           SubmissionErrorCodes.ERROR_DATAFIELD296_EVENT_DATE);
/* 134 */         errorCodes.add(errorObj);
/*     */       }
/*     */     } else {
/* 137 */       errorObj = new ErrorObject(
/* 138 */         SubmissionErrorCodes.ERROR_DATAFIELD298_EVENT_DATE);
/* 139 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateEventIndividualTicketPriceAmount(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 156 */     ErrorObject errorObj = null;
/*     */     
/* 158 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 160 */       if (CommonValidator.validateData(value, 
/* 161 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ĭ']))
/*     */       {
/* 163 */         String reqLength = CommonValidator.validateLength(value, 8, 8);
/*     */         
/* 165 */         if (reqLength.equals("greaterThanMax"))
/*     */         {
/* 167 */           errorObj = new ErrorObject(
/* 168 */             SubmissionErrorCodes.ERROR_DATAFIELD300_EVENTINDIVIDUAL_TICKETPRICEAMOUNT);
/* 169 */           errorCodes.add(errorObj);
/*     */         }
/*     */       } else {
/* 172 */         errorObj = new ErrorObject(
/* 173 */           SubmissionErrorCodes.ERROR_DATAFIELD299_EVENTINDIVIDUAL_TICKETPRICEAMOUNT);
/* 174 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateEventTicketQuantity(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 191 */     ErrorObject errorObj = null;
/*     */     
/* 193 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 195 */       if (CommonValidator.validateData(value, 
/* 196 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Į']))
/*     */       {
/*     */ 
/* 199 */         String reqLength = CommonValidator.validateLength(value, 4, 1);
/* 200 */         if (reqLength.equals("greaterThanMax"))
/*     */         {
/* 202 */           errorObj = new ErrorObject(
/* 203 */             SubmissionErrorCodes.ERROR_DATAFIELD302_EVENT_TICKETQUANTITY);
/* 204 */           errorCodes.add(errorObj);
/*     */         }
/*     */       } else {
/* 207 */         errorObj = new ErrorObject(
/* 208 */           SubmissionErrorCodes.ERROR_DATAFIELD301_EVENT_TICKETQUANTITY);
/* 209 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateEventlocation(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 226 */     ErrorObject errorObj = null;
/*     */     
/* 228 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 234 */       String reqLength = 
/* 235 */         CommonValidator.validateLength(value, 40, 40);
/*     */       
/* 237 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/* 239 */         errorObj = new ErrorObject(
/* 240 */           SubmissionErrorCodes.ERROR_DATAFIELD303_EVENT_LOCATION);
/* 241 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateEventCountryCode(EntertainmentTicketingIndustryTAABean eIndustryTAABean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 264 */     ErrorObject errorObj = null;
/*     */     
/* 266 */     if (!CommonValidator.isNullOrEmpty(eIndustryTAABean
/* 267 */       .getEventCountryCode()))
/*     */     {
/* 269 */       if (CommonValidator.validateData(eIndustryTAABean
/* 270 */         .getEventCountryCode(), 
/* 271 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['İ']))
/*     */       {
/* 273 */         String reqLength = CommonValidator.validateLength(
/* 274 */           eIndustryTAABean.getEventCountryCode(), 3, 3);
/*     */         
/* 276 */         if (reqLength.equals("greaterThanMax")) {
/* 277 */           errorObj = new ErrorObject(
/* 278 */             SubmissionErrorCodes.ERROR_DATAFIELD308_EVENT_COUNTRYCODE);
/* 279 */           errorCodes.add(errorObj);
/*     */ 
/*     */         }
/* 282 */         else if (CommonValidator.isNullOrEmpty(eIndustryTAABean
/* 283 */           .getEventRegionCode())) {
/* 284 */           errorObj = new ErrorObject(
/* 285 */             SubmissionErrorCodes.ERROR_DATAFIELD307_EVENT_REGIONCODE);
/* 286 */           errorCodes.add(errorObj);
/*     */         }
/*     */         
/*     */       }
/*     */       else
/*     */       {
/* 292 */         errorObj = new ErrorObject(
/* 293 */           SubmissionErrorCodes.ERROR_DATAFIELD309_EVENT_COUNTRYCODE);
/* 294 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateEventRegionCode(EntertainmentTicketingIndustryTAABean eIndustryTAABean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 312 */     ErrorObject errorObj = null;
/*     */     
/* 314 */     if (!CommonValidator.isNullOrEmpty(eIndustryTAABean
/* 315 */       .getEventRegionCode()))
/*     */     {
/* 317 */       if (CommonValidator.validateData(eIndustryTAABean
/* 318 */         .getEventRegionCode(), 
/* 319 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ı']))
/*     */       {
/*     */ 
/*     */ 
/*     */ 
/* 324 */         String reqLength = CommonValidator.validateLength(
/* 325 */           eIndustryTAABean.getEventRegionCode(), 3, 1);
/* 326 */         if (reqLength.equals("greaterThanMax")) {
/* 327 */           errorObj = new ErrorObject(
/* 328 */             SubmissionErrorCodes.ERROR_DATAFIELD305_EVENT_REGIONCODE);
/* 329 */           errorCodes.add(errorObj);
/*     */ 
/*     */         }
/* 332 */         else if (CommonValidator.isNullOrEmpty(eIndustryTAABean
/* 333 */           .getEventCountryCode())) {
/* 334 */           errorObj = new ErrorObject(
/* 335 */             SubmissionErrorCodes.ERROR_DATAFIELD310_EVENT_COUNTRYCODE);
/* 336 */           errorCodes.add(errorObj);
/*     */         }
/*     */       }
/*     */       else {
/* 340 */         errorObj = new ErrorObject(
/* 341 */           SubmissionErrorCodes.ERROR_DATAFIELD306_EVENT_REGIONCODE);
/* 342 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\validator\industry\ETTIndRecordValidator.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */