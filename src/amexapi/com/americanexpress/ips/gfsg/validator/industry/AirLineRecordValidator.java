/*      */ package com.americanexpress.ips.gfsg.validator.industry;
/*      */ 
/*      */ import com.americanexpress.ips.gfsg.beans.ErrorObject;
/*      */ import com.americanexpress.ips.gfsg.beans.common.TransactionAdviceAddendumBean;
/*      */ import com.americanexpress.ips.gfsg.beans.common.TransactionAdviceBasicBean;
/*      */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.AirlineIndustryTAABean;
/*      */ import com.americanexpress.ips.gfsg.enumerations.SubmissionErrorCodes;
/*      */ import com.americanexpress.ips.gfsg.enumerations.SubmissionMethod;
/*      */ import com.americanexpress.ips.gfsg.exceptions.SettlementException;
/*      */ import com.americanexpress.ips.gfsg.validator.CommonValidator;
/*      */ import java.util.List;
/*      */ import org.apache.log4j.Logger;
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ public class AirLineRecordValidator
/*      */ {
/*   28 */   private static final Logger LOGGER = Logger.getLogger(AirLineRecordValidator.class);
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void validateAirLineTypeRec(TransactionAdviceBasicBean transactionAdviceBasicType, TransactionAdviceAddendumBean transactionAddendumType, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*   45 */     LOGGER.info("Entering into validateAirLineTypeRec");
/*   46 */     boolean isPaperSubmitter = false;
/*   47 */     if (!CommonValidator.isNullOrEmpty(transactionAdviceBasicType
/*   48 */       .getSubmissionMethod()))
/*      */     {
/*   50 */       if (SubmissionMethod.Paper_external_vendor_paper_processor.getSubmissionMethod().equals(
/*   51 */         transactionAdviceBasicType.getSubmissionMethod()))
/*   52 */         isPaperSubmitter = true;
/*      */     }
/*   54 */     AirlineIndustryTAABean airlineIndustryTAABean = (AirlineIndustryTAABean)transactionAddendumType;
/*      */     
/*      */ 
/*   57 */     TAADBRecordValidator.validateRecordType(airlineIndustryTAABean
/*   58 */       .getRecordType(), errorCodes);
/*   59 */     TAADBRecordValidator.validateRecordNumber(airlineIndustryTAABean
/*   60 */       .getRecordNumber(), errorCodes);
/*   61 */     TAADBRecordValidator.validateTransactionIdentifier(
/*   62 */       airlineIndustryTAABean.getTransactionIdentifier(), 
/*   63 */       transactionAdviceBasicType, errorCodes);
/*   64 */     TAADBRecordValidator.validateAddendaTypeCode(airlineIndustryTAABean
/*   65 */       .getAddendaTypeCode(), errorCodes);
/*   66 */     TAADBRecordValidator.validateFormatCode(airlineIndustryTAABean
/*   67 */       .getFormatCode(), transactionAdviceBasicType, errorCodes);
/*      */     
/*   69 */     validateTransactionType(airlineIndustryTAABean.getTransactionType(), 
/*   70 */       errorCodes);
/*   71 */     validateTicketNumber(airlineIndustryTAABean, errorCodes);
/*   72 */     validateDocumentType(airlineIndustryTAABean.getDocumentType(), isPaperSubmitter, 
/*   73 */       errorCodes);
/*   74 */     validateAirlineProcessIdentifier(airlineIndustryTAABean
/*   75 */       .getAirlineProcessIdentifier(), isPaperSubmitter, errorCodes);
/*   76 */     validateIataNumericCode(airlineIndustryTAABean.getIataNumericCode(), 
/*   77 */       isPaperSubmitter, errorCodes);
/*   78 */     validateTicketingCarrierName(airlineIndustryTAABean
/*   79 */       .getTicketingCarrierName(), isPaperSubmitter, errorCodes);
/*   80 */     validateTicketIssueCity(airlineIndustryTAABean.getTicketIssueCity(), 
/*   81 */       isPaperSubmitter, errorCodes);
/*   82 */     validateTicketIssueDate(airlineIndustryTAABean.getTicketIssueDate(), 
/*   83 */       isPaperSubmitter, errorCodes);
/*   84 */     validateNumberInParty(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
/*   85 */     validatePassengerName(airlineIndustryTAABean, errorCodes);
/*   86 */     validateConjunctionTicketIndicator(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
/*   87 */     validateElectronicTicketIndicator(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
/*   88 */     validateTotalNumberOfAirSegments(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
/*   89 */     validateOriginalTicketNumber(airlineIndustryTAABean, errorCodes);
/*   90 */     validateOriginalTransactionAmount(airlineIndustryTAABean, errorCodes);
/*      */     
/*   92 */     validateStopperIndicator1(airlineIndustryTAABean
/*   93 */       .getStopoverIndicator1(), errorCodes);
/*   94 */     validateDepartureLocationCodeSegment1(airlineIndustryTAABean, 
/*   95 */       isPaperSubmitter, errorCodes);
/*   96 */     validateDepartureDateSegment1(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
/*   97 */     validateArrivalLocationCodeSegment1(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
/*   98 */     validateSegmentCarrierCode1(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
/*   99 */     validateSegment1FareBasis(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
/*  100 */     validateClassOfServiceCodeSegment1(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
/*  101 */     validateFlightNumberSegment1(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
/*  102 */     validateSegment1Fare(airlineIndustryTAABean, errorCodes);
/*      */     
/*  104 */     validateStopperIndicator2(airlineIndustryTAABean
/*  105 */       .getStopoverIndicator2(), errorCodes);
/*  106 */     validateDepartureLocationCodeSegment2(airlineIndustryTAABean, 
/*  107 */       isPaperSubmitter, errorCodes);
/*  108 */     validateDepartureDateSegment2(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
/*  109 */     validateArrivalLocationCodeSegment2(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
/*  110 */     validateSegmentCarrierCode2(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
/*  111 */     validateSegment2FareBasis(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
/*  112 */     validateClassOfServiceCodeSegment2(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
/*  113 */     validateFlightNumberSegment2(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
/*  114 */     validateSegment2Fare(airlineIndustryTAABean, errorCodes);
/*      */     
/*  116 */     validateStopperIndicator3(airlineIndustryTAABean
/*  117 */       .getStopoverIndicator3(), errorCodes);
/*  118 */     validateDepartureLocationCodeSegment3(airlineIndustryTAABean, 
/*  119 */       isPaperSubmitter, errorCodes);
/*  120 */     validateDepartureDateSegment3(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
/*  121 */     validateArrivalLocationCodeSegment3(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
/*  122 */     validateSegmentCarrierCode3(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
/*  123 */     validateSegment3FareBasis(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
/*  124 */     validateClassOfServiceCodeSegment3(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
/*  125 */     validateFlightNumberSegment3(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
/*  126 */     validateSegment3Fare(airlineIndustryTAABean, errorCodes);
/*      */     
/*  128 */     validateStopperIndicator4(airlineIndustryTAABean
/*  129 */       .getStopoverIndicator4(), errorCodes);
/*  130 */     validateDepartureLocationCodeSegment4(airlineIndustryTAABean, 
/*  131 */       isPaperSubmitter, errorCodes);
/*  132 */     validateDepartureDateSegment4(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
/*  133 */     validateArrivalLocationCodeSegment4(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
/*  134 */     validateSegmentCarrierCode4(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
/*  135 */     validateSegment4FareBasis(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
/*  136 */     validateClassOfServiceCodeSegment4(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
/*  137 */     validateFlightNumberSegment4(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
/*  138 */     validateSegment4Fare(airlineIndustryTAABean, errorCodes);
/*      */     
/*  140 */     validateStopperIndicator5(airlineIndustryTAABean
/*  141 */       .getStopoverIndicator5(), errorCodes);
/*  142 */     validateOriginalCurrencyCode(airlineIndustryTAABean, errorCodes);
/*      */     
/*  144 */     LOGGER.info("Exiting from validateAirLineTypeRec");
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTransactionType(String transactionType, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  160 */     ErrorObject errorObj = null;
/*  161 */     if (CommonValidator.isNullOrEmpty(transactionType))
/*      */     {
/*  163 */       errorObj = new ErrorObject(
/*  164 */         SubmissionErrorCodes.ERROR_DATAFIELD139_TRANSACTION_TYPE);
/*      */       
/*      */ 
/*      */ 
/*  168 */       errorCodes.add(errorObj);
/*      */     }
/*  170 */     else if (CommonValidator.validateData(transactionType, 
/*  171 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['']))
/*      */     {
/*  173 */       String reqLength = CommonValidator.validateLength(transactionType, 3, 3);
/*      */       
/*  175 */       if (reqLength.equals("greaterThanMax")) {
/*  176 */         errorObj = new ErrorObject(
/*  177 */           SubmissionErrorCodes.ERROR_DATAFIELD139_TRANSACTION_TYPE);
/*      */         
/*      */ 
/*      */ 
/*  181 */         errorCodes.add(errorObj);
/*      */       }
/*  183 */       else if (!transactionType.equalsIgnoreCase("TKT"))
/*      */       {
/*  185 */         if (!transactionType.equalsIgnoreCase("REF"))
/*      */         {
/*  187 */           if (!transactionType.equalsIgnoreCase("EXC"))
/*      */           {
/*  189 */             if (!transactionType.equalsIgnoreCase("MSC")) {
/*  190 */               errorObj = new ErrorObject(
/*  191 */                 SubmissionErrorCodes.ERROR_DATAFIELD139_TRANSACTION_TYPE);
/*      */               
/*      */ 
/*      */ 
/*  195 */               errorCodes.add(errorObj);
/*      */             } } }
/*      */       }
/*      */     } else {
/*  199 */       errorObj = new ErrorObject(
/*  200 */         SubmissionErrorCodes.ERROR_DATAFIELD139_TRANSACTION_TYPE);
/*      */       
/*      */ 
/*      */ 
/*  204 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTicketNumber(AirlineIndustryTAABean airlineIndustryTAABean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  223 */     ErrorObject errorObj = null;
/*      */     
/*      */ 
/*  226 */     if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
/*      */     
/*  228 */       (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && 
/*  229 */       (CommonValidator.isNullOrEmpty(airlineIndustryTAABean
/*  230 */       .getTicketNumber())))
/*      */     {
/*  232 */       errorObj = new ErrorObject(
/*  233 */         SubmissionErrorCodes.ERROR_DATAFIELD140_TICKET_NUMBER);
/*      */       
/*      */ 
/*      */ 
/*  237 */       errorCodes.add(errorObj);
/*      */     }
/*  239 */     else if (!CommonValidator.isNullOrEmpty(airlineIndustryTAABean
/*  240 */       .getTicketNumber()))
/*      */     {
/*      */ 
/*      */ 
/*      */ 
/*  245 */       String reqLength = CommonValidator.validateLength(
/*  246 */         airlineIndustryTAABean.getTicketNumber(), 14, 14);
/*      */       
/*  248 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  250 */         errorObj = new ErrorObject(
/*  251 */           SubmissionErrorCodes.ERROR_DATAFIELD140_TICKET_NUMBER);
/*      */         
/*      */ 
/*      */ 
/*  255 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateDocumentType(String documentType, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  281 */     ErrorObject errorObj = null;
/*  282 */     if (CommonValidator.isNullOrEmpty(documentType))
/*      */     {
/*  284 */       if (!isPaperSubmitter) {
/*  285 */         errorObj = new ErrorObject(
/*  286 */           SubmissionErrorCodes.ERROR_DATAFIELD141_DOCUMENT_TYPE);
/*      */         
/*      */ 
/*      */ 
/*  290 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*  293 */     else if (CommonValidator.validateData(documentType, 
/*  294 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['']))
/*      */     {
/*  296 */       String reqLength = CommonValidator.validateLength(documentType, 2, 2);
/*      */       
/*  298 */       if (reqLength.equals("greaterThanMax")) {
/*  299 */         errorObj = new ErrorObject(
/*  300 */           SubmissionErrorCodes.ERROR_DATAFIELD141_DOCUMENT_TYPE);
/*      */         
/*      */ 
/*      */ 
/*  304 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/*  307 */       errorObj = new ErrorObject(
/*  308 */         SubmissionErrorCodes.ERROR_DATAFIELD141_DOCUMENT_TYPE);
/*      */       
/*      */ 
/*      */ 
/*  312 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateAirlineProcessIdentifier(String airlineProcessIdentifier, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  330 */     ErrorObject errorObj = null;
/*  331 */     if (CommonValidator.isNullOrEmpty(airlineProcessIdentifier))
/*      */     {
/*  333 */       if (!isPaperSubmitter) {
/*  334 */         errorObj = new ErrorObject(
/*  335 */           SubmissionErrorCodes.ERROR_DATAFIELD142_AIRLINEPROCESS_IDENTIFIER);
/*      */         
/*      */ 
/*      */ 
/*  339 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*  342 */     else if (CommonValidator.validateData(airlineProcessIdentifier, 
/*  343 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['']))
/*      */     {
/*  345 */       String reqLength = CommonValidator.validateLength(airlineProcessIdentifier, 3, 3);
/*      */       
/*  347 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  349 */         errorObj = new ErrorObject(
/*  350 */           SubmissionErrorCodes.ERROR_DATAFIELD142_AIRLINEPROCESS_IDENTIFIER);
/*      */         
/*      */ 
/*      */ 
/*  354 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/*  357 */       errorObj = new ErrorObject(
/*  358 */         SubmissionErrorCodes.ERROR_DATAFIELD142_AIRLINEPROCESS_IDENTIFIER);
/*      */       
/*      */ 
/*      */ 
/*  362 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateIataNumericCode(String iataNumericCode, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  380 */     ErrorObject errorObj = null;
/*  381 */     if (CommonValidator.isNullOrEmpty(iataNumericCode))
/*      */     {
/*  383 */       if (!isPaperSubmitter) {
/*  384 */         errorObj = new ErrorObject(
/*  385 */           SubmissionErrorCodes.ERROR_DATAFIELD143_IATANUMERIC_CODE);
/*      */         
/*      */ 
/*      */ 
/*  389 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*  392 */     else if (CommonValidator.validateData(iataNumericCode, 
/*  393 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['']))
/*      */     {
/*  395 */       String reqLength = CommonValidator.validateLength(iataNumericCode, 8, 8);
/*      */       
/*  397 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  399 */         errorObj = new ErrorObject(
/*  400 */           SubmissionErrorCodes.ERROR_DATAFIELD143_IATANUMERIC_CODE);
/*      */         
/*      */ 
/*      */ 
/*  404 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/*  407 */       errorObj = new ErrorObject(
/*  408 */         SubmissionErrorCodes.ERROR_DATAFIELD143_IATANUMERIC_CODE);
/*      */       
/*      */ 
/*      */ 
/*  412 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTicketingCarrierName(String ticketingCarrierName, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  430 */     ErrorObject errorObj = null;
/*  431 */     if (CommonValidator.isNullOrEmpty(ticketingCarrierName))
/*      */     {
/*  433 */       if (!isPaperSubmitter) {
/*  434 */         errorObj = new ErrorObject(
/*  435 */           SubmissionErrorCodes.ERROR_DATAFIELD144_TICKETING_CARRIERNAME);
/*      */         
/*      */ 
/*      */ 
/*  439 */         errorCodes.add(errorObj);
/*      */       }
/*      */       
/*      */ 
/*      */     }
/*      */     else
/*      */     {
/*  446 */       String reqLength = 
/*  447 */         CommonValidator.validateLength(ticketingCarrierName, 25, 25);
/*      */       
/*  449 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  451 */         errorObj = new ErrorObject(
/*  452 */           SubmissionErrorCodes.ERROR_DATAFIELD144_TICKETING_CARRIERNAME);
/*      */         
/*      */ 
/*      */ 
/*  456 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTicketIssueCity(String ticketIssueCity, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  482 */     ErrorObject errorObj = null;
/*  483 */     if (CommonValidator.isNullOrEmpty(ticketIssueCity))
/*      */     {
/*  485 */       if (!isPaperSubmitter) {
/*  486 */         errorObj = new ErrorObject(
/*  487 */           SubmissionErrorCodes.ERROR_DATAFIELD145_TICKETISSUE_CITY);
/*      */         
/*      */ 
/*      */ 
/*  491 */         errorCodes.add(errorObj);
/*      */       }
/*      */       
/*      */ 
/*      */     }
/*      */     else
/*      */     {
/*  498 */       String reqLength = 
/*  499 */         CommonValidator.validateLength(ticketIssueCity, 18, 18);
/*      */       
/*  501 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  503 */         errorObj = new ErrorObject(
/*  504 */           SubmissionErrorCodes.ERROR_DATAFIELD145_TICKETISSUE_CITY);
/*      */         
/*      */ 
/*      */ 
/*  508 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTicketIssueDate(String ticketIssueDate, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  534 */     ErrorObject errorObj = null;
/*  535 */     if (CommonValidator.isNullOrEmpty(ticketIssueDate))
/*      */     {
/*  537 */       if (!isPaperSubmitter) {
/*  538 */         errorObj = new ErrorObject(
/*  539 */           SubmissionErrorCodes.ERROR_DATAFIELD146_TICKETISSUE_DATE);
/*      */         
/*      */ 
/*      */ 
/*  543 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*  546 */     else if (CommonValidator.validateData(ticketIssueDate, 
/*  547 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['']))
/*      */     {
/*  549 */       String reqLength = CommonValidator.validateLength(ticketIssueDate, 8, 8);
/*      */       
/*  551 */       if ((!reqLength.equals("equal")) || 
/*  552 */         (!CommonValidator.isValidDate(ticketIssueDate, 
/*  553 */         "CCYYMMDD")))
/*      */       {
/*  555 */         errorObj = new ErrorObject(
/*  556 */           SubmissionErrorCodes.ERROR_DATAFIELD146_TICKETISSUE_DATE);
/*      */         
/*      */ 
/*      */ 
/*  560 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/*  563 */       errorObj = new ErrorObject(
/*  564 */         SubmissionErrorCodes.ERROR_DATAFIELD146_TICKETISSUE_DATE);
/*      */       
/*      */ 
/*      */ 
/*  568 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateNumberInParty(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  587 */     ErrorObject errorObj = null;
/*  588 */     if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean
/*  589 */       .getNumberInParty()))
/*      */     {
/*      */ 
/*  592 */       if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
/*      */       
/*  594 */         (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter)) {
/*  595 */         errorObj = new ErrorObject(
/*  596 */           SubmissionErrorCodes.ERROR_DATAFIELD147_NUMBERIN_PARTY);
/*      */         
/*      */ 
/*      */ 
/*  600 */         errorCodes.add(errorObj);
/*      */       }
/*      */       
/*      */     }
/*  604 */     else if (CommonValidator.validateData(airlineIndustryTAABean
/*  605 */       .getNumberInParty(), com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['']))
/*      */     {
/*  607 */       String reqLength = CommonValidator.validateLength(
/*  608 */         airlineIndustryTAABean.getNumberInParty(), 3, 3);
/*      */       
/*  610 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  612 */         errorObj = new ErrorObject(
/*  613 */           SubmissionErrorCodes.ERROR_DATAFIELD147_NUMBERIN_PARTY);
/*      */         
/*      */ 
/*      */ 
/*  617 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/*  620 */       errorObj = new ErrorObject(
/*  621 */         SubmissionErrorCodes.ERROR_DATAFIELD147_NUMBERIN_PARTY);
/*      */       
/*      */ 
/*      */ 
/*  625 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validatePassengerName(AirlineIndustryTAABean airlineIndustryTAABean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  644 */     ErrorObject errorObj = null;
/*      */     
/*      */ 
/*  647 */     if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
/*      */     
/*  649 */       (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && 
/*  650 */       (CommonValidator.isNullOrEmpty(airlineIndustryTAABean
/*  651 */       .getPassengerName())))
/*      */     {
/*  653 */       errorObj = new ErrorObject(
/*  654 */         SubmissionErrorCodes.ERROR_DATAFIELD148_PASSENGER_NAME);
/*      */       
/*      */ 
/*      */ 
/*  658 */       errorCodes.add(errorObj);
/*      */     }
/*  660 */     else if (!CommonValidator.isNullOrEmpty(airlineIndustryTAABean
/*  661 */       .getPassengerName()))
/*      */     {
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  667 */       String reqLength = CommonValidator.validateLength(
/*  668 */         airlineIndustryTAABean.getPassengerName(), 25, 25);
/*      */       
/*  670 */       if (reqLength.equals("greaterThanMax")) {
/*  671 */         errorObj = new ErrorObject(
/*  672 */           SubmissionErrorCodes.ERROR_DATAFIELD148_PASSENGER_NAME);
/*      */         
/*      */ 
/*      */ 
/*  676 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateConjunctionTicketIndicator(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  703 */     ErrorObject errorObj = null;
/*  704 */     if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean
/*  705 */       .getConjunctionTicketIndicator()))
/*      */     {
/*      */ 
/*  708 */       if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
/*      */       
/*  710 */         (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter)) {
/*  711 */         errorObj = new ErrorObject(
/*  712 */           SubmissionErrorCodes.ERROR_DATAFIELD149_CONJUNCTIONTICKET_INDICATOR);
/*      */         
/*      */ 
/*      */ 
/*  716 */         errorCodes.add(errorObj);
/*      */       }
/*      */       
/*      */     }
/*  720 */     else if (CommonValidator.validateData(airlineIndustryTAABean
/*  721 */       .getConjunctionTicketIndicator(), 
/*  722 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['']))
/*      */     {
/*  724 */       String reqLength = CommonValidator.validateLength(
/*  725 */         airlineIndustryTAABean.getConjunctionTicketIndicator(), 
/*  726 */         1, 1);
/*      */       
/*  728 */       if (reqLength.equals("greaterThanMax")) {
/*  729 */         errorObj = new ErrorObject(
/*  730 */           SubmissionErrorCodes.ERROR_DATAFIELD149_CONJUNCTIONTICKET_INDICATOR);
/*      */         
/*      */ 
/*      */ 
/*  734 */         errorCodes.add(errorObj);
/*      */ 
/*      */       }
/*  737 */       else if (!airlineIndustryTAABean.getConjunctionTicketIndicator().equalsIgnoreCase("Y"))
/*      */       {
/*      */ 
/*  740 */         if (!airlineIndustryTAABean.getConjunctionTicketIndicator().equalsIgnoreCase("N")) {
/*  741 */           errorObj = new ErrorObject(
/*  742 */             SubmissionErrorCodes.ERROR_DATAFIELD149_CONJUNCTIONTICKET_INDICATOR);
/*      */           
/*      */ 
/*      */ 
/*  746 */           errorCodes.add(errorObj);
/*      */         }
/*      */       }
/*      */     } else {
/*  750 */       errorObj = new ErrorObject(
/*  751 */         SubmissionErrorCodes.ERROR_DATAFIELD149_CONJUNCTIONTICKET_INDICATOR);
/*      */       
/*      */ 
/*      */ 
/*  755 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateOriginalTransactionAmount(AirlineIndustryTAABean airIndustryTAABean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  774 */     ErrorObject errorObj = null;
/*  775 */     if (!CommonValidator.isNullOrEmpty(airIndustryTAABean
/*  776 */       .getOriginalCurrencyCode()))
/*  777 */       if (CommonValidator.isNullOrEmpty(airIndustryTAABean
/*  778 */         .getOriginalTransactionAmount()))
/*      */       {
/*  780 */         errorObj = new ErrorObject(
/*  781 */           SubmissionErrorCodes.ERROR_DATAFIELD154_ORIGINALTRANSACTION_AMOUNT);
/*      */         
/*      */ 
/*      */ 
/*  785 */         errorCodes.add(errorObj); return; }
/*  786 */     if (!CommonValidator.isNullOrEmpty(airIndustryTAABean
/*  787 */       .getOriginalTransactionAmount())) {
/*  788 */       if (CommonValidator.validateData(airIndustryTAABean
/*  789 */         .getOriginalTransactionAmount(), 
/*  790 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['']))
/*      */       {
/*  792 */         String reqLength = CommonValidator.validateLength(
/*  793 */           airIndustryTAABean.getOriginalTransactionAmount(), 12, 
/*  794 */           12);
/*      */         
/*  796 */         if (reqLength.equals("greaterThanMax"))
/*      */         {
/*  798 */           errorObj = new ErrorObject(
/*  799 */             SubmissionErrorCodes.ERROR_DATAFIELD153_ORIGINALTRANSACTION_AMOUNT);
/*      */           
/*      */ 
/*      */ 
/*  803 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/*  806 */         errorObj = new ErrorObject(
/*  807 */           SubmissionErrorCodes.ERROR_DATAFIELD152_ORIGINALTRANSACTION_AMOUNT);
/*      */         
/*      */ 
/*      */ 
/*  811 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateOriginalCurrencyCode(AirlineIndustryTAABean airIndustryTAABean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  829 */     ErrorObject errorObj = null;
/*  830 */     if (!CommonValidator.isNullOrEmpty(airIndustryTAABean
/*  831 */       .getOriginalTransactionAmount()))
/*  832 */       if (CommonValidator.isNullOrEmpty(airIndustryTAABean
/*  833 */         .getOriginalCurrencyCode()))
/*      */       {
/*  835 */         errorObj = new ErrorObject(
/*  836 */           SubmissionErrorCodes.ERROR_DATAFIELD156_ORIGINALCURRENCY_CODE);
/*      */         
/*      */ 
/*      */ 
/*  840 */         errorCodes.add(errorObj); return; }
/*  841 */     if (!CommonValidator.isNullOrEmpty(airIndustryTAABean
/*  842 */       .getOriginalCurrencyCode())) {
/*  843 */       if (CommonValidator.validateData(airIndustryTAABean
/*  844 */         .getOriginalCurrencyCode(), 
/*  845 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['']))
/*      */       {
/*  847 */         String reqLength = CommonValidator.validateLength(
/*  848 */           airIndustryTAABean.getOriginalCurrencyCode(), 3, 3);
/*      */         
/*  850 */         if (reqLength.equals("greaterThanMax"))
/*      */         {
/*  852 */           errorObj = new ErrorObject(
/*  853 */             SubmissionErrorCodes.ERROR_DATAFIELD155_ORIGINALCURRENCY_CODE);
/*      */           
/*      */ 
/*      */ 
/*  857 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/*  860 */         errorObj = new ErrorObject(
/*  861 */           SubmissionErrorCodes.ERROR_DATAFIELD155_ORIGINALCURRENCY_CODE);
/*      */         
/*      */ 
/*      */ 
/*  865 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateElectronicTicketIndicator(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  883 */     ErrorObject errorObj = null;
/*  884 */     if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean
/*  885 */       .getElectronicTicketIndicator()))
/*      */     {
/*      */ 
/*  888 */       if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
/*      */       
/*  890 */         (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter)) {
/*  891 */         errorObj = new ErrorObject(
/*  892 */           SubmissionErrorCodes.ERROR_DATAFIELD150_ELECTRONIC_TICKETINDICATOR);
/*      */         
/*      */ 
/*      */ 
/*  896 */         errorCodes.add(errorObj);
/*      */       }
/*      */       
/*      */     }
/*  900 */     else if (CommonValidator.validateData(airlineIndustryTAABean
/*  901 */       .getElectronicTicketIndicator(), 
/*  902 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['']))
/*      */     {
/*  904 */       String reqLength = CommonValidator.validateLength(
/*  905 */         airlineIndustryTAABean.getElectronicTicketIndicator(), 
/*  906 */         1, 1);
/*      */       
/*  908 */       if (reqLength.equals("greaterThanMax")) {
/*  909 */         errorObj = new ErrorObject(
/*  910 */           SubmissionErrorCodes.ERROR_DATAFIELD150_ELECTRONIC_TICKETINDICATOR);
/*      */         
/*      */ 
/*      */ 
/*  914 */         errorCodes.add(errorObj);
/*      */ 
/*      */       }
/*  917 */       else if (!airlineIndustryTAABean.getElectronicTicketIndicator().equalsIgnoreCase("Y"))
/*      */       {
/*      */ 
/*  920 */         if (!airlineIndustryTAABean.getElectronicTicketIndicator().equalsIgnoreCase("N")) {
/*  921 */           errorObj = new ErrorObject(
/*  922 */             SubmissionErrorCodes.ERROR_DATAFIELD150_ELECTRONIC_TICKETINDICATOR);
/*      */           
/*      */ 
/*      */ 
/*  926 */           errorCodes.add(errorObj);
/*      */         }
/*      */       }
/*      */     } else {
/*  930 */       errorObj = new ErrorObject(
/*  931 */         SubmissionErrorCodes.ERROR_DATAFIELD150_ELECTRONIC_TICKETINDICATOR);
/*      */       
/*      */ 
/*      */ 
/*  935 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTotalNumberOfAirSegments(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  954 */     ErrorObject errorObj = null;
/*  955 */     if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean
/*  956 */       .getTotalNumberOfAirSegments()))
/*      */     {
/*      */ 
/*  959 */       if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
/*      */       
/*  961 */         (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter)) {
/*  962 */         errorObj = new ErrorObject(
/*  963 */           SubmissionErrorCodes.ERROR_DATAFIELD151_TOTAL_NUMBER_OF_AIR_SEGMENTS);
/*      */         
/*      */ 
/*      */ 
/*  967 */         errorCodes.add(errorObj);
/*      */       }
/*      */       
/*      */ 
/*      */     }
/*  972 */     else if (CommonValidator.validateData(airlineIndustryTAABean
/*  973 */       .getTotalNumberOfAirSegments(), 
/*  974 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['']))
/*      */     {
/*  976 */       String reqLength = CommonValidator.validateLength(
/*  977 */         airlineIndustryTAABean.getTotalNumberOfAirSegments(), 
/*  978 */         1, 1);
/*      */       
/*  980 */       if (reqLength.equals("greaterThanMax")) {
/*  981 */         errorObj = new ErrorObject(
/*  982 */           SubmissionErrorCodes.ERROR_DATAFIELD151_TOTAL_NUMBER_OF_AIR_SEGMENTS);
/*      */         
/*      */ 
/*      */ 
/*  986 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/*  989 */       errorObj = new ErrorObject(
/*  990 */         SubmissionErrorCodes.ERROR_DATAFIELD151_TOTAL_NUMBER_OF_AIR_SEGMENTS);
/*      */       
/*      */ 
/*      */ 
/*  994 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateStopperIndicator1(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1012 */     ErrorObject errorObj = null;
/*      */     
/* 1014 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/* 1016 */       if (CommonValidator.validateData(value, 
/* 1017 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['']))
/*      */       {
/* 1019 */         String reqLength = CommonValidator.validateLength(value, 1, 1);
/*      */         
/* 1021 */         if ((reqLength.equals("greaterThanMax")) || 
/* 1022 */           (!CommonValidator.isValidStopOverIndicator(value))) {
/* 1023 */           errorObj = new ErrorObject(
/* 1024 */             SubmissionErrorCodes.ERROR_DATAFIELD157_STOPOVER1_INDICATOR);
/*      */           
/*      */ 
/*      */ 
/* 1028 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/* 1031 */         errorObj = new ErrorObject(
/* 1032 */           SubmissionErrorCodes.ERROR_DATAFIELD157_STOPOVER1_INDICATOR);
/*      */         
/*      */ 
/*      */ 
/* 1036 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateDepartureLocationCodeSegment1(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1055 */     ErrorObject errorObj = null;
/* 1056 */     if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean
/* 1057 */       .getDepartureLocationCodeSegment1()))
/*      */     {
/*      */ 
/* 1060 */       if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
/*      */       
/* 1062 */         (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter)) {
/* 1063 */         errorObj = new ErrorObject(
/* 1064 */           SubmissionErrorCodes.ERROR_DATAFIELD158_DEPARTURELOCATION_CODE_SEGMENT1);
/*      */         
/*      */ 
/*      */ 
/* 1068 */         errorCodes.add(errorObj);
/*      */       }
/*      */       
/*      */     }
/* 1072 */     else if (CommonValidator.validateData(airlineIndustryTAABean
/* 1073 */       .getDepartureLocationCodeSegment1(), 
/* 1074 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['']))
/*      */     {
/* 1076 */       String reqLength = CommonValidator.validateLength(
/* 1077 */         airlineIndustryTAABean
/* 1078 */         .getDepartureLocationCodeSegment1(), 3, 3);
/*      */       
/* 1080 */       if (reqLength.equals("greaterThanMax")) {
/* 1081 */         errorObj = new ErrorObject(
/* 1082 */           SubmissionErrorCodes.ERROR_DATAFIELD159_DEPARTURELOCATION_CODE_SEGMENT1);
/*      */       }
/*      */       
/*      */ 
/*      */     }
/*      */     else
/*      */     {
/* 1089 */       errorObj = new ErrorObject(
/* 1090 */         SubmissionErrorCodes.ERROR_DATAFIELD159_DEPARTURELOCATION_CODE_SEGMENT1);
/*      */       
/*      */ 
/*      */ 
/* 1094 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateDepartureDateSegment1(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1113 */     ErrorObject errorObj = null;
/* 1114 */     if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean
/* 1115 */       .getDepartureDateSegment1()))
/*      */     {
/*      */ 
/* 1118 */       if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
/*      */       
/* 1120 */         (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter)) {
/* 1121 */         errorObj = new ErrorObject(
/* 1122 */           SubmissionErrorCodes.ERROR_DATAFIELD160_DEPARTURE_DATE_SEGMENT1);
/*      */         
/*      */ 
/*      */ 
/* 1126 */         errorCodes.add(errorObj);
/*      */       }
/*      */       
/*      */     }
/* 1130 */     else if (CommonValidator.validateData(airlineIndustryTAABean
/* 1131 */       .getDepartureDateSegment1(), 
/* 1132 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['']))
/*      */     {
/* 1134 */       String reqLength = 
/* 1135 */         CommonValidator.validateLength(airlineIndustryTAABean
/* 1136 */         .getDepartureDateSegment1(), 8, 8);
/*      */       
/* 1138 */       if (reqLength.equals("greaterThanMax")) {
/* 1139 */         errorObj = new ErrorObject(
/* 1140 */           SubmissionErrorCodes.ERROR_DATAFIELD161_DEPARTURE_DATE_SEGMENT1);
/*      */         
/*      */ 
/*      */ 
/* 1144 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/* 1147 */       errorObj = new ErrorObject(
/* 1148 */         SubmissionErrorCodes.ERROR_DATAFIELD161_DEPARTURE_DATE_SEGMENT1);
/*      */       
/*      */ 
/*      */ 
/* 1152 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateArrivalLocationCodeSegment1(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1171 */     ErrorObject errorObj = null;
/* 1172 */     if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean
/* 1173 */       .getArrivalLocationCodeSegment1()))
/*      */     {
/*      */ 
/* 1176 */       if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
/*      */       
/* 1178 */         (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter)) {
/* 1179 */         errorObj = new ErrorObject(
/* 1180 */           SubmissionErrorCodes.ERROR_DATAFIELD162_ARRIVALLOCATION_CODESEGMENT1);
/*      */         
/*      */ 
/*      */ 
/* 1184 */         errorCodes.add(errorObj);
/*      */       }
/*      */       
/*      */     }
/* 1188 */     else if (CommonValidator.validateData(airlineIndustryTAABean
/* 1189 */       .getArrivalLocationCodeSegment1(), 
/* 1190 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['']))
/*      */     {
/* 1192 */       String reqLength = 
/* 1193 */         CommonValidator.validateLength(airlineIndustryTAABean
/* 1194 */         .getArrivalLocationCodeSegment1(), 3, 3);
/*      */       
/* 1196 */       if (reqLength.equals("greaterThanMax")) {
/* 1197 */         errorObj = new ErrorObject(
/* 1198 */           SubmissionErrorCodes.ERROR_DATAFIELD163_ARRIVALLOCATION_CODESEGMENT1);
/*      */         
/*      */ 
/*      */ 
/* 1202 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/* 1205 */       errorObj = new ErrorObject(
/* 1206 */         SubmissionErrorCodes.ERROR_DATAFIELD163_ARRIVALLOCATION_CODESEGMENT1);
/*      */       
/*      */ 
/*      */ 
/* 1210 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateSegmentCarrierCode1(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1229 */     ErrorObject errorObj = null;
/* 1230 */     if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean
/* 1231 */       .getSegmentCarrierCode1()))
/*      */     {
/*      */ 
/* 1234 */       if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
/*      */       
/* 1236 */         (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter)) {
/* 1237 */         errorObj = new ErrorObject(
/* 1238 */           SubmissionErrorCodes.ERROR_DATAFIELD164_SEGMENTCARRIER_CODE1);
/*      */         
/*      */ 
/*      */ 
/* 1242 */         errorCodes.add(errorObj);
/*      */       }
/*      */       
/*      */     }
/* 1246 */     else if (CommonValidator.validateData(airlineIndustryTAABean
/* 1247 */       .getSegmentCarrierCode1(), 
/* 1248 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['']))
/*      */     {
/* 1250 */       String reqLength = CommonValidator.validateLength(
/* 1251 */         airlineIndustryTAABean.getSegmentCarrierCode1(), 2, 2);
/*      */       
/* 1253 */       if (reqLength.equals("greaterThanMax")) {
/* 1254 */         errorObj = new ErrorObject(
/* 1255 */           SubmissionErrorCodes.ERROR_DATAFIELD165_SEGMENTCARRIER_CODE1);
/*      */         
/*      */ 
/*      */ 
/* 1259 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/* 1262 */       errorObj = new ErrorObject(
/* 1263 */         SubmissionErrorCodes.ERROR_DATAFIELD165_SEGMENTCARRIER_CODE1);
/*      */       
/*      */ 
/*      */ 
/* 1267 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateSegment1FareBasis(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1286 */     ErrorObject errorObj = null;
/* 1287 */     if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean
/* 1288 */       .getSegment1FareBasis()))
/*      */     {
/*      */ 
/* 1291 */       if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
/*      */       
/* 1293 */         (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter)) {
/* 1294 */         errorObj = new ErrorObject(
/* 1295 */           SubmissionErrorCodes.ERROR_DATAFIELD166_SEGMENT1FARE_BASIS);
/*      */         
/*      */ 
/*      */ 
/* 1299 */         errorCodes.add(errorObj);
/*      */ 
/*      */       }
/*      */       
/*      */ 
/*      */     }
/*      */     else
/*      */     {
/*      */ 
/* 1308 */       String reqLength = CommonValidator.validateLength(
/* 1309 */         airlineIndustryTAABean.getSegment1FareBasis(), 15, 15);
/*      */       
/* 1311 */       if (reqLength.equals("greaterThanMax")) {
/* 1312 */         errorObj = new ErrorObject("AIR", 
/* 1313 */           "Segment1FareBasis length is out of Sequence");
/* 1314 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateClassOfServiceCodeSegment1(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1338 */     ErrorObject errorObj = null;
/* 1339 */     if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean
/* 1340 */       .getClassOfServiceCodeSegment1()))
/*      */     {
/*      */ 
/* 1343 */       if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
/*      */       
/* 1345 */         (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter)) {
/* 1346 */         errorObj = new ErrorObject(
/* 1347 */           SubmissionErrorCodes.ERROR_DATAFIELD167_CLASSOFSERVICE_CODESEGMENT1);
/*      */         
/*      */ 
/*      */ 
/* 1351 */         errorCodes.add(errorObj);
/*      */       }
/*      */       
/*      */     }
/* 1355 */     else if (CommonValidator.validateData(airlineIndustryTAABean
/* 1356 */       .getClassOfServiceCodeSegment1(), 
/* 1357 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['']))
/*      */     {
/* 1359 */       String reqLength = CommonValidator.validateLength(
/* 1360 */         airlineIndustryTAABean.getClassOfServiceCodeSegment1(), 
/* 1361 */         2, 2);
/*      */       
/* 1363 */       if (reqLength.equals("greaterThanMax")) {
/* 1364 */         errorObj = new ErrorObject(
/* 1365 */           SubmissionErrorCodes.ERROR_DATAFIELD167_CLASSOFSERVICE_CODESEGMENT1);
/*      */         
/* 1367 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/* 1370 */       errorObj = new ErrorObject(
/* 1371 */         SubmissionErrorCodes.ERROR_DATAFIELD167_CLASSOFSERVICE_CODESEGMENT1);
/* 1372 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateFlightNumberSegment1(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1391 */     ErrorObject errorObj = null;
/* 1392 */     if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean
/* 1393 */       .getFlightNumberSegment1()))
/*      */     {
/*      */ 
/* 1396 */       if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
/*      */       
/* 1398 */         (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter)) {
/* 1399 */         errorObj = new ErrorObject(
/* 1400 */           SubmissionErrorCodes.ERROR_DATAFIELD168_FLIGHTNUMBER_SEGMENT1);
/* 1401 */         errorCodes.add(errorObj);
/*      */ 
/*      */       }
/*      */       
/*      */ 
/*      */     }
/*      */     else
/*      */     {
/*      */ 
/* 1410 */       String reqLength = CommonValidator.validateLength(
/* 1411 */         airlineIndustryTAABean.getFlightNumberSegment1(), 4, 4);
/*      */       
/* 1413 */       if (reqLength.equals("greaterThanMax")) {
/* 1414 */         errorObj = new ErrorObject("AIR", 
/* 1415 */           "FlightNumberSegment1 Length is Out of Sequence");
/* 1416 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateSegment1Fare(AirlineIndustryTAABean airlineIndustryTAABean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1440 */     ErrorObject errorObj = null;
/*      */     
/* 1442 */     if (!CommonValidator.isNullOrEmpty(airlineIndustryTAABean
/* 1443 */       .getSegment1Fare())) {
/* 1444 */       if (CommonValidator.validateData(airlineIndustryTAABean
/* 1445 */         .getSegment1Fare(), com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['']))
/*      */       {
/* 1447 */         String reqLength = CommonValidator.validateLength(
/* 1448 */           airlineIndustryTAABean.getSegment1Fare(), 12, 12);
/*      */         
/* 1450 */         if (reqLength.equals("greaterThanMax")) {
/* 1451 */           errorObj = new ErrorObject(
/* 1452 */             SubmissionErrorCodes.ERROR_DATAFIELD170_SEGMENT_1FARE);
/* 1453 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/* 1456 */         errorObj = new ErrorObject(
/* 1457 */           SubmissionErrorCodes.ERROR_DATAFIELD169_SEGMENT_1FARE);
/* 1458 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateStopperIndicator2(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1476 */     ErrorObject errorObj = null;
/*      */     
/* 1478 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/* 1480 */       if (CommonValidator.validateData(value, 
/* 1481 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['']))
/*      */       {
/* 1483 */         String reqLength = CommonValidator.validateLength(value, 1, 1);
/*      */         
/* 1485 */         if ((reqLength.equals("greaterThanMax")) || 
/* 1486 */           (!CommonValidator.isValidStopOverIndicator(value)))
/*      */         {
/* 1488 */           errorObj = new ErrorObject(
/* 1489 */             SubmissionErrorCodes.ERROR_DATAFIELD171_STOPOVER2_INDICATOR);
/* 1490 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/* 1493 */         errorObj = new ErrorObject(
/* 1494 */           SubmissionErrorCodes.ERROR_DATAFIELD171_STOPOVER2_INDICATOR);
/* 1495 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateDepartureLocationCodeSegment2(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1514 */     ErrorObject errorObj = null;
/* 1515 */     if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean
/* 1516 */       .getDepartureLocationCodeSegment2()))
/*      */     {
/*      */ 
/* 1519 */       if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
/*      */       
/* 1521 */         (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter)) {
/* 1522 */         errorObj = new ErrorObject(
/* 1523 */           SubmissionErrorCodes.ERROR_DATAFIELD172_DEPARTURELOCATION_CODE_SEGMENT2);
/* 1524 */         errorCodes.add(errorObj);
/*      */       }
/*      */       
/*      */     }
/* 1528 */     else if (CommonValidator.validateData(airlineIndustryTAABean
/* 1529 */       .getDepartureLocationCodeSegment2(), 
/* 1530 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['']))
/*      */     {
/* 1532 */       String reqLength = CommonValidator.validateLength(
/* 1533 */         airlineIndustryTAABean
/* 1534 */         .getDepartureLocationCodeSegment2(), 3, 3);
/*      */       
/* 1536 */       if (reqLength.equals("greaterThanMax")) {
/* 1537 */         errorObj = new ErrorObject(
/* 1538 */           SubmissionErrorCodes.ERROR_DATAFIELD173_DEPARTURELOCATION_CODE_SEGMENT2);
/* 1539 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/* 1542 */       errorObj = new ErrorObject(
/* 1543 */         SubmissionErrorCodes.ERROR_DATAFIELD173_DEPARTURELOCATION_CODE_SEGMENT2);
/* 1544 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateDepartureDateSegment2(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1563 */     ErrorObject errorObj = null;
/* 1564 */     if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean
/* 1565 */       .getDepartureDateSegment2()))
/*      */     {
/*      */ 
/* 1568 */       if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
/*      */       
/* 1570 */         (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter)) {
/* 1571 */         errorObj = new ErrorObject(
/* 1572 */           SubmissionErrorCodes.ERROR_DATAFIELD174_DEPARTURE_DATE_SEGMENT2);
/* 1573 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/* 1576 */     else if (CommonValidator.validateData(airlineIndustryTAABean
/* 1577 */       .getDepartureDateSegment2(), 
/* 1578 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[' ']))
/*      */     {
/* 1580 */       String reqLength = 
/* 1581 */         CommonValidator.validateLength(airlineIndustryTAABean
/* 1582 */         .getDepartureDateSegment2(), 8, 8);
/*      */       
/* 1584 */       if (reqLength.equals("greaterThanMax")) {
/* 1585 */         errorObj = new ErrorObject(
/* 1586 */           SubmissionErrorCodes.ERROR_DATAFIELD175_DEPARTURE_DATE_SEGMENT2);
/* 1587 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/* 1590 */       errorObj = new ErrorObject(
/* 1591 */         SubmissionErrorCodes.ERROR_DATAFIELD175_DEPARTURE_DATE_SEGMENT2);
/* 1592 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateArrivalLocationCodeSegment2(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1611 */     ErrorObject errorObj = null;
/* 1612 */     if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean
/* 1613 */       .getArrivalLocationCodeSegment2()))
/*      */     {
/*      */ 
/* 1616 */       if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
/*      */       
/* 1618 */         (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter)) {
/* 1619 */         errorObj = new ErrorObject(
/* 1620 */           SubmissionErrorCodes.ERROR_DATAFIELD176_ARRIVALLOCATION_CODESEGMENT2);
/* 1621 */         errorCodes.add(errorObj);
/*      */       }
/*      */       
/*      */     }
/* 1625 */     else if (CommonValidator.validateData(airlineIndustryTAABean
/* 1626 */       .getArrivalLocationCodeSegment2(), 
/* 1627 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['¡']))
/*      */     {
/* 1629 */       String reqLength = 
/* 1630 */         CommonValidator.validateLength(airlineIndustryTAABean
/* 1631 */         .getArrivalLocationCodeSegment2(), 3, 3);
/*      */       
/* 1633 */       if (reqLength.equals("greaterThanMax")) {
/* 1634 */         errorObj = new ErrorObject(
/* 1635 */           SubmissionErrorCodes.ERROR_DATAFIELD177_ARRIVALLOCATION_CODESEGMENT2);
/* 1636 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/* 1639 */       errorObj = new ErrorObject(
/* 1640 */         SubmissionErrorCodes.ERROR_DATAFIELD177_ARRIVALLOCATION_CODESEGMENT2);
/* 1641 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateSegmentCarrierCode2(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1660 */     ErrorObject errorObj = null;
/* 1661 */     if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean
/* 1662 */       .getSegmentCarrierCode2()))
/*      */     {
/*      */ 
/* 1665 */       if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
/*      */       
/* 1667 */         (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter)) {
/* 1668 */         errorObj = new ErrorObject(
/* 1669 */           SubmissionErrorCodes.ERROR_DATAFIELD178_SEGMENTCARRIER_CODE2);
/* 1670 */         errorCodes.add(errorObj);
/*      */       }
/*      */       
/*      */     }
/* 1674 */     else if (CommonValidator.validateData(airlineIndustryTAABean
/* 1675 */       .getSegmentCarrierCode2(), 
/* 1676 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['¢']))
/*      */     {
/* 1678 */       String reqLength = CommonValidator.validateLength(
/* 1679 */         airlineIndustryTAABean.getSegmentCarrierCode2(), 2, 2);
/*      */       
/* 1681 */       if (reqLength.equals("greaterThanMax")) {
/* 1682 */         errorObj = new ErrorObject(
/* 1683 */           SubmissionErrorCodes.ERROR_DATAFIELD179_SEGMENTCARRIER_CODE2);
/* 1684 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/* 1687 */       errorObj = new ErrorObject(
/* 1688 */         SubmissionErrorCodes.ERROR_DATAFIELD179_SEGMENTCARRIER_CODE2);
/* 1689 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateSegment2FareBasis(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1708 */     ErrorObject errorObj = null;
/* 1709 */     if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean
/* 1710 */       .getSegment2FareBasis()))
/*      */     {
/*      */ 
/* 1713 */       if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
/*      */       
/* 1715 */         (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter)) {
/* 1716 */         errorObj = new ErrorObject(
/* 1717 */           SubmissionErrorCodes.ERROR_DATAFIELD180_SEGMENT2FARE_BASIS);
/* 1718 */         errorCodes.add(errorObj);
/*      */ 
/*      */       }
/*      */       
/*      */ 
/*      */     }
/*      */     else
/*      */     {
/*      */ 
/* 1727 */       String reqLength = CommonValidator.validateLength(
/* 1728 */         airlineIndustryTAABean.getSegment2FareBasis(), 15, 15);
/*      */       
/* 1730 */       if (reqLength.equals("greaterThanMax")) {
/* 1731 */         errorObj = new ErrorObject("AIR", 
/* 1732 */           "Segment2FareBasis length is out of Sequence");
/* 1733 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateClassOfServiceCodeSegment2(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1757 */     ErrorObject errorObj = null;
/* 1758 */     if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean
/* 1759 */       .getClassOfServiceCodeSegment2()))
/*      */     {
/*      */ 
/* 1762 */       if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
/*      */       
/* 1764 */         (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter)) {
/* 1765 */         errorObj = new ErrorObject("AIR", 
/* 1766 */           "ClassOfServiceCodeSegment2 is Missing");
/* 1767 */         errorCodes.add(errorObj);
/*      */       }
/*      */       
/*      */     }
/* 1771 */     else if (CommonValidator.validateData(airlineIndustryTAABean
/* 1772 */       .getClassOfServiceCodeSegment2(), 
/* 1773 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['¤']))
/*      */     {
/* 1775 */       String reqLength = CommonValidator.validateLength(
/* 1776 */         airlineIndustryTAABean.getClassOfServiceCodeSegment2(), 
/* 1777 */         2, 2);
/*      */       
/* 1779 */       if (reqLength.equals("greaterThanMax")) {
/* 1780 */         errorObj = new ErrorObject(
/* 1781 */           SubmissionErrorCodes.ERROR_DATAFIELD181_CLASSOFSERVICE_CODESEGMENT2);
/* 1782 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/* 1785 */       errorObj = new ErrorObject(
/* 1786 */         SubmissionErrorCodes.ERROR_DATAFIELD181_CLASSOFSERVICE_CODESEGMENT2);
/* 1787 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateFlightNumberSegment2(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1806 */     ErrorObject errorObj = null;
/* 1807 */     if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean
/* 1808 */       .getFlightNumberSegment2()))
/*      */     {
/*      */ 
/* 1811 */       if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
/*      */       
/* 1813 */         (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter)) {
/* 1814 */         errorObj = new ErrorObject(
/* 1815 */           SubmissionErrorCodes.ERROR_DATAFIELD182_FLIGHTNUMBER_SEGMENT2);
/* 1816 */         errorCodes.add(errorObj);
/*      */ 
/*      */       }
/*      */       
/*      */ 
/*      */     }
/*      */     else
/*      */     {
/*      */ 
/* 1825 */       String reqLength = CommonValidator.validateLength(
/* 1826 */         airlineIndustryTAABean.getFlightNumberSegment2(), 4, 4);
/*      */       
/* 1828 */       if (reqLength.equals("greaterThanMax")) {
/* 1829 */         errorObj = new ErrorObject("AIR", 
/* 1830 */           "FlightNumberSegment2 Length is Out of Sequence");
/* 1831 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateSegment2Fare(AirlineIndustryTAABean airlineIndustryTAABean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1855 */     ErrorObject errorObj = null;
/*      */     
/* 1857 */     if (!CommonValidator.isNullOrEmpty(airlineIndustryTAABean
/* 1858 */       .getSegment2Fare())) {
/* 1859 */       if (CommonValidator.validateData(airlineIndustryTAABean
/* 1860 */         .getSegment2Fare(), com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['§']))
/*      */       {
/* 1862 */         String reqLength = CommonValidator.validateLength(
/* 1863 */           airlineIndustryTAABean.getSegment2Fare(), 12, 12);
/*      */         
/* 1865 */         if (reqLength.equals("greaterThanMax")) {
/* 1866 */           errorObj = new ErrorObject(
/* 1867 */             SubmissionErrorCodes.ERROR_DATAFIELD184_SEGMENT_2FARE);
/* 1868 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/* 1871 */         errorObj = new ErrorObject(
/* 1872 */           SubmissionErrorCodes.ERROR_DATAFIELD183_SEGMENT_2FARE);
/* 1873 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateStopperIndicator3(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1891 */     ErrorObject errorObj = null;
/*      */     
/* 1893 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/* 1895 */       if (CommonValidator.validateData(value, 
/* 1896 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['¨']))
/*      */       {
/* 1898 */         String reqLength = CommonValidator.validateLength(value, 1, 1);
/*      */         
/* 1900 */         if ((reqLength.equals("greaterThanMax")) || 
/* 1901 */           (!CommonValidator.isValidStopOverIndicator(value)))
/*      */         {
/* 1903 */           errorObj = new ErrorObject(
/* 1904 */             SubmissionErrorCodes.ERROR_DATAFIELD185_STOPOVER3_INDICATOR);
/* 1905 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/* 1908 */         errorObj = new ErrorObject(
/* 1909 */           SubmissionErrorCodes.ERROR_DATAFIELD185_STOPOVER3_INDICATOR);
/* 1910 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateDepartureLocationCodeSegment3(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1929 */     ErrorObject errorObj = null;
/* 1930 */     if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean
/* 1931 */       .getDepartureLocationCodeSegment3()))
/*      */     {
/*      */ 
/* 1934 */       if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
/*      */       
/* 1936 */         (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter)) {
/* 1937 */         errorObj = new ErrorObject(
/* 1938 */           SubmissionErrorCodes.ERROR_DATAFIELD186_DEPARTURELOCATION_CODE_SEGMENT3);
/* 1939 */         errorCodes.add(errorObj);
/*      */       }
/*      */       
/*      */     }
/* 1943 */     else if (CommonValidator.validateData(airlineIndustryTAABean
/* 1944 */       .getDepartureLocationCodeSegment3(), 
/* 1945 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['©']))
/*      */     {
/* 1947 */       String reqLength = CommonValidator.validateLength(
/* 1948 */         airlineIndustryTAABean
/* 1949 */         .getDepartureLocationCodeSegment3(), 3, 3);
/*      */       
/* 1951 */       if (reqLength.equals("greaterThanMax")) {
/* 1952 */         errorObj = new ErrorObject(
/* 1953 */           SubmissionErrorCodes.ERROR_DATAFIELD187_DEPARTURELOCATION_CODE_SEGMENT3);
/* 1954 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/* 1957 */       errorObj = new ErrorObject(
/* 1958 */         SubmissionErrorCodes.ERROR_DATAFIELD187_DEPARTURELOCATION_CODE_SEGMENT3);
/* 1959 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateDepartureDateSegment3(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1978 */     ErrorObject errorObj = null;
/* 1979 */     if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean
/* 1980 */       .getDepartureDateSegment3()))
/*      */     {
/*      */ 
/* 1983 */       if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
/*      */       
/* 1985 */         (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter)) {
/* 1986 */         errorObj = new ErrorObject(
/* 1987 */           SubmissionErrorCodes.ERROR_DATAFIELD188_DEPARTURE_DATE_SEGMENT3);
/* 1988 */         errorCodes.add(errorObj);
/*      */       }
/*      */       
/*      */     }
/* 1992 */     else if (CommonValidator.validateData(airlineIndustryTAABean
/* 1993 */       .getDepartureDateSegment3(), 
/* 1994 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ª']))
/*      */     {
/* 1996 */       String reqLength = 
/* 1997 */         CommonValidator.validateLength(airlineIndustryTAABean
/* 1998 */         .getDepartureDateSegment3(), 8, 8);
/*      */       
/* 2000 */       if (reqLength.equals("greaterThanMax")) {
/* 2001 */         errorObj = new ErrorObject(
/* 2002 */           SubmissionErrorCodes.ERROR_DATAFIELD189_DEPARTURE_DATE_SEGMENT3);
/* 2003 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/* 2006 */       errorObj = new ErrorObject(
/* 2007 */         SubmissionErrorCodes.ERROR_DATAFIELD189_DEPARTURE_DATE_SEGMENT3);
/* 2008 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateArrivalLocationCodeSegment3(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 2027 */     ErrorObject errorObj = null;
/* 2028 */     if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean
/* 2029 */       .getArrivalLocationCodeSegment3()))
/*      */     {
/*      */ 
/* 2032 */       if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
/*      */       
/* 2034 */         (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter)) {
/* 2035 */         errorObj = new ErrorObject(
/* 2036 */           SubmissionErrorCodes.ERROR_DATAFIELD190_ARRIVALLOCATION_CODESEGMENT3);
/* 2037 */         errorCodes.add(errorObj);
/*      */       }
/*      */       
/*      */     }
/* 2041 */     else if (CommonValidator.validateData(airlineIndustryTAABean
/* 2042 */       .getArrivalLocationCodeSegment3(), 
/* 2043 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['«']))
/*      */     {
/* 2045 */       String reqLength = 
/* 2046 */         CommonValidator.validateLength(airlineIndustryTAABean
/* 2047 */         .getArrivalLocationCodeSegment3(), 3, 3);
/*      */       
/* 2049 */       if (reqLength.equals("greaterThanMax")) {
/* 2050 */         errorObj = new ErrorObject(
/* 2051 */           SubmissionErrorCodes.ERROR_DATAFIELD191_ARRIVALLOCATION_CODESEGMENT3);
/* 2052 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/* 2055 */       errorObj = new ErrorObject(
/* 2056 */         SubmissionErrorCodes.ERROR_DATAFIELD191_ARRIVALLOCATION_CODESEGMENT3);
/* 2057 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateSegmentCarrierCode3(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 2076 */     ErrorObject errorObj = null;
/* 2077 */     if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean
/* 2078 */       .getSegmentCarrierCode3()))
/*      */     {
/*      */ 
/* 2081 */       if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
/*      */       
/* 2083 */         (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter)) {
/* 2084 */         errorObj = new ErrorObject(
/* 2085 */           SubmissionErrorCodes.ERROR_DATAFIELD192_SEGMENTCARRIER_CODE3);
/* 2086 */         errorCodes.add(errorObj);
/*      */       }
/*      */       
/*      */     }
/* 2090 */     else if (CommonValidator.validateData(airlineIndustryTAABean
/* 2091 */       .getSegmentCarrierCode3(), 
/* 2092 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['¬']))
/*      */     {
/* 2094 */       String reqLength = CommonValidator.validateLength(
/* 2095 */         airlineIndustryTAABean.getSegmentCarrierCode3(), 2, 2);
/*      */       
/* 2097 */       if (reqLength.equals("greaterThanMax")) {
/* 2098 */         errorObj = new ErrorObject(
/* 2099 */           SubmissionErrorCodes.ERROR_DATAFIELD193_SEGMENTCARRIER_CODE3);
/* 2100 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/* 2103 */       errorObj = new ErrorObject(
/* 2104 */         SubmissionErrorCodes.ERROR_DATAFIELD193_SEGMENTCARRIER_CODE3);
/* 2105 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateSegment3FareBasis(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 2124 */     ErrorObject errorObj = null;
/* 2125 */     if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean
/* 2126 */       .getSegment3FareBasis()))
/*      */     {
/*      */ 
/* 2129 */       if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
/*      */       
/* 2131 */         (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter)) {
/* 2132 */         errorObj = new ErrorObject(
/* 2133 */           SubmissionErrorCodes.ERROR_DATAFIELD194_SEGMENT3FARE_BASIS);
/* 2134 */         errorCodes.add(errorObj);
/*      */ 
/*      */       }
/*      */       
/*      */ 
/*      */     }
/*      */     else
/*      */     {
/*      */ 
/* 2143 */       String reqLength = CommonValidator.validateLength(
/* 2144 */         airlineIndustryTAABean.getSegment3FareBasis(), 15, 15);
/*      */       
/* 2146 */       if (reqLength.equals("greaterThanMax")) {
/* 2147 */         errorObj = new ErrorObject("AIR", 
/* 2148 */           "Segment3FareBasis length is out of Sequence");
/* 2149 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateClassOfServiceCodeSegment3(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 2173 */     ErrorObject errorObj = null;
/* 2174 */     if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean
/* 2175 */       .getClassOfServiceCodeSegment3()))
/*      */     {
/*      */ 
/* 2178 */       if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
/*      */       
/* 2180 */         (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter)) {
/* 2181 */         errorObj = new ErrorObject("AIR", 
/* 2182 */           "ClassOfServiceCodeSegment3 is Missing");
/* 2183 */         errorCodes.add(errorObj);
/*      */       }
/*      */       
/*      */     }
/* 2187 */     else if (CommonValidator.validateData(airlineIndustryTAABean
/* 2188 */       .getClassOfServiceCodeSegment3(), 
/* 2189 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['®']))
/*      */     {
/* 2191 */       String reqLength = CommonValidator.validateLength(
/* 2192 */         airlineIndustryTAABean.getClassOfServiceCodeSegment3(), 
/* 2193 */         2, 2);
/*      */       
/* 2195 */       if (reqLength.equals("greaterThanMax")) {
/* 2196 */         errorObj = new ErrorObject(
/* 2197 */           SubmissionErrorCodes.ERROR_DATAFIELD195_CLASSOFSERVICE_CODESEGMENT3);
/* 2198 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/* 2201 */       errorObj = new ErrorObject(
/* 2202 */         SubmissionErrorCodes.ERROR_DATAFIELD195_CLASSOFSERVICE_CODESEGMENT3);
/* 2203 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateFlightNumberSegment3(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 2222 */     ErrorObject errorObj = null;
/* 2223 */     if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean
/* 2224 */       .getFlightNumberSegment3()))
/*      */     {
/*      */ 
/* 2227 */       if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
/*      */       
/* 2229 */         (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter)) {
/* 2230 */         errorObj = new ErrorObject(
/* 2231 */           SubmissionErrorCodes.ERROR_DATAFIELD196_FLIGHTNUMBER_SEGMENT3);
/* 2232 */         errorCodes.add(errorObj);
/*      */ 
/*      */       }
/*      */       
/*      */ 
/*      */     }
/*      */     else
/*      */     {
/*      */ 
/* 2241 */       String reqLength = CommonValidator.validateLength(
/* 2242 */         airlineIndustryTAABean.getFlightNumberSegment3(), 4, 4);
/*      */       
/* 2244 */       if (reqLength.equals("greaterThanMax")) {
/* 2245 */         errorObj = new ErrorObject("AIR", 
/* 2246 */           "FlightNumberSegment3 Length is Out of Sequence");
/* 2247 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateSegment3Fare(AirlineIndustryTAABean airlineIndustryTAABean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 2271 */     ErrorObject errorObj = null;
/*      */     
/* 2273 */     if (!CommonValidator.isNullOrEmpty(airlineIndustryTAABean
/* 2274 */       .getSegment3Fare())) {
/* 2275 */       if (CommonValidator.validateData(airlineIndustryTAABean
/* 2276 */         .getSegment3Fare(), com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['±']))
/*      */       {
/* 2278 */         String reqLength = CommonValidator.validateLength(
/* 2279 */           airlineIndustryTAABean.getSegment3Fare(), 12, 12);
/*      */         
/* 2281 */         if (reqLength.equals("greaterThanMax")) {
/* 2282 */           errorObj = new ErrorObject(
/* 2283 */             SubmissionErrorCodes.ERROR_DATAFIELD198_SEGMENT_3FARE);
/* 2284 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/* 2287 */         errorObj = new ErrorObject(
/* 2288 */           SubmissionErrorCodes.ERROR_DATAFIELD197_SEGMENT_3FARE);
/* 2289 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateStopperIndicator4(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 2306 */     ErrorObject errorObj = null;
/*      */     
/* 2308 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/* 2310 */       if (CommonValidator.validateData(value, 
/* 2311 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['²']))
/*      */       {
/* 2313 */         String reqLength = CommonValidator.validateLength(value, 1, 1);
/*      */         
/* 2315 */         if ((reqLength.equals("greaterThanMax")) || 
/* 2316 */           (!CommonValidator.isValidStopOverIndicator(value)))
/*      */         {
/* 2318 */           errorObj = new ErrorObject(
/* 2319 */             SubmissionErrorCodes.ERROR_DATAFIELD199_STOPOVER4_INDICATOR);
/* 2320 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/* 2323 */         errorObj = new ErrorObject(
/* 2324 */           SubmissionErrorCodes.ERROR_DATAFIELD199_STOPOVER4_INDICATOR);
/* 2325 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateDepartureLocationCodeSegment4(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 2344 */     ErrorObject errorObj = null;
/* 2345 */     if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean
/* 2346 */       .getDepartureLocationCodeSegment4()))
/*      */     {
/*      */ 
/* 2349 */       if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
/*      */       
/* 2351 */         (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter)) {
/* 2352 */         errorObj = new ErrorObject(
/* 2353 */           SubmissionErrorCodes.ERROR_DATAFIELD200_DEPARTURELOCATION_CODE_SEGMENT4);
/* 2354 */         errorCodes.add(errorObj);
/*      */       }
/*      */       
/*      */     }
/* 2358 */     else if (CommonValidator.validateData(airlineIndustryTAABean
/* 2359 */       .getDepartureLocationCodeSegment4(), 
/* 2360 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['³']))
/*      */     {
/* 2362 */       String reqLength = CommonValidator.validateLength(
/* 2363 */         airlineIndustryTAABean
/* 2364 */         .getDepartureLocationCodeSegment4(), 3, 3);
/*      */       
/* 2366 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/* 2368 */         errorObj = new ErrorObject(
/* 2369 */           SubmissionErrorCodes.ERROR_DATAFIELD201_DEPARTURELOCATION_CODE_SEGMENT4);
/* 2370 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */     else {
/* 2374 */       errorObj = new ErrorObject(
/* 2375 */         SubmissionErrorCodes.ERROR_DATAFIELD201_DEPARTURELOCATION_CODE_SEGMENT4);
/* 2376 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateDepartureDateSegment4(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 2395 */     ErrorObject errorObj = null;
/* 2396 */     if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean
/* 2397 */       .getDepartureDateSegment4()))
/*      */     {
/*      */ 
/* 2400 */       if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
/*      */       
/* 2402 */         (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter)) {
/* 2403 */         errorObj = new ErrorObject(
/* 2404 */           SubmissionErrorCodes.ERROR_DATAFIELD202_DEPARTURE_DATE_SEGMENT4);
/* 2405 */         errorCodes.add(errorObj);
/*      */       }
/*      */       
/*      */     }
/* 2409 */     else if (CommonValidator.validateData(airlineIndustryTAABean
/* 2410 */       .getDepartureDateSegment4(), 
/* 2411 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['´']))
/*      */     {
/* 2413 */       String reqLength = 
/* 2414 */         CommonValidator.validateLength(airlineIndustryTAABean
/* 2415 */         .getDepartureDateSegment4(), 8, 8);
/*      */       
/* 2417 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/* 2419 */         errorObj = new ErrorObject(
/* 2420 */           SubmissionErrorCodes.ERROR_DATAFIELD203_DEPARTURE_DATE_SEGMENT4);
/* 2421 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */     else {
/* 2425 */       errorObj = new ErrorObject(
/* 2426 */         SubmissionErrorCodes.ERROR_DATAFIELD203_DEPARTURE_DATE_SEGMENT4);
/* 2427 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateArrivalLocationCodeSegment4(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 2446 */     ErrorObject errorObj = null;
/* 2447 */     if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean
/* 2448 */       .getArrivalLocationCodeSegment4()))
/*      */     {
/*      */ 
/* 2451 */       if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
/*      */       
/* 2453 */         (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter)) {
/* 2454 */         errorObj = new ErrorObject(
/* 2455 */           SubmissionErrorCodes.ERROR_DATAFIELD204_ARRIVALLOCATION_CODESEGMENT4);
/* 2456 */         errorCodes.add(errorObj);
/*      */       }
/*      */       
/*      */     }
/* 2460 */     else if (CommonValidator.validateData(airlineIndustryTAABean
/* 2461 */       .getArrivalLocationCodeSegment4(), 
/* 2462 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['µ']))
/*      */     {
/* 2464 */       String reqLength = 
/* 2465 */         CommonValidator.validateLength(airlineIndustryTAABean
/* 2466 */         .getArrivalLocationCodeSegment4(), 3, 3);
/*      */       
/* 2468 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/* 2470 */         errorObj = new ErrorObject(
/* 2471 */           SubmissionErrorCodes.ERROR_DATAFIELD205_ARRIVALLOCATION_CODESEGMENT4);
/* 2472 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */     else {
/* 2476 */       errorObj = new ErrorObject(
/* 2477 */         SubmissionErrorCodes.ERROR_DATAFIELD205_ARRIVALLOCATION_CODESEGMENT4);
/* 2478 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateSegmentCarrierCode4(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 2497 */     ErrorObject errorObj = null;
/* 2498 */     if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean
/* 2499 */       .getSegmentCarrierCode4()))
/*      */     {
/*      */ 
/* 2502 */       if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
/*      */       
/* 2504 */         (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter)) {
/* 2505 */         errorObj = new ErrorObject(
/* 2506 */           SubmissionErrorCodes.ERROR_DATAFIELD206_SEGMENTCARRIER_CODE4);
/* 2507 */         errorCodes.add(errorObj);
/*      */       }
/*      */       
/*      */     }
/* 2511 */     else if (CommonValidator.validateData(airlineIndustryTAABean
/* 2512 */       .getSegmentCarrierCode4(), 
/* 2513 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['¶']))
/*      */     {
/* 2515 */       String reqLength = CommonValidator.validateLength(
/* 2516 */         airlineIndustryTAABean.getSegmentCarrierCode4(), 2, 2);
/*      */       
/* 2518 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/* 2520 */         errorObj = new ErrorObject(
/* 2521 */           SubmissionErrorCodes.ERROR_DATAFIELD207_SEGMENTCARRIER_CODE4);
/* 2522 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */     else {
/* 2526 */       errorObj = new ErrorObject(
/* 2527 */         SubmissionErrorCodes.ERROR_DATAFIELD207_SEGMENTCARRIER_CODE4);
/* 2528 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateSegment4FareBasis(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 2547 */     ErrorObject errorObj = null;
/* 2548 */     if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean
/* 2549 */       .getSegment4FareBasis()))
/*      */     {
/*      */ 
/* 2552 */       if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
/*      */       
/* 2554 */         (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter)) {
/* 2555 */         errorObj = new ErrorObject(
/* 2556 */           SubmissionErrorCodes.ERROR_DATAFIELD208_SEGMENT4FARE_BASIS);
/* 2557 */         errorCodes.add(errorObj);
/*      */ 
/*      */       }
/*      */       
/*      */ 
/*      */     }
/*      */     else
/*      */     {
/*      */ 
/* 2566 */       String reqLength = CommonValidator.validateLength(
/* 2567 */         airlineIndustryTAABean.getSegment4FareBasis(), 15, 15);
/*      */       
/* 2569 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/* 2571 */         errorObj = new ErrorObject("AIR", 
/* 2572 */           "Segment4FareBasis length is out of Sequence");
/* 2573 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateClassOfServiceCodeSegment4(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 2598 */     ErrorObject errorObj = null;
/* 2599 */     if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean
/* 2600 */       .getClassOfServiceCodeSegment4()))
/*      */     {
/*      */ 
/* 2603 */       if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
/*      */       
/* 2605 */         (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter)) {
/* 2606 */         errorObj = new ErrorObject("AIR", 
/* 2607 */           "ClassOfServiceCodeSegment4 is Missing");
/* 2608 */         errorCodes.add(errorObj);
/*      */       }
/*      */       
/*      */     }
/* 2612 */     else if (CommonValidator.validateData(airlineIndustryTAABean
/* 2613 */       .getClassOfServiceCodeSegment4(), 
/* 2614 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['¸']))
/*      */     {
/* 2616 */       String reqLength = CommonValidator.validateLength(
/* 2617 */         airlineIndustryTAABean.getClassOfServiceCodeSegment4(), 
/* 2618 */         2, 2);
/*      */       
/* 2620 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/* 2622 */         errorObj = new ErrorObject(
/* 2623 */           SubmissionErrorCodes.ERROR_DATAFIELD209_CLASSOFSERVICE_CODESEGMENT4);
/* 2624 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */     else {
/* 2628 */       errorObj = new ErrorObject(
/* 2629 */         SubmissionErrorCodes.ERROR_DATAFIELD209_CLASSOFSERVICE_CODESEGMENT4);
/* 2630 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateFlightNumberSegment4(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 2649 */     ErrorObject errorObj = null;
/* 2650 */     if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean
/* 2651 */       .getFlightNumberSegment4()))
/*      */     {
/*      */ 
/* 2654 */       if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
/*      */       
/* 2656 */         (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter)) {
/* 2657 */         errorObj = new ErrorObject(
/* 2658 */           SubmissionErrorCodes.ERROR_DATAFIELD210_FLIGHTNUMBER_SEGMENT4);
/* 2659 */         errorCodes.add(errorObj);
/*      */ 
/*      */       }
/*      */       
/*      */ 
/*      */     }
/*      */     else
/*      */     {
/*      */ 
/* 2668 */       String reqLength = CommonValidator.validateLength(
/* 2669 */         airlineIndustryTAABean.getFlightNumberSegment4(), 4, 4);
/*      */       
/* 2671 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/* 2673 */         errorObj = new ErrorObject("AIR", 
/* 2674 */           "FlightNumberSegment4 Length is Out of Sequence");
/* 2675 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateSegment4Fare(AirlineIndustryTAABean airlineIndustryTAABean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 2700 */     ErrorObject errorObj = null;
/*      */     
/* 2702 */     if (!CommonValidator.isNullOrEmpty(airlineIndustryTAABean
/* 2703 */       .getSegment4Fare())) {
/* 2704 */       if (CommonValidator.validateData(airlineIndustryTAABean
/* 2705 */         .getSegment4Fare(), com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['»']))
/*      */       {
/* 2707 */         String reqLength = CommonValidator.validateLength(
/* 2708 */           airlineIndustryTAABean.getSegment4Fare(), 12, 12);
/*      */         
/* 2710 */         if (reqLength.equals("greaterThanMax"))
/*      */         {
/* 2712 */           errorObj = new ErrorObject(
/* 2713 */             SubmissionErrorCodes.ERROR_DATAFIELD212_SEGMENT_4FARE);
/* 2714 */           errorCodes.add(errorObj);
/*      */         }
/*      */       }
/*      */       else {
/* 2718 */         errorObj = new ErrorObject(
/* 2719 */           SubmissionErrorCodes.ERROR_DATAFIELD211_SEGMENT_4FARE);
/* 2720 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateStopperIndicator5(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 2739 */     ErrorObject errorObj = null;
/*      */     
/* 2741 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/* 2743 */       if (CommonValidator.validateData(value, 
/* 2744 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['²']))
/*      */       {
/* 2746 */         String reqLength = CommonValidator.validateLength(value, 1, 1);
/*      */         
/* 2748 */         if ((reqLength.equals("greaterThanMax")) || 
/* 2749 */           (!CommonValidator.isValidStopOverIndicator(value)))
/*      */         {
/* 2751 */           errorObj = new ErrorObject(
/* 2752 */             SubmissionErrorCodes.ERROR_DATAFIELD199_STOPOVER5_INDICATOR);
/* 2753 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/* 2756 */         errorObj = new ErrorObject(
/* 2757 */           SubmissionErrorCodes.ERROR_DATAFIELD199_STOPOVER5_INDICATOR);
/* 2758 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateOriginalTicketNumber(AirlineIndustryTAABean airlineIndustryTAABean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 2777 */     ErrorObject errorObj = null;
/*      */     
/* 2779 */     if ((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC")) && 
/* 2780 */       (CommonValidator.isNullOrEmpty(airlineIndustryTAABean
/* 2781 */       .getExchangedOrOriginalTicketNumber())))
/*      */     {
/* 2783 */       errorObj = new ErrorObject("AIR", "OriginalTicketNumber is Missing");
/* 2784 */       errorCodes.add(errorObj);
/*      */     }
/* 2786 */     else if (!CommonValidator.isNullOrEmpty(airlineIndustryTAABean
/* 2787 */       .getExchangedOrOriginalTicketNumber()))
/*      */     {
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 2793 */       String reqLength = CommonValidator.validateLength(
/* 2794 */         airlineIndustryTAABean
/* 2795 */         .getExchangedOrOriginalTicketNumber(), 14, 14);
/*      */       
/* 2797 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/* 2799 */         errorObj = new ErrorObject("AIR", 
/* 2800 */           "OriginalTicketNumber Length exceeded");
/* 2801 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\validator\industry\AirLineRecordValidator.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */