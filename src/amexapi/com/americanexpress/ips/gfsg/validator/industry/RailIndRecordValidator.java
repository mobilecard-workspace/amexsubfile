/*     */ package com.americanexpress.ips.gfsg.validator.industry;
/*     */ 
/*     */ import com.americanexpress.ips.gfsg.beans.ErrorObject;
/*     */ import com.americanexpress.ips.gfsg.beans.common.TransactionAdviceAddendumBean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.TransactionAdviceBasicBean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.RailIndustryTAABean;
/*     */ import com.americanexpress.ips.gfsg.enumerations.SubmissionErrorCodes;
/*     */ import com.americanexpress.ips.gfsg.exceptions.SettlementException;
/*     */ import com.americanexpress.ips.gfsg.validator.CommonValidator;
/*     */ import java.util.List;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class RailIndRecordValidator
/*     */ {
/*     */   public static void validateRailIndRecord(TransactionAdviceBasicBean transactionAdviceBasicType, TransactionAdviceAddendumBean transactionAddendumType, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/*  37 */     RailIndustryTAABean railIndustryTAABean = (RailIndustryTAABean)transactionAddendumType;
/*     */     
/*     */ 
/*  40 */     TAADBRecordValidator.validateRecordType(railIndustryTAABean
/*  41 */       .getRecordType(), errorCodes);
/*  42 */     TAADBRecordValidator.validateRecordNumber(railIndustryTAABean
/*  43 */       .getRecordNumber(), errorCodes);
/*  44 */     TAADBRecordValidator.validateTransactionIdentifier(railIndustryTAABean
/*  45 */       .getTransactionIdentifier(), transactionAdviceBasicType, 
/*  46 */       errorCodes);
/*  47 */     TAADBRecordValidator.validateAddendaTypeCode(railIndustryTAABean
/*  48 */       .getAddendaTypeCode(), errorCodes);
/*  49 */     TAADBRecordValidator.validateFormatCode(railIndustryTAABean
/*  50 */       .getFormatCode(), transactionAdviceBasicType, errorCodes);
/*  51 */     validateTransactionType(railIndustryTAABean.getTransactionType(), 
/*  52 */       errorCodes);
/*  53 */     validateTicketNumber(railIndustryTAABean.getTicketNumber(), errorCodes);
/*  54 */     validatePassengerName(railIndustryTAABean.getPassengerName(), 
/*  55 */       errorCodes);
/*  56 */     validateIataCarrierCode(railIndustryTAABean.getIataCarrierCode(), 
/*  57 */       errorCodes);
/*  58 */     validateTicketIssuerName(railIndustryTAABean.getTicketIssuerName(), 
/*  59 */       errorCodes);
/*  60 */     validateTicketIssueCity(railIndustryTAABean.getTicketIssueCity(), 
/*  61 */       errorCodes);
/*  62 */     validateDepartureStation1(railIndustryTAABean.getDepartureStation1(), 
/*  63 */       errorCodes);
/*  64 */     validateDepartureDate1(railIndustryTAABean.getDepartureDate1(), 
/*  65 */       errorCodes);
/*  66 */     validateArrivalStation1(railIndustryTAABean.getArrivalStation1(), 
/*  67 */       errorCodes);
/*  68 */     validateDepartureStation2(railIndustryTAABean.getDepartureStation2(), 
/*  69 */       errorCodes);
/*  70 */     validateDepartureDate2(railIndustryTAABean.getDepartureDate2(), 
/*  71 */       errorCodes);
/*  72 */     validateArrivalStation2(railIndustryTAABean.getArrivalStation2(), 
/*  73 */       errorCodes);
/*  74 */     validateDepartureStation3(railIndustryTAABean.getDepartureStation3(), 
/*  75 */       errorCodes);
/*  76 */     validateDepartureDate3(railIndustryTAABean.getDepartureDate3(), 
/*  77 */       errorCodes);
/*  78 */     validateArrivalStation3(railIndustryTAABean.getArrivalStation3(), 
/*  79 */       errorCodes);
/*  80 */     validateDepartureStation4(railIndustryTAABean.getDepartureStation4(), 
/*  81 */       errorCodes);
/*  82 */     validateDepartureDate4(railIndustryTAABean.getDepartureDate4(), 
/*  83 */       errorCodes);
/*  84 */     validateArrivalStation4(railIndustryTAABean.getArrivalStation4(), 
/*  85 */       errorCodes);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateTransactionType(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 101 */     ErrorObject errorObj = null;
/*     */     
/* 103 */     if (CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 105 */       errorObj = new ErrorObject(
/* 106 */         SubmissionErrorCodes.ERROR_DATAFIELD213_TRANSACTION_TYPE);
/* 107 */       errorCodes.add(errorObj);
/*     */     }
/* 109 */     else if (CommonValidator.validateData(value, 
/* 110 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ã']))
/*     */     {
/* 112 */       String reqLength = CommonValidator.validateLength(value, 3, 3);
/*     */       
/* 114 */       if (reqLength.equals("greaterThanMax")) {
/* 115 */         errorObj = new ErrorObject(
/* 116 */           SubmissionErrorCodes.ERROR_DATAFIELD213_TRANSACTION_TYPE);
/* 117 */         errorCodes.add(errorObj);
/*     */ 
/*     */       }
/* 120 */       else if (!value.equalsIgnoreCase("TKT"))
/*     */       {
/* 122 */         if (!value.equalsIgnoreCase("REF"))
/*     */         {
/* 124 */           if (!value.equalsIgnoreCase("EXC"))
/*     */           {
/* 126 */             if (!value.equalsIgnoreCase("MSC")) {
/* 127 */               errorObj = new ErrorObject(
/* 128 */                 SubmissionErrorCodes.ERROR_DATAFIELD213_TRANSACTION_TYPE);
/* 129 */               errorCodes.add(errorObj);
/*     */             } } }
/*     */       }
/*     */     } else {
/* 133 */       errorObj = new ErrorObject(
/* 134 */         SubmissionErrorCodes.ERROR_DATAFIELD213_TRANSACTION_TYPE);
/* 135 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateTicketNumber(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 152 */     ErrorObject errorObj = null;
/*     */     
/* 154 */     if (CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 156 */       errorObj = new ErrorObject(
/* 157 */         SubmissionErrorCodes.ERROR_DATAFIELD214_TICKET_NUMBER);
/* 158 */       errorCodes.add(errorObj);
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/* 164 */       String reqLength = 
/* 165 */         CommonValidator.validateLength(value, 14, 14);
/*     */       
/* 167 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/* 169 */         errorObj = new ErrorObject("RAIL", 
/* 170 */           "TicketNumber Length is out of Sequence");
/* 171 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validatePassengerName(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 193 */     ErrorObject errorObj = null;
/*     */     
/* 195 */     if (CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 197 */       errorObj = new ErrorObject(
/* 198 */         SubmissionErrorCodes.ERROR_DATAFIELD215_PASSENGER_NAME);
/* 199 */       errorCodes.add(errorObj);
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/* 205 */       String reqLength = 
/* 206 */         CommonValidator.validateLength(value, 25, 25);
/*     */       
/* 208 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/* 210 */         errorObj = new ErrorObject("RAIL", 
/* 211 */           "PassengerName Length is out of Sequence");
/* 212 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateIataCarrierCode(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 234 */     ErrorObject errorObj = null;
/*     */     
/* 236 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 238 */       if (CommonValidator.validateData(value, 
/* 239 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Æ']))
/*     */       {
/* 241 */         String reqLength = CommonValidator.validateLength(value, 3, 3);
/*     */         
/* 243 */         if (reqLength.equals("greaterThanMax"))
/*     */         {
/* 245 */           errorObj = new ErrorObject("RAIL", 
/* 246 */             "ItaCarrierCode Length is out of Sequence");
/* 247 */           errorCodes.add(errorObj);
/*     */         }
/*     */       } else {
/* 250 */         errorObj = new ErrorObject("RAIL", 
/* 251 */           "ItaCarrierCode Type is Missing");
/* 252 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateTicketIssuerName(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 269 */     ErrorObject errorObj = null;
/*     */     
/* 271 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 277 */       String reqLength = 
/* 278 */         CommonValidator.validateLength(value, 32, 32);
/*     */       
/* 280 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/* 282 */         errorObj = new ErrorObject("RAIL", 
/* 283 */           "TicketIssuerName Length is out of Sequence");
/* 284 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateTicketIssueCity(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 306 */     ErrorObject errorObj = null;
/*     */     
/* 308 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/* 313 */       String reqLength = 
/* 314 */         CommonValidator.validateLength(value, 18, 18);
/*     */       
/* 316 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/* 318 */         errorObj = new ErrorObject("RAIL", 
/* 319 */           "TicketIssueCity Length is out of Sequence");
/* 320 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateDepartureStation1(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 342 */     ErrorObject errorObj = null;
/*     */     
/* 344 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/* 349 */       String reqLength = CommonValidator.validateLength(value, 3, 3);
/*     */       
/* 351 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/* 353 */         errorObj = new ErrorObject("RAIL", 
/* 354 */           "DepartureStation1 Length is out of Sequence");
/* 355 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateDepartureDate1(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 377 */     ErrorObject errorObj = null;
/*     */     
/* 379 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 381 */       if (CommonValidator.validateData(value, 
/* 382 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ê']))
/*     */       {
/* 384 */         String reqLength = CommonValidator.validateLength(value, 8, 8);
/*     */         
/* 386 */         if ((!reqLength.equals("equal")) || 
/* 387 */           (!CommonValidator.isValidDate(value, 
/* 388 */           "CCYYMMDD")))
/*     */         {
/* 390 */           errorObj = new ErrorObject(
/* 391 */             SubmissionErrorCodes.ERROR_DATAFIELD216_DEPARTURE_DATE1);
/* 392 */           errorCodes.add(errorObj);
/*     */         }
/*     */       } else {
/* 395 */         errorObj = new ErrorObject(
/* 396 */           SubmissionErrorCodes.ERROR_DATAFIELD216_DEPARTURE_DATE1);
/* 397 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateArrivalStation1(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 414 */     ErrorObject errorObj = null;
/*     */     
/* 416 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/* 421 */       String reqLength = CommonValidator.validateLength(value, 8, 8);
/*     */       
/* 423 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/* 425 */         errorObj = new ErrorObject("RAIL", 
/* 426 */           "validateArrivalStation1 Length is out of Sequence");
/* 427 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateDepartureStation2(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 449 */     ErrorObject errorObj = null;
/*     */     
/* 451 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/* 456 */       String reqLength = CommonValidator.validateLength(value, 3, 3);
/*     */       
/* 458 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/* 460 */         errorObj = new ErrorObject("RAIL", 
/* 461 */           "DepartureStation2 Length is out of Sequence");
/* 462 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateDepartureDate2(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 484 */     ErrorObject errorObj = null;
/*     */     
/* 486 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 488 */       if (CommonValidator.validateData(value, 
/* 489 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Í']))
/*     */       {
/* 491 */         String reqLength = CommonValidator.validateLength(value, 8, 8);
/*     */         
/* 493 */         if ((!reqLength.equals("equal")) || 
/* 494 */           (!CommonValidator.isValidDate(value, 
/* 495 */           "CCYYMMDD")))
/*     */         {
/* 497 */           errorObj = new ErrorObject(
/* 498 */             SubmissionErrorCodes.ERROR_DATAFIELD217_DEPARTURE_DATE2);
/* 499 */           errorCodes.add(errorObj);
/*     */         }
/*     */       } else {
/* 502 */         errorObj = new ErrorObject(
/* 503 */           SubmissionErrorCodes.ERROR_DATAFIELD217_DEPARTURE_DATE2);
/* 504 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateArrivalStation2(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 521 */     ErrorObject errorObj = null;
/*     */     
/* 523 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/* 528 */       String reqLength = CommonValidator.validateLength(value, 8, 8);
/*     */       
/* 530 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/* 532 */         errorObj = new ErrorObject("RAIL", 
/* 533 */           "validateArrivalStation2 Length is out of Sequence");
/* 534 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateDepartureStation3(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 556 */     ErrorObject errorObj = null;
/*     */     
/* 558 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/* 563 */       String reqLength = CommonValidator.validateLength(value, 3, 3);
/*     */       
/* 565 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/* 567 */         errorObj = new ErrorObject("RAIL", 
/* 568 */           "DepartureStation3 Length is out of Sequence");
/* 569 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateDepartureDate3(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 591 */     ErrorObject errorObj = null;
/*     */     
/* 593 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 595 */       if (CommonValidator.validateData(value, 
/* 596 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ð']))
/*     */       {
/* 598 */         String reqLength = CommonValidator.validateLength(value, 8, 8);
/*     */         
/* 600 */         if ((!reqLength.equals("equal")) || 
/* 601 */           (!CommonValidator.isValidDate(value, 
/* 602 */           "CCYYMMDD")))
/*     */         {
/* 604 */           errorObj = new ErrorObject(
/* 605 */             SubmissionErrorCodes.ERROR_DATAFIELD218_DEPARTURE_DATE3);
/* 606 */           errorCodes.add(errorObj);
/*     */         }
/*     */       } else {
/* 609 */         errorObj = new ErrorObject(
/* 610 */           SubmissionErrorCodes.ERROR_DATAFIELD218_DEPARTURE_DATE3);
/* 611 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateArrivalStation3(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 628 */     ErrorObject errorObj = null;
/*     */     
/* 630 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/* 635 */       String reqLength = CommonValidator.validateLength(value, 8, 8);
/*     */       
/* 637 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/* 639 */         errorObj = new ErrorObject("RAIL", 
/* 640 */           "validateArrivalStation3 Length is out of Sequence");
/* 641 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateDepartureStation4(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 663 */     ErrorObject errorObj = null;
/*     */     
/* 665 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 671 */       String reqLength = CommonValidator.validateLength(value, 3, 3);
/*     */       
/* 673 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/* 675 */         errorObj = new ErrorObject("RAIL", 
/* 676 */           "DepartureStation4 Length is out of Sequence");
/* 677 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateDepartureDate4(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 699 */     ErrorObject errorObj = null;
/*     */     
/* 701 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 703 */       if (CommonValidator.validateData(value, 
/* 704 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ó']))
/*     */       {
/* 706 */         String reqLength = CommonValidator.validateLength(value, 8, 8);
/*     */         
/* 708 */         if ((!reqLength.equals("equal")) || 
/* 709 */           (!CommonValidator.isValidDate(value, 
/* 710 */           "CCYYMMDD")))
/*     */         {
/* 712 */           errorObj = new ErrorObject(
/* 713 */             SubmissionErrorCodes.ERROR_DATAFIELD219_DEPARTURE_DATE4);
/* 714 */           errorCodes.add(errorObj);
/*     */         }
/*     */       } else {
/* 717 */         errorObj = new ErrorObject(
/* 718 */           SubmissionErrorCodes.ERROR_DATAFIELD219_DEPARTURE_DATE4);
/* 719 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateArrivalStation4(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 736 */     ErrorObject errorObj = null;
/*     */     
/* 738 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 744 */       String reqLength = CommonValidator.validateLength(value, 8, 8);
/*     */       
/* 746 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/* 748 */         errorObj = new ErrorObject("RAIL", 
/* 749 */           "validateArrivalStation4 Length is out of Sequence");
/* 750 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\validator\industry\RailIndRecordValidator.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */