/*     */ package com.americanexpress.ips.gfsg.validator.industry;
/*     */ 
/*     */ import com.americanexpress.ips.gfsg.beans.ErrorObject;
/*     */ import com.americanexpress.ips.gfsg.beans.common.TransactionAdviceAddendumBean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.TransactionAdviceBasicBean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.InsuranceIndustryTAABean;
/*     */ import com.americanexpress.ips.gfsg.enumerations.SubmissionErrorCodes;
/*     */ import com.americanexpress.ips.gfsg.exceptions.SettlementException;
/*     */ import com.americanexpress.ips.gfsg.validator.CommonValidator;
/*     */ import java.util.List;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class InsuranceRecordValidator
/*     */ {
/*     */   public static void validateInsuranceRecord(TransactionAdviceBasicBean transactionAdviceBasicType, TransactionAdviceAddendumBean transactionAddendumType, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/*  37 */     InsuranceIndustryTAABean insuranceIndustryTAABean = (InsuranceIndustryTAABean)transactionAddendumType;
/*     */     
/*     */ 
/*  40 */     TAADBRecordValidator.validateRecordType(insuranceIndustryTAABean
/*  41 */       .getRecordType(), errorCodes);
/*  42 */     TAADBRecordValidator.validateRecordNumber(insuranceIndustryTAABean
/*  43 */       .getRecordNumber(), errorCodes);
/*  44 */     TAADBRecordValidator.validateTransactionIdentifier(
/*  45 */       insuranceIndustryTAABean.getTransactionIdentifier(), 
/*  46 */       transactionAdviceBasicType, errorCodes);
/*  47 */     TAADBRecordValidator.validateAddendaTypeCode(insuranceIndustryTAABean
/*  48 */       .getAddendaTypeCode(), errorCodes);
/*  49 */     TAADBRecordValidator.validateFormatCode(insuranceIndustryTAABean
/*  50 */       .getFormatCode(), transactionAdviceBasicType, errorCodes);
/*     */     
/*  52 */     validateInsurancePolicyNumber(insuranceIndustryTAABean
/*  53 */       .getInsurancePolicyNumber(), errorCodes);
/*  54 */     validateInsuranceCoverageStartDate(insuranceIndustryTAABean
/*  55 */       .getInsuranceCoverageStartDate(), errorCodes);
/*  56 */     validateInsuranceCoverageEndDate(insuranceIndustryTAABean, errorCodes);
/*  57 */     validateInsurancePolicyPremiumFrequency(insuranceIndustryTAABean
/*  58 */       .getInsurancePolicyPremiumFrequency(), errorCodes);
/*  59 */     validateAdditionalInsurancePolicyNumber(insuranceIndustryTAABean
/*  60 */       .getAdditionalInsurancePolicyNumber(), errorCodes);
/*  61 */     validateTypeOfPolicy(insuranceIndustryTAABean.getTypeOfPolicy(), 
/*  62 */       errorCodes);
/*  63 */     validateNameOfInsured(insuranceIndustryTAABean.getNameOfInsured(), 
/*  64 */       errorCodes);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateInsurancePolicyNumber(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/*  80 */     ErrorObject errorObj = null;
/*     */     
/*  82 */     if (CommonValidator.isNullOrEmpty(value))
/*     */     {
/*     */ 
/*  85 */       errorObj = new ErrorObject(
/*  86 */         SubmissionErrorCodes.ERROR_DATAFIELD387_INSURANCE_POLICYNUMBER);
/*  87 */       errorCodes.add(errorObj);
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/*  94 */       String reqLength = 
/*  95 */         CommonValidator.validateLength(value, 23, 23);
/*     */       
/*  97 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/*  99 */         errorObj = new ErrorObject(
/* 100 */           SubmissionErrorCodes.ERROR_DATAFIELD388_INSURANCE_POLICYNUMBER);
/* 101 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateInsuranceCoverageStartDate(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 123 */     ErrorObject errorObj = null;
/*     */     
/* 125 */     if (CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 127 */       errorObj = new ErrorObject(
/* 128 */         SubmissionErrorCodes.ERROR_DATAFIELD390_INSURANCECOVERAGE_STARTDATE);
/* 129 */       errorCodes.add(errorObj);
/*     */ 
/*     */ 
/*     */     }
/* 133 */     else if (CommonValidator.validateData(value, 
/* 134 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ť']))
/*     */     {
/* 136 */       String reqLength = CommonValidator.validateLength(value, 8, 8);
/*     */       
/* 138 */       if ((!reqLength.equals("equal")) || 
/* 139 */         (!CommonValidator.isValidDate(value, 
/* 140 */         "CCYYMMDD")))
/*     */       {
/* 142 */         errorObj = new ErrorObject(
/* 143 */           SubmissionErrorCodes.ERROR_DATAFIELD390_INSURANCECOVERAGE_STARTDATE);
/* 144 */         errorCodes.add(errorObj);
/*     */       }
/*     */     } else {
/* 147 */       errorObj = new ErrorObject(
/* 148 */         SubmissionErrorCodes.ERROR_DATAFIELD392_INSURANCECOVERAGE_STARTDATE);
/* 149 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateInsuranceCoverageEndDate(InsuranceIndustryTAABean insuranceIndustryTAABean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 167 */     ErrorObject errorObj = null;
/*     */     
/* 169 */     if (CommonValidator.isNullOrEmpty(insuranceIndustryTAABean
/* 170 */       .getInsuranceCoverageEndDate())) {
/* 171 */       errorObj = new ErrorObject(
/* 172 */         SubmissionErrorCodes.ERROR_DATAFIELD393_INSURANCECOVERAGE_ENDDATE);
/* 173 */       errorCodes.add(errorObj);
/*     */ 
/*     */ 
/*     */     }
/* 177 */     else if (CommonValidator.validateData(insuranceIndustryTAABean
/* 178 */       .getInsuranceCoverageEndDate(), 
/* 179 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ŧ']))
/*     */     {
/* 181 */       String reqLength = CommonValidator.validateLength(
/* 182 */         insuranceIndustryTAABean.getInsuranceCoverageEndDate(), 
/* 183 */         8, 8);
/*     */       
/* 185 */       if ((reqLength.equals("equal")) && 
/* 186 */         (CommonValidator.isValidDate(insuranceIndustryTAABean
/* 187 */         .getInsuranceCoverageEndDate(), 
/* 188 */         "CCYYMMDD")))
/*     */       {
/*     */ 
/*     */ 
/* 192 */         if (Integer.parseInt(insuranceIndustryTAABean.getInsuranceCoverageEndDate()) < Integer.parseInt(insuranceIndustryTAABean
/* 193 */           .getInsuranceCoverageStartDate()))
/*     */         {
/* 195 */           errorObj = new ErrorObject(
/* 196 */             SubmissionErrorCodes.ERROR_DATAFIELD394_INSURANCECOVERAGE_ENDDATE);
/* 197 */           errorCodes.add(errorObj);
/*     */         }
/*     */       } else {
/* 200 */         errorObj = new ErrorObject(
/* 201 */           SubmissionErrorCodes.ERROR_DATAFIELD393_INSURANCECOVERAGE_ENDDATE);
/* 202 */         errorCodes.add(errorObj);
/*     */       }
/*     */     } else {
/* 205 */       errorObj = new ErrorObject(
/* 206 */         SubmissionErrorCodes.ERROR_DATAFIELD395_INSURANCECOVERAGE_ENDDATE);
/* 207 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateInsurancePolicyPremiumFrequency(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 224 */     ErrorObject errorObj = null;
/*     */     
/* 226 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/* 231 */       String reqLength = CommonValidator.validateLength(value, 7, 7);
/*     */       
/* 233 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/* 235 */         errorObj = new ErrorObject(
/* 236 */           SubmissionErrorCodes.ERROR_DATAFIELD398_INSURANCEPOLICY_PREMIUMFREQUENCY);
/* 237 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateAdditionalInsurancePolicyNumber(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 259 */     ErrorObject errorObj = null;
/*     */     
/* 261 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/* 266 */       String reqLength = 
/* 267 */         CommonValidator.validateLength(value, 23, 23);
/*     */       
/* 269 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/* 271 */         errorObj = new ErrorObject(
/* 272 */           SubmissionErrorCodes.ERROR_DATAFIELD400_ADDITIONALINSURANCE_POLICYNUMBER);
/* 273 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateTypeOfPolicy(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 295 */     ErrorObject errorObj = null;
/*     */     
/* 297 */     if (CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 299 */       errorObj = new ErrorObject(
/* 300 */         SubmissionErrorCodes.ERROR_DATAFIELD401_TYPEOF_POLICY);
/* 301 */       errorCodes.add(errorObj);
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/* 308 */       String reqLength = 
/* 309 */         CommonValidator.validateLength(value, 25, 25);
/*     */       
/* 311 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/* 313 */         errorObj = new ErrorObject(
/* 314 */           SubmissionErrorCodes.ERROR_DATAFIELD403_TYPEOF_POLICY);
/* 315 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateNameOfInsured(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 337 */     ErrorObject errorObj = null;
/*     */     
/* 339 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 345 */       String reqLength = 
/* 346 */         CommonValidator.validateLength(value, 30, 30);
/*     */       
/* 348 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/* 350 */         errorObj = new ErrorObject(
/* 351 */           SubmissionErrorCodes.ERROR_DATAFIELD403_NAMEOF_INSURED);
/* 352 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\validator\industry\InsuranceRecordValidator.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */