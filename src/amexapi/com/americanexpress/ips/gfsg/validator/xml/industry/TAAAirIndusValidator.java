/*      */ package com.americanexpress.ips.gfsg.validator.xml.industry;
/*      */ 
/*      */ import com.americanexpress.ips.gfsg.beans.ErrorObject;
/*      */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.AirlineSegInfoBean;
/*      */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.TAAAirIndustryBean;
/*      */ import com.americanexpress.ips.gfsg.enumerations.StopOverIndicator;
/*      */ import com.americanexpress.ips.gfsg.enumerations.TravTransTypeCd;
/*      */ import com.americanexpress.ips.gfsg.enumerations.XMLSubmissionErrorCodes;
/*      */ import com.americanexpress.ips.gfsg.exceptions.SettlementException;
/*      */ import com.americanexpress.ips.gfsg.validator.CommonValidator;
/*      */ import java.util.List;
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ public class TAAAirIndusValidator
/*      */ {
/*      */   public static void validateAirLineIndustry(TAAAirIndustryBean tAAAirIndusBean, List<ErrorObject> errorCodes, String transCurrCd)
/*      */     throws SettlementException
/*      */   {
/*   40 */     validateTravTransTypeCd(tAAAirIndusBean, errorCodes);
/*   41 */     validateTktNbr(tAAAirIndusBean, errorCodes);
/*   42 */     validateDocTypeCd(tAAAirIndusBean, errorCodes);
/*   43 */     validateIATACarrierCd(tAAAirIndusBean, errorCodes);
/*   44 */     validateIATAAgcyNbr(tAAAirIndusBean, errorCodes);
/*   45 */     validateTktCarrierNm(tAAAirIndusBean, errorCodes);
/*   46 */     validateTktIssCityNm(tAAAirIndusBean, errorCodes);
/*   47 */     validateTktIssDt(tAAAirIndusBean, errorCodes);
/*   48 */     validatePassCnt(tAAAirIndusBean, errorCodes);
/*   49 */     validatePassNm(tAAAirIndusBean, errorCodes);
/*   50 */     validateConjTktInd(tAAAirIndusBean, errorCodes);
/*   51 */     validateElecTktInd(tAAAirIndusBean, errorCodes);
/*   52 */     validateExchTktNbr(tAAAirIndusBean, errorCodes);
/*   53 */     validateAirSegInfo(tAAAirIndusBean, errorCodes);
/*   54 */     validateOrigTransAmt(tAAAirIndusBean, errorCodes, transCurrCd);
/*   55 */     validateOrigCurrCd(tAAAirIndusBean, errorCodes);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTravTransTypeCd(TAAAirIndustryBean tAAAirIndusBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*   71 */     ErrorObject errorObj = null;
/*   72 */     boolean isValid = true;
/*   73 */     if (CommonValidator.isNullOrEmpty(tAAAirIndusBean.getTravTransTypeCd())) {
/*   74 */       isValid = false;
/*      */ 
/*      */     }
/*   77 */     else if ((CommonValidator.validateData(tAAAirIndusBean
/*   78 */       .getTravTransTypeCd(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[66])) && (CommonValidator.isValidTravTransTypeCd(tAAAirIndusBean.getTravTransTypeCd())))
/*      */     {
/*   80 */       String reqLength = CommonValidator.validateLength(
/*   81 */         tAAAirIndusBean.getTravTransTypeCd(), 3, 3);
/*      */       
/*   83 */       if (reqLength.equals("greaterThanMax")) {
/*   84 */         isValid = false;
/*      */       }
/*      */     }
/*      */     else {
/*   88 */       isValid = false;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*   93 */     if (!isValid)
/*      */     {
/*      */ 
/*   96 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_AIRTRAVTRANSTYPECD);
/*   97 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTktNbr(TAAAirIndustryBean tAAAirIndusBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  113 */     ErrorObject errorObj = null;
/*  114 */     boolean isValid = true;
/*  115 */     if (CommonValidator.isNullOrEmpty(tAAAirIndusBean.getTktNbr()))
/*      */     {
/*  117 */       if (!CommonValidator.isNullOrEmpty(tAAAirIndusBean.getTravTransTypeCd()))
/*      */       {
/*  119 */         if ((tAAAirIndusBean.getTravTransTypeCd().equalsIgnoreCase(TravTransTypeCd.Exchange_Ticket.getTravTransTypeCd())) || (tAAAirIndusBean.getTravTransTypeCd().equalsIgnoreCase(TravTransTypeCd.Ticket.getTravTransTypeCd()))) {
/*  120 */           isValid = false;
/*      */         }
/*      */         
/*      */       }
/*      */       
/*      */ 
/*      */     }
/*      */     else
/*      */     {
/*      */ 
/*  130 */       String reqLength = CommonValidator.validateLength(
/*  131 */         tAAAirIndusBean.getTktNbr(), 14, 1);
/*      */       
/*  133 */       if (reqLength.equals("greaterThanMax")) {
/*  134 */         isValid = false;
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  144 */     if (!isValid)
/*      */     {
/*  146 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_AIRTKTNBR);
/*  147 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateDocTypeCd(TAAAirIndustryBean tAAAirIndusBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  163 */     boolean isValid = true;
/*  164 */     ErrorObject errorObj = null;
/*      */     
/*  166 */     if (CommonValidator.isNullOrEmpty(tAAAirIndusBean.getDocTypeCd())) {
/*  167 */       isValid = false;
/*      */ 
/*      */ 
/*      */     }
/*  171 */     else if (CommonValidator.validateData(tAAAirIndusBean
/*  172 */       .getDocTypeCd(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[68]))
/*      */     {
/*  174 */       String reqLength = CommonValidator.validateLength(
/*  175 */         tAAAirIndusBean.getDocTypeCd(), 2, 2);
/*      */       
/*  177 */       if (reqLength.equals("greaterThanMax")) {
/*  178 */         isValid = false;
/*      */       }
/*      */     }
/*      */     else {
/*  182 */       isValid = false;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*  187 */     if (!isValid)
/*      */     {
/*  189 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_DOCTYPECD);
/*  190 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateIATACarrierCd(TAAAirIndustryBean tAAAirIndusBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  210 */     ErrorObject errorObj = null;
/*  211 */     boolean isValid = true;
/*  212 */     if (CommonValidator.isNullOrEmpty(tAAAirIndusBean.getIATACarrierCd())) {
/*  213 */       isValid = false;
/*      */ 
/*      */     }
/*  216 */     else if (CommonValidator.validateData(tAAAirIndusBean
/*  217 */       .getIATACarrierCd(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[69]))
/*      */     {
/*  219 */       String reqLength = CommonValidator.validateLength(
/*  220 */         tAAAirIndusBean.getIATACarrierCd(), 3, 2);
/*      */       
/*  222 */       if (reqLength.equals("greaterThanMax")) {
/*  223 */         isValid = false;
/*  224 */       } else if (reqLength.equals("lessThanMin")) {
/*  225 */         isValid = false;
/*      */       }
/*      */     } else {
/*  228 */       isValid = false;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*  233 */     if (!isValid)
/*      */     {
/*  235 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_AIRLINEIATACARRIERCD);
/*  236 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateIATAAgcyNbr(TAAAirIndustryBean tAAAirIndusBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  253 */     ErrorObject errorObj = null;
/*  254 */     boolean isValid = true;
/*  255 */     if (!CommonValidator.isNullOrEmpty(tAAAirIndusBean.getIATAAgcyNbr()))
/*      */     {
/*      */ 
/*  258 */       if (CommonValidator.validateData(tAAAirIndusBean
/*  259 */         .getIATAAgcyNbr(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[70]))
/*      */       {
/*  261 */         String reqLength = CommonValidator.validateLength(
/*  262 */           tAAAirIndusBean.getIATAAgcyNbr(), 8, 1);
/*      */         
/*  264 */         if (reqLength.equals("greaterThanMax")) {
/*  265 */           isValid = false;
/*      */         }
/*      */       }
/*      */       else {
/*  269 */         isValid = false;
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*  274 */     if (!isValid)
/*      */     {
/*  276 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_AIRIATAAGCYNBR);
/*  277 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTktCarrierNm(TAAAirIndustryBean tAAAirIndusBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  296 */     ErrorObject errorObj = null;
/*  297 */     boolean isValid = true;
/*  298 */     if (CommonValidator.isNullOrEmpty(tAAAirIndusBean.getTktCarrierNm())) {
/*  299 */       isValid = false;
/*      */ 
/*      */ 
/*      */     }
/*      */     else
/*      */     {
/*      */ 
/*  306 */       String reqLength = CommonValidator.validateLength(
/*  307 */         tAAAirIndusBean.getTktCarrierNm(), 25, 1);
/*      */       
/*  309 */       if (reqLength.equals("greaterThanMax")) {
/*  310 */         isValid = false;
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  319 */     if (!isValid)
/*      */     {
/*  321 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_TKTCARRIERNM);
/*  322 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTktIssCityNm(TAAAirIndustryBean tAAAirIndusBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  339 */     ErrorObject errorObj = null;
/*  340 */     boolean isValid = true;
/*  341 */     if (CommonValidator.isNullOrEmpty(tAAAirIndusBean.getTktIssCityNm())) {
/*  342 */       isValid = false;
/*      */ 
/*      */ 
/*      */     }
/*      */     else
/*      */     {
/*      */ 
/*  349 */       String reqLength = CommonValidator.validateLength(
/*  350 */         tAAAirIndusBean.getTktIssCityNm(), 18, 1);
/*      */       
/*  352 */       if (reqLength.equals("greaterThanMax")) {
/*  353 */         isValid = false;
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  362 */     if (!isValid)
/*      */     {
/*  364 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_AIRLINETKTISSCITYNM);
/*  365 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTktIssDt(TAAAirIndustryBean tAAAirIndusBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  382 */     ErrorObject errorObj = null;
/*  383 */     boolean isValid = true;
/*  384 */     if (CommonValidator.isNullOrEmpty(tAAAirIndusBean.getTktIssDt())) {
/*  385 */       isValid = false;
/*      */ 
/*      */     }
/*  388 */     else if (CommonValidator.validateData(tAAAirIndusBean
/*  389 */       .getTktIssDt(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[73]))
/*      */     {
/*  391 */       isValid = CommonValidator.isValidXmlDate(tAAAirIndusBean
/*  392 */         .getTktIssDt(), "CCYYMMDD");
/*      */     } else {
/*  394 */       isValid = false;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*  399 */     if (!isValid)
/*      */     {
/*  401 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_TKTISSDT);
/*  402 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validatePassCnt(TAAAirIndustryBean tAAAirIndusBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  420 */     ErrorObject errorObj = null;
/*  421 */     boolean isValid = true;
/*  422 */     if (CommonValidator.isNullOrEmpty(tAAAirIndusBean.getPassCnt()))
/*      */     {
/*      */ 
/*  425 */       if (!CommonValidator.isNullOrEmpty(tAAAirIndusBean.getTravTransTypeCd()))
/*      */       {
/*  427 */         if ((tAAAirIndusBean.getTravTransTypeCd().equalsIgnoreCase(TravTransTypeCd.Exchange_Ticket.getTravTransTypeCd())) || (tAAAirIndusBean.getTravTransTypeCd().equalsIgnoreCase(TravTransTypeCd.Ticket.getTravTransTypeCd())))
/*      */         {
/*  429 */           isValid = false;
/*      */         }
/*      */         
/*      */       }
/*      */       
/*      */     }
/*  435 */     else if (CommonValidator.validateData(tAAAirIndusBean
/*  436 */       .getPassCnt(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[74]))
/*      */     {
/*  438 */       String reqLength = CommonValidator.validateLength(
/*  439 */         tAAAirIndusBean.getPassCnt(), 3, 1);
/*      */       
/*  441 */       if (reqLength.equals("greaterThanMax")) {
/*  442 */         isValid = false;
/*      */       }
/*      */     }
/*      */     else {
/*  446 */       isValid = false;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*  451 */     if (!isValid)
/*      */     {
/*  453 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_PASSCNT);
/*  454 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validatePassNm(TAAAirIndustryBean tAAAirIndusBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  471 */     ErrorObject errorObj = null;
/*  472 */     boolean isValid = true;
/*  473 */     if (CommonValidator.isNullOrEmpty(tAAAirIndusBean.getPassNm()))
/*      */     {
/*  475 */       if (!CommonValidator.isNullOrEmpty(tAAAirIndusBean.getTravTransTypeCd()))
/*      */       {
/*  477 */         if ((tAAAirIndusBean.getTravTransTypeCd().equalsIgnoreCase(TravTransTypeCd.Exchange_Ticket.getTravTransTypeCd())) || (tAAAirIndusBean.getTravTransTypeCd().equalsIgnoreCase(TravTransTypeCd.Ticket.getTravTransTypeCd()))) {
/*  478 */           isValid = false;
/*      */         }
/*      */         
/*      */       }
/*      */       
/*      */ 
/*      */     }
/*      */     else
/*      */     {
/*  487 */       String reqLength = CommonValidator.validateLength(
/*  488 */         tAAAirIndusBean.getPassNm(), 25, 1);
/*      */       
/*  490 */       if (reqLength.equals("greaterThanMax")) {
/*  491 */         isValid = false;
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  500 */     if (!isValid)
/*      */     {
/*  502 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_AIRPASSNM);
/*  503 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateConjTktInd(TAAAirIndustryBean tAAAirIndusBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  521 */     ErrorObject errorObj = null;
/*  522 */     boolean isValid = true;
/*  523 */     if (CommonValidator.isNullOrEmpty(tAAAirIndusBean.getConjTktInd()))
/*      */     {
/*  525 */       if (!CommonValidator.isNullOrEmpty(tAAAirIndusBean.getTravTransTypeCd()))
/*      */       {
/*  527 */         if ((tAAAirIndusBean.getTravTransTypeCd().equalsIgnoreCase(TravTransTypeCd.Exchange_Ticket.getTravTransTypeCd())) || (tAAAirIndusBean.getTravTransTypeCd().equalsIgnoreCase(TravTransTypeCd.Ticket.getTravTransTypeCd()))) {
/*  528 */           isValid = false;
/*      */         }
/*      */         
/*      */       }
/*      */     }
/*      */     else
/*      */     {
/*  535 */       if (CommonValidator.validateData(tAAAirIndusBean
/*  536 */         .getConjTktInd(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[76])) {
/*  537 */         if ((tAAAirIndusBean.getConjTktInd().equalsIgnoreCase("Y")) || 
/*  538 */           (tAAAirIndusBean.getConjTktInd().equalsIgnoreCase("N")))
/*      */         {
/*  540 */           String reqLength = CommonValidator.validateLength(
/*  541 */             tAAAirIndusBean.getConjTktInd(), 1, 1);
/*      */           
/*  543 */           if (!reqLength.equals("greaterThanMax")) break label129;
/*  544 */           isValid = false;
/*      */           break label129;
/*      */         }
/*      */       }
/*  548 */       isValid = false;
/*      */     }
/*      */     
/*      */ 
/*      */     label129:
/*      */     
/*  554 */     if (!isValid)
/*      */     {
/*  556 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_CONJTKTIND);
/*  557 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateElecTktInd(TAAAirIndustryBean tAAAirIndusBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  574 */     ErrorObject errorObj = null;
/*  575 */     boolean isValid = true;
/*  576 */     if (CommonValidator.isNullOrEmpty(tAAAirIndusBean.getElecTktInd()))
/*      */     {
/*  578 */       if (!CommonValidator.isNullOrEmpty(tAAAirIndusBean.getTravTransTypeCd()))
/*      */       {
/*  580 */         if ((tAAAirIndusBean.getTravTransTypeCd().equalsIgnoreCase(TravTransTypeCd.Exchange_Ticket.getTravTransTypeCd())) || (tAAAirIndusBean.getTravTransTypeCd().equalsIgnoreCase(TravTransTypeCd.Ticket.getTravTransTypeCd())))
/*      */         {
/*  582 */           isValid = false;
/*      */         }
/*      */       }
/*      */     }
/*      */     else
/*      */     {
/*  588 */       if (CommonValidator.validateData(tAAAirIndusBean
/*  589 */         .getElecTktInd(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[77])) {
/*  590 */         if ((tAAAirIndusBean.getElecTktInd().equalsIgnoreCase("Y")) || 
/*  591 */           (tAAAirIndusBean.getElecTktInd().equalsIgnoreCase("N")))
/*      */         {
/*  593 */           String reqLength = CommonValidator.validateLength(
/*  594 */             tAAAirIndusBean.getElecTktInd(), 1, 1);
/*      */           
/*  596 */           if (!reqLength.equals("greaterThanMax")) break label129;
/*  597 */           isValid = false;
/*      */           break label129;
/*      */         }
/*      */       }
/*  601 */       isValid = false;
/*      */     }
/*      */     
/*      */ 
/*      */     label129:
/*      */     
/*  607 */     if (!isValid)
/*      */     {
/*  609 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_ELECTKTIND);
/*  610 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateExchTktNbr(TAAAirIndustryBean tAAAirIndusBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  627 */     ErrorObject errorObj = null;
/*  628 */     boolean isValid = true;
/*  629 */     if (!CommonValidator.isNullOrEmpty(tAAAirIndusBean.getExchTktNbr()))
/*      */     {
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  635 */       String reqLength = CommonValidator.validateLength(
/*  636 */         tAAAirIndusBean.getExchTktNbr(), 14, 1);
/*      */       
/*  638 */       if (reqLength.equals("greaterThanMax")) {
/*  639 */         isValid = false;
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  648 */     if (!isValid)
/*      */     {
/*  650 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_EXCHTKTNBR);
/*  651 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateStopoverCd(AirlineSegInfoBean airlineSegInfoBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  668 */     ErrorObject errorObj = null;
/*  669 */     boolean isValid = true;
/*  670 */     if (!CommonValidator.isNullOrEmpty(airlineSegInfoBean.getStopoverCd()))
/*      */     {
/*  672 */       if (CommonValidator.validateData(
/*  673 */         airlineSegInfoBean.getStopoverCd(), 
/*  674 */         com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[79]))
/*      */       {
/*  676 */         if ((airlineSegInfoBean.getStopoverCd().equalsIgnoreCase(StopOverIndicator.Stopover_permitted.getStopOverIndicator())) || 
/*  677 */           (airlineSegInfoBean.getStopoverCd().equalsIgnoreCase(StopOverIndicator.No_stopoverpermitted.getStopOverIndicator())))
/*      */         {
/*  679 */           String reqLength = CommonValidator.validateLength(
/*  680 */             airlineSegInfoBean.getStopoverCd(), 1, 1);
/*      */           
/*  682 */           if (!reqLength.equals("greaterThanMax")) break label90;
/*  683 */           isValid = false;
/*      */           break label90;
/*      */         }
/*      */       }
/*  687 */       isValid = false;
/*      */     }
/*      */     
/*      */     label90:
/*      */     
/*  692 */     if (!isValid)
/*      */     {
/*  694 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_STOPOVERCD);
/*  695 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateDprtLocCd(AirlineSegInfoBean airlineSegInfoBean, String travTransTypeCd, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  715 */     ErrorObject errorObj = null;
/*  716 */     boolean isValid = true;
/*  717 */     if (CommonValidator.isNullOrEmpty(airlineSegInfoBean.getDprtLocCd()))
/*      */     {
/*  719 */       if (!CommonValidator.isNullOrEmpty(travTransTypeCd))
/*      */       {
/*  721 */         if ((travTransTypeCd.equalsIgnoreCase(TravTransTypeCd.Exchange_Ticket
/*  722 */           .getTravTransTypeCd())) || 
/*      */           
/*  724 */           (travTransTypeCd.equalsIgnoreCase(TravTransTypeCd.Ticket
/*  725 */           .getTravTransTypeCd())))
/*      */         {
/*  727 */           isValid = false;
/*      */         }
/*      */         
/*      */       }
/*      */       
/*      */     }
/*  733 */     else if (CommonValidator.validateData(airlineSegInfoBean.getDprtLocCd(), 
/*  734 */       com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[80]))
/*      */     {
/*  736 */       String reqLength = CommonValidator.validateLength(
/*  737 */         airlineSegInfoBean.getDprtLocCd(), 3, 3);
/*      */       
/*  739 */       if (reqLength.equals("greaterThanMax")) {
/*  740 */         isValid = false;
/*      */       }
/*      */     }
/*      */     else {
/*  744 */       isValid = false;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*  749 */     if (!isValid)
/*      */     {
/*  751 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_DPRTLOCCD);
/*  752 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateDprtDt(AirlineSegInfoBean airlineSegInfoBean, String travTransTypeCd, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  773 */     ErrorObject errorObj = null;
/*  774 */     boolean isValid = true;
/*  775 */     if (CommonValidator.isNullOrEmpty(airlineSegInfoBean.getDprtDt())) {
/*  776 */       if (!CommonValidator.isNullOrEmpty(travTransTypeCd))
/*      */       {
/*  778 */         if ((travTransTypeCd.equalsIgnoreCase(TravTransTypeCd.Exchange_Ticket
/*  779 */           .getTravTransTypeCd())) || 
/*      */           
/*  781 */           (travTransTypeCd.equalsIgnoreCase(TravTransTypeCd.Ticket
/*  782 */           .getTravTransTypeCd())))
/*      */         {
/*  784 */           isValid = false;
/*      */         }
/*      */         
/*      */       }
/*      */     }
/*  789 */     else if (CommonValidator.validateData(airlineSegInfoBean.getDprtDt(), 
/*  790 */       com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[81]))
/*      */     {
/*  792 */       isValid = CommonValidator.isValidXmlDate(airlineSegInfoBean
/*  793 */         .getDprtDt(), 
/*  794 */         "CCYYMMDD");
/*      */     } else {
/*  796 */       isValid = false;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*  801 */     if (!isValid)
/*      */     {
/*  803 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_AIRDPRTDT);
/*  804 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateArrLocCd(AirlineSegInfoBean airlineSegInfoBean, String travTransTypeCd, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  824 */     ErrorObject errorObj = null;
/*  825 */     boolean isValid = true;
/*  826 */     if (CommonValidator.isNullOrEmpty(airlineSegInfoBean.getArrLocCd())) {
/*  827 */       if (!CommonValidator.isNullOrEmpty(travTransTypeCd))
/*      */       {
/*  829 */         if ((travTransTypeCd.equalsIgnoreCase(TravTransTypeCd.Exchange_Ticket
/*  830 */           .getTravTransTypeCd())) || 
/*      */           
/*  832 */           (travTransTypeCd.equalsIgnoreCase(TravTransTypeCd.Ticket
/*  833 */           .getTravTransTypeCd()))) {
/*  834 */           isValid = false;
/*      */         }
/*      */         
/*      */       }
/*      */     }
/*  839 */     else if (CommonValidator.validateData(airlineSegInfoBean.getArrLocCd(), 
/*  840 */       com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[82]))
/*      */     {
/*  842 */       String reqLength = CommonValidator.validateLength(
/*  843 */         airlineSegInfoBean.getArrLocCd(), 3, 3);
/*      */       
/*  845 */       if (reqLength.equals("greaterThanMax")) {
/*  846 */         isValid = false;
/*      */       }
/*      */     }
/*      */     else {
/*  850 */       isValid = false;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*  855 */     if (!isValid)
/*      */     {
/*  857 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_ARRLOCCD);
/*  858 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateSegIATACarrierCd(AirlineSegInfoBean airlineSegInfoBean, String travTransTypeCd, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  879 */     ErrorObject errorObj = null;
/*  880 */     boolean isValid = true;
/*  881 */     if (CommonValidator.isNullOrEmpty(airlineSegInfoBean
/*  882 */       .getSegIATACarrierCd())) {
/*  883 */       if (!CommonValidator.isNullOrEmpty(travTransTypeCd))
/*      */       {
/*  885 */         if ((travTransTypeCd.equalsIgnoreCase(TravTransTypeCd.Exchange_Ticket
/*  886 */           .getTravTransTypeCd())) || 
/*      */           
/*  888 */           (travTransTypeCd.equalsIgnoreCase(TravTransTypeCd.Ticket
/*  889 */           .getTravTransTypeCd())))
/*      */         {
/*  891 */           isValid = false;
/*      */         }
/*      */         
/*      */       }
/*      */     }
/*  896 */     else if (CommonValidator.validateData(airlineSegInfoBean
/*  897 */       .getSegIATACarrierCd(), 
/*  898 */       com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[83]))
/*      */     {
/*  900 */       String reqLength = CommonValidator.validateLength(
/*  901 */         airlineSegInfoBean.getSegIATACarrierCd(), 2, 2);
/*      */       
/*  903 */       if (reqLength.equals("greaterThanMax")) {
/*  904 */         isValid = false;
/*      */       }
/*  906 */       else if (reqLength.equals("lessThanMin")) {
/*  907 */         isValid = false;
/*      */       }
/*      */     }
/*      */     else {
/*  911 */       isValid = false;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*  916 */     if (!isValid)
/*      */     {
/*  918 */       errorObj = new ErrorObject(
/*  919 */         XMLSubmissionErrorCodes.ERROR_AIRSEGIATACARRIERCD);
/*  920 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateFareBasisTxt(AirlineSegInfoBean airlineSegInfoBean, String travTransTypeCd, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  941 */     ErrorObject errorObj = null;
/*  942 */     boolean isValid = true;
/*  943 */     if (CommonValidator.isNullOrEmpty(airlineSegInfoBean.getFareBasisTxt()))
/*      */     {
/*  945 */       if (!CommonValidator.isNullOrEmpty(travTransTypeCd))
/*      */       {
/*  947 */         if ((travTransTypeCd.equalsIgnoreCase(TravTransTypeCd.Exchange_Ticket
/*  948 */           .getTravTransTypeCd())) || 
/*      */           
/*  950 */           (travTransTypeCd.equalsIgnoreCase(TravTransTypeCd.Ticket
/*  951 */           .getTravTransTypeCd()))) {
/*  952 */           isValid = false;
/*      */         }
/*      */         
/*      */       }
/*      */       
/*      */ 
/*      */     }
/*      */     else
/*      */     {
/*      */ 
/*  962 */       String reqLength = CommonValidator.validateLength(
/*  963 */         airlineSegInfoBean.getFareBasisTxt(), 15, 1);
/*      */       
/*  965 */       if (reqLength.equals("greaterThanMax")) {
/*  966 */         isValid = false;
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  975 */     if (!isValid)
/*      */     {
/*  977 */       errorObj = new ErrorObject(
/*  978 */         XMLSubmissionErrorCodes.ERROR_FAREBASISTXT);
/*  979 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateServClassCd(AirlineSegInfoBean airlineSegInfoBean, String travTransTypeCd, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  999 */     ErrorObject errorObj = null;
/* 1000 */     boolean isValid = true;
/* 1001 */     if (CommonValidator.isNullOrEmpty(airlineSegInfoBean.getServClassCd()))
/*      */     {
/* 1003 */       if (!CommonValidator.isNullOrEmpty(travTransTypeCd))
/*      */       {
/* 1005 */         if ((travTransTypeCd.equalsIgnoreCase(TravTransTypeCd.Exchange_Ticket
/* 1006 */           .getTravTransTypeCd())) || 
/*      */           
/* 1008 */           (travTransTypeCd.equalsIgnoreCase(TravTransTypeCd.Ticket
/* 1009 */           .getTravTransTypeCd()))) {
/* 1010 */           isValid = false;
/*      */         }
/*      */         
/*      */       }
/*      */     }
/* 1015 */     else if (CommonValidator.validateData(airlineSegInfoBean
/* 1016 */       .getServClassCd(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[85]))
/*      */     {
/* 1018 */       String reqLength = CommonValidator.validateLength(
/* 1019 */         airlineSegInfoBean.getServClassCd(), 2, 2);
/*      */       
/* 1021 */       if (reqLength.equals("greaterThanMax")) {
/* 1022 */         isValid = false;
/*      */       }
/*      */     }
/*      */     else {
/* 1026 */       isValid = false;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/* 1031 */     if (!isValid)
/*      */     {
/* 1033 */       errorObj = new ErrorObject(
/* 1034 */         XMLSubmissionErrorCodes.ERROR_AIRSERVCLASSCD);
/* 1035 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateFlghtNbr(AirlineSegInfoBean airlineSegInfoBean, String travTransTypeCd, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1055 */     ErrorObject errorObj = null;
/* 1056 */     boolean isValid = true;
/* 1057 */     if (CommonValidator.isNullOrEmpty(airlineSegInfoBean.getFlghtNbr()))
/*      */     {
/* 1059 */       if (!CommonValidator.isNullOrEmpty(travTransTypeCd))
/*      */       {
/* 1061 */         if ((travTransTypeCd.equalsIgnoreCase(TravTransTypeCd.Exchange_Ticket
/* 1062 */           .getTravTransTypeCd())) || 
/*      */           
/* 1064 */           (travTransTypeCd.equalsIgnoreCase(TravTransTypeCd.Ticket
/* 1065 */           .getTravTransTypeCd()))) {
/* 1066 */           isValid = false;
/*      */         }
/*      */         
/*      */       }
/*      */       
/*      */ 
/*      */     }
/*      */     else
/*      */     {
/* 1075 */       String reqLength = CommonValidator.validateLength(
/* 1076 */         airlineSegInfoBean.getFlghtNbr(), 4, 1);
/*      */       
/* 1078 */       if (reqLength.equals("greaterThanMax")) {
/* 1079 */         isValid = false;
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1088 */     if (!isValid)
/*      */     {
/* 1090 */       errorObj = new ErrorObject(
/* 1091 */         XMLSubmissionErrorCodes.ERROR_AIRFLGHTNBR);
/* 1092 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateSegTotFareAmt(AirlineSegInfoBean airlineSegInfoBean, List<ErrorObject> errorCodes, String originalCurrencyCode)
/*      */     throws SettlementException
/*      */   {
/* 1113 */     ErrorObject errorObj = null;
/* 1114 */     boolean isValid = true;
/* 1115 */     if (!CommonValidator.isNullOrEmpty(airlineSegInfoBean
/* 1116 */       .getSegTotFareAmt())) {
/* 1117 */       if (CommonValidator.validateData(airlineSegInfoBean
/* 1118 */         .getSegTotFareAmt(), 
/* 1119 */         com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[87])) {
/* 1120 */         if (CommonValidator.isValidXmlAmount(originalCurrencyCode, 
/* 1121 */           airlineSegInfoBean.getSegTotFareAmt()))
/*      */         {
/* 1123 */           String reqLength = CommonValidator.validateLength(
/* 1124 */             airlineSegInfoBean.getSegTotFareAmt(), 12, 1);
/*      */           
/* 1126 */           if (reqLength.equals("greaterThanMax")) {
/* 1127 */             isValid = false;
/*      */             break label97; }
/* 1129 */           if ((CommonValidator.isNullOrEmpty(originalCurrencyCode)) || 
/* 1130 */             (CommonValidator.isValidXmlAmount(
/* 1131 */             originalCurrencyCode, 
/* 1132 */             airlineSegInfoBean.getSegTotFareAmt()))) break label97;
/* 1133 */           isValid = false;
/*      */           break label97;
/*      */         }
/*      */       }
/* 1137 */       isValid = false;
/*      */     }
/*      */     
/*      */     label97:
/* 1141 */     if (!isValid)
/*      */     {
/* 1143 */       errorObj = new ErrorObject(
/* 1144 */         XMLSubmissionErrorCodes.ERROR_SEGTOTFAREAMT);
/* 1145 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateOrigTransAmt(TAAAirIndustryBean tAAAirIndusBean, List<ErrorObject> errorCodes, String transCurrCd)
/*      */     throws SettlementException
/*      */   {
/* 1165 */     ErrorObject errorObj = null;
/* 1166 */     boolean isValid = true;
/* 1167 */     if (CommonValidator.isNullOrEmpty(tAAAirIndusBean.getOrigTransAmt()))
/*      */     {
/* 1169 */       if (!CommonValidator.isNullOrEmpty(tAAAirIndusBean.getOrigCurrCd())) {
/* 1170 */         isValid = false;
/*      */       }
/*      */       
/*      */     }
/* 1174 */     else if (CommonValidator.validateData(tAAAirIndusBean.getOrigTransAmt(), 
/* 1175 */       com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[88])) {
/* 1176 */       String reqLength = CommonValidator.validateLength(
/* 1177 */         tAAAirIndusBean.getOrigTransAmt(), 12, 1);
/*      */       
/* 1179 */       if (reqLength.equals("greaterThanMax")) {
/* 1180 */         isValid = false;
/*      */       }
/* 1182 */       else if ((!CommonValidator.isNullOrEmpty(transCurrCd)) && 
/* 1183 */         (!CommonValidator.isValidXmlAmount(transCurrCd, 
/* 1184 */         tAAAirIndusBean.getOrigTransAmt()))) {
/* 1185 */         isValid = false;
/*      */       }
/*      */     }
/*      */     else
/*      */     {
/* 1190 */       isValid = false;
/*      */     }
/*      */     
/*      */ 
/* 1194 */     if (!isValid)
/*      */     {
/* 1196 */       errorObj = new ErrorObject(
/* 1197 */         XMLSubmissionErrorCodes.ERROR_ORIGTRANSAMT);
/* 1198 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateOrigCurrCd(TAAAirIndustryBean tAAAirIndusBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1216 */     ErrorObject errorObj = null;
/* 1217 */     boolean isValid = true;
/* 1218 */     if (CommonValidator.isNullOrEmpty(tAAAirIndusBean.getOrigCurrCd()))
/*      */     {
/* 1220 */       if (!CommonValidator.isNullOrEmpty(tAAAirIndusBean
/* 1221 */         .getOrigTransAmt())) {
/* 1222 */         isValid = false;
/*      */       }
/*      */     }
/*      */     else {
/* 1226 */       if (CommonValidator.validateData(tAAAirIndusBean.getOrigCurrCd(), 
/* 1227 */         com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[89])) {
/* 1228 */         if (CommonValidator.isValidXmlCurrencyCode(tAAAirIndusBean
/* 1229 */           .getOrigCurrCd()))
/*      */         {
/* 1231 */           String reqLength = CommonValidator.validateLength(
/* 1232 */             tAAAirIndusBean.getOrigCurrCd(), 3, 3);
/*      */           
/* 1234 */           if (!reqLength.equals("greaterThanMax")) break label83;
/* 1235 */           isValid = false;
/*      */           break label83;
/*      */         }
/*      */       }
/* 1239 */       isValid = false;
/*      */     }
/*      */     
/*      */     label83:
/*      */     
/* 1244 */     if (!isValid) {
/* 1245 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_ORIGCURRCD);
/* 1246 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateAirSegInfo(TAAAirIndustryBean tAAAirIndusBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1264 */     ErrorObject errorObj = null;
/*      */     
/* 1266 */     if ((tAAAirIndusBean.getAirlineSegInfo() == null) || 
/* 1267 */       (tAAAirIndusBean.getAirlineSegInfo().isEmpty()))
/*      */     {
/* 1269 */       errorObj = new ErrorObject(
/* 1270 */         XMLSubmissionErrorCodes.ERROR_AIRLINESEGINFO);
/* 1271 */       errorCodes.add(errorObj);
/*      */     }
/*      */     else
/*      */     {
/* 1275 */       for (AirlineSegInfoBean airlineSegInfo : tAAAirIndusBean.getAirlineSegInfo())
/*      */       {
/* 1277 */         validateStopoverCd(airlineSegInfo, errorCodes);
/* 1278 */         validateDprtLocCd(airlineSegInfo, tAAAirIndusBean.getTravTransTypeCd(), errorCodes);
/* 1279 */         validateDprtDt(airlineSegInfo, tAAAirIndusBean.getTravTransTypeCd(), errorCodes);
/* 1280 */         validateArrLocCd(airlineSegInfo, tAAAirIndusBean.getTravTransTypeCd(), errorCodes);
/* 1281 */         validateSegIATACarrierCd(airlineSegInfo, tAAAirIndusBean.getTravTransTypeCd(), errorCodes);
/* 1282 */         validateFareBasisTxt(airlineSegInfo, tAAAirIndusBean.getTravTransTypeCd(), errorCodes);
/* 1283 */         validateServClassCd(airlineSegInfo, tAAAirIndusBean.getTravTransTypeCd(), errorCodes);
/* 1284 */         validateFlghtNbr(airlineSegInfo, tAAAirIndusBean.getTravTransTypeCd(), errorCodes);
/* 1285 */         validateSegTotFareAmt(airlineSegInfo, errorCodes, tAAAirIndusBean.getOrigCurrCd());
/*      */       }
/*      */     }
/*      */   }
/*      */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\validator\xml\industry\TAAAirIndusValidator.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */