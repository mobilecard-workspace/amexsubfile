/*     */ package com.americanexpress.ips.gfsg.validator.xml;
/*     */ 
/*     */ import com.americanexpress.ips.gfsg.beans.ErrorObject;
/*     */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.InvoiceMassageInfoBean;
/*     */ import com.americanexpress.ips.gfsg.enumerations.XMLSubmissionErrorCodes;
/*     */ import com.americanexpress.ips.gfsg.exceptions.SettlementException;
/*     */ import com.americanexpress.ips.gfsg.validator.CommonValidator;
/*     */ import java.util.List;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class InvoiceMsgInfoValidator
/*     */ {
/*     */   public static void validateInvoiceMsgInfo(InvoiceMassageInfoBean invoiceMassageInfoBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/*  36 */     validateInvoiceNumber(invoiceMassageInfoBean, errorCodes);
/*  37 */     validateReleaseNumber(invoiceMassageInfoBean, errorCodes);
/*  38 */     validateDescription1(invoiceMassageInfoBean, errorCodes);
/*  39 */     validateDescription2(invoiceMassageInfoBean, errorCodes);
/*  40 */     validateDescription3(invoiceMassageInfoBean, errorCodes);
/*  41 */     validateDescription4(invoiceMassageInfoBean, errorCodes);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateInvoiceNumber(InvoiceMassageInfoBean invoiceMassageInfoBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/*  58 */     ErrorObject errorObj = null;
/*  59 */     boolean isValid = true;
/*  60 */     if (CommonValidator.isNullOrEmpty(invoiceMassageInfoBean
/*  61 */       .getInvoiceNumber())) {
/*  62 */       isValid = false;
/*     */     }
/*  64 */     else if (CommonValidator.validateData(invoiceMassageInfoBean
/*  65 */       .getInvoiceNumber(), 
/*  66 */       com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE['º']))
/*     */     {
/*  68 */       String reqLength = CommonValidator.validateLength(
/*  69 */         invoiceMassageInfoBean.getInvoiceNumber(), 13, 1);
/*     */       
/*  71 */       if (reqLength.equals("greaterThanMax")) {
/*  72 */         isValid = false;
/*     */       }
/*     */     } else {
/*  75 */       isValid = false;
/*     */     }
/*     */     
/*     */ 
/*  79 */     if (!isValid)
/*     */     {
/*     */ 
/*  82 */       errorObj = new ErrorObject(
/*  83 */         XMLSubmissionErrorCodes.DCS_ERROR_INVALID_INVOICE_NBR);
/*  84 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateReleaseNumber(InvoiceMassageInfoBean invoiceMassageInfoBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 101 */     ErrorObject errorObj = null;
/* 102 */     boolean isValid = true;
/* 103 */     if (!CommonValidator.isNullOrEmpty(invoiceMassageInfoBean
/* 104 */       .getReleaseNumber()))
/*     */     {
/* 106 */       if (CommonValidator.validateData(invoiceMassageInfoBean
/* 107 */         .getReleaseNumber(), 
/* 108 */         com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE['»']))
/*     */       {
/* 110 */         String reqLength = CommonValidator.validateLength(
/* 111 */           invoiceMassageInfoBean.getReleaseNumber(), 30, 1);
/*     */         
/* 113 */         if (reqLength.equals("greaterThanMax")) {
/* 114 */           isValid = false;
/*     */         }
/*     */       } else {
/* 117 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/* 121 */     if (!isValid)
/*     */     {
/* 123 */       errorObj = new ErrorObject(
/* 124 */         XMLSubmissionErrorCodes.DCS_ERROR_INVALID_RELEASE_NBR);
/* 125 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateDescription1(InvoiceMassageInfoBean invoiceMassageInfoBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 143 */     ErrorObject errorObj = null;
/* 144 */     boolean isValid = true;
/* 145 */     if (CommonValidator.isNullOrEmpty(invoiceMassageInfoBean
/* 146 */       .getDescription1())) {
/* 147 */       isValid = false;
/*     */     }
/* 149 */     else if (CommonValidator.validateData(invoiceMassageInfoBean
/* 150 */       .getDescription1(), 
/* 151 */       com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE['»'])) {
/* 152 */       String reqLength = CommonValidator.validateLength(
/* 153 */         invoiceMassageInfoBean.getDescription1(), 40, 1);
/*     */       
/* 155 */       if (reqLength.equals("greaterThanMax")) {
/* 156 */         isValid = false;
/*     */       }
/*     */     } else {
/* 159 */       isValid = false;
/*     */     }
/*     */     
/*     */ 
/* 163 */     if (!isValid)
/*     */     {
/* 165 */       errorObj = new ErrorObject(
/* 166 */         XMLSubmissionErrorCodes.DCS_ERROR_DESCRIPTION);
/* 167 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateDescription2(InvoiceMassageInfoBean invoiceMassageInfoBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 184 */     ErrorObject errorObj = null;
/* 185 */     boolean isValid = true;
/* 186 */     if (!CommonValidator.isNullOrEmpty(invoiceMassageInfoBean
/* 187 */       .getDescription2()))
/*     */     {
/* 189 */       if (CommonValidator.validateData(invoiceMassageInfoBean
/* 190 */         .getDescription2(), 
/* 191 */         com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE['»']))
/*     */       {
/* 193 */         String reqLength = CommonValidator.validateLength(
/* 194 */           invoiceMassageInfoBean.getDescription2(), 40, 1);
/*     */         
/* 196 */         if (reqLength.equals("greaterThanMax")) {
/* 197 */           isValid = false;
/*     */         }
/*     */       } else {
/* 200 */         isValid = false;
/*     */       }
/*     */     }
/* 203 */     if (!isValid)
/*     */     {
/* 205 */       errorObj = new ErrorObject(
/* 206 */         XMLSubmissionErrorCodes.DCS_ERROR_DESCRIPTION);
/* 207 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateDescription3(InvoiceMassageInfoBean invoiceMassageInfoBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 224 */     ErrorObject errorObj = null;
/* 225 */     boolean isValid = true;
/* 226 */     if (!CommonValidator.isNullOrEmpty(invoiceMassageInfoBean
/* 227 */       .getDescription3()))
/*     */     {
/* 229 */       if (CommonValidator.validateData(invoiceMassageInfoBean
/* 230 */         .getDescription3(), 
/* 231 */         com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE['»']))
/*     */       {
/* 233 */         String reqLength = CommonValidator.validateLength(
/* 234 */           invoiceMassageInfoBean.getDescription3(), 40, 1);
/*     */         
/* 236 */         if (reqLength.equals("greaterThanMax")) {
/* 237 */           isValid = false;
/*     */         }
/*     */       } else {
/* 240 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/* 244 */     if (!isValid)
/*     */     {
/* 246 */       errorObj = new ErrorObject(
/* 247 */         XMLSubmissionErrorCodes.DCS_ERROR_DESCRIPTION);
/* 248 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateDescription4(InvoiceMassageInfoBean invoiceMassageInfoBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 265 */     ErrorObject errorObj = null;
/* 266 */     boolean isValid = true;
/* 267 */     if (!CommonValidator.isNullOrEmpty(invoiceMassageInfoBean
/* 268 */       .getDescription4()))
/*     */     {
/* 270 */       if (CommonValidator.validateData(invoiceMassageInfoBean
/* 271 */         .getDescription4(), 
/* 272 */         com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE['»']))
/*     */       {
/* 274 */         String reqLength = CommonValidator.validateLength(
/* 275 */           invoiceMassageInfoBean.getDescription4(), 40, 1);
/*     */         
/* 277 */         if (reqLength.equals("greaterThanMax")) {
/* 278 */           isValid = false;
/*     */         }
/*     */       } else {
/* 281 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/* 285 */     if (!isValid)
/*     */     {
/* 287 */       errorObj = new ErrorObject(
/* 288 */         XMLSubmissionErrorCodes.DCS_ERROR_DESCRIPTION);
/* 289 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\validator\xml\InvoiceMsgInfoValidator.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */