/*    */ package com.americanexpress.ips.gfsg.validator.xml;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class ModulusNineCheckValidator
/*    */ {
/*    */   public static boolean modulusNineCheck(String number)
/*    */   {
/* 23 */     boolean isValid = false;
/* 24 */     if (number.length() == 10) {
/* 25 */       int digits = number.length();
/* 26 */       long checkDigit = Integer.parseInt(number.charAt(digits - 1));
/* 27 */       long newSum = 0L;
/* 28 */       number = number.substring(0, digits - 1);
/* 29 */       int intfirstThreeDigits = Integer.parseInt(number.substring(0, 3));
/* 30 */       if ((intfirstThreeDigits < 930) || (intfirstThreeDigits > 939)) {
/* 31 */         String strDigits = number.substring(1, number.length());
/* 32 */         number = "0" + strDigits;
/*    */       }
/* 34 */       digits = number.length();
/* 35 */       long sum = 0L;
/* 36 */       long oddsum = 0L;
/* 37 */       for (int count = 0; count < digits; count++) {
/* 38 */         int digit = 0;
/* 39 */         if (count % 2 == 0) {
/* 40 */           digit = Integer.parseInt(number.charAt(count)) * 2;
/* 41 */           if (digit > 9) {
/* 42 */             digit -= 9;
/*    */           }
/* 44 */           sum += digit;
/*    */         }
/*    */         else {
/* 47 */           oddsum += Integer.parseInt(number.charAt(count));
/*    */         }
/*    */       }
/* 50 */       sum += oddsum;
/* 51 */       if ((sum % 10L == 0L) && (checkDigit == 0L)) {
/* 52 */         isValid = true;
/*    */       }
/* 54 */       if (sum % 10L != 0L) {
/* 55 */         newSum = sum + (10L - sum % 10L);
/* 56 */         if (newSum - sum == checkDigit) {
/* 57 */           isValid = true;
/*    */         }
/*    */       }
/*    */     }
/*    */     
/* 62 */     return isValid;
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\validator\xml\ModulusNineCheckValidator.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */