/*     */ package com.americanexpress.ips.gfsg.validator.xml.industry;
/*     */ 
/*     */ import com.americanexpress.ips.gfsg.beans.ErrorObject;
/*     */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.TAAInsuranceIndustryBean;
/*     */ import com.americanexpress.ips.gfsg.enumerations.XMLSubmissionErrorCodes;
/*     */ import com.americanexpress.ips.gfsg.exceptions.SettlementException;
/*     */ import com.americanexpress.ips.gfsg.validator.CommonValidator;
/*     */ import java.util.List;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class TAAInsrIndusValidator
/*     */ {
/*     */   public static void validateInsuranceIndustry(TAAInsuranceIndustryBean taaInsuranceIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/*  35 */     validateInsrPlcyNbr(taaInsuranceIndustryBean, errorCodes);
/*     */     
/*  37 */     validateInsrCovEndDt(taaInsuranceIndustryBean, errorCodes);
/*  38 */     validateInsrPlcyPremFreqTxt(taaInsuranceIndustryBean, errorCodes);
/*  39 */     validateAddInsrPlcyNo(taaInsuranceIndustryBean, errorCodes);
/*  40 */     validatePlcyTypeTxt(taaInsuranceIndustryBean, errorCodes);
/*  41 */     validateInsrNm(taaInsuranceIndustryBean, errorCodes);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateInsrPlcyNbr(TAAInsuranceIndustryBean taaInsuranceIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/*  58 */     ErrorObject errorObj = null;
/*  59 */     boolean isValid = true;
/*  60 */     if (CommonValidator.isNullOrEmpty(taaInsuranceIndustryBean
/*  61 */       .getInsrPlcyNbr())) {
/*  62 */       isValid = false;
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/*  69 */       String reqLength = CommonValidator.validateLength(
/*  70 */         taaInsuranceIndustryBean.getInsrPlcyNbr(), 23, 1);
/*     */       
/*  72 */       if (reqLength.equals("greaterThanMax")) {
/*  73 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*  80 */     if (!isValid)
/*     */     {
/*     */ 
/*  83 */       errorObj = new ErrorObject(
/*  84 */         XMLSubmissionErrorCodes.ERROR_INSRPLCYNBR);
/*  85 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static boolean validateInsrCovStartDt(String covStartDt)
/*     */     throws SettlementException
/*     */   {
/* 100 */     boolean isValid = true;
/* 101 */     if (CommonValidator.isNullOrEmpty(covStartDt)) {
/* 102 */       isValid = false;
/*     */ 
/*     */     }
/* 105 */     else if (CommonValidator.validateData(covStartDt, 
/* 106 */       com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE['']))
/*     */     {
/* 108 */       if (!CommonValidator.isValidXmlDate(covStartDt, 
/* 109 */         "CCYYMMDD")) {
/* 110 */         isValid = false;
/*     */       }
/*     */     }
/*     */     else {
/* 114 */       isValid = false;
/*     */     }
/*     */     
/*     */ 
/*     */ 
/* 119 */     return isValid;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateInsrCovEndDt(TAAInsuranceIndustryBean taaInsuranceIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 134 */     ErrorObject errorObj = null;
/* 135 */     boolean isValid = true;
/* 136 */     boolean startDate = true;
/* 137 */     startDate = validateInsrCovStartDt(taaInsuranceIndustryBean
/* 138 */       .getInsrCovStartDt());
/* 139 */     if (CommonValidator.isNullOrEmpty(taaInsuranceIndustryBean
/* 140 */       .getInsrCovEndDt())) {
/* 141 */       isValid = false;
/*     */ 
/*     */ 
/*     */     }
/* 145 */     else if (CommonValidator.validateData(taaInsuranceIndustryBean
/* 146 */       .getInsrCovEndDt(), 
/* 147 */       com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[''])) {
/* 148 */       if (CommonValidator.isValidXmlDate(taaInsuranceIndustryBean
/* 149 */         .getInsrCovEndDt(), 
/* 150 */         "CCYYMMDD")) {
/* 151 */         if (startDate) {
/* 152 */           int insrCovEndDt = 
/* 153 */             Integer.parseInt(taaInsuranceIndustryBean
/* 154 */             .getInsrCovEndDt());
/* 155 */           int insrCovStartDt = 
/* 156 */             Integer.parseInt(taaInsuranceIndustryBean
/* 157 */             .getInsrCovStartDt());
/* 158 */           if (insrCovEndDt < insrCovStartDt) {
/* 159 */             isValid = false;
/*     */           }
/*     */         }
/*     */       } else {
/* 163 */         isValid = false;
/*     */       }
/*     */     }
/*     */     else {
/* 167 */       isValid = false;
/*     */     }
/*     */     
/*     */ 
/* 171 */     if (!isValid)
/*     */     {
/* 173 */       errorObj = new ErrorObject(
/* 174 */         XMLSubmissionErrorCodes.ERROR_INSRCOVENDDT);
/* 175 */       errorCodes.add(errorObj);
/*     */     }
/*     */     
/*     */ 
/* 179 */     if (!startDate)
/*     */     {
/* 181 */       errorObj = new ErrorObject(
/* 182 */         XMLSubmissionErrorCodes.ERROR_INSRCOVSTARTDT);
/* 183 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateInsrPlcyPremFreqTxt(TAAInsuranceIndustryBean taaInsuranceIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 202 */     ErrorObject errorObj = null;
/* 203 */     boolean isValid = true;
/* 204 */     if (!CommonValidator.isNullOrEmpty(taaInsuranceIndustryBean
/* 205 */       .getInsrPlcyPremFreqTxt()))
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 212 */       String reqLength = 
/* 213 */         CommonValidator.validateLength(taaInsuranceIndustryBean
/* 214 */         .getInsrPlcyPremFreqTxt(), 7, 1);
/*     */       
/* 216 */       if (reqLength.equals("greaterThanMax")) {
/* 217 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 224 */     if (!isValid)
/*     */     {
/* 226 */       errorObj = new ErrorObject(
/* 227 */         XMLSubmissionErrorCodes.ERROR_INSRPLCYPREMFREQTXT);
/* 228 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateAddInsrPlcyNo(TAAInsuranceIndustryBean taaInsuranceIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 246 */     ErrorObject errorObj = null;
/* 247 */     boolean isValid = true;
/* 248 */     if (!CommonValidator.isNullOrEmpty(taaInsuranceIndustryBean
/* 249 */       .getAddInsrPlcyNo()))
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 256 */       String reqLength = CommonValidator.validateLength(
/* 257 */         taaInsuranceIndustryBean.getAddInsrPlcyNo(), 23, 1);
/*     */       
/* 259 */       if (reqLength.equals("greaterThanMax")) {
/* 260 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 267 */     if (!isValid)
/*     */     {
/* 269 */       errorObj = new ErrorObject(
/* 270 */         XMLSubmissionErrorCodes.ERROR_ADDINSRPLCYNO);
/* 271 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validatePlcyTypeTxt(TAAInsuranceIndustryBean taaInsuranceIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 289 */     ErrorObject errorObj = null;
/* 290 */     boolean isValid = true;
/* 291 */     if (CommonValidator.isNullOrEmpty(taaInsuranceIndustryBean
/* 292 */       .getPlcyTypeTxt())) {
/* 293 */       isValid = false;
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/*     */ 
/* 301 */       String reqLength = CommonValidator.validateLength(
/* 302 */         taaInsuranceIndustryBean.getPlcyTypeTxt(), 25, 1);
/*     */       
/* 304 */       if (reqLength.equals("greaterThanMax")) {
/* 305 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 313 */     if (!isValid)
/*     */     {
/* 315 */       errorObj = new ErrorObject(
/* 316 */         XMLSubmissionErrorCodes.ERROR_PLCYTYPETXT);
/* 317 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateInsrNm(TAAInsuranceIndustryBean taaInsuranceIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 335 */     ErrorObject errorObj = null;
/* 336 */     boolean isValid = true;
/*     */     
/* 338 */     if (!CommonValidator.isNullOrEmpty(taaInsuranceIndustryBean.getInsrNm()))
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 344 */       String reqLength = CommonValidator.validateLength(
/* 345 */         taaInsuranceIndustryBean.getInsrNm(), 30, 1);
/*     */       
/* 347 */       if (reqLength.equals("greaterThanMax")) {
/* 348 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 355 */     if (!isValid)
/*     */     {
/* 357 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_INSRNM);
/* 358 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\validator\xml\industry\TAAInsrIndusValidator.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */