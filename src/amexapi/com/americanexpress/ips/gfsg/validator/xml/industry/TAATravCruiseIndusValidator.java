/*     */ package com.americanexpress.ips.gfsg.validator.xml.industry;
/*     */ 
/*     */ import com.americanexpress.ips.gfsg.beans.ErrorObject;
/*     */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.TAATravCruiseIndustryBean;
/*     */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.TravelSegInfoBean;
/*     */ import com.americanexpress.ips.gfsg.enumerations.XMLSubmissionErrorCodes;
/*     */ import com.americanexpress.ips.gfsg.exceptions.SettlementException;
/*     */ import com.americanexpress.ips.gfsg.validator.CommonValidator;
/*     */ import java.util.List;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class TAATravCruiseIndusValidator
/*     */ {
/*     */   public static void validateTravelCruiseIndustry(TAATravCruiseIndustryBean taaTravCruiseIndustryBean, List<ErrorObject> errorCodes, String transCurrCd)
/*     */     throws SettlementException
/*     */   {
/*  37 */     validateIATACarrierCd(taaTravCruiseIndustryBean, errorCodes);
/*  38 */     validateIATAAgcyNbr(taaTravCruiseIndustryBean, errorCodes);
/*  39 */     validateTravPackTypeCd(taaTravCruiseIndustryBean, errorCodes);
/*  40 */     validateTktNbr(taaTravCruiseIndustryBean, errorCodes);
/*  41 */     validatePassNm(taaTravCruiseIndustryBean, errorCodes);
/*     */     
/*  43 */     validateLdgCheckOutDt(taaTravCruiseIndustryBean, errorCodes);
/*  44 */     validateLdgRmRate(taaTravCruiseIndustryBean, errorCodes, transCurrCd);
/*  45 */     validateNbrOfNgtCnt(taaTravCruiseIndustryBean, errorCodes);
/*  46 */     validateLdgNm(taaTravCruiseIndustryBean, errorCodes);
/*  47 */     validateLdgCityNm(taaTravCruiseIndustryBean, errorCodes);
/*  48 */     validateLdgRgnCd(taaTravCruiseIndustryBean, errorCodes);
/*  49 */     validateLdgCtryCd(taaTravCruiseIndustryBean, errorCodes);
/*  50 */     validateTravelSegInfo(taaTravCruiseIndustryBean, errorCodes);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateIATACarrierCd(TAATravCruiseIndustryBean taaTravCruiseIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/*  66 */     ErrorObject errorObj = null;
/*  67 */     boolean isValid = true;
/*  68 */     if (!CommonValidator.isNullOrEmpty(taaTravCruiseIndustryBean.getIATACarrierCd()))
/*     */     {
/*     */ 
/*  71 */       if (CommonValidator.validateData(taaTravCruiseIndustryBean.getIATACarrierCd(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE['']))
/*     */       {
/*  73 */         String reqLength = CommonValidator.validateLength(
/*  74 */           taaTravCruiseIndustryBean.getIATACarrierCd(), 3, 1);
/*     */         
/*  76 */         if (reqLength.equals("greaterThanMax")) {
/*  77 */           isValid = false;
/*     */         }
/*     */       }
/*     */       else {
/*  81 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*  86 */     if (!isValid)
/*     */     {
/*     */ 
/*  89 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_RETAILIATACARRIERCD);
/*  90 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateIATAAgcyNbr(TAATravCruiseIndustryBean taaTravCruiseIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 108 */     ErrorObject errorObj = null;
/* 109 */     boolean isValid = true;
/* 110 */     if (!CommonValidator.isNullOrEmpty(taaTravCruiseIndustryBean.getIATAAgcyNbr()))
/*     */     {
/*     */ 
/* 113 */       if (CommonValidator.validateData(taaTravCruiseIndustryBean.getIATAAgcyNbr(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE['']))
/*     */       {
/* 115 */         String reqLength = CommonValidator.validateLength(
/* 116 */           taaTravCruiseIndustryBean.getIATAAgcyNbr(), 8, 1);
/*     */         
/* 118 */         if (reqLength.equals("greaterThanMax")) {
/* 119 */           isValid = false;
/*     */         }
/*     */       } else {
/* 122 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/* 126 */     if (!isValid)
/*     */     {
/* 128 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_IATAAGCYNBR);
/* 129 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateTravPackTypeCd(TAATravCruiseIndustryBean taaTravCruiseIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 149 */     ErrorObject errorObj = null;
/* 150 */     boolean isValid = true;
/* 151 */     if (CommonValidator.isNullOrEmpty(taaTravCruiseIndustryBean.getTravPackTypeCd())) {
/* 152 */       isValid = false;
/*     */     }
/* 154 */     else if ((CommonValidator.validateData(taaTravCruiseIndustryBean.getTravPackTypeCd(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[' '])) && (CommonValidator.isValidTravPackTypeCd(taaTravCruiseIndustryBean.getTravPackTypeCd())))
/*     */     {
/* 156 */       String reqLength = CommonValidator.validateLength(
/* 157 */         taaTravCruiseIndustryBean.getTravPackTypeCd(), 1, 1);
/*     */       
/* 159 */       if (reqLength.equals("greaterThanMax")) {
/* 160 */         isValid = false;
/*     */       }
/*     */     } else {
/* 163 */       isValid = false;
/*     */     }
/*     */     
/*     */ 
/* 167 */     if (!isValid) {
/* 168 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_TRAVPACKTYPECD);
/* 169 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateTktNbr(TAATravCruiseIndustryBean taaTravCruiseIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 186 */     ErrorObject errorObj = null;
/* 187 */     boolean isValid = true;
/* 188 */     if (!CommonValidator.isNullOrEmpty(taaTravCruiseIndustryBean.getTktNbr()))
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/* 193 */       String reqLength = CommonValidator.validateLength(
/* 194 */         taaTravCruiseIndustryBean.getTktNbr(), 14, 1);
/*     */       
/* 196 */       if (reqLength.equals("greaterThanMax")) {
/* 197 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 204 */     if (!isValid)
/*     */     {
/* 206 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_TKTNBR);
/* 207 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validatePassNm(TAATravCruiseIndustryBean taaTravCruiseIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 226 */     ErrorObject errorObj = null;
/* 227 */     boolean isValid = true;
/* 228 */     if (!CommonValidator.isNullOrEmpty(taaTravCruiseIndustryBean.getPassNm()))
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/* 233 */       String reqLength = CommonValidator.validateLength(
/* 234 */         taaTravCruiseIndustryBean.getPassNm(), 25, 1);
/*     */       
/* 236 */       if (reqLength.equals("greaterThanMax")) {
/* 237 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 245 */     if (!isValid)
/*     */     {
/* 247 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_PASSNM);
/* 248 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static boolean validateLdgCheckInDt(String checkInDt)
/*     */     throws SettlementException
/*     */   {
/* 266 */     boolean isValid = true;
/* 267 */     if (!CommonValidator.isNullOrEmpty(checkInDt))
/*     */     {
/* 269 */       if (CommonValidator.validateData(checkInDt, 
/* 270 */         com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE['£']))
/*     */       {
/* 272 */         if (!CommonValidator.isValidXmlDate(checkInDt, "CCYYMMDD")) {
/* 273 */           isValid = false;
/*     */         }
/*     */       }
/*     */       else {
/* 277 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 285 */     return isValid;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateLdgCheckOutDt(TAATravCruiseIndustryBean taaTravCruiseIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 302 */     ErrorObject errorObj = null;
/* 303 */     boolean isValid = true;
/* 304 */     boolean startDate = true;
/* 305 */     startDate = validateLdgCheckInDt(taaTravCruiseIndustryBean.getLdgCheckInDt());
/* 306 */     if (!CommonValidator.isNullOrEmpty(taaTravCruiseIndustryBean
/* 307 */       .getLdgCheckOutDt()))
/*     */     {
/*     */ 
/* 310 */       if (CommonValidator.validateData(taaTravCruiseIndustryBean
/* 311 */         .getLdgCheckOutDt(), 
/* 312 */         com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE['¤'])) {
/* 313 */         if (CommonValidator.isValidXmlDate(taaTravCruiseIndustryBean.getLdgCheckOutDt(), "CCYYMMDD")) {
/* 314 */           if ((startDate) && (!CommonValidator.isNullOrEmpty(taaTravCruiseIndustryBean.getLdgCheckInDt()))) {
/* 315 */             int ldgCheckInDt = Integer.parseInt(taaTravCruiseIndustryBean.getLdgCheckInDt());
/* 316 */             int ldgCheckOutDt = Integer.parseInt(taaTravCruiseIndustryBean.getLdgCheckOutDt());
/* 317 */             if (ldgCheckOutDt < ldgCheckInDt) {
/* 318 */               isValid = false;
/*     */             }
/*     */           }
/*     */         } else {
/* 322 */           isValid = false;
/*     */         }
/*     */       }
/*     */       else {
/* 326 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/* 330 */     if (!isValid)
/*     */     {
/* 332 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_LDGCHECKOUTDT);
/* 333 */       errorCodes.add(errorObj);
/*     */     }
/*     */     
/*     */ 
/* 337 */     if (!startDate)
/*     */     {
/*     */ 
/* 340 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_LDGCHECKINDT);
/* 341 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateLdgRmRate(TAATravCruiseIndustryBean taaTravCruiseIndustryBean, List<ErrorObject> errorCodes, String transCurrCd)
/*     */     throws SettlementException
/*     */   {
/* 360 */     ErrorObject errorObj = null;
/* 361 */     boolean isValid = true;
/* 362 */     if (!CommonValidator.isNullOrEmpty(taaTravCruiseIndustryBean.getLdgRmRate()))
/*     */     {
/*     */ 
/* 365 */       if (CommonValidator.validateData(taaTravCruiseIndustryBean.getLdgRmRate(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE['¥']))
/*     */       {
/* 367 */         String reqLength = CommonValidator.validateLength(
/* 368 */           taaTravCruiseIndustryBean.getLdgRmRate(), 12, 1);
/*     */         
/* 370 */         if (reqLength.equals("greaterThanMax")) {
/* 371 */           isValid = false;
/*     */         }
/* 373 */         else if ((!CommonValidator.isNullOrEmpty(transCurrCd)) && 
/* 374 */           (!CommonValidator.isValidXmlAmount(transCurrCd, taaTravCruiseIndustryBean.getLdgRmRate()))) {
/* 375 */           isValid = false;
/*     */         }
/*     */       }
/*     */       else
/*     */       {
/* 380 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/* 384 */     if (!isValid)
/*     */     {
/* 386 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_LDGRMRATE);
/* 387 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateNbrOfNgtCnt(TAATravCruiseIndustryBean taaTravCruiseIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 406 */     ErrorObject errorObj = null;
/* 407 */     boolean isValid = true;
/* 408 */     if (!CommonValidator.isNullOrEmpty(taaTravCruiseIndustryBean.getNbrOfNgtCnt()))
/*     */     {
/* 410 */       if (CommonValidator.validateData(taaTravCruiseIndustryBean.getNbrOfNgtCnt(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE['¦']))
/*     */       {
/* 412 */         String reqLength = CommonValidator.validateLength(
/* 413 */           taaTravCruiseIndustryBean.getNbrOfNgtCnt(), 2, 1);
/*     */         
/* 415 */         if (reqLength.equals("greaterThanMax")) {
/* 416 */           isValid = false;
/*     */         }
/*     */       } else {
/* 419 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/* 423 */     if (!isValid)
/*     */     {
/* 425 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_NBROFNGTCNT);
/* 426 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateLdgNm(TAATravCruiseIndustryBean taaTravCruiseIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 445 */     ErrorObject errorObj = null;
/* 446 */     boolean isValid = true;
/* 447 */     if (!CommonValidator.isNullOrEmpty(taaTravCruiseIndustryBean.getLdgNm()))
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/* 452 */       String reqLength = CommonValidator.validateLength(
/* 453 */         taaTravCruiseIndustryBean.getLdgNm(), 20, 1);
/*     */       
/* 455 */       if (reqLength.equals("greaterThanMax")) {
/* 456 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 464 */     if (!isValid)
/*     */     {
/* 466 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_LDGNM);
/* 467 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateLdgCityNm(TAATravCruiseIndustryBean taaTravCruiseIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 485 */     ErrorObject errorObj = null;
/* 486 */     boolean isValid = true;
/* 487 */     if (!CommonValidator.isNullOrEmpty(taaTravCruiseIndustryBean.getLdgCityNm()))
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/* 492 */       String reqLength = CommonValidator.validateLength(
/* 493 */         taaTravCruiseIndustryBean.getLdgCityNm(), 18, 1);
/*     */       
/* 495 */       if (reqLength.equals("greaterThanMax")) {
/* 496 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 505 */     if (!isValid)
/*     */     {
/* 507 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_LdgCityNm);
/* 508 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateLdgRgnCd(TAATravCruiseIndustryBean taaTravCruiseIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 527 */     ErrorObject errorObj = null;
/* 528 */     boolean isValid = true;
/* 529 */     if (CommonValidator.isNullOrEmpty(taaTravCruiseIndustryBean.getLdgRgnCd())) {
/* 530 */       if (!CommonValidator.isNullOrEmpty(taaTravCruiseIndustryBean.getLdgCtryCd())) {
/* 531 */         isValid = false;
/*     */       }
/*     */       
/*     */     }
/* 535 */     else if (CommonValidator.validateData(taaTravCruiseIndustryBean.getLdgRgnCd(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE['©'])) {
/* 536 */       if (!CommonValidator.isNullOrEmpty(taaTravCruiseIndustryBean.getLdgCtryCd())) {
/* 537 */         if (CommonValidator.isValidRegionCode(taaTravCruiseIndustryBean.getLdgRgnCd(), taaTravCruiseIndustryBean.getLdgCtryCd()))
/*     */         {
/* 539 */           String reqLength = CommonValidator.validateLength(
/* 540 */             taaTravCruiseIndustryBean.getLdgRgnCd(), 3, 3);
/*     */           
/* 542 */           if (reqLength.equals("greaterThanMax")) {
/* 543 */             isValid = false;
/*     */           }
/*     */         }
/*     */         else {
/* 547 */           isValid = false;
/*     */         }
/*     */       } else {
/* 550 */         isValid = false;
/*     */       }
/*     */     }
/*     */     else {
/* 554 */       isValid = false;
/*     */     }
/*     */     
/*     */ 
/*     */ 
/* 559 */     if (!isValid)
/*     */     {
/* 561 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_LDGRGNCD);
/* 562 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateLdgCtryCd(TAATravCruiseIndustryBean taaTravCruiseIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 580 */     boolean isValid = true;
/* 581 */     ErrorObject errorObj = null;
/* 582 */     if (CommonValidator.isNullOrEmpty(taaTravCruiseIndustryBean.getLdgCtryCd()))
/*     */     {
/* 584 */       if (!CommonValidator.isNullOrEmpty(taaTravCruiseIndustryBean.getLdgRgnCd()))
/*     */       {
/* 586 */         isValid = false;
/*     */       }
/*     */       
/*     */     }
/* 590 */     else if ((!CommonValidator.validateData(taaTravCruiseIndustryBean.getLdgCtryCd(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE['ª'])) || (!CommonValidator.isValidXmlCountryCode(taaTravCruiseIndustryBean.getLdgCtryCd())))
/*     */     {
/*     */ 
/* 593 */       isValid = false;
/*     */     }
/*     */     
/*     */ 
/*     */ 
/* 598 */     if (!isValid)
/*     */     {
/* 600 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_LDGCTRYCD);
/* 601 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateDprtDt(TravelSegInfoBean travelSegInfoBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 619 */     boolean isValid = true;
/* 620 */     ErrorObject errorObj = null;
/* 621 */     if (!CommonValidator.isNullOrEmpty(travelSegInfoBean.getDprtDt()))
/*     */     {
/*     */ 
/* 624 */       if (CommonValidator.validateData(travelSegInfoBean.getDprtDt(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE['«']))
/*     */       {
/* 626 */         if (!CommonValidator.isValidXmlDate(travelSegInfoBean.getDprtDt(), "CCYYMMDD")) {
/* 627 */           isValid = false;
/*     */         }
/*     */       } else {
/* 630 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/* 636 */     if (!isValid)
/*     */     {
/* 638 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_DPRTDT);
/* 639 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateDestCd(TravelSegInfoBean travelSegInfoBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 655 */     boolean isValid = true;
/* 656 */     ErrorObject errorObj = null;
/* 657 */     if (!CommonValidator.isNullOrEmpty(travelSegInfoBean.getDestCd()))
/*     */     {
/*     */ 
/* 660 */       if (CommonValidator.validateData(travelSegInfoBean.getDestCd(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE['¬']))
/*     */       {
/* 662 */         String reqLength = CommonValidator.validateLength(
/* 663 */           travelSegInfoBean.getDestCd(), 3, 3);
/*     */         
/* 665 */         if (reqLength.equals("greaterThanMax")) {
/* 666 */           isValid = false;
/*     */         }
/*     */       } else {
/* 669 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 674 */     if (!isValid)
/*     */     {
/* 676 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_DESTCD);
/* 677 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateDprtAirCd(TravelSegInfoBean travelSegInfoBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 694 */     boolean isValid = true;
/* 695 */     ErrorObject errorObj = null;
/* 696 */     if (!CommonValidator.isNullOrEmpty(travelSegInfoBean.getDprtAirCd()))
/*     */     {
/*     */ 
/* 699 */       if (CommonValidator.validateData(travelSegInfoBean.getDprtAirCd(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE['­']))
/*     */       {
/* 701 */         String reqLength = CommonValidator.validateLength(
/* 702 */           travelSegInfoBean.getDprtAirCd(), 3, 3);
/*     */         
/* 704 */         if (reqLength.equals("greaterThanMax")) {
/* 705 */           isValid = false;
/*     */         }
/*     */       }
/*     */       else {
/* 709 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 714 */     if (!isValid)
/*     */     {
/* 716 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_DPRTAIRCD);
/* 717 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateSegIATACarrierCd(TravelSegInfoBean travelSegInfoBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 736 */     boolean isValid = true;
/* 737 */     ErrorObject errorObj = null;
/* 738 */     if (!CommonValidator.isNullOrEmpty(travelSegInfoBean.getSegIATACarrierCd()))
/*     */     {
/*     */ 
/* 741 */       if (CommonValidator.validateData(travelSegInfoBean.getSegIATACarrierCd(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE['®']))
/*     */       {
/* 743 */         String reqLength = CommonValidator.validateLength(
/* 744 */           travelSegInfoBean.getSegIATACarrierCd(), 2, 2);
/*     */         
/* 746 */         if (reqLength.equals("greaterThanMax")) {
/* 747 */           isValid = false;
/*     */         }
/*     */       }
/*     */       else {
/* 751 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 756 */     if (!isValid)
/*     */     {
/* 758 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_SEGIATACARRIERCD);
/* 759 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateFlghtNbr(TravelSegInfoBean travelSegInfoBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 775 */     boolean isValid = true;
/* 776 */     ErrorObject errorObj = null;
/* 777 */     if (!CommonValidator.isNullOrEmpty(travelSegInfoBean.getFlghtNbr()))
/*     */     {
/*     */ 
/* 780 */       if (CommonValidator.validateData(travelSegInfoBean.getFlghtNbr(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE['¯']))
/*     */       {
/* 782 */         String reqLength = CommonValidator.validateLength(
/* 783 */           travelSegInfoBean.getFlghtNbr(), 4, 1);
/*     */         
/* 785 */         if (reqLength.equals("greaterThanMax")) {
/* 786 */           isValid = false;
/*     */         }
/*     */       } else {
/* 789 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/* 793 */     if (!isValid)
/*     */     {
/* 795 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_FLGHTNBR);
/* 796 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateServClassCd(TravelSegInfoBean travelSegInfoBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 812 */     boolean isValid = true;
/* 813 */     ErrorObject errorObj = null;
/* 814 */     if (!CommonValidator.isNullOrEmpty(travelSegInfoBean.getServClassCd()))
/*     */     {
/*     */ 
/* 817 */       if (CommonValidator.validateData(travelSegInfoBean.getServClassCd(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE['°']))
/*     */       {
/* 819 */         String reqLength = CommonValidator.validateLength(
/* 820 */           travelSegInfoBean.getServClassCd(), 1, 1);
/*     */         
/* 822 */         if (reqLength.equals("greaterThanMax")) {
/* 823 */           isValid = false;
/*     */         }
/*     */       } else {
/* 826 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/* 830 */     if (!isValid)
/*     */     {
/* 832 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_SERVCLASSCD);
/* 833 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateTravelSegInfo(TAATravCruiseIndustryBean taaTravCruiseIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 852 */     if ((taaTravCruiseIndustryBean.getTravelSegInfoBean() != null) && (!taaTravCruiseIndustryBean.getTravelSegInfoBean().isEmpty())) {
/* 853 */       for (TravelSegInfoBean travelSegInfo : taaTravCruiseIndustryBean.getTravelSegInfoBean())
/*     */       {
/* 855 */         validateDprtDt(travelSegInfo, errorCodes);
/* 856 */         validateDestCd(travelSegInfo, errorCodes);
/* 857 */         validateDprtAirCd(travelSegInfo, errorCodes);
/* 858 */         validateSegIATACarrierCd(travelSegInfo, errorCodes);
/* 859 */         validateFlghtNbr(travelSegInfo, errorCodes);
/* 860 */         validateServClassCd(travelSegInfo, errorCodes);
/*     */       }
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\validator\xml\industry\TAATravCruiseIndusValidator.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */