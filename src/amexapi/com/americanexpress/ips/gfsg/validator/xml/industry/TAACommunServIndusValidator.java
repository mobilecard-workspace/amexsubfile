/*     */ package com.americanexpress.ips.gfsg.validator.xml.industry;
/*     */ 
/*     */ import com.americanexpress.ips.gfsg.beans.ErrorObject;
/*     */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.TAACommunServIndustryBean;
/*     */ import com.americanexpress.ips.gfsg.enumerations.XMLSubmissionErrorCodes;
/*     */ import com.americanexpress.ips.gfsg.exceptions.SettlementException;
/*     */ import com.americanexpress.ips.gfsg.validator.CommonValidator;
/*     */ import java.util.List;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class TAACommunServIndusValidator
/*     */ {
/*     */   public static void validateCommunicationServiceIndustry(TAACommunServIndustryBean taaCommunServIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/*  34 */     validateCallDt(taaCommunServIndustryBean, errorCodes);
/*  35 */     validateCallTm(taaCommunServIndustryBean, errorCodes);
/*  36 */     validateCallDurTm(taaCommunServIndustryBean, errorCodes);
/*  37 */     validateCallFromLocNm(taaCommunServIndustryBean, errorCodes);
/*  38 */     validateCallFromRgnCd(taaCommunServIndustryBean, errorCodes);
/*  39 */     validateCallFromCtryCd(taaCommunServIndustryBean, errorCodes);
/*  40 */     validateCallFromPhoneNbr(taaCommunServIndustryBean, errorCodes);
/*  41 */     validateCallToLocNm(taaCommunServIndustryBean, errorCodes);
/*  42 */     validateCallToRgnCd(taaCommunServIndustryBean, errorCodes);
/*  43 */     validateCallToCtryCd(taaCommunServIndustryBean, errorCodes);
/*  44 */     validateCallToPhoneNbr(taaCommunServIndustryBean, errorCodes);
/*  45 */     validateCallingCardId(taaCommunServIndustryBean, errorCodes);
/*  46 */     validateServTypeDescTxt(taaCommunServIndustryBean, errorCodes);
/*  47 */     validateBillPerTxt(taaCommunServIndustryBean, errorCodes);
/*  48 */     validateCallTypeCd(taaCommunServIndustryBean, errorCodes);
/*  49 */     validateRateClassCd(taaCommunServIndustryBean, errorCodes);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateCallDt(TAACommunServIndustryBean taaCommunServIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/*  69 */     ErrorObject errorObj = null;
/*  70 */     boolean isValid = true;
/*  71 */     if (CommonValidator.isNullOrEmpty(taaCommunServIndustryBean.getCallDt())) {
/*  72 */       isValid = false;
/*     */ 
/*     */     }
/*  75 */     else if (CommonValidator.validateData(taaCommunServIndustryBean
/*  76 */       .getCallDt(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[108]))
/*     */     {
/*  78 */       isValid = CommonValidator.isValidXmlDate(taaCommunServIndustryBean.getCallDt(), "CCYYMMDD");
/*     */     }
/*     */     else {
/*  81 */       isValid = false;
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*  87 */     if (!isValid)
/*     */     {
/*     */ 
/*  90 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_CALLDT);
/*  91 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateCallTm(TAACommunServIndustryBean taaCommunServIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 109 */     ErrorObject errorObj = null;
/* 110 */     boolean isValid = true;
/* 111 */     if (!CommonValidator.isNullOrEmpty(taaCommunServIndustryBean.getCallTm()))
/*     */     {
/*     */ 
/* 114 */       if (CommonValidator.validateData(taaCommunServIndustryBean
/* 115 */         .getCallTm(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[109])) {
/* 116 */         if (!CommonValidator.validateTime(taaCommunServIndustryBean.getCallTm())) {
/* 117 */           isValid = false;
/*     */         }
/*     */       } else {
/* 120 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/* 126 */     if (!isValid) {
/* 127 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_CALLTM);
/* 128 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateCallDurTm(TAACommunServIndustryBean taaCommunServIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 145 */     ErrorObject errorObj = null;
/* 146 */     boolean isValid = true;
/* 147 */     if (CommonValidator.isNullOrEmpty(taaCommunServIndustryBean.getCallDurTm())) {
/* 148 */       isValid = false;
/*     */     }
/* 150 */     else if (CommonValidator.validateData(taaCommunServIndustryBean
/* 151 */       .getCallDurTm(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[110]))
/*     */     {
/* 153 */       if (!CommonValidator.validateTime(taaCommunServIndustryBean.getCallDurTm())) {
/* 154 */         isValid = false;
/*     */       }
/*     */     } else {
/* 157 */       isValid = false;
/*     */     }
/*     */     
/*     */ 
/* 161 */     if (!isValid)
/*     */     {
/* 163 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_CallDurTm);
/* 164 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateCallFromLocNm(TAACommunServIndustryBean taaCommunServIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 182 */     ErrorObject errorObj = null;
/* 183 */     boolean isValid = true;
/* 184 */     if (CommonValidator.isNullOrEmpty(taaCommunServIndustryBean.getCallFromLocNm())) {
/* 185 */       isValid = false;
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/* 191 */       String reqLength = CommonValidator.validateLength(
/* 192 */         taaCommunServIndustryBean.getCallFromLocNm(), 18, 1);
/*     */       
/* 194 */       if (reqLength.equals("greaterThanMax")) {
/* 195 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 202 */     if (!isValid)
/*     */     {
/* 204 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_CALLFROMLOCNM);
/* 205 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateCallFromRgnCd(TAACommunServIndustryBean taaCommunServIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 224 */     ErrorObject errorObj = null;
/* 225 */     boolean isValid = true;
/* 226 */     if (CommonValidator.isNullOrEmpty(taaCommunServIndustryBean.getCallFromRgnCd()))
/*     */     {
/*     */ 
/* 229 */       if (!CommonValidator.isNullOrEmpty(taaCommunServIndustryBean.getCallFromCtryCd()))
/*     */       {
/* 231 */         isValid = false;
/*     */       }
/*     */       
/*     */ 
/*     */     }
/* 236 */     else if (CommonValidator.validateData(taaCommunServIndustryBean
/* 237 */       .getCallFromRgnCd(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[112]))
/*     */     {
/* 239 */       if (!CommonValidator.isNullOrEmpty(taaCommunServIndustryBean.getCallFromCtryCd()))
/*     */       {
/*     */ 
/* 242 */         if (!CommonValidator.isValidRegionCode(taaCommunServIndustryBean.getCallFromRgnCd(), taaCommunServIndustryBean.getCallFromCtryCd()))
/*     */         {
/* 244 */           isValid = false;
/*     */         }
/*     */         
/*     */ 
/*     */       }
/*     */       else
/*     */       {
/* 251 */         isValid = false;
/*     */       }
/*     */       
/*     */ 
/*     */     }
/*     */     else {
/* 257 */       isValid = false;
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 265 */     if (!isValid)
/*     */     {
/* 267 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_CallFromRgnCd);
/* 268 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateCallFromCtryCd(TAACommunServIndustryBean taaCommunServIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 287 */     ErrorObject errorObj = null;
/* 288 */     boolean isValid = true;
/* 289 */     if (CommonValidator.isNullOrEmpty(taaCommunServIndustryBean.getCallFromCtryCd()))
/*     */     {
/*     */ 
/* 292 */       if (!CommonValidator.isNullOrEmpty(taaCommunServIndustryBean.getCallFromRgnCd()))
/*     */       {
/* 294 */         isValid = false;
/*     */       }
/*     */       
/*     */ 
/*     */     }
/* 299 */     else if ((CommonValidator.validateData(taaCommunServIndustryBean
/* 300 */       .getCallFromCtryCd(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[113])) && (CommonValidator.isValidXmlCountryCode(taaCommunServIndustryBean
/* 301 */       .getCallFromCtryCd())))
/*     */     {
/* 303 */       String reqLength = CommonValidator.validateLength(
/* 304 */         taaCommunServIndustryBean.getCallFromCtryCd(), 3, 3);
/*     */       
/* 306 */       if (reqLength.equals("greaterThanMax")) {
/* 307 */         isValid = false;
/*     */       }
/*     */     } else {
/* 310 */       isValid = false;
/*     */     }
/*     */     
/*     */ 
/* 314 */     if (!isValid)
/*     */     {
/* 316 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_CALLFROMCTRYCD);
/* 317 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateCallFromPhoneNbr(TAACommunServIndustryBean taaCommunServIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 335 */     ErrorObject errorObj = null;
/* 336 */     boolean isValid = true;
/* 337 */     if (CommonValidator.isNullOrEmpty(taaCommunServIndustryBean.getCallFromPhoneNbr())) {
/* 338 */       isValid = false;
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/* 345 */       String reqLength = CommonValidator.validateLength(
/* 346 */         taaCommunServIndustryBean.getCallFromPhoneNbr(), 16, 1);
/*     */       
/* 348 */       if (reqLength.equals("greaterThanMax")) {
/* 349 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 359 */     if (!isValid)
/*     */     {
/* 361 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_CALLFROMPHONENBR);
/* 362 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateCallToLocNm(TAACommunServIndustryBean taaCommunServIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 380 */     ErrorObject errorObj = null;
/* 381 */     boolean isValid = true;
/* 382 */     if (CommonValidator.isNullOrEmpty(taaCommunServIndustryBean.getCallToLocNm())) {
/* 383 */       isValid = false;
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/* 390 */       String reqLength = CommonValidator.validateLength(
/* 391 */         taaCommunServIndustryBean.getCallToLocNm(), 18, 1);
/*     */       
/* 393 */       if (reqLength.equals("greaterThanMax")) {
/* 394 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 404 */     if (!isValid)
/*     */     {
/* 406 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_CALLTOLOCNM);
/* 407 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateCallToRgnCd(TAACommunServIndustryBean taaCommunServIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 425 */     ErrorObject errorObj = null;
/* 426 */     boolean isValid = true;
/* 427 */     if (CommonValidator.isNullOrEmpty(taaCommunServIndustryBean.getCallToRgnCd())) {
/* 428 */       if (!CommonValidator.isNullOrEmpty(taaCommunServIndustryBean.getCallToCtryCd())) {
/* 429 */         isValid = false;
/*     */       }
/*     */       
/*     */     }
/* 433 */     else if (CommonValidator.validateData(taaCommunServIndustryBean
/* 434 */       .getCallFromRgnCd(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[112]))
/*     */     {
/* 436 */       if (!CommonValidator.isNullOrEmpty(taaCommunServIndustryBean.getCallFromCtryCd())) {
/* 437 */         if (!CommonValidator.isValidRegionCode(taaCommunServIndustryBean.getCallFromRgnCd(), taaCommunServIndustryBean.getCallFromCtryCd())) {
/* 438 */           isValid = false;
/*     */         }
/*     */       } else {
/* 441 */         isValid = false;
/*     */       }
/*     */     } else {
/* 444 */       isValid = false;
/*     */     }
/*     */     
/*     */ 
/*     */ 
/* 449 */     if (!isValid)
/*     */     {
/* 451 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_CALLTORGNCD);
/* 452 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateCallToCtryCd(TAACommunServIndustryBean taaCommunServIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 471 */     ErrorObject errorObj = null;
/* 472 */     boolean isValid = true;
/* 473 */     if (CommonValidator.isNullOrEmpty(taaCommunServIndustryBean.getCallToCtryCd()))
/*     */     {
/*     */ 
/* 476 */       if (!CommonValidator.isNullOrEmpty(taaCommunServIndustryBean.getCallToRgnCd()))
/*     */       {
/*     */ 
/* 479 */         isValid = false;
/*     */ 
/*     */       }
/*     */       
/*     */ 
/*     */     }
/* 485 */     else if ((CommonValidator.validateData(taaCommunServIndustryBean
/* 486 */       .getCallToCtryCd(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[117])) && (CommonValidator.isValidXmlCountryCode(taaCommunServIndustryBean
/* 487 */       .getCallToCtryCd())))
/*     */     {
/* 489 */       String reqLength = CommonValidator.validateLength(
/* 490 */         taaCommunServIndustryBean.getCallToCtryCd(), 3, 3);
/*     */       
/* 492 */       if (reqLength.equals("greaterThanMax")) {
/* 493 */         isValid = false;
/*     */       }
/*     */     } else {
/* 496 */       isValid = false;
/*     */     }
/*     */     
/*     */ 
/*     */ 
/* 501 */     if (!isValid)
/*     */     {
/* 503 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_CALLTOCTRYCD);
/* 504 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateCallToPhoneNbr(TAACommunServIndustryBean taaCommunServIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 524 */     ErrorObject errorObj = null;
/* 525 */     boolean isValid = true;
/* 526 */     if (CommonValidator.isNullOrEmpty(taaCommunServIndustryBean.getCallToPhoneNbr())) {
/* 527 */       isValid = false;
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/* 533 */       String reqLength = CommonValidator.validateLength(
/* 534 */         taaCommunServIndustryBean.getCallToPhoneNbr(), 16, 1);
/*     */       
/* 536 */       if (reqLength.equals("greaterThanMax")) {
/* 537 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 544 */     if (!isValid)
/*     */     {
/* 546 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_CALLTOPHONENBR);
/* 547 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateCallingCardId(TAACommunServIndustryBean taaCommunServIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 566 */     ErrorObject errorObj = null;
/* 567 */     boolean isValid = true;
/* 568 */     if (!CommonValidator.isNullOrEmpty(taaCommunServIndustryBean.getCallingCardId()))
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 574 */       String reqLength = CommonValidator.validateLength(
/* 575 */         taaCommunServIndustryBean.getCallingCardId(), 8, 1);
/*     */       
/* 577 */       if (reqLength.equals("greaterThanMax")) {
/* 578 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 586 */     if (!isValid)
/*     */     {
/* 588 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_CALLINGCARDID);
/* 589 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateServTypeDescTxt(TAACommunServIndustryBean taaCommunServIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 608 */     ErrorObject errorObj = null;
/* 609 */     boolean isValid = true;
/* 610 */     if (!CommonValidator.isNullOrEmpty(taaCommunServIndustryBean.getServTypeDescTxt()))
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 616 */       String reqLength = CommonValidator.validateLength(
/* 617 */         taaCommunServIndustryBean.getServTypeDescTxt(), 20, 1);
/*     */       
/* 619 */       if (reqLength.equals("greaterThanMax")) {
/* 620 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 628 */     if (!isValid)
/*     */     {
/* 630 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_SERVTYPEDESCTXT);
/* 631 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateBillPerTxt(TAACommunServIndustryBean taaCommunServIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 650 */     ErrorObject errorObj = null;
/* 651 */     boolean isValid = true;
/* 652 */     if (!CommonValidator.isNullOrEmpty(taaCommunServIndustryBean.getBillPerTxt()))
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 658 */       String reqLength = CommonValidator.validateLength(
/* 659 */         taaCommunServIndustryBean.getBillPerTxt(), 18, 1);
/*     */       
/* 661 */       if (reqLength.equals("greaterThanMax")) {
/* 662 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 670 */     if (!isValid)
/*     */     {
/* 672 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_BILLPERTXT);
/* 673 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateCallTypeCd(TAACommunServIndustryBean taaCommunServIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 692 */     ErrorObject errorObj = null;
/* 693 */     boolean isValid = true;
/* 694 */     if (!CommonValidator.isNullOrEmpty(taaCommunServIndustryBean.getCallTypeCd()))
/*     */     {
/*     */ 
/* 697 */       if ((CommonValidator.validateData(taaCommunServIndustryBean
/* 698 */         .getCallTypeCd(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[122])) && (CommonValidator.isValidCallTypeCd(taaCommunServIndustryBean.getCallTypeCd())))
/*     */       {
/* 700 */         String reqLength = CommonValidator.validateLength(
/* 701 */           taaCommunServIndustryBean.getCallTypeCd(), 5, 1);
/*     */         
/* 703 */         if (reqLength.equals("greaterThanMax")) {
/* 704 */           isValid = false;
/*     */         }
/*     */       } else {
/* 707 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/* 711 */     if (!isValid)
/*     */     {
/* 713 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_CALLTYPECD);
/* 714 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateRateClassCd(TAACommunServIndustryBean taaCommunServIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 732 */     ErrorObject errorObj = null;
/* 733 */     boolean isValid = true;
/* 734 */     if (!CommonValidator.isNullOrEmpty(taaCommunServIndustryBean.getRateClassCd()))
/*     */     {
/*     */ 
/* 737 */       if ((CommonValidator.validateData(taaCommunServIndustryBean
/* 738 */         .getRateClassCd(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[123])) && (CommonValidator.isValidRateClassCd(taaCommunServIndustryBean.getRateClassCd())))
/*     */       {
/* 740 */         String reqLength = CommonValidator.validateLength(
/* 741 */           taaCommunServIndustryBean.getRateClassCd(), 1, 1);
/*     */         
/* 743 */         if (reqLength.equals("greaterThanMax")) {
/* 744 */           isValid = false;
/*     */         }
/*     */       } else {
/* 747 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/* 751 */     if (!isValid)
/*     */     {
/* 753 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_RATECLASSCD);
/* 754 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\validator\xml\industry\TAACommunServIndusValidator.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */