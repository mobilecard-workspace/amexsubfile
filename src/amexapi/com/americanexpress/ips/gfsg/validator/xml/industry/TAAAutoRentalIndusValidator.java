/*      */ package com.americanexpress.ips.gfsg.validator.xml.industry;
/*      */ 
/*      */ import com.americanexpress.ips.gfsg.beans.ErrorObject;
/*      */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.TAAAutoRentalIndustryBean;
/*      */ import com.americanexpress.ips.gfsg.enumerations.XMLSubmissionErrorCodes;
/*      */ import com.americanexpress.ips.gfsg.exceptions.SettlementException;
/*      */ import com.americanexpress.ips.gfsg.validator.CommonValidator;
/*      */ import java.util.List;
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ public class TAAAutoRentalIndusValidator
/*      */ {
/*      */   public static void validateAutoRentalIndustry(TAAAutoRentalIndustryBean taaAutoRentalIndustryBean, List<ErrorObject> errorCodes, String transCurrCd)
/*      */     throws SettlementException
/*      */   {
/*   35 */     validateAgrNbr(taaAutoRentalIndustryBean, errorCodes);
/*   36 */     validatePickupLocNm(taaAutoRentalIndustryBean, errorCodes);
/*   37 */     validatePickupCityNm(taaAutoRentalIndustryBean, errorCodes);
/*   38 */     validatePickupRgnCd(taaAutoRentalIndustryBean, errorCodes);
/*   39 */     validatePickupCtryCd(taaAutoRentalIndustryBean, errorCodes);
/*   40 */     validatePickupDt(taaAutoRentalIndustryBean, errorCodes);
/*   41 */     validatePickupTm(taaAutoRentalIndustryBean, errorCodes);
/*   42 */     validateRtrnCityNm(taaAutoRentalIndustryBean, errorCodes);
/*   43 */     validateRtrnRgnCd(taaAutoRentalIndustryBean, errorCodes);
/*   44 */     validateRtrnCtryCd(taaAutoRentalIndustryBean, errorCodes);
/*   45 */     validateRtrnDt(taaAutoRentalIndustryBean, errorCodes);
/*   46 */     validateRtrnTm(taaAutoRentalIndustryBean, errorCodes);
/*   47 */     validateRenterNm(taaAutoRentalIndustryBean, errorCodes);
/*   48 */     validateVehClassId(taaAutoRentalIndustryBean, errorCodes);
/*   49 */     validateTravDisCnt(taaAutoRentalIndustryBean, errorCodes);
/*   50 */     validateTravDisUnit(taaAutoRentalIndustryBean, errorCodes);
/*   51 */     validateAuditAdjInd(taaAutoRentalIndustryBean, errorCodes);
/*   52 */     validateAuditAdjAmt(taaAutoRentalIndustryBean, errorCodes, transCurrCd);
/*   53 */     validateRtrnLocNm(taaAutoRentalIndustryBean, errorCodes);
/*   54 */     validateVehIdNbr(taaAutoRentalIndustryBean, errorCodes);
/*   55 */     validateDrvIdNbr(taaAutoRentalIndustryBean, errorCodes);
/*   56 */     validateDrvTaxIdNbr(taaAutoRentalIndustryBean, errorCodes);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateAgrNbr(TAAAutoRentalIndustryBean tAAAutoRentalIndusBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*   72 */     ErrorObject errorObj = null;
/*   73 */     boolean isValid = true;
/*   74 */     if (CommonValidator.isNullOrEmpty(tAAAutoRentalIndusBean.getAgrNbr())) {
/*   75 */       isValid = false;
/*      */ 
/*      */ 
/*      */     }
/*      */     else
/*      */     {
/*      */ 
/*      */ 
/*   83 */       String reqLength = CommonValidator.validateLength(
/*   84 */         tAAAutoRentalIndusBean.getAgrNbr(), 14, 1);
/*      */       
/*   86 */       if (reqLength.equals("greaterThanMax")) {
/*   87 */         isValid = false;
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*   96 */     if (!isValid)
/*      */     {
/*      */ 
/*   99 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_AGRNBR);
/*  100 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validatePickupLocNm(TAAAutoRentalIndustryBean tAAAutoRentalIndusBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  118 */     ErrorObject errorObj = null;
/*  119 */     boolean isValid = true;
/*  120 */     if (!CommonValidator.isNullOrEmpty(tAAAutoRentalIndusBean
/*  121 */       .getPickupLocNm()))
/*      */     {
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  127 */       String reqLength = CommonValidator.validateLength(
/*  128 */         tAAAutoRentalIndusBean.getPickupLocNm(), 38, 1);
/*      */       
/*  130 */       if (reqLength.equals("greaterThanMax")) {
/*  131 */         isValid = false;
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*  138 */     if (!isValid)
/*      */     {
/*  140 */       errorObj = new ErrorObject(
/*  141 */         XMLSubmissionErrorCodes.ERROR_PICKUPLOCNM);
/*  142 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validatePickupCityNm(TAAAutoRentalIndustryBean tAAAutoRentalIndusBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  160 */     ErrorObject errorObj = null;
/*  161 */     boolean isValid = true;
/*  162 */     if (CommonValidator.isNullOrEmpty(tAAAutoRentalIndusBean
/*  163 */       .getPickupCityNm())) {
/*  164 */       isValid = false;
/*      */ 
/*      */ 
/*      */     }
/*      */     else
/*      */     {
/*      */ 
/*  171 */       String reqLength = CommonValidator.validateLength(
/*  172 */         tAAAutoRentalIndusBean.getPickupCityNm(), 18, 1);
/*      */       
/*  174 */       if (reqLength.equals("greaterThanMax")) {
/*  175 */         isValid = false;
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*  182 */     if (!isValid)
/*      */     {
/*  184 */       errorObj = new ErrorObject(
/*  185 */         XMLSubmissionErrorCodes.ERROR_PICKUPCITYNM);
/*  186 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validatePickupRgnCd(TAAAutoRentalIndustryBean tAAAutoRentalIndusBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  204 */     ErrorObject errorObj = null;
/*  205 */     boolean isValid = true;
/*  206 */     if (CommonValidator.isNullOrEmpty(tAAAutoRentalIndusBean
/*  207 */       .getPickupRgnCd())) {
/*  208 */       isValid = false;
/*      */     }
/*  210 */     else if (CommonValidator.isNullOrEmpty(tAAAutoRentalIndusBean
/*  211 */       .getPickupCtryCd()))
/*      */     {
/*  213 */       errorObj = new ErrorObject(
/*  214 */         XMLSubmissionErrorCodes.ERROR_PICKUPCTRYCD);
/*  215 */       errorCodes.add(errorObj);
/*      */     }
/*  217 */     else if (CommonValidator.isValidRegionCode(tAAAutoRentalIndusBean
/*  218 */       .getPickupRgnCd(), 
/*  219 */       tAAAutoRentalIndusBean.getPickupCtryCd()))
/*      */     {
/*  221 */       String reqLength = CommonValidator.validateLength(
/*  222 */         tAAAutoRentalIndusBean.getPickupRgnCd(), 3, 3);
/*      */       
/*  224 */       if (reqLength.equals("greaterThanMax")) {
/*  225 */         isValid = false;
/*      */       }
/*      */     }
/*      */     else
/*      */     {
/*  230 */       isValid = false;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*  235 */     if (!isValid)
/*      */     {
/*  237 */       errorObj = new ErrorObject(
/*  238 */         XMLSubmissionErrorCodes.ERROR_PICKUPRGNCD);
/*  239 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validatePickupCtryCd(TAAAutoRentalIndustryBean tAAAutoRentalIndusBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  257 */     ErrorObject errorObj = null;
/*  258 */     boolean isValid = true;
/*  259 */     if (CommonValidator.isNullOrEmpty(tAAAutoRentalIndusBean
/*  260 */       .getPickupCtryCd())) {
/*  261 */       isValid = false;
/*      */ 
/*      */     }
/*  264 */     else if (CommonValidator.isValidXmlCountryCode(tAAAutoRentalIndusBean
/*  265 */       .getPickupCtryCd()))
/*      */     {
/*  267 */       String reqLength = CommonValidator.validateLength(
/*  268 */         tAAAutoRentalIndusBean.getPickupCtryCd(), 3, 3);
/*      */       
/*  270 */       if (reqLength.equals("greaterThanMax")) {
/*  271 */         isValid = false;
/*      */       }
/*      */     }
/*      */     else {
/*  275 */       isValid = false;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*  280 */     if (!isValid)
/*      */     {
/*  282 */       errorObj = new ErrorObject(
/*  283 */         XMLSubmissionErrorCodes.ERROR_PICKUPCTRYCD);
/*  284 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validatePickupDt(TAAAutoRentalIndustryBean tAAAutoRentalIndusBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  302 */     ErrorObject errorObj = null;
/*  303 */     boolean isValid = true;
/*  304 */     if (CommonValidator.isNullOrEmpty(tAAAutoRentalIndusBean.getPickupDt())) {
/*  305 */       isValid = false;
/*      */ 
/*      */ 
/*      */     }
/*  309 */     else if (CommonValidator.validateData(tAAAutoRentalIndusBean
/*  310 */       .getPickupDt(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[95]))
/*      */     {
/*  312 */       if (!CommonValidator.isValidXmlDate(tAAAutoRentalIndusBean
/*  313 */         .getPickupDt(), "CCYYMMDD")) {
/*  314 */         isValid = false;
/*      */       }
/*      */     } else {
/*  317 */       isValid = false;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*  322 */     if (!isValid)
/*      */     {
/*  324 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_PICKUPDT);
/*  325 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validatePickupTm(TAAAutoRentalIndustryBean tAAAutoRentalIndusBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  343 */     ErrorObject errorObj = null;
/*  344 */     boolean isValid = true;
/*      */     
/*  346 */     if (!CommonValidator.isNullOrEmpty(tAAAutoRentalIndusBean.getPickupTm()))
/*      */     {
/*  348 */       if (CommonValidator.validateData(tAAAutoRentalIndusBean
/*  349 */         .getPickupTm(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[96]))
/*      */       {
/*  351 */         if (!CommonValidator.validateTime(tAAAutoRentalIndusBean
/*  352 */           .getPickupTm())) {
/*  353 */           isValid = false;
/*      */         }
/*      */       } else {
/*  356 */         isValid = false;
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*  361 */     if (!isValid)
/*      */     {
/*  363 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_PICKUPTM);
/*  364 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateRtrnCityNm(TAAAutoRentalIndustryBean tAAAutoRentalIndusBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  382 */     ErrorObject errorObj = null;
/*  383 */     boolean isValid = true;
/*  384 */     if (CommonValidator.isNullOrEmpty(tAAAutoRentalIndusBean
/*  385 */       .getRtrnCityNm())) {
/*  386 */       isValid = false;
/*      */ 
/*      */     }
/*      */     else
/*      */     {
/*      */ 
/*  392 */       String reqLength = CommonValidator.validateLength(
/*  393 */         tAAAutoRentalIndusBean.getRtrnCityNm(), 18, 1);
/*      */       
/*  395 */       if (reqLength.equals("greaterThanMax")) {
/*  396 */         isValid = false;
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*  403 */     if (!isValid)
/*      */     {
/*  405 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_RTRNCITYNM);
/*  406 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateRtrnRgnCd(TAAAutoRentalIndustryBean tAAAutoRentalIndusBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  424 */     ErrorObject errorObj = null;
/*  425 */     boolean isValid = true;
/*      */     
/*  427 */     if (CommonValidator.isNullOrEmpty(tAAAutoRentalIndusBean.getRtrnRgnCd())) {
/*  428 */       isValid = false;
/*      */     }
/*  430 */     else if (CommonValidator.isNullOrEmpty(tAAAutoRentalIndusBean
/*  431 */       .getRtrnCtryCd()))
/*      */     {
/*  433 */       errorObj = new ErrorObject(
/*  434 */         XMLSubmissionErrorCodes.ERROR_RTRNCTRYCD);
/*  435 */       errorCodes.add(errorObj);
/*      */ 
/*      */     }
/*  438 */     else if (CommonValidator.isValidRegionCode(tAAAutoRentalIndusBean
/*      */     
/*  440 */       .getRtrnRgnCd(), tAAAutoRentalIndusBean.getRtrnCtryCd()))
/*      */     {
/*  442 */       String reqLength = CommonValidator.validateLength(
/*  443 */         tAAAutoRentalIndusBean.getRtrnRgnCd(), 3, 3);
/*      */       
/*  445 */       if (reqLength.equals("greaterThanMax")) {
/*  446 */         isValid = false;
/*      */       }
/*      */     }
/*      */     else
/*      */     {
/*  451 */       isValid = false;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*  456 */     if (!isValid)
/*      */     {
/*  458 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_RTRNRGNCD);
/*  459 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateRtrnCtryCd(TAAAutoRentalIndustryBean tAAAutoRentalIndusBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  478 */     ErrorObject errorObj = null;
/*  479 */     boolean isValid = true;
/*  480 */     if (CommonValidator.isNullOrEmpty(tAAAutoRentalIndusBean
/*  481 */       .getRtrnCtryCd())) {
/*  482 */       isValid = false;
/*      */ 
/*      */     }
/*  485 */     else if (CommonValidator.isValidXmlCountryCode(tAAAutoRentalIndusBean
/*  486 */       .getRtrnCtryCd()))
/*      */     {
/*  488 */       String reqLength = CommonValidator.validateLength(
/*  489 */         tAAAutoRentalIndusBean.getRtrnCtryCd(), 3, 3);
/*      */       
/*  491 */       if (reqLength.equals("greaterThanMax")) {
/*  492 */         isValid = false;
/*      */       }
/*      */     } else {
/*  495 */       isValid = false;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*  500 */     if (!isValid)
/*      */     {
/*  502 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_RTRNCTRYCD);
/*  503 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateRtrnDt(TAAAutoRentalIndustryBean tAAAutoRentalIndusBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  520 */     ErrorObject errorObj = null;
/*  521 */     boolean isValid = true;
/*  522 */     if (CommonValidator.isNullOrEmpty(tAAAutoRentalIndusBean.getRtrnDt())) {
/*  523 */       isValid = false;
/*      */ 
/*      */     }
/*  526 */     else if (CommonValidator.validateData(
/*  527 */       tAAAutoRentalIndusBean.getRtrnDt(), 
/*  528 */       com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[100]))
/*      */     {
/*  530 */       if (!CommonValidator.isValidXmlDate(tAAAutoRentalIndusBean
/*  531 */         .getRtrnDt(), "CCYYMMDD")) {
/*  532 */         isValid = false;
/*      */       }
/*      */     } else {
/*  535 */       isValid = false;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*  540 */     if (!isValid)
/*      */     {
/*  542 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_RTRNDT);
/*  543 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateRtrnTm(TAAAutoRentalIndustryBean tAAAutoRentalIndusBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  560 */     ErrorObject errorObj = null;
/*  561 */     boolean isValid = true;
/*  562 */     if (!CommonValidator.isNullOrEmpty(tAAAutoRentalIndusBean.getRtrnTm()))
/*      */     {
/*  564 */       if (CommonValidator.validateData(
/*  565 */         tAAAutoRentalIndusBean.getRtrnTm(), 
/*  566 */         com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[101]))
/*      */       {
/*  568 */         if (!CommonValidator.validateTime(tAAAutoRentalIndusBean
/*  569 */           .getRtrnTm())) {
/*  570 */           isValid = false;
/*      */         }
/*      */       }
/*      */       else {
/*  574 */         isValid = false;
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*  579 */     if (!isValid)
/*      */     {
/*  581 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_RTRNTM);
/*  582 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateRenterNm(TAAAutoRentalIndustryBean tAAAutoRentalIndusBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  600 */     ErrorObject errorObj = null;
/*  601 */     boolean isValid = true;
/*  602 */     if (CommonValidator.isNullOrEmpty(tAAAutoRentalIndusBean.getRenterNm())) {
/*  603 */       isValid = false;
/*      */ 
/*      */     }
/*      */     else
/*      */     {
/*      */ 
/*  609 */       String reqLength = CommonValidator.validateLength(
/*  610 */         tAAAutoRentalIndusBean.getRenterNm(), 26, 1);
/*      */       
/*  612 */       if (reqLength.equals("greaterThanMax")) {
/*  613 */         isValid = false;
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*  620 */     if (!isValid)
/*      */     {
/*  622 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_RENTERNM);
/*  623 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateVehClassId(TAAAutoRentalIndustryBean tAAAutoRentalIndusBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  641 */     ErrorObject errorObj = null;
/*  642 */     boolean isValid = true;
/*  643 */     if (!CommonValidator.isNullOrEmpty(tAAAutoRentalIndusBean
/*  644 */       .getVehClassId()))
/*      */     {
/*  646 */       if (CommonValidator.validateData(tAAAutoRentalIndusBean
/*  647 */         .getVehClassId(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[103]))
/*  648 */         if (CommonValidator.isVehClassId(tAAAutoRentalIndusBean
/*  649 */           .getVehClassId()))
/*      */         {
/*  651 */           String reqLength = CommonValidator.validateLength(
/*  652 */             tAAAutoRentalIndusBean.getVehClassId(), 4, 4);
/*      */           
/*  654 */           if (!reqLength.equals("greaterThanMax")) break label68;
/*  655 */           isValid = false;
/*      */           break label68;
/*      */         }
/*  658 */       isValid = false;
/*      */     }
/*      */     
/*      */     label68:
/*  662 */     if (!isValid)
/*      */     {
/*  664 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_VEHCLASSID);
/*  665 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTravDisCnt(TAAAutoRentalIndustryBean tAAAutoRentalIndusBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  683 */     ErrorObject errorObj = null;
/*  684 */     boolean isValid = true;
/*  685 */     if (CommonValidator.isNullOrEmpty(tAAAutoRentalIndusBean
/*  686 */       .getTravDisCnt()))
/*      */     {
/*  688 */       if (!CommonValidator.isNullOrEmpty(tAAAutoRentalIndusBean
/*  689 */         .getTravDisUnit()))
/*      */       {
/*  691 */         isValid = false;
/*      */       }
/*      */       
/*      */ 
/*      */     }
/*  696 */     else if (CommonValidator.validateData(tAAAutoRentalIndusBean
/*  697 */       .getTravDisCnt(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[104]))
/*      */     {
/*  699 */       String reqLength = CommonValidator.validateLength(
/*  700 */         tAAAutoRentalIndusBean.getTravDisCnt(), 5, 1);
/*      */       
/*  702 */       if (reqLength.equals("greaterThanMax")) {
/*  703 */         isValid = false;
/*      */       }
/*      */     } else {
/*  706 */       isValid = false;
/*      */     }
/*      */     
/*      */ 
/*  710 */     if (!isValid)
/*      */     {
/*  712 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_TRAVDISCNT);
/*  713 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTravDisUnit(TAAAutoRentalIndustryBean tAAAutoRentalIndusBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  731 */     ErrorObject errorObj = null;
/*  732 */     boolean isValid = true;
/*  733 */     if (CommonValidator.isNullOrEmpty(tAAAutoRentalIndusBean
/*  734 */       .getTravDisUnit())) {
/*  735 */       if (!CommonValidator.isNullOrEmpty(tAAAutoRentalIndusBean
/*  736 */         .getTravDisCnt()))
/*      */       {
/*  738 */         isValid = false;
/*      */       }
/*      */       
/*      */     }
/*  742 */     else if (CommonValidator.isValidTravDisUnit(tAAAutoRentalIndusBean
/*  743 */       .getTravDisUnit()))
/*      */     {
/*  745 */       String reqLength = CommonValidator.validateLength(
/*  746 */         tAAAutoRentalIndusBean.getTravDisUnit(), 1, 1);
/*      */       
/*  748 */       if (reqLength.equals("greaterThanMax")) {
/*  749 */         isValid = false;
/*      */       }
/*      */     } else {
/*  752 */       isValid = false;
/*      */     }
/*      */     
/*      */ 
/*  756 */     if (!isValid)
/*      */     {
/*  758 */       errorObj = new ErrorObject(
/*  759 */         XMLSubmissionErrorCodes.ERROR_TRAVDISUNIT);
/*  760 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateAuditAdjInd(TAAAutoRentalIndustryBean tAAAutoRentalIndusBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  778 */     ErrorObject errorObj = null;
/*  779 */     boolean isValid = true;
/*  780 */     if (CommonValidator.isNullOrEmpty(tAAAutoRentalIndusBean
/*  781 */       .getAuditAdjInd()))
/*      */     {
/*  783 */       if (!CommonValidator.isNullOrEmpty(tAAAutoRentalIndusBean
/*  784 */         .getAuditAdjAmt()))
/*      */       {
/*  786 */         isValid = false;
/*      */       }
/*      */       
/*      */     }
/*      */     else
/*      */     {
/*  792 */       if (CommonValidator.validateData(tAAAutoRentalIndusBean.getAuditAdjInd(), 
/*  793 */         com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[106]))
/*      */       {
/*  795 */         if (CommonValidator.isValidAuditAdjInd(tAAAutoRentalIndusBean
/*  796 */           .getAuditAdjInd()))
/*      */         {
/*  798 */           String reqLength = CommonValidator.validateLength(
/*  799 */             tAAAutoRentalIndusBean.getAuditAdjInd(), 1, 1);
/*      */           
/*  801 */           if (!reqLength.equals("greaterThanMax")) break label83;
/*  802 */           isValid = false;
/*      */           break label83;
/*      */         } }
/*  805 */       isValid = false;
/*      */     }
/*      */     
/*      */     label83:
/*  809 */     if (!isValid)
/*      */     {
/*  811 */       errorObj = new ErrorObject(
/*  812 */         XMLSubmissionErrorCodes.ERROR_AUDITADJIND);
/*  813 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateAuditAdjAmt(TAAAutoRentalIndustryBean tAAAutoRentalIndusBean, List<ErrorObject> errorCodes, String transCurrCd)
/*      */     throws SettlementException
/*      */   {
/*  832 */     ErrorObject errorObj = null;
/*  833 */     boolean isValid = true;
/*  834 */     if (CommonValidator.isNullOrEmpty(tAAAutoRentalIndusBean
/*  835 */       .getAuditAdjAmt()))
/*      */     {
/*  837 */       if (!CommonValidator.isNullOrEmpty(tAAAutoRentalIndusBean
/*  838 */         .getAuditAdjInd()))
/*      */       {
/*  840 */         isValid = false;
/*      */       }
/*      */       
/*      */ 
/*      */     }
/*  845 */     else if (CommonValidator.validateData(tAAAutoRentalIndusBean.getAuditAdjAmt(), 
/*  846 */       com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[107]))
/*      */     {
/*  848 */       String reqLength = CommonValidator.validateLength(
/*  849 */         tAAAutoRentalIndusBean.getAuditAdjAmt(), 12, 1);
/*      */       
/*  851 */       if (reqLength.equals("greaterThanMax")) {
/*  852 */         isValid = false;
/*      */       }
/*  854 */       else if ((!CommonValidator.isNullOrEmpty(transCurrCd)) && 
/*  855 */         (!CommonValidator.isValidXmlAmount(transCurrCd, tAAAutoRentalIndusBean.getAuditAdjAmt()))) {
/*  856 */         isValid = false;
/*      */       }
/*      */     }
/*      */     else
/*      */     {
/*  861 */       isValid = false;
/*      */     }
/*      */     
/*      */ 
/*  865 */     if (!isValid)
/*      */     {
/*  867 */       errorObj = new ErrorObject(
/*  868 */         XMLSubmissionErrorCodes.ERROR_AUDITADJAMT);
/*  869 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateRtrnLocNm(TAAAutoRentalIndustryBean tAAAutoRentalIndusBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  888 */     ErrorObject errorObj = null;
/*  889 */     boolean isValid = true;
/*  890 */     if (!CommonValidator.isNullOrEmpty(tAAAutoRentalIndusBean
/*  891 */       .getRtrnLocNm()))
/*      */     {
/*      */ 
/*      */ 
/*      */ 
/*  896 */       String reqLength = CommonValidator.validateLength(
/*  897 */         tAAAutoRentalIndusBean.getRtrnLocNm(), 38, 1);
/*      */       
/*  899 */       if (reqLength.equals("greaterThanMax")) {
/*  900 */         isValid = false;
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*  907 */     if (!isValid)
/*      */     {
/*  909 */       errorObj = new ErrorObject(
/*  910 */         XMLSubmissionErrorCodes.ERROR_RTRNLOCNM);
/*  911 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateVehIdNbr(TAAAutoRentalIndustryBean tAAAutoRentalIndusBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  931 */     ErrorObject errorObj = null;
/*  932 */     boolean isValid = true;
/*  933 */     if (!CommonValidator.isNullOrEmpty(tAAAutoRentalIndusBean
/*  934 */       .getVehIdNbr()))
/*      */     {
/*      */ 
/*      */ 
/*      */ 
/*  939 */       String reqLength = CommonValidator.validateLength(
/*  940 */         tAAAutoRentalIndusBean.getVehIdNbr(), 20, 1);
/*      */       
/*  942 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  944 */         isValid = false;
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*  951 */     if (!isValid)
/*      */     {
/*  953 */       errorObj = new ErrorObject(
/*  954 */         XMLSubmissionErrorCodes.ERROR_VEHICLEIDNBR);
/*  955 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateDrvIdNbr(TAAAutoRentalIndustryBean tAAAutoRentalIndusBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  974 */     ErrorObject errorObj = null;
/*  975 */     boolean isValid = true;
/*  976 */     if (!CommonValidator.isNullOrEmpty(tAAAutoRentalIndusBean
/*  977 */       .getDrvIdNbr()))
/*      */     {
/*      */ 
/*      */ 
/*      */ 
/*  982 */       String reqLength = CommonValidator.validateLength(
/*  983 */         tAAAutoRentalIndusBean.getDrvIdNbr(), 20, 1);
/*      */       
/*  985 */       if (reqLength.equals("greaterThanMax")) {
/*  986 */         isValid = false;
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*  993 */     if (!isValid)
/*      */     {
/*  995 */       errorObj = new ErrorObject(
/*  996 */         XMLSubmissionErrorCodes.ERROR_DRIVERIDNBR);
/*  997 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateDrvTaxIdNbr(TAAAutoRentalIndustryBean tAAAutoRentalIndusBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1016 */     ErrorObject errorObj = null;
/* 1017 */     boolean isValid = true;
/* 1018 */     if (!CommonValidator.isNullOrEmpty(tAAAutoRentalIndusBean
/* 1019 */       .getDrvTaxIdNbr()))
/*      */     {
/*      */ 
/*      */ 
/*      */ 
/* 1024 */       String reqLength = CommonValidator.validateLength(
/* 1025 */         tAAAutoRentalIndusBean.getDrvTaxIdNbr(), 20, 1);
/*      */       
/* 1027 */       if (reqLength.equals("greaterThanMax")) {
/* 1028 */         isValid = false;
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/* 1035 */     if (!isValid)
/*      */     {
/* 1037 */       errorObj = new ErrorObject(
/* 1038 */         XMLSubmissionErrorCodes.ERROR_DRIVERTAXID);
/* 1039 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\validator\xml\industry\TAAAutoRentalIndusValidator.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */