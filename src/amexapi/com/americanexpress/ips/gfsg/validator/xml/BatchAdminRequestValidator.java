/*     */ package com.americanexpress.ips.gfsg.validator.xml;
/*     */ 
/*     */ import com.americanexpress.ips.gfsg.beans.ErrorObject;
/*     */ import com.americanexpress.ips.gfsg.beans.xml.batchadmin.BatchAdminRequestBean;
/*     */ import com.americanexpress.ips.gfsg.beans.xml.batchadmin.CardAcceptorDetail;
/*     */ import com.americanexpress.ips.gfsg.enumerations.BatchOperation;
/*     */ import com.americanexpress.ips.gfsg.enumerations.XMLSubmissionErrorCodes;
/*     */ import com.americanexpress.ips.gfsg.exceptions.SettlementException;
/*     */ import com.americanexpress.ips.gfsg.validator.CommonValidator;
/*     */ import java.util.List;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class BatchAdminRequestValidator
/*     */ {
/*     */   public static void validateBatchAdminRequestRec(BatchAdminRequestBean batchAdminRequestBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/*  38 */     validateVersion(batchAdminRequestBean, errorCodes);
/*  39 */     validateMerId(batchAdminRequestBean, errorCodes);
/*  40 */     validateMerTrmnlId(batchAdminRequestBean, errorCodes);
/*  41 */     validateBatchID(batchAdminRequestBean, errorCodes);
/*  42 */     validateBatchOperation(batchAdminRequestBean, errorCodes);
/*  43 */     validateReturnBatchSummary(batchAdminRequestBean, errorCodes);
/*  44 */     validateBatchImageSeqNbr(batchAdminRequestBean, errorCodes);
/*  45 */     validateServAgtMerId(batchAdminRequestBean, errorCodes);
/*  46 */     validateInvoiceFormatType(batchAdminRequestBean, errorCodes);
/*  47 */     validateReturnInvoiceStatus(batchAdminRequestBean, errorCodes);
/*  48 */     validateInvoiceNumber(batchAdminRequestBean, errorCodes);
/*  49 */     validateSubmitterCode(batchAdminRequestBean, errorCodes);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static List<ErrorObject> validateVersion(BatchAdminRequestBean batchAdminRequestBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/*  69 */     ErrorObject errorObj = null;
/*  70 */     boolean isValid = true;
/*  71 */     if (CommonValidator.isNullOrEmpty(batchAdminRequestBean.getVersion())) {
/*  72 */       isValid = false;
/*     */ 
/*     */     }
/*  75 */     else if ((!CommonValidator.validateData(
/*  76 */       batchAdminRequestBean.getVersion(), 
/*  77 */       com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[0])) || 
/*     */       
/*  79 */       (!batchAdminRequestBean.getVersion().equalsIgnoreCase("12010000")))
/*     */     {
/*  81 */       isValid = false;
/*     */     }
/*     */     
/*  84 */     if (!isValid)
/*     */     {
/*     */ 
/*  87 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_VERSION);
/*  88 */       errorCodes.add(errorObj);
/*     */     }
/*  90 */     return errorCodes;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateMerId(BatchAdminRequestBean batchAdminRequestBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 106 */     ErrorObject errorObj = null;
/* 107 */     boolean isValid = true;
/* 108 */     if (CommonValidator.isNullOrEmpty(batchAdminRequestBean.getMerId())) {
/* 109 */       isValid = false;
/*     */     }
/*     */     else {
/* 112 */       if (CommonValidator.validateData(batchAdminRequestBean.getMerId(), 
/* 113 */         com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[1]))
/*     */       {
/* 115 */         if (CommonValidator.isValidModulusNineCheck(batchAdminRequestBean
/* 116 */           .getMerId()))
/*     */         {
/* 118 */           String reqLength = CommonValidator.validateLength(
/* 119 */             batchAdminRequestBean.getMerId(), 10, 10);
/*     */           
/* 121 */           if (reqLength.equals("greaterThanMax"))
/*     */           {
/* 123 */             isValid = false;
/*     */             break label89; }
/* 125 */           if (!reqLength.equals("lessThanMin"))
/*     */             break label89;
/* 127 */           isValid = false;
/*     */           break label89;
/*     */         }
/*     */       }
/* 131 */       isValid = false;
/*     */     }
/*     */     
/*     */     label89:
/* 135 */     if (!isValid)
/*     */     {
/* 137 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_MERID);
/* 138 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateMerTrmnlId(BatchAdminRequestBean batchAdminRequestBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 156 */     ErrorObject errorObj = null;
/* 157 */     boolean isValid = true;
/*     */     
/* 159 */     if (CommonValidator.isNullOrEmpty(batchAdminRequestBean.getMerTrmnlId())) {
/* 160 */       isValid = false;
/*     */ 
/*     */     }
/* 163 */     else if (CommonValidator.validateData(batchAdminRequestBean
/* 164 */       .getMerTrmnlId(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[2]))
/*     */     {
/* 166 */       String reqLength = CommonValidator.validateLength(
/* 167 */         batchAdminRequestBean.getMerTrmnlId(), 8, 1);
/*     */       
/* 169 */       if (reqLength.equals("greaterThanMax")) {
/* 170 */         isValid = false;
/*     */       }
/*     */     } else {
/* 173 */       isValid = false;
/*     */     }
/*     */     
/*     */ 
/* 177 */     if (!isValid)
/*     */     {
/* 179 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_MERTRMNLID);
/* 180 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateBatchID(BatchAdminRequestBean batchAdminRequestBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 198 */     ErrorObject errorObj = null;
/* 199 */     boolean isValid = true;
/* 200 */     if (CommonValidator.isNullOrEmpty(batchAdminRequestBean.getBatchId()))
/*     */     {
/* 202 */       isValid = false;
/*     */     }
/* 204 */     else if (CommonValidator.validateData(
/* 205 */       batchAdminRequestBean.getBatchId(), 
/* 206 */       com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[3]))
/*     */     {
/* 208 */       String reqLength = CommonValidator.validateLength(
/* 209 */         batchAdminRequestBean.getBatchId(), 6, 1);
/*     */       
/* 211 */       if (reqLength.equals("greaterThanMax")) {
/* 212 */         isValid = false;
/*     */       }
/*     */     } else {
/* 215 */       isValid = false;
/*     */     }
/*     */     
/*     */ 
/* 219 */     if (!isValid)
/*     */     {
/* 221 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_BATCHID);
/* 222 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateBatchOperation(BatchAdminRequestBean batchAdminRequestBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 241 */     ErrorObject errorObj = null;
/* 242 */     boolean isValid = true;
/* 243 */     if (CommonValidator.isNullOrEmpty(batchAdminRequestBean
/* 244 */       .getBatchOperation())) {
/* 245 */       isValid = false;
/*     */     }
/* 247 */     else if ((!CommonValidator.validateData(batchAdminRequestBean
/* 248 */       .getBatchOperation(), 
/* 249 */       com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[4])) || 
/*     */       
/* 251 */       (!CommonValidator.isValidBatchOperation(batchAdminRequestBean
/* 252 */       .getBatchOperation()))) {
/* 253 */       isValid = false;
/*     */     }
/*     */     
/*     */ 
/* 257 */     if (!isValid)
/*     */     {
/* 259 */       errorObj = new ErrorObject(
/* 260 */         XMLSubmissionErrorCodes.ERROR_BATCHOPERATION);
/* 261 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateReturnBatchSummary(BatchAdminRequestBean batchAdminRequestBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 279 */     ErrorObject errorObj = null;
/* 280 */     boolean isValid = true;
/* 281 */     if (!CommonValidator.isNullOrEmpty(batchAdminRequestBean
/* 282 */       .getReturnBatchSummary())) {
/* 283 */       if (CommonValidator.validateData(batchAdminRequestBean
/* 284 */         .getReturnBatchSummary(), 
/* 285 */         com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[5]))
/*     */       {
/* 287 */         if (CommonValidator.isValidReturnBatchOperationSummary(batchAdminRequestBean
/* 288 */           .getReturnBatchSummary()))
/*     */         {
/* 290 */           String reqLength = CommonValidator.validateLength(
/* 291 */             batchAdminRequestBean.getReturnBatchSummary(), 2, 2);
/*     */           
/* 293 */           if (!reqLength.equals("greaterThanMax")) break label67;
/* 294 */           isValid = false;
/*     */           break label67;
/*     */         } }
/* 297 */       isValid = false;
/*     */     }
/*     */     
/*     */     label67:
/* 301 */     if (!isValid)
/*     */     {
/* 303 */       errorObj = new ErrorObject(
/* 304 */         XMLSubmissionErrorCodes.ERROR_RETURNBATCHSUMMARY);
/* 305 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateCardAcptStreetNm(CardAcceptorDetail cardAcceptorDetail, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 323 */     boolean isValid = true;
/* 324 */     ErrorObject errorObj = null;
/* 325 */     if (!CommonValidator.isNullOrEmpty(cardAcceptorDetail
/* 326 */       .getCardAcptStreetNm())) {
/* 327 */       if (CommonValidator.validateData(cardAcceptorDetail
/* 328 */         .getCardAcptStreetNm(), 
/* 329 */         com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[8]))
/*     */       {
/* 331 */         String reqLength = CommonValidator.validateLength(
/* 332 */           cardAcceptorDetail.getCardAcptStreetNm(), 38, 1);
/*     */         
/* 334 */         if (reqLength.equals("greaterThanMax")) {
/* 335 */           isValid = false;
/*     */         }
/*     */       } else {
/* 338 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/* 342 */     if (!isValid)
/*     */     {
/* 344 */       errorObj = new ErrorObject(
/* 345 */         XMLSubmissionErrorCodes.ERROR_CARDACPTSTREETNM);
/* 346 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateCardAcptNm(CardAcceptorDetail cardAcceptorDetail, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 365 */     ErrorObject errorObj = null;
/* 366 */     boolean isValid = true;
/*     */     
/* 368 */     if (CommonValidator.isNullOrEmpty(cardAcceptorDetail.getCardAcptNm()))
/*     */     {
/* 370 */       isValid = false;
/*     */     }
/* 372 */     else if (CommonValidator.validateData(
/* 373 */       cardAcceptorDetail.getCardAcptNm(), 
/* 374 */       com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[7]))
/*     */     {
/* 376 */       String reqLength = CommonValidator.validateLength(
/* 377 */         cardAcceptorDetail.getCardAcptNm(), 38, 1);
/*     */       
/* 379 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/* 381 */         isValid = false;
/*     */       }
/*     */     }
/*     */     else {
/* 385 */       isValid = false;
/*     */     }
/*     */     
/*     */ 
/* 389 */     if (!isValid)
/*     */     {
/* 391 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_CARDACPTNM);
/* 392 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateCardAcptCityNm(CardAcceptorDetail cardAcceptorDetail, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 410 */     ErrorObject errorObj = null;
/* 411 */     boolean isValid = true;
/* 412 */     if (CommonValidator.isNullOrEmpty(cardAcceptorDetail
/* 413 */       .getCardAcptCityNm())) {
/* 414 */       isValid = false;
/*     */     }
/* 416 */     else if (CommonValidator.validateData(cardAcceptorDetail
/* 417 */       .getCardAcptCityNm(), 
/* 418 */       com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[9]))
/*     */     {
/* 420 */       String reqLength = CommonValidator.validateLength(
/* 421 */         cardAcceptorDetail.getCardAcptCityNm(), 21, 1);
/*     */       
/* 423 */       if (reqLength.equals("greaterThanMax")) {
/* 424 */         isValid = false;
/*     */       }
/*     */     }
/*     */     else {
/* 428 */       isValid = false;
/*     */     }
/*     */     
/*     */ 
/* 432 */     if (!isValid)
/*     */     {
/* 434 */       errorObj = new ErrorObject(
/* 435 */         XMLSubmissionErrorCodes.ERROR_CARDACPTCITYNM);
/* 436 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateCardAcptRgnCd(CardAcceptorDetail cardAcceptorDetail, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 455 */     ErrorObject errorObj = null;
/* 456 */     boolean isValid = true;
/*     */     
/* 458 */     if (CommonValidator.isNullOrEmpty(cardAcceptorDetail.getCardAcptRegCd())) {
/* 459 */       isValid = false;
/*     */     } else {
/* 461 */       if (CommonValidator.validateData(cardAcceptorDetail
/* 462 */         .getCardAcptRegCd(), 
/* 463 */         com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[10])) {
/* 464 */         if (CommonValidator.isValidRegionCode(cardAcceptorDetail
/* 465 */           .getCardAcptRegCd(), 
/* 466 */           cardAcceptorDetail.getCardAcptCtryCd()))
/*     */         {
/* 468 */           String reqLength = CommonValidator.validateLength(
/* 469 */             cardAcceptorDetail.getCardAcptRegCd(), 3, 1);
/*     */           
/* 471 */           if (reqLength.equals("greaterThanMax")) {
/* 472 */             isValid = false;
/*     */             break label92; }
/* 474 */           if (!reqLength.equals("lessThanMin")) break label92;
/* 475 */           isValid = false;
/*     */           break label92;
/*     */         }
/*     */       }
/* 479 */       isValid = false;
/*     */     }
/*     */     
/*     */     label92:
/* 483 */     if (!isValid)
/*     */     {
/* 485 */       errorObj = new ErrorObject(
/* 486 */         XMLSubmissionErrorCodes.ERROR_CARDACPTRGNCD);
/* 487 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateCardAcptCtryCd(CardAcceptorDetail cardAcceptorDetail, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 504 */     boolean isValid = true;
/* 505 */     ErrorObject errorObj = null;
/*     */     
/* 507 */     if (CommonValidator.isNullOrEmpty(cardAcceptorDetail
/* 508 */       .getCardAcptCtryCd())) {
/* 509 */       isValid = false;
/*     */     }
/*     */     else {
/* 512 */       if (CommonValidator.validateData(cardAcceptorDetail
/* 513 */         .getCardAcptCtryCd(), 
/* 514 */         com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[11])) {
/* 515 */         if (CommonValidator.isValidXmlCountryCode(cardAcceptorDetail
/* 516 */           .getCardAcptCtryCd()))
/*     */         {
/* 518 */           String reqLength = CommonValidator.validateLength(
/* 519 */             cardAcceptorDetail.getCardAcptCtryCd(), 3, 3);
/* 520 */           if (!reqLength.equals("greaterThanMax")) break label73;
/* 521 */           isValid = false;
/*     */           break label73;
/*     */         }
/*     */       }
/* 525 */       isValid = false;
/*     */     }
/*     */     
/*     */     label73:
/* 529 */     if (!isValid)
/*     */     {
/* 531 */       errorObj = new ErrorObject(
/* 532 */         XMLSubmissionErrorCodes.ERROR_CARDACPTCTRYCD);
/* 533 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateCardAcptPostCd(CardAcceptorDetail cardAcceptorDetail, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 551 */     ErrorObject errorObj = null;
/* 552 */     boolean isValid = true;
/* 553 */     if (!CommonValidator.isNullOrEmpty(cardAcceptorDetail
/* 554 */       .getCardAcptPostCd()))
/*     */     {
/* 556 */       if (CommonValidator.validateData(cardAcceptorDetail
/* 557 */         .getCardAcptPostCd(), 
/* 558 */         com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[12]))
/*     */       {
/* 560 */         String reqLength = CommonValidator.validateLength(
/* 561 */           cardAcceptorDetail.getCardAcptPostCd(), 15, 1);
/*     */         
/* 563 */         if (reqLength.equals("greaterThanMax")) {
/* 564 */           isValid = false;
/*     */         }
/*     */       }
/*     */       else {
/* 568 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/* 572 */     if (!isValid)
/*     */     {
/* 574 */       errorObj = new ErrorObject(
/* 575 */         XMLSubmissionErrorCodes.ERROR_CARDACPTPOSTCD);
/* 576 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateSubmitterCode(BatchAdminRequestBean batchAdminRequestBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 594 */     ErrorObject errorObj = null;
/* 595 */     boolean isValid = true;
/* 596 */     if (CommonValidator.isNullOrEmpty(batchAdminRequestBean
/* 597 */       .getSubmitterCode()))
/*     */     {
/* 599 */       if (!CommonValidator.isNullOrEmpty(batchAdminRequestBean
/* 600 */         .getBatchOperation()))
/*     */       {
/*     */ 
/* 603 */         if (String.valueOf(batchAdminRequestBean.getBatchOperation()).equalsIgnoreCase(
/* 604 */           BatchOperation.Open.getBatchOperation())) {
/* 605 */           isValid = false;
/*     */         }
/*     */         
/*     */       }
/*     */     }
/*     */     else
/*     */     {
/* 612 */       if (!CommonValidator.isNullOrEmpty(batchAdminRequestBean
/*     */       
/* 614 */         .getSubmitterCode()))
/* 615 */         if (CommonValidator.validateData(batchAdminRequestBean
/* 616 */           .getSubmitterCode(), 
/* 617 */           com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[13])) {
/* 618 */           String reqLength = CommonValidator.validateLength(
/* 619 */             batchAdminRequestBean.getSubmitterCode(), 10, 1);
/* 620 */           if (!reqLength.equals("greaterThanMax")) break label103;
/* 621 */           isValid = false;
/*     */           break label103;
/*     */         }
/* 624 */       isValid = false;
/*     */     }
/*     */     
/*     */     label103:
/* 628 */     if (!isValid) {
/* 629 */       errorObj = new ErrorObject(
/* 630 */         XMLSubmissionErrorCodes.ERROR_SUBMITTERCODE);
/* 631 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateBatchImageSeqNbr(BatchAdminRequestBean batchAdminRequestBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 649 */     ErrorObject errorObj = null;
/* 650 */     boolean isValid = true;
/* 651 */     if (!CommonValidator.isNullOrEmpty(batchAdminRequestBean
/* 652 */       .getBatchImageSeqNbr()))
/*     */     {
/* 654 */       if (CommonValidator.validateData(batchAdminRequestBean
/* 655 */         .getBatchImageSeqNbr(), 
/* 656 */         com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE['½']))
/*     */       {
/* 658 */         String reqLength = CommonValidator.validateLength(
/* 659 */           batchAdminRequestBean.getBatchImageSeqNbr(), 8, 1);
/*     */         
/* 661 */         if (reqLength.equals("greaterThanMax")) {
/* 662 */           isValid = false;
/*     */         }
/*     */       }
/*     */       else {
/* 666 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/* 670 */     if (!isValid)
/*     */     {
/* 672 */       errorObj = new ErrorObject(
/* 673 */         XMLSubmissionErrorCodes.DCS_ERROR_BATCH_IMAGE_SEQ_NBR);
/* 674 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateServAgtMerId(BatchAdminRequestBean batchAdminRequestBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 692 */     ErrorObject errorObj = null;
/* 693 */     boolean isValid = true;
/* 694 */     if (!CommonValidator.isNullOrEmpty(batchAdminRequestBean
/* 695 */       .getServAgtMerId()))
/*     */     {
/* 697 */       if (CommonValidator.validateData(batchAdminRequestBean
/* 698 */         .getServAgtMerId(), 
/* 699 */         com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE['¾']))
/*     */       {
/* 701 */         String reqLength = CommonValidator.validateLength(
/* 702 */           batchAdminRequestBean.getServAgtMerId(), 15, 1);
/*     */         
/* 704 */         if (reqLength.equals("greaterThanMax")) {
/* 705 */           isValid = false;
/*     */         }
/*     */       } else {
/* 708 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/* 712 */     if (!isValid)
/*     */     {
/* 714 */       errorObj = new ErrorObject(
/* 715 */         XMLSubmissionErrorCodes.DCS_ERROR_SERV_AGT_MER_ID);
/* 716 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateInvoiceFormatType(BatchAdminRequestBean batchAdminRequestBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 734 */     ErrorObject errorObj = null;
/* 735 */     boolean isValid = true;
/* 736 */     if (!CommonValidator.isNullOrEmpty(batchAdminRequestBean
/* 737 */       .getInvoiceFormatType()))
/*     */     {
/* 739 */       if (CommonValidator.validateData(batchAdminRequestBean
/* 740 */         .getInvoiceFormatType(), 
/* 741 */         com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE['¿']))
/*     */       {
/* 743 */         if (CommonValidator.isValidInvoiceFormatType(batchAdminRequestBean
/* 744 */           .getInvoiceFormatType()))
/*     */         {
/* 746 */           String reqLength = CommonValidator.validateLength(
/* 747 */             batchAdminRequestBean.getInvoiceFormatType(), 3, 1);
/*     */           
/* 749 */           if (!reqLength.equals("greaterThanMax")) break label69;
/* 750 */           isValid = false;
/*     */           break label69;
/*     */         }
/*     */       }
/* 754 */       isValid = false;
/*     */     }
/*     */     
/*     */     label69:
/* 758 */     if (!isValid)
/*     */     {
/* 760 */       errorObj = new ErrorObject(
/* 761 */         XMLSubmissionErrorCodes.DCS_ERROR_INVALID_LID_FORMAT_TYPE);
/* 762 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateReturnInvoiceStatus(BatchAdminRequestBean batchAdminRequestBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 780 */     ErrorObject errorObj = null;
/* 781 */     boolean isValid = true;
/* 782 */     if (!CommonValidator.isNullOrEmpty(batchAdminRequestBean
/* 783 */       .getReturnInvoiceStatus()))
/*     */     {
/* 785 */       if (CommonValidator.validateData(batchAdminRequestBean
/* 786 */         .getReturnInvoiceStatus(), 
/* 787 */         com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE['À']))
/*     */       {
/* 789 */         if (CommonValidator.isValidReturnInvoiceStatus(batchAdminRequestBean
/* 790 */           .getReturnInvoiceStatus()))
/*     */         {
/* 792 */           String reqLength = CommonValidator.validateLength(
/* 793 */             batchAdminRequestBean.getReturnInvoiceStatus(), 2, 1);
/*     */           
/* 795 */           if (!reqLength.equals("greaterThanMax")) break label69;
/* 796 */           isValid = false;
/*     */           break label69;
/*     */         } }
/* 799 */       isValid = false;
/*     */     }
/*     */     
/*     */     label69:
/* 803 */     if (!isValid)
/*     */     {
/* 805 */       errorObj = new ErrorObject(
/* 806 */         XMLSubmissionErrorCodes.DCS_ERROR_INVALID_RETURN_INV_STATUS);
/* 807 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateInvoiceNumber(BatchAdminRequestBean batchAdminRequestBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 826 */     ErrorObject errorObj = null;
/* 827 */     boolean isValid = true;
/* 828 */     if (!CommonValidator.isNullOrEmpty(batchAdminRequestBean
/* 829 */       .getInvoiceNumber()))
/*     */     {
/* 831 */       if (CommonValidator.validateData(batchAdminRequestBean
/* 832 */         .getInvoiceNumber(), 
/* 833 */         com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE['Á']))
/*     */       {
/* 835 */         String reqLength = CommonValidator.validateLength(
/* 836 */           batchAdminRequestBean.getInvoiceNumber(), 13, 1);
/*     */         
/* 838 */         if (reqLength.equals("greaterThanMax")) {
/* 839 */           isValid = false;
/*     */         }
/*     */       }
/*     */       else {
/* 843 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/* 847 */     if (!isValid)
/*     */     {
/* 849 */       errorObj = new ErrorObject(
/* 850 */         XMLSubmissionErrorCodes.DCS_ERROR_INVALID_INVOICE_NBR);
/* 851 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void validateCardAcceptorDetail(BatchAdminRequestBean batchAdminRequestBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 868 */     ErrorObject errorObj = null;
/* 869 */     if (batchAdminRequestBean.getCardAcceptorDetail() == null)
/*     */     {
/* 871 */       errorObj = new ErrorObject(
/* 872 */         XMLSubmissionErrorCodes.ERROR_CARDTRANSDETAIL);
/* 873 */       errorCodes.add(errorObj);
/*     */     } else {
/* 875 */       validateCardAcptCityNm(
/* 876 */         batchAdminRequestBean.getCardAcceptorDetail(), errorCodes);
/* 877 */       validateCardAcptStreetNm(batchAdminRequestBean
/* 878 */         .getCardAcceptorDetail(), errorCodes);
/* 879 */       validateCardAcptNm(batchAdminRequestBean.getCardAcceptorDetail(), 
/* 880 */         errorCodes);
/* 881 */       validateCardAcptRgnCd(
/* 882 */         batchAdminRequestBean.getCardAcceptorDetail(), errorCodes);
/* 883 */       validateCardAcptCtryCd(batchAdminRequestBean
/* 884 */         .getCardAcceptorDetail(), errorCodes);
/* 885 */       validateCardAcptPostCd(batchAdminRequestBean
/* 886 */         .getCardAcceptorDetail(), errorCodes);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\validator\xml\BatchAdminRequestValidator.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */