/*     */ package com.americanexpress.ips.gfsg.validator.xml.industry;
/*     */ 
/*     */ import com.americanexpress.ips.gfsg.beans.ErrorObject;
/*     */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.TAAEntertainmentTktIndustryBean;
/*     */ import com.americanexpress.ips.gfsg.enumerations.XMLSubmissionErrorCodes;
/*     */ import com.americanexpress.ips.gfsg.exceptions.SettlementException;
/*     */ import com.americanexpress.ips.gfsg.validator.CommonValidator;
/*     */ import java.util.List;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class TAAEntTktIndusValidator
/*     */ {
/*     */   public static void validateEntTktIndustry(TAAEntertainmentTktIndustryBean taaEntertainmentTktIndustryBean, List<ErrorObject> errorCodes, String transCurrCd)
/*     */     throws SettlementException
/*     */   {
/*  37 */     validateEventNm(taaEntertainmentTktIndustryBean, errorCodes);
/*  38 */     validateEventDt(taaEntertainmentTktIndustryBean, errorCodes);
/*  39 */     validateEventTktPrcAmt(taaEntertainmentTktIndustryBean, errorCodes, transCurrCd);
/*  40 */     validateEventTktCnt(taaEntertainmentTktIndustryBean, errorCodes);
/*  41 */     validateEventLocNm(taaEntertainmentTktIndustryBean, errorCodes);
/*  42 */     validateEventRgnCd(taaEntertainmentTktIndustryBean, errorCodes);
/*  43 */     validateEventCtryCd(taaEntertainmentTktIndustryBean, errorCodes);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateEventNm(TAAEntertainmentTktIndustryBean taaEntertainmentTktIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/*  59 */     ErrorObject errorObj = null;
/*  60 */     boolean isValid = true;
/*  61 */     if (!CommonValidator.isNullOrEmpty(taaEntertainmentTktIndustryBean
/*  62 */       .getEventNm()))
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  68 */       String reqLength = CommonValidator.validateLength(
/*  69 */         taaEntertainmentTktIndustryBean.getEventNm(), 30, 1);
/*     */       
/*  71 */       if (reqLength.equals("greaterThanMax")) {
/*  72 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  81 */     if (!isValid)
/*     */     {
/*     */ 
/*  84 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_EVENTNM);
/*  85 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateEventDt(TAAEntertainmentTktIndustryBean taaEntertainmentTktIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 103 */     ErrorObject errorObj = null;
/* 104 */     boolean isValid = true;
/* 105 */     if (CommonValidator.isNullOrEmpty(taaEntertainmentTktIndustryBean
/* 106 */       .getEventDt())) {
/* 107 */       isValid = false;
/*     */ 
/*     */     }
/* 110 */     else if (CommonValidator.validateData(taaEntertainmentTktIndustryBean
/* 111 */       .getEventDt(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[125]))
/*     */     {
/* 113 */       if (!CommonValidator.isValidXmlDate(
/* 114 */         taaEntertainmentTktIndustryBean.getEventDt(), 
/* 115 */         "CCYYMMDD")) {
/* 116 */         isValid = false;
/*     */       }
/*     */     } else {
/* 119 */       isValid = false;
/*     */     }
/*     */     
/*     */ 
/* 123 */     if (!isValid)
/*     */     {
/* 125 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_EVENTDT);
/* 126 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateEventTktPrcAmt(TAAEntertainmentTktIndustryBean taaEntertainmentTktIndustryBean, List<ErrorObject> errorCodes, String transCurrCd)
/*     */     throws SettlementException
/*     */   {
/* 145 */     ErrorObject errorObj = null;
/* 146 */     boolean isValid = true;
/* 147 */     if (!CommonValidator.isNullOrEmpty(taaEntertainmentTktIndustryBean
/* 148 */       .getEventTktPrcAmt()))
/*     */     {
/* 150 */       if (CommonValidator.validateData(taaEntertainmentTktIndustryBean
/* 151 */         .getEventTktPrcAmt(), 
/* 152 */         com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[126]))
/*     */       {
/* 154 */         String reqLength = CommonValidator.validateLength(
/* 155 */           taaEntertainmentTktIndustryBean.getEventTktPrcAmt(), 
/* 156 */           12, 1);
/*     */         
/* 158 */         if (reqLength.equals("greaterThanMax")) {
/* 159 */           isValid = false;
/*     */         }
/* 161 */         else if ((!CommonValidator.isNullOrEmpty(transCurrCd)) && 
/* 162 */           (!CommonValidator.isValidXmlAmount(transCurrCd, taaEntertainmentTktIndustryBean.getEventTktPrcAmt()))) {
/* 163 */           isValid = false;
/*     */         }
/*     */       }
/*     */       else
/*     */       {
/* 168 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 173 */     if (!isValid)
/*     */     {
/* 175 */       errorObj = new ErrorObject(
/* 176 */         XMLSubmissionErrorCodes.ERROR_EVENTTKTPRCAMT);
/* 177 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateEventTktCnt(TAAEntertainmentTktIndustryBean taaEntertainmentTktIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 195 */     ErrorObject errorObj = null;
/* 196 */     boolean isValid = true;
/* 197 */     if (!CommonValidator.isNullOrEmpty(taaEntertainmentTktIndustryBean
/* 198 */       .getEventTktCnt()))
/*     */     {
/*     */ 
/* 201 */       if (CommonValidator.validateData(taaEntertainmentTktIndustryBean
/* 202 */         .getEventTktCnt(), 
/* 203 */         com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[127]))
/*     */       {
/* 205 */         String reqLength = CommonValidator.validateLength(
/* 206 */           taaEntertainmentTktIndustryBean.getEventTktCnt(), 4, 1);
/*     */         
/* 208 */         if (reqLength.equals("greaterThanMax")) {
/* 209 */           isValid = false;
/*     */         }
/*     */       }
/*     */       else
/*     */       {
/* 214 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 219 */     if (!isValid)
/*     */     {
/* 221 */       errorObj = new ErrorObject(
/* 222 */         XMLSubmissionErrorCodes.ERROR_EVENTTKTCNT);
/* 223 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateEventLocNm(TAAEntertainmentTktIndustryBean taaEntertainmentTktIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 241 */     ErrorObject errorObj = null;
/* 242 */     boolean isValid = true;
/* 243 */     if (!CommonValidator.isNullOrEmpty(taaEntertainmentTktIndustryBean
/* 244 */       .getEventLocNm()))
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 250 */       String reqLength = CommonValidator.validateLength(
/* 251 */         taaEntertainmentTktIndustryBean.getEventLocNm(), 40, 1);
/*     */       
/* 253 */       if (reqLength.equals("greaterThanMax")) {
/* 254 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 263 */     if (!isValid)
/*     */     {
/* 265 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_EVENTLOCNM);
/* 266 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateEventRgnCd(TAAEntertainmentTktIndustryBean taaEntertainmentTktIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 284 */     ErrorObject errorObj = null;
/* 285 */     boolean isValid = true;
/* 286 */     if (CommonValidator.isNullOrEmpty(taaEntertainmentTktIndustryBean
/* 287 */       .getEventRgnCd()))
/*     */     {
/* 289 */       if (!CommonValidator.isNullOrEmpty(taaEntertainmentTktIndustryBean
/* 290 */         .getEventCtryCd()))
/*     */       {
/* 292 */         isValid = false;
/*     */ 
/*     */       }
/*     */       
/*     */ 
/*     */     }
/* 298 */     else if (CommonValidator.validateData(taaEntertainmentTktIndustryBean
/* 299 */       .getEventRgnCd(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE['']))
/*     */     {
/*     */ 
/* 302 */       if (!CommonValidator.isNullOrEmpty(taaEntertainmentTktIndustryBean
/* 303 */         .getEventCtryCd()))
/*     */       {
/* 305 */         if (CommonValidator.isValidRegionCode(
/* 306 */           taaEntertainmentTktIndustryBean.getEventRgnCd(), 
/* 307 */           taaEntertainmentTktIndustryBean.getEventCtryCd()))
/*     */         {
/* 309 */           String reqLength = 
/* 310 */             CommonValidator.validateLength(taaEntertainmentTktIndustryBean
/* 311 */             .getEventRgnCd(), 3, 1);
/*     */           
/* 313 */           if (reqLength.equals("greaterThanMax")) {
/* 314 */             isValid = false;
/*     */           }
/*     */           
/*     */         }
/*     */         else
/*     */         {
/* 320 */           isValid = false;
/*     */         }
/*     */         
/*     */       }
/*     */       else {
/* 325 */         isValid = false;
/*     */       }
/*     */       
/*     */     }
/*     */     else {
/* 330 */       isValid = false;
/*     */     }
/*     */     
/*     */ 
/*     */ 
/* 335 */     if (!isValid)
/*     */     {
/* 337 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_EVENTRGNCD);
/* 338 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateEventCtryCd(TAAEntertainmentTktIndustryBean taaEntertainmentTktIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 356 */     ErrorObject errorObj = null;
/* 357 */     boolean isValid = true;
/* 358 */     if (CommonValidator.isNullOrEmpty(taaEntertainmentTktIndustryBean
/* 359 */       .getEventCtryCd()))
/*     */     {
/* 361 */       if (!CommonValidator.isNullOrEmpty(taaEntertainmentTktIndustryBean
/* 362 */         .getEventRgnCd()))
/*     */       {
/* 364 */         isValid = false;
/*     */       }
/*     */       
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/* 371 */       if (CommonValidator.validateData(taaEntertainmentTktIndustryBean
/* 372 */         .getEventCtryCd(), 
/* 373 */         com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE['']))
/*     */       {
/* 375 */         if (CommonValidator.isValidXmlCountryCode(taaEntertainmentTktIndustryBean
/* 376 */           .getEventCtryCd()))
/*     */         {
/* 378 */           String reqLength = CommonValidator.validateLength(
/* 379 */             taaEntertainmentTktIndustryBean.getEventCtryCd(), 3, 1);
/*     */           
/* 381 */           if (!reqLength.equals("greaterThanMax")) break label84;
/* 382 */           isValid = false;
/*     */           break label84;
/*     */         }
/*     */       }
/* 386 */       isValid = false;
/*     */     }
/*     */     
/*     */     label84:
/*     */     
/* 391 */     if (!isValid)
/*     */     {
/* 393 */       errorObj = new ErrorObject(
/* 394 */         XMLSubmissionErrorCodes.ERROR_EVENTCTRYCD);
/* 395 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\validator\xml\industry\TAAEntTktIndusValidator.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */