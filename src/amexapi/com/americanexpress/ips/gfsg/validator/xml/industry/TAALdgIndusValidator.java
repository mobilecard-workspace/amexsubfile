/*     */ package com.americanexpress.ips.gfsg.validator.xml.industry;
/*     */ 
/*     */ import com.americanexpress.ips.gfsg.beans.ErrorObject;
/*     */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.LodgeRoomInformationBean;
/*     */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.TAALodgingIndustryBean;
/*     */ import com.americanexpress.ips.gfsg.enumerations.XMLSubmissionErrorCodes;
/*     */ import com.americanexpress.ips.gfsg.exceptions.SettlementException;
/*     */ import com.americanexpress.ips.gfsg.validator.CommonValidator;
/*     */ import java.util.List;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class TAALdgIndusValidator
/*     */ {
/*     */   public static void validateLodgingIndustry(TAALodgingIndustryBean taaLodgingIndustryBean, List<ErrorObject> errorCodes, String transCurrCd)
/*     */     throws SettlementException
/*     */   {
/*  35 */     validateLdgSpecProgCd(taaLodgingIndustryBean, errorCodes);
/*     */     
/*  37 */     validateLdgCheckOutDt(taaLodgingIndustryBean, errorCodes);
/*  38 */     validateLdgRoomInfo(taaLodgingIndustryBean, errorCodes, transCurrCd);
/*  39 */     validateLdgRentNm(taaLodgingIndustryBean, errorCodes);
/*  40 */     validateLdgFolioNbr(taaLodgingIndustryBean, errorCodes);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateLdgSpecProgCd(TAALodgingIndustryBean taaLodgingIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/*  58 */     ErrorObject errorObj = null;
/*  59 */     boolean isValid = true;
/*  60 */     if (CommonValidator.isNullOrEmpty(taaLodgingIndustryBean.getLdgSpecProgCd())) {
/*  61 */       isValid = false;
/*     */     }
/*  63 */     else if ((CommonValidator.validateData(taaLodgingIndustryBean
/*  64 */       .getLdgSpecProgCd(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[''])) && (CommonValidator.isValidLdgSpecProgCd(taaLodgingIndustryBean.getLdgSpecProgCd())))
/*     */     {
/*  66 */       String reqLength = CommonValidator.validateLength(
/*  67 */         taaLodgingIndustryBean.getLdgSpecProgCd(), 1, 1);
/*     */       
/*  69 */       if (reqLength.equals("greaterThanMax")) {
/*  70 */         isValid = false;
/*     */       }
/*     */     } else {
/*  73 */       isValid = false;
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*  78 */     if (!isValid)
/*     */     {
/*     */ 
/*  81 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_LDGSPECPROGCD);
/*  82 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static boolean validateLdgCheckInDt(String checkInDt)
/*     */     throws SettlementException
/*     */   {
/*  97 */     boolean isValid = true;
/*  98 */     if (CommonValidator.isNullOrEmpty(checkInDt)) {
/*  99 */       isValid = false;
/*     */ 
/*     */     }
/* 102 */     else if (CommonValidator.validateData(checkInDt, com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE['']))
/*     */     {
/* 104 */       if (!CommonValidator.isValidXmlDate(checkInDt, "CCYYMMDD")) {
/* 105 */         isValid = false;
/*     */       }
/*     */     } else {
/* 108 */       isValid = false;
/*     */     }
/*     */     
/* 111 */     return isValid;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateLdgCheckOutDt(TAALodgingIndustryBean taaLodgingIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 127 */     ErrorObject errorObj = null;
/* 128 */     boolean isValid = true;
/* 129 */     boolean checkInDt = true;
/* 130 */     checkInDt = validateLdgCheckInDt(taaLodgingIndustryBean.getLdgCheckInDt());
/* 131 */     if (CommonValidator.isNullOrEmpty(taaLodgingIndustryBean.getLdgCheckOutDt())) {
/* 132 */       isValid = false;
/*     */ 
/*     */ 
/*     */     }
/* 136 */     else if (CommonValidator.validateData(taaLodgingIndustryBean.getLdgCheckOutDt(), 
/* 137 */       com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[''])) {
/* 138 */       if (CommonValidator.isValidXmlDate(taaLodgingIndustryBean.getLdgCheckOutDt(), "CCYYMMDD")) {
/* 139 */         if (checkInDt) {
/* 140 */           int lodgCheckOutDt = Integer.parseInt(taaLodgingIndustryBean.getLdgCheckOutDt());
/* 141 */           int lodgCheckInDt = Integer.parseInt(taaLodgingIndustryBean.getLdgCheckInDt());
/* 142 */           if (lodgCheckOutDt < lodgCheckInDt) {
/* 143 */             isValid = false;
/*     */           }
/*     */         }
/*     */       } else {
/* 147 */         isValid = false;
/*     */       }
/*     */     } else {
/* 150 */       isValid = false;
/*     */     }
/*     */     
/*     */ 
/* 154 */     if (!isValid)
/*     */     {
/* 156 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_LODGINGLDGCHECKOUTDT);
/* 157 */       errorCodes.add(errorObj);
/*     */     }
/*     */     
/* 160 */     if (!checkInDt)
/*     */     {
/* 162 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_LODGINGLDGCHECKINDT);
/* 163 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateLdgRmRate(LodgeRoomInformationBean lodgeRoomInformationBean, List<ErrorObject> errorCodes, String transCurrCd)
/*     */     throws SettlementException
/*     */   {
/* 183 */     ErrorObject errorObj = null;
/* 184 */     boolean isValid = true;
/* 185 */     if (!CommonValidator.isNullOrEmpty(lodgeRoomInformationBean.getLdgRmRate()))
/*     */     {
/*     */ 
/* 188 */       if (CommonValidator.validateData(lodgeRoomInformationBean.getLdgRmRate(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE['']))
/*     */       {
/* 190 */         String reqLength = CommonValidator.validateLength(
/* 191 */           lodgeRoomInformationBean.getLdgRmRate(), 12, 1);
/*     */         
/* 193 */         if (reqLength.equals("greaterThanMax")) {
/* 194 */           isValid = false;
/*     */         }
/* 196 */         else if ((!CommonValidator.isNullOrEmpty(transCurrCd)) && 
/* 197 */           (!CommonValidator.isValidXmlAmount(transCurrCd, lodgeRoomInformationBean.getLdgRmRate()))) {
/* 198 */           isValid = false;
/*     */         }
/*     */       }
/*     */       else
/*     */       {
/* 203 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 208 */     if (!isValid)
/*     */     {
/* 210 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_LODGINGLDGRMRATE);
/* 211 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateNbrOfNgtCnt(LodgeRoomInformationBean lodgeRoomInformationBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 229 */     ErrorObject errorObj = null;
/* 230 */     boolean isValid = true;
/* 231 */     if (!CommonValidator.isNullOrEmpty(lodgeRoomInformationBean.getNbrOfNgtCnt()))
/*     */     {
/*     */ 
/* 234 */       if (CommonValidator.validateData(lodgeRoomInformationBean.getNbrOfNgtCnt(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE['']))
/*     */       {
/* 236 */         String reqLength = CommonValidator.validateLength(
/* 237 */           lodgeRoomInformationBean.getNbrOfNgtCnt(), 2, 1);
/*     */         
/* 239 */         if (reqLength.equals("greaterThanMax")) {
/* 240 */           isValid = false;
/*     */         }
/*     */       } else {
/* 243 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 248 */     if (!isValid)
/*     */     {
/*     */ 
/* 251 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_LODGINGNBROFNGTCNT);
/* 252 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateLdgRentNm(TAALodgingIndustryBean taaLodgingIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 269 */     ErrorObject errorObj = null;
/* 270 */     boolean isValid = true;
/* 271 */     if (!CommonValidator.isNullOrEmpty(taaLodgingIndustryBean.getLdgRentNm()))
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 277 */       String reqLength = CommonValidator.validateLength(
/* 278 */         taaLodgingIndustryBean.getLdgRentNm(), 26, 1);
/*     */       
/* 280 */       if (reqLength.equals("greaterThanMax")) {
/* 281 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 289 */     if (!isValid)
/*     */     {
/* 291 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_LDGRENTNM);
/* 292 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateLdgFolioNbr(TAALodgingIndustryBean taaLodgingIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 311 */     ErrorObject errorObj = null;
/* 312 */     boolean isValid = true;
/* 313 */     if (CommonValidator.isNullOrEmpty(taaLodgingIndustryBean.getLdgFolioNbr())) {
/* 314 */       isValid = false;
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/* 320 */       String reqLength = CommonValidator.validateLength(
/* 321 */         taaLodgingIndustryBean.getLdgFolioNbr(), 12, 1);
/*     */       
/* 323 */       if (reqLength.equals("greaterThanMax")) {
/* 324 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 332 */     if (!isValid)
/*     */     {
/* 334 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_LDGFOLIONBR);
/* 335 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateLdgRoomInfo(TAALodgingIndustryBean taaLodgingIndustryBean, List<ErrorObject> errorCodes, String transCurrCd)
/*     */     throws SettlementException
/*     */   {
/* 354 */     if ((taaLodgingIndustryBean.getLodgeRoomInformationBean() != null) && (!taaLodgingIndustryBean.getLodgeRoomInformationBean().isEmpty())) {
/* 355 */       for (LodgeRoomInformationBean lodgeRoomInformation : taaLodgingIndustryBean.getLodgeRoomInformationBean())
/*     */       {
/* 357 */         validateLdgRmRate(lodgeRoomInformation, errorCodes, transCurrCd);
/* 358 */         validateNbrOfNgtCnt(lodgeRoomInformation, errorCodes);
/*     */       }
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\validator\xml\industry\TAALdgIndusValidator.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */