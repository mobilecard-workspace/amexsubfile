/*     */ package com.americanexpress.ips.gfsg.validator.xml.industry;
/*     */ 
/*     */ import com.americanexpress.ips.gfsg.beans.ErrorObject;
/*     */ import com.americanexpress.ips.gfsg.beans.common.TransactionAdviceBasicBean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.InsuranceIndustryTAABean;
/*     */ import com.americanexpress.ips.gfsg.enumerations.SubmissionErrorCodes;
/*     */ import com.americanexpress.ips.gfsg.exceptions.SettlementException;
/*     */ import com.americanexpress.ips.gfsg.validator.CommonValidator;
/*     */ import java.util.List;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class INSRNSIndRecordValidator
/*     */ {
/*     */   public static void validateINSRNSIndRecord(TransactionAdviceBasicBean transactionAdviceBasicType, List<Object> errorCodes)
/*     */     throws SettlementException
/*     */   {
/*  32 */     InsuranceIndustryTAABean insuranceIndustryTAABean = (InsuranceIndustryTAABean)transactionAdviceBasicType.getTransactionAdviceAddendumBean();
/*     */     
/*  34 */     validateInsurancePolicyNumber(insuranceIndustryTAABean.getInsurancePolicyNumber(), errorCodes);
/*  35 */     validateInsuranceCoverageStartDate(insuranceIndustryTAABean.getInsuranceCoverageStartDate(), errorCodes);
/*  36 */     validateInsuranceCoverageEndDate(insuranceIndustryTAABean, errorCodes);
/*  37 */     validateInsurancePolicyPremiumFrequency(insuranceIndustryTAABean.getInsurancePolicyPremiumFrequency(), errorCodes);
/*  38 */     validateAdditionalInsurancePolicyNumber(insuranceIndustryTAABean.getAdditionalInsurancePolicyNumber(), errorCodes);
/*  39 */     validateTypeOfPolicy(insuranceIndustryTAABean.getTypeOfPolicy(), errorCodes);
/*  40 */     validateNameOfInsured(insuranceIndustryTAABean.getNameOfInsured(), errorCodes);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateInsurancePolicyNumber(String value, List<Object> errorCodes)
/*     */   {
/*  55 */     ErrorObject errorObj = null;
/*  56 */     boolean isValid = true;
/*  57 */     if (CommonValidator.isNullOrEmpty(value)) {
/*  58 */       isValid = false;
/*     */ 
/*     */ 
/*     */ 
/*     */     }
/*  63 */     else if (CommonValidator.validateData(value, com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ť']))
/*     */     {
/*  65 */       String reqLength = CommonValidator.validateLength(value, 23, 23);
/*     */       
/*  67 */       if (reqLength.equals("greaterThanMax")) {
/*  68 */         isValid = false;
/*     */       }
/*     */     }
/*     */     else {
/*  72 */       isValid = false;
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*  77 */     if (!isValid)
/*     */     {
/*  79 */       errorObj = new ErrorObject(SubmissionErrorCodes.ERROR_DATAFIELD389_INSURANCE_POLICYNUMBER);
/*  80 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateInsuranceCoverageStartDate(String value, List<Object> errorCodes)
/*     */   {
/*  96 */     ErrorObject errorObj = null;
/*  97 */     boolean isValid = true;
/*  98 */     if (CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 100 */       errorObj = new ErrorObject(SubmissionErrorCodes.ERROR_DATAFIELD390_INSURANCECOVERAGE_STARTDATE);
/* 101 */       errorCodes.add(errorObj);
/*     */ 
/*     */ 
/*     */ 
/*     */     }
/* 106 */     else if (CommonValidator.validateData(value, com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ť']))
/*     */     {
/* 108 */       String reqLength = CommonValidator.validateLength(value, 8, 8);
/*     */       
/* 110 */       if (reqLength.equals("greaterThanMax")) {
/* 111 */         isValid = false;
/*     */       }
/*     */     }
/*     */     else {
/* 115 */       isValid = false;
/*     */     }
/*     */     
/*     */ 
/*     */ 
/* 120 */     if (!isValid)
/*     */     {
/* 122 */       errorObj = new ErrorObject(SubmissionErrorCodes.ERROR_DATAFIELD392_INSURANCECOVERAGE_STARTDATE);
/* 123 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateInsuranceCoverageEndDate(InsuranceIndustryTAABean insuranceIndustryTAABean, List<Object> errorCodes)
/*     */   {
/* 139 */     ErrorObject errorObj = null;
/* 140 */     boolean isValid = true;
/* 141 */     if (CommonValidator.isNullOrEmpty(insuranceIndustryTAABean.getInsuranceCoverageEndDate())) {
/* 142 */       isValid = false;
/*     */ 
/*     */ 
/*     */ 
/*     */     }
/* 147 */     else if (CommonValidator.validateData(insuranceIndustryTAABean.getInsuranceCoverageEndDate(), com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ŧ']))
/*     */     {
/* 149 */       String reqLength = CommonValidator.validateLength(insuranceIndustryTAABean.getInsuranceCoverageEndDate(), 8, 8);
/*     */       
/* 151 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/* 153 */         if (Integer.parseInt(insuranceIndustryTAABean.getInsuranceCoverageEndDate()) < Integer.parseInt(insuranceIndustryTAABean.getInsuranceCoverageStartDate())) {
/* 154 */           isValid = false;
/*     */         }
/*     */         
/*     */       }
/*     */       else {
/* 159 */         isValid = false;
/*     */       }
/*     */     }
/*     */     else {
/* 163 */       isValid = false;
/*     */     }
/*     */     
/*     */ 
/*     */ 
/* 168 */     if (!isValid)
/*     */     {
/* 170 */       errorObj = new ErrorObject(SubmissionErrorCodes.ERROR_DATAFIELD395_INSURANCECOVERAGE_ENDDATE);
/* 171 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateInsurancePolicyPremiumFrequency(String value, List<Object> errorCodes)
/*     */   {
/* 186 */     ErrorObject errorObj = null;
/* 187 */     boolean isValid = true;
/* 188 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 190 */       if (CommonValidator.validateData(value, com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ŧ']))
/*     */       {
/* 192 */         String reqLength = CommonValidator.validateLength(value, 7, 7);
/*     */         
/* 194 */         if (!reqLength.equals("greaterThanMax")) {
/* 195 */           isValid = false;
/*     */         }
/*     */       }
/*     */       else {
/* 199 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 204 */     if (!isValid)
/*     */     {
/* 206 */       errorObj = new ErrorObject(SubmissionErrorCodes.ERROR_DATAFIELD397_INSURANCEPOLICY_PREMIUMFREQUENCY);
/* 207 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateAdditionalInsurancePolicyNumber(String value, List<Object> errorCodes)
/*     */   {
/* 223 */     ErrorObject errorObj = null;
/* 224 */     boolean isValid = true;
/* 225 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/*     */ 
/* 228 */       if (CommonValidator.validateData(value, com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ũ']))
/*     */       {
/* 230 */         String reqLength = CommonValidator.validateLength(value, 23, 23);
/*     */         
/* 232 */         if (!reqLength.equals("greaterThanMax")) {
/* 233 */           isValid = false;
/*     */         }
/*     */       }
/*     */       else {
/* 237 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 242 */     if (!isValid)
/*     */     {
/* 244 */       errorObj = new ErrorObject(SubmissionErrorCodes.ERROR_DATAFIELD399_ADDITIONALINSURANCE_POLICYNUMBER);
/* 245 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateTypeOfPolicy(String value, List<Object> errorCodes)
/*     */   {
/* 260 */     ErrorObject errorObj = null;
/* 261 */     boolean isValid = true;
/* 262 */     if (CommonValidator.isNullOrEmpty(value)) {
/* 263 */       isValid = false;
/*     */ 
/*     */     }
/* 266 */     else if (CommonValidator.validateData(value, com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ũ']))
/*     */     {
/* 268 */       String reqLength = CommonValidator.validateLength(value, 25, 25);
/*     */       
/* 270 */       if (!reqLength.equals("greaterThanMax")) {
/* 271 */         isValid = false;
/*     */       }
/*     */     }
/*     */     else {
/* 275 */       isValid = false;
/*     */     }
/*     */     
/*     */ 
/*     */ 
/* 280 */     if (!isValid)
/*     */     {
/* 282 */       errorObj = new ErrorObject(SubmissionErrorCodes.ERROR_DATAFIELD402_TYPEOF_POLICY);
/* 283 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateNameOfInsured(String value, List<Object> errorCodes)
/*     */   {
/* 298 */     ErrorObject errorObj = null;
/* 299 */     boolean isValid = true;
/* 300 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 302 */       if (CommonValidator.validateData(value, com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ū']))
/*     */       {
/* 304 */         String reqLength = CommonValidator.validateLength(value, 30, 30);
/*     */         
/* 306 */         if (!reqLength.equals("greaterThanMax")) {
/* 307 */           isValid = false;
/*     */         }
/*     */       }
/*     */       else {
/* 311 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/* 315 */     if (!isValid)
/*     */     {
/* 317 */       errorObj = new ErrorObject(SubmissionErrorCodes.ERROR_DATAFIELD402_NAMEOF_INSURED);
/* 318 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\validator\xml\industry\INSRNSIndRecordValidator.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */