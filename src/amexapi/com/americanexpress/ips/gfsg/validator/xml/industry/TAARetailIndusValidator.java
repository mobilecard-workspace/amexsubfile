/*     */ package com.americanexpress.ips.gfsg.validator.xml.industry;
/*     */ 
/*     */ import com.americanexpress.ips.gfsg.beans.ErrorObject;
/*     */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.RetailItemInfoBean;
/*     */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.TAARetailIndustryBean;
/*     */ import com.americanexpress.ips.gfsg.enumerations.XMLSubmissionErrorCodes;
/*     */ import com.americanexpress.ips.gfsg.exceptions.SettlementException;
/*     */ import com.americanexpress.ips.gfsg.validator.CommonValidator;
/*     */ import java.util.List;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class TAARetailIndusValidator
/*     */ {
/*     */   public static void validateRetailIndustry(TAARetailIndustryBean taaRetailIndustryBean, List<ErrorObject> errorCodes, String transCurrCd)
/*     */     throws SettlementException
/*     */   {
/*  37 */     validateRetailDeptNm(taaRetailIndustryBean, errorCodes);
/*  38 */     validateRetailItemInfo(taaRetailIndustryBean, errorCodes, transCurrCd);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateRetailDeptNm(TAARetailIndustryBean taaRetailIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/*  57 */     ErrorObject errorObj = null;
/*  58 */     boolean isValid = true;
/*  59 */     if (CommonValidator.isNullOrEmpty(taaRetailIndustryBean.getRetailDeptNm())) {
/*  60 */       isValid = false;
/*     */ 
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/*     */ 
/*  68 */       String reqLength = CommonValidator.validateLength(
/*  69 */         taaRetailIndustryBean.getRetailDeptNm(), 40, 1);
/*     */       
/*  71 */       if (reqLength.equals("greaterThanMax")) {
/*  72 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*  79 */     if (!isValid)
/*     */     {
/*  81 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_RETAILDEPTNM);
/*  82 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateRetailItemDesc(RetailItemInfoBean retailItemInfoBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 101 */     ErrorObject errorObj = null;
/* 102 */     boolean isValid = true;
/* 103 */     if (CommonValidator.isNullOrEmpty(retailItemInfoBean.getRetailItemDesc())) {
/* 104 */       isValid = false;
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/* 109 */       String reqLength = CommonValidator.validateLength(
/* 110 */         retailItemInfoBean.getRetailItemDesc(), 19, 1);
/*     */       
/* 112 */       if (reqLength.equals("greaterThanMax")) {
/* 113 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 120 */     if (!isValid)
/*     */     {
/* 122 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_RETAILITEMDESC);
/* 123 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateRetailItemQty(RetailItemInfoBean retailItemInfoBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 142 */     ErrorObject errorObj = null;
/* 143 */     boolean isValid = true;
/* 144 */     if (!CommonValidator.isNullOrEmpty(retailItemInfoBean.getRetailItemQty()))
/*     */     {
/*     */ 
/* 147 */       if (CommonValidator.validateData(retailItemInfoBean.getRetailItemQty(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE['']))
/*     */       {
/* 149 */         String reqLength = CommonValidator.validateLength(
/* 150 */           retailItemInfoBean.getRetailItemQty(), 3, 1);
/*     */         
/* 152 */         if (reqLength.equals("greaterThanMax")) {
/* 153 */           isValid = false;
/*     */         }
/*     */       } else {
/* 156 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/* 160 */     if (!isValid)
/*     */     {
/* 162 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_RETAILITEMQTY);
/* 163 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateRetailItemAmt(RetailItemInfoBean retailItemInfoBean, List<ErrorObject> errorCodes, String transCurrCd)
/*     */     throws SettlementException
/*     */   {
/* 183 */     ErrorObject errorObj = null;
/* 184 */     boolean isValid = true;
/* 185 */     if (!CommonValidator.isNullOrEmpty(retailItemInfoBean.getRetailItemAmt()))
/*     */     {
/*     */ 
/* 188 */       if (CommonValidator.validateData(retailItemInfoBean.getRetailItemAmt(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE['']))
/*     */       {
/* 190 */         String reqLength = CommonValidator.validateLength(
/* 191 */           retailItemInfoBean.getRetailItemAmt(), 12, 1);
/*     */         
/* 193 */         if (reqLength.equals("greaterThanMax")) {
/* 194 */           isValid = false;
/*     */         }
/* 196 */         else if ((!CommonValidator.isNullOrEmpty(transCurrCd)) && 
/* 197 */           (!CommonValidator.isValidXmlAmount(transCurrCd, retailItemInfoBean.getRetailItemAmt()))) {
/* 198 */           isValid = false;
/*     */         }
/*     */       }
/*     */       else
/*     */       {
/* 203 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/* 207 */     if (!isValid)
/*     */     {
/* 209 */       errorObj = new ErrorObject(
/* 210 */         XMLSubmissionErrorCodes.ERROR_RETAILITEMAMT);
/* 211 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateRetailItemInfo(TAARetailIndustryBean taaRetailIndustryBean, List<ErrorObject> errorCodes, String transCurrCd)
/*     */     throws SettlementException
/*     */   {
/* 229 */     ErrorObject errorObj = null;
/*     */     
/* 231 */     if ((taaRetailIndustryBean.getRetailItemInfoBean() == null) || (taaRetailIndustryBean.getRetailItemInfoBean().isEmpty())) {
/* 232 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_RETAILITEMINFO);
/* 233 */       errorCodes.add(errorObj);
/*     */     } else {
/* 235 */       for (RetailItemInfoBean retailItemInfo : taaRetailIndustryBean.getRetailItemInfoBean())
/*     */       {
/* 237 */         validateRetailItemDesc(retailItemInfo, errorCodes);
/* 238 */         validateRetailItemQty(retailItemInfo, errorCodes);
/* 239 */         validateRetailItemAmt(retailItemInfo, errorCodes, transCurrCd);
/*     */       }
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\validator\xml\industry\TAARetailIndusValidator.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */