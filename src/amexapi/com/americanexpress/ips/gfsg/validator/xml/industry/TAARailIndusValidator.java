/*     */ package com.americanexpress.ips.gfsg.validator.xml.industry;
/*     */ 
/*     */ import com.americanexpress.ips.gfsg.beans.ErrorObject;
/*     */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.RailSegInfoBean;
/*     */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.TAARailIndustryBean;
/*     */ import com.americanexpress.ips.gfsg.enumerations.XMLSubmissionErrorCodes;
/*     */ import com.americanexpress.ips.gfsg.exceptions.SettlementException;
/*     */ import com.americanexpress.ips.gfsg.validator.CommonValidator;
/*     */ import java.util.List;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class TAARailIndusValidator
/*     */ {
/*     */   public static void validateRailIndustry(TAARailIndustryBean taaRailIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/*  34 */     validateTravTransTypeCd(taaRailIndustryBean, errorCodes);
/*  35 */     validateTravTktNbr(taaRailIndustryBean, errorCodes);
/*  36 */     validatePassNm(taaRailIndustryBean, errorCodes);
/*  37 */     validateIATACarrierCd(taaRailIndustryBean, errorCodes);
/*  38 */     validateTktIssNm(taaRailIndustryBean, errorCodes);
/*  39 */     validateTktIssCityNm(taaRailIndustryBean, errorCodes);
/*  40 */     validateRailSegInfo(taaRailIndustryBean, errorCodes);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateTravTransTypeCd(TAARailIndustryBean taaRailIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/*  57 */     ErrorObject errorObj = null;
/*  58 */     boolean isValid = true;
/*  59 */     if (CommonValidator.isNullOrEmpty(taaRailIndustryBean.getTravTransTypeCd()))
/*     */     {
/*     */ 
/*  62 */       isValid = false;
/*     */     }
/*  64 */     else if ((CommonValidator.validateData(taaRailIndustryBean.getTravTransTypeCd(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[''])) && (CommonValidator.isValidTravTransTypeCd(taaRailIndustryBean.getTravTransTypeCd())))
/*     */     {
/*  66 */       String reqLength = CommonValidator.validateLength(
/*  67 */         taaRailIndustryBean.getTravTransTypeCd(), 3, 3);
/*     */       
/*  69 */       if (reqLength.equals("greaterThanMax")) {
/*  70 */         isValid = false;
/*     */         
/*  72 */         errorObj = new ErrorObject(
/*  73 */           XMLSubmissionErrorCodes.ERROR_TRAVTRANSTYPECD);
/*  74 */         errorCodes.add(errorObj);
/*  75 */       } else if (reqLength.equals("lessThanMin")) {
/*  76 */         isValid = false;
/*     */       }
/*     */     } else {
/*  79 */       isValid = false;
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*  84 */     if (!isValid)
/*     */     {
/*  86 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_TRAVTRANSTYPECD);
/*  87 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateTravTktNbr(TAARailIndustryBean taaRailIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 105 */     ErrorObject errorObj = null;
/* 106 */     boolean isValid = true;
/* 107 */     if (CommonValidator.isNullOrEmpty(taaRailIndustryBean.getTktNbr())) {
/* 108 */       isValid = false;
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/* 113 */       String reqLength = CommonValidator.validateLength(
/* 114 */         taaRailIndustryBean.getTktNbr(), 14, 1);
/*     */       
/* 116 */       if (reqLength.equals("greaterThanMax")) {
/* 117 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 124 */     if (!isValid)
/*     */     {
/* 126 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_LODGINGTKTNBR);
/* 127 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validatePassNm(TAARailIndustryBean taaRailIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 145 */     ErrorObject errorObj = null;
/* 146 */     boolean isValid = true;
/* 147 */     if (CommonValidator.isNullOrEmpty(taaRailIndustryBean.getPassNm())) {
/* 148 */       isValid = false;
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/* 153 */       String reqLength = CommonValidator.validateLength(
/* 154 */         taaRailIndustryBean.getPassNm(), 25, 1);
/*     */       
/* 156 */       if (reqLength.equals("greaterThanMax")) {
/* 157 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 164 */     if (!isValid)
/*     */     {
/* 166 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_LODGINGPASSNM);
/* 167 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateIATACarrierCd(TAARailIndustryBean taaRailIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 186 */     ErrorObject errorObj = null;
/* 187 */     boolean isValid = true;
/*     */     
/* 189 */     if (!CommonValidator.isNullOrEmpty(taaRailIndustryBean.getIATACarrierCd()))
/*     */     {
/*     */ 
/* 192 */       if (CommonValidator.validateData(taaRailIndustryBean.getIATACarrierCd(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE['']))
/*     */       {
/* 194 */         String reqLength = CommonValidator.validateLength(
/* 195 */           taaRailIndustryBean.getIATACarrierCd(), 3, 2);
/*     */         
/* 197 */         if (reqLength.equals("greaterThanMax")) {
/* 198 */           isValid = false;
/*     */         }
/*     */       } else {
/* 201 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/* 205 */     if (!isValid)
/*     */     {
/* 207 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_RAILIATACARRIERCD);
/* 208 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateTktIssNm(TAARailIndustryBean taaRailIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 226 */     ErrorObject errorObj = null;
/* 227 */     boolean isValid = true;
/* 228 */     if (!CommonValidator.isNullOrEmpty(taaRailIndustryBean.getTktIssNm()))
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/* 233 */       String reqLength = CommonValidator.validateLength(
/* 234 */         taaRailIndustryBean.getTktIssNm(), 32, 1);
/*     */       
/* 236 */       if (reqLength.equals("greaterThanMax")) {
/* 237 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 245 */     if (!isValid)
/*     */     {
/* 247 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_TKTISSNM);
/* 248 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateTktIssCityNm(TAARailIndustryBean taaRailIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 267 */     ErrorObject errorObj = null;
/* 268 */     boolean isValid = true;
/* 269 */     if (!CommonValidator.isNullOrEmpty(taaRailIndustryBean.getTktIssCityNm()))
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/* 274 */       String reqLength = CommonValidator.validateLength(
/* 275 */         taaRailIndustryBean.getTktIssCityNm(), 18, 1);
/*     */       
/* 277 */       if (reqLength.equals("greaterThanMax")) {
/* 278 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 285 */     if (!isValid)
/*     */     {
/* 287 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_TKTISSCITYNM);
/* 288 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateDprtStnCd(RailSegInfoBean railSegInfoBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 306 */     ErrorObject errorObj = null;
/* 307 */     boolean isValid = true;
/* 308 */     if (!CommonValidator.isNullOrEmpty(railSegInfoBean.getDprtStnCd()))
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/* 313 */       String reqLength = CommonValidator.validateLength(
/* 314 */         railSegInfoBean.getDprtStnCd(), 3, 3);
/*     */       
/* 316 */       if (reqLength.equals("greaterThanMax")) {
/* 317 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 324 */     if (!isValid)
/*     */     {
/* 326 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_DPRTSTNCD);
/* 327 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateDprtDt(RailSegInfoBean railSegInfoBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 344 */     ErrorObject errorObj = null;
/* 345 */     boolean isValid = true;
/* 346 */     if (!CommonValidator.isNullOrEmpty(railSegInfoBean.getDprtDt()))
/*     */     {
/* 348 */       if (CommonValidator.validateData(railSegInfoBean.getDprtDt(), 
/* 349 */         com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE['']))
/*     */       {
/* 351 */         String reqLength = CommonValidator.validateLength(
/* 352 */           railSegInfoBean.getDprtDt(), 8, 8);
/*     */         
/* 354 */         if ((reqLength.equals("greaterThanMax")) || (reqLength.equals("lessThanMin"))) {
/* 355 */           isValid = false;
/*     */         }
/* 357 */         else if (!CommonValidator.isValidXmlDate(railSegInfoBean.getDprtDt(), "CCYYMMDD")) {
/* 358 */           isValid = false;
/*     */         }
/*     */       }
/*     */       else {
/* 362 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 367 */     if (!isValid)
/*     */     {
/* 369 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_LODGINGDPRTDT);
/* 370 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateArrStnCd(RailSegInfoBean railSegInfoBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 389 */     ErrorObject errorObj = null;
/* 390 */     boolean isValid = true;
/* 391 */     if (!CommonValidator.isNullOrEmpty(railSegInfoBean.getArrStnCd()))
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/* 396 */       String reqLength = CommonValidator.validateLength(
/* 397 */         railSegInfoBean.getArrStnCd(), 3, 3);
/*     */       
/* 399 */       if (reqLength.equals("greaterThanMax")) {
/* 400 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 408 */     if (!isValid)
/*     */     {
/* 410 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_ARRSTNCD);
/* 411 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateRailSegInfo(TAARailIndustryBean taaRailIndustryBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 430 */     if ((taaRailIndustryBean.getRailSegInfoBean() != null) && (!taaRailIndustryBean.getRailSegInfoBean().isEmpty())) {
/* 431 */       for (RailSegInfoBean railSegInfo : taaRailIndustryBean.getRailSegInfoBean())
/*     */       {
/*     */ 
/* 434 */         validateDprtStnCd(railSegInfo, errorCodes);
/* 435 */         validateDprtDt(railSegInfo, errorCodes);
/* 436 */         validateArrStnCd(railSegInfo, errorCodes);
/*     */       }
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\validator\xml\industry\TAARailIndusValidator.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */