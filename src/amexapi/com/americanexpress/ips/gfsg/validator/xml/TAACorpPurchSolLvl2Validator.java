/*     */ package com.americanexpress.ips.gfsg.validator.xml;
/*     */ 
/*     */ import com.americanexpress.ips.gfsg.beans.ErrorObject;
/*     */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.ChargeItemInfoBean;
/*     */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.TAACorpPurchSolLvl2Bean;
/*     */ import com.americanexpress.ips.gfsg.enumerations.XMLSubmissionErrorCodes;
/*     */ import com.americanexpress.ips.gfsg.exceptions.SettlementException;
/*     */ import com.americanexpress.ips.gfsg.validator.CommonValidator;
/*     */ import java.io.PrintStream;
/*     */ import java.util.Iterator;
/*     */ import java.util.List;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class TAACorpPurchSolLvl2Validator
/*     */ {
/*     */   public static void validateTAACorpPurchSolLvl2(TAACorpPurchSolLvl2Bean taaCorpPurchSolLvl2, List<ErrorObject> errorCodes, String transCurrCd)
/*     */     throws SettlementException
/*     */   {
/*  38 */     validateReqNm(taaCorpPurchSolLvl2, errorCodes);
/*  39 */     validateCMRefNbr(taaCorpPurchSolLvl2, errorCodes);
/*  40 */     validateShipToPostCd(taaCorpPurchSolLvl2, errorCodes);
/*  41 */     validateTaxTotAmt(taaCorpPurchSolLvl2, errorCodes, transCurrCd);
/*  42 */     validateTaxTypeCd(taaCorpPurchSolLvl2, errorCodes);
/*  43 */     validateChrgItemInfo(taaCorpPurchSolLvl2, errorCodes, transCurrCd);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateReqNm(TAACorpPurchSolLvl2Bean taaCorpPurchSolLvl2, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/*  59 */     ErrorObject errorObj = null;
/*  60 */     boolean isValid = true;
/*  61 */     if (!CommonValidator.isNullOrEmpty(taaCorpPurchSolLvl2.getReqNm()))
/*     */     {
/*     */ 
/*  64 */       if (CommonValidator.validateData(taaCorpPurchSolLvl2.getReqNm(), 
/*  65 */         com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE['²']))
/*     */       {
/*  67 */         String reqLength = CommonValidator.validateLength(
/*  68 */           taaCorpPurchSolLvl2.getReqNm(), 38, 1);
/*     */         
/*  70 */         if (reqLength.equals("greaterThanMax")) {
/*  71 */           isValid = false;
/*     */         }
/*     */       } else {
/*  74 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/*  78 */     if (!isValid)
/*     */     {
/*     */ 
/*  81 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.DCS_ERROR_REQ_NM);
/*  82 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateChrgDesc(ChargeItemInfoBean chargeItemInfoBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/*  98 */     ErrorObject errorObj = null;
/*  99 */     boolean isValid = true;
/* 100 */     if (!CommonValidator.isNullOrEmpty(chargeItemInfoBean.getChargeDesc()))
/*     */     {
/*     */ 
/* 103 */       if (CommonValidator.validateData(
/* 104 */         chargeItemInfoBean.getChargeDesc(), 
/* 105 */         com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE['²']))
/*     */       {
/* 107 */         String reqLength = CommonValidator.validateLength(
/* 108 */           chargeItemInfoBean.getChargeDesc(), 40, 1);
/*     */         
/* 110 */         if (reqLength.equals("greaterThanMax")) {
/* 111 */           isValid = false;
/*     */         }
/*     */       }
/*     */       else
/*     */       {
/* 116 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 121 */     if (!isValid)
/*     */     {
/* 123 */       errorObj = new ErrorObject(
/* 124 */         XMLSubmissionErrorCodes.DCS_ERROR_CHRG_DESC);
/* 125 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateChargeItemAmt(ChargeItemInfoBean chargeItemInfoBean, List<ErrorObject> errorCodes, String transCurrCd)
/*     */     throws SettlementException
/*     */   {
/* 144 */     ErrorObject errorObj = null;
/* 145 */     boolean isValid = true;
/* 146 */     if (!CommonValidator.isNullOrEmpty(chargeItemInfoBean
/* 147 */       .getChargeItemAmt()))
/*     */     {
/* 149 */       if (CommonValidator.validateData(chargeItemInfoBean
/* 150 */         .getChargeItemAmt(), 
/* 151 */         com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE['µ']))
/*     */       {
/* 153 */         String reqLength = CommonValidator.validateLength(
/* 154 */           chargeItemInfoBean.getChargeItemAmt(), 12, 1);
/*     */         
/* 156 */         if (reqLength.equals("greaterThanMax")) {
/* 157 */           isValid = false;
/*     */         }
/* 159 */         else if ((!CommonValidator.isNullOrEmpty(transCurrCd)) && 
/* 160 */           (!CommonValidator.isValidXmlAmount(transCurrCd, chargeItemInfoBean.getChargeItemAmt()))) {
/* 161 */           isValid = false;
/*     */         }
/*     */       }
/*     */       else
/*     */       {
/* 166 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/* 170 */     if (!isValid)
/*     */     {
/* 172 */       errorObj = new ErrorObject(
/* 173 */         XMLSubmissionErrorCodes.DCS_ERROR_CHRG_ITEM_AMT);
/* 174 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateChargeItemQty(ChargeItemInfoBean chargeItemInfoBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 192 */     ErrorObject errorObj = null;
/* 193 */     boolean isValid = true;
/* 194 */     if (!CommonValidator.isNullOrEmpty(chargeItemInfoBean
/* 195 */       .getChargeItemQty()))
/*     */     {
/* 197 */       if (CommonValidator.validateData(chargeItemInfoBean
/* 198 */         .getChargeItemQty(), 
/* 199 */         com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE['²']))
/*     */       {
/* 201 */         String reqLength = CommonValidator.validateLength(
/* 202 */           chargeItemInfoBean.getChargeItemQty(), 12, 1);
/*     */         
/* 204 */         if (reqLength.equals("greaterThanMax")) {
/* 205 */           isValid = false;
/*     */         }
/*     */       } else {
/* 208 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/* 212 */     if (!isValid)
/*     */     {
/* 214 */       errorObj = new ErrorObject(
/* 215 */         XMLSubmissionErrorCodes.DCS_ERROR_CHRG_ITEM_QTY);
/* 216 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateCMRefNbr(TAACorpPurchSolLvl2Bean taaCorpPurchSolLvl2, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 232 */     System.out.println("Coming here:: Value" + taaCorpPurchSolLvl2.getCmRefNbr());
/* 233 */     ErrorObject errorObj = null;
/* 234 */     boolean isValid = true;
/* 235 */     if (!CommonValidator.isNullOrEmpty(taaCorpPurchSolLvl2.getCmRefNbr()))
/*     */     {
/*     */ 
/* 238 */       if (CommonValidator.validateData(taaCorpPurchSolLvl2.getCmRefNbr(), 
/* 239 */         com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE['´']))
/*     */       {
/*     */ 
/* 242 */         String reqLength = CommonValidator.validateLength(
/* 243 */           taaCorpPurchSolLvl2.getCmRefNbr(), 20, 1);
/*     */         
/* 245 */         if (reqLength.equals("greaterThanMax")) {
/* 246 */           isValid = false;
/*     */         }
/*     */       } else {
/* 249 */         isValid = false;
/*     */       }
/*     */     }
/*     */     
/* 253 */     if (!isValid)
/*     */     {
/* 255 */       errorObj = new ErrorObject(
/* 256 */         XMLSubmissionErrorCodes.DCS_ERROR_CM_REF_NBR);
/* 257 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateShipToPostCd(TAACorpPurchSolLvl2Bean taaCorpPurchSolLvl2, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 275 */     ErrorObject errorObj = null;
/* 276 */     boolean isValid = true;
/*     */     
/* 278 */     if (CommonValidator.isNullOrEmpty(taaCorpPurchSolLvl2.getShipToPostCd())) {
/* 279 */       isValid = false;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */     }
/* 289 */     else if (CommonValidator.validateData(taaCorpPurchSolLvl2
/* 290 */       .getShipToPostCd(), 
/* 291 */       com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE['·']))
/*     */     {
/* 293 */       String reqLength = CommonValidator.validateLength(
/* 294 */         taaCorpPurchSolLvl2.getShipToPostCd(), 17, 1);
/*     */       
/* 296 */       if (reqLength.equals("greaterThanMax")) {
/* 297 */         isValid = false;
/*     */       }
/*     */       
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/* 305 */       isValid = false;
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 314 */     if (!isValid)
/*     */     {
/* 316 */       errorObj = new ErrorObject(
/* 317 */         XMLSubmissionErrorCodes.DCS_ERROR_SHIP_TO_POST_CD);
/* 318 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateTaxTotAmt(TAACorpPurchSolLvl2Bean taaCorpPurchSolLvl2, List<ErrorObject> errorCodes, String transCurrCd)
/*     */     throws SettlementException
/*     */   {
/* 336 */     ErrorObject errorObj = null;
/* 337 */     boolean isValid = true;
/* 338 */     if (CommonValidator.isNullOrEmpty(taaCorpPurchSolLvl2.getTaxTotAmt())) {
/* 339 */       isValid = false;
/*     */     }
/* 341 */     else if (CommonValidator.validateData(
/* 342 */       taaCorpPurchSolLvl2.getTaxTotAmt(), 
/* 343 */       com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE['¸']))
/*     */     {
/* 345 */       String reqLength = CommonValidator.validateLength(
/* 346 */         taaCorpPurchSolLvl2.getTaxTotAmt(), 12, 1);
/*     */       
/* 348 */       if (reqLength.equals("greaterThanMax")) {
/* 349 */         isValid = false;
/*     */       }
/* 351 */       else if ((!CommonValidator.isNullOrEmpty(transCurrCd)) && 
/* 352 */         (!CommonValidator.isValidXmlAmount(transCurrCd, taaCorpPurchSolLvl2.getTaxTotAmt()))) {
/* 353 */         isValid = false;
/*     */       }
/*     */     }
/*     */     else
/*     */     {
/* 358 */       isValid = false;
/*     */     }
/*     */     
/*     */ 
/* 362 */     if (!isValid)
/*     */     {
/* 364 */       errorObj = new ErrorObject(
/* 365 */         XMLSubmissionErrorCodes.DCS_ERROR_TAX_TOT_AMT);
/* 366 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateTaxTypeCd(TAACorpPurchSolLvl2Bean taaCorpPurchSolLvl2, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 383 */     ErrorObject errorObj = null;
/* 384 */     boolean isValid = true;
/* 385 */     if (CommonValidator.isNullOrEmpty(taaCorpPurchSolLvl2.getTaxTypeCd()))
/*     */     {
/* 387 */       isValid = false;
/*     */     }
/*     */     else {
/* 390 */       if (CommonValidator.validateData(taaCorpPurchSolLvl2
/* 391 */         .getTaxTypeCd(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE['¸']))
/* 392 */         if (CommonValidator.isValidTaxTypeCd(taaCorpPurchSolLvl2
/* 393 */           .getTaxTypeCd()))
/*     */         {
/* 395 */           String reqLength = CommonValidator.validateLength(
/* 396 */             taaCorpPurchSolLvl2.getTaxTypeCd(), 3, 1);
/*     */           
/* 398 */           if (!reqLength.equals("greaterThanMax")) break label74;
/* 399 */           isValid = false;
/*     */           break label74;
/*     */         }
/* 402 */       isValid = false;
/*     */     }
/*     */     
/*     */     label74:
/* 406 */     if (!isValid)
/*     */     {
/* 408 */       errorObj = new ErrorObject(
/* 409 */         XMLSubmissionErrorCodes.DCS_ERROR_TAX_TYPE_CD);
/* 410 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateChrgItemInfo(TAACorpPurchSolLvl2Bean taaCorpPurchSolLvl2, List<ErrorObject> errorCodes, String transCurrCd)
/*     */     throws SettlementException
/*     */   {
/* 429 */     if (!taaCorpPurchSolLvl2.getChargeItemInfoBean().isEmpty())
/*     */     {
/* 431 */       Iterator localIterator = taaCorpPurchSolLvl2.getChargeItemInfoBean().iterator();
/* 430 */       while (localIterator.hasNext()) {
/* 431 */         ChargeItemInfoBean chargeInfo = (ChargeItemInfoBean)localIterator.next();
/*     */         
/* 433 */         validateChrgDesc(chargeInfo, errorCodes);
/* 434 */         validateChargeItemAmt(chargeInfo, errorCodes, transCurrCd);
/* 435 */         validateChargeItemQty(chargeInfo, errorCodes);
/*     */       }
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\validator\xml\TAACorpPurchSolLvl2Validator.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */