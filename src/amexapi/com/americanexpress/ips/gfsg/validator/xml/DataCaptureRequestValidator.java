/*      */ package com.americanexpress.ips.gfsg.validator.xml;
/*      */ 
/*      */ import com.americanexpress.ips.gfsg.beans.ErrorObject;
/*      */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.CardTransactionDetailBean;
/*      */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.DataCaptureRequestBean;
/*      */ import com.americanexpress.ips.gfsg.beans.xml.datacapture.PointOfServiceDataBean;
/*      */ import com.americanexpress.ips.gfsg.enumerations.TransProcCd;
/*      */ import com.americanexpress.ips.gfsg.enumerations.XMLSubmissionErrorCodes;
/*      */ import com.americanexpress.ips.gfsg.exceptions.SettlementException;
/*      */ import com.americanexpress.ips.gfsg.validator.CommonValidator;
/*      */ import java.util.Iterator;
/*      */ import java.util.List;
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ public class DataCaptureRequestValidator
/*      */ {
/*      */   public static void validateDataCaptureRequestRec(DataCaptureRequestBean dataCaptureRequestBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*   38 */     validateVersion(dataCaptureRequestBean, errorCodes);
/*   39 */     validateMerId(dataCaptureRequestBean, errorCodes);
/*   40 */     validateMerTrmnlId(dataCaptureRequestBean, errorCodes);
/*   41 */     validateBatchID(dataCaptureRequestBean, errorCodes);
/*   42 */     validateRefNumber(dataCaptureRequestBean, errorCodes);
/*   43 */     validateCardNbr(dataCaptureRequestBean, errorCodes);
/*   44 */     validateCardExprDt(dataCaptureRequestBean, errorCodes);
/*   45 */     validateTransDt(dataCaptureRequestBean, errorCodes);
/*   46 */     validateTransTm(dataCaptureRequestBean, errorCodes);
/*   47 */     validateTransAmt(dataCaptureRequestBean, errorCodes);
/*   48 */     validateTransCurrCd(dataCaptureRequestBean, errorCodes);
/*   49 */     validateTransProcCd(dataCaptureRequestBean, errorCodes);
/*   50 */     validateTransId(dataCaptureRequestBean, errorCodes);
/*   51 */     validateTransAprvCd(dataCaptureRequestBean, errorCodes);
/*   52 */     validateMediaCd(dataCaptureRequestBean, errorCodes);
/*   53 */     validatePOSCodeData(dataCaptureRequestBean, errorCodes);
/*   54 */     validateElecComrceInd(dataCaptureRequestBean, errorCodes);
/*   55 */     validateSubmMthdCd(dataCaptureRequestBean, errorCodes);
/*   56 */     validateMerLocId(dataCaptureRequestBean, errorCodes);
/*   57 */     validateMerCtcInfoTxt(dataCaptureRequestBean, errorCodes);
/*   58 */     validateMatchKeyTypeCd(dataCaptureRequestBean, errorCodes);
/*   59 */     validateMatchKeyId(dataCaptureRequestBean, errorCodes);
/*   60 */     validateDefPaymentPlan(dataCaptureRequestBean, errorCodes);
/*   61 */     validateNumDefPayments(dataCaptureRequestBean, errorCodes);
/*   62 */     validateEMVData(dataCaptureRequestBean, errorCodes);
/*   63 */     validateMerCtgyCd(dataCaptureRequestBean, errorCodes);
/*   64 */     validateSellId(dataCaptureRequestBean, errorCodes);
/*   65 */     validateCardTransDetail(dataCaptureRequestBean, errorCodes, dataCaptureRequestBean.getTransCurrCd());
/*   66 */     validateTransAddCd(dataCaptureRequestBean, errorCodes);
/*   67 */     validateTransImageSeqNbr(dataCaptureRequestBean, errorCodes);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateVersion(DataCaptureRequestBean dataCaptureRequestBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*   83 */     boolean isValid = true;
/*   84 */     ErrorObject errorObj = null;
/*      */     
/*   86 */     if (CommonValidator.isNullOrEmpty(dataCaptureRequestBean.getVersion())) {
/*   87 */       isValid = false;
/*      */ 
/*      */     }
/*   90 */     else if (CommonValidator.validateData(dataCaptureRequestBean
/*   91 */       .getVersion(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[0]))
/*      */     {
/*   93 */       String reqLength = CommonValidator.validateLength(
/*   94 */         dataCaptureRequestBean.getVersion(), 8, 1);
/*      */       
/*   96 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*   98 */         isValid = false;
/*      */ 
/*      */ 
/*      */       }
/*  102 */       else if (!dataCaptureRequestBean.getVersion().equals("12010000")) {
/*  103 */         isValid = false;
/*      */       }
/*      */     }
/*      */     else {
/*  107 */       isValid = false;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*  112 */     if (!isValid)
/*      */     {
/*      */ 
/*  115 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_VERSION);
/*  116 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateMerId(DataCaptureRequestBean dataCaptureRequestBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  136 */     ErrorObject errorObj = null;
/*  137 */     boolean isValid = true;
/*  138 */     if (CommonValidator.isNullOrEmpty(dataCaptureRequestBean.getMerId())) {
/*  139 */       isValid = false;
/*      */     }
/*      */     else {
/*  142 */       if (CommonValidator.validateData(dataCaptureRequestBean.getMerId(), 
/*  143 */         com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[1]))
/*      */       {
/*  145 */         if (CommonValidator.isValidModulusNineCheck(dataCaptureRequestBean
/*  146 */           .getMerId()))
/*      */         {
/*  148 */           String reqLength = CommonValidator.validateLength(
/*  149 */             dataCaptureRequestBean.getMerId(), 10, 1);
/*      */           
/*  151 */           if (!reqLength.equals("greaterThanMax")) break label73;
/*  152 */           isValid = false;
/*      */           break label73;
/*      */         }
/*      */       }
/*  156 */       isValid = false;
/*      */     }
/*      */     
/*      */     label73:
/*      */     
/*  161 */     if (!isValid)
/*      */     {
/*  163 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_MERID);
/*  164 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateMerTrmnlId(DataCaptureRequestBean dataCaptureRequestBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  182 */     ErrorObject errorObj = null;
/*  183 */     boolean isValid = true;
/*  184 */     if (CommonValidator.isNullOrEmpty(dataCaptureRequestBean
/*  185 */       .getMerTrmnlId())) {
/*  186 */       isValid = false;
/*      */ 
/*      */     }
/*  189 */     else if (CommonValidator.validateData(dataCaptureRequestBean
/*  190 */       .getMerTrmnlId(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[2]))
/*      */     {
/*  192 */       String reqLength = CommonValidator.validateLength(
/*  193 */         dataCaptureRequestBean.getMerTrmnlId(), 8, 1);
/*      */       
/*  195 */       if (reqLength.equals("greaterThanMax")) {
/*  196 */         isValid = false;
/*      */       }
/*      */     }
/*      */     else {
/*  200 */       isValid = false;
/*      */     }
/*      */     
/*      */ 
/*  204 */     if (!isValid)
/*      */     {
/*  206 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_MERTRMNLID);
/*  207 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateBatchID(DataCaptureRequestBean dataCaptureRequestBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  226 */     ErrorObject errorObj = null;
/*  227 */     boolean isValid = true;
/*  228 */     if (CommonValidator.isNullOrEmpty(dataCaptureRequestBean.getBatchId()))
/*      */     {
/*  230 */       isValid = false;
/*      */ 
/*      */     }
/*  233 */     else if (CommonValidator.validateData(dataCaptureRequestBean
/*  234 */       .getBatchId(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[3]))
/*      */     {
/*  236 */       String reqLength = CommonValidator.validateLength(
/*  237 */         dataCaptureRequestBean.getBatchId(), 6, 1);
/*      */       
/*  239 */       if (reqLength.equals("greaterThanMax")) {
/*  240 */         isValid = false;
/*      */       }
/*      */     }
/*      */     else {
/*  244 */       isValid = false;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*  249 */     if (!isValid)
/*      */     {
/*  251 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_BATCHID);
/*  252 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateRefNumber(DataCaptureRequestBean dataCaptureRequestBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  272 */     ErrorObject errorObj = null;
/*  273 */     boolean isValid = true;
/*      */     
/*  275 */     if (CommonValidator.isNullOrEmpty(dataCaptureRequestBean.getRefNumber())) {
/*  276 */       isValid = false;
/*      */ 
/*      */     }
/*  279 */     else if (CommonValidator.validateData(dataCaptureRequestBean
/*  280 */       .getRefNumber(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[28]))
/*      */     {
/*  282 */       String reqLength = CommonValidator.validateLength(
/*  283 */         dataCaptureRequestBean.getRefNumber(), 13, 1);
/*      */       
/*  285 */       if (reqLength.equals("greaterThanMax")) {
/*  286 */         isValid = false;
/*      */       }
/*      */     }
/*      */     else
/*      */     {
/*  291 */       isValid = false;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*  296 */     if (!isValid) {
/*  297 */       errorObj = new ErrorObject(
/*  298 */         XMLSubmissionErrorCodes.ERROR_REFERENCENUMBER);
/*  299 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateCardNbr(DataCaptureRequestBean dataCaptureRequestBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  317 */     boolean isValid = true;
/*  318 */     ErrorObject errorObj = null;
/*  319 */     if (CommonValidator.isNullOrEmpty(dataCaptureRequestBean.getCardNbr())) {
/*  320 */       isValid = false;
/*      */     }
/*      */     else {
/*  323 */       if (CommonValidator.validateData(dataCaptureRequestBean
/*  324 */         .getCardNbr(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[29])) {
/*  325 */         if (CommonValidator.isValidModulusTenCheck(dataCaptureRequestBean
/*  326 */           .getCardNbr()))
/*      */         {
/*  328 */           String reqLength = CommonValidator.validateLength(
/*  329 */             dataCaptureRequestBean.getCardNbr(), 19, 15);
/*      */           
/*  331 */           if (reqLength.equals("greaterThanMax")) {
/*  332 */             isValid = false;
/*      */             break label90; }
/*  334 */           if (!reqLength.equals("lessThanMin")) break label90;
/*  335 */           isValid = false;
/*      */           break label90;
/*      */         }
/*      */       }
/*  339 */       isValid = false;
/*      */     }
/*      */     
/*      */     label90:
/*      */     
/*  344 */     if (!isValid)
/*      */     {
/*  346 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_CARDNBR);
/*  347 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateCardExprDt(DataCaptureRequestBean dataCaptureRequestBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  365 */     boolean isValid = true;
/*  366 */     ErrorObject errorObj = null;
/*  367 */     if (!CommonValidator.isNullOrEmpty(dataCaptureRequestBean
/*  368 */       .getCardExprDt())) {
/*  369 */       if (CommonValidator.validateData(dataCaptureRequestBean
/*  370 */         .getCardExprDt(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[30]))
/*      */       {
/*  372 */         if (!CommonValidator.isValidXmlDate(dataCaptureRequestBean
/*  373 */           .getCardExprDt(), "YYMM"))
/*      */         {
/*  375 */           isValid = false;
/*      */         }
/*      */       }
/*      */       else {
/*  379 */         isValid = false;
/*      */       }
/*      */     }
/*      */     
/*  383 */     if (!isValid)
/*      */     {
/*  385 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_CARDEXPRDT);
/*  386 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTransDt(DataCaptureRequestBean dataCaptureRequestBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  404 */     boolean isValid = true;
/*  405 */     ErrorObject errorObj = null;
/*  406 */     if (CommonValidator.isNullOrEmpty(dataCaptureRequestBean.getTransDt())) {
/*  407 */       isValid = false;
/*      */ 
/*      */     }
/*  410 */     else if (CommonValidator.validateData(dataCaptureRequestBean
/*  411 */       .getTransDt(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[31]))
/*      */     {
/*  413 */       if (!CommonValidator.isValidXmlDate(dataCaptureRequestBean
/*  414 */         .getTransDt(), "CCYYMMDD"))
/*      */       {
/*  416 */         isValid = false;
/*      */       }
/*      */     }
/*      */     else {
/*  420 */       isValid = false;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*  425 */     if (!isValid)
/*      */     {
/*  427 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_TRANSDT);
/*  428 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTransTm(DataCaptureRequestBean dataCaptureRequestBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  445 */     boolean isValid = true;
/*  446 */     ErrorObject errorObj = null;
/*  447 */     if (!CommonValidator.isNullOrEmpty(dataCaptureRequestBean.getTransTm())) {
/*  448 */       if (CommonValidator.validateData(dataCaptureRequestBean
/*  449 */         .getTransTm(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[32])) {
/*  450 */         if (!CommonValidator.validateTime(dataCaptureRequestBean
/*  451 */           .getTransTm()))
/*      */         {
/*  453 */           isValid = false;
/*      */         }
/*      */       }
/*      */       else {
/*  457 */         isValid = false;
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*  462 */     if (!isValid)
/*      */     {
/*  464 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_TRANSTM);
/*  465 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTransAmt(DataCaptureRequestBean dataCaptureRequestBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  483 */     boolean isValid = true;
/*  484 */     ErrorObject errorObj = null;
/*  485 */     if (CommonValidator.isNullOrEmpty(dataCaptureRequestBean.getTransAmt())) {
/*  486 */       isValid = false;
/*      */ 
/*      */     }
/*  489 */     else if (CommonValidator.validateData(dataCaptureRequestBean
/*  490 */       .getTransAmt(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[33]))
/*      */     {
/*  492 */       String reqLength = CommonValidator.validateLength(
/*  493 */         dataCaptureRequestBean.getTransAmt(), 12, 1);
/*      */       
/*  495 */       if (reqLength.equals("greaterThanMax")) {
/*  496 */         isValid = false;
/*      */       }
/*  498 */       else if ((!CommonValidator.isNullOrEmpty(dataCaptureRequestBean.getTransCurrCd())) && 
/*  499 */         (!CommonValidator.isValidXmlAmount(dataCaptureRequestBean.getTransCurrCd(), dataCaptureRequestBean.getTransAmt()))) {
/*  500 */         isValid = false;
/*      */       }
/*      */     }
/*      */     else
/*      */     {
/*  505 */       isValid = false;
/*      */     }
/*      */     
/*  508 */     if (!isValid)
/*      */     {
/*  510 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_TRANSAMT);
/*  511 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTransCurrCd(DataCaptureRequestBean dataCaptureRequestBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  529 */     boolean isValid = true;
/*  530 */     ErrorObject errorObj = null;
/*  531 */     if (CommonValidator.isNullOrEmpty(dataCaptureRequestBean
/*  532 */       .getTransCurrCd())) {
/*  533 */       isValid = false;
/*      */     }
/*      */     else {
/*  536 */       if (CommonValidator.validateData(dataCaptureRequestBean
/*  537 */         .getTransCurrCd(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[34]))
/*      */       {
/*  539 */         if (CommonValidator.isValidXmlCurrencyCode(dataCaptureRequestBean
/*  540 */           .getTransCurrCd()))
/*      */         {
/*  542 */           String reqLength = CommonValidator.validateLength(
/*  543 */             dataCaptureRequestBean.getTransCurrCd(), 3, 3);
/*      */           
/*  545 */           if (!reqLength.equals("greaterThanMax")) break label73;
/*  546 */           isValid = false;
/*      */           break label73;
/*      */         }
/*      */       }
/*  550 */       isValid = false;
/*      */     }
/*      */     
/*      */     label73:
/*      */     
/*  555 */     if (!isValid)
/*      */     {
/*  557 */       errorObj = new ErrorObject(
/*  558 */         XMLSubmissionErrorCodes.ERROR_TRANSCURRCD);
/*  559 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTransProcCd(DataCaptureRequestBean dataCaptureRequestBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  577 */     boolean isValid = true;
/*  578 */     ErrorObject errorObj = null;
/*  579 */     if (CommonValidator.isNullOrEmpty(dataCaptureRequestBean
/*  580 */       .getTransProcCd())) {
/*  581 */       isValid = false;
/*      */ 
/*      */     }
/*  584 */     else if ((!CommonValidator.validateData(dataCaptureRequestBean
/*  585 */       .getTransProcCd(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[35])) || 
/*      */       
/*  587 */       (!CommonValidator.isValidXmlTransProcCd(dataCaptureRequestBean
/*  588 */       .getTransProcCd())))
/*      */     {
/*      */ 
/*      */ 
/*  592 */       isValid = false;
/*      */     }
/*      */     
/*      */ 
/*  596 */     if (!isValid)
/*      */     {
/*  598 */       errorObj = new ErrorObject(
/*  599 */         XMLSubmissionErrorCodes.ERROR_TRANSPROCCD);
/*  600 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTransId(DataCaptureRequestBean dataCaptureRequestBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  619 */     ErrorObject errorObj = null;
/*  620 */     boolean isValid = true;
/*  621 */     if (CommonValidator.isNullOrEmpty(dataCaptureRequestBean.getTransId())) {
/*  622 */       if ((!CommonValidator.isNullOrEmpty(dataCaptureRequestBean.getTransProcCd())) && 
/*  623 */         (dataCaptureRequestBean.getTransProcCd().equals(TransProcCd.DEBIT.getTransProcCd()))) {
/*  624 */         isValid = false;
/*      */       }
/*      */     }
/*      */     else {
/*  628 */       if (CommonValidator.validateData(dataCaptureRequestBean
/*  629 */         .getTransId(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[36]))
/*  630 */         if (CommonValidator.isValidModulusTenCheck(dataCaptureRequestBean
/*  631 */           .getTransId()))
/*      */         {
/*  633 */           String reqLength = CommonValidator.validateLength(
/*  634 */             dataCaptureRequestBean.getTransId(), 15, 15);
/*      */           
/*  636 */           if ((!reqLength.equals("greaterThanMax")) && (!reqLength.equals("lessThanMin")))
/*      */             break label111;
/*  638 */           isValid = false;
/*      */           break label111;
/*      */         }
/*  641 */       isValid = false;
/*      */     }
/*      */     
/*      */     label111:
/*  645 */     if (!isValid)
/*      */     {
/*  647 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_TRANSID);
/*  648 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTransAprvCd(DataCaptureRequestBean dataCaptureRequestBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  666 */     ErrorObject errorObj = null;
/*  667 */     boolean isValid = true;
/*      */     
/*  669 */     if (CommonValidator.isNullOrEmpty(dataCaptureRequestBean
/*  670 */       .getTransAprvCd()))
/*      */     {
/*  672 */       if ((!CommonValidator.isNullOrEmpty(dataCaptureRequestBean.getTransProcCd())) && 
/*  673 */         (dataCaptureRequestBean.getTransProcCd().equals(TransProcCd.DEBIT.getTransProcCd()))) {
/*  674 */         isValid = false;
/*      */       }
/*      */     }
/*      */     else
/*      */     {
/*  679 */       int approvalCodeLen = dataCaptureRequestBean.getTransAprvCd()
/*  680 */         .length();
/*  681 */       if ((!CommonValidator.validateData(dataCaptureRequestBean
/*  682 */         .getTransAprvCd(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[37])) || (
/*  683 */         (approvalCodeLen != 6) && (approvalCodeLen != 2)))
/*      */       {
/*      */ 
/*  686 */         isValid = false;
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*  691 */     if (!isValid)
/*      */     {
/*  693 */       errorObj = new ErrorObject(
/*  694 */         XMLSubmissionErrorCodes.ERROR_TRANSAPRVCD);
/*  695 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateCardDataInpCpblCd(PointOfServiceDataBean pointOfServiceDataBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  712 */     boolean isValid = true;
/*  713 */     ErrorObject errorObj = null;
/*  714 */     if (CommonValidator.isNullOrEmpty(pointOfServiceDataBean
/*  715 */       .getCardDataInpCpblCd())) {
/*  716 */       isValid = false;
/*      */ 
/*      */     }
/*  719 */     else if (CommonValidator.validateData(pointOfServiceDataBean
/*  720 */       .getCardDataInpCpblCd(), 
/*  721 */       com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[38]))
/*      */     {
/*  723 */       String reqLength = CommonValidator.validateLength(
/*  724 */         pointOfServiceDataBean.getCardDataInpCpblCd(), 1, 1);
/*      */       
/*  726 */       if (reqLength.equals("greaterThanMax")) {
/*  727 */         isValid = false;
/*      */       }
/*      */     }
/*      */     else {
/*  731 */       isValid = false;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*  736 */     if (!isValid)
/*      */     {
/*  738 */       errorObj = new ErrorObject(
/*  739 */         XMLSubmissionErrorCodes.ERROR_POINTOFSERVICEDATA_CARDDATAINPCPBLCD);
/*  740 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateCMAuthnCpblCd(PointOfServiceDataBean pointOfServiceDataBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  757 */     boolean isValid = true;
/*  758 */     ErrorObject errorObj = null;
/*  759 */     if (CommonValidator.isNullOrEmpty(pointOfServiceDataBean
/*  760 */       .getCmAuthnCpblCd())) {
/*  761 */       isValid = false;
/*      */ 
/*      */     }
/*  764 */     else if (CommonValidator.validateData(pointOfServiceDataBean
/*  765 */       .getCmAuthnCpblCd(), 
/*  766 */       com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[39]))
/*      */     {
/*  768 */       String reqLength = CommonValidator.validateLength(
/*  769 */         pointOfServiceDataBean.getCmAuthnCpblCd(), 1, 1);
/*      */       
/*  771 */       if (reqLength.equals("greaterThanMax")) {
/*  772 */         isValid = false;
/*      */       }
/*      */     }
/*      */     else
/*      */     {
/*  777 */       isValid = false;
/*      */     }
/*      */     
/*      */ 
/*  781 */     if (!isValid)
/*      */     {
/*  783 */       errorObj = new ErrorObject(
/*  784 */         XMLSubmissionErrorCodes.ERROR_POINTOFSERVICEDATA_CMAUTHNCPBLCD);
/*  785 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateCardCptrCpblCd(PointOfServiceDataBean pointOfServiceDataBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  802 */     boolean isValid = true;
/*  803 */     ErrorObject errorObj = null;
/*  804 */     if (CommonValidator.isNullOrEmpty(pointOfServiceDataBean
/*  805 */       .getCardCptrCpblCd())) {
/*  806 */       isValid = false;
/*      */ 
/*      */     }
/*  809 */     else if (CommonValidator.validateData(pointOfServiceDataBean
/*  810 */       .getCardCptrCpblCd(), 
/*  811 */       com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[40]))
/*      */     {
/*  813 */       String reqLength = CommonValidator.validateLength(
/*  814 */         pointOfServiceDataBean.getCardCptrCpblCd(), 1, 1);
/*      */       
/*  816 */       if (reqLength.equals("greaterThanMax")) {
/*  817 */         isValid = false;
/*      */       }
/*      */     }
/*      */     else
/*      */     {
/*  822 */       isValid = false;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*  827 */     if (!isValid)
/*      */     {
/*  829 */       errorObj = new ErrorObject(
/*  830 */         XMLSubmissionErrorCodes.ERROR_POINTOFSERVICEDATA_CARDCPTRCPBLCD);
/*  831 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateOprEnvirCd(PointOfServiceDataBean pointOfServiceDataBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  848 */     boolean isValid = true;
/*  849 */     ErrorObject errorObj = null;
/*  850 */     if (CommonValidator.isNullOrEmpty(pointOfServiceDataBean
/*  851 */       .getOprEnvirCd())) {
/*  852 */       isValid = false;
/*      */ 
/*      */     }
/*  855 */     else if (CommonValidator.validateData(pointOfServiceDataBean
/*  856 */       .getOprEnvirCd(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[41]))
/*      */     {
/*  858 */       String reqLength = CommonValidator.validateLength(
/*  859 */         pointOfServiceDataBean.getOprEnvirCd(), 1, 1);
/*      */       
/*  861 */       if (reqLength.equals("greaterThanMax")) {
/*  862 */         isValid = false;
/*      */       }
/*      */     }
/*      */     else {
/*  866 */       isValid = false;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*  871 */     if (!isValid)
/*      */     {
/*  873 */       errorObj = new ErrorObject(
/*  874 */         XMLSubmissionErrorCodes.ERROR_POINTOFSERVICEDATA_OPRENVIRCD);
/*  875 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateCMPresentCd(PointOfServiceDataBean pointOfServiceDataBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  893 */     boolean isValid = true;
/*  894 */     ErrorObject errorObj = null;
/*  895 */     if (CommonValidator.isNullOrEmpty(pointOfServiceDataBean
/*  896 */       .getCmPresentCd()))
/*      */     {
/*  898 */       isValid = false;
/*      */ 
/*      */     }
/*  901 */     else if (CommonValidator.validateData(pointOfServiceDataBean
/*  902 */       .getCmPresentCd(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[42]))
/*      */     {
/*  904 */       String reqLength = CommonValidator.validateLength(
/*  905 */         pointOfServiceDataBean.getCmPresentCd(), 1, 1);
/*      */       
/*  907 */       if (reqLength.equals("greaterThanMax")) {
/*  908 */         isValid = false;
/*      */       }
/*      */     }
/*      */     else {
/*  912 */       isValid = false;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*  917 */     if (!isValid)
/*      */     {
/*  919 */       errorObj = new ErrorObject(
/*  920 */         XMLSubmissionErrorCodes.ERROR_POINTOFSERVICEDATA_CMPRESENTCD);
/*  921 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateCardPresentCd(PointOfServiceDataBean pointOfServiceDataBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  939 */     ErrorObject errorObj = null;
/*  940 */     boolean isValid = true;
/*  941 */     if (CommonValidator.isNullOrEmpty(pointOfServiceDataBean
/*  942 */       .getCardPresentCd())) {
/*  943 */       isValid = false;
/*      */ 
/*      */     }
/*  946 */     else if (CommonValidator.validateData(pointOfServiceDataBean
/*  947 */       .getCardPresentCd(), 
/*  948 */       com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[43]))
/*      */     {
/*  950 */       String reqLength = CommonValidator.validateLength(
/*  951 */         pointOfServiceDataBean.getCardPresentCd(), 1, 1);
/*      */       
/*  953 */       if (reqLength.equals("greaterThanMax")) {
/*  954 */         isValid = false;
/*      */       }
/*      */     }
/*      */     else
/*      */     {
/*  959 */       isValid = false;
/*      */     }
/*      */     
/*      */ 
/*  963 */     if (!isValid)
/*      */     {
/*  965 */       errorObj = new ErrorObject(
/*  966 */         XMLSubmissionErrorCodes.ERROR_POINTOFSERVICEDATA_CARDPRESENTCD);
/*  967 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateCardDataInpModeCd(PointOfServiceDataBean pointOfServiceDataBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  984 */     boolean isValid = true;
/*  985 */     ErrorObject errorObj = null;
/*  986 */     if (CommonValidator.isNullOrEmpty(pointOfServiceDataBean
/*  987 */       .getCardDataInpModeCd())) {
/*  988 */       isValid = false;
/*      */ 
/*      */     }
/*  991 */     else if (CommonValidator.validateData(pointOfServiceDataBean
/*  992 */       .getCardDataInpModeCd(), 
/*  993 */       com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[44]))
/*      */     {
/*  995 */       String reqLength = CommonValidator.validateLength(
/*  996 */         pointOfServiceDataBean.getCardDataInpModeCd(), 1, 1);
/*      */       
/*  998 */       if (reqLength.equals("greaterThanMax")) {
/*  999 */         isValid = false;
/*      */       }
/*      */     }
/*      */     else {
/* 1003 */       isValid = false;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/* 1008 */     if (!isValid)
/*      */     {
/* 1010 */       errorObj = new ErrorObject(
/* 1011 */         XMLSubmissionErrorCodes.ERROR_POINTOFSERVICEDATA_CARDDATAINPMODECD);
/* 1012 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateCMAuthnMthdCd(PointOfServiceDataBean pointOfServiceDataBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1029 */     boolean isValid = true;
/* 1030 */     ErrorObject errorObj = null;
/* 1031 */     if (CommonValidator.isNullOrEmpty(pointOfServiceDataBean
/* 1032 */       .getCmAuthnMthdCd())) {
/* 1033 */       isValid = false;
/*      */ 
/*      */     }
/* 1036 */     else if (CommonValidator.validateData(pointOfServiceDataBean
/* 1037 */       .getCmAuthnMthdCd(), 
/* 1038 */       com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[45]))
/*      */     {
/* 1040 */       String reqLength = CommonValidator.validateLength(
/* 1041 */         pointOfServiceDataBean.getCmAuthnMthdCd(), 1, 1);
/*      */       
/* 1043 */       if (reqLength.equals("greaterThanMax")) {
/* 1044 */         isValid = false;
/*      */       }
/*      */     }
/*      */     else {
/* 1048 */       isValid = false;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/* 1053 */     if (!isValid)
/*      */     {
/* 1055 */       errorObj = new ErrorObject(
/* 1056 */         XMLSubmissionErrorCodes.ERROR_POINTOFSERVICEDATA_CMAUTHNMTHDCD);
/* 1057 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateCMAuthnEnttyCd(PointOfServiceDataBean pointOfServiceDataBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1074 */     boolean isValid = true;
/* 1075 */     ErrorObject errorObj = null;
/* 1076 */     if (CommonValidator.isNullOrEmpty(pointOfServiceDataBean
/* 1077 */       .getCmAuthnEnttyCd())) {
/* 1078 */       isValid = false;
/*      */ 
/*      */     }
/* 1081 */     else if (CommonValidator.validateData(pointOfServiceDataBean
/* 1082 */       .getCmAuthnEnttyCd(), 
/* 1083 */       com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[46]))
/*      */     {
/* 1085 */       String reqLength = CommonValidator.validateLength(
/* 1086 */         pointOfServiceDataBean.getCmAuthnEnttyCd(), 1, 1);
/*      */       
/* 1088 */       if (reqLength.equals("greaterThanMax")) {
/* 1089 */         isValid = false;
/*      */       }
/*      */     }
/*      */     else {
/* 1093 */       isValid = false;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/* 1098 */     if (!isValid)
/*      */     {
/* 1100 */       errorObj = new ErrorObject(
/* 1101 */         XMLSubmissionErrorCodes.ERROR_POINTOFSERVICEDATA_CMAUTHNENTTYCD);
/* 1102 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateCardDataOpCpblCd(PointOfServiceDataBean pointOfServiceDataBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1119 */     boolean isValid = true;
/* 1120 */     ErrorObject errorObj = null;
/* 1121 */     if (CommonValidator.isNullOrEmpty(pointOfServiceDataBean
/* 1122 */       .getCardDataOpCpblCd())) {
/* 1123 */       isValid = false;
/*      */ 
/*      */     }
/* 1126 */     else if (CommonValidator.validateData(pointOfServiceDataBean
/* 1127 */       .getCardDataOpCpblCd(), 
/* 1128 */       com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[47]))
/*      */     {
/* 1130 */       String reqLength = CommonValidator.validateLength(
/* 1131 */         pointOfServiceDataBean.getCardDataOpCpblCd(), 1, 1);
/*      */       
/* 1133 */       if (reqLength.equals("greaterThanMax")) {
/* 1134 */         isValid = false;
/*      */       }
/*      */     }
/*      */     else {
/* 1138 */       isValid = false;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/* 1143 */     if (!isValid)
/*      */     {
/* 1145 */       errorObj = new ErrorObject(
/* 1146 */         XMLSubmissionErrorCodes.ERROR_POINTOFSERVICEDATA_CARDDATAOPCPBLCD);
/* 1147 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTrmnlOpCpblCd(PointOfServiceDataBean pointOfServiceDataBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1164 */     boolean isValid = true;
/* 1165 */     ErrorObject errorObj = null;
/* 1166 */     if (CommonValidator.isNullOrEmpty(pointOfServiceDataBean
/* 1167 */       .getTrmnlOpCpblCd())) {
/* 1168 */       isValid = false;
/*      */ 
/*      */     }
/* 1171 */     else if (CommonValidator.validateData(pointOfServiceDataBean
/* 1172 */       .getTrmnlOpCpblCd(), 
/* 1173 */       com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[48]))
/*      */     {
/* 1175 */       String reqLength = CommonValidator.validateLength(
/* 1176 */         pointOfServiceDataBean.getTrmnlOpCpblCd(), 1, 1);
/*      */       
/* 1178 */       if (reqLength.equals("greaterThanMax")) {
/* 1179 */         isValid = false;
/*      */       }
/*      */     }
/*      */     else {
/* 1183 */       isValid = false;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/* 1188 */     if (!isValid)
/*      */     {
/* 1190 */       errorObj = new ErrorObject(
/* 1191 */         XMLSubmissionErrorCodes.ERROR_POINTOFSERVICEDATA_TRMNLOPCPBLCD);
/* 1192 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validatePINCptrCpblCd(PointOfServiceDataBean pointOfServiceDataBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1210 */     ErrorObject errorObj = null;
/* 1211 */     boolean isValid = true;
/* 1212 */     if (CommonValidator.isNullOrEmpty(pointOfServiceDataBean
/* 1213 */       .getPinCptrCpblCd())) {
/* 1214 */       isValid = false;
/*      */ 
/*      */     }
/* 1217 */     else if (CommonValidator.validateData(pointOfServiceDataBean
/* 1218 */       .getPinCptrCpblCd(), 
/* 1219 */       com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[49]))
/*      */     {
/* 1221 */       String reqLength = CommonValidator.validateLength(
/* 1222 */         pointOfServiceDataBean.getPinCptrCpblCd(), 1, 1);
/*      */       
/* 1224 */       if (reqLength.equals("greaterThanMax")) {
/* 1225 */         isValid = false;
/*      */       }
/*      */     }
/*      */     else {
/* 1229 */       isValid = false;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/* 1234 */     if (!isValid)
/*      */     {
/* 1236 */       errorObj = new ErrorObject(
/* 1237 */         XMLSubmissionErrorCodes.ERROR_POINTOFSERVICEDATA_PINCPTRCPBLCD);
/* 1238 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateElecComrceInd(DataCaptureRequestBean dataCaptureRequestBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1255 */     boolean isValid = true;
/* 1256 */     ErrorObject errorObj = null;
/* 1257 */     if (!CommonValidator.isNullOrEmpty(dataCaptureRequestBean
/* 1258 */       .getElecComrceInd())) {
/* 1259 */       if (CommonValidator.validateData(dataCaptureRequestBean
/* 1260 */         .getElecComrceInd(), 
/* 1261 */         com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[50]))
/*      */       {
/* 1263 */         if (CommonValidator.isValidElecComrceInd(dataCaptureRequestBean
/* 1264 */           .getElecComrceInd()))
/*      */         {
/* 1266 */           String reqLength = CommonValidator.validateLength(
/* 1267 */             dataCaptureRequestBean.getElecComrceInd(), 2, 2);
/*      */           
/* 1269 */           if (!reqLength.equals("greaterThanMax")) break label68;
/* 1270 */           isValid = false;
/*      */           break label68;
/*      */         }
/*      */       }
/* 1274 */       isValid = false;
/*      */     }
/*      */     
/*      */     label68:
/*      */     
/* 1279 */     if (!isValid)
/*      */     {
/* 1281 */       errorObj = new ErrorObject(
/* 1282 */         XMLSubmissionErrorCodes.ERROR_ELECCOMRCEIND);
/* 1283 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateMediaCd(DataCaptureRequestBean dataCaptureRequestBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1300 */     boolean isValid = true;
/* 1301 */     ErrorObject errorObj = null;
/* 1302 */     if (!CommonValidator.isNullOrEmpty(dataCaptureRequestBean.getMediaCd())) {
/* 1303 */       if (CommonValidator.validateData(dataCaptureRequestBean
/* 1304 */         .getMediaCd(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[51])) {
/* 1305 */         if (CommonValidator.isValidMediaCode(dataCaptureRequestBean
/* 1306 */           .getMediaCd()))
/*      */         {
/* 1308 */           String reqLength = CommonValidator.validateLength(
/* 1309 */             dataCaptureRequestBean.getMediaCd(), 2, 2);
/*      */           
/* 1311 */           if (!reqLength.equals("greaterThanMax")) break label68;
/* 1312 */           isValid = false;
/*      */           break label68;
/*      */         }
/*      */       }
/* 1316 */       isValid = false;
/*      */     }
/*      */     
/*      */     label68:
/*      */     
/* 1321 */     if (!isValid)
/*      */     {
/* 1323 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_MEDIACD);
/* 1324 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateSubmMthdCd(DataCaptureRequestBean dataCaptureRequestBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1341 */     boolean isValid = true;
/* 1342 */     ErrorObject errorObj = null;
/* 1343 */     if (!CommonValidator.isNullOrEmpty(dataCaptureRequestBean
/* 1344 */       .getSubmMthdCd())) {
/* 1345 */       if (CommonValidator.validateData(dataCaptureRequestBean
/* 1346 */         .getSubmMthdCd(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[52])) {
/* 1347 */         if (CommonValidator.isValidSubCode(dataCaptureRequestBean
/* 1348 */           .getSubmMthdCd()))
/*      */         {
/* 1350 */           String reqLength = CommonValidator.validateLength(
/* 1351 */             dataCaptureRequestBean.getSubmMthdCd(), 2, 2);
/*      */           
/* 1353 */           if (!reqLength.equals("greaterThanMax")) break label68;
/* 1354 */           isValid = false;
/*      */           break label68;
/*      */         }
/*      */       }
/* 1358 */       isValid = false;
/*      */     }
/*      */     
/*      */     label68:
/*      */     
/* 1363 */     if (!isValid) {
/* 1364 */       isValid = false;
/* 1365 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_SUBMMTHDCD);
/* 1366 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateMerLocId(DataCaptureRequestBean dataCaptureRequestBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1383 */     boolean isValid = true;
/* 1384 */     ErrorObject errorObj = null;
/*      */     
/* 1386 */     if (!CommonValidator.isNullOrEmpty(dataCaptureRequestBean.getMerLocId())) {
/* 1387 */       if (CommonValidator.validateData(dataCaptureRequestBean
/* 1388 */         .getMerLocId(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[53]))
/*      */       {
/* 1390 */         String reqLength = CommonValidator.validateLength(
/* 1391 */           dataCaptureRequestBean.getMerLocId(), 15, 1);
/*      */         
/* 1393 */         if (reqLength.equals("greaterThanMax")) {
/* 1394 */           isValid = false;
/*      */         }
/*      */       }
/*      */       else
/*      */       {
/* 1399 */         isValid = false;
/*      */       }
/*      */     }
/*      */     
/*      */ 
/* 1404 */     if (!isValid)
/*      */     {
/* 1406 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_MERLOCID);
/* 1407 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateMerCtcInfoTxt(DataCaptureRequestBean dataCaptureRequestBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1425 */     boolean isValid = true;
/* 1426 */     ErrorObject errorObj = null;
/* 1427 */     if (!CommonValidator.isNullOrEmpty(dataCaptureRequestBean
/* 1428 */       .getMerCtcInfoTxt())) {
/* 1429 */       if (CommonValidator.validateData(dataCaptureRequestBean
/* 1430 */         .getMerCtcInfoTxt(), 
/* 1431 */         com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[54]))
/*      */       {
/* 1433 */         String reqLength = CommonValidator.validateLength(
/* 1434 */           dataCaptureRequestBean.getMerCtcInfoTxt(), 40, 1);
/*      */         
/* 1436 */         if (reqLength.equals("greaterThanMax")) {
/* 1437 */           isValid = false;
/*      */         }
/*      */       }
/*      */       else {
/* 1441 */         isValid = false;
/*      */       }
/*      */     }
/*      */     
/*      */ 
/* 1446 */     if (!isValid)
/*      */     {
/* 1448 */       errorObj = new ErrorObject(
/* 1449 */         XMLSubmissionErrorCodes.ERROR_MERCTCINFOTXT);
/* 1450 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateMatchKeyTypeCd(DataCaptureRequestBean dataCaptureRequestBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1467 */     boolean isValid = true;
/* 1468 */     ErrorObject errorObj = null;
/* 1469 */     if (CommonValidator.isNullOrEmpty(dataCaptureRequestBean
/* 1470 */       .getMatchKeyTypeCd()))
/*      */     {
/* 1472 */       if (!CommonValidator.isNullOrEmpty(dataCaptureRequestBean
/* 1473 */         .getMatchKeyId()))
/*      */       {
/* 1475 */         isValid = false;
/*      */       }
/*      */       
/*      */ 
/*      */     }
/* 1480 */     else if (CommonValidator.validateData(dataCaptureRequestBean
/* 1481 */       .getMatchKeyTypeCd(), 
/* 1482 */       com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[55]))
/*      */     {
/* 1484 */       String reqLength = CommonValidator.validateLength(
/* 1485 */         dataCaptureRequestBean.getMatchKeyTypeCd(), 2, 2);
/*      */       
/* 1487 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/* 1489 */         isValid = false;
/*      */       }
/*      */     }
/*      */     else
/*      */     {
/* 1494 */       isValid = false;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/* 1499 */     if (!isValid)
/*      */     {
/* 1501 */       errorObj = new ErrorObject(
/* 1502 */         XMLSubmissionErrorCodes.ERROR_MATCHKEYTYPECD);
/* 1503 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateMatchKeyId(DataCaptureRequestBean dataCaptureRequestBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1520 */     boolean isValid = true;
/* 1521 */     ErrorObject errorObj = null;
/* 1522 */     if (CommonValidator.isNullOrEmpty(dataCaptureRequestBean
/* 1523 */       .getMatchKeyId()))
/*      */     {
/* 1525 */       if (!CommonValidator.isNullOrEmpty(dataCaptureRequestBean
/* 1526 */         .getMatchKeyTypeCd()))
/*      */       {
/* 1528 */         isValid = false;
/*      */ 
/*      */       }
/*      */       
/*      */ 
/*      */     }
/* 1534 */     else if (CommonValidator.validateData(dataCaptureRequestBean
/* 1535 */       .getMatchKeyId(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[56]))
/*      */     {
/* 1537 */       String reqLength = CommonValidator.validateLength(
/* 1538 */         dataCaptureRequestBean.getMatchKeyId(), 21, 1);
/*      */       
/* 1540 */       if (reqLength.equals("greaterThanMax")) {
/* 1541 */         isValid = false;
/*      */       }
/*      */     }
/*      */     else
/*      */     {
/* 1546 */       isValid = false;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/* 1551 */     if (!isValid)
/*      */     {
/* 1553 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_MATCHKEYID);
/* 1554 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateDefPaymentPlan(DataCaptureRequestBean dataCaptureRequestBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1571 */     boolean isValid = true;
/* 1572 */     ErrorObject errorObj = null;
/* 1573 */     if (CommonValidator.isNullOrEmpty(dataCaptureRequestBean
/* 1574 */       .getDefPaymentPlan()))
/*      */     {
/* 1576 */       if (!CommonValidator.isNullOrEmpty(dataCaptureRequestBean
/* 1577 */         .getNumDefPayments()))
/*      */       {
/* 1579 */         isValid = false;
/*      */       }
/*      */       
/*      */ 
/*      */     }
/* 1584 */     else if (CommonValidator.validateData(dataCaptureRequestBean
/* 1585 */       .getDefPaymentPlan(), 
/* 1586 */       com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[57]))
/*      */     {
/* 1588 */       String reqLength = CommonValidator.validateLength(
/* 1589 */         dataCaptureRequestBean.getDefPaymentPlan(), 4, 4);
/*      */       
/* 1591 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/* 1593 */         isValid = false;
/*      */       }
/*      */     }
/*      */     else
/*      */     {
/* 1598 */       isValid = false;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/* 1603 */     if (!isValid)
/*      */     {
/* 1605 */       errorObj = new ErrorObject(
/* 1606 */         XMLSubmissionErrorCodes.ERROR_DEFERREDPAYMENTPLAN);
/* 1607 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateNumDefPayments(DataCaptureRequestBean dataCaptureRequestBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1625 */     ErrorObject errorObj = null;
/* 1626 */     boolean isValid = true;
/* 1627 */     if (CommonValidator.isNullOrEmpty(dataCaptureRequestBean
/* 1628 */       .getNumDefPayments()))
/*      */     {
/* 1630 */       if (!CommonValidator.isNullOrEmpty(dataCaptureRequestBean
/* 1631 */         .getDefPaymentPlan()))
/*      */       {
/* 1633 */         isValid = false;
/*      */ 
/*      */       }
/*      */       
/*      */ 
/*      */     }
/* 1639 */     else if (CommonValidator.validateData(dataCaptureRequestBean
/* 1640 */       .getNumDefPayments(), 
/* 1641 */       com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[58]))
/*      */     {
/* 1643 */       String numDefPay = dataCaptureRequestBean.getNumDefPayments();
/*      */       
/* 1645 */       if ((Integer.parseInt(numDefPay) <= 2) || 
/* 1646 */         (Integer.parseInt(numDefPay) >= 99)) {
/* 1647 */         isValid = false;
/*      */       }
/*      */     } else {
/* 1650 */       isValid = false;
/*      */     }
/*      */     
/*      */ 
/* 1654 */     if (!isValid)
/*      */     {
/* 1656 */       errorObj = new ErrorObject(
/* 1657 */         XMLSubmissionErrorCodes.ERROR_NUMBEROFDEFERREDPAYMENTS);
/* 1658 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateEMVData(DataCaptureRequestBean dataCaptureRequestBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1677 */     ErrorObject errorObj = null;
/* 1678 */     boolean isValid = true;
/* 1679 */     if (!CommonValidator.isNullOrEmpty(dataCaptureRequestBean.getEmvData())) {
/* 1680 */       if (CommonValidator.validateData(dataCaptureRequestBean
/* 1681 */         .getEmvData(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[59]))
/*      */       {
/* 1683 */         String reqLength = CommonValidator.validateLength(
/* 1684 */           dataCaptureRequestBean.getEmvData(), 256, 1);
/*      */         
/* 1686 */         if (reqLength.equals("greaterThanMax")) {
/* 1687 */           isValid = false;
/*      */         }
/*      */       }
/*      */       else {
/* 1691 */         isValid = false;
/*      */       }
/*      */     }
/*      */     
/* 1695 */     if (!isValid)
/*      */     {
/* 1697 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_EMVDATA);
/* 1698 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateMerCtgyCd(DataCaptureRequestBean dataCaptureRequestBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1716 */     boolean isValid = true;
/* 1717 */     ErrorObject errorObj = null;
/* 1718 */     if (!CommonValidator.isNullOrEmpty(dataCaptureRequestBean
/* 1719 */       .getMerCtgyCd()))
/*      */     {
/* 1721 */       if (CommonValidator.validateData(dataCaptureRequestBean
/* 1722 */         .getMerCtgyCd(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[60]))
/*      */       {
/* 1724 */         String reqLength = CommonValidator.validateLength(
/* 1725 */           dataCaptureRequestBean.getMerCtgyCd(), 4, 4);
/*      */         
/* 1727 */         if (reqLength.equals("greaterThanMax")) {
/* 1728 */           isValid = false;
/*      */         }
/*      */       }
/*      */       else
/*      */       {
/* 1733 */         isValid = false;
/*      */       }
/*      */     }
/*      */     
/* 1737 */     if (!isValid)
/*      */     {
/* 1739 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_MERCTGYCD);
/* 1740 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateSellId(DataCaptureRequestBean dataCaptureRequestBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1757 */     boolean isValid = true;
/* 1758 */     ErrorObject errorObj = null;
/* 1759 */     if (!CommonValidator.isNullOrEmpty(dataCaptureRequestBean.getSellId())) {
/* 1760 */       if (CommonValidator.validateData(
/* 1761 */         dataCaptureRequestBean.getSellId(), 
/* 1762 */         com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[61]))
/*      */       {
/* 1764 */         String reqLength = CommonValidator.validateLength(
/* 1765 */           dataCaptureRequestBean.getSellId(), 1, 20);
/*      */         
/* 1767 */         if (reqLength.equals("greaterThanMax")) {
/* 1768 */           isValid = false;
/*      */         }
/*      */       }
/*      */       else {
/* 1772 */         isValid = false;
/*      */       }
/*      */     }
/*      */     
/*      */ 
/* 1777 */     if (!isValid)
/*      */     {
/* 1779 */       errorObj = new ErrorObject(XMLSubmissionErrorCodes.ERROR_SELLID);
/* 1780 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateAddAmtTypeCd(CardTransactionDetailBean cardTransactionDetailBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1799 */     ErrorObject errorObj = null;
/* 1800 */     boolean isValid = true;
/* 1801 */     if (CommonValidator.isNullOrEmpty(cardTransactionDetailBean
/* 1802 */       .getAddAmtTypeCd())) {
/* 1803 */       isValid = false;
/*      */ 
/*      */     }
/*      */     else
/*      */     {
/* 1808 */       if (CommonValidator.validateData(cardTransactionDetailBean.getAddAmtTypeCd(), 
/* 1809 */         com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[62]))
/*      */       {
/* 1811 */         if (CommonValidator.isValidAddAmtTypeCd(cardTransactionDetailBean
/* 1812 */           .getAddAmtTypeCd()))
/*      */         {
/* 1814 */           String reqLength = CommonValidator.validateLength(
/* 1815 */             cardTransactionDetailBean.getAddAmtTypeCd(), 3, 3);
/*      */           
/* 1817 */           if (!reqLength.equals("greaterThanMax")) break label73;
/* 1818 */           isValid = false;
/*      */           break label73;
/*      */         }
/*      */       }
/* 1822 */       isValid = false;
/*      */     }
/*      */     
/*      */     label73:
/*      */     
/* 1827 */     if (!isValid)
/*      */     {
/* 1829 */       errorObj = new ErrorObject(
/* 1830 */         XMLSubmissionErrorCodes.ERROR_ADDITIONALAMOUNTTYPE);
/* 1831 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateAddAmt(CardTransactionDetailBean cardTransactionDetailBean, List<ErrorObject> errorCodes, String transCurrCd)
/*      */     throws SettlementException
/*      */   {
/* 1851 */     ErrorObject errorObj = null;
/* 1852 */     boolean isValid = true;
/*      */     
/* 1854 */     if (CommonValidator.isNullOrEmpty(cardTransactionDetailBean.getAddAmt())) {
/* 1855 */       if (!CommonValidator.isNullOrEmpty(cardTransactionDetailBean
/* 1856 */         .getSignInd())) {
/* 1857 */         isValid = false;
/*      */       }
/*      */       
/*      */ 
/*      */     }
/* 1862 */     else if (CommonValidator.validateData(cardTransactionDetailBean
/* 1863 */       .getAddAmt(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[63]))
/*      */     {
/* 1865 */       String reqLength = CommonValidator.validateLength(
/* 1866 */         cardTransactionDetailBean.getAddAmt(), 12, 1);
/*      */       
/* 1868 */       if (reqLength.equals("greaterThanMax")) {
/* 1869 */         isValid = false;
/*      */       }
/* 1871 */       else if ((!CommonValidator.isNullOrEmpty(transCurrCd)) && 
/* 1872 */         (!CommonValidator.isValidXmlAmount(transCurrCd, cardTransactionDetailBean.getAddAmt()))) {
/* 1873 */         isValid = false;
/*      */       }
/*      */     }
/*      */     else
/*      */     {
/* 1878 */       isValid = false;
/*      */     }
/*      */     
/*      */ 
/* 1882 */     if (!isValid)
/*      */     {
/* 1884 */       errorObj = new ErrorObject(
/* 1885 */         XMLSubmissionErrorCodes.ERROR_ADDITIONALAMOUNT);
/* 1886 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateSignInd(CardTransactionDetailBean cardTransactionDetailBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1903 */     ErrorObject errorObj = null;
/* 1904 */     boolean isValid = true;
/* 1905 */     if (CommonValidator.isNullOrEmpty(cardTransactionDetailBean
/* 1906 */       .getSignInd()))
/*      */     {
/* 1908 */       if (!CommonValidator.isNullOrEmpty(cardTransactionDetailBean
/* 1909 */         .getAddAmt())) {
/* 1910 */         isValid = false;
/*      */       }
/*      */       
/*      */ 
/*      */     }
/* 1915 */     else if (CommonValidator.validateData(cardTransactionDetailBean
/* 1916 */       .getSignInd(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[64]))
/*      */     {
/* 1918 */       String reqLength = CommonValidator.validateLength(
/* 1919 */         cardTransactionDetailBean.getSignInd(), 1, 1);
/*      */       
/* 1921 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/* 1923 */         isValid = false;
/*      */       }
/*      */     }
/*      */     else
/*      */     {
/* 1928 */       isValid = false;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/* 1933 */     if (!isValid)
/*      */     {
/* 1935 */       errorObj = new ErrorObject(
/* 1936 */         XMLSubmissionErrorCodes.ERROR_ADDITIONALAMOUNTSIGN);
/* 1937 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTransAddCd(DataCaptureRequestBean dataCaptureRequestBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1955 */     ErrorObject errorObj = null;
/* 1956 */     boolean isValid = true;
/* 1957 */     if (!CommonValidator.isNullOrEmpty(dataCaptureRequestBean
/* 1958 */       .getTransAddCd())) {
/* 1959 */       if (CommonValidator.validateData(dataCaptureRequestBean
/* 1960 */         .getTransAddCd(), com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE[69]))
/*      */       {
/* 1962 */         String reqLength = CommonValidator.validateLength(
/* 1963 */           dataCaptureRequestBean.getTransAddCd(), 2, 2);
/*      */         
/* 1965 */         if (reqLength.equals("greaterThanMax")) {
/* 1966 */           isValid = false;
/*      */         }
/*      */       } else {
/* 1969 */         isValid = false;
/*      */       }
/*      */       
/*      */     }
/* 1973 */     else if (CommonValidator.isIndustryRecordPresent(dataCaptureRequestBean)) {
/* 1974 */       errorObj = new ErrorObject(
/* 1975 */         XMLSubmissionErrorCodes.ERROR_ADDENDUMNOTFOUND);
/* 1976 */       errorCodes.add(errorObj);
/*      */     }
/*      */     
/*      */ 
/* 1980 */     if (!isValid)
/*      */     {
/* 1982 */       errorObj = new ErrorObject(
/* 1983 */         XMLSubmissionErrorCodes.ERROR_ADDENDUMCODE);
/* 1984 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTransImageSeqNbr(DataCaptureRequestBean dataCaptureRequestBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 2002 */     ErrorObject errorObj = null;
/* 2003 */     boolean isValid = true;
/* 2004 */     if (!CommonValidator.isNullOrEmpty(dataCaptureRequestBean
/* 2005 */       .getTransImageSeqNbr()))
/*      */     {
/* 2007 */       if (CommonValidator.validateData(dataCaptureRequestBean
/* 2008 */         .getTransImageSeqNbr(), 
/* 2009 */         com.americanexpress.ips.gfsg.constants.XMLSubmissionConstants.RECORD_VALUE['Â']))
/*      */       {
/* 2011 */         String reqLength = CommonValidator.validateLength(
/* 2012 */           dataCaptureRequestBean.getTransImageSeqNbr(), 8, 1);
/*      */         
/* 2014 */         if (reqLength.equals("greaterThanMax"))
/*      */         {
/* 2016 */           isValid = false;
/*      */         }
/*      */       }
/*      */       else {
/* 2020 */         isValid = false;
/*      */       }
/*      */     }
/*      */     
/*      */ 
/* 2025 */     if (!isValid)
/*      */     {
/* 2027 */       errorObj = new ErrorObject(
/* 2028 */         XMLSubmissionErrorCodes.DCS_ERROR_TRANS_IMAGE_SEQ_NBR);
/* 2029 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateCardTransDetail(DataCaptureRequestBean dataCaptureRequestBean, List<ErrorObject> errorCodes, String transCurrCd)
/*      */     throws SettlementException
/*      */   {
/* 2048 */     if ((dataCaptureRequestBean.getCardTransDetail() != null) && 
/* 2049 */       (!dataCaptureRequestBean.getCardTransDetail().isEmpty()))
/*      */     {
/* 2051 */       Iterator localIterator = dataCaptureRequestBean.getCardTransDetail().iterator();
/* 2050 */       while (localIterator.hasNext()) {
/* 2051 */         CardTransactionDetailBean cardTransactionDetail = (CardTransactionDetailBean)localIterator.next();
/* 2052 */         validateAddAmtTypeCd(cardTransactionDetail, errorCodes);
/* 2053 */         validateAddAmt(cardTransactionDetail, errorCodes, transCurrCd);
/* 2054 */         validateSignInd(cardTransactionDetail, errorCodes);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validatePOSCodeData(DataCaptureRequestBean dataCaptureRequestBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 2072 */     ErrorObject errorObj = null;
/*      */     
/* 2074 */     if (dataCaptureRequestBean.getPointOfServiceData() == null) {
/* 2075 */       errorObj = new ErrorObject(
/* 2076 */         XMLSubmissionErrorCodes.ERROR_POINTOFSERVICEDATA);
/* 2077 */       errorCodes.add(errorObj);
/*      */     }
/*      */     else {
/* 2080 */       validateCardDataInpCpblCd(
/* 2081 */         dataCaptureRequestBean.getPointOfServiceData(), errorCodes);
/* 2082 */       validateCMAuthnCpblCd(dataCaptureRequestBean
/* 2083 */         .getPointOfServiceData(), errorCodes);
/* 2084 */       validateCardCptrCpblCd(dataCaptureRequestBean
/* 2085 */         .getPointOfServiceData(), errorCodes);
/* 2086 */       validateOprEnvirCd(dataCaptureRequestBean.getPointOfServiceData(), 
/* 2087 */         errorCodes);
/* 2088 */       validateCMPresentCd(dataCaptureRequestBean.getPointOfServiceData(), 
/* 2089 */         errorCodes);
/* 2090 */       validateCardPresentCd(dataCaptureRequestBean
/* 2091 */         .getPointOfServiceData(), errorCodes);
/* 2092 */       validateCardDataInpModeCd(dataCaptureRequestBean
/* 2093 */         .getPointOfServiceData(), errorCodes);
/* 2094 */       validateCMAuthnMthdCd(dataCaptureRequestBean
/* 2095 */         .getPointOfServiceData(), errorCodes);
/* 2096 */       validateCMAuthnEnttyCd(dataCaptureRequestBean
/* 2097 */         .getPointOfServiceData(), errorCodes);
/* 2098 */       validateCardDataOpCpblCd(dataCaptureRequestBean
/* 2099 */         .getPointOfServiceData(), errorCodes);
/* 2100 */       validateTrmnlOpCpblCd(dataCaptureRequestBean
/* 2101 */         .getPointOfServiceData(), errorCodes);
/* 2102 */       validatePINCptrCpblCd(dataCaptureRequestBean
/* 2103 */         .getPointOfServiceData(), errorCodes);
/*      */     }
/*      */   }
/*      */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\validator\xml\DataCaptureRequestValidator.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */