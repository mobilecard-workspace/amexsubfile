/*      */ package com.americanexpress.ips.gfsg.validator;
/*      */ 
/*      */ import com.americanexpress.ips.gfsg.beans.ErrorObject;
/*      */ import com.americanexpress.ips.gfsg.beans.common.TransactionAdviceBasicBean;
/*      */ import com.americanexpress.ips.gfsg.beans.common.TransactionBatchTrailerBean;
/*      */ import com.americanexpress.ips.gfsg.beans.common.TransactionTBTSpecificBean;
/*      */ import com.americanexpress.ips.gfsg.enumerations.ProcessingCode;
/*      */ import com.americanexpress.ips.gfsg.enumerations.RecordType;
/*      */ import com.americanexpress.ips.gfsg.enumerations.SubmissionErrorCodes;
/*      */ import com.americanexpress.ips.gfsg.enumerations.SubmissionMethod;
/*      */ import com.americanexpress.ips.gfsg.exceptions.SettlementException;
/*      */ import java.util.Iterator;
/*      */ import java.util.List;
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ public class TBTRecValidator
/*      */ {
/*      */   private static String recordNumber;
/*      */   
/*      */   public static void validateTBTRecord(TransactionTBTSpecificBean transactionTBTSpecificBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*   42 */     recordNumber = 
/*   43 */       transactionTBTSpecificBean.getTransactionBatchTrailerBean().getRecordNumber();
/*   44 */     validateRecordType(transactionTBTSpecificBean
/*   45 */       .getTransactionBatchTrailerBean(), errorCodes);
/*   46 */     validateRecordNumber(transactionTBTSpecificBean
/*   47 */       .getTransactionBatchTrailerBean(), errorCodes);
/*   48 */     validateMerchantId(transactionTBTSpecificBean, errorCodes);
/*   49 */     validateTBTId(transactionTBTSpecificBean
/*   50 */       .getTransactionBatchTrailerBean(), errorCodes);
/*   51 */     validateTBTCreationDate(transactionTBTSpecificBean
/*   52 */       .getTransactionBatchTrailerBean(), errorCodes);
/*   53 */     validateTotalNoOfTabs(transactionTBTSpecificBean, errorCodes);
/*   54 */     validateTBTAmount(transactionTBTSpecificBean, errorCodes);
/*      */     
/*      */ 
/*   57 */     validateTBTCurrencyCode(transactionTBTSpecificBean, errorCodes);
/*   58 */     validateTBTImageSequenceNumber(transactionTBTSpecificBean, errorCodes);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateRecordType(TransactionBatchTrailerBean transactionBatchTrailerBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*   75 */     ErrorObject errorObj = null;
/*      */     
/*   77 */     if (CommonValidator.isNullOrEmpty(transactionBatchTrailerBean
/*   78 */       .getRecordType()))
/*      */     {
/*   80 */       errorObj = new ErrorObject(
/*   81 */         SubmissionErrorCodes.ERROR_DATAFIELD1_RECORDTYPE
/*   82 */         .getErrorCode(), 
/*   83 */         SubmissionErrorCodes.ERROR_DATAFIELD1_RECORDTYPE
/*   84 */         .getErrorDescription() + 
/*   85 */         "\n" + 
/*   86 */         "TBT" + 
/*   87 */         "|" + 
/*   88 */         "RecordNumber:" + 
/*   89 */         recordNumber + 
/*   90 */         "|" + 
/*   91 */         "RecordType:" + 
/*   92 */         "|" + 
/*   93 */         "This field is mandatory and cannot be empty");
/*   94 */       errorCodes.add(errorObj);
/*      */     }
/*   96 */     else if (!RecordType.Transaction_Batch_Trailer.getRecordType().equalsIgnoreCase(
/*   97 */       transactionBatchTrailerBean.getRecordType()))
/*      */     {
/*      */ 
/*  100 */       errorObj = new ErrorObject(
/*  101 */         SubmissionErrorCodes.ERROR_DATAFIELD1_RECORDTYPE);
/*  102 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateRecordNumber(TransactionBatchTrailerBean transactionBatchTrailerBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  120 */     ErrorObject errorObj = null;
/*      */     
/*  122 */     if (CommonValidator.isNullOrEmpty(transactionBatchTrailerBean
/*  123 */       .getRecordNumber()))
/*      */     {
/*  125 */       errorObj = new ErrorObject(
/*  126 */         SubmissionErrorCodes.ERROR_DATAFIELD585_RECORDNUMBER
/*  127 */         .getErrorCode(), 
/*  128 */         SubmissionErrorCodes.ERROR_DATAFIELD585_RECORDNUMBER
/*  129 */         .getErrorDescription() + 
/*  130 */         "\n" + 
/*  131 */         "RecordType:" + 
/*  132 */         "TBT" + 
/*  133 */         "|" + 
/*  134 */         "RecordNumber:" + 
/*  135 */         recordNumber + 
/*  136 */         "|" + 
/*  137 */         "RecordNumber:" + 
/*  138 */         "|" + 
/*  139 */         "This field is mandatory and cannot be empty");
/*  140 */       errorCodes.add(errorObj);
/*      */     }
/*  142 */     else if (CommonValidator.validateData(transactionBatchTrailerBean
/*  143 */       .getRecordNumber(), com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[112]))
/*      */     {
/*  145 */       String reqLength = CommonValidator.validateLength(
/*  146 */         transactionBatchTrailerBean.getRecordNumber(), 8, 8);
/*      */       
/*  148 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  150 */         errorObj = new ErrorObject(
/*  151 */           SubmissionErrorCodes.ERROR_DATAFIELD3_RECORDNUMBER
/*  152 */           .getErrorCode(), 
/*  153 */           SubmissionErrorCodes.ERROR_DATAFIELD3_RECORDNUMBER
/*  154 */           .getErrorDescription() + 
/*  155 */           "\n" + 
/*  156 */           "RecordType:" + 
/*  157 */           "TBT" + 
/*  158 */           "|" + 
/*  159 */           "RecordNumber:" + 
/*  160 */           recordNumber + 
/*  161 */           "|" + 
/*  162 */           "RecordNumber:" + 
/*  163 */           "|" + 
/*  164 */           "This field length Cannot be greater than 8");
/*  165 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/*  168 */       errorObj = new ErrorObject(
/*  169 */         SubmissionErrorCodes.ERROR_DATAFIELD2_RECORDNUMBER
/*  170 */         .getErrorCode(), 
/*  171 */         SubmissionErrorCodes.ERROR_DATAFIELD2_RECORDNUMBER
/*  172 */         .getErrorDescription() + 
/*  173 */         "\n" + 
/*  174 */         "RecordType:" + 
/*  175 */         "TBT" + 
/*  176 */         "|" + 
/*  177 */         "RecordNumber:" + 
/*  178 */         recordNumber + 
/*  179 */         "|" + 
/*  180 */         "RecordNumber:" + 
/*  181 */         "|" + 
/*  182 */         "This field can only be Numeric");
/*  183 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateMerchantId(TransactionTBTSpecificBean transactionTBTSpecifictType, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  201 */     ErrorObject errorObj = null;
/*      */     
/*  203 */     String value = transactionTBTSpecifictType
/*  204 */       .getTransactionBatchTrailerBean().getMerchantId();
/*  205 */     if (CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  207 */       errorObj = new ErrorObject(
/*  208 */         SubmissionErrorCodes.ERROR_DATAFIELD117_MERCHANT_ID
/*  209 */         .getErrorCode(), 
/*  210 */         SubmissionErrorCodes.ERROR_DATAFIELD117_MERCHANT_ID
/*  211 */         .getErrorDescription() + 
/*  212 */         "\n" + 
/*  213 */         "RecordType:" + 
/*  214 */         "TBT" + 
/*  215 */         "|" + 
/*  216 */         "RecordNumber:" + 
/*  217 */         recordNumber + 
/*  218 */         "|" + 
/*  219 */         "MerchantId" + 
/*  220 */         "|" + 
/*  221 */         "This field is mandatory and cannot be empty");
/*  222 */       errorCodes.add(errorObj);
/*      */     }
/*  224 */     else if (CommonValidator.validateData(value, 
/*  225 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[113]))
/*      */     {
/*  227 */       String reqLength = 
/*  228 */         CommonValidator.validateLength(value, 15, 15);
/*      */       
/*  230 */       if (reqLength.equals("greaterThanMax")) {
/*  231 */         errorObj = new ErrorObject(
/*  232 */           SubmissionErrorCodes.ERROR_DATAFIELD116_MERCHANT_ID
/*  233 */           .getErrorCode(), 
/*  234 */           SubmissionErrorCodes.ERROR_DATAFIELD116_MERCHANT_ID
/*  235 */           .getErrorDescription() + 
/*  236 */           "\n" + 
/*  237 */           "RecordType:" + 
/*  238 */           "TBT" + 
/*  239 */           "|" + 
/*  240 */           "RecordNumber:" + 
/*  241 */           recordNumber + 
/*  242 */           "|" + 
/*  243 */           "MerchantId" + 
/*  244 */           "|" + 
/*  245 */           "This field length Cannot be greater than 15");
/*  246 */         errorCodes.add(errorObj);
/*      */ 
/*      */       }
/*  249 */       else if (CommonValidator.isValidModulusNineCheck(value))
/*      */       {
/*  251 */         if (!compareMerchantId(transactionTBTSpecifictType))
/*      */         {
/*  253 */           errorObj = new ErrorObject(
/*  254 */             SubmissionErrorCodes.ERROR_DATAFIELD118_MERCHANT_ID
/*  255 */             .getErrorCode(), 
/*  256 */             SubmissionErrorCodes.ERROR_DATAFIELD118_MERCHANT_ID
/*  257 */             .getErrorDescription() + 
/*  258 */             "\n" + 
/*  259 */             "RecordType:" + 
/*  260 */             "TBT" + 
/*  261 */             "|" + 
/*  262 */             "RecordNumber:" + 
/*  263 */             recordNumber + 
/*  264 */             "|" + 
/*  265 */             "MerchantId" + 
/*  266 */             "|" + 
/*  267 */             SubmissionErrorCodes.ERROR_DATAFIELD118_MERCHANT_ID
/*  268 */             .getErrorDescription());
/*  269 */           errorCodes.add(errorObj);
/*      */         }
/*      */       }
/*      */       else
/*      */       {
/*  274 */         errorObj = new ErrorObject(
/*  275 */           SubmissionErrorCodes.ERROR_DATAFIELD116_MERCHANT_ID
/*  276 */           .getErrorCode(), 
/*  277 */           SubmissionErrorCodes.ERROR_DATAFIELD116_MERCHANT_ID
/*  278 */           .getErrorDescription() + 
/*  279 */           "\n" + 
/*  280 */           "RecordType:" + 
/*  281 */           "TBT" + 
/*  282 */           "|" + 
/*  283 */           "RecordNumber:" + 
/*  284 */           recordNumber + 
/*  285 */           "|" + 
/*  286 */           "MerchantId" + 
/*  287 */           "|" + 
/*  288 */           SubmissionErrorCodes.ERROR_DATAFIELD116_MERCHANT_ID
/*  289 */           .getErrorDescription());
/*  290 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */     else {
/*  294 */       errorObj = new ErrorObject(
/*  295 */         SubmissionErrorCodes.ERROR_DATAFIELD116_MERCHANT_ID
/*  296 */         .getErrorCode(), 
/*  297 */         SubmissionErrorCodes.ERROR_DATAFIELD116_MERCHANT_ID
/*  298 */         .getErrorDescription() + 
/*  299 */         "\n" + 
/*  300 */         "RecordType:" + 
/*  301 */         "TBT" + 
/*  302 */         "|" + 
/*  303 */         "RecordNumber:" + 
/*  304 */         recordNumber + 
/*  305 */         "|" + 
/*  306 */         "MerchantId" + 
/*  307 */         "|" + 
/*  308 */         "This field can only be AlphaNumeric");
/*  309 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTBTId(TransactionBatchTrailerBean transactionBatchTrailerBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  327 */     ErrorObject errorObj = null;
/*      */     
/*  329 */     if (!CommonValidator.isNullOrEmpty(transactionBatchTrailerBean
/*  330 */       .getTbtIdentificationNumber()))
/*      */     {
/*  332 */       if (CommonValidator.validateData(transactionBatchTrailerBean
/*  333 */         .getTbtIdentificationNumber(), 
/*  334 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[115]))
/*      */       {
/*  336 */         String reqLength = CommonValidator.validateLength(
/*  337 */           transactionBatchTrailerBean
/*  338 */           .getTbtIdentificationNumber(), 15, 15);
/*      */         
/*  340 */         if (reqLength.equals("greaterThanMax"))
/*      */         {
/*  342 */           errorObj = new ErrorObject(
/*  343 */             SubmissionErrorCodes.ERROR_DATAFIELD25_TRANSACTION_IDENTIFIER
/*  344 */             .getErrorCode(), 
/*  345 */             SubmissionErrorCodes.ERROR_DATAFIELD25_TRANSACTION_IDENTIFIER
/*  346 */             .getErrorDescription() + 
/*  347 */             "\n" + 
/*  348 */             "RecordType:" + 
/*  349 */             "TBT" + 
/*  350 */             "|" + 
/*  351 */             "RecordNumber:" + 
/*  352 */             recordNumber + 
/*  353 */             "|" + 
/*  354 */             "TBTID" + 
/*  355 */             "|" + 
/*  356 */             "This field length Cannot be greater than 15");
/*  357 */           errorCodes.add(errorObj);
/*      */         }
/*      */       }
/*      */       else {
/*  361 */         errorObj = new ErrorObject(
/*  362 */           SubmissionErrorCodes.ERROR_DATAFIELD122_TBTIDENTIFICATION_NUMBER
/*  363 */           .getErrorCode(), 
/*  364 */           SubmissionErrorCodes.ERROR_DATAFIELD122_TBTIDENTIFICATION_NUMBER
/*  365 */           .getErrorDescription() + 
/*  366 */           "\n" + 
/*  367 */           "RecordType:" + 
/*  368 */           "TBT" + 
/*  369 */           "|" + 
/*  370 */           "RecordNumber:" + 
/*  371 */           recordNumber + 
/*  372 */           "|" + 
/*  373 */           "TBTID" + 
/*  374 */           "|" + 
/*  375 */           "This field can only be Numeric");
/*  376 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTBTCreationDate(TransactionBatchTrailerBean transactionBatchTrailerBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  394 */     ErrorObject errorObj = null;
/*      */     
/*  396 */     if (CommonValidator.isNullOrEmpty(transactionBatchTrailerBean
/*  397 */       .getTbtCreationDate()))
/*      */     {
/*  399 */       errorObj = new ErrorObject(
/*  400 */         SubmissionErrorCodes.ERROR_DATAFIELD123_TBT_CREATION_DATE
/*  401 */         .getErrorCode(), 
/*  402 */         SubmissionErrorCodes.ERROR_DATAFIELD123_TBT_CREATION_DATE
/*  403 */         .getErrorDescription() + 
/*  404 */         "\n" + 
/*  405 */         "RecordType:" + 
/*  406 */         "TBT" + 
/*  407 */         "|" + 
/*  408 */         "RecordNumber:" + 
/*  409 */         recordNumber + 
/*  410 */         "|" + 
/*  411 */         "TbtCreationDate" + 
/*  412 */         "|" + 
/*  413 */         "This field is mandatory and cannot be empty");
/*  414 */       errorCodes.add(errorObj);
/*      */     }
/*  416 */     else if (CommonValidator.validateData(transactionBatchTrailerBean
/*  417 */       .getTbtCreationDate(), 
/*  418 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[116]))
/*      */     {
/*  420 */       String reqLength = CommonValidator.validateLength(
/*  421 */         transactionBatchTrailerBean.getTbtCreationDate(), 8, 8);
/*      */       
/*  423 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  425 */         errorObj = new ErrorObject(
/*  426 */           SubmissionErrorCodes.ERROR_DATAFIELD123_TBT_CREATION_DATE
/*  427 */           .getErrorCode(), 
/*  428 */           SubmissionErrorCodes.ERROR_DATAFIELD123_TBT_CREATION_DATE
/*  429 */           .getErrorDescription() + 
/*  430 */           "\n" + 
/*  431 */           "RecordType:" + 
/*  432 */           "TBT" + 
/*  433 */           "|" + 
/*  434 */           "RecordNumber:" + 
/*  435 */           recordNumber + 
/*  436 */           "|" + 
/*  437 */           "TbtCreationDate" + 
/*  438 */           "|" + 
/*  439 */           "This field length Cannot be greater than 8");
/*  440 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */     else {
/*  444 */       errorObj = new ErrorObject(
/*  445 */         SubmissionErrorCodes.ERROR_DATAFIELD123_TBT_CREATION_DATE
/*  446 */         .getErrorCode(), 
/*  447 */         SubmissionErrorCodes.ERROR_DATAFIELD123_TBT_CREATION_DATE
/*  448 */         .getErrorDescription() + 
/*  449 */         "\n" + 
/*  450 */         "RecordType:" + 
/*  451 */         "TBT" + 
/*  452 */         "|" + 
/*  453 */         "RecordNumber:" + 
/*  454 */         recordNumber + 
/*  455 */         "|" + 
/*  456 */         "TbtCreationDate" + 
/*  457 */         "|" + 
/*  458 */         "This field can only be Numeric");
/*  459 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTotalNoOfTabs(TransactionTBTSpecificBean transactionTBTSpecificBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  479 */     ErrorObject errorObj = null;
/*  480 */     String noOfTBTTABS = transactionTBTSpecificBean
/*  481 */       .getTransactionBatchTrailerBean().getTotalNoOfTabs();
/*      */     
/*  483 */     if (CommonValidator.isNullOrEmpty(noOfTBTTABS))
/*      */     {
/*  485 */       errorObj = new ErrorObject(
/*  486 */         SubmissionErrorCodes.ERROR_DATAFIELD595_NUMBEROF_TABS
/*  487 */         .getErrorCode(), 
/*  488 */         SubmissionErrorCodes.ERROR_DATAFIELD595_NUMBEROF_TABS
/*  489 */         .getErrorDescription() + 
/*  490 */         "\n" + 
/*  491 */         "RecordType:" + 
/*  492 */         "TBT" + 
/*  493 */         "|" + 
/*  494 */         "RecordNumber:" + 
/*  495 */         recordNumber + 
/*  496 */         "|" + 
/*  497 */         "TotalNoOfTabs" + 
/*  498 */         "|" + 
/*  499 */         "This field can only be Numeric");
/*  500 */       errorCodes.add(errorObj);
/*      */     }
/*  502 */     else if (CommonValidator.validateData(noOfTBTTABS, 
/*  503 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[117]))
/*      */     {
/*  505 */       String reqLength = CommonValidator.validateLength(noOfTBTTABS, 
/*  506 */         8, 8);
/*      */       
/*  508 */       if (reqLength.equals("greaterThanMax")) {
/*  509 */         errorObj = new ErrorObject(
/*  510 */           SubmissionErrorCodes.ERROR_DATAFIELD595_NUMBEROF_TABS
/*  511 */           .getErrorCode(), 
/*  512 */           SubmissionErrorCodes.ERROR_DATAFIELD595_NUMBEROF_TABS
/*  513 */           .getErrorDescription() + 
/*  514 */           "\n" + 
/*  515 */           "RecordType:" + 
/*  516 */           "TBT" + 
/*  517 */           "|" + 
/*  518 */           "RecordNumber:" + 
/*  519 */           recordNumber + 
/*  520 */           "|" + 
/*  521 */           "TotalNoOfTabs" + 
/*  522 */           "|" + 
/*  523 */           "This field length Cannot be greater than 8");
/*  524 */         errorCodes.add(errorObj);
/*      */       }
/*      */       else {
/*  527 */         int noOfTABS = transactionTBTSpecificBean
/*  528 */           .getTransactionAdviceBasicType().size();
/*      */         
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  551 */         if (Integer.parseInt(noOfTBTTABS) != noOfTABS) {
/*  552 */           errorObj = new ErrorObject(
/*  553 */             SubmissionErrorCodes.ERROR_DATAFIELD125_TOTAL_NOOFTABS
/*  554 */             .getErrorCode(), 
/*  555 */             SubmissionErrorCodes.ERROR_DATAFIELD125_TOTAL_NOOFTABS
/*  556 */             .getErrorDescription() + 
/*  557 */             "\n" + 
/*  558 */             "RecordType:" + 
/*  559 */             "TBT" + 
/*  560 */             "|" + 
/*  561 */             "RecordNumber:" + 
/*  562 */             recordNumber + 
/*  563 */             "|" + 
/*  564 */             "TotalNoOfTabs" + 
/*  565 */             "|" + 
/*  566 */             SubmissionErrorCodes.ERROR_DATAFIELD125_TOTAL_NOOFTABS
/*  567 */             .getErrorDescription());
/*  568 */           errorCodes.add(errorObj);
/*      */         }
/*      */       }
/*      */     } else {
/*  572 */       errorObj = new ErrorObject(
/*  573 */         SubmissionErrorCodes.ERROR_DATAFIELD124_TOTALNO_OFTABS
/*  574 */         .getErrorCode(), 
/*  575 */         SubmissionErrorCodes.ERROR_DATAFIELD124_TOTALNO_OFTABS
/*  576 */         .getErrorDescription() + 
/*  577 */         "\n" + 
/*  578 */         "RecordType:" + 
/*  579 */         "TBT" + 
/*  580 */         "|" + 
/*  581 */         "RecordNumber:" + 
/*  582 */         recordNumber + 
/*  583 */         "|" + 
/*  584 */         "TotalNoOfTabs" + 
/*  585 */         "|" + 
/*  586 */         "This field can only be Numeric");
/*  587 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTBTAmount(TransactionTBTSpecificBean transactionTBTSpecificBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  606 */     ErrorObject errorObj = null;
/*  607 */     long totalTABAmout = 0L;
/*  608 */     StringBuffer sign = new StringBuffer();
/*  609 */     String tbtAmt = transactionTBTSpecificBean
/*  610 */       .getTransactionBatchTrailerBean().getTbtAmount();
/*      */     
/*  612 */     if (CommonValidator.isNullOrEmpty(tbtAmt))
/*      */     {
/*  614 */       errorObj = new ErrorObject(
/*  615 */         SubmissionErrorCodes.ERROR_DATAFIELD595_TBT_AMT
/*  616 */         .getErrorCode(), 
/*  617 */         SubmissionErrorCodes.ERROR_DATAFIELD595_TBT_AMT
/*  618 */         .getErrorDescription() + 
/*  619 */         "\n" + 
/*  620 */         "RecordType:" + 
/*  621 */         "TBT" + 
/*  622 */         "|" + 
/*  623 */         "RecordNumber:" + 
/*  624 */         recordNumber + 
/*  625 */         "|" + 
/*  626 */         "TBTAmount" + 
/*  627 */         "|" + 
/*  628 */         "This field is mandatory and cannot be empty");
/*  629 */       errorCodes.add(errorObj);
/*      */     }
/*  631 */     else if (CommonValidator.validateData(tbtAmt, 
/*  632 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[118]))
/*      */     {
/*  634 */       String reqLength = CommonValidator.validateLength(tbtAmt, 20, 
/*  635 */         20);
/*      */       
/*  637 */       if (reqLength.equals("greaterThanMax")) {
/*  638 */         errorObj = new ErrorObject(
/*  639 */           SubmissionErrorCodes.ERROR_DATAFIELD129_TBT_AMOUNT
/*  640 */           .getErrorCode(), 
/*  641 */           SubmissionErrorCodes.ERROR_DATAFIELD129_TBT_AMOUNT
/*  642 */           .getErrorDescription() + 
/*  643 */           "\n" + 
/*  644 */           "RecordType:" + 
/*  645 */           "TBT" + 
/*  646 */           "|" + 
/*  647 */           "RecordNumber:" + 
/*  648 */           recordNumber + 
/*  649 */           "|" + 
/*  650 */           "TBTAmount" + 
/*  651 */           "|" + 
/*  652 */           "This field length Cannot be greater than 20");
/*  653 */         errorCodes.add(errorObj);
/*      */       } else {
/*  655 */         totalTABAmout = calculateTotalTABAmount(transactionTBTSpecificBean, sign);
/*  656 */         if (Long.parseLong(tbtAmt) != totalTABAmout)
/*      */         {
/*  658 */           errorObj = new ErrorObject(
/*  659 */             SubmissionErrorCodes.ERROR_DATAFIELD128_TBT_AMOUNT
/*  660 */             .getErrorCode(), 
/*  661 */             SubmissionErrorCodes.ERROR_DATAFIELD128_TBT_AMOUNT
/*  662 */             .getErrorDescription() + 
/*  663 */             "\n" + 
/*  664 */             "RecordType:" + 
/*  665 */             "TBT" + 
/*  666 */             "|" + 
/*  667 */             "RecordNumber:" + 
/*  668 */             recordNumber + 
/*  669 */             "|" + 
/*  670 */             "TBTAmount" + 
/*  671 */             "|" + 
/*  672 */             SubmissionErrorCodes.ERROR_DATAFIELD128_TBT_AMOUNT
/*  673 */             .getErrorDescription());
/*  674 */           errorCodes.add(errorObj);
/*      */         }
/*      */         
/*      */ 
/*  678 */         if (!CommonValidator.isValidAmount(
/*  679 */           transactionTBTSpecificBean
/*  680 */           .getTransactionBatchTrailerBean()
/*  681 */           .getTbtCurrencyCode(), tbtAmt))
/*      */         {
/*  683 */           errorObj = new ErrorObject(
/*  684 */             SubmissionErrorCodes.ERROR_DATAFIELD129_TBT_AMOUNT
/*  685 */             .getErrorCode(), 
/*  686 */             SubmissionErrorCodes.ERROR_DATAFIELD129_TBT_AMOUNT
/*  687 */             .getErrorDescription() + 
/*  688 */             "\n" + 
/*  689 */             "RecordType:" + 
/*  690 */             "TBT" + 
/*  691 */             "|" + 
/*  692 */             "RecordNumber:" + 
/*  693 */             recordNumber + 
/*  694 */             "|" + 
/*  695 */             "TBTAmount" + 
/*  696 */             "|" + 
/*  697 */             SubmissionErrorCodes.ERROR_DATAFIELD129_TBT_AMOUNT
/*  698 */             .getErrorDescription());
/*  699 */           errorCodes.add(errorObj);
/*      */         }
/*      */       }
/*      */     } else {
/*  703 */       errorObj = new ErrorObject(
/*  704 */         SubmissionErrorCodes.ERROR_DATAFIELD127_TBT_AMOUNT
/*  705 */         .getErrorCode(), 
/*  706 */         SubmissionErrorCodes.ERROR_DATAFIELD127_TBT_AMOUNT
/*  707 */         .getErrorDescription() + 
/*  708 */         "\n" + 
/*  709 */         "RecordType:" + 
/*  710 */         "TBT" + 
/*  711 */         "|" + 
/*  712 */         "RecordNumber:" + 
/*  713 */         recordNumber + 
/*  714 */         "|" + 
/*  715 */         "TBTAmount" + 
/*  716 */         "|" + 
/*  717 */         "This field can only be Numeric");
/*  718 */       errorCodes.add(errorObj);
/*      */     }
/*      */     
/*  721 */     validateTBTAmountSign(
/*  722 */       transactionTBTSpecificBean.getTransactionBatchTrailerBean(), sign.toString(), errorCodes);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTBTAmountSign(TransactionBatchTrailerBean transactionBatchTrailerBean, String sign, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  738 */     ErrorObject errorObj = null;
/*      */     
/*  740 */     if (CommonValidator.isNullOrEmpty(transactionBatchTrailerBean
/*  741 */       .getTbtAmountSign()))
/*      */     {
/*  743 */       errorObj = new ErrorObject(
/*  744 */         SubmissionErrorCodes.ERROR_DATAFIELD130_TBTAMOUNT_SIGN
/*  745 */         .getErrorCode(), 
/*  746 */         SubmissionErrorCodes.ERROR_DATAFIELD130_TBTAMOUNT_SIGN
/*  747 */         .getErrorDescription() + 
/*  748 */         "\n" + 
/*  749 */         "RecordType:" + 
/*  750 */         "TBT" + 
/*  751 */         "|" + 
/*  752 */         "RecordNumber:" + 
/*  753 */         recordNumber + 
/*  754 */         "|" + 
/*  755 */         "TbtAmountSign" + 
/*  756 */         "|" + 
/*  757 */         "This field is mandatory and cannot be empty");
/*  758 */       errorCodes.add(errorObj);
/*      */     }
/*  760 */     else if ((!CommonValidator.validateData(transactionBatchTrailerBean
/*  761 */       .getTbtAmountSign(), com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[119])) || 
/*  762 */       (!transactionBatchTrailerBean.getTbtAmountSign().equals(sign))) {
/*  763 */       errorObj = new ErrorObject(
/*  764 */         SubmissionErrorCodes.ERROR_DATAFIELD130_TBTAMOUNT_SIGN
/*  765 */         .getErrorCode(), 
/*  766 */         SubmissionErrorCodes.ERROR_DATAFIELD130_TBTAMOUNT_SIGN
/*  767 */         .getErrorDescription() + 
/*  768 */         "\n" + 
/*  769 */         "RecordType:" + 
/*  770 */         "TBT" + 
/*  771 */         "|" + 
/*  772 */         "RecordNumber:" + 
/*  773 */         recordNumber + 
/*  774 */         "|" + 
/*  775 */         "TbtAmountSign" + 
/*  776 */         "|" + 
/*  777 */         SubmissionErrorCodes.ERROR_DATAFIELD130_TBTAMOUNT_SIGN.getErrorDescription());
/*  778 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTBTCurrencyCode(TransactionTBTSpecificBean transactionTBTSpecificBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  796 */     ErrorObject errorObj = null;
/*  797 */     String tbtCurCode = transactionTBTSpecificBean
/*  798 */       .getTransactionBatchTrailerBean().getTbtCurrencyCode();
/*      */     
/*  800 */     if (CommonValidator.isNullOrEmpty(tbtCurCode))
/*      */     {
/*  802 */       errorObj = new ErrorObject(
/*  803 */         SubmissionErrorCodes.ERROR_DATAFIELD132_TBTCURRENCYCODE
/*  804 */         .getErrorCode(), 
/*  805 */         SubmissionErrorCodes.ERROR_DATAFIELD132_TBTCURRENCYCODE
/*  806 */         .getErrorDescription() + 
/*  807 */         "\n" + 
/*  808 */         "RecordType:" + 
/*  809 */         "TBT" + 
/*  810 */         "|" + 
/*  811 */         "RecordNumber:" + 
/*  812 */         recordNumber + 
/*  813 */         "|" + 
/*  814 */         "TbtCurrencyCode" + 
/*  815 */         "|" + 
/*  816 */         "This field is mandatory and cannot be empty");
/*  817 */       errorCodes.add(errorObj);
/*      */     }
/*  819 */     else if (CommonValidator.validateData(tbtCurCode, 
/*  820 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[120]))
/*      */     {
/*  822 */       String reqLength = CommonValidator.validateLength(tbtCurCode, 
/*  823 */         3, 3);
/*      */       
/*  825 */       if (reqLength.equals("greaterThanMax")) {
/*  826 */         errorObj = new ErrorObject(
/*  827 */           SubmissionErrorCodes.ERROR_DATAFIELD132_TBTCURRENCYCODE
/*  828 */           .getErrorCode(), 
/*  829 */           SubmissionErrorCodes.ERROR_DATAFIELD132_TBTCURRENCYCODE
/*  830 */           .getErrorDescription() + 
/*  831 */           "\n" + 
/*  832 */           "RecordType:" + 
/*  833 */           "TBT" + 
/*  834 */           "|" + 
/*  835 */           "RecordNumber:" + 
/*  836 */           recordNumber + 
/*  837 */           "|" + 
/*  838 */           "TbtCurrencyCode" + 
/*  839 */           "|" + 
/*  840 */           "This field length Cannot be greater than 3");
/*  841 */         errorCodes.add(errorObj);
/*      */       }
/*      */       else {
/*  844 */         if (!compareCurrencyCode(transactionTBTSpecificBean)) {
/*  845 */           errorObj = new ErrorObject(
/*  846 */             SubmissionErrorCodes.ERROR_DATAFIELD132_TBTCURRENCYCODE
/*  847 */             .getErrorCode(), 
/*  848 */             SubmissionErrorCodes.ERROR_DATAFIELD132_TBTCURRENCYCODE
/*  849 */             .getErrorDescription() + 
/*  850 */             "\n" + 
/*  851 */             "RecordType:" + 
/*  852 */             "TBT" + 
/*  853 */             "|" + 
/*  854 */             "RecordNumber:" + 
/*  855 */             recordNumber + 
/*  856 */             "|" + 
/*  857 */             "TbtCurrencyCode" + 
/*  858 */             "|" + 
/*  859 */             SubmissionErrorCodes.ERROR_DATAFIELD132_TBTCURRENCYCODE
/*  860 */             .getErrorDescription());
/*  861 */           errorCodes.add(errorObj);
/*      */         }
/*  863 */         if (CommonValidator.isProhibitedCurrencyCode(tbtCurCode))
/*      */         {
/*  865 */           errorObj = new ErrorObject(
/*  866 */             SubmissionErrorCodes.ERROR_DATAFIELD133_TBTCURRENCYCODE
/*  867 */             .getErrorCode(), 
/*  868 */             SubmissionErrorCodes.ERROR_DATAFIELD133_TBTCURRENCYCODE
/*  869 */             .getErrorDescription() + 
/*  870 */             "\n" + 
/*  871 */             "RecordType:" + 
/*  872 */             "TBT" + 
/*  873 */             "|" + 
/*  874 */             "RecordNumber:" + 
/*  875 */             recordNumber + 
/*  876 */             "|" + 
/*  877 */             "TransactionCurrencyCode" + 
/*  878 */             "|" + 
/*  879 */             SubmissionErrorCodes.ERROR_DATAFIELD47_CURRENCY_CODE
/*  880 */             .getErrorDescription());
/*  881 */           errorCodes.add(errorObj);
/*      */         }
/*      */       }
/*      */     } else {
/*  885 */       errorObj = new ErrorObject(
/*  886 */         SubmissionErrorCodes.ERROR_DATAFIELD132_TBTCURRENCYCODE
/*  887 */         .getErrorCode(), 
/*  888 */         SubmissionErrorCodes.ERROR_DATAFIELD132_TBTCURRENCYCODE
/*  889 */         .getErrorDescription() + 
/*  890 */         "\n" + 
/*  891 */         "RecordType:" + 
/*  892 */         "TBT" + 
/*  893 */         "|" + 
/*  894 */         "RecordNumber:" + 
/*  895 */         recordNumber + 
/*  896 */         "|" + 
/*  897 */         "TbtCurrencyCode" + 
/*  898 */         "|" + 
/*  899 */         "This field can only be AlphaNumeric");
/*  900 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTBTImageSequenceNumber(TransactionTBTSpecificBean transactionTBTSpecificBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  918 */     ErrorObject errorObj = null;
/*      */     
/*  920 */     String imageSeqNum = "";
/*      */     
/*  922 */     imageSeqNum = transactionTBTSpecificBean
/*  923 */       .getTransactionBatchTrailerBean().getTbtImageSequenceNumber();
/*  924 */     if (CommonValidator.isNullOrEmpty(imageSeqNum))
/*      */     {
/*      */ 
/*  927 */       if (!transactionTBTSpecificBean.getTransactionAdviceBasicType().isEmpty())
/*      */       {
/*  929 */         Iterator localIterator = transactionTBTSpecificBean.getTransactionAdviceBasicType().iterator();
/*  928 */         while (localIterator.hasNext()) {
/*  929 */           TransactionAdviceBasicBean tabBean = (TransactionAdviceBasicBean)localIterator.next();
/*      */           
/*  931 */           if (!CommonValidator.isNullOrEmpty(tabBean
/*  932 */             .getSubmissionMethod()))
/*      */           {
/*  934 */             if (SubmissionMethod.Paper_external_vendor_paper_processor.getSubmissionMethod().equals(
/*  935 */               tabBean.getSubmissionMethod())) {
/*  936 */               errorObj = new ErrorObject(
/*  937 */                 SubmissionErrorCodes.ERROR_DATAFIELD136_TBTIMAGE_SEQUENCENUMBER
/*  938 */                 .getErrorCode(), 
/*  939 */                 SubmissionErrorCodes.ERROR_DATAFIELD136_TBTIMAGE_SEQUENCENUMBER
/*  940 */                 .getErrorDescription() + 
/*  941 */                 "\n" + 
/*  942 */                 "RecordType:" + 
/*  943 */                 "TBT" + 
/*  944 */                 "|" + 
/*  945 */                 "RecordNumber:" + 
/*  946 */                 recordNumber);
/*  947 */               errorCodes.add(errorObj);
/*      */             }
/*      */           }
/*      */         }
/*      */       }
/*      */     }
/*  953 */     else if (CommonValidator.validateData(imageSeqNum, 
/*  954 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[124]))
/*      */     {
/*  956 */       String reqLength = CommonValidator.validateLength(imageSeqNum, 
/*  957 */         8, 8);
/*      */       
/*  959 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  961 */         errorObj = new ErrorObject(
/*  962 */           SubmissionErrorCodes.ERROR_DATAFIELD135_TBTIMAGE_SEQUENCENUMBER);
/*  963 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/*  966 */       errorObj = new ErrorObject(
/*  967 */         SubmissionErrorCodes.ERROR_DATAFIELD135_TBTIMAGE_SEQUENCENUMBER);
/*  968 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static boolean compareMerchantId(TransactionTBTSpecificBean transBean)
/*      */   {
/*  985 */     boolean isvalid = true;
/*      */     
/*      */ 
/*  988 */     Iterator localIterator = transBean.getTransactionAdviceBasicType().iterator();
/*  987 */     while (localIterator.hasNext()) {
/*  988 */       TransactionAdviceBasicBean transactionAdviceBasicType = (TransactionAdviceBasicBean)localIterator.next();
/*      */       
/*      */ 
/*  991 */       if (!transBean.getTransactionBatchTrailerBean().getMerchantId().equals(transactionAdviceBasicType.getMerchantId()))
/*      */       {
/*  993 */         isvalid = false;
/*      */       }
/*      */     }
/*  996 */     return isvalid;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static long calculateTotalTABAmount(TransactionTBTSpecificBean transactionTBTSpecificBean, StringBuffer sign)
/*      */   {
/* 1009 */     long totTABAmt = 0L;
/* 1010 */     long totDebitTABAmt = 0L;
/* 1011 */     long totCreditTABAmt = 0L;
/*      */     
/*      */ 
/* 1014 */     Iterator localIterator = transactionTBTSpecificBean.getTransactionAdviceBasicType().iterator();
/* 1013 */     while (localIterator.hasNext()) {
/* 1014 */       TransactionAdviceBasicBean transactionAdviceBasicType = (TransactionAdviceBasicBean)localIterator.next();
/*      */       
/* 1016 */       if ((transactionAdviceBasicType.getProcessingCode().equals(ProcessingCode.DEBIT.getProcessingCode())) || (transactionAdviceBasicType.getProcessingCode().equals(ProcessingCode.CREDIT_REVERSALS.getProcessingCode())) || 
/* 1017 */         (transactionAdviceBasicType.getProcessingCode().equals(ProcessingCode.BONUS_PAY_DEBIT.getProcessingCode())) || (transactionAdviceBasicType.getProcessingCode().equals(ProcessingCode.BONUS_PAY_CREDIT_REVERSAL.getProcessingCode())))
/*      */       {
/*      */ 
/* 1020 */         totDebitTABAmt = totDebitTABAmt + Long.parseLong(transactionAdviceBasicType.getTransactionAmount());
/* 1021 */       } else if ((transactionAdviceBasicType.getProcessingCode().equals(ProcessingCode.CREDIT.getProcessingCode())) || (transactionAdviceBasicType.getProcessingCode().equals(ProcessingCode.DEBIT_REVERSALS.getProcessingCode())) || 
/* 1022 */         (transactionAdviceBasicType.getProcessingCode().equals(ProcessingCode.BONUS_PAY_CREDIT.getProcessingCode())) || (transactionAdviceBasicType.getProcessingCode().equals(ProcessingCode.BONUS_PAY_DEBIT_REVERSAL.getProcessingCode())))
/*      */       {
/* 1024 */         totCreditTABAmt = totCreditTABAmt + Long.parseLong(transactionAdviceBasicType.getTransactionAmount());
/*      */       }
/*      */     }
/* 1027 */     if (totDebitTABAmt < totCreditTABAmt) {
/* 1028 */       totTABAmt = totCreditTABAmt - totDebitTABAmt;
/* 1029 */       sign.append("-");
/*      */     } else {
/* 1031 */       totTABAmt = totDebitTABAmt - totCreditTABAmt;
/* 1032 */       sign.append("+");
/*      */     }
/*      */     
/* 1035 */     return totTABAmt;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static boolean compareCurrencyCode(TransactionTBTSpecificBean transBean)
/*      */   {
/* 1050 */     boolean isvalid = true;
/*      */     
/*      */ 
/* 1053 */     Iterator localIterator = transBean.getTransactionAdviceBasicType().iterator();
/* 1052 */     while (localIterator.hasNext()) {
/* 1053 */       TransactionAdviceBasicBean transactionAdviceBasicType = (TransactionAdviceBasicBean)localIterator.next();
/*      */       
/*      */ 
/* 1056 */       if (!transBean.getTransactionBatchTrailerBean().getTbtCurrencyCode().equalsIgnoreCase(
/* 1057 */         transactionAdviceBasicType
/* 1058 */         .getTransactionCurrencyCode()))
/*      */       {
/* 1060 */         isvalid = false;
/*      */       }
/*      */     }
/* 1063 */     return isvalid;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static int compareNoOfTABS(TransactionTBTSpecificBean transBean)
/*      */   {
/* 1075 */     int duplicateTabs = 0;
/*      */     
/* 1077 */     String tabTransId = ((TransactionAdviceBasicBean)transBean.getTransactionAdviceBasicType().get(0))
/* 1078 */       .getTransactionIdentifier();
/*      */     
/*      */ 
/* 1081 */     Iterator localIterator = transBean.getTransactionAdviceBasicType().iterator();
/* 1080 */     while (localIterator.hasNext()) {
/* 1081 */       TransactionAdviceBasicBean transactionAdviceBasicType = (TransactionAdviceBasicBean)localIterator.next();
/*      */       
/* 1083 */       if (tabTransId.equals(
/* 1084 */         transactionAdviceBasicType.getTransactionIdentifier())) {
/* 1085 */         duplicateTabs++;
/*      */       }
/*      */     }
/* 1088 */     return duplicateTabs;
/*      */   }
/*      */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\validator\TBTRecValidator.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */