/*     */ package com.americanexpress.ips.gfsg.validator;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class CheckDigitValidator
/*     */ {
/*     */   public static boolean modulusNineCheck(String number)
/*     */   {
/*  20 */     boolean isValid = false;
/*  21 */     if (number.length() == 10)
/*     */     {
/*  23 */       int digits = number.length();
/*     */       
/*  25 */       long checkDigit = 0L;
/*     */       try {
/*  27 */         checkDigit = Integer.parseInt(number.charAt(digits - 1));
/*     */       }
/*     */       catch (NumberFormatException nfe) {
/*  30 */         return false;
/*     */       }
/*  32 */       long newSum = 0L;
/*  33 */       number = number.substring(0, digits - 1);
/*     */       
/*  35 */       int intfirstThreeDigits = 0;
/*     */       try {
/*  37 */         intfirstThreeDigits = Integer.parseInt(number.substring(0, 3));
/*     */       }
/*     */       catch (NumberFormatException nfe) {
/*  40 */         return false;
/*     */       }
/*  42 */       if ((intfirstThreeDigits < 930) || (intfirstThreeDigits > 939)) {
/*  43 */         String strDigits = number.substring(1, number.length());
/*  44 */         number = "0" + strDigits;
/*     */       }
/*  46 */       digits = number.length();
/*  47 */       long sum = 0L;
/*  48 */       long oddsum = 0L;
/*  49 */       for (int count = 0; count < digits; count++) {
/*  50 */         int digit = 0;
/*  51 */         if (count % 2 == 0) {
/*     */           try {
/*  53 */             digit = Integer.parseInt(number.charAt(count)) * 2;
/*     */           }
/*     */           catch (NumberFormatException nfe) {
/*  56 */             return false;
/*     */           }
/*  58 */           if (digit > 9) {
/*  59 */             digit -= 9;
/*     */           }
/*  61 */           sum += digit;
/*     */         } else {
/*     */           try {
/*  64 */             oddsum += Integer.parseInt(number.charAt(count));
/*     */           }
/*     */           catch (NumberFormatException nfe) {
/*  67 */             return false;
/*     */           }
/*     */         }
/*     */       }
/*  71 */       sum += oddsum;
/*  72 */       if ((sum % 10L == 0L) && (checkDigit == 0L)) {
/*  73 */         isValid = true;
/*     */       }
/*  75 */       if (sum % 10L != 0L) {
/*  76 */         newSum = sum + (10L - sum % 10L);
/*  77 */         if (newSum - sum == checkDigit) {
/*  78 */           isValid = true;
/*     */         }
/*     */       }
/*     */     }
/*  82 */     return isValid;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static boolean modulusTenCheck(String cardNumber)
/*     */   {
/*  94 */     boolean isValid = true;
/*  95 */     int digits = cardNumber.length();
/*  96 */     int oddOrEven = digits & 0x1;
/*  97 */     long sum = 0L;
/*  98 */     for (int count = 0; count < digits; count++) {
/*  99 */       int digit = 0;
/*     */       try {
/* 101 */         digit = Integer.parseInt(cardNumber.charAt(count));
/*     */       } catch (NumberFormatException e) {
/* 103 */         isValid = false;
/* 104 */         break;
/*     */       }
/*     */       
/* 107 */       if ((count & 0x1 ^ oddOrEven) == 0) {
/* 108 */         digit *= 2;
/* 109 */         if (digit > 9) {
/* 110 */           digit -= 9;
/*     */         }
/*     */       }
/* 113 */       sum += digit;
/*     */     }
/* 115 */     if (isValid) {
/* 116 */       isValid = sum != 0L;
/*     */     }
/* 118 */     return isValid;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\validator\CheckDigitValidator.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */