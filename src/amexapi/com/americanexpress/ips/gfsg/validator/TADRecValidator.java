/*      */ package com.americanexpress.ips.gfsg.validator;
/*      */ 
/*      */ import com.americanexpress.ips.gfsg.beans.ErrorObject;
/*      */ import com.americanexpress.ips.gfsg.beans.common.TransactionAdviceBasicBean;
/*      */ import com.americanexpress.ips.gfsg.beans.common.TransactionAdviceDetailBean;
/*      */ import com.americanexpress.ips.gfsg.enumerations.RecordType;
/*      */ import com.americanexpress.ips.gfsg.enumerations.SubmissionErrorCodes;
/*      */ import com.americanexpress.ips.gfsg.exceptions.SettlementException;
/*      */ import java.util.List;
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ public class TADRecValidator
/*      */ {
/*      */   private static String recordNumber;
/*      */   private static String recordType;
/*      */   
/*      */   public static void validateTADRecord(TransactionAdviceBasicBean transactionAdviceBasicType, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*   41 */     TransactionAdviceDetailBean transactionAdviceDetailBean = transactionAdviceBasicType
/*   42 */       .getTransactionAdviceDetailBean();
/*   43 */     recordNumber = transactionAdviceDetailBean.getRecordNumber();
/*   44 */     recordType = transactionAdviceDetailBean.getRecordType();
/*   45 */     validateRecordType(transactionAdviceDetailBean.getRecordType(), 
/*   46 */       errorCodes);
/*   47 */     validateRecordNumber(transactionAdviceDetailBean.getRecordNumber(), 
/*   48 */       errorCodes);
/*   49 */     validateTransactionIdentifier(transactionAdviceDetailBean
/*   50 */       .getTransactionIdentifier(), transactionAdviceBasicType, 
/*   51 */       errorCodes);
/*   52 */     validateAdditionalAmountType1(transactionAdviceDetailBean, errorCodes);
/*   53 */     validateAdditionalAmount1(transactionAdviceDetailBean, errorCodes);
/*   54 */     validateAdditionalAmountSign1(transactionAdviceDetailBean
/*   55 */       .getAdditionalAmountSign1(), errorCodes);
/*   56 */     validateAdditionalAmountType2(transactionAdviceDetailBean, errorCodes);
/*   57 */     validateAdditionalAmount2(transactionAdviceDetailBean, errorCodes);
/*   58 */     validateAdditionalAmountSign2(transactionAdviceDetailBean
/*   59 */       .getAdditionalAmountSign2(), errorCodes);
/*   60 */     validateAdditionalAmountType3(transactionAdviceDetailBean, errorCodes);
/*   61 */     validateAdditionalAmount3(transactionAdviceDetailBean, errorCodes);
/*   62 */     validateAdditionalAmountSign3(transactionAdviceDetailBean
/*   63 */       .getAdditionalAmountSign3(), errorCodes);
/*   64 */     validateAdditionalAmountType4(transactionAdviceDetailBean, errorCodes);
/*   65 */     validateAdditionalAmount4(transactionAdviceDetailBean, errorCodes);
/*   66 */     validateAdditionalAmountSign4(transactionAdviceDetailBean
/*   67 */       .getAdditionalAmountSign4(), errorCodes);
/*   68 */     validateAdditionalAmountType5(transactionAdviceDetailBean, errorCodes);
/*   69 */     validateAdditionalAmount5(transactionAdviceDetailBean, errorCodes);
/*   70 */     validateAdditionalAmountSign5(transactionAdviceDetailBean
/*   71 */       .getAdditionalAmountSign5(), errorCodes);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateRecordType(String recordType, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*   87 */     ErrorObject errorObj = null;
/*   88 */     if (CommonValidator.isNullOrEmpty(recordType))
/*      */     {
/*   90 */       errorObj = new ErrorObject(
/*   91 */         SubmissionErrorCodes.ERROR_DATAFIELD1_RECORDTYPE
/*   92 */         .getErrorCode(), 
/*   93 */         SubmissionErrorCodes.ERROR_DATAFIELD1_RECORDTYPE
/*   94 */         .getErrorDescription() + 
/*   95 */         "\n" + 
/*   96 */         "TFH" + 
/*   97 */         "|" + 
/*   98 */         "RecordNumber:" + 
/*   99 */         recordNumber + 
/*  100 */         "|" + 
/*  101 */         "RecordType:" + 
/*  102 */         "|" + 
/*  103 */         "This field is mandatory and cannot be empty");
/*  104 */       errorCodes.add(errorObj);
/*      */     }
/*  106 */     else if (!RecordType.Transaction_Advice_Detail.getRecordType().equalsIgnoreCase(recordType))
/*      */     {
/*      */ 
/*  109 */       errorObj = new ErrorObject(
/*  110 */         SubmissionErrorCodes.ERROR_DATAFIELD1_RECORDTYPE);
/*  111 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateRecordNumber(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  128 */     ErrorObject errorObj = null;
/*      */     
/*  130 */     if (CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  132 */       errorObj = new ErrorObject(
/*  133 */         SubmissionErrorCodes.ERROR_DATAFIELD585_RECORDNUMBER
/*  134 */         .getErrorCode(), 
/*  135 */         SubmissionErrorCodes.ERROR_DATAFIELD585_RECORDNUMBER
/*  136 */         .getErrorDescription() + 
/*  137 */         "\n" + 
/*  138 */         "RecordType:" + 
/*  139 */         recordType + 
/*  140 */         "|" + 
/*  141 */         "RecordNumber:" + 
/*  142 */         recordNumber + 
/*  143 */         "|" + 
/*  144 */         "RecordNumber:" + 
/*  145 */         "|" + 
/*  146 */         "This field is mandatory and cannot be empty");
/*  147 */       errorCodes.add(errorObj);
/*      */     }
/*  149 */     else if (CommonValidator.validateData(value, 
/*  150 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[88]))
/*      */     {
/*  152 */       String reqLength = CommonValidator.validateLength(value, 8, 8);
/*      */       
/*  154 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  156 */         errorObj = new ErrorObject(
/*  157 */           SubmissionErrorCodes.ERROR_DATAFIELD3_RECORDNUMBER
/*  158 */           .getErrorCode(), 
/*  159 */           SubmissionErrorCodes.ERROR_DATAFIELD3_RECORDNUMBER
/*  160 */           .getErrorDescription() + 
/*  161 */           "\n" + 
/*  162 */           "RecordType:" + 
/*  163 */           recordType + 
/*  164 */           "|" + 
/*  165 */           "RecordNumber:" + 
/*  166 */           recordNumber + 
/*  167 */           "|" + 
/*  168 */           "RecordNumber:" + 
/*  169 */           "|" + 
/*  170 */           "This field length Cannot be greater than 8");
/*  171 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/*  174 */       errorObj = new ErrorObject(
/*  175 */         SubmissionErrorCodes.ERROR_DATAFIELD2_RECORDNUMBER
/*  176 */         .getErrorCode(), 
/*  177 */         SubmissionErrorCodes.ERROR_DATAFIELD2_RECORDNUMBER
/*  178 */         .getErrorDescription() + 
/*  179 */         "\n" + 
/*  180 */         "RecordType:" + 
/*  181 */         recordType + 
/*  182 */         "|" + 
/*  183 */         "RecordNumber:" + 
/*  184 */         recordNumber + 
/*  185 */         "|" + 
/*  186 */         "RecordNumber:" + 
/*  187 */         "|" + 
/*  188 */         "This field can only be Numeric");
/*  189 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTransactionIdentifier(String value, TransactionAdviceBasicBean transactionAdviceBasicType, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  208 */     ErrorObject errorObj = null;
/*      */     
/*  210 */     if (CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  212 */       errorObj = new ErrorObject(
/*  213 */         SubmissionErrorCodes.ERROR_DATAFIELD25_TRANSACTION_IDENTIFIER
/*  214 */         .getErrorCode(), 
/*  215 */         SubmissionErrorCodes.ERROR_DATAFIELD25_TRANSACTION_IDENTIFIER
/*  216 */         .getErrorDescription() + 
/*  217 */         "\n" + 
/*  218 */         "RecordType:" + 
/*  219 */         recordType + 
/*  220 */         "|" + 
/*  221 */         "RecordNumber:" + 
/*  222 */         recordNumber + 
/*  223 */         "|" + 
/*  224 */         "TransactionIdentifier" + 
/*  225 */         "|" + 
/*  226 */         "This field is mandatory and cannot be empty");
/*  227 */       errorCodes.add(errorObj);
/*      */     }
/*  229 */     else if (CommonValidator.validateData(value, 
/*  230 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[89]))
/*      */     {
/*  232 */       String reqLength = 
/*  233 */         CommonValidator.validateLength(value, 15, 15);
/*      */       
/*  235 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  237 */         errorObj = new ErrorObject(
/*  238 */           SubmissionErrorCodes.ERROR_DATAFIELD25_TRANSACTION_IDENTIFIER
/*  239 */           .getErrorCode(), 
/*  240 */           SubmissionErrorCodes.ERROR_DATAFIELD25_TRANSACTION_IDENTIFIER
/*  241 */           .getErrorDescription() + 
/*  242 */           "\n" + 
/*  243 */           "RecordType:" + 
/*  244 */           recordType + 
/*  245 */           "|" + 
/*  246 */           "RecordNumber:" + 
/*  247 */           recordNumber + 
/*  248 */           "|" + 
/*  249 */           "TransactionIdentifier" + 
/*  250 */           "|" + 
/*  251 */           "This field length Cannot be greater than 15");
/*  252 */         errorCodes.add(errorObj);
/*      */       }
/*  254 */       else if (!value.equalsIgnoreCase(
/*  255 */         transactionAdviceBasicType.getTransactionIdentifier()))
/*      */       {
/*  257 */         errorObj = new ErrorObject(
/*  258 */           SubmissionErrorCodes.ERROR_DATAFIELD67_TRANSACTION_IDENTIFIER
/*  259 */           .getErrorCode(), 
/*  260 */           SubmissionErrorCodes.ERROR_DATAFIELD67_TRANSACTION_IDENTIFIER
/*  261 */           .getErrorDescription() + 
/*  262 */           "\n" + 
/*  263 */           "RecordType:" + 
/*  264 */           recordType + 
/*  265 */           "|" + 
/*  266 */           "RecordNumber:" + 
/*  267 */           recordNumber + 
/*  268 */           "|" + 
/*  269 */           "TransactionIdentifier" + 
/*  270 */           "|" + 
/*  271 */           SubmissionErrorCodes.ERROR_DATAFIELD67_TRANSACTION_IDENTIFIER
/*  272 */           .getErrorDescription());
/*  273 */         errorCodes.add(errorObj);
/*      */       }
/*      */       
/*      */ 
/*      */     }
/*      */     else
/*      */     {
/*  280 */       errorObj = new ErrorObject(
/*  281 */         SubmissionErrorCodes.ERROR_DATAFIELD25_TRANSACTION_IDENTIFIER
/*  282 */         .getErrorCode(), 
/*  283 */         SubmissionErrorCodes.ERROR_DATAFIELD25_TRANSACTION_IDENTIFIER
/*  284 */         .getErrorDescription() + 
/*  285 */         "\n" + 
/*  286 */         "RecordType:" + 
/*  287 */         recordType + 
/*  288 */         "|" + 
/*  289 */         "RecordNumber:" + 
/*  290 */         recordNumber + 
/*  291 */         "|" + 
/*  292 */         "TransactionIdentifier" + 
/*  293 */         "|" + 
/*  294 */         "This field can only be Numeric");
/*  295 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateAdditionalAmountType1(TransactionAdviceDetailBean transactionAdviceDetailBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  313 */     ErrorObject errorObj = null;
/*      */     
/*  315 */     if (!CommonValidator.isNullOrEmpty(transactionAdviceDetailBean
/*  316 */       .getAdditionalAmountType1()))
/*      */     {
/*  318 */       if (CommonValidator.validateData(transactionAdviceDetailBean
/*  319 */         .getAdditionalAmountType1(), 
/*  320 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[90]))
/*      */       {
/*  322 */         String reqLength = CommonValidator.validateLength(
/*  323 */           transactionAdviceDetailBean.getAdditionalAmountType1(), 
/*  324 */           3, 3);
/*      */         
/*  326 */         if (reqLength.equals("greaterThanMax"))
/*      */         {
/*  328 */           errorObj = new ErrorObject(
/*  329 */             SubmissionErrorCodes.ERROR_DATAFIELD86_ADDITIONAL_AMOUNTTYPE1
/*  330 */             .getErrorCode(), 
/*  331 */             SubmissionErrorCodes.ERROR_DATAFIELD86_ADDITIONAL_AMOUNTTYPE1
/*  332 */             .getErrorDescription() + 
/*  333 */             "\n" + 
/*  334 */             "RecordType:" + 
/*  335 */             recordType + 
/*  336 */             "|" + 
/*  337 */             "RecordNumber:" + 
/*  338 */             recordNumber + 
/*  339 */             "|" + 
/*  340 */             "AdditionalAmountType1" + 
/*  341 */             "|" + 
/*  342 */             "This field length Cannot be greater than 3");
/*  343 */           errorCodes.add(errorObj);
/*      */ 
/*      */         }
/*  346 */         else if (CommonValidator.isNullOrEmpty(transactionAdviceDetailBean
/*  347 */           .getAdditionalAmount1())) {
/*  348 */           errorObj = new ErrorObject(
/*  349 */             SubmissionErrorCodes.ERROR_DATAFIELD89_ADDITIONAL_AMOUNT1
/*  350 */             .getErrorCode(), 
/*  351 */             SubmissionErrorCodes.ERROR_DATAFIELD89_ADDITIONAL_AMOUNT1
/*  352 */             .getErrorDescription() + 
/*  353 */             "\n" + 
/*  354 */             "RecordType:" + 
/*  355 */             recordType + 
/*  356 */             "|" + 
/*  357 */             "RecordNumber:" + 
/*  358 */             recordNumber + 
/*  359 */             "|" + 
/*  360 */             "AdditionalAmountType1" + 
/*  361 */             "|" + 
/*  362 */             SubmissionErrorCodes.ERROR_DATAFIELD89_ADDITIONAL_AMOUNT1
/*  363 */             .getErrorDescription());
/*  364 */           errorCodes.add(errorObj);
/*      */         }
/*      */       }
/*      */       else
/*      */       {
/*  369 */         errorObj = new ErrorObject(
/*  370 */           SubmissionErrorCodes.ERROR_DATAFIELD86_ADDITIONAL_AMOUNTTYPE1
/*  371 */           .getErrorCode(), 
/*  372 */           SubmissionErrorCodes.ERROR_DATAFIELD86_ADDITIONAL_AMOUNTTYPE1
/*  373 */           .getErrorDescription() + 
/*  374 */           "\n" + 
/*  375 */           "RecordType:" + 
/*  376 */           recordType + 
/*  377 */           "|" + 
/*  378 */           "RecordNumber:" + 
/*  379 */           recordNumber + 
/*  380 */           "|" + 
/*  381 */           "AdditionalAmountType1" + 
/*  382 */           "|" + 
/*  383 */           "This field can only be AlphaNumeric");
/*  384 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateAdditionalAmount1(TransactionAdviceDetailBean transactionAdviceDetailBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  402 */     ErrorObject errorObj = null;
/*      */     
/*  404 */     if (!CommonValidator.isNullOrEmpty(transactionAdviceDetailBean
/*  405 */       .getAdditionalAmount1()))
/*      */     {
/*  407 */       if (CommonValidator.validateData(transactionAdviceDetailBean
/*  408 */         .getAdditionalAmount1(), 
/*  409 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[92]))
/*      */       {
/*  411 */         String reqLength = CommonValidator.validateLength(
/*  412 */           transactionAdviceDetailBean.getAdditionalAmount1(), 12, 
/*  413 */           12);
/*      */         
/*  415 */         if (reqLength.equals("greaterThanMax")) {
/*  416 */           errorObj = new ErrorObject(
/*  417 */             SubmissionErrorCodes.ERROR_DATAFIELD90_ADDITIONAL_AMOUNT1
/*  418 */             .getErrorCode(), 
/*  419 */             SubmissionErrorCodes.ERROR_DATAFIELD90_ADDITIONAL_AMOUNT1
/*  420 */             .getErrorDescription() + 
/*  421 */             "\n" + 
/*  422 */             "RecordType:" + 
/*  423 */             recordType + 
/*  424 */             "|" + 
/*  425 */             "RecordNumber:" + 
/*  426 */             recordNumber + 
/*  427 */             "|" + 
/*  428 */             "AdditionalAmount1" + 
/*  429 */             "|" + 
/*  430 */             "This field length Cannot be greater than 12");
/*  431 */           errorCodes.add(errorObj);
/*      */ 
/*      */ 
/*      */         }
/*  435 */         else if (CommonValidator.isNullOrEmpty(transactionAdviceDetailBean
/*  436 */           .getAdditionalAmountType1())) {
/*  437 */           errorObj = new ErrorObject(
/*  438 */             SubmissionErrorCodes.ERROR_DATAFIELD87_ADDITIONAL_AMOUNTTYPE1
/*  439 */             .getErrorCode(), 
/*  440 */             SubmissionErrorCodes.ERROR_DATAFIELD87_ADDITIONAL_AMOUNTTYPE1
/*  441 */             .getErrorDescription() + 
/*  442 */             "\n" + 
/*  443 */             "RecordType:" + 
/*  444 */             recordType + 
/*  445 */             "|" + 
/*  446 */             "RecordNumber:" + 
/*  447 */             recordNumber + 
/*  448 */             "|" + 
/*  449 */             "AdditionalAmount1" + 
/*  450 */             "|" + 
/*  451 */             SubmissionErrorCodes.ERROR_DATAFIELD87_ADDITIONAL_AMOUNTTYPE1
/*  452 */             .getErrorDescription());
/*  453 */           errorCodes.add(errorObj);
/*      */         }
/*      */       }
/*      */       else
/*      */       {
/*  458 */         errorObj = new ErrorObject(
/*  459 */           SubmissionErrorCodes.ERROR_DATAFIELD88_ADDITIONAL_AMOUNT1
/*  460 */           .getErrorCode(), 
/*  461 */           SubmissionErrorCodes.ERROR_DATAFIELD88_ADDITIONAL_AMOUNT1
/*  462 */           .getErrorDescription() + 
/*  463 */           "\n" + 
/*  464 */           "RecordType:" + 
/*  465 */           recordType + 
/*  466 */           "|" + 
/*  467 */           "RecordNumber:" + 
/*  468 */           recordNumber + 
/*  469 */           "|" + 
/*  470 */           "AdditionalAmount1" + 
/*  471 */           "|" + 
/*  472 */           "This field can only be Numeric");
/*  473 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateAdditionalAmountSign1(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  490 */     ErrorObject errorObj = null;
/*      */     
/*  492 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  494 */       if (CommonValidator.validateData(value, 
/*  495 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[93]))
/*      */       {
/*      */ 
/*  498 */         String reqLength = CommonValidator.validateLength(value, 1, 1);
/*      */         
/*  500 */         if (reqLength.equals("greaterThanMax")) {
/*  501 */           errorObj = new ErrorObject(
/*  502 */             SubmissionErrorCodes.ERROR_DATAFIELD91_ADDITIONAL_AMOUNT_SIGN1
/*  503 */             .getErrorCode(), 
/*  504 */             SubmissionErrorCodes.ERROR_DATAFIELD91_ADDITIONAL_AMOUNT_SIGN1
/*  505 */             .getErrorDescription() + 
/*  506 */             "\n" + 
/*  507 */             "RecordType:" + 
/*  508 */             recordType + 
/*  509 */             "|" + 
/*  510 */             "RecordNumber:" + 
/*  511 */             recordNumber + 
/*  512 */             "|" + 
/*  513 */             "AdditionalAmountSign1" + 
/*  514 */             "|" + 
/*  515 */             "This field length Cannot be greater than 1");
/*  516 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/*  519 */         errorObj = new ErrorObject(
/*  520 */           SubmissionErrorCodes.ERROR_DATAFIELD91_ADDITIONAL_AMOUNT_SIGN1
/*  521 */           .getErrorCode(), 
/*  522 */           SubmissionErrorCodes.ERROR_DATAFIELD91_ADDITIONAL_AMOUNT_SIGN1
/*  523 */           .getErrorDescription() + 
/*  524 */           "\n" + 
/*  525 */           "RecordType:" + 
/*  526 */           recordType + 
/*  527 */           "|" + 
/*  528 */           "RecordNumber:" + 
/*  529 */           recordNumber + 
/*  530 */           "|" + 
/*  531 */           "AdditionalAmountSign1" + 
/*  532 */           "|" + 
/*  533 */           "This field can only be AlphaNumeric");
/*  534 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateAdditionalAmountType2(TransactionAdviceDetailBean transactionAdviceDetailBean, List<ErrorObject> errorCodes)
/*      */   {
/*  552 */     ErrorObject errorObj = null;
/*      */     
/*  554 */     if (!CommonValidator.isNullOrEmpty(transactionAdviceDetailBean
/*  555 */       .getAdditionalAmountType2()))
/*      */     {
/*  557 */       if (CommonValidator.validateData(transactionAdviceDetailBean
/*  558 */         .getAdditionalAmountType2(), 
/*  559 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[94]))
/*      */       {
/*  561 */         String reqLength = CommonValidator.validateLength(
/*  562 */           transactionAdviceDetailBean.getAdditionalAmountType2(), 
/*  563 */           3, 3);
/*      */         
/*  565 */         if (reqLength.equals("greaterThanMax")) {
/*  566 */           errorObj = new ErrorObject(
/*  567 */             SubmissionErrorCodes.ERROR_DATAFIELD92_ADDITIONAL_AMOUNTTYPE2
/*  568 */             .getErrorCode(), 
/*  569 */             SubmissionErrorCodes.ERROR_DATAFIELD92_ADDITIONAL_AMOUNTTYPE2
/*  570 */             .getErrorDescription() + 
/*  571 */             "\n" + 
/*  572 */             "RecordType:" + 
/*  573 */             recordType + 
/*  574 */             "|" + 
/*  575 */             "RecordNumber:" + 
/*  576 */             recordNumber + 
/*  577 */             "|" + 
/*  578 */             "AdditionalAmountType2" + 
/*  579 */             "|" + 
/*  580 */             "This field length Cannot be greater than 3");
/*  581 */           errorCodes.add(errorObj);
/*      */ 
/*      */ 
/*      */         }
/*  585 */         else if (CommonValidator.isNullOrEmpty(transactionAdviceDetailBean
/*  586 */           .getAdditionalAmount2())) {
/*  587 */           errorObj = new ErrorObject(
/*  588 */             SubmissionErrorCodes.ERROR_DATAFIELD95_ADDITIONAL_AMOUNT2
/*  589 */             .getErrorCode(), 
/*  590 */             SubmissionErrorCodes.ERROR_DATAFIELD95_ADDITIONAL_AMOUNT2
/*  591 */             .getErrorDescription() + 
/*  592 */             "\n" + 
/*  593 */             "RecordType:" + 
/*  594 */             recordType + 
/*  595 */             "|" + 
/*  596 */             "RecordNumber:" + 
/*  597 */             recordNumber + 
/*  598 */             "|" + 
/*  599 */             "AdditionalAmountType2" + 
/*  600 */             "|" + 
/*  601 */             SubmissionErrorCodes.ERROR_DATAFIELD95_ADDITIONAL_AMOUNT2
/*  602 */             .getErrorDescription());
/*  603 */           errorCodes.add(errorObj);
/*      */         }
/*      */       }
/*      */       else {
/*  607 */         errorObj = new ErrorObject(
/*  608 */           SubmissionErrorCodes.ERROR_DATAFIELD92_ADDITIONAL_AMOUNTTYPE2
/*  609 */           .getErrorCode(), 
/*  610 */           SubmissionErrorCodes.ERROR_DATAFIELD92_ADDITIONAL_AMOUNTTYPE2
/*  611 */           .getErrorDescription() + 
/*  612 */           "\n" + 
/*  613 */           "RecordType:" + 
/*  614 */           recordType + 
/*  615 */           "|" + 
/*  616 */           "RecordNumber:" + 
/*  617 */           recordNumber + 
/*  618 */           "|" + 
/*  619 */           "AdditionalAmountType2" + 
/*  620 */           "|" + 
/*  621 */           "This field can only be AlphaNumeric");
/*  622 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateAdditionalAmount2(TransactionAdviceDetailBean transactionAdviceDetailBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  640 */     ErrorObject errorObj = null;
/*      */     
/*  642 */     if (!CommonValidator.isNullOrEmpty(transactionAdviceDetailBean
/*  643 */       .getAdditionalAmount2()))
/*      */     {
/*  645 */       if (CommonValidator.validateData(transactionAdviceDetailBean
/*  646 */         .getAdditionalAmount2(), 
/*  647 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[96]))
/*      */       {
/*  649 */         String reqLength = CommonValidator.validateLength(
/*  650 */           transactionAdviceDetailBean.getAdditionalAmount2(), 12, 
/*  651 */           12);
/*      */         
/*  653 */         if (reqLength.equals("greaterThanMax")) {
/*  654 */           errorObj = new ErrorObject(
/*  655 */             SubmissionErrorCodes.ERROR_DATAFIELD96_ADDITIONAL_AMOUNT2
/*  656 */             .getErrorCode(), 
/*  657 */             SubmissionErrorCodes.ERROR_DATAFIELD96_ADDITIONAL_AMOUNT2
/*  658 */             .getErrorDescription() + 
/*  659 */             "\n" + 
/*  660 */             "RecordType:" + 
/*  661 */             recordType + 
/*  662 */             "|" + 
/*  663 */             "RecordNumber:" + 
/*  664 */             recordNumber + 
/*  665 */             "|" + 
/*  666 */             "AdditionalAmount2" + 
/*  667 */             "|" + 
/*  668 */             "This field length Cannot be greater than 12");
/*  669 */           errorCodes.add(errorObj);
/*      */ 
/*      */ 
/*      */         }
/*  673 */         else if (CommonValidator.isNullOrEmpty(transactionAdviceDetailBean
/*  674 */           .getAdditionalAmountType2())) {
/*  675 */           errorObj = new ErrorObject(
/*  676 */             SubmissionErrorCodes.ERROR_DATAFIELD93_ADDITIONAL_AMOUNTTYPE2
/*  677 */             .getErrorCode(), 
/*  678 */             SubmissionErrorCodes.ERROR_DATAFIELD93_ADDITIONAL_AMOUNTTYPE2
/*  679 */             .getErrorDescription() + 
/*  680 */             "\n" + 
/*  681 */             "RecordType:" + 
/*  682 */             recordType + 
/*  683 */             "|" + 
/*  684 */             "RecordNumber:" + 
/*  685 */             recordNumber + 
/*  686 */             "|" + 
/*  687 */             "AdditionalAmount2" + 
/*  688 */             "|" + 
/*  689 */             SubmissionErrorCodes.ERROR_DATAFIELD93_ADDITIONAL_AMOUNTTYPE2
/*  690 */             .getErrorDescription());
/*  691 */           errorCodes.add(errorObj);
/*      */         }
/*      */       }
/*      */       else {
/*  695 */         errorObj = new ErrorObject(
/*  696 */           SubmissionErrorCodes.ERROR_DATAFIELD94_ADDITIONAL_AMOUNT2
/*  697 */           .getErrorCode(), 
/*  698 */           SubmissionErrorCodes.ERROR_DATAFIELD94_ADDITIONAL_AMOUNT2
/*  699 */           .getErrorDescription() + 
/*  700 */           "\n" + 
/*  701 */           "RecordType:" + 
/*  702 */           recordType + 
/*  703 */           "|" + 
/*  704 */           "RecordNumber:" + 
/*  705 */           recordNumber + 
/*  706 */           "|" + 
/*  707 */           "AdditionalAmount2" + 
/*  708 */           "|" + 
/*  709 */           "This field can only be Numeric");
/*  710 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateAdditionalAmountSign2(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  727 */     ErrorObject errorObj = null;
/*      */     
/*  729 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  731 */       if (CommonValidator.validateData(value, 
/*  732 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[97]))
/*      */       {
/*      */ 
/*  735 */         String reqLength = CommonValidator.validateLength(value, 1, 1);
/*      */         
/*  737 */         if (reqLength.equals("greaterThanMax")) {
/*  738 */           errorObj = new ErrorObject(
/*  739 */             SubmissionErrorCodes.ERROR_DATAFIELD97_ADDITIONAL_AMOUNT_SIGN2
/*  740 */             .getErrorCode(), 
/*  741 */             SubmissionErrorCodes.ERROR_DATAFIELD97_ADDITIONAL_AMOUNT_SIGN2
/*  742 */             .getErrorDescription() + 
/*  743 */             "\n" + 
/*  744 */             "RecordType:" + 
/*  745 */             recordType + 
/*  746 */             "|" + 
/*  747 */             "RecordNumber:" + 
/*  748 */             recordNumber + 
/*  749 */             "|" + 
/*  750 */             "AdditionalAmountSign2" + 
/*  751 */             "|" + 
/*  752 */             "This field length Cannot be greater than 1");
/*  753 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/*  756 */         errorObj = new ErrorObject(
/*  757 */           SubmissionErrorCodes.ERROR_DATAFIELD97_ADDITIONAL_AMOUNT_SIGN2
/*  758 */           .getErrorCode(), 
/*  759 */           SubmissionErrorCodes.ERROR_DATAFIELD97_ADDITIONAL_AMOUNT_SIGN2
/*  760 */           .getErrorDescription() + 
/*  761 */           "\n" + 
/*  762 */           "RecordType:" + 
/*  763 */           recordType + 
/*  764 */           "|" + 
/*  765 */           "RecordNumber:" + 
/*  766 */           recordNumber + 
/*  767 */           "|" + 
/*  768 */           "AdditionalAmountSign2" + 
/*  769 */           "|" + 
/*  770 */           "This field can only be AlphaNumeric");
/*  771 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateAdditionalAmountType3(TransactionAdviceDetailBean transactionAdviceDetailBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  789 */     ErrorObject errorObj = null;
/*      */     
/*  791 */     if (!CommonValidator.isNullOrEmpty(transactionAdviceDetailBean
/*  792 */       .getAdditionalAmountType3()))
/*      */     {
/*  794 */       if (CommonValidator.validateData(transactionAdviceDetailBean
/*  795 */         .getAdditionalAmountType3(), 
/*  796 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[98]))
/*      */       {
/*  798 */         String reqLength = CommonValidator.validateLength(
/*  799 */           transactionAdviceDetailBean.getAdditionalAmountType3(), 
/*  800 */           3, 3);
/*      */         
/*  802 */         if (reqLength.equals("greaterThanMax")) {
/*  803 */           errorObj = new ErrorObject(
/*  804 */             SubmissionErrorCodes.ERROR_DATAFIELD98_ADDITIONAL_AMOUNTTYPE3
/*  805 */             .getErrorCode(), 
/*  806 */             SubmissionErrorCodes.ERROR_DATAFIELD98_ADDITIONAL_AMOUNTTYPE3
/*  807 */             .getErrorDescription() + 
/*  808 */             "\n" + 
/*  809 */             "RecordType:" + 
/*  810 */             recordType + 
/*  811 */             "|" + 
/*  812 */             "RecordNumber:" + 
/*  813 */             recordNumber + 
/*  814 */             "|" + 
/*  815 */             "AdditionalAmountType3" + 
/*  816 */             "|" + 
/*  817 */             "This field length Cannot be greater than 3");
/*  818 */           errorCodes.add(errorObj);
/*      */ 
/*      */ 
/*      */         }
/*  822 */         else if (CommonValidator.isNullOrEmpty(transactionAdviceDetailBean
/*  823 */           .getAdditionalAmount3())) {
/*  824 */           errorObj = new ErrorObject(
/*  825 */             SubmissionErrorCodes.ERROR_DATAFIELD101_ADDITIONAL_AMOUNT3
/*  826 */             .getErrorCode(), 
/*  827 */             SubmissionErrorCodes.ERROR_DATAFIELD101_ADDITIONAL_AMOUNT3
/*  828 */             .getErrorDescription() + 
/*  829 */             "\n" + 
/*  830 */             "RecordType:" + 
/*  831 */             recordType + 
/*  832 */             "|" + 
/*  833 */             "RecordNumber:" + 
/*  834 */             recordNumber + 
/*  835 */             "|" + 
/*  836 */             "AdditionalAmountType3" + 
/*  837 */             "|" + 
/*  838 */             SubmissionErrorCodes.ERROR_DATAFIELD101_ADDITIONAL_AMOUNT3
/*  839 */             .getErrorDescription());
/*  840 */           errorCodes.add(errorObj);
/*      */         }
/*      */       }
/*      */       else {
/*  844 */         errorObj = new ErrorObject(
/*  845 */           SubmissionErrorCodes.ERROR_DATAFIELD98_ADDITIONAL_AMOUNTTYPE3
/*  846 */           .getErrorCode(), 
/*  847 */           SubmissionErrorCodes.ERROR_DATAFIELD98_ADDITIONAL_AMOUNTTYPE3
/*  848 */           .getErrorDescription() + 
/*  849 */           "\n" + 
/*  850 */           "RecordType:" + 
/*  851 */           recordType + 
/*  852 */           "|" + 
/*  853 */           "RecordNumber:" + 
/*  854 */           recordNumber + 
/*  855 */           "|" + 
/*  856 */           "AdditionalAmountType3" + 
/*  857 */           "|" + 
/*  858 */           "This field can only be AlphaNumeric");
/*  859 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateAdditionalAmount3(TransactionAdviceDetailBean transactionAdviceDetailBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  877 */     ErrorObject errorObj = null;
/*      */     
/*  879 */     if (!CommonValidator.isNullOrEmpty(transactionAdviceDetailBean
/*  880 */       .getAdditionalAmount3()))
/*      */     {
/*  882 */       if (CommonValidator.validateData(transactionAdviceDetailBean
/*  883 */         .getAdditionalAmount3(), 
/*  884 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[100]))
/*      */       {
/*  886 */         String reqLength = CommonValidator.validateLength(
/*  887 */           transactionAdviceDetailBean.getAdditionalAmount3(), 12, 
/*  888 */           12);
/*      */         
/*  890 */         if (reqLength.equals("greaterThanMax"))
/*      */         {
/*  892 */           errorObj = new ErrorObject(
/*  893 */             SubmissionErrorCodes.ERROR_DATAFIELD102_ADDITIONAL_AMOUNT3
/*  894 */             .getErrorCode(), 
/*  895 */             SubmissionErrorCodes.ERROR_DATAFIELD102_ADDITIONAL_AMOUNT3
/*  896 */             .getErrorDescription() + 
/*  897 */             "\n" + 
/*  898 */             "RecordType:" + 
/*  899 */             recordType + 
/*  900 */             "|" + 
/*  901 */             "RecordNumber:" + 
/*  902 */             recordNumber + 
/*  903 */             "|" + 
/*  904 */             "AdditionalAmount3" + 
/*  905 */             "|" + 
/*  906 */             "This field length Cannot be greater than 12");
/*  907 */           errorCodes.add(errorObj);
/*      */ 
/*      */         }
/*  910 */         else if (CommonValidator.isNullOrEmpty(transactionAdviceDetailBean
/*  911 */           .getAdditionalAmountType3())) {
/*  912 */           errorObj = new ErrorObject(
/*  913 */             SubmissionErrorCodes.ERROR_DATAFIELD99_ADDITIONAL_AMOUNTTYPE3
/*  914 */             .getErrorCode(), 
/*  915 */             SubmissionErrorCodes.ERROR_DATAFIELD99_ADDITIONAL_AMOUNTTYPE3
/*  916 */             .getErrorDescription() + 
/*  917 */             "\n" + 
/*  918 */             "RecordType:" + 
/*  919 */             recordType + 
/*  920 */             "|" + 
/*  921 */             "RecordNumber:" + 
/*  922 */             recordNumber + 
/*  923 */             "|" + 
/*  924 */             "AdditionalAmount3" + 
/*  925 */             "|" + 
/*  926 */             SubmissionErrorCodes.ERROR_DATAFIELD99_ADDITIONAL_AMOUNTTYPE3
/*  927 */             .getErrorDescription());
/*  928 */           errorCodes.add(errorObj);
/*      */         }
/*      */       }
/*      */       else {
/*  932 */         errorObj = new ErrorObject(
/*  933 */           SubmissionErrorCodes.ERROR_DATAFIELD100_ADDITIONAL_AMOUNT3
/*  934 */           .getErrorCode(), 
/*  935 */           SubmissionErrorCodes.ERROR_DATAFIELD100_ADDITIONAL_AMOUNT3
/*  936 */           .getErrorDescription() + 
/*  937 */           "\n" + 
/*  938 */           "RecordType:" + 
/*  939 */           recordType + 
/*  940 */           "|" + 
/*  941 */           "RecordNumber:" + 
/*  942 */           recordNumber + 
/*  943 */           "|" + 
/*  944 */           "AdditionalAmount3" + 
/*  945 */           "|" + 
/*  946 */           "This field can only be Numeric");
/*  947 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateAdditionalAmountSign3(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  964 */     ErrorObject errorObj = null;
/*      */     
/*  966 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  968 */       if (CommonValidator.validateData(value, 
/*  969 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[101]))
/*      */       {
/*      */ 
/*      */ 
/*  973 */         String reqLength = CommonValidator.validateLength(value, 1, 1);
/*      */         
/*  975 */         if (reqLength.equals("greaterThanMax")) {
/*  976 */           errorObj = new ErrorObject(
/*  977 */             SubmissionErrorCodes.ERROR_DATAFIELD103_ADDITIONAL_AMOUNT_SIGN3
/*  978 */             .getErrorCode(), 
/*  979 */             SubmissionErrorCodes.ERROR_DATAFIELD103_ADDITIONAL_AMOUNT_SIGN3
/*  980 */             .getErrorDescription() + 
/*  981 */             "\n" + 
/*  982 */             "RecordType:" + 
/*  983 */             recordType + 
/*  984 */             "|" + 
/*  985 */             "RecordNumber:" + 
/*  986 */             recordNumber + 
/*  987 */             "|" + 
/*  988 */             "AdditionalAmountSign3" + 
/*  989 */             "|" + 
/*  990 */             "This field length Cannot be greater than 1");
/*  991 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/*  994 */         errorObj = new ErrorObject(
/*  995 */           SubmissionErrorCodes.ERROR_DATAFIELD103_ADDITIONAL_AMOUNT_SIGN3
/*  996 */           .getErrorCode(), 
/*  997 */           SubmissionErrorCodes.ERROR_DATAFIELD103_ADDITIONAL_AMOUNT_SIGN3
/*  998 */           .getErrorDescription() + 
/*  999 */           "\n" + 
/* 1000 */           "RecordType:" + 
/* 1001 */           recordType + 
/* 1002 */           "|" + 
/* 1003 */           "RecordNumber:" + 
/* 1004 */           recordNumber + 
/* 1005 */           "|" + 
/* 1006 */           "AdditionalAmountSign3" + 
/* 1007 */           "|" + 
/* 1008 */           "This field can only be AlphaNumeric");
/*      */         
/* 1010 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateAdditionalAmountType4(TransactionAdviceDetailBean transactionAdviceDetailBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1028 */     ErrorObject errorObj = null;
/*      */     
/* 1030 */     if (!CommonValidator.isNullOrEmpty(transactionAdviceDetailBean
/* 1031 */       .getAdditionalAmountType4()))
/*      */     {
/* 1033 */       if (CommonValidator.validateData(transactionAdviceDetailBean
/* 1034 */         .getAdditionalAmountType4(), 
/* 1035 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[102]))
/*      */       {
/* 1037 */         String reqLength = CommonValidator.validateLength(
/* 1038 */           transactionAdviceDetailBean.getAdditionalAmountType4(), 
/* 1039 */           3, 3);
/*      */         
/* 1041 */         if (reqLength.equals("greaterThanMax")) {
/* 1042 */           errorObj = new ErrorObject(
/* 1043 */             SubmissionErrorCodes.ERROR_DATAFIELD104_ADDITIONAL_AMOUNTTYPE4
/* 1044 */             .getErrorCode(), 
/* 1045 */             SubmissionErrorCodes.ERROR_DATAFIELD104_ADDITIONAL_AMOUNTTYPE4
/* 1046 */             .getErrorDescription() + 
/* 1047 */             "\n" + 
/* 1048 */             "RecordType:" + 
/* 1049 */             recordType + 
/* 1050 */             "|" + 
/* 1051 */             "RecordNumber:" + 
/* 1052 */             recordNumber + 
/* 1053 */             "|" + 
/* 1054 */             "AdditionalAmountType4" + 
/* 1055 */             "|" + 
/* 1056 */             "This field length Cannot be greater than 3");
/* 1057 */           errorCodes.add(errorObj);
/*      */ 
/*      */         }
/* 1060 */         else if (CommonValidator.isNullOrEmpty(transactionAdviceDetailBean
/* 1061 */           .getAdditionalAmount4())) {
/* 1062 */           errorObj = new ErrorObject(
/* 1063 */             SubmissionErrorCodes.ERROR_DATAFIELD107_ADDITIONAL_AMOUNT4
/* 1064 */             .getErrorCode(), 
/* 1065 */             SubmissionErrorCodes.ERROR_DATAFIELD107_ADDITIONAL_AMOUNT4
/* 1066 */             .getErrorDescription() + 
/* 1067 */             "\n" + 
/* 1068 */             "RecordType:" + 
/* 1069 */             recordType + 
/* 1070 */             "|" + 
/* 1071 */             "RecordNumber:" + 
/* 1072 */             recordNumber + 
/* 1073 */             "|" + 
/* 1074 */             "AdditionalAmountType4" + 
/* 1075 */             "|" + 
/* 1076 */             SubmissionErrorCodes.ERROR_DATAFIELD107_ADDITIONAL_AMOUNT4
/* 1077 */             .getErrorDescription());
/* 1078 */           errorCodes.add(errorObj);
/*      */         }
/*      */       }
/*      */       else {
/* 1082 */         errorObj = new ErrorObject(
/* 1083 */           SubmissionErrorCodes.ERROR_DATAFIELD104_ADDITIONAL_AMOUNTTYPE4
/* 1084 */           .getErrorCode(), 
/* 1085 */           SubmissionErrorCodes.ERROR_DATAFIELD104_ADDITIONAL_AMOUNTTYPE4
/* 1086 */           .getErrorDescription() + 
/* 1087 */           "\n" + 
/* 1088 */           "RecordType:" + 
/* 1089 */           recordType + 
/* 1090 */           "|" + 
/* 1091 */           "RecordNumber:" + 
/* 1092 */           recordNumber + 
/* 1093 */           "|" + 
/* 1094 */           "AdditionalAmountType4" + 
/* 1095 */           "|" + 
/* 1096 */           "This field can only be AlphaNumeric");
/* 1097 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateAdditionalAmount4(TransactionAdviceDetailBean transactionAdviceDetailBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1115 */     ErrorObject errorObj = null;
/*      */     
/* 1117 */     if (!CommonValidator.isNullOrEmpty(transactionAdviceDetailBean
/* 1118 */       .getAdditionalAmount4()))
/*      */     {
/* 1120 */       if (CommonValidator.validateData(transactionAdviceDetailBean
/* 1121 */         .getAdditionalAmount4(), 
/* 1122 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[104]))
/*      */       {
/* 1124 */         String reqLength = CommonValidator.validateLength(
/* 1125 */           transactionAdviceDetailBean.getAdditionalAmount4(), 12, 
/* 1126 */           12);
/*      */         
/* 1128 */         if (reqLength.equals("greaterThanMax")) {
/* 1129 */           errorObj = new ErrorObject(
/* 1130 */             SubmissionErrorCodes.ERROR_DATAFIELD108_ADDITIONAL_AMOUNT4
/* 1131 */             .getErrorCode(), 
/* 1132 */             SubmissionErrorCodes.ERROR_DATAFIELD108_ADDITIONAL_AMOUNT4
/* 1133 */             .getErrorDescription() + 
/* 1134 */             "\n" + 
/* 1135 */             "RecordType:" + 
/* 1136 */             recordType + 
/* 1137 */             "|" + 
/* 1138 */             "RecordNumber:" + 
/* 1139 */             recordNumber + 
/* 1140 */             "|" + 
/* 1141 */             "AdditionalAmount4" + 
/* 1142 */             "|" + 
/* 1143 */             "This field length Cannot be greater than 12");
/* 1144 */           errorCodes.add(errorObj);
/*      */ 
/*      */ 
/*      */         }
/* 1148 */         else if (CommonValidator.isNullOrEmpty(transactionAdviceDetailBean
/* 1149 */           .getAdditionalAmountType4())) {
/* 1150 */           errorObj = new ErrorObject(
/* 1151 */             SubmissionErrorCodes.ERROR_DATAFIELD105_ADDITIONAL_AMOUNTTYPE4
/* 1152 */             .getErrorCode(), 
/* 1153 */             SubmissionErrorCodes.ERROR_DATAFIELD105_ADDITIONAL_AMOUNTTYPE4
/* 1154 */             .getErrorDescription() + 
/* 1155 */             "\n" + 
/* 1156 */             "RecordType:" + 
/* 1157 */             recordType + 
/* 1158 */             "|" + 
/* 1159 */             "RecordNumber:" + 
/* 1160 */             recordNumber + 
/* 1161 */             "|" + 
/* 1162 */             "AdditionalAmount4" + 
/* 1163 */             "|" + 
/* 1164 */             SubmissionErrorCodes.ERROR_DATAFIELD105_ADDITIONAL_AMOUNTTYPE4
/* 1165 */             .getErrorDescription());
/* 1166 */           errorCodes.add(errorObj);
/*      */         }
/*      */       }
/*      */       else {
/* 1170 */         errorObj = new ErrorObject(
/* 1171 */           SubmissionErrorCodes.ERROR_DATAFIELD106_ADDITIONAL_AMOUNT4
/* 1172 */           .getErrorCode(), 
/* 1173 */           SubmissionErrorCodes.ERROR_DATAFIELD106_ADDITIONAL_AMOUNT4
/* 1174 */           .getErrorDescription() + 
/* 1175 */           "\n" + 
/* 1176 */           "RecordType:" + 
/* 1177 */           recordType + 
/* 1178 */           "|" + 
/* 1179 */           "RecordNumber:" + 
/* 1180 */           recordNumber + 
/* 1181 */           "|" + 
/* 1182 */           "AdditionalAmount4" + 
/* 1183 */           "|" + 
/* 1184 */           "This field can only be Numeric");
/* 1185 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateAdditionalAmountSign4(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1202 */     ErrorObject errorObj = null;
/*      */     
/* 1204 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/* 1206 */       if (CommonValidator.validateData(value, 
/* 1207 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[105]))
/*      */       {
/*      */ 
/* 1210 */         String reqLength = CommonValidator.validateLength(value, 1, 1);
/*      */         
/* 1212 */         if (reqLength.equals("greaterThanMax")) {
/* 1213 */           errorObj = new ErrorObject(
/* 1214 */             SubmissionErrorCodes.ERROR_DATAFIELD109_ADDITIONAL_AMOUNT_SIGN4
/* 1215 */             .getErrorCode(), 
/* 1216 */             SubmissionErrorCodes.ERROR_DATAFIELD109_ADDITIONAL_AMOUNT_SIGN4
/* 1217 */             .getErrorDescription() + 
/* 1218 */             "\n" + 
/* 1219 */             "RecordType:" + 
/* 1220 */             recordType + 
/* 1221 */             "|" + 
/* 1222 */             "RecordNumber:" + 
/* 1223 */             recordNumber + 
/* 1224 */             "|" + 
/* 1225 */             "AdditionalAmountSign4" + 
/* 1226 */             "|" + 
/* 1227 */             "This field length Cannot be greater than 1");
/* 1228 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/* 1231 */         errorObj = new ErrorObject(
/* 1232 */           SubmissionErrorCodes.ERROR_DATAFIELD109_ADDITIONAL_AMOUNT_SIGN4
/* 1233 */           .getErrorCode(), 
/* 1234 */           SubmissionErrorCodes.ERROR_DATAFIELD109_ADDITIONAL_AMOUNT_SIGN4
/* 1235 */           .getErrorDescription() + 
/* 1236 */           "\n" + 
/* 1237 */           "RecordType:" + 
/* 1238 */           recordType + 
/* 1239 */           "|" + 
/* 1240 */           "RecordNumber:" + 
/* 1241 */           recordNumber + 
/* 1242 */           "|" + 
/* 1243 */           "AdditionalAmountSign4" + 
/* 1244 */           "|" + 
/* 1245 */           "This field can only be AlphaNumeric");
/* 1246 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateAdditionalAmountType5(TransactionAdviceDetailBean transactionAdviceDetailBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1264 */     ErrorObject errorObj = null;
/*      */     
/* 1266 */     if (!CommonValidator.isNullOrEmpty(transactionAdviceDetailBean
/* 1267 */       .getAdditionalAmountType5()))
/*      */     {
/* 1269 */       if (CommonValidator.validateData(transactionAdviceDetailBean
/* 1270 */         .getAdditionalAmountType5(), 
/* 1271 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[106]))
/*      */       {
/* 1273 */         String reqLength = CommonValidator.validateLength(
/* 1274 */           transactionAdviceDetailBean.getAdditionalAmountType5(), 
/* 1275 */           3, 3);
/*      */         
/* 1277 */         if (reqLength.equals("greaterThanMax")) {
/* 1278 */           errorObj = new ErrorObject(
/* 1279 */             SubmissionErrorCodes.ERROR_DATAFIELD110_ADDITIONAL_AMOUNTTYPE5
/* 1280 */             .getErrorCode(), 
/* 1281 */             SubmissionErrorCodes.ERROR_DATAFIELD110_ADDITIONAL_AMOUNTTYPE5
/* 1282 */             .getErrorDescription() + 
/* 1283 */             "\n" + 
/* 1284 */             "RecordType:" + 
/* 1285 */             recordType + 
/* 1286 */             "|" + 
/* 1287 */             "RecordNumber:" + 
/* 1288 */             recordNumber + 
/* 1289 */             "|" + 
/* 1290 */             "AdditionalAmountType5" + 
/* 1291 */             "|" + 
/* 1292 */             "This field length Cannot be greater than 3");
/* 1293 */           errorCodes.add(errorObj);
/*      */ 
/*      */ 
/*      */         }
/* 1297 */         else if (CommonValidator.isNullOrEmpty(transactionAdviceDetailBean
/* 1298 */           .getAdditionalAmount5())) {
/* 1299 */           errorObj = new ErrorObject(
/* 1300 */             SubmissionErrorCodes.ERROR_DATAFIELD113_ADDITIONAL_AMOUNT5
/* 1301 */             .getErrorCode(), 
/* 1302 */             SubmissionErrorCodes.ERROR_DATAFIELD113_ADDITIONAL_AMOUNT5
/* 1303 */             .getErrorDescription() + 
/* 1304 */             "\n" + 
/* 1305 */             "RecordType:" + 
/* 1306 */             recordType + 
/* 1307 */             "|" + 
/* 1308 */             "RecordNumber:" + 
/* 1309 */             recordNumber + 
/* 1310 */             "|" + 
/* 1311 */             "AdditionalAmountType5" + 
/* 1312 */             "|" + 
/* 1313 */             SubmissionErrorCodes.ERROR_DATAFIELD113_ADDITIONAL_AMOUNT5
/* 1314 */             .getErrorDescription());
/* 1315 */           errorCodes.add(errorObj);
/*      */         }
/*      */       }
/*      */       else {
/* 1319 */         errorObj = new ErrorObject(
/* 1320 */           SubmissionErrorCodes.ERROR_DATAFIELD110_ADDITIONAL_AMOUNTTYPE5
/* 1321 */           .getErrorCode(), 
/* 1322 */           SubmissionErrorCodes.ERROR_DATAFIELD110_ADDITIONAL_AMOUNTTYPE5
/* 1323 */           .getErrorDescription() + 
/* 1324 */           "\n" + 
/* 1325 */           "RecordType:" + 
/* 1326 */           recordType + 
/* 1327 */           "|" + 
/* 1328 */           "RecordNumber:" + 
/* 1329 */           recordNumber + 
/* 1330 */           "|" + 
/* 1331 */           "AdditionalAmountType5" + 
/* 1332 */           "|" + 
/* 1333 */           "This field can only be AlphaNumeric");
/* 1334 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateAdditionalAmount5(TransactionAdviceDetailBean transactionAdviceDetailBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1352 */     ErrorObject errorObj = null;
/*      */     
/* 1354 */     if (!CommonValidator.isNullOrEmpty(transactionAdviceDetailBean
/* 1355 */       .getAdditionalAmount5()))
/*      */     {
/* 1357 */       if (CommonValidator.validateData(transactionAdviceDetailBean
/* 1358 */         .getAdditionalAmount5(), 
/* 1359 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[108]))
/*      */       {
/* 1361 */         String reqLength = CommonValidator.validateLength(
/* 1362 */           transactionAdviceDetailBean.getAdditionalAmount5(), 12, 
/* 1363 */           12);
/*      */         
/* 1365 */         if (reqLength.equals("greaterThanMax")) {
/* 1366 */           errorObj = new ErrorObject(
/* 1367 */             SubmissionErrorCodes.ERROR_DATAFIELD114_ADDITIONAL_AMOUNT5
/* 1368 */             .getErrorCode(), 
/* 1369 */             SubmissionErrorCodes.ERROR_DATAFIELD114_ADDITIONAL_AMOUNT5
/* 1370 */             .getErrorDescription() + 
/* 1371 */             "\n" + 
/* 1372 */             "RecordType:" + 
/* 1373 */             recordType + 
/* 1374 */             "|" + 
/* 1375 */             "RecordNumber:" + 
/* 1376 */             recordNumber + 
/* 1377 */             "|" + 
/* 1378 */             "AdditionalAmount5" + 
/* 1379 */             "|" + 
/* 1380 */             "This field length Cannot be greater than 12");
/* 1381 */           errorCodes.add(errorObj);
/*      */ 
/*      */ 
/*      */         }
/* 1385 */         else if (CommonValidator.isNullOrEmpty(transactionAdviceDetailBean
/* 1386 */           .getAdditionalAmountType5())) {
/* 1387 */           errorObj = new ErrorObject(
/* 1388 */             SubmissionErrorCodes.ERROR_DATAFIELD111_ADDITIONAL_AMOUNTTYPE5
/* 1389 */             .getErrorCode(), 
/* 1390 */             SubmissionErrorCodes.ERROR_DATAFIELD111_ADDITIONAL_AMOUNTTYPE5
/* 1391 */             .getErrorDescription() + 
/* 1392 */             "\n" + 
/* 1393 */             "RecordType:" + 
/* 1394 */             recordType + 
/* 1395 */             "|" + 
/* 1396 */             "RecordNumber:" + 
/* 1397 */             recordNumber + 
/* 1398 */             "|" + 
/* 1399 */             "AdditionalAmount5" + 
/* 1400 */             "|" + 
/* 1401 */             SubmissionErrorCodes.ERROR_DATAFIELD111_ADDITIONAL_AMOUNTTYPE5
/* 1402 */             .getErrorDescription());
/* 1403 */           errorCodes.add(errorObj);
/*      */         }
/*      */       }
/*      */       else {
/* 1407 */         errorObj = new ErrorObject(
/* 1408 */           SubmissionErrorCodes.ERROR_DATAFIELD112_ADDITIONAL_AMOUNT5
/* 1409 */           .getErrorCode(), 
/* 1410 */           SubmissionErrorCodes.ERROR_DATAFIELD112_ADDITIONAL_AMOUNT5
/* 1411 */           .getErrorDescription() + 
/* 1412 */           "\n" + 
/* 1413 */           "RecordType:" + 
/* 1414 */           recordType + 
/* 1415 */           "|" + 
/* 1416 */           "RecordNumber:" + 
/* 1417 */           recordNumber + 
/* 1418 */           "|" + 
/* 1419 */           "AdditionalAmount5" + 
/* 1420 */           "|" + 
/* 1421 */           "This field can only be Numeric");
/* 1422 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateAdditionalAmountSign5(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1439 */     ErrorObject errorObj = null;
/*      */     
/* 1441 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/* 1443 */       if (CommonValidator.validateData(value, 
/* 1444 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[109]))
/*      */       {
/* 1446 */         String reqLength = CommonValidator.validateLength(value, 1, 1);
/*      */         
/* 1448 */         if (reqLength.equals("greaterThanMax")) {
/* 1449 */           errorObj = new ErrorObject(
/* 1450 */             SubmissionErrorCodes.ERROR_DATAFIELD115_ADDITIONAL_AMOUNT_SIGN5
/* 1451 */             .getErrorCode(), 
/* 1452 */             SubmissionErrorCodes.ERROR_DATAFIELD115_ADDITIONAL_AMOUNT_SIGN5
/* 1453 */             .getErrorDescription() + 
/* 1454 */             "\n" + 
/* 1455 */             "RecordType:" + 
/* 1456 */             recordType + 
/* 1457 */             "|" + 
/* 1458 */             "RecordNumber:" + 
/* 1459 */             recordNumber + 
/* 1460 */             "|" + 
/* 1461 */             "AdditionalAmountSign5" + 
/* 1462 */             "|" + 
/* 1463 */             "This field length Cannot be greater than 1");
/* 1464 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/* 1467 */         errorObj = new ErrorObject(
/* 1468 */           SubmissionErrorCodes.ERROR_DATAFIELD115_ADDITIONAL_AMOUNT_SIGN5
/* 1469 */           .getErrorCode(), 
/* 1470 */           SubmissionErrorCodes.ERROR_DATAFIELD115_ADDITIONAL_AMOUNT_SIGN5
/* 1471 */           .getErrorDescription() + 
/* 1472 */           "\n" + 
/* 1473 */           "RecordType:" + 
/* 1474 */           recordType + 
/* 1475 */           "|" + 
/* 1476 */           "RecordNumber:" + 
/* 1477 */           recordNumber + 
/* 1478 */           "|" + 
/* 1479 */           "AdditionalAmountSign5" + 
/* 1480 */           "|" + 
/* 1481 */           "This field can only be AlphaNumeric");
/* 1482 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\validator\TADRecValidator.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */