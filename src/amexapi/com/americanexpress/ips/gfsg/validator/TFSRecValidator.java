/*     */ package com.americanexpress.ips.gfsg.validator;
/*     */ 
/*     */ import com.americanexpress.ips.gfsg.beans.ErrorObject;
/*     */ import com.americanexpress.ips.gfsg.beans.SettlementRequestDataObject;
/*     */ import com.americanexpress.ips.gfsg.beans.common.TransactionAdviceBasicBean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.TransactionFileSummaryBean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.TransactionTBTSpecificBean;
/*     */ import com.americanexpress.ips.gfsg.enumerations.ProcessingCode;
/*     */ import com.americanexpress.ips.gfsg.enumerations.RecordType;
/*     */ import com.americanexpress.ips.gfsg.enumerations.SubmissionErrorCodes;
/*     */ import com.americanexpress.ips.gfsg.exceptions.SettlementException;
/*     */ import java.util.Iterator;
/*     */ import java.util.List;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class TFSRecValidator
/*     */ {
/*     */   private static String recordNumber;
/*     */   
/*     */   public static void validateTFSRecord(TransactionFileSummaryBean transactionFileSummaryType, SettlementRequestDataObject settlementReqBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/*  45 */     recordNumber = transactionFileSummaryType.getRecordNumber();
/*  46 */     validateRecordType(transactionFileSummaryType.getRecordType(), 
/*  47 */       errorCodes);
/*  48 */     validateRecordNumber(transactionFileSummaryType.getRecordNumber(), 
/*  49 */       errorCodes);
/*  50 */     validateNumberOfDebits(transactionFileSummaryType.getNumberOfDebits(), 
/*  51 */       settlementReqBean, errorCodes);
/*  52 */     validateHashTotalDebitAmount(transactionFileSummaryType
/*  53 */       .getHashTotalDebitAmount(), settlementReqBean, errorCodes);
/*  54 */     validateNumberOfCredits(
/*  55 */       transactionFileSummaryType.getNumberOfCredits(), 
/*  56 */       settlementReqBean, errorCodes);
/*  57 */     validateHashTotalCreditAmount(transactionFileSummaryType
/*  58 */       .getHashTotalCreditAmount(), settlementReqBean, errorCodes);
/*  59 */     validateHashTotalAmount(
/*  60 */       transactionFileSummaryType.getHashTotalAmount(), 
/*  61 */       settlementReqBean, errorCodes);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateRecordType(String recordType, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/*  76 */     ErrorObject errorObj = null;
/*     */     
/*  78 */     if (CommonValidator.isNullOrEmpty(recordType))
/*     */     {
/*  80 */       errorObj = new ErrorObject(
/*  81 */         SubmissionErrorCodes.ERROR_DATAFIELD1_RECORDTYPE
/*  82 */         .getErrorCode(), 
/*  83 */         SubmissionErrorCodes.ERROR_DATAFIELD1_RECORDTYPE
/*  84 */         .getErrorDescription() + 
/*  85 */         "\n" + 
/*  86 */         "RecordType:" + 
/*  87 */         "TFS" + 
/*  88 */         "|" + 
/*  89 */         "RecordNumber:" + 
/*  90 */         recordNumber + 
/*  91 */         "|" + 
/*  92 */         "RecordNumber:" + 
/*  93 */         "|" + 
/*  94 */         "This field is mandatory and cannot be empty");
/*  95 */       errorCodes.add(errorObj);
/*     */     }
/*  97 */     else if (!RecordType.Transaction_File_Summary.getRecordType().equalsIgnoreCase(recordType))
/*     */     {
/*     */ 
/* 100 */       errorObj = new ErrorObject(
/* 101 */         SubmissionErrorCodes.ERROR_DATAFIELD1_RECORDTYPE);
/* 102 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateRecordNumber(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 119 */     ErrorObject errorObj = null;
/*     */     
/* 121 */     if (CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 123 */       errorObj = new ErrorObject(
/* 124 */         SubmissionErrorCodes.ERROR_DATAFIELD585_RECORDNUMBER
/* 125 */         .getErrorCode(), 
/* 126 */         SubmissionErrorCodes.ERROR_DATAFIELD585_RECORDNUMBER
/* 127 */         .getErrorDescription() + 
/* 128 */         "\n" + 
/* 129 */         "RecordType:" + 
/* 130 */         "TFS" + 
/* 131 */         "|" + 
/* 132 */         "RecordNumber:" + 
/* 133 */         recordNumber + 
/* 134 */         "|" + 
/* 135 */         "RecordNumber:" + 
/* 136 */         "|" + 
/* 137 */         "This field is mandatory and cannot be empty");
/* 138 */       errorCodes.add(errorObj);
/*     */     }
/* 140 */     else if (CommonValidator.validateData(value, 
/* 141 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[11]))
/*     */     {
/* 143 */       String reqLength = CommonValidator.validateLength(value, 8, 8);
/*     */       
/* 145 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/* 147 */         errorObj = new ErrorObject(
/* 148 */           SubmissionErrorCodes.ERROR_DATAFIELD3_RECORDNUMBER
/* 149 */           .getErrorCode(), 
/* 150 */           SubmissionErrorCodes.ERROR_DATAFIELD3_RECORDNUMBER
/* 151 */           .getErrorDescription() + 
/* 152 */           "\n" + 
/* 153 */           "RecordType:" + 
/* 154 */           "TFS" + 
/* 155 */           "|" + 
/* 156 */           "RecordNumber:" + 
/* 157 */           recordNumber + 
/* 158 */           "|" + 
/* 159 */           "RecordNumber:" + 
/* 160 */           "|" + 
/* 161 */           "This field length Cannot be greater than 8");
/* 162 */         errorCodes.add(errorObj);
/*     */       }
/*     */     } else {
/* 165 */       errorObj = new ErrorObject(
/* 166 */         SubmissionErrorCodes.ERROR_DATAFIELD2_RECORDNUMBER
/* 167 */         .getErrorCode(), 
/* 168 */         SubmissionErrorCodes.ERROR_DATAFIELD2_RECORDNUMBER
/* 169 */         .getErrorDescription() + 
/* 170 */         "\n" + 
/* 171 */         "RecordType:" + 
/* 172 */         "TFS" + 
/* 173 */         "|" + 
/* 174 */         "RecordNumber:" + 
/* 175 */         recordNumber + 
/* 176 */         "|" + 
/* 177 */         "RecordNumber:" + 
/* 178 */         "|" + 
/* 179 */         "This field can only be Numeric");
/* 180 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateNumberOfDebits(String value, SettlementRequestDataObject settlementReqBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 199 */     ErrorObject errorObj = null;
/*     */     
/* 201 */     if (CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 203 */       errorObj = new ErrorObject(
/* 204 */         SubmissionErrorCodes.ERROR_DATAFIELD594_NUMBEROF_DEBITS
/* 205 */         .getErrorCode(), 
/* 206 */         SubmissionErrorCodes.ERROR_DATAFIELD594_NUMBEROF_DEBITS
/* 207 */         .getErrorDescription() + 
/* 208 */         "\n" + 
/* 209 */         "RecordType:" + 
/* 210 */         "TFS" + 
/* 211 */         "|" + 
/* 212 */         "RecordNumber:" + 
/* 213 */         recordNumber + 
/* 214 */         "|" + 
/* 215 */         "numberOfDebits" + 
/* 216 */         "|" + 
/* 217 */         "This field is mandatory and cannot be empty");
/* 218 */       errorCodes.add(errorObj);
/*     */     }
/* 220 */     else if (CommonValidator.validateData(value, 
/* 221 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[12]))
/*     */     {
/* 223 */       String reqLength = CommonValidator.validateLength(value, 8, 8);
/*     */       
/* 225 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/* 227 */         errorObj = new ErrorObject(
/* 228 */           SubmissionErrorCodes.ERROR_DATAFIELD016_NUMBEROF_DEBITS
/* 229 */           .getErrorCode(), 
/* 230 */           SubmissionErrorCodes.ERROR_DATAFIELD016_NUMBEROF_DEBITS
/* 231 */           .getErrorDescription() + 
/* 232 */           "\n" + 
/* 233 */           "RecordType:" + 
/* 234 */           "TFS" + 
/* 235 */           "|" + 
/* 236 */           "RecordNumber:" + 
/* 237 */           recordNumber + 
/* 238 */           "|" + 
/* 239 */           "numberOfDebits" + 
/* 240 */           "|" + 
/* 241 */           "This field length Cannot be greater than 8");
/* 242 */         errorCodes.add(errorObj);
/*     */       }
/*     */       else {
/* 245 */         int totTABDebits = noOfTABDebits(settlementReqBean);
/* 246 */         int noOfDebits = Integer.parseInt(value);
/*     */         
/* 248 */         if (totTABDebits != noOfDebits)
/*     */         {
/* 250 */           errorObj = new ErrorObject(
/* 251 */             SubmissionErrorCodes.ERROR_DATAFIELD16_NUMBEROF_DEBITS
/* 252 */             .getErrorCode(), 
/* 253 */             SubmissionErrorCodes.ERROR_DATAFIELD16_NUMBEROF_DEBITS
/* 254 */             .getErrorDescription() + 
/* 255 */             "\n" + 
/* 256 */             "RecordType:" + 
/* 257 */             "TFS" + 
/* 258 */             "|" + 
/* 259 */             "RecordNumber:" + 
/* 260 */             recordNumber + 
/* 261 */             "|" + 
/* 262 */             "numberOfDebits" + 
/* 263 */             "|" + 
/* 264 */             SubmissionErrorCodes.ERROR_DATAFIELD16_NUMBEROF_DEBITS
/* 265 */             .getErrorDescription());
/* 266 */           errorCodes.add(errorObj);
/*     */         }
/*     */       }
/*     */     }
/*     */     else
/*     */     {
/* 272 */       errorObj = new ErrorObject(
/* 273 */         SubmissionErrorCodes.ERROR_DATAFIELD15_NUMBEROF_DEBITS
/* 274 */         .getErrorCode(), 
/* 275 */         SubmissionErrorCodes.ERROR_DATAFIELD15_NUMBEROF_DEBITS
/* 276 */         .getErrorDescription() + 
/* 277 */         "\n" + 
/* 278 */         "RecordType:" + 
/* 279 */         "TFS" + 
/* 280 */         "|" + 
/* 281 */         "RecordNumber:" + 
/* 282 */         recordNumber + 
/* 283 */         "|" + 
/* 284 */         "numberOfDebits" + 
/* 285 */         "|" + 
/* 286 */         "This field can only be Numeric");
/* 287 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateHashTotalDebitAmount(String value, SettlementRequestDataObject settlementReqBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 306 */     ErrorObject errorObj = null;
/*     */     
/* 308 */     if (CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 310 */       errorObj = new ErrorObject(
/* 311 */         SubmissionErrorCodes.ERROR_DATAFIELD18_HASH_TOTALDEBITAMOUNT
/* 312 */         .getErrorCode(), 
/* 313 */         SubmissionErrorCodes.ERROR_DATAFIELD18_HASH_TOTALDEBITAMOUNT
/* 314 */         .getErrorDescription() + 
/* 315 */         "\n" + 
/* 316 */         "RecordType:" + 
/* 317 */         "TFS" + 
/* 318 */         "|" + 
/* 319 */         "RecordNumber:" + 
/* 320 */         recordNumber + 
/* 321 */         "|" + 
/* 322 */         "Hash Total Debit Amount" + 
/* 323 */         "|" + 
/* 324 */         "This field is mandatory and cannot be empty");
/* 325 */       errorCodes.add(errorObj);
/*     */     }
/* 327 */     else if (CommonValidator.validateData(value, 
/* 328 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[14]))
/*     */     {
/* 330 */       String reqLength = 
/* 331 */         CommonValidator.validateLength(value, 20, 20);
/*     */       
/* 333 */       if (reqLength.equals("greaterThanMax")) {
/* 334 */         errorObj = new ErrorObject(
/* 335 */           SubmissionErrorCodes.ERROR_DATAFIELD18_HASH_TOTALDEBITAMOUNT
/* 336 */           .getErrorCode(), 
/* 337 */           SubmissionErrorCodes.ERROR_DATAFIELD18_HASH_TOTALDEBITAMOUNT
/* 338 */           .getErrorDescription() + 
/* 339 */           "\n" + 
/* 340 */           "RecordType:" + 
/* 341 */           "TFS" + 
/* 342 */           "|" + 
/* 343 */           "RecordNumber:" + 
/* 344 */           recordNumber + 
/* 345 */           "|" + 
/* 346 */           "Hash Total Debit Amount" + 
/* 347 */           "|" + 
/* 348 */           "This field length Cannot be greater than 20");
/* 349 */         errorCodes.add(errorObj);
/*     */       }
/*     */       else {
/* 352 */         long tabDbtAmt = totalDebitAmount(settlementReqBean);
/* 353 */         long hasTotDbtAmt = Long.parseLong(value);
/*     */         
/* 355 */         if (tabDbtAmt != hasTotDbtAmt)
/*     */         {
/* 357 */           errorObj = new ErrorObject(
/* 358 */             SubmissionErrorCodes.ERROR_DATAFIELD18_HASH_TOTALDEBITAMOUNT
/* 359 */             .getErrorCode(), 
/* 360 */             SubmissionErrorCodes.ERROR_DATAFIELD18_HASH_TOTALDEBITAMOUNT
/* 361 */             .getErrorDescription() + 
/* 362 */             "\n" + 
/* 363 */             "RecordType:" + 
/* 364 */             "TFS" + 
/* 365 */             "|" + 
/* 366 */             "RecordNumber:" + 
/* 367 */             recordNumber + 
/* 368 */             "|" + 
/* 369 */             "Hash Total Debit Amount" + 
/* 370 */             "|" + 
/* 371 */             SubmissionErrorCodes.ERROR_DATAFIELD18_HASH_TOTALDEBITAMOUNT
/* 372 */             .getErrorDescription());
/* 373 */           errorCodes.add(errorObj);
/*     */         }
/*     */       }
/*     */     }
/*     */     else
/*     */     {
/* 379 */       errorObj = new ErrorObject(
/* 380 */         SubmissionErrorCodes.ERROR_DATAFIELD17_HASH_TOTALDEBITAMOUNT
/* 381 */         .getErrorCode(), 
/* 382 */         SubmissionErrorCodes.ERROR_DATAFIELD17_HASH_TOTALDEBITAMOUNT
/* 383 */         .getErrorDescription() + 
/* 384 */         "\n" + 
/* 385 */         "RecordType:" + 
/* 386 */         "TFS" + 
/* 387 */         "|" + 
/* 388 */         "RecordNumber:" + 
/* 389 */         recordNumber + 
/* 390 */         "|" + 
/* 391 */         "Hash Total Debit Amount" + 
/* 392 */         "|" + 
/* 393 */         "This field can only be Numeric");
/* 394 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateNumberOfCredits(String value, SettlementRequestDataObject settlementReqBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 413 */     ErrorObject errorObj = null;
/*     */     
/* 415 */     if (CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 417 */       errorObj = new ErrorObject(
/* 418 */         SubmissionErrorCodes.ERROR_DATAFIELD20_NUMBEROFCREDITS
/* 419 */         .getErrorCode(), 
/* 420 */         SubmissionErrorCodes.ERROR_DATAFIELD20_NUMBEROFCREDITS
/* 421 */         .getErrorDescription() + 
/* 422 */         "\n" + 
/* 423 */         "RecordType:" + 
/* 424 */         "TFS" + 
/* 425 */         "|" + 
/* 426 */         "RecordNumber:" + 
/* 427 */         recordNumber + 
/* 428 */         "|" + 
/* 429 */         "numberOfCredits" + 
/* 430 */         "|" + 
/* 431 */         "This field is mandatory and cannot be empty");
/* 432 */       errorCodes.add(errorObj);
/*     */     }
/* 434 */     else if (CommonValidator.validateData(value, 
/* 435 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[15]))
/*     */     {
/* 437 */       String reqLength = CommonValidator.validateLength(value, 8, 8);
/*     */       
/* 439 */       if (reqLength.equals("greaterThanMax")) {
/* 440 */         errorObj = new ErrorObject(
/* 441 */           SubmissionErrorCodes.ERROR_DATAFIELD20_NUMBEROFCREDITS
/* 442 */           .getErrorCode(), 
/* 443 */           SubmissionErrorCodes.ERROR_DATAFIELD20_NUMBEROFCREDITS
/* 444 */           .getErrorDescription() + 
/* 445 */           "\n" + 
/* 446 */           "RecordType:" + 
/* 447 */           "TFS" + 
/* 448 */           "|" + 
/* 449 */           "RecordNumber:" + 
/* 450 */           recordNumber + 
/* 451 */           "|" + 
/* 452 */           "numberOfCredits" + 
/* 453 */           "|" + 
/* 454 */           "This field length Cannot be greater than 8");
/* 455 */         errorCodes.add(errorObj);
/*     */       }
/*     */       else {
/* 458 */         int tabCredits = noOfTABCredits(settlementReqBean);
/* 459 */         int noOfCredits = Integer.parseInt(value);
/*     */         
/* 461 */         if (tabCredits != noOfCredits)
/*     */         {
/* 463 */           errorObj = new ErrorObject(
/* 464 */             SubmissionErrorCodes.ERROR_DATAFIELD20_NUMBEROFCREDITS
/* 465 */             .getErrorCode(), 
/* 466 */             SubmissionErrorCodes.ERROR_DATAFIELD20_NUMBEROFCREDITS
/* 467 */             .getErrorDescription() + 
/* 468 */             "\n" + 
/* 469 */             "RecordType:" + 
/* 470 */             "TFS" + 
/* 471 */             "|" + 
/* 472 */             "RecordNumber:" + 
/* 473 */             recordNumber + 
/* 474 */             "|" + 
/* 475 */             "numberOfCredits" + 
/* 476 */             "|" + 
/* 477 */             SubmissionErrorCodes.ERROR_DATAFIELD20_NUMBEROFCREDITS
/* 478 */             .getErrorDescription());
/* 479 */           errorCodes.add(errorObj);
/*     */         }
/*     */       }
/*     */     }
/*     */     else {
/* 484 */       errorObj = new ErrorObject(
/* 485 */         SubmissionErrorCodes.ERROR_DATAFIELD19_NUMBEROFCREDITS
/* 486 */         .getErrorCode(), 
/* 487 */         SubmissionErrorCodes.ERROR_DATAFIELD19_NUMBEROFCREDITS
/* 488 */         .getErrorDescription() + 
/* 489 */         "\n" + 
/* 490 */         "RecordType:" + 
/* 491 */         "TFS" + 
/* 492 */         "|" + 
/* 493 */         "RecordNumber:" + 
/* 494 */         recordNumber + 
/* 495 */         "|" + 
/* 496 */         "numberOfCredits" + 
/* 497 */         "|" + 
/* 498 */         "This field can only be Numeric");
/* 499 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateHashTotalCreditAmount(String value, SettlementRequestDataObject settlementReqBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 518 */     ErrorObject errorObj = null;
/*     */     
/* 520 */     if (CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 522 */       errorObj = new ErrorObject(
/* 523 */         SubmissionErrorCodes.ERROR_DATAFIELD22_HASHTOTALCREDITAMOUNT
/* 524 */         .getErrorCode(), 
/* 525 */         SubmissionErrorCodes.ERROR_DATAFIELD22_HASHTOTALCREDITAMOUNT
/* 526 */         .getErrorDescription() + 
/* 527 */         "\n" + 
/* 528 */         "RecordType:" + 
/* 529 */         "TFS" + 
/* 530 */         "|" + 
/* 531 */         "RecordNumber:" + 
/* 532 */         recordNumber + 
/* 533 */         "|" + 
/* 534 */         "Hash Total Credit Amount" + 
/* 535 */         "|" + 
/* 536 */         "This field is mandatory and cannot be empty");
/* 537 */       errorCodes.add(errorObj);
/*     */     }
/* 539 */     else if (CommonValidator.validateData(value, 
/* 540 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[17]))
/*     */     {
/* 542 */       String reqLength = 
/* 543 */         CommonValidator.validateLength(value, 20, 20);
/*     */       
/* 545 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/* 547 */         errorObj = new ErrorObject(
/* 548 */           SubmissionErrorCodes.ERROR_DATAFIELD21_HASHTOTALCREDITAMOUNT
/* 549 */           .getErrorCode(), 
/* 550 */           SubmissionErrorCodes.ERROR_DATAFIELD21_HASHTOTALCREDITAMOUNT
/* 551 */           .getErrorDescription() + 
/* 552 */           "\n" + 
/* 553 */           "RecordType:" + 
/* 554 */           "TFS" + 
/* 555 */           "|" + 
/* 556 */           "RecordNumber:" + 
/* 557 */           recordNumber + 
/* 558 */           "|" + 
/* 559 */           "Hash Total Credit Amount" + 
/* 560 */           "|" + 
/* 561 */           "This field length Cannot be greater than 20");
/* 562 */         errorCodes.add(errorObj);
/*     */       }
/*     */       else {
/* 565 */         long tabCreditAmt = totalCreditAmount(settlementReqBean);
/* 566 */         long hsahCreditAmt = Long.parseLong(value);
/*     */         
/* 568 */         if (tabCreditAmt != hsahCreditAmt) {
/* 569 */           errorObj = new ErrorObject(
/* 570 */             SubmissionErrorCodes.ERROR_DATAFIELD22_HASHTOTALCREDITAMOUNT
/* 571 */             .getErrorCode(), 
/* 572 */             SubmissionErrorCodes.ERROR_DATAFIELD22_HASHTOTALCREDITAMOUNT
/* 573 */             .getErrorDescription() + 
/* 574 */             "\n" + 
/* 575 */             "RecordType:" + 
/* 576 */             "TFS" + 
/* 577 */             "|" + 
/* 578 */             "RecordNumber:" + 
/* 579 */             recordNumber + 
/* 580 */             "|" + 
/* 581 */             "Hash Total Credit Amount" + 
/* 582 */             "|" + 
/* 583 */             SubmissionErrorCodes.ERROR_DATAFIELD22_HASHTOTALCREDITAMOUNT
/* 584 */             .getErrorDescription());
/* 585 */           errorCodes.add(errorObj);
/*     */         }
/*     */       }
/*     */     }
/*     */     else {
/* 590 */       errorObj = new ErrorObject(
/* 591 */         SubmissionErrorCodes.ERROR_DATAFIELD21_HASHTOTALCREDITAMOUNT
/* 592 */         .getErrorCode(), 
/* 593 */         SubmissionErrorCodes.ERROR_DATAFIELD21_HASHTOTALCREDITAMOUNT
/* 594 */         .getErrorDescription() + 
/* 595 */         "\n" + 
/* 596 */         "RecordType:" + 
/* 597 */         "TFS" + 
/* 598 */         "|" + 
/* 599 */         "RecordNumber:" + 
/* 600 */         recordNumber + 
/* 601 */         "|" + 
/* 602 */         "Hash Total Credit Amount" + 
/* 603 */         "|" + 
/* 604 */         "This field can only be Numeric");
/* 605 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateHashTotalAmount(String value, SettlementRequestDataObject settlementReqBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 624 */     ErrorObject errorObj = null;
/*     */     
/* 626 */     if (CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 628 */       errorObj = new ErrorObject(
/* 629 */         SubmissionErrorCodes.ERROR_DATAFIELD24_HASH_TOTAL_AMOUNT
/* 630 */         .getErrorCode(), 
/* 631 */         SubmissionErrorCodes.ERROR_DATAFIELD24_HASH_TOTAL_AMOUNT
/* 632 */         .getErrorDescription() + 
/* 633 */         "\n" + 
/* 634 */         "RecordType:" + 
/* 635 */         "TFS" + 
/* 636 */         "|" + 
/* 637 */         "RecordNumber:" + 
/* 638 */         recordNumber + 
/* 639 */         "|" + 
/* 640 */         "HashTotalAmount" + 
/* 641 */         "|" + 
/* 642 */         "This field is mandatory and cannot be empty");
/* 643 */       errorCodes.add(errorObj);
/*     */     }
/* 645 */     else if (CommonValidator.validateData(value, 
/* 646 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[19]))
/*     */     {
/* 648 */       String reqLength = 
/* 649 */         CommonValidator.validateLength(value, 20, 20);
/*     */       
/* 651 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/* 653 */         errorObj = new ErrorObject(
/* 654 */           SubmissionErrorCodes.ERROR_DATAFIELD23_HASH_TOTAL_AMOUNT
/* 655 */           .getErrorCode(), 
/* 656 */           SubmissionErrorCodes.ERROR_DATAFIELD23_HASH_TOTAL_AMOUNT
/* 657 */           .getErrorDescription() + 
/* 658 */           "\n" + 
/* 659 */           "RecordType:" + 
/* 660 */           "TFS" + 
/* 661 */           "|" + 
/* 662 */           "RecordNumber:" + 
/* 663 */           recordNumber + 
/* 664 */           "|" + 
/* 665 */           "HashTotalAmount" + 
/* 666 */           "|" + 
/* 667 */           "This field length Cannot be greater than 20");
/* 668 */         errorCodes.add(errorObj);
/*     */       } else {
/* 670 */         long totTabAmount = totalTabAmount(settlementReqBean);
/* 671 */         long hashTotalAmt = Long.parseLong(value);
/*     */         
/* 673 */         if (hashTotalAmt != totTabAmount)
/*     */         {
/* 675 */           errorObj = new ErrorObject(
/* 676 */             SubmissionErrorCodes.ERROR_DATAFIELD24_HASH_TOTAL_AMOUNT
/* 677 */             .getErrorCode(), 
/* 678 */             SubmissionErrorCodes.ERROR_DATAFIELD24_HASH_TOTAL_AMOUNT
/* 679 */             .getErrorDescription() + 
/* 680 */             "\n" + 
/* 681 */             "RecordType:" + 
/* 682 */             "TFS" + 
/* 683 */             "|" + 
/* 684 */             "RecordNumber:" + 
/* 685 */             recordNumber + 
/* 686 */             "|" + 
/* 687 */             "HashTotalAmount" + 
/* 688 */             "|" + 
/* 689 */             SubmissionErrorCodes.ERROR_DATAFIELD24_HASH_TOTAL_AMOUNT
/* 690 */             .getErrorDescription());
/* 691 */           errorCodes.add(errorObj);
/*     */         }
/*     */       }
/*     */     }
/*     */     else {
/* 696 */       errorObj = new ErrorObject(
/* 697 */         SubmissionErrorCodes.ERROR_DATAFIELD23_HASH_TOTAL_AMOUNT
/* 698 */         .getErrorCode(), 
/* 699 */         SubmissionErrorCodes.ERROR_DATAFIELD23_HASH_TOTAL_AMOUNT
/* 700 */         .getErrorDescription() + 
/* 701 */         "\n" + 
/* 702 */         "RecordType:" + 
/* 703 */         "TFS" + 
/* 704 */         "|" + 
/* 705 */         "RecordNumber:" + 
/* 706 */         recordNumber + 
/* 707 */         "|" + 
/* 708 */         "HashTotalAmount" + 
/* 709 */         "|" + 
/* 710 */         "This field can only be Numeric");
/* 711 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static int noOfTABDebits(SettlementRequestDataObject settlementReqBean)
/*     */   {
/* 727 */     int totTABDebits = 0;
/*     */     
/* 729 */     if (!settlementReqBean.getTransactionTBTSpecificType().isEmpty())
/*     */     {
/* 731 */       Iterator localIterator1 = settlementReqBean.getTransactionTBTSpecificType().iterator();
/* 730 */       while (localIterator1.hasNext()) {
/* 731 */         TransactionTBTSpecificBean transactionTBTSpecificType = (TransactionTBTSpecificBean)localIterator1.next();
/*     */         
/*     */ 
/* 734 */         if (!transactionTBTSpecificType.getTransactionAdviceBasicType().isEmpty())
/*     */         {
/* 736 */           Iterator localIterator2 = transactionTBTSpecificType.getTransactionAdviceBasicType().iterator();
/* 735 */           while (localIterator2.hasNext()) {
/* 736 */             TransactionAdviceBasicBean transactionAdviceBasicType = (TransactionAdviceBasicBean)localIterator2.next();
/*     */             
/*     */ 
/* 739 */             if ((ProcessingCode.DEBIT.getProcessingCode().equalsIgnoreCase(
/* 740 */               transactionAdviceBasicType
/* 741 */               .getProcessingCode())) || 
/*     */               
/* 743 */               (ProcessingCode.CREDIT_REVERSALS.getProcessingCode().equalsIgnoreCase(
/* 744 */               transactionAdviceBasicType
/* 745 */               .getProcessingCode())) || 
/*     */               
/* 747 */               (ProcessingCode.BONUS_PAY_DEBIT.getProcessingCode().equalsIgnoreCase(
/* 748 */               transactionAdviceBasicType
/* 749 */               .getProcessingCode())) || 
/*     */               
/* 751 */               (ProcessingCode.BONUS_PAY_CREDIT_REVERSAL.getProcessingCode().equalsIgnoreCase(
/* 752 */               transactionAdviceBasicType
/* 753 */               .getProcessingCode())))
/*     */             {
/* 755 */               totTABDebits++;
/*     */             }
/*     */           }
/*     */         }
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 763 */     return totTABDebits;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static int noOfTABCredits(SettlementRequestDataObject settlementReqBean)
/*     */   {
/* 777 */     int totTABCredits = 0;
/*     */     
/* 779 */     if (!settlementReqBean.getTransactionTBTSpecificType().isEmpty())
/*     */     {
/* 781 */       Iterator localIterator1 = settlementReqBean.getTransactionTBTSpecificType().iterator();
/* 780 */       while (localIterator1.hasNext()) {
/* 781 */         TransactionTBTSpecificBean transactionTBTSpecificType = (TransactionTBTSpecificBean)localIterator1.next();
/*     */         
/*     */ 
/* 784 */         if (!transactionTBTSpecificType.getTransactionAdviceBasicType().isEmpty())
/*     */         {
/* 786 */           Iterator localIterator2 = transactionTBTSpecificType.getTransactionAdviceBasicType().iterator();
/* 785 */           while (localIterator2.hasNext()) {
/* 786 */             TransactionAdviceBasicBean transactionAdviceBasicType = (TransactionAdviceBasicBean)localIterator2.next();
/*     */             
/*     */ 
/* 789 */             if ((ProcessingCode.CREDIT.getProcessingCode().equalsIgnoreCase(
/* 790 */               transactionAdviceBasicType
/* 791 */               .getProcessingCode())) || 
/*     */               
/* 793 */               (ProcessingCode.DEBIT_REVERSALS.getProcessingCode().equalsIgnoreCase(
/* 794 */               transactionAdviceBasicType
/* 795 */               .getProcessingCode())) || 
/*     */               
/* 797 */               (ProcessingCode.BONUS_PAY_CREDIT.getProcessingCode().equalsIgnoreCase(
/* 798 */               transactionAdviceBasicType
/* 799 */               .getProcessingCode())) || 
/*     */               
/* 801 */               (ProcessingCode.BONUS_PAY_DEBIT_REVERSAL.getProcessingCode().equalsIgnoreCase(
/* 802 */               transactionAdviceBasicType
/* 803 */               .getProcessingCode())))
/*     */             {
/* 805 */               totTABCredits++;
/*     */             }
/*     */           }
/*     */         }
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/* 814 */     return totTABCredits;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static long totalDebitAmount(SettlementRequestDataObject settlementReqBean)
/*     */   {
/* 828 */     long totalDbtAmt = 0L;
/*     */     
/* 830 */     if (!settlementReqBean.getTransactionTBTSpecificType().isEmpty())
/*     */     {
/* 832 */       Iterator localIterator1 = settlementReqBean.getTransactionTBTSpecificType().iterator();
/* 831 */       while (localIterator1.hasNext()) {
/* 832 */         TransactionTBTSpecificBean transactionTBTSpecificType = (TransactionTBTSpecificBean)localIterator1.next();
/*     */         
/*     */ 
/* 835 */         if (!transactionTBTSpecificType.getTransactionAdviceBasicType().isEmpty())
/*     */         {
/* 837 */           Iterator localIterator2 = transactionTBTSpecificType.getTransactionAdviceBasicType().iterator();
/* 836 */           while (localIterator2.hasNext()) {
/* 837 */             TransactionAdviceBasicBean transactionAdviceBasicType = (TransactionAdviceBasicBean)localIterator2.next();
/*     */             
/*     */ 
/* 840 */             if ((ProcessingCode.DEBIT.getProcessingCode().equalsIgnoreCase(
/* 841 */               transactionAdviceBasicType
/* 842 */               .getProcessingCode())) || 
/*     */               
/* 844 */               (ProcessingCode.CREDIT_REVERSALS.getProcessingCode().equalsIgnoreCase(
/* 845 */               transactionAdviceBasicType
/* 846 */               .getProcessingCode())) || 
/*     */               
/* 848 */               (ProcessingCode.BONUS_PAY_DEBIT.getProcessingCode().equalsIgnoreCase(
/* 849 */               transactionAdviceBasicType
/* 850 */               .getProcessingCode())) || 
/*     */               
/* 852 */               (ProcessingCode.BONUS_PAY_CREDIT_REVERSAL.getProcessingCode().equalsIgnoreCase(
/* 853 */               transactionAdviceBasicType
/* 854 */               .getProcessingCode())))
/*     */             {
/*     */ 
/* 857 */               totalDbtAmt = totalDbtAmt + Long.parseLong(transactionAdviceBasicType
/* 858 */                 .getTransactionAmount());
/*     */             }
/*     */           }
/*     */         }
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 866 */     return totalDbtAmt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static long totalCreditAmount(SettlementRequestDataObject settlementReqBean)
/*     */   {
/* 879 */     long totalCreditAmt = 0L;
/*     */     
/* 881 */     if (!settlementReqBean.getTransactionTBTSpecificType().isEmpty())
/*     */     {
/* 883 */       Iterator localIterator1 = settlementReqBean.getTransactionTBTSpecificType().iterator();
/* 882 */       while (localIterator1.hasNext()) {
/* 883 */         TransactionTBTSpecificBean transactionTBTSpecificType = (TransactionTBTSpecificBean)localIterator1.next();
/*     */         
/*     */ 
/* 886 */         if (!transactionTBTSpecificType.getTransactionAdviceBasicType().isEmpty())
/*     */         {
/* 888 */           Iterator localIterator2 = transactionTBTSpecificType.getTransactionAdviceBasicType().iterator();
/* 887 */           while (localIterator2.hasNext()) {
/* 888 */             TransactionAdviceBasicBean transactionAdviceBasicType = (TransactionAdviceBasicBean)localIterator2.next();
/*     */             
/*     */ 
/* 891 */             if ((ProcessingCode.CREDIT.getProcessingCode().equalsIgnoreCase(
/* 892 */               transactionAdviceBasicType
/* 893 */               .getProcessingCode())) || 
/*     */               
/* 895 */               (ProcessingCode.DEBIT_REVERSALS.getProcessingCode().equalsIgnoreCase(
/* 896 */               transactionAdviceBasicType
/* 897 */               .getProcessingCode())) || 
/*     */               
/* 899 */               (ProcessingCode.BONUS_PAY_CREDIT.getProcessingCode().equalsIgnoreCase(
/* 900 */               transactionAdviceBasicType
/* 901 */               .getProcessingCode())) || 
/*     */               
/* 903 */               (ProcessingCode.BONUS_PAY_DEBIT_REVERSAL.getProcessingCode().equalsIgnoreCase(
/* 904 */               transactionAdviceBasicType
/* 905 */               .getProcessingCode())))
/*     */             {
/*     */ 
/* 908 */               totalCreditAmt = totalCreditAmt + Long.parseLong(transactionAdviceBasicType
/* 909 */                 .getTransactionAmount());
/*     */             }
/*     */           }
/*     */         }
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 917 */     return totalCreditAmt;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static long totalTabAmount(SettlementRequestDataObject settlementReqBean)
/*     */   {
/* 930 */     long totalAmt = 0L;
/*     */     
/* 932 */     if (!settlementReqBean.getTransactionTBTSpecificType().isEmpty())
/*     */     {
/* 934 */       Iterator localIterator1 = settlementReqBean.getTransactionTBTSpecificType().iterator();
/* 933 */       while (localIterator1.hasNext()) {
/* 934 */         TransactionTBTSpecificBean transactionTBTSpecificType = (TransactionTBTSpecificBean)localIterator1.next();
/*     */         
/*     */ 
/* 937 */         if (!transactionTBTSpecificType.getTransactionAdviceBasicType().isEmpty())
/*     */         {
/* 939 */           Iterator localIterator2 = transactionTBTSpecificType.getTransactionAdviceBasicType().iterator();
/* 938 */           while (localIterator2.hasNext()) {
/* 939 */             TransactionAdviceBasicBean transactionAdviceBasicType = (TransactionAdviceBasicBean)localIterator2.next();
/*     */             
/*     */ 
/* 942 */             totalAmt = totalAmt + Long.parseLong(transactionAdviceBasicType
/* 943 */               .getTransactionAmount());
/*     */           }
/*     */         }
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 950 */     return totalAmt;
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\validator\TFSRecValidator.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */