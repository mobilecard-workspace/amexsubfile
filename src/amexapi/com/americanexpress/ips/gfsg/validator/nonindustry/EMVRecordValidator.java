/*      */ package com.americanexpress.ips.gfsg.validator.nonindustry;
/*      */ 
/*      */ import com.americanexpress.ips.gfsg.beans.ErrorObject;
/*      */ import com.americanexpress.ips.gfsg.beans.common.TransactionAdviceAddendumBean;
/*      */ import com.americanexpress.ips.gfsg.beans.common.TransactionAdviceBasicBean;
/*      */ import com.americanexpress.ips.gfsg.beans.common.taa.nonindustrytype.EMVBean;
/*      */ import com.americanexpress.ips.gfsg.beans.common.taa.nonindustrytype.IccSystemRelatedDataBean;
/*      */ import com.americanexpress.ips.gfsg.enumerations.SubmissionErrorCodes;
/*      */ import com.americanexpress.ips.gfsg.exceptions.SettlementException;
/*      */ import com.americanexpress.ips.gfsg.validator.CommonValidator;
/*      */ import com.americanexpress.ips.gfsg.validator.industry.TAADBRecordValidator;
/*      */ import java.util.List;
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ public class EMVRecordValidator
/*      */ {
/*      */   public static void validateEMVRecord(TransactionAdviceBasicBean transactionAdviceBasicType, TransactionAdviceAddendumBean transactionAdviceAddendumType, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*   38 */     EMVBean emvBean = (EMVBean)transactionAdviceAddendumType;
/*      */     
/*      */ 
/*   41 */     TAADBRecordValidator.validateRecordType(emvBean.getRecordType(), 
/*   42 */       errorCodes);
/*   43 */     TAADBRecordValidator.validateRecordNumber(emvBean.getRecordNumber(), 
/*   44 */       errorCodes);
/*   45 */     TAADBRecordValidator.validateTransactionIdentifier(emvBean
/*   46 */       .getTransactionIdentifier(), transactionAdviceBasicType, 
/*   47 */       errorCodes);
/*   48 */     TAADBRecordValidator.validateAddendaTypeCode(emvBean
/*   49 */       .getAddendaTypeCode(), errorCodes);
/*   50 */     validateEMVFormatType(emvBean.getEMVFormatType(), errorCodes);
/*      */     
/*   52 */     if (CommonValidator.isNullOrEmpty(emvBean.getICCSystemRelatedData()))
/*      */     {
/*      */ 
/*      */ 
/*      */ 
/*   57 */       if (emvBean.getIccSystemRelatedDataBean() != null)
/*      */       {
/*   59 */         if (Integer.parseInt(emvBean.getEMVFormatType()) == 0)
/*      */         {
/*   61 */           validatePcakedFormatICCSystemRelatedData(
/*   62 */             emvBean.getIccSystemRelatedDataBean(), errorCodes);
/*      */         }
/*   64 */         else if (Integer.parseInt(emvBean.getEMVFormatType()) == 1)
/*      */         {
/*   66 */           validateUnPcakedFormatICCSystemRelatedData(
/*   67 */             emvBean.getIccSystemRelatedDataBean(), errorCodes);
/*      */         }
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateEMVFormatType(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*   85 */     ErrorObject errorObj = null;
/*   86 */     if (CommonValidator.isNullOrEmpty(value))
/*      */     {
/*      */ 
/*   89 */       errorObj = new ErrorObject(
/*   90 */         SubmissionErrorCodes.ERROR_DATAFIELD404_EMV_FORMATTYPE);
/*   91 */       errorCodes.add(errorObj);
/*      */     }
/*   93 */     else if (CommonValidator.validateData(value, 
/*   94 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ů']))
/*      */     {
/*   96 */       String reqLength = CommonValidator.validateLength(value, 2, 2);
/*      */       
/*   98 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  100 */         errorObj = new ErrorObject(
/*  101 */           SubmissionErrorCodes.ERROR_DATAFIELD406_EMV_FORMATTYPE);
/*  102 */         errorCodes.add(errorObj);
/*      */ 
/*      */ 
/*      */       }
/*  106 */       else if (!value.equalsIgnoreCase("00"))
/*      */       {
/*  108 */         if (!value.equalsIgnoreCase("01"))
/*      */         {
/*  110 */           errorObj = new ErrorObject(
/*  111 */             SubmissionErrorCodes.ERROR_DATAFIELD404_EMV_FORMATTYPE);
/*  112 */           errorCodes.add(errorObj);
/*      */         }
/*      */       }
/*      */     } else {
/*  116 */       errorObj = new ErrorObject(
/*  117 */         SubmissionErrorCodes.ERROR_DATAFIELD405_EMV_FORMATTYPE);
/*  118 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validatePcakedFormatICCSystemRelatedData(IccSystemRelatedDataBean iccDataBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  136 */     validateHeaderVersionName(iccDataBean.getHeaderVersionName(), 
/*  137 */       errorCodes);
/*  138 */     validateHeaderVersionNumber(iccDataBean.getHeaderVersionNumber(), 
/*  139 */       errorCodes);
/*  140 */     validateApplicationCryptogram(iccDataBean.getApplicationCryptogram(), 
/*  141 */       errorCodes);
/*  142 */     validateIssuerApplicationData(iccDataBean.getIssuerApplicationData(), 
/*  143 */       errorCodes);
/*  144 */     validateUnPredictableNumber(iccDataBean.getUnPredictableNumber(), 
/*  145 */       errorCodes);
/*  146 */     validateApplicationTransactionCounter(iccDataBean
/*  147 */       .getApplicationTransactionCounter(), errorCodes);
/*  148 */     validateTerminalVerificationResults(iccDataBean
/*  149 */       .getTerminalVerificationResults(), errorCodes);
/*  150 */     validateApplicationTransactionDate(iccDataBean.getTransactionDate(), 
/*  151 */       errorCodes);
/*  152 */     validateApplicationTransactionType(iccDataBean.getTransactionType(), 
/*  153 */       errorCodes);
/*  154 */     validateAmountAuthorized(iccDataBean.getAmountAuthorized(), errorCodes);
/*  155 */     validateTerminalTransactionCurrencyCode(iccDataBean
/*  156 */       .getTerminalTransactionCurrencyCode(), errorCodes);
/*  157 */     validateTerminalCountryCode(iccDataBean.getTerminalCountryCode(), 
/*  158 */       errorCodes);
/*  159 */     validateApplicationInterCahngeProfile(iccDataBean
/*  160 */       .getApplicationInterCahngeProfile(), errorCodes);
/*  161 */     validateAmountOther(iccDataBean.getAmountOther(), errorCodes);
/*  162 */     validateApplicationPanSequenceNumber(iccDataBean
/*  163 */       .getApplicationPanSequenceNumber(), errorCodes);
/*  164 */     validateCryptogramInformationData(iccDataBean
/*  165 */       .getCryptogramInformationData(), errorCodes);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateHeaderVersionName(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  181 */     ErrorObject errorObj = null;
/*  182 */     if (CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  184 */       errorObj = new ErrorObject(
/*  185 */         SubmissionErrorCodes.ERROR_DATAFIELD502_HEADER_VERSIONNAME);
/*  186 */       errorCodes.add(errorObj);
/*      */     }
/*  188 */     else if (CommonValidator.validateData(value, 
/*  189 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ƛ']))
/*      */     {
/*  191 */       String reqLength = CommonValidator.validateLength(value, 4, 4);
/*      */       
/*  193 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  195 */         errorObj = new ErrorObject(
/*  196 */           SubmissionErrorCodes.ERROR_DATAFIELD503_HEADER_VERSIONNAME);
/*  197 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/*  200 */       errorObj = new ErrorObject(
/*  201 */         SubmissionErrorCodes.ERROR_DATAFIELD504_HEADER_VERSIONNAME);
/*  202 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateHeaderVersionNumber(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  219 */     ErrorObject errorObj = null;
/*  220 */     if (CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  222 */       errorObj = new ErrorObject(
/*  223 */         SubmissionErrorCodes.ERROR_DATAFIELD505_HEADER_VERSIONNUMBER);
/*  224 */       errorCodes.add(errorObj);
/*      */     }
/*  226 */     else if (CommonValidator.validateData(value, 
/*  227 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ɯ']))
/*      */     {
/*  229 */       String reqLength = CommonValidator.validateLength(value, 2, 2);
/*      */       
/*  231 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  233 */         errorObj = new ErrorObject(
/*  234 */           SubmissionErrorCodes.ERROR_DATAFIELD506_HEADER_VERSIONNUMBER);
/*  235 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/*  238 */       errorObj = new ErrorObject(
/*  239 */         SubmissionErrorCodes.ERROR_DATAFIELD507_HEADER_VERSIONNUMBER);
/*  240 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateApplicationCryptogram(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  257 */     ErrorObject errorObj = null;
/*  258 */     if (CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  260 */       errorObj = new ErrorObject(
/*  261 */         SubmissionErrorCodes.ERROR_DATAFIELD508_APPLICATION_CRYPTOGRAM);
/*  262 */       errorCodes.add(errorObj);
/*      */     }
/*  264 */     else if (CommonValidator.validateData(value, 
/*  265 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ɲ']))
/*      */     {
/*  267 */       String reqLength = CommonValidator.validateLength(value, 8, 8);
/*      */       
/*  269 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  271 */         errorObj = new ErrorObject(
/*  272 */           SubmissionErrorCodes.ERROR_DATAFIELD509_APPLICATION_CRYPTOGRAM);
/*  273 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/*  276 */       errorObj = new ErrorObject(
/*  277 */         SubmissionErrorCodes.ERROR_DATAFIELD510_APPLICATION_CRYPTOGRAM);
/*  278 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateIssuerApplicationData(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  295 */     ErrorObject errorObj = null;
/*  296 */     if (CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  298 */       errorObj = new ErrorObject(
/*  299 */         SubmissionErrorCodes.ERROR_DATAFIELD511_ISSUER_APPLICATION_DATA);
/*  300 */       errorCodes.add(errorObj);
/*      */     }
/*  302 */     else if (CommonValidator.validateData(value, 
/*  303 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ƞ']))
/*      */     {
/*  305 */       String reqLength = 
/*  306 */         CommonValidator.validateLength(value, 33, 33);
/*      */       
/*  308 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  310 */         errorObj = new ErrorObject(
/*  311 */           SubmissionErrorCodes.ERROR_DATAFIELD512_ISSUER_APPLICATION_DATA);
/*  312 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/*  315 */       errorObj = new ErrorObject(
/*  316 */         SubmissionErrorCodes.ERROR_DATAFIELD513_ISSUER_APPLICATION_DATA);
/*  317 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateUnPredictableNumber(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  334 */     ErrorObject errorObj = null;
/*  335 */     if (CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  337 */       errorObj = new ErrorObject(
/*  338 */         SubmissionErrorCodes.ERROR_DATAFIELD514_UNPREDICTABLE_NUMBER);
/*  339 */       errorCodes.add(errorObj);
/*      */     }
/*  341 */     else if (CommonValidator.validateData(value, 
/*  342 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ɵ']))
/*      */     {
/*  344 */       String reqLength = CommonValidator.validateLength(value, 4, 4);
/*      */       
/*  346 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  348 */         errorObj = new ErrorObject(
/*  349 */           SubmissionErrorCodes.ERROR_DATAFIELD515_UNPREDICTABLE_NUMBER);
/*  350 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/*  353 */       errorObj = new ErrorObject(
/*  354 */         SubmissionErrorCodes.ERROR_DATAFIELD516_UNPREDICTABLE_NUMBER);
/*  355 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateApplicationTransactionCounter(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  372 */     ErrorObject errorObj = null;
/*  373 */     if (CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  375 */       errorObj = new ErrorObject(
/*  376 */         SubmissionErrorCodes.ERROR_DATAFIELD517_APPLICATION_TRANSACTIONCOUNTER);
/*  377 */       errorCodes.add(errorObj);
/*      */     }
/*  379 */     else if (CommonValidator.validateData(value, 
/*  380 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ơ']))
/*      */     {
/*  382 */       String reqLength = CommonValidator.validateLength(value, 2, 2);
/*      */       
/*  384 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  386 */         errorObj = new ErrorObject(
/*  387 */           SubmissionErrorCodes.ERROR_DATAFIELD518_APPLICATION_TRANSACTIONCOUNTER);
/*  388 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/*  391 */       errorObj = new ErrorObject(
/*  392 */         SubmissionErrorCodes.ERROR_DATAFIELD519_APPLICATION_TRANSACTIONCOUNTER);
/*  393 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTerminalVerificationResults(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  410 */     ErrorObject errorObj = null;
/*  411 */     if (CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  413 */       errorObj = new ErrorObject(
/*  414 */         SubmissionErrorCodes.ERROR_DATAFIELD520_TERMINAL_VERIFICATIONRESULTS);
/*  415 */       errorCodes.add(errorObj);
/*      */     }
/*  417 */     else if (CommonValidator.validateData(value, 
/*  418 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ơ']))
/*      */     {
/*  420 */       String reqLength = CommonValidator.validateLength(value, 5, 5);
/*      */       
/*  422 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  424 */         errorObj = new ErrorObject(
/*  425 */           SubmissionErrorCodes.ERROR_DATAFIELD521_TERMINAL_VERIFICATIONRESULTS);
/*  426 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/*  429 */       errorObj = new ErrorObject(
/*  430 */         SubmissionErrorCodes.ERROR_DATAFIELD522_TERMINAL_VERIFICATIONRESULTS);
/*  431 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateApplicationTransactionDate(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  448 */     ErrorObject errorObj = null;
/*  449 */     if (CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  451 */       errorObj = new ErrorObject(
/*  452 */         SubmissionErrorCodes.ERROR_DATAFIELD523_TRANSACTION_DATE);
/*  453 */       errorCodes.add(errorObj);
/*      */     }
/*  455 */     else if (CommonValidator.validateData(value, 
/*  456 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ƣ']))
/*      */     {
/*  458 */       String reqLength = CommonValidator.validateLength(value, 3, 3);
/*      */       
/*  460 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  462 */         errorObj = new ErrorObject(
/*  463 */           SubmissionErrorCodes.ERROR_DATAFIELD524_TRANSACTION_DATE);
/*  464 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/*  467 */       errorObj = new ErrorObject(
/*  468 */         SubmissionErrorCodes.ERROR_DATAFIELD525_TRANSACTION_DATE);
/*  469 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateApplicationTransactionType(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  486 */     ErrorObject errorObj = null;
/*  487 */     if (CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  489 */       errorObj = new ErrorObject(
/*  490 */         SubmissionErrorCodes.ERROR_DATAFIELD526_TRANSACTION_TYPE);
/*  491 */       errorCodes.add(errorObj);
/*      */     }
/*  493 */     else if (CommonValidator.validateData(value, 
/*  494 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ƣ']))
/*      */     {
/*  496 */       String reqLength = CommonValidator.validateLength(value, 1, 1);
/*      */       
/*  498 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  500 */         errorObj = new ErrorObject(
/*  501 */           SubmissionErrorCodes.ERROR_DATAFIELD527_TRANSACTION_TYPE);
/*  502 */         errorCodes.add(errorObj);
/*      */ 
/*      */ 
/*      */ 
/*      */       }
/*  507 */       else if (!value.equalsIgnoreCase("00"))
/*      */       {
/*  509 */         if (!value.equalsIgnoreCase("01"))
/*      */         {
/*  511 */           if (!value.equalsIgnoreCase("20")) {
/*  512 */             errorObj = new ErrorObject(
/*  513 */               SubmissionErrorCodes.ERROR_DATAFIELD527_TRANSACTION_TYPE);
/*  514 */             errorCodes.add(errorObj);
/*      */           } }
/*      */       }
/*      */     } else {
/*  518 */       errorObj = new ErrorObject(
/*  519 */         SubmissionErrorCodes.ERROR_DATAFIELD528_TRANSACTION_TYPE);
/*  520 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateAmountAuthorized(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  537 */     ErrorObject errorObj = null;
/*  538 */     if (CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  540 */       errorObj = new ErrorObject(
/*  541 */         SubmissionErrorCodes.ERROR_DATAFIELD529_AMOUNT_AUTHORIZED);
/*  542 */       errorCodes.add(errorObj);
/*      */     }
/*  544 */     else if (CommonValidator.validateData(value, 
/*  545 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ƥ']))
/*      */     {
/*  547 */       String reqLength = CommonValidator.validateLength(value, 6, 6);
/*      */       
/*  549 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  551 */         errorObj = new ErrorObject(
/*  552 */           SubmissionErrorCodes.ERROR_DATAFIELD530_AMOUNT_AUTHORIZED);
/*  553 */         errorCodes.add(errorObj);
/*      */ 
/*      */ 
/*      */       }
/*  557 */       else if (Integer.parseInt(value) == 0)
/*      */       {
/*  559 */         errorObj = new ErrorObject(
/*  560 */           SubmissionErrorCodes.ERROR_DATAFIELD0530_AMOUNT_AUTHORIZED);
/*  561 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */     else {
/*  565 */       errorObj = new ErrorObject(
/*  566 */         SubmissionErrorCodes.ERROR_DATAFIELD531_AMOUNT_AUTHORIZED);
/*  567 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTerminalTransactionCurrencyCode(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  584 */     ErrorObject errorObj = null;
/*  585 */     if (CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  587 */       errorObj = new ErrorObject(
/*  588 */         SubmissionErrorCodes.ERROR_DATAFIELD532_TERMINALTRANSACTION_CURRENCYCODE);
/*  589 */       errorCodes.add(errorObj);
/*      */     }
/*  591 */     else if (CommonValidator.validateData(value, 
/*  592 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ƥ']))
/*      */     {
/*  594 */       String reqLength = CommonValidator.validateLength(value, 2, 2);
/*      */       
/*  596 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  598 */         errorObj = new ErrorObject(
/*  599 */           SubmissionErrorCodes.ERROR_DATAFIELD533_TERMINALTRANSACTION_CURRENCYCODE);
/*  600 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/*  603 */       errorObj = new ErrorObject(
/*  604 */         SubmissionErrorCodes.ERROR_DATAFIELD534_TERMINALTRANSACTION_CURRENCYCODE);
/*  605 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTerminalCountryCode(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  622 */     ErrorObject errorObj = null;
/*  623 */     if (CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  625 */       errorObj = new ErrorObject(
/*  626 */         SubmissionErrorCodes.ERROR_DATAFIELD535_TERMINAL_COUNTRY_CODE);
/*  627 */       errorCodes.add(errorObj);
/*      */     }
/*  629 */     else if (CommonValidator.validateData(value, 
/*  630 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ʀ']))
/*      */     {
/*  632 */       String reqLength = CommonValidator.validateLength(value, 2, 2);
/*      */       
/*  634 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  636 */         errorObj = new ErrorObject(
/*  637 */           SubmissionErrorCodes.ERROR_DATAFIELD536_TERMINAL_COUNTRY_CODE);
/*  638 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/*  641 */       errorObj = new ErrorObject(
/*  642 */         SubmissionErrorCodes.ERROR_DATAFIELD537_TERMINAL_COUNTRY_CODE);
/*  643 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateApplicationInterCahngeProfile(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  660 */     ErrorObject errorObj = null;
/*  661 */     if (CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  663 */       errorObj = new ErrorObject(
/*  664 */         SubmissionErrorCodes.ERROR_DATAFIELD538_APPLICATION_INTERCHANGEPROFILE);
/*  665 */       errorCodes.add(errorObj);
/*      */     }
/*  667 */     else if (CommonValidator.validateData(value, 
/*  668 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ƨ']))
/*      */     {
/*  670 */       String reqLength = CommonValidator.validateLength(value, 2, 2);
/*      */       
/*  672 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  674 */         errorObj = new ErrorObject(
/*  675 */           SubmissionErrorCodes.ERROR_DATAFIELD539_APPLICATION_INTERCHANGEPROFILE);
/*  676 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/*  679 */       errorObj = new ErrorObject(
/*  680 */         SubmissionErrorCodes.ERROR_DATAFIELD540_APPLICATION_INTERCHANGEPROFILE);
/*  681 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateAmountOther(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  698 */     ErrorObject errorObj = null;
/*  699 */     if (CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  701 */       errorObj = new ErrorObject(
/*  702 */         SubmissionErrorCodes.ERROR_DATAFIELD541_AMOUNT_OTHER);
/*  703 */       errorCodes.add(errorObj);
/*      */     }
/*  705 */     else if (CommonValidator.validateData(value, 
/*  706 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ƨ']))
/*      */     {
/*  708 */       String reqLength = CommonValidator.validateLength(value, 6, 6);
/*      */       
/*  710 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  712 */         errorObj = new ErrorObject(
/*  713 */           SubmissionErrorCodes.ERROR_DATAFIELD542_AMOUNT_OTHER);
/*  714 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/*  717 */       errorObj = new ErrorObject(
/*  718 */         SubmissionErrorCodes.ERROR_DATAFIELD543_AMOUNT_OTHER);
/*  719 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateApplicationPanSequenceNumber(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  736 */     ErrorObject errorObj = null;
/*  737 */     if (CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  739 */       errorObj = new ErrorObject(
/*  740 */         SubmissionErrorCodes.ERROR_DATAFIELD544_APPLICATION_PANSEQUENCENUMBER);
/*  741 */       errorCodes.add(errorObj);
/*      */     }
/*  743 */     else if (CommonValidator.validateData(value, 
/*  744 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ʃ']))
/*      */     {
/*  746 */       String reqLength = CommonValidator.validateLength(value, 1, 1);
/*      */       
/*  748 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  750 */         errorObj = new ErrorObject(
/*  751 */           SubmissionErrorCodes.ERROR_DATAFIELD545_APPLICATION_PANSEQUENCENUMBER);
/*  752 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/*  755 */       errorObj = new ErrorObject(
/*  756 */         SubmissionErrorCodes.ERROR_DATAFIELD546_APPLICATION_PANSEQUENCENUMBER);
/*  757 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateCryptogramInformationData(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  774 */     ErrorObject errorObj = null;
/*  775 */     if (CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  777 */       errorObj = new ErrorObject(
/*  778 */         SubmissionErrorCodes.ERROR_DATAFIELD547_CRYPTOGRAM_INFORMATIONDATA);
/*  779 */       errorCodes.add(errorObj);
/*      */     }
/*  781 */     else if (CommonValidator.validateData(value, 
/*  782 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ƪ']))
/*      */     {
/*  784 */       String reqLength = CommonValidator.validateLength(value, 1, 1);
/*      */       
/*  786 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  788 */         errorObj = new ErrorObject(
/*  789 */           SubmissionErrorCodes.ERROR_DATAFIELD548_CRYPTOGRAM_INFORMATIONDATA);
/*  790 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/*  793 */       errorObj = new ErrorObject(
/*  794 */         SubmissionErrorCodes.ERROR_DATAFIELD549_CRYPTOGRAM_INFORMATIONDATA);
/*  795 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateUnPcakedFormatICCSystemRelatedData(IccSystemRelatedDataBean iccDataBean, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  813 */     validateUnPackHeaderVersionName(iccDataBean.getHeaderVersionName(), 
/*  814 */       errorCodes);
/*  815 */     validateUnPackHeaderVersionNumber(iccDataBean.getHeaderVersionNumber(), 
/*  816 */       errorCodes);
/*  817 */     validateUnPackApplicationCryptogram(iccDataBean
/*  818 */       .getApplicationCryptogram(), errorCodes);
/*  819 */     validateUnPackIssuerApplicationData(iccDataBean
/*  820 */       .getIssuerApplicationData(), errorCodes);
/*  821 */     validateUnPackUnPredictableNumber(iccDataBean.getUnPredictableNumber(), 
/*  822 */       errorCodes);
/*  823 */     validateUnPackApplicationTransactionCounter(iccDataBean
/*  824 */       .getApplicationTransactionCounter(), errorCodes);
/*  825 */     validateUnPackTerminalVerificationResults(iccDataBean
/*  826 */       .getTerminalVerificationResults(), errorCodes);
/*  827 */     validateUnPackApplicationTransactionDate(iccDataBean
/*  828 */       .getTransactionDate(), errorCodes);
/*  829 */     validateUnPackApplicationTransactionType(iccDataBean
/*  830 */       .getTransactionType(), errorCodes);
/*  831 */     validateUnPackAmountAuthorized(iccDataBean.getAmountAuthorized(), 
/*  832 */       errorCodes);
/*  833 */     validateUnPackTerminalTransactionCurrencyCode(iccDataBean
/*  834 */       .getTerminalTransactionCurrencyCode(), errorCodes);
/*  835 */     validateUnPackTerminalCountryCode(iccDataBean.getTerminalCountryCode(), 
/*  836 */       errorCodes);
/*  837 */     validateUnPackApplicationInterCahngeProfile(iccDataBean
/*  838 */       .getApplicationInterCahngeProfile(), errorCodes);
/*  839 */     validateUnPackAmountOther(iccDataBean.getAmountOther(), errorCodes);
/*  840 */     validateUnPackApplicationPanSequenceNumber(iccDataBean
/*  841 */       .getApplicationPanSequenceNumber(), errorCodes);
/*  842 */     validateUnPackCryptogramInformationData(iccDataBean
/*  843 */       .getCryptogramInformationData(), errorCodes);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateUnPackHeaderVersionName(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  859 */     ErrorObject errorObj = null;
/*  860 */     if (CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  862 */       errorObj = new ErrorObject(
/*  863 */         SubmissionErrorCodes.ERROR_DATAFIELD502_HEADER_VERSIONNAME);
/*  864 */       errorCodes.add(errorObj);
/*      */     }
/*  866 */     else if (CommonValidator.validateData(value, 
/*  867 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ƭ']))
/*      */     {
/*  869 */       String reqLength = CommonValidator.validateLength(value, 4, 4);
/*      */       
/*  871 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  873 */         errorObj = new ErrorObject(
/*  874 */           SubmissionErrorCodes.ERROR_DATAFIELD503_HEADER_VERSIONNAME);
/*  875 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/*  878 */       errorObj = new ErrorObject(
/*  879 */         SubmissionErrorCodes.ERROR_DATAFIELD504_HEADER_VERSIONNAME);
/*  880 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateUnPackHeaderVersionNumber(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  897 */     ErrorObject errorObj = null;
/*  898 */     if (CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  900 */       errorObj = new ErrorObject(
/*  901 */         SubmissionErrorCodes.ERROR_DATAFIELD505_HEADER_VERSIONNUMBER);
/*  902 */       errorCodes.add(errorObj);
/*      */     }
/*  904 */     else if (CommonValidator.validateData(value, 
/*  905 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ƭ']))
/*      */     {
/*  907 */       String reqLength = CommonValidator.validateLength(value, 4, 4);
/*      */       
/*  909 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  911 */         errorObj = new ErrorObject(
/*  912 */           SubmissionErrorCodes.ERROR_DATAFIELD550_HEADER_VERSIONNUMBER);
/*  913 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/*  916 */       errorObj = new ErrorObject(
/*  917 */         SubmissionErrorCodes.ERROR_DATAFIELD551_HEADER_VERSIONNUMBER);
/*  918 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateUnPackApplicationCryptogram(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  935 */     ErrorObject errorObj = null;
/*  936 */     if (CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  938 */       errorObj = new ErrorObject(
/*  939 */         SubmissionErrorCodes.ERROR_DATAFIELD508_APPLICATION_CRYPTOGRAM);
/*  940 */       errorCodes.add(errorObj);
/*      */     }
/*  942 */     else if (CommonValidator.validateData(value, 
/*  943 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ʈ']))
/*      */     {
/*  945 */       String reqLength = 
/*  946 */         CommonValidator.validateLength(value, 16, 16);
/*      */       
/*  948 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  950 */         errorObj = new ErrorObject(
/*  951 */           SubmissionErrorCodes.ERROR_DATAFIELD552_APPLICATION_CRYPTOGRAM);
/*  952 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/*  955 */       errorObj = new ErrorObject(
/*  956 */         SubmissionErrorCodes.ERROR_DATAFIELD553_APPLICATION_CRYPTOGRAM);
/*  957 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateUnPackIssuerApplicationData(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  974 */     ErrorObject errorObj = null;
/*  975 */     if (CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  977 */       errorObj = new ErrorObject(
/*  978 */         SubmissionErrorCodes.ERROR_DATAFIELD511_ISSUER_APPLICATION_DATA);
/*  979 */       errorCodes.add(errorObj);
/*      */     }
/*  981 */     else if (CommonValidator.validateData(value, 
/*  982 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ư']))
/*      */     {
/*  984 */       String reqLength = 
/*  985 */         CommonValidator.validateLength(value, 64, 64);
/*      */       
/*  987 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  989 */         errorObj = new ErrorObject(
/*  990 */           SubmissionErrorCodes.ERROR_DATAFIELD554_ISSUER_APPLICATION_DATA);
/*  991 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/*  994 */       errorObj = new ErrorObject(
/*  995 */         SubmissionErrorCodes.ERROR_DATAFIELD555_ISSUER_APPLICATION_DATA);
/*  996 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateUnPackUnPredictableNumber(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1013 */     ErrorObject errorObj = null;
/* 1014 */     if (CommonValidator.isNullOrEmpty(value))
/*      */     {
/* 1016 */       errorObj = new ErrorObject(
/* 1017 */         SubmissionErrorCodes.ERROR_DATAFIELD556_UNPREDICTABLE_NUMBER);
/* 1018 */       errorCodes.add(errorObj);
/*      */     }
/* 1020 */     else if (CommonValidator.validateData(value, 
/* 1021 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ư']))
/*      */     {
/* 1023 */       String reqLength = CommonValidator.validateLength(value, 8, 8);
/*      */       
/* 1025 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/* 1027 */         errorObj = new ErrorObject(
/* 1028 */           SubmissionErrorCodes.ERROR_DATAFIELD557_UNPREDICTABLE_NUMBER);
/* 1029 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/* 1032 */       errorObj = new ErrorObject(
/* 1033 */         SubmissionErrorCodes.ERROR_DATAFIELD558_UNPREDICTABLE_NUMBER);
/* 1034 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateUnPackApplicationTransactionCounter(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1052 */     ErrorObject errorObj = null;
/* 1053 */     if (CommonValidator.isNullOrEmpty(value))
/*      */     {
/* 1055 */       errorObj = new ErrorObject(
/* 1056 */         SubmissionErrorCodes.ERROR_DATAFIELD559_APPLICATION_TRANSACTIONCOUNTER);
/* 1057 */       errorCodes.add(errorObj);
/*      */     }
/* 1059 */     else if (CommonValidator.validateData(value, 
/* 1060 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ʊ']))
/*      */     {
/* 1062 */       String reqLength = CommonValidator.validateLength(value, 4, 4);
/*      */       
/* 1064 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/* 1066 */         errorObj = new ErrorObject(
/* 1067 */           SubmissionErrorCodes.ERROR_DATAFIELD560_APPLICATION_TRANSACTIONCOUNTER);
/* 1068 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/* 1071 */       errorObj = new ErrorObject(
/* 1072 */         SubmissionErrorCodes.ERROR_DATAFIELD561_APPLICATION_TRANSACTIONCOUNTER);
/* 1073 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateUnPackTerminalVerificationResults(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1090 */     ErrorObject errorObj = null;
/* 1091 */     if (CommonValidator.isNullOrEmpty(value))
/*      */     {
/* 1093 */       errorObj = new ErrorObject(
/* 1094 */         SubmissionErrorCodes.ERROR_DATAFIELD562_TERMINAL_VERIFICATIONRESULTS);
/* 1095 */       errorCodes.add(errorObj);
/*      */     }
/* 1097 */     else if (CommonValidator.validateData(value, 
/* 1098 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ʋ']))
/*      */     {
/* 1100 */       String reqLength = 
/* 1101 */         CommonValidator.validateLength(value, 10, 10);
/*      */       
/* 1103 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/* 1105 */         errorObj = new ErrorObject(
/* 1106 */           SubmissionErrorCodes.ERROR_DATAFIELD563_TERMINAL_VERIFICATIONRESULTS);
/* 1107 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/* 1110 */       errorObj = new ErrorObject(
/* 1111 */         SubmissionErrorCodes.ERROR_DATAFIELD564_TERMINAL_VERIFICATIONRESULTS);
/* 1112 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateUnPackApplicationTransactionDate(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1129 */     ErrorObject errorObj = null;
/* 1130 */     if (CommonValidator.isNullOrEmpty(value))
/*      */     {
/* 1132 */       errorObj = new ErrorObject(
/* 1133 */         SubmissionErrorCodes.ERROR_DATAFIELD523_TRANSACTION_DATE);
/* 1134 */       errorCodes.add(errorObj);
/*      */     }
/* 1136 */     else if (CommonValidator.validateData(value, 
/* 1137 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ƴ']))
/*      */     {
/* 1139 */       String reqLength = CommonValidator.validateLength(value, 6, 6);
/*      */       
/* 1141 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/* 1143 */         errorObj = new ErrorObject(
/* 1144 */           SubmissionErrorCodes.ERROR_DATAFIELD565_TRANSACTION_DATE);
/* 1145 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/* 1148 */       errorObj = new ErrorObject(
/* 1149 */         SubmissionErrorCodes.ERROR_DATAFIELD566_TRANSACTION_DATE);
/* 1150 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateUnPackApplicationTransactionType(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1167 */     ErrorObject errorObj = null;
/* 1168 */     if (CommonValidator.isNullOrEmpty(value))
/*      */     {
/* 1170 */       errorObj = new ErrorObject(
/* 1171 */         SubmissionErrorCodes.ERROR_DATAFIELD526_TRANSACTION_TYPE);
/* 1172 */       errorCodes.add(errorObj);
/*      */     }
/* 1174 */     else if (CommonValidator.validateData(value, 
/* 1175 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ƴ']))
/*      */     {
/* 1177 */       String reqLength = CommonValidator.validateLength(value, 2, 2);
/*      */       
/* 1179 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/* 1181 */         errorObj = new ErrorObject(
/* 1182 */           SubmissionErrorCodes.ERROR_DATAFIELD567_TRANSACTION_TYPE);
/* 1183 */         errorCodes.add(errorObj);
/*      */ 
/*      */ 
/*      */ 
/*      */       }
/* 1188 */       else if (!value.equalsIgnoreCase("00"))
/*      */       {
/* 1190 */         if (!value.equalsIgnoreCase("01"))
/*      */         {
/* 1192 */           if (!value.equalsIgnoreCase("20")) {
/* 1193 */             errorObj = new ErrorObject(
/* 1194 */               SubmissionErrorCodes.ERROR_DATAFIELD527_TRANSACTION_TYPE);
/* 1195 */             errorCodes.add(errorObj);
/*      */           } }
/*      */       }
/*      */     } else {
/* 1199 */       errorObj = new ErrorObject(
/* 1200 */         SubmissionErrorCodes.ERROR_DATAFIELD568_TRANSACTION_TYPE);
/* 1201 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateUnPackAmountAuthorized(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1218 */     ErrorObject errorObj = null;
/* 1219 */     if (CommonValidator.isNullOrEmpty(value))
/*      */     {
/* 1221 */       errorObj = new ErrorObject(
/* 1222 */         SubmissionErrorCodes.ERROR_DATAFIELD569_AMOUNT_AUTHORIZED);
/* 1223 */       errorCodes.add(errorObj);
/*      */     }
/* 1225 */     else if (CommonValidator.validateData(value, 
/* 1226 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ƶ']))
/*      */     {
/* 1228 */       String reqLength = 
/* 1229 */         CommonValidator.validateLength(value, 12, 12);
/*      */       
/* 1231 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/* 1233 */         errorObj = new ErrorObject(
/* 1234 */           SubmissionErrorCodes.ERROR_DATAFIELD570_AMOUNT_AUTHORIZED);
/* 1235 */         errorCodes.add(errorObj);
/*      */ 
/*      */       }
/* 1238 */       else if (Integer.parseInt(value) == 0)
/*      */       {
/* 1240 */         errorObj = new ErrorObject(
/* 1241 */           SubmissionErrorCodes.ERROR_DATAFIELD0530_AMOUNT_AUTHORIZED);
/* 1242 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */     else {
/* 1246 */       errorObj = new ErrorObject(
/* 1247 */         SubmissionErrorCodes.ERROR_DATAFIELD531_AMOUNT_AUTHORIZED);
/* 1248 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateUnPackTerminalTransactionCurrencyCode(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1266 */     ErrorObject errorObj = null;
/* 1267 */     if (CommonValidator.isNullOrEmpty(value))
/*      */     {
/* 1269 */       errorObj = new ErrorObject(
/* 1270 */         SubmissionErrorCodes.ERROR_DATAFIELD532_TERMINALTRANSACTION_CURRENCYCODE);
/* 1271 */       errorCodes.add(errorObj);
/*      */     }
/* 1273 */     else if (CommonValidator.validateData(value, 
/* 1274 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ƶ']))
/*      */     {
/* 1276 */       String reqLength = CommonValidator.validateLength(value, 4, 4);
/*      */       
/* 1278 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/* 1280 */         errorObj = new ErrorObject(
/* 1281 */           SubmissionErrorCodes.ERROR_DATAFIELD570_TERMINALTRANSACTION_CURRENCYCODE);
/* 1282 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/* 1285 */       errorObj = new ErrorObject(
/* 1286 */         SubmissionErrorCodes.ERROR_DATAFIELD571_TERMINALTRANSACTION_CURRENCYCODE);
/* 1287 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateUnPackTerminalCountryCode(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1304 */     ErrorObject errorObj = null;
/* 1305 */     if (CommonValidator.isNullOrEmpty(value))
/*      */     {
/* 1307 */       errorObj = new ErrorObject(
/* 1308 */         SubmissionErrorCodes.ERROR_DATAFIELD535_TERMINAL_COUNTRY_CODE);
/* 1309 */       errorCodes.add(errorObj);
/*      */     }
/* 1311 */     else if (CommonValidator.validateData(value, 
/* 1312 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ʒ']))
/*      */     {
/* 1314 */       String reqLength = CommonValidator.validateLength(value, 4, 4);
/*      */       
/* 1316 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/* 1318 */         errorObj = new ErrorObject(
/* 1319 */           SubmissionErrorCodes.ERROR_DATAFIELD572_TERMINAL_COUNTRY_CODE);
/* 1320 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/* 1323 */       errorObj = new ErrorObject(
/* 1324 */         SubmissionErrorCodes.ERROR_DATAFIELD573_TERMINAL_COUNTRY_CODE);
/* 1325 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateUnPackApplicationInterCahngeProfile(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1343 */     ErrorObject errorObj = null;
/* 1344 */     if (CommonValidator.isNullOrEmpty(value))
/*      */     {
/* 1346 */       errorObj = new ErrorObject(
/* 1347 */         SubmissionErrorCodes.ERROR_DATAFIELD573_APPLICATION_INTERCHANGEPROFILE);
/* 1348 */       errorCodes.add(errorObj);
/*      */     }
/* 1350 */     else if (CommonValidator.validateData(value, 
/* 1351 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ƹ']))
/*      */     {
/* 1353 */       String reqLength = CommonValidator.validateLength(value, 4, 4);
/*      */       
/* 1355 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/* 1357 */         errorObj = new ErrorObject(
/* 1358 */           SubmissionErrorCodes.ERROR_DATAFIELD574_APPLICATION_INTERCHANGEPROFILE);
/* 1359 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/* 1362 */       errorObj = new ErrorObject(
/* 1363 */         SubmissionErrorCodes.ERROR_DATAFIELD575_APPLICATION_INTERCHANGEPROFILE);
/* 1364 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateUnPackAmountOther(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1381 */     ErrorObject errorObj = null;
/* 1382 */     if (CommonValidator.isNullOrEmpty(value))
/*      */     {
/* 1384 */       errorObj = new ErrorObject(
/* 1385 */         SubmissionErrorCodes.ERROR_DATAFIELD541_AMOUNT_OTHER);
/* 1386 */       errorCodes.add(errorObj);
/*      */     }
/* 1388 */     else if (CommonValidator.validateData(value, 
/* 1389 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ƹ']))
/*      */     {
/* 1391 */       String reqLength = 
/* 1392 */         CommonValidator.validateLength(value, 12, 12);
/*      */       
/* 1394 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/* 1396 */         errorObj = new ErrorObject(
/* 1397 */           SubmissionErrorCodes.ERROR_DATAFIELD576_AMOUNT_OTHER);
/* 1398 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/* 1401 */       errorObj = new ErrorObject(
/* 1402 */         SubmissionErrorCodes.ERROR_DATAFIELD577_AMOUNT_OTHER);
/* 1403 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateUnPackApplicationPanSequenceNumber(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1420 */     ErrorObject errorObj = null;
/* 1421 */     if (CommonValidator.isNullOrEmpty(value))
/*      */     {
/* 1423 */       errorObj = new ErrorObject(
/* 1424 */         SubmissionErrorCodes.ERROR_DATAFIELD578_APPLICATION_PANSEQUENCENUMBER);
/* 1425 */       errorCodes.add(errorObj);
/*      */     }
/* 1427 */     else if (CommonValidator.validateData(value, 
/* 1428 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ƺ']))
/*      */     {
/* 1430 */       String reqLength = CommonValidator.validateLength(value, 2, 2);
/*      */       
/* 1432 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/* 1434 */         errorObj = new ErrorObject(
/* 1435 */           SubmissionErrorCodes.ERROR_DATAFIELD579_APPLICATION_PANSEQUENCENUMBER);
/* 1436 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/* 1439 */       errorObj = new ErrorObject(
/* 1440 */         SubmissionErrorCodes.ERROR_DATAFIELD580_APPLICATION_PANSEQUENCENUMBER);
/* 1441 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateUnPackCryptogramInformationData(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1458 */     ErrorObject errorObj = null;
/* 1459 */     if (CommonValidator.isNullOrEmpty(value))
/*      */     {
/* 1461 */       errorObj = new ErrorObject(
/* 1462 */         SubmissionErrorCodes.ERROR_DATAFIELD581_CRYPTOGRAM_INFORMATIONDATA);
/* 1463 */       errorCodes.add(errorObj);
/*      */     }
/* 1465 */     else if (CommonValidator.validateData(value, 
/* 1466 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ƻ']))
/*      */     {
/* 1468 */       String reqLength = CommonValidator.validateLength(value, 2, 2);
/*      */       
/* 1470 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/* 1472 */         errorObj = new ErrorObject(
/* 1473 */           SubmissionErrorCodes.ERROR_DATAFIELD582_CRYPTOGRAM_INFORMATIONDATA);
/* 1474 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/* 1477 */       errorObj = new ErrorObject(
/* 1478 */         SubmissionErrorCodes.ERROR_DATAFIELD583_CRYPTOGRAM_INFORMATIONDATA);
/* 1479 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\validator\nonindustry\EMVRecordValidator.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */