/*     */ package com.americanexpress.ips.gfsg.validator.nonindustry;
/*     */ 
/*     */ import com.americanexpress.ips.gfsg.beans.ErrorObject;
/*     */ import com.americanexpress.ips.gfsg.beans.common.TransactionAdviceAddendumBean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.TransactionAdviceBasicBean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.taa.nonindustrytype.DeferredPaymentPlanBean;
/*     */ import com.americanexpress.ips.gfsg.enumerations.SubmissionErrorCodes;
/*     */ import com.americanexpress.ips.gfsg.exceptions.SettlementException;
/*     */ import com.americanexpress.ips.gfsg.validator.CommonValidator;
/*     */ import com.americanexpress.ips.gfsg.validator.industry.TAADBRecordValidator;
/*     */ import java.util.List;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class DPPRecordValidator
/*     */ {
/*     */   public static void validateDPPRecord(TransactionAdviceBasicBean transactionAdviceBasicType, TransactionAdviceAddendumBean transactionAdviceAddendumType, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/*  39 */     DeferredPaymentPlanBean dppBean = (DeferredPaymentPlanBean)transactionAdviceAddendumType;
/*     */     
/*     */ 
/*  42 */     TAADBRecordValidator.validateRecordType(dppBean.getRecordType(), 
/*  43 */       errorCodes);
/*  44 */     TAADBRecordValidator.validateRecordNumber(dppBean.getRecordNumber(), 
/*  45 */       errorCodes);
/*  46 */     TAADBRecordValidator.validateTransactionIdentifier(dppBean
/*  47 */       .getTransactionIdentifier(), transactionAdviceBasicType, 
/*  48 */       errorCodes);
/*  49 */     TAADBRecordValidator.validateAddendaTypeCode(dppBean
/*  50 */       .getAddendaTypeCode(), errorCodes);
/*  51 */     validateFullTransactionAmount(dppBean.getFullTransactionAmount(), 
/*  52 */       errorCodes);
/*  53 */     validateTypeOfPlanCode(dppBean.getTypeOfPlanCode(), errorCodes);
/*  54 */     validateNoOfInstallments(dppBean.getNoOfInstallments(), errorCodes);
/*  55 */     validateAmountOfInstallment(dppBean.getAmountOfInstallment(), 
/*  56 */       errorCodes);
/*  57 */     validateInstallmentNumber(dppBean.getInstallmentNumber(), errorCodes);
/*  58 */     validateContractNumber(dppBean.getContractNumber(), errorCodes);
/*  59 */     validatePaymentTypeCode1(dppBean.getPaymentTypeCode1(), errorCodes);
/*  60 */     validatePaymentTypeCode2(dppBean.getPaymentTypeCode2(), errorCodes);
/*  61 */     validatePaymentTypeCode3(dppBean.getPaymentTypeCode3(), errorCodes);
/*  62 */     validatePaymentTypeCode4(dppBean.getPaymentTypeCode4(), errorCodes);
/*  63 */     validatePaymentTypeCode5(dppBean.getPaymentTypeCode5(), errorCodes);
/*  64 */     validatePaymentTypeAmount1(dppBean.getPaymentTypeAmount1(), errorCodes);
/*  65 */     validatePaymentTypeAmount2(dppBean.getPaymentTypeAmount2(), errorCodes);
/*  66 */     validatePaymentTypeAmount3(dppBean.getPaymentTypeAmount3(), errorCodes);
/*  67 */     validatePaymentTypeAmount4(dppBean.getPaymentTypeAmount4(), errorCodes);
/*  68 */     validatePaymentTypeAmount5(dppBean.getPaymentTypeAmount5(), errorCodes);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateFullTransactionAmount(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/*  84 */     ErrorObject errorObj = null;
/*     */     
/*  86 */     if (CommonValidator.isNullOrEmpty(value))
/*     */     {
/*     */ 
/*  89 */       errorObj = new ErrorObject(
/*  90 */         SubmissionErrorCodes.ERROR_DATAFIELD445_FULLTRANSACTION_AMOUNT);
/*  91 */       errorCodes.add(errorObj);
/*     */     }
/*  93 */     else if (CommonValidator.validateData(value, 
/*  94 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ə']))
/*     */     {
/*  96 */       String reqLength = 
/*  97 */         CommonValidator.validateLength(value, 12, 12);
/*     */       
/*  99 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/* 101 */         errorObj = new ErrorObject(
/* 102 */           SubmissionErrorCodes.ERROR_DATAFIELD446_FULLTRANSACTION_AMOUNT);
/* 103 */         errorCodes.add(errorObj);
/*     */       }
/*     */     } else {
/* 106 */       errorObj = new ErrorObject(
/* 107 */         SubmissionErrorCodes.ERROR_DATAFIELD447_FULLTRANSACTION_AMOUNT);
/* 108 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateTypeOfPlanCode(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 125 */     ErrorObject errorObj = null;
/*     */     
/* 127 */     if (CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 129 */       errorObj = new ErrorObject(
/* 130 */         SubmissionErrorCodes.ERROR_DATAFIELD454_TYPEOF_PLANCODE);
/* 131 */       errorCodes.add(errorObj);
/*     */ 
/*     */     }
/* 134 */     else if (CommonValidator.validateData(value, 
/* 135 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ɛ']))
/*     */     {
/* 137 */       String reqLength = CommonValidator.validateLength(value, 4, 4);
/*     */       
/* 139 */       if (reqLength.equals("greaterThanMax")) {
/* 140 */         errorObj = new ErrorObject(
/* 141 */           SubmissionErrorCodes.ERROR_DATAFIELD455_TYPEOF_PLANCODE);
/* 142 */         errorCodes.add(errorObj);
/*     */ 
/*     */ 
/*     */       }
/* 146 */       else if (!"0003".equalsIgnoreCase(value))
/*     */       {
/* 148 */         if (!"0005".equalsIgnoreCase(value)) {
/* 149 */           errorObj = new ErrorObject(
/* 150 */             SubmissionErrorCodes.ERROR_DATAFIELD454_TYPEOF_PLANCODE);
/* 151 */           errorCodes.add(errorObj);
/*     */         }
/*     */       }
/*     */     }
/*     */     else {
/* 156 */       errorObj = new ErrorObject(
/* 157 */         SubmissionErrorCodes.ERROR_DATAFIELD456_TYPEOF_PLANCODE);
/* 158 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateNoOfInstallments(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 175 */     ErrorObject errorObj = null;
/*     */     
/* 177 */     if (CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 179 */       errorObj = new ErrorObject(
/* 180 */         SubmissionErrorCodes.ERROR_DATAFIELD461_NUMBEROF_INSTALLMENTS);
/* 181 */       errorCodes.add(errorObj);
/*     */     }
/* 183 */     else if (CommonValidator.validateData(value, 
/* 184 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ƒ']))
/*     */     {
/* 186 */       String reqLength = CommonValidator.validateLength(value, 4, 4);
/*     */       
/* 188 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/* 190 */         errorObj = new ErrorObject(
/* 191 */           SubmissionErrorCodes.ERROR_DATAFIELD460_NUMBEROF_INSTALLMENTS);
/* 192 */         errorCodes.add(errorObj);
/*     */ 
/*     */       }
/* 195 */       else if (Integer.parseInt(value) < 1) {
/* 196 */         errorObj = new ErrorObject(
/* 197 */           SubmissionErrorCodes.ERROR_DATAFIELD459_NUMBEROF_INSTALLMENTS);
/* 198 */         errorCodes.add(errorObj);
/*     */       }
/* 200 */       else if (Integer.parseInt(value) > 99)
/*     */       {
/* 202 */         errorObj = new ErrorObject(
/* 203 */           SubmissionErrorCodes.ERROR_DATAFIELD460_NUMBEROF_INSTALLMENTS);
/* 204 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */     else {
/* 208 */       errorObj = new ErrorObject(
/* 209 */         SubmissionErrorCodes.ERROR_DATAFIELD458_NUMBEROF_INSTALLMENTS);
/* 210 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateAmountOfInstallment(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 227 */     ErrorObject errorObj = null;
/*     */     
/* 229 */     if (CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 231 */       errorObj = new ErrorObject(
/* 232 */         SubmissionErrorCodes.ERROR_DATAFIELD462_AMOUNT_OF_INSTALLMENT);
/* 233 */       errorCodes.add(errorObj);
/*     */     }
/* 235 */     else if (CommonValidator.validateData(value, 
/* 236 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ɠ']))
/*     */     {
/* 238 */       String reqLength = 
/* 239 */         CommonValidator.validateLength(value, 12, 12);
/*     */       
/* 241 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/* 243 */         errorObj = new ErrorObject(
/* 244 */           SubmissionErrorCodes.ERROR_DATAFIELD462_AMOUNT_OF_INSTALLMENT);
/* 245 */         errorCodes.add(errorObj);
/*     */       }
/*     */     } else {
/* 248 */       errorObj = new ErrorObject(
/* 249 */         SubmissionErrorCodes.ERROR_DATAFIELD462_AMOUNT_OF_INSTALLMENT);
/* 250 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateInstallmentNumber(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 267 */     ErrorObject errorObj = null;
/*     */     
/* 269 */     if (CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 271 */       errorObj = new ErrorObject(
/* 272 */         SubmissionErrorCodes.ERROR_DATAFIELD468_INSTALLMENT_NUMBER);
/* 273 */       errorCodes.add(errorObj);
/*     */     }
/* 275 */     else if (CommonValidator.validateData(value, 
/* 276 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ɣ']))
/*     */     {
/* 278 */       String reqLength = CommonValidator.validateLength(value, 4, 4);
/*     */       
/* 280 */       if (reqLength.equals("greaterThanMax")) {
/* 281 */         errorObj = new ErrorObject(
/* 282 */           SubmissionErrorCodes.ERROR_DATAFIELD466_INSTALLMENT_NUMBER);
/* 283 */         errorCodes.add(errorObj);
/*     */ 
/*     */       }
/* 286 */       else if (Integer.parseInt(value) != 1) {
/* 287 */         errorObj = new ErrorObject(
/* 288 */           SubmissionErrorCodes.ERROR_DATAFIELD465_INSTALLMENT_NUMBER);
/* 289 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */     else
/*     */     {
/* 294 */       errorObj = new ErrorObject(
/* 295 */         SubmissionErrorCodes.ERROR_DATAFIELD467_INSTALLMENT_NUMBER);
/* 296 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateContractNumber(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 313 */     ErrorObject errorObj = null;
/*     */     
/* 315 */     if (CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 317 */       errorObj = new ErrorObject(
/* 318 */         SubmissionErrorCodes.ERROR_DATAFIELD469_CONTRACT_NUMBER);
/* 319 */       errorCodes.add(errorObj);
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/* 325 */       String reqLength = 
/* 326 */         CommonValidator.validateLength(value, 14, 14);
/*     */       
/* 328 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/* 330 */         errorObj = new ErrorObject(
/* 331 */           SubmissionErrorCodes.ERROR_DATAFIELD471_CONTRACT_NUMBER);
/* 332 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validatePaymentTypeCode1(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 354 */     ErrorObject errorObj = null;
/*     */     
/* 356 */     if (CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 358 */       errorObj = new ErrorObject(
/* 359 */         SubmissionErrorCodes.ERROR_DATAFIELD472_PAYMENT_TYPECODE1);
/* 360 */       errorCodes.add(errorObj);
/*     */     }
/* 362 */     else if (CommonValidator.validateData(value, 
/* 363 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ɩ']))
/*     */     {
/* 365 */       String reqLength = CommonValidator.validateLength(value, 2, 2);
/*     */       
/* 367 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/* 369 */         errorObj = new ErrorObject(
/* 370 */           SubmissionErrorCodes.ERROR_DATAFIELD474_PAYMENT_TYPECODE1);
/* 371 */         errorCodes.add(errorObj);
/*     */       }
/*     */     } else {
/* 374 */       errorObj = new ErrorObject(
/* 375 */         SubmissionErrorCodes.ERROR_DATAFIELD473_PAYMENT_TYPECODE1);
/* 376 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validatePaymentTypeCode2(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 393 */     ErrorObject errorObj = null;
/*     */     
/* 395 */     if (CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 397 */       errorObj = new ErrorObject(
/* 398 */         SubmissionErrorCodes.ERROR_DATAFIELD475_PAYMENT_TYPECODE2);
/* 399 */       errorCodes.add(errorObj);
/*     */     }
/* 401 */     else if (CommonValidator.validateData(value, 
/* 402 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ɩ']))
/*     */     {
/* 404 */       String reqLength = CommonValidator.validateLength(value, 2, 2);
/*     */       
/* 406 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/* 408 */         errorObj = new ErrorObject(
/* 409 */           SubmissionErrorCodes.ERROR_DATAFIELD477_PAYMENT_TYPECODE2);
/* 410 */         errorCodes.add(errorObj);
/*     */       }
/*     */     } else {
/* 413 */       errorObj = new ErrorObject(
/* 414 */         SubmissionErrorCodes.ERROR_DATAFIELD476_PAYMENT_TYPECODE2);
/* 415 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validatePaymentTypeCode3(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 432 */     ErrorObject errorObj = null;
/*     */     
/* 434 */     if (CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 436 */       errorObj = new ErrorObject(
/* 437 */         SubmissionErrorCodes.ERROR_DATAFIELD478_PAYMENT_TYPECODE3);
/* 438 */       errorCodes.add(errorObj);
/*     */     }
/* 440 */     else if (CommonValidator.validateData(value, 
/* 441 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ɩ']))
/*     */     {
/* 443 */       String reqLength = CommonValidator.validateLength(value, 2, 2);
/*     */       
/* 445 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/* 447 */         errorObj = new ErrorObject(
/* 448 */           SubmissionErrorCodes.ERROR_DATAFIELD480_PAYMENT_TYPECODE3);
/* 449 */         errorCodes.add(errorObj);
/*     */       }
/*     */     } else {
/* 452 */       errorObj = new ErrorObject(
/* 453 */         SubmissionErrorCodes.ERROR_DATAFIELD479_PAYMENT_TYPECODE3);
/* 454 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validatePaymentTypeCode4(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 471 */     ErrorObject errorObj = null;
/*     */     
/* 473 */     if (CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 475 */       errorObj = new ErrorObject(
/* 476 */         SubmissionErrorCodes.ERROR_DATAFIELD481_PAYMENT_TYPECODE4);
/* 477 */       errorCodes.add(errorObj);
/*     */     }
/* 479 */     else if (CommonValidator.validateData(value, 
/* 480 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ɩ']))
/*     */     {
/* 482 */       String reqLength = CommonValidator.validateLength(value, 2, 2);
/*     */       
/* 484 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/* 486 */         errorObj = new ErrorObject(
/* 487 */           SubmissionErrorCodes.ERROR_DATAFIELD483_PAYMENT_TYPECODE4);
/* 488 */         errorCodes.add(errorObj);
/*     */       }
/*     */     } else {
/* 491 */       errorObj = new ErrorObject(
/* 492 */         SubmissionErrorCodes.ERROR_DATAFIELD482_PAYMENT_TYPECODE4);
/* 493 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validatePaymentTypeCode5(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 510 */     ErrorObject errorObj = null;
/*     */     
/* 512 */     if (CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 514 */       errorObj = new ErrorObject(
/* 515 */         SubmissionErrorCodes.ERROR_DATAFIELD484_PAYMENT_TYPECODE5);
/* 516 */       errorCodes.add(errorObj);
/*     */     }
/* 518 */     else if (CommonValidator.validateData(value, 
/* 519 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ɩ']))
/*     */     {
/* 521 */       String reqLength = CommonValidator.validateLength(value, 2, 2);
/*     */       
/* 523 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/* 525 */         errorObj = new ErrorObject(
/* 526 */           SubmissionErrorCodes.ERROR_DATAFIELD486_PAYMENT_TYPECODE5);
/* 527 */         errorCodes.add(errorObj);
/*     */       }
/*     */     } else {
/* 530 */       errorObj = new ErrorObject(
/* 531 */         SubmissionErrorCodes.ERROR_DATAFIELD485_PAYMENT_TYPECODE5);
/* 532 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validatePaymentTypeAmount1(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 549 */     ErrorObject errorObj = null;
/*     */     
/* 551 */     if (CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 553 */       errorObj = new ErrorObject(
/* 554 */         SubmissionErrorCodes.ERROR_DATAFIELD487_PAYMENTTYPE_AMOUNT1);
/* 555 */       errorCodes.add(errorObj);
/*     */     }
/* 557 */     else if (CommonValidator.validateData(value, 
/* 558 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ƙ']))
/*     */     {
/* 560 */       String reqLength = 
/* 561 */         CommonValidator.validateLength(value, 12, 12);
/*     */       
/* 563 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/* 565 */         errorObj = new ErrorObject(
/* 566 */           SubmissionErrorCodes.ERROR_DATAFIELD488_PAYMENTTYPE_AMOUNT1);
/* 567 */         errorCodes.add(errorObj);
/*     */       }
/*     */     } else {
/* 570 */       errorObj = new ErrorObject(
/* 571 */         SubmissionErrorCodes.ERROR_DATAFIELD489_PAYMENTTYPE_AMOUNT1);
/* 572 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validatePaymentTypeAmount2(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 589 */     ErrorObject errorObj = null;
/*     */     
/* 591 */     if (CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 593 */       errorObj = new ErrorObject(
/* 594 */         SubmissionErrorCodes.ERROR_DATAFIELD490_PAYMENTTYPE_AMOUNT2);
/* 595 */       errorCodes.add(errorObj);
/*     */     }
/* 597 */     else if (CommonValidator.validateData(value, 
/* 598 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ƙ']))
/*     */     {
/* 600 */       String reqLength = 
/* 601 */         CommonValidator.validateLength(value, 12, 12);
/*     */       
/* 603 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/* 605 */         errorObj = new ErrorObject(
/* 606 */           SubmissionErrorCodes.ERROR_DATAFIELD491_PAYMENTTYPE_AMOUNT2);
/* 607 */         errorCodes.add(errorObj);
/*     */       }
/*     */     } else {
/* 610 */       errorObj = new ErrorObject(
/* 611 */         SubmissionErrorCodes.ERROR_DATAFIELD492_PAYMENTTYPE_AMOUNT2);
/* 612 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validatePaymentTypeAmount3(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 629 */     ErrorObject errorObj = null;
/*     */     
/* 631 */     if (CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 633 */       errorObj = new ErrorObject(
/* 634 */         SubmissionErrorCodes.ERROR_DATAFIELD493_PAYMENTTYPE_AMOUNT3);
/* 635 */       errorCodes.add(errorObj);
/*     */     }
/* 637 */     else if (CommonValidator.validateData(value, 
/* 638 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ƙ']))
/*     */     {
/* 640 */       String reqLength = 
/* 641 */         CommonValidator.validateLength(value, 12, 12);
/*     */       
/* 643 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/* 645 */         errorObj = new ErrorObject(
/* 646 */           SubmissionErrorCodes.ERROR_DATAFIELD494_PAYMENTTYPE_AMOUNT3);
/* 647 */         errorCodes.add(errorObj);
/*     */       }
/*     */     } else {
/* 650 */       errorObj = new ErrorObject(
/* 651 */         SubmissionErrorCodes.ERROR_DATAFIELD495_PAYMENTTYPE_AMOUNT3);
/* 652 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validatePaymentTypeAmount4(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 669 */     ErrorObject errorObj = null;
/*     */     
/* 671 */     if (CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 673 */       errorObj = new ErrorObject(
/* 674 */         SubmissionErrorCodes.ERROR_DATAFIELD496_PAYMENTTYPE_AMOUNT4);
/* 675 */       errorCodes.add(errorObj);
/*     */     }
/* 677 */     else if (CommonValidator.validateData(value, 
/* 678 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ƙ']))
/*     */     {
/* 680 */       String reqLength = 
/* 681 */         CommonValidator.validateLength(value, 12, 12);
/*     */       
/* 683 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/* 685 */         errorObj = new ErrorObject(
/* 686 */           SubmissionErrorCodes.ERROR_DATAFIELD497_PAYMENTTYPE_AMOUNT4);
/* 687 */         errorCodes.add(errorObj);
/*     */       }
/*     */     } else {
/* 690 */       errorObj = new ErrorObject(
/* 691 */         SubmissionErrorCodes.ERROR_DATAFIELD498_PAYMENTTYPE_AMOUNT4);
/* 692 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validatePaymentTypeAmount5(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 709 */     ErrorObject errorObj = null;
/*     */     
/* 711 */     if (CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 713 */       errorObj = new ErrorObject(
/* 714 */         SubmissionErrorCodes.ERROR_DATAFIELD499_PAYMENTTYPE_AMOUNT5);
/* 715 */       errorCodes.add(errorObj);
/*     */     }
/* 717 */     else if (CommonValidator.validateData(value, 
/* 718 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ƙ']))
/*     */     {
/* 720 */       String reqLength = 
/* 721 */         CommonValidator.validateLength(value, 12, 12);
/*     */       
/* 723 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/* 725 */         errorObj = new ErrorObject(
/* 726 */           SubmissionErrorCodes.ERROR_DATAFIELD500_PAYMENTTYPE_AMOUNT5);
/* 727 */         errorCodes.add(errorObj);
/*     */       }
/*     */     } else {
/* 730 */       errorObj = new ErrorObject(
/* 731 */         SubmissionErrorCodes.ERROR_DATAFIELD501_PAYMENTTYPE_AMOUNT5);
/* 732 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\validator\nonindustry\DPPRecordValidator.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */