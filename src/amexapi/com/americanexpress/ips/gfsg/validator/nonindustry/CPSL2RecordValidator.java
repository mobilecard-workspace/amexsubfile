/*      */ package com.americanexpress.ips.gfsg.validator.nonindustry;
/*      */ 
/*      */ import com.americanexpress.ips.gfsg.beans.ErrorObject;
/*      */ import com.americanexpress.ips.gfsg.beans.common.TransactionAdviceAddendumBean;
/*      */ import com.americanexpress.ips.gfsg.beans.common.TransactionAdviceBasicBean;
/*      */ import com.americanexpress.ips.gfsg.beans.common.taa.LocationDetailTAABean;
/*      */ import com.americanexpress.ips.gfsg.beans.common.taa.nonindustrytype.CPSLevel2Bean;
/*      */ import com.americanexpress.ips.gfsg.enumerations.CountryCode;
/*      */ import com.americanexpress.ips.gfsg.enumerations.CurrencyCode;
/*      */ import com.americanexpress.ips.gfsg.enumerations.SubmissionErrorCodes;
/*      */ import com.americanexpress.ips.gfsg.enumerations.TaxTypeCd;
/*      */ import com.americanexpress.ips.gfsg.exceptions.SettlementException;
/*      */ import com.americanexpress.ips.gfsg.validator.CommonValidator;
/*      */ import com.americanexpress.ips.gfsg.validator.industry.TAADBRecordValidator;
/*      */ import java.util.List;
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ public class CPSL2RecordValidator
/*      */ {
/*      */   public static void validateCPSL2Record(TransactionAdviceBasicBean transactionAdviceBasicType, TransactionAdviceAddendumBean transactionAdviceAddendumType, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*   43 */     CPSLevel2Bean cspl2Bean = (CPSLevel2Bean)transactionAdviceAddendumType;
/*      */     
/*      */ 
/*   46 */     LocationDetailTAABean locDetailTAA = transactionAdviceBasicType.getLocationDetailTAABean();
/*      */     
/*   48 */     TAADBRecordValidator.validateRecordType(cspl2Bean.getRecordType(), 
/*   49 */       errorCodes);
/*   50 */     TAADBRecordValidator.validateRecordNumber(cspl2Bean.getRecordNumber(), 
/*   51 */       errorCodes);
/*   52 */     TAADBRecordValidator.validateTransactionIdentifier(cspl2Bean
/*   53 */       .getTransactionIdentifier(), transactionAdviceBasicType, 
/*   54 */       errorCodes);
/*      */     
/*   56 */     validateAddendaTypeCode(transactionAdviceBasicType, transactionAdviceAddendumType, errorCodes);
/*   57 */     validateRequesterName(cspl2Bean.getRequesterName(), errorCodes);
/*   58 */     validateChargeDescription1(cspl2Bean.getChargeDescription1(), 
/*   59 */       errorCodes);
/*      */     
/*   61 */     validateChargeItemQuantity1(cspl2Bean.getChargeItemQuantity1(), 
/*   62 */       errorCodes);
/*   63 */     validateChargeItemAmount1(cspl2Bean.getChargeItemAmount1(), transactionAdviceBasicType.getTransactionCurrencyCode(), errorCodes);
/*   64 */     validateChargeItemAmount2(cspl2Bean.getChargeItemAmount2(), transactionAdviceBasicType.getTransactionCurrencyCode(), errorCodes);
/*   65 */     validateChargeItemAmount3(cspl2Bean.getChargeItemAmount3(), transactionAdviceBasicType.getTransactionCurrencyCode(), errorCodes);
/*   66 */     validateChargeItemAmount4(cspl2Bean.getChargeItemAmount4(), transactionAdviceBasicType.getTransactionCurrencyCode(), errorCodes);
/*      */     
/*   68 */     validateChargeItemQuantity2(cspl2Bean.getChargeItemQuantity2(), 
/*   69 */       errorCodes);
/*   70 */     validateChargeItemQuantity3(cspl2Bean.getChargeItemQuantity3(), 
/*   71 */       errorCodes);
/*   72 */     validateChargeItemQuantity4(cspl2Bean.getChargeItemQuantity4(), 
/*   73 */       errorCodes);
/*      */     
/*   75 */     validateChargeDescription2(cspl2Bean.getChargeDescription2(), 
/*   76 */       errorCodes);
/*   77 */     validateChargeDescription3(cspl2Bean.getChargeDescription3(), 
/*   78 */       errorCodes);
/*   79 */     validateChargeDescription4(cspl2Bean.getChargeDescription4(), 
/*   80 */       errorCodes);
/*      */     
/*   82 */     validateCardMemberReferenceNumber(cspl2Bean
/*   83 */       .getCardMemberReferenceNumber(), errorCodes);
/*   84 */     validateShipToPostalCode(cspl2Bean.getShipToPostalCode(), errorCodes, locDetailTAA.getLocationCountryCode(), cspl2Bean);
/*   85 */     validateTotalTaxAmount(cspl2Bean, errorCodes, locDetailTAA.getLocationCountryCode());
/*      */     
/*   87 */     validateTaxTypeCode(cspl2Bean.getTaxTypeCode(), errorCodes, transactionAdviceBasicType.getTransactionCurrencyCode(), locDetailTAA.getLocationCountryCode(), cspl2Bean);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateAddendaTypeCode(TransactionAdviceBasicBean transactionAdviceBasicType, TransactionAdviceAddendumBean transactionAddendumType, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  107 */     ErrorObject errorObj = null;
/*  108 */     String value = transactionAddendumType.getAddendaTypeCode();
/*      */     
/*      */ 
/*  111 */     if (CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  113 */       errorObj = new ErrorObject(
/*  114 */         SubmissionErrorCodes.ERROR_DATAFIELD68_ADDENDA_TYPECODE);
/*  115 */       errorCodes.add(errorObj);
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */     }
/*  156 */     else if (!CommonValidator.isValidAddendaTypeCode(value))
/*      */     {
/*  158 */       errorObj = new ErrorObject(
/*  159 */         SubmissionErrorCodes.ERROR_DATAFIELD70_ADDENDA_TYPECODE
/*  160 */         .getErrorCode(), 
/*  161 */         SubmissionErrorCodes.ERROR_DATAFIELD70_ADDENDA_TYPECODE
/*  162 */         .getErrorDescription() + 
/*  163 */         "\n" + 
/*  164 */         "RecordType:" + 
/*  165 */         "|" + 
/*  166 */         "RecordNumber:" + 
/*  167 */         "|" + 
/*  168 */         "AddendaTypeCode" + 
/*  169 */         "|" + 
/*  170 */         SubmissionErrorCodes.ERROR_DATAFIELD70_ADDENDA_TYPECODE
/*  171 */         .getErrorDescription());
/*  172 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateRequesterName(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  189 */     ErrorObject errorObj = null;
/*      */     
/*  191 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/*      */ 
/*      */ 
/*      */ 
/*  196 */       String reqLength = 
/*  197 */         CommonValidator.validateLength(value, 38, 38);
/*      */       
/*  199 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  201 */         errorObj = new ErrorObject(
/*  202 */           SubmissionErrorCodes.ERROR_DATAFIELD408_REQUESTER_NAME);
/*  203 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateChargeDescription1(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  225 */     ErrorObject errorObj = null;
/*      */     
/*  227 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  233 */       String reqLength = 
/*  234 */         CommonValidator.validateLength(value, 40, 40);
/*      */       
/*  236 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  238 */         errorObj = new ErrorObject(
/*  239 */           SubmissionErrorCodes.ERROR_DATAFIELD410_CHARGE_DESCRIPTION1);
/*  240 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateChargeDescription2(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  262 */     ErrorObject errorObj = null;
/*      */     
/*  264 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  270 */       String reqLength = 
/*  271 */         CommonValidator.validateLength(value, 40, 40);
/*      */       
/*  273 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  275 */         errorObj = new ErrorObject(
/*  276 */           SubmissionErrorCodes.ERROR_DATAFIELD412_CHARGE_DESCRIPTION2);
/*  277 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateChargeDescription3(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  299 */     ErrorObject errorObj = null;
/*      */     
/*  301 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  307 */       String reqLength = 
/*  308 */         CommonValidator.validateLength(value, 40, 40);
/*      */       
/*  310 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  312 */         errorObj = new ErrorObject(
/*  313 */           SubmissionErrorCodes.ERROR_DATAFIELD414_CHARGE_DESCRIPTION3);
/*  314 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateChargeDescription4(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  336 */     ErrorObject errorObj = null;
/*      */     
/*  338 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  344 */       String reqLength = 
/*  345 */         CommonValidator.validateLength(value, 40, 40);
/*      */       
/*  347 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  349 */         errorObj = new ErrorObject(
/*  350 */           SubmissionErrorCodes.ERROR_DATAFIELD416_CHARGE_DESCRIPTION4);
/*  351 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateChargeItemQuantity1(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  373 */     ErrorObject errorObj = null;
/*      */     
/*  375 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  377 */       if (CommonValidator.validateData(value, 
/*  378 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ž']))
/*      */       {
/*  380 */         String reqLength = CommonValidator.validateLength(value, 3, 3);
/*      */         
/*  382 */         if (reqLength.equals("greaterThanMax"))
/*      */         {
/*  384 */           errorObj = new ErrorObject(
/*  385 */             SubmissionErrorCodes.ERROR_DATAFIELD418_CHARGE_ITEMQUANTITY1);
/*  386 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/*  389 */         errorObj = new ErrorObject(
/*  390 */           SubmissionErrorCodes.ERROR_DATAFIELD417_CHARGE_ITEMQUANTITY1);
/*  391 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateChargeItemQuantity2(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  408 */     ErrorObject errorObj = null;
/*      */     
/*  410 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  412 */       if (CommonValidator.validateData(value, 
/*  413 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ž']))
/*      */       {
/*  415 */         String reqLength = CommonValidator.validateLength(value, 3, 3);
/*      */         
/*  417 */         if (reqLength.equals("greaterThanMax"))
/*      */         {
/*  419 */           errorObj = new ErrorObject(
/*  420 */             SubmissionErrorCodes.ERROR_DATAFIELD420_CHARGE_ITEMQUANTITY2);
/*  421 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/*  424 */         errorObj = new ErrorObject(
/*  425 */           SubmissionErrorCodes.ERROR_DATAFIELD419_CHARGE_ITEMQUANTITY2);
/*  426 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateChargeItemQuantity3(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  443 */     ErrorObject errorObj = null;
/*      */     
/*  445 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  447 */       if (CommonValidator.validateData(value, 
/*  448 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ž']))
/*      */       {
/*  450 */         String reqLength = CommonValidator.validateLength(value, 3, 3);
/*      */         
/*  452 */         if (reqLength.equals("greaterThanMax"))
/*      */         {
/*  454 */           errorObj = new ErrorObject(
/*  455 */             SubmissionErrorCodes.ERROR_DATAFIELD422_CHARGE_ITEMQUANTITY3);
/*  456 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/*  459 */         errorObj = new ErrorObject(
/*  460 */           SubmissionErrorCodes.ERROR_DATAFIELD421_CHARGE_ITEMQUANTITY3);
/*  461 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateChargeItemQuantity4(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  478 */     ErrorObject errorObj = null;
/*      */     
/*  480 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  482 */       if (CommonValidator.validateData(value, 
/*  483 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ž']))
/*      */       {
/*  485 */         String reqLength = CommonValidator.validateLength(value, 3, 3);
/*      */         
/*  487 */         if (reqLength.equals("greaterThanMax"))
/*      */         {
/*  489 */           errorObj = new ErrorObject(
/*  490 */             SubmissionErrorCodes.ERROR_DATAFIELD424_CHARGE_ITEMQUANTITY4);
/*  491 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/*  494 */         errorObj = new ErrorObject(
/*  495 */           SubmissionErrorCodes.ERROR_DATAFIELD423_CHARGE_ITEMQUANTITY4);
/*  496 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateChargeItemAmount1(String value, String transactionCurrencyCode, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  513 */     ErrorObject errorObj = null;
/*      */     
/*  515 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  517 */       if (CommonValidator.validateData(value, 
/*  518 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ſ']))
/*      */       {
/*  520 */         String reqLength = 
/*  521 */           CommonValidator.validateLength(value, 12, 1);
/*      */         
/*  523 */         if (reqLength.equals("greaterThanMax"))
/*      */         {
/*  525 */           errorObj = new ErrorObject(
/*  526 */             SubmissionErrorCodes.ERROR_DATAFIELD426_CHARGE_ITEMAMOUNT1);
/*  527 */           errorCodes.add(errorObj);
/*  528 */         } else if ((!CommonValidator.isNullOrEmpty(transactionCurrencyCode)) && 
/*  529 */           (!CommonValidator.isValidAmount(
/*  530 */           transactionCurrencyCode, value))) {
/*  531 */           errorObj = new ErrorObject(
/*  532 */             SubmissionErrorCodes.ERROR_DATAFIELD426_CHARGE_ITEMAMOUNT1);
/*  533 */           errorCodes.add(errorObj);
/*      */         }
/*      */       }
/*      */       else {
/*  537 */         errorObj = new ErrorObject(
/*  538 */           SubmissionErrorCodes.ERROR_DATAFIELD425_CHARGE_ITEMAMOUNT1);
/*  539 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateChargeItemAmount2(String value, String transactionCurrencyCode, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  556 */     ErrorObject errorObj = null;
/*      */     
/*  558 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  560 */       if (CommonValidator.validateData(value, 
/*  561 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ſ']))
/*      */       {
/*  563 */         String reqLength = 
/*  564 */           CommonValidator.validateLength(value, 12, 1);
/*      */         
/*  566 */         if (reqLength.equals("greaterThanMax"))
/*      */         {
/*  568 */           errorObj = new ErrorObject(
/*  569 */             SubmissionErrorCodes.ERROR_DATAFIELD428_CHARGE_ITEMAMOUNT2);
/*  570 */           errorCodes.add(errorObj);
/*  571 */         } else if ((!CommonValidator.isNullOrEmpty(transactionCurrencyCode)) && 
/*  572 */           (!CommonValidator.isValidAmount(
/*  573 */           transactionCurrencyCode, value))) {
/*  574 */           errorObj = new ErrorObject(
/*  575 */             SubmissionErrorCodes.ERROR_DATAFIELD428_CHARGE_ITEMAMOUNT2);
/*  576 */           errorCodes.add(errorObj);
/*      */         }
/*      */       }
/*      */       else {
/*  580 */         errorObj = new ErrorObject(
/*  581 */           SubmissionErrorCodes.ERROR_DATAFIELD427_CHARGE_ITEMAMOUNT2);
/*  582 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateChargeItemAmount3(String value, String transactionCurrencyCode, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  599 */     ErrorObject errorObj = null;
/*      */     
/*  601 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  603 */       if (CommonValidator.validateData(value, 
/*  604 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ſ']))
/*      */       {
/*  606 */         String reqLength = 
/*  607 */           CommonValidator.validateLength(value, 12, 1);
/*      */         
/*  609 */         if (reqLength.equals("greaterThanMax"))
/*      */         {
/*  611 */           errorObj = new ErrorObject(
/*  612 */             SubmissionErrorCodes.ERROR_DATAFIELD430_CHARGE_ITEMAMOUNT3);
/*  613 */           errorCodes.add(errorObj);
/*  614 */         } else if ((!CommonValidator.isNullOrEmpty(transactionCurrencyCode)) && 
/*  615 */           (!CommonValidator.isValidAmount(
/*  616 */           transactionCurrencyCode, value))) {
/*  617 */           errorObj = new ErrorObject(
/*  618 */             SubmissionErrorCodes.ERROR_DATAFIELD430_CHARGE_ITEMAMOUNT3);
/*  619 */           errorCodes.add(errorObj);
/*      */         }
/*      */       }
/*      */       else {
/*  623 */         errorObj = new ErrorObject(
/*  624 */           SubmissionErrorCodes.ERROR_DATAFIELD429_CHARGE_ITEMAMOUNT3);
/*  625 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateChargeItemAmount4(String value, String transactionCurrencyCode, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  642 */     ErrorObject errorObj = null;
/*      */     
/*  644 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  646 */       if (CommonValidator.validateData(value, 
/*  647 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['ſ']))
/*      */       {
/*  649 */         String reqLength = 
/*  650 */           CommonValidator.validateLength(value, 12, 1);
/*      */         
/*  652 */         if (reqLength.equals("greaterThanMax"))
/*      */         {
/*  654 */           errorObj = new ErrorObject(
/*  655 */             SubmissionErrorCodes.ERROR_DATAFIELD432_CHARGE_ITEMAMOUNT4);
/*  656 */           errorCodes.add(errorObj);
/*  657 */         } else if ((!CommonValidator.isNullOrEmpty(transactionCurrencyCode)) && 
/*  658 */           (!CommonValidator.isValidAmount(
/*  659 */           transactionCurrencyCode, value))) {
/*  660 */           errorObj = new ErrorObject(
/*  661 */             SubmissionErrorCodes.ERROR_DATAFIELD432_CHARGE_ITEMAMOUNT4);
/*  662 */           errorCodes.add(errorObj);
/*      */         }
/*      */       }
/*      */       else {
/*  666 */         errorObj = new ErrorObject(
/*  667 */           SubmissionErrorCodes.ERROR_DATAFIELD431_CHARGE_ITEMAMOUNT4);
/*  668 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateCardMemberReferenceNumber(String value, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  685 */     ErrorObject errorObj = null;
/*      */     
/*  687 */     if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  693 */       String reqLength = 
/*  694 */         CommonValidator.validateLength(value, 20, 20);
/*      */       
/*  696 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  698 */         errorObj = new ErrorObject(
/*  699 */           SubmissionErrorCodes.ERROR_DATAFIELD434_CARDMEMBER_REFERENCENUMBER);
/*  700 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateShipToPostalCode(String value, List<ErrorObject> errorCodes, String countryCode, CPSLevel2Bean cspl2Bean)
/*      */     throws SettlementException
/*      */   {
/*  722 */     ErrorObject errorObj = null;
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  728 */     if ((!CommonValidator.isNullOrEmpty(countryCode)) && (isValidCountryCodeForCPL2TAA(countryCode)))
/*      */     {
/*  730 */       if (CommonValidator.isNullOrEmpty(value))
/*      */       {
/*  732 */         errorObj = new ErrorObject(
/*  733 */           SubmissionErrorCodes.ERROR_DATAFIELD435_SHIPTO_POSTALCODE.getErrorCode(), 
/*  734 */           SubmissionErrorCodes.ERROR_DATAFIELD435_SHIPTO_POSTALCODE
/*  735 */           .getErrorDescription() + 
/*  736 */           "\n" + 
/*  737 */           "RecordType:" + 
/*  738 */           cspl2Bean.getRecordType() + 
/*  739 */           "|" + 
/*  740 */           "RecordNumber:" + 
/*  741 */           cspl2Bean.getRecordNumber() + 
/*  742 */           "|" + 
/*  743 */           "ShipToPostalCode" + 
/*  744 */           "|" + 
/*  745 */           "This field is mandatory and cannot be empty");
/*  746 */         errorCodes.add(errorObj);
/*      */ 
/*      */ 
/*      */       }
/*      */       else
/*      */       {
/*      */ 
/*  753 */         String reqLength = 
/*  754 */           CommonValidator.validateLength(value, 15, 15);
/*      */         
/*  756 */         if (reqLength.equals("greaterThanMax"))
/*      */         {
/*  758 */           errorObj = new ErrorObject(
/*  759 */             SubmissionErrorCodes.ERROR_DATAFIELD437_SHIPTO_POSTALCODE.getErrorCode(), 
/*  760 */             SubmissionErrorCodes.ERROR_DATAFIELD437_SHIPTO_POSTALCODE
/*  761 */             .getErrorDescription() + 
/*  762 */             "\n" + 
/*  763 */             "RecordType:" + 
/*  764 */             cspl2Bean.getRecordType() + 
/*  765 */             "|" + 
/*  766 */             "RecordNumber:" + 
/*  767 */             cspl2Bean.getRecordNumber() + 
/*  768 */             "|" + 
/*  769 */             "ShipToPostalCode" + 
/*  770 */             "|" + 
/*  771 */             "This field length Cannot be greater than 15");
/*  772 */           errorCodes.add(errorObj);
/*      */ 
/*      */         }
/*      */         
/*      */ 
/*      */       }
/*      */       
/*      */ 
/*      */     }
/*  781 */     else if (!CommonValidator.isNullOrEmpty(value))
/*      */     {
/*  783 */       String reqLength = CommonValidator.validateLength(value, 15, 15);
/*      */       
/*  785 */       if (reqLength.equals("greaterThanMax"))
/*      */       {
/*  787 */         errorObj = new ErrorObject(
/*  788 */           SubmissionErrorCodes.ERROR_DATAFIELD437_SHIPTO_POSTALCODE.getErrorCode(), 
/*  789 */           SubmissionErrorCodes.ERROR_DATAFIELD437_SHIPTO_POSTALCODE
/*  790 */           .getErrorDescription() + 
/*  791 */           "\n" + 
/*  792 */           "RecordType:" + 
/*  793 */           cspl2Bean.getRecordType() + 
/*  794 */           "|" + 
/*  795 */           "RecordNumber:" + 
/*  796 */           cspl2Bean.getRecordNumber() + 
/*  797 */           "|" + 
/*  798 */           "ShipToPostalCode" + 
/*  799 */           "|" + 
/*  800 */           "This field length Cannot be greater than 15");
/*  801 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTotalTaxAmount(CPSLevel2Bean csBean, List<ErrorObject> errorCodes, String countryCode)
/*      */     throws SettlementException
/*      */   {
/*  818 */     ErrorObject errorObj = null;
/*      */     
/*      */ 
/*      */ 
/*  822 */     if ((!CommonValidator.isNullOrEmpty(countryCode)) && (isValidCountryCodeForCPL2TAA(countryCode))) {
/*  823 */       if (CommonValidator.isNullOrEmpty(csBean.getTotalTaxAmount()))
/*      */       {
/*  825 */         errorObj = new ErrorObject(
/*  826 */           SubmissionErrorCodes.ERROR_DATAFIELD440_TOTAL_TAXAMOUNT.getErrorCode(), 
/*  827 */           SubmissionErrorCodes.ERROR_DATAFIELD440_TOTAL_TAXAMOUNT
/*  828 */           .getErrorDescription() + 
/*  829 */           "\n" + 
/*  830 */           "RecordType:" + 
/*  831 */           csBean.getRecordType() + 
/*  832 */           "|" + 
/*  833 */           "RecordNumber:" + 
/*  834 */           csBean.getRecordNumber() + 
/*  835 */           "|" + 
/*  836 */           "TotalTaxAmount" + 
/*  837 */           "|" + 
/*  838 */           "This field is mandatory and cannot be empty");
/*  839 */         errorCodes.add(errorObj);
/*      */ 
/*      */       }
/*  842 */       else if (CommonValidator.validateData(csBean.getTotalTaxAmount(), 
/*  843 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ƅ']))
/*      */       {
/*  845 */         String reqLength = CommonValidator.validateLength(csBean
/*  846 */           .getTotalTaxAmount(), 12, 12);
/*      */         
/*  848 */         if (reqLength.equals("greaterThanMax")) {
/*  849 */           errorObj = new ErrorObject(
/*  850 */             SubmissionErrorCodes.ERROR_DATAFIELD439_TOTAL_TAXAMOUNT.getErrorCode(), 
/*  851 */             SubmissionErrorCodes.ERROR_DATAFIELD439_TOTAL_TAXAMOUNT
/*  852 */             .getErrorDescription() + 
/*  853 */             "\n" + 
/*  854 */             "RecordType:" + 
/*  855 */             csBean.getRecordType() + 
/*  856 */             "|" + 
/*  857 */             "RecordNumber:" + 
/*  858 */             csBean.getRecordNumber() + 
/*  859 */             "|" + 
/*  860 */             "TotalTaxAmount" + 
/*  861 */             "|" + 
/*  862 */             "This field length Cannot be greater than 12");
/*  863 */           errorCodes.add(errorObj);
/*      */         }
/*  865 */         else if (CommonValidator.isNullOrEmpty(csBean.getTaxTypeCode()))
/*      */         {
/*  867 */           errorObj = new ErrorObject(
/*  868 */             SubmissionErrorCodes.ERROR_DATAFIELD442_TAX_TYPECODE.getErrorCode(), 
/*  869 */             SubmissionErrorCodes.ERROR_DATAFIELD442_TAX_TYPECODE
/*  870 */             .getErrorDescription() + 
/*  871 */             "\n" + 
/*  872 */             "RecordType:" + 
/*  873 */             csBean.getRecordType() + 
/*  874 */             "|" + 
/*  875 */             "RecordNumber:" + 
/*  876 */             csBean.getRecordNumber() + 
/*  877 */             "|" + 
/*  878 */             "TotalTaxAmount" + 
/*  879 */             "|" + 
/*  880 */             "Tax Type Code required when Total Tax Amount is populated");
/*  881 */           errorCodes.add(errorObj);
/*      */         }
/*      */       }
/*      */       else {
/*  885 */         errorObj = new ErrorObject(
/*  886 */           SubmissionErrorCodes.ERROR_DATAFIELD438_TOTAL_TAXAMOUNT.getErrorCode(), 
/*  887 */           SubmissionErrorCodes.ERROR_DATAFIELD438_TOTAL_TAXAMOUNT
/*  888 */           .getErrorDescription() + 
/*  889 */           "\n" + 
/*  890 */           "RecordType:" + 
/*  891 */           csBean.getRecordType() + 
/*  892 */           "|" + 
/*  893 */           "RecordNumber:" + 
/*  894 */           csBean.getRecordNumber() + 
/*  895 */           "|" + 
/*  896 */           "TotalTaxAmount" + 
/*  897 */           "|" + 
/*  898 */           "This field can only be Numeric");
/*  899 */         errorCodes.add(errorObj);
/*      */       }
/*      */       
/*      */     }
/*  903 */     else if (!CommonValidator.isNullOrEmpty(csBean.getTotalTaxAmount()))
/*      */     {
/*  905 */       if (CommonValidator.validateData(csBean.getTotalTaxAmount(), 
/*  906 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ƅ']))
/*      */       {
/*  908 */         String reqLength = CommonValidator.validateLength(csBean
/*  909 */           .getTotalTaxAmount(), 12, 12);
/*      */         
/*  911 */         if (reqLength.equals("greaterThanMax")) {
/*  912 */           errorObj = new ErrorObject(
/*  913 */             SubmissionErrorCodes.ERROR_DATAFIELD439_TOTAL_TAXAMOUNT.getErrorCode(), 
/*  914 */             SubmissionErrorCodes.ERROR_DATAFIELD439_TOTAL_TAXAMOUNT
/*  915 */             .getErrorDescription() + 
/*  916 */             "\n" + 
/*  917 */             "RecordType:" + 
/*  918 */             csBean.getRecordType() + 
/*  919 */             "|" + 
/*  920 */             "RecordNumber:" + 
/*  921 */             csBean.getRecordNumber() + 
/*  922 */             "|" + 
/*  923 */             "TotalTaxAmount" + 
/*  924 */             "|" + 
/*  925 */             "This field length Cannot be greater than 12");
/*  926 */           errorCodes.add(errorObj);
/*      */         }
/*  928 */         else if (CommonValidator.isNullOrEmpty(csBean.getTaxTypeCode()))
/*      */         {
/*  930 */           errorObj = new ErrorObject(
/*  931 */             SubmissionErrorCodes.ERROR_DATAFIELD442_TAX_TYPECODE.getErrorCode(), 
/*  932 */             SubmissionErrorCodes.ERROR_DATAFIELD442_TAX_TYPECODE
/*  933 */             .getErrorDescription() + 
/*  934 */             "\n" + 
/*  935 */             "RecordType:" + 
/*  936 */             csBean.getRecordType() + 
/*  937 */             "|" + 
/*  938 */             "RecordNumber:" + 
/*  939 */             csBean.getRecordNumber() + 
/*  940 */             "|" + 
/*  941 */             "TotalTaxAmount" + 
/*  942 */             "|" + 
/*  943 */             "Tax Type Code required when Total Tax Amount is populated");
/*  944 */           errorCodes.add(errorObj);
/*      */         }
/*      */       }
/*      */       else {
/*  948 */         errorObj = new ErrorObject(
/*  949 */           SubmissionErrorCodes.ERROR_DATAFIELD438_TOTAL_TAXAMOUNT.getErrorCode(), 
/*  950 */           SubmissionErrorCodes.ERROR_DATAFIELD438_TOTAL_TAXAMOUNT
/*  951 */           .getErrorDescription() + 
/*  952 */           "\n" + 
/*  953 */           "RecordType:" + 
/*  954 */           csBean.getRecordType() + 
/*  955 */           "|" + 
/*  956 */           "RecordNumber:" + 
/*  957 */           csBean.getRecordNumber() + 
/*  958 */           "|" + 
/*  959 */           "TotalTaxAmount" + 
/*  960 */           "|" + 
/*  961 */           "This field can only be Numeric");
/*  962 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTaxTypeCode(String value, List<ErrorObject> errorCodes, String transactionCurrencyCode, String countryCode, CPSLevel2Bean cspl2Bean)
/*      */     throws SettlementException
/*      */   {
/*  980 */     ErrorObject errorObj = null;
/*      */     
/*      */ 
/*      */ 
/*  984 */     if ((!CommonValidator.isNullOrEmpty(countryCode)) && (isValidCountryCodeForCPL2TAA(countryCode))) {
/*  985 */       if (CommonValidator.isNullOrEmpty(value))
/*      */       {
/*  987 */         errorObj = new ErrorObject(
/*  988 */           SubmissionErrorCodes.ERROR_DATAFIELD441_TAX_TYPECODE.getErrorCode(), 
/*  989 */           SubmissionErrorCodes.ERROR_DATAFIELD441_TAX_TYPECODE
/*  990 */           .getErrorDescription() + 
/*  991 */           "\n" + 
/*  992 */           "RecordType:" + 
/*  993 */           cspl2Bean.getRecordType() + 
/*  994 */           "|" + 
/*  995 */           "RecordNumber:" + 
/*  996 */           cspl2Bean.getRecordNumber() + 
/*  997 */           "|" + 
/*  998 */           "TaxTypeCode" + 
/*  999 */           "|" + 
/* 1000 */           "This field is mandatory and cannot be empty");
/* 1001 */         errorCodes.add(errorObj);
/*      */       }
/* 1003 */       else if ((!CommonValidator.isNullOrEmpty(transactionCurrencyCode)) && (transactionCurrencyCode.equalsIgnoreCase(CurrencyCode.US_Dollar.getCurrencyCode())))
/*      */       {
/* 1005 */         if (!value.equalsIgnoreCase(
/* 1006 */           TaxTypeCd.Sales_Tax.getTaxTypeCd()))
/*      */         {
/* 1008 */           errorObj = new ErrorObject(
/* 1009 */             SubmissionErrorCodes.ERROR_DATAFIELD441_TAX_TYPECODE.getErrorCode(), 
/* 1010 */             SubmissionErrorCodes.ERROR_DATAFIELD441_TAX_TYPECODE
/* 1011 */             .getErrorDescription() + 
/* 1012 */             "\n" + 
/* 1013 */             "RecordType:" + 
/* 1014 */             cspl2Bean.getRecordType() + 
/* 1015 */             "|" + 
/* 1016 */             "RecordNumber:" + 
/* 1017 */             cspl2Bean.getRecordNumber() + 
/* 1018 */             "|" + 
/* 1019 */             "TaxTypeCode" + 
/* 1020 */             "|" + 
/* 1021 */             "This field is missing or invalid");
/* 1022 */           errorCodes.add(errorObj);
/*      */         }
/* 1024 */       } else if ((!CommonValidator.isNullOrEmpty(transactionCurrencyCode)) && (transactionCurrencyCode.equalsIgnoreCase(CurrencyCode.Canadian_Dollar.getCurrencyCode())))
/*      */       {
/* 1026 */         if (!value.equalsIgnoreCase(
/* 1027 */           TaxTypeCd.All_Taxes.getTaxTypeCd()))
/*      */         {
/* 1029 */           errorObj = new ErrorObject(
/* 1030 */             SubmissionErrorCodes.ERROR_DATAFIELD441_TAX_TYPECODE.getErrorCode(), 
/* 1031 */             SubmissionErrorCodes.ERROR_DATAFIELD441_TAX_TYPECODE
/* 1032 */             .getErrorDescription() + 
/* 1033 */             "\n" + 
/* 1034 */             "RecordType:" + 
/* 1035 */             cspl2Bean.getRecordType() + 
/* 1036 */             "|" + 
/* 1037 */             "RecordNumber:" + 
/* 1038 */             cspl2Bean.getRecordNumber() + 
/* 1039 */             "|" + 
/* 1040 */             "TaxTypeCode" + 
/* 1041 */             "|" + 
/* 1042 */             "This field is missing or invalid");
/* 1043 */           errorCodes.add(errorObj);
/*      */         }
/*      */         
/*      */       }
/* 1047 */       else if (CommonValidator.validateData(value, 
/* 1048 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ɔ']))
/*      */       {
/* 1050 */         String reqLength = CommonValidator.validateLength(value, 3, 3);
/*      */         
/* 1052 */         if (reqLength.equals("greaterThanMax"))
/*      */         {
/* 1054 */           errorObj = new ErrorObject(
/* 1055 */             SubmissionErrorCodes.ERROR_DATAFIELD443_TAX_TYPECODE.getErrorCode(), 
/* 1056 */             SubmissionErrorCodes.ERROR_DATAFIELD443_TAX_TYPECODE
/* 1057 */             .getErrorDescription() + 
/* 1058 */             "\n" + 
/* 1059 */             "RecordType:" + 
/* 1060 */             cspl2Bean.getRecordType() + 
/* 1061 */             "|" + 
/* 1062 */             "RecordNumber:" + 
/* 1063 */             cspl2Bean.getRecordNumber() + 
/* 1064 */             "|" + 
/* 1065 */             "TaxTypeCode" + 
/* 1066 */             "|" + 
/* 1067 */             "This field length Cannot be greater than 3");
/* 1068 */           errorCodes.add(errorObj);
/*      */         }
/*      */       }
/*      */       else
/*      */       {
/* 1073 */         errorObj = new ErrorObject(
/* 1074 */           SubmissionErrorCodes.ERROR_DATAFIELD444_TAX_TYPECODE.getErrorCode(), 
/* 1075 */           SubmissionErrorCodes.ERROR_DATAFIELD444_TAX_TYPECODE
/* 1076 */           .getErrorDescription() + 
/* 1077 */           "\n" + 
/* 1078 */           "RecordType:" + 
/* 1079 */           cspl2Bean.getRecordType() + 
/* 1080 */           "|" + 
/* 1081 */           "RecordNumber:" + 
/* 1082 */           cspl2Bean.getRecordNumber() + 
/* 1083 */           "|" + 
/* 1084 */           "TaxTypeCode" + 
/* 1085 */           "|" + 
/* 1086 */           "This field can only be AlphaNumeric");
/* 1087 */         errorCodes.add(errorObj);
/*      */       }
/*      */       
/*      */ 
/*      */     }
/* 1092 */     else if (!CommonValidator.isNullOrEmpty(value)) {
/* 1093 */       if ((!CommonValidator.isNullOrEmpty(transactionCurrencyCode)) && (transactionCurrencyCode.equalsIgnoreCase(CurrencyCode.US_Dollar.getCurrencyCode())))
/*      */       {
/* 1095 */         if (!value.equalsIgnoreCase(
/* 1096 */           TaxTypeCd.Sales_Tax.getTaxTypeCd()))
/*      */         {
/* 1098 */           errorObj = new ErrorObject(
/* 1099 */             SubmissionErrorCodes.ERROR_DATAFIELD441_TAX_TYPECODE.getErrorCode(), 
/* 1100 */             SubmissionErrorCodes.ERROR_DATAFIELD441_TAX_TYPECODE
/* 1101 */             .getErrorDescription() + 
/* 1102 */             "\n" + 
/* 1103 */             "RecordType:" + 
/* 1104 */             cspl2Bean.getRecordType() + 
/* 1105 */             "|" + 
/* 1106 */             "RecordNumber:" + 
/* 1107 */             cspl2Bean.getRecordNumber() + 
/* 1108 */             "|" + 
/* 1109 */             "TaxTypeCode" + 
/* 1110 */             "|" + 
/* 1111 */             "This field is missing or invalid");
/* 1112 */           errorCodes.add(errorObj);
/*      */         }
/* 1114 */       } else if ((!CommonValidator.isNullOrEmpty(transactionCurrencyCode)) && (transactionCurrencyCode.equalsIgnoreCase(CurrencyCode.Canadian_Dollar.getCurrencyCode())))
/*      */       {
/* 1116 */         if (!value.equalsIgnoreCase(
/* 1117 */           TaxTypeCd.All_Taxes.getTaxTypeCd()))
/*      */         {
/* 1119 */           errorObj = new ErrorObject(
/* 1120 */             SubmissionErrorCodes.ERROR_DATAFIELD441_TAX_TYPECODE.getErrorCode(), 
/* 1121 */             SubmissionErrorCodes.ERROR_DATAFIELD441_TAX_TYPECODE
/* 1122 */             .getErrorDescription() + 
/* 1123 */             "\n" + 
/* 1124 */             "RecordType:" + 
/* 1125 */             cspl2Bean.getRecordType() + 
/* 1126 */             "|" + 
/* 1127 */             "RecordNumber:" + 
/* 1128 */             cspl2Bean.getRecordNumber() + 
/* 1129 */             "|" + 
/* 1130 */             "TaxTypeCode" + 
/* 1131 */             "|" + 
/* 1132 */             "This field is missing or invalid");
/* 1133 */           errorCodes.add(errorObj);
/*      */         }
/*      */         
/*      */       }
/* 1137 */       else if (CommonValidator.validateData(value, 
/* 1138 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ɔ']))
/*      */       {
/* 1140 */         String reqLength = CommonValidator.validateLength(value, 3, 3);
/*      */         
/* 1142 */         if (reqLength.equals("greaterThanMax"))
/*      */         {
/* 1144 */           errorObj = new ErrorObject(
/* 1145 */             SubmissionErrorCodes.ERROR_DATAFIELD443_TAX_TYPECODE.getErrorCode(), 
/* 1146 */             SubmissionErrorCodes.ERROR_DATAFIELD443_TAX_TYPECODE
/* 1147 */             .getErrorDescription() + 
/* 1148 */             "\n" + 
/* 1149 */             "RecordType:" + 
/* 1150 */             cspl2Bean.getRecordType() + 
/* 1151 */             "|" + 
/* 1152 */             "RecordNumber:" + 
/* 1153 */             cspl2Bean.getRecordNumber() + 
/* 1154 */             "|" + 
/* 1155 */             "TaxTypeCode" + 
/* 1156 */             "|" + 
/* 1157 */             "This field length Cannot be greater than 3");
/* 1158 */           errorCodes.add(errorObj);
/*      */         }
/*      */       }
/*      */       else
/*      */       {
/* 1163 */         errorObj = new ErrorObject(
/* 1164 */           SubmissionErrorCodes.ERROR_DATAFIELD444_TAX_TYPECODE.getErrorCode(), 
/* 1165 */           SubmissionErrorCodes.ERROR_DATAFIELD444_TAX_TYPECODE
/* 1166 */           .getErrorDescription() + 
/* 1167 */           "\n" + 
/* 1168 */           "RecordType:" + 
/* 1169 */           cspl2Bean.getRecordType() + 
/* 1170 */           "|" + 
/* 1171 */           "RecordNumber:" + 
/* 1172 */           cspl2Bean.getRecordNumber() + 
/* 1173 */           "|" + 
/* 1174 */           "TaxTypeCode" + 
/* 1175 */           "|" + 
/* 1176 */           "This field can only be AlphaNumeric");
/* 1177 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static boolean isValidCountryCodeForCPL2TAA(String countryCode)
/*      */   {
/* 1193 */     if ((countryCode.equals(CountryCode.American_Samoa.getCountryCode())) || (countryCode.equals(CountryCode.Guam.getCountryCode())) || 
/* 1194 */       (countryCode.equals(CountryCode.Marshall_Islands.getCountryCode())) || (countryCode.equals(CountryCode.Federated_States_Of_Micronesia.getCountryCode())) || 
/* 1195 */       (countryCode.equals(CountryCode.PuertoRico.getCountryCode())) || (countryCode.equals(CountryCode.United_States.getCountryCode())) || 
/* 1196 */       (countryCode.equals(CountryCode.Northern_Mariana_Islands.getCountryCode())) || (countryCode.equals(CountryCode.Palau.getCountryCode())) || 
/* 1197 */       (countryCode.equals(CountryCode.VirginIslandsUS.getCountryCode()))) {
/* 1198 */       return true;
/*      */     }
/*      */     
/* 1201 */     return false;
/*      */   }
/*      */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\validator\nonindustry\CPSL2RecordValidator.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */