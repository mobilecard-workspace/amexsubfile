/*     */ package com.americanexpress.ips.gfsg.validator;
/*     */ 
/*     */ import com.americanexpress.ips.gfsg.beans.ErrorObject;
/*     */ import com.americanexpress.ips.gfsg.beans.common.TransactionFileHeaderBean;
/*     */ import com.americanexpress.ips.gfsg.enumerations.RecordType;
/*     */ import com.americanexpress.ips.gfsg.enumerations.SubmissionErrorCodes;
/*     */ import com.americanexpress.ips.gfsg.exceptions.SettlementException;
/*     */ import java.util.List;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class TFHRecValidator
/*     */ {
/*     */   private static String recordNumber;
/*     */   
/*     */   public static void validateTFHRecord(TransactionFileHeaderBean transactionFileHeaderType, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/*  39 */     recordNumber = transactionFileHeaderType.getRecordNumber();
/*  40 */     validateRecordType(transactionFileHeaderType.getRecordType(), 
/*  41 */       errorCodes);
/*  42 */     validateRecordNumber(transactionFileHeaderType.getRecordNumber(), 
/*  43 */       errorCodes);
/*  44 */     validateSubmitterId(transactionFileHeaderType.getSubmitterId(), 
/*  45 */       errorCodes);
/*  46 */     validateSubmitterFileReferenceNumber(transactionFileHeaderType
/*  47 */       .getSubmitterFileReferenceNumber(), errorCodes);
/*  48 */     validateSubmitterFileSequenceNumber(transactionFileHeaderType
/*  49 */       .getSubmitterFileSequenceNumber(), errorCodes);
/*  50 */     validateFileCreationDate(transactionFileHeaderType
/*  51 */       .getFileCreationDate(), errorCodes);
/*  52 */     validateFileCreationTime(transactionFileHeaderType
/*  53 */       .getFileCreationTime(), errorCodes);
/*  54 */     validateFileVersionNumber(transactionFileHeaderType
/*  55 */       .getFileVersionNumber(), errorCodes);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateRecordType(String recordType, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/*  71 */     ErrorObject errorObj = null;
/*  72 */     if (CommonValidator.isNullOrEmpty(recordType))
/*     */     {
/*  74 */       errorObj = new ErrorObject(
/*  75 */         SubmissionErrorCodes.ERROR_DATAFIELD1_RECORDTYPE
/*  76 */         .getErrorCode(), 
/*  77 */         SubmissionErrorCodes.ERROR_DATAFIELD1_RECORDTYPE
/*  78 */         .getErrorDescription() + 
/*  79 */         "\n" + 
/*  80 */         "TFH" + 
/*  81 */         "|" + 
/*  82 */         "RecordNumber:" + 
/*  83 */         recordNumber + 
/*  84 */         "|" + 
/*  85 */         "RecordType:" + 
/*  86 */         "|" + 
/*  87 */         "This field is mandatory and cannot be empty");
/*  88 */       errorCodes.add(errorObj);
/*     */     }
/*  90 */     else if (!RecordType.Transaction_File_Header.getRecordType().equalsIgnoreCase(recordType))
/*     */     {
/*     */ 
/*  93 */       errorObj = new ErrorObject(
/*  94 */         SubmissionErrorCodes.ERROR_DATAFIELD1_RECORDTYPE);
/*  95 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateRecordNumber(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 112 */     ErrorObject errorObj = null;
/*     */     
/* 114 */     if (CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 116 */       errorObj = new ErrorObject(
/* 117 */         SubmissionErrorCodes.ERROR_DATAFIELD585_RECORDNUMBER
/* 118 */         .getErrorCode(), 
/* 119 */         SubmissionErrorCodes.ERROR_DATAFIELD585_RECORDNUMBER
/* 120 */         .getErrorDescription() + 
/* 121 */         "\n" + 
/* 122 */         "RecordType:" + 
/* 123 */         "TFH" + 
/* 124 */         "|" + 
/* 125 */         "RecordNumber:" + 
/* 126 */         recordNumber + 
/* 127 */         "|" + 
/* 128 */         "RecordNumber:" + 
/* 129 */         "|" + 
/* 130 */         "This field is mandatory and cannot be empty");
/* 131 */       errorCodes.add(errorObj);
/*     */     }
/* 133 */     else if (CommonValidator.validateData(value, 
/* 134 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[2]))
/*     */     {
/* 136 */       String reqLength = CommonValidator.validateLength(value, 8, 8);
/*     */       
/* 138 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/* 140 */         errorObj = new ErrorObject(
/* 141 */           SubmissionErrorCodes.ERROR_DATAFIELD3_RECORDNUMBER
/* 142 */           .getErrorCode(), 
/* 143 */           SubmissionErrorCodes.ERROR_DATAFIELD3_RECORDNUMBER
/* 144 */           .getErrorDescription() + 
/* 145 */           "\n" + 
/* 146 */           "RecordType:" + 
/* 147 */           "TFH" + 
/* 148 */           "|" + 
/* 149 */           "RecordNumber:" + 
/* 150 */           recordNumber + 
/* 151 */           "|" + 
/* 152 */           "RecordNumber:" + 
/* 153 */           "|" + 
/* 154 */           "This field length Cannot be greater than 8");
/* 155 */         errorCodes.add(errorObj);
/*     */       }
/*     */     } else {
/* 158 */       errorObj = new ErrorObject(
/* 159 */         SubmissionErrorCodes.ERROR_DATAFIELD2_RECORDNUMBER
/* 160 */         .getErrorCode(), 
/* 161 */         SubmissionErrorCodes.ERROR_DATAFIELD2_RECORDNUMBER
/* 162 */         .getErrorDescription() + 
/* 163 */         "\n" + 
/* 164 */         "RecordType:" + 
/* 165 */         "TFH" + 
/* 166 */         "|" + 
/* 167 */         "RecordNumber:" + 
/* 168 */         recordNumber + 
/* 169 */         "|" + 
/* 170 */         "RecordNumber:" + 
/* 171 */         "|" + 
/* 172 */         "This field can only be Numeric");
/* 173 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateSubmitterId(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 191 */     ErrorObject errorObj = null;
/*     */     
/* 193 */     if (CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 195 */       errorObj = new ErrorObject(
/*     */       
/*     */ 
/*     */ 
/*     */ 
/* 200 */         SubmissionErrorCodes.ERROR_DATAFIELD604_SUBMITTERID
/* 201 */         .getErrorCode(), 
/* 202 */         SubmissionErrorCodes.ERROR_DATAFIELD604_SUBMITTERID
/* 203 */         .getErrorDescription() + 
/* 204 */         "\n" + 
/* 205 */         "RecordType:" + 
/* 206 */         "TFH" + 
/* 207 */         "|" + 
/* 208 */         "RecordNumber:" + 
/* 209 */         recordNumber + 
/* 210 */         "|" + 
/* 211 */         "SubmitterId" + 
/* 212 */         "|" + 
/* 213 */         "This field is mandatory and cannot be empty");
/* 214 */       errorCodes.add(errorObj);
/*     */ 
/*     */     }
/* 217 */     else if (CommonValidator.validateData(value, 
/* 218 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[3]))
/*     */     {
/* 220 */       String reqLength = 
/* 221 */         CommonValidator.validateLength(value, 11, 11);
/*     */       
/* 223 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/* 225 */         errorObj = new ErrorObject(
/*     */         
/* 227 */           SubmissionErrorCodes.ERROR_DATAFIELD606_SUBMITTERID
/* 228 */           .getErrorCode(), 
/* 229 */           SubmissionErrorCodes.ERROR_DATAFIELD606_SUBMITTERID
/*     */           
/*     */ 
/*     */ 
/* 233 */           .getErrorDescription() + 
/* 234 */           "\n" + 
/* 235 */           "RecordType:" + 
/* 236 */           "TFH" + 
/* 237 */           "|" + 
/* 238 */           "RecordNumber:" + 
/* 239 */           recordNumber + 
/* 240 */           "|" + 
/* 241 */           "SubmitterId" + 
/* 242 */           "|" + 
/* 243 */           "This field length Cannot be greater than 11");
/* 244 */         errorCodes.add(errorObj);
/*     */       }
/*     */     } else {
/* 247 */       errorObj = new ErrorObject(
/*     */       
/* 249 */         SubmissionErrorCodes.ERROR_DATAFIELD605_SUBMITTERID
/* 250 */         .getErrorCode(), 
/* 251 */         SubmissionErrorCodes.ERROR_DATAFIELD605_SUBMITTERID
/*     */         
/*     */ 
/*     */ 
/* 255 */         .getErrorDescription() + 
/* 256 */         "\n" + 
/* 257 */         "RecordType:" + 
/* 258 */         "TFH" + 
/* 259 */         "|" + 
/* 260 */         "RecordNumber:" + 
/* 261 */         recordNumber + 
/* 262 */         "|" + 
/* 263 */         "SubmitterId" + 
/* 264 */         "|" + 
/* 265 */         "This field can only be AlphaNumeric");
/* 266 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateSubmitterFileReferenceNumber(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 284 */     ErrorObject errorObj = null;
/*     */     
/* 286 */     if (CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 288 */       errorObj = new ErrorObject(
/* 289 */         SubmissionErrorCodes.ERROR_DATAFIELD6_SUBMITTERFILE_REFERENCENUMBER
/* 290 */         .getErrorCode(), 
/* 291 */         SubmissionErrorCodes.ERROR_DATAFIELD6_SUBMITTERFILE_REFERENCENUMBER
/* 292 */         .getErrorDescription() + 
/* 293 */         "\n" + 
/* 294 */         "RecordType:" + 
/* 295 */         "TFH" + 
/* 296 */         "|" + 
/* 297 */         "RecordNumber:" + 
/* 298 */         recordNumber + 
/* 299 */         "|" + 
/* 300 */         "Submitter File Reference Number:" + 
/* 301 */         "|" + 
/* 302 */         "This field is mandatory and cannot be empty");
/* 303 */       errorCodes.add(errorObj);
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/* 309 */       String reqLength = CommonValidator.validateLength(value, 9, 9);
/*     */       
/* 311 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/* 313 */         errorObj = new ErrorObject(
/*     */         
/* 315 */           SubmissionErrorCodes.ERROR_DATAFIELD612_SUBMITTERFILE_REFERENCENUMBER
/* 316 */           .getErrorCode(), 
/* 317 */           SubmissionErrorCodes.ERROR_DATAFIELD612_SUBMITTERFILE_REFERENCENUMBER
/*     */           
/*     */ 
/*     */ 
/* 321 */           .getErrorDescription() + 
/* 322 */           "\n" + 
/* 323 */           "RecordType:" + 
/* 324 */           "TFH" + 
/* 325 */           "|" + 
/* 326 */           "RecordNumber:" + 
/* 327 */           recordNumber + 
/* 328 */           "|" + 
/* 329 */           "Submitter File Reference Number:" + 
/* 330 */           "|" + 
/* 331 */           "This field length Cannot be greater than 9");
/* 332 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateSubmitterFileSequenceNumber(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 370 */     ErrorObject errorObj = null;
/*     */     
/* 372 */     if (!CommonValidator.isNullOrEmpty(value)) {
/* 373 */       if (CommonValidator.validateData(value, 
/* 374 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[6]))
/*     */       {
/* 376 */         String reqLength = CommonValidator.validateLength(value, 9, 9);
/*     */         
/* 378 */         if (reqLength.equals("greaterThanMax"))
/*     */         {
/* 380 */           errorObj = new ErrorObject(
/*     */           
/* 382 */             SubmissionErrorCodes.ERROR_DATAFIELD607_SUBMITTERFILE_SEQUENCENUMBER
/* 383 */             .getErrorCode(), 
/* 384 */             SubmissionErrorCodes.ERROR_DATAFIELD607_SUBMITTERFILE_SEQUENCENUMBER
/*     */             
/*     */ 
/*     */ 
/* 388 */             .getErrorDescription() + 
/* 389 */             "\n" + 
/* 390 */             "RecordType:" + 
/* 391 */             "TFH" + 
/* 392 */             "|" + 
/* 393 */             "RecordNumber:" + 
/* 394 */             recordNumber + 
/* 395 */             "|" + 
/* 396 */             "SubmitterFileSequenceNumber:" + 
/* 397 */             "|" + 
/* 398 */             "This field length Cannot be greater than 9");
/* 399 */           errorCodes.add(errorObj);
/*     */         }
/*     */       } else {
/* 402 */         errorObj = new ErrorObject(
/*     */         
/* 404 */           SubmissionErrorCodes.ERROR_DATAFIELD608_SUBMITTERFILE_SEQUENCENUMBER
/* 405 */           .getErrorCode(), 
/* 406 */           SubmissionErrorCodes.ERROR_DATAFIELD608_SUBMITTERFILE_SEQUENCENUMBER
/*     */           
/*     */ 
/*     */ 
/* 410 */           .getErrorDescription() + 
/* 411 */           "\n" + 
/* 412 */           "RecordType:" + 
/* 413 */           "TFH" + 
/* 414 */           "|" + 
/* 415 */           "RecordNumber:" + 
/* 416 */           recordNumber + 
/* 417 */           "|" + 
/* 418 */           "SubmitterFileSequenceNumber:" + 
/* 419 */           "|" + 
/* 420 */           "This field can only be Numeric");
/* 421 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateFileCreationDate(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 439 */     ErrorObject errorObj = null;
/*     */     
/* 441 */     if (CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 443 */       errorObj = new ErrorObject(
/* 444 */         SubmissionErrorCodes.ERROR_DATAFIELD11_FILECREATION_DATE
/* 445 */         .getErrorCode(), 
/* 446 */         SubmissionErrorCodes.ERROR_DATAFIELD11_FILECREATION_DATE
/* 447 */         .getErrorDescription() + 
/* 448 */         "\n" + 
/* 449 */         "RecordType:" + 
/* 450 */         "TFH" + 
/* 451 */         "|" + 
/* 452 */         "RecordNumber:" + 
/* 453 */         recordNumber + 
/* 454 */         "|" + 
/* 455 */         "FileCreationDate" + 
/* 456 */         "|" + 
/* 457 */         "This field is mandatory and cannot be empty");
/* 458 */       errorCodes.add(errorObj);
/*     */     }
/* 460 */     else if (CommonValidator.validateData(value, 
/* 461 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[7]))
/*     */     {
/* 463 */       String reqLength = CommonValidator.validateLength(value, 8, 8);
/*     */       
/* 465 */       if ((reqLength.equals("equal")) && 
/* 466 */         (CommonValidator.isValidDate(value, 
/* 467 */         "CCYYMMDD")))
/*     */       {
/*     */ 
/* 470 */         String dateStatus = CommonValidator.validateDate(value);
/*     */         
/* 472 */         if (dateStatus.equals("expired"))
/*     */         {
/* 474 */           errorObj = new ErrorObject(
/* 475 */             SubmissionErrorCodes.ERROR_DATAFIELD13_FILECREATIONDATE
/* 476 */             .getErrorCode(), 
/* 477 */             SubmissionErrorCodes.ERROR_DATAFIELD13_FILECREATIONDATE
/* 478 */             .getErrorDescription() + 
/* 479 */             "\n" + 
/* 480 */             "RecordType:" + 
/* 481 */             "TFH" + 
/* 482 */             "|" + 
/* 483 */             "RecordNumber:" + 
/* 484 */             recordNumber + 
/* 485 */             "|" + 
/* 486 */             "FileCreationDate" + 
/* 487 */             "|" + 
/* 488 */             "File Creation Date too many days in past");
/* 489 */           errorCodes.add(errorObj);
/* 490 */         } else if (dateStatus.equals("futureDate"))
/*     */         {
/* 492 */           errorObj = new ErrorObject(
/* 493 */             SubmissionErrorCodes.ERROR_DATAFIELD12_FILECREATION_DATE
/* 494 */             .getErrorCode(), 
/* 495 */             SubmissionErrorCodes.ERROR_DATAFIELD12_FILECREATION_DATE
/* 496 */             .getErrorDescription() + 
/* 497 */             "\n" + 
/* 498 */             "RecordType:" + 
/* 499 */             "TFH" + 
/* 500 */             "|" + 
/* 501 */             "RecordNumber:" + 
/* 502 */             recordNumber + 
/* 503 */             "|" + 
/* 504 */             "FileCreationDate" + 
/* 505 */             "|" + 
/* 506 */             "File Creation Date too many days in future");
/* 507 */           errorCodes.add(errorObj);
/*     */         }
/*     */       } else {
/* 510 */         errorObj = new ErrorObject(
/* 511 */           SubmissionErrorCodes.ERROR_DATAFIELD592_FILECREATION_DATE
/* 512 */           .getErrorCode(), 
/* 513 */           SubmissionErrorCodes.ERROR_DATAFIELD592_FILECREATION_DATE
/* 514 */           .getErrorDescription() + 
/* 515 */           "\n" + 
/* 516 */           "RecordType:" + 
/* 517 */           "TFH" + 
/* 518 */           "|" + 
/* 519 */           "RecordNumber:" + 
/* 520 */           recordNumber + 
/* 521 */           "|" + 
/* 522 */           "FileCreationDate" + 
/* 523 */           "|" + 
/* 524 */           "This field length Cannot be greater than 8");
/* 525 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */     else {
/* 529 */       errorObj = new ErrorObject(
/* 530 */         SubmissionErrorCodes.ERROR_DATAFIELD591_FILECREATION_DATE
/* 531 */         .getErrorCode(), 
/* 532 */         SubmissionErrorCodes.ERROR_DATAFIELD591_FILECREATION_DATE
/* 533 */         .getErrorDescription() + 
/* 534 */         "\n" + 
/* 535 */         "RecordType:" + 
/* 536 */         "TFH" + 
/* 537 */         "|" + 
/* 538 */         "RecordNumber:" + 
/* 539 */         recordNumber + 
/* 540 */         "|" + 
/* 541 */         "FileCreationDate" + 
/* 542 */         "|" + 
/* 543 */         "This field can only be Numeric");
/* 544 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateFileCreationTime(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 561 */     ErrorObject errorObj = null;
/*     */     
/* 563 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 565 */       if (CommonValidator.validateData(value, 
/* 566 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[8]))
/*     */       {
/* 568 */         String reqLength = CommonValidator.validateLength(value, 6, 6);
/*     */         
/* 570 */         if (reqLength.equals("greaterThanMax")) {
/* 571 */           errorObj = new ErrorObject(
/* 572 */             SubmissionErrorCodes.ERROR_DATAFIELD14_FILECREATION_TIME
/* 573 */             .getErrorCode(), 
/* 574 */             SubmissionErrorCodes.ERROR_DATAFIELD14_FILECREATION_TIME
/* 575 */             .getErrorDescription() + 
/* 576 */             "\n" + 
/* 577 */             "RecordType:" + 
/* 578 */             "TFH" + 
/* 579 */             "|" + 
/* 580 */             "RecordNumber:" + 
/* 581 */             recordNumber + 
/* 582 */             "|" + 
/* 583 */             "FileCreationTime" + 
/* 584 */             "|" + 
/* 585 */             "This field length Cannot be greater than 6");
/* 586 */           errorCodes.add(errorObj);
/*     */ 
/*     */         }
/* 589 */         else if (!CommonValidator.validateTime(value))
/*     */         {
/* 591 */           errorObj = new ErrorObject(
/* 592 */             SubmissionErrorCodes.ERROR_DATAFIELD14_FILECREATION_TIME
/* 593 */             .getErrorCode(), 
/* 594 */             SubmissionErrorCodes.ERROR_DATAFIELD14_FILECREATION_TIME
/* 595 */             .getErrorDescription() + 
/* 596 */             "\n" + 
/* 597 */             "RecordType:" + 
/* 598 */             "TFH" + 
/* 599 */             "|" + 
/* 600 */             "RecordNumber:" + 
/* 601 */             recordNumber + 
/* 602 */             "|" + 
/* 603 */             "FileCreationTime" + 
/* 604 */             "|" + 
/* 605 */             SubmissionErrorCodes.ERROR_DATAFIELD14_FILECREATION_TIME
/* 606 */             .getErrorDescription());
/* 607 */           errorCodes.add(errorObj);
/*     */         }
/*     */         
/*     */       }
/*     */       else
/*     */       {
/* 613 */         errorObj = new ErrorObject(
/* 614 */           SubmissionErrorCodes.ERROR_DATAFIELD593_FILECREATION_TIME
/* 615 */           .getErrorCode(), 
/* 616 */           SubmissionErrorCodes.ERROR_DATAFIELD593_FILECREATION_TIME
/* 617 */           .getErrorDescription() + 
/* 618 */           "\n" + 
/* 619 */           "RecordType:" + 
/* 620 */           "TFH" + 
/* 621 */           "|" + 
/* 622 */           "RecordNumber:" + 
/* 623 */           recordNumber + 
/* 624 */           "|" + 
/* 625 */           "FileCreationTime" + 
/* 626 */           "|" + 
/* 627 */           "This field can only be Numeric");
/* 628 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateFileVersionNumber(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 647 */     ErrorObject errorObj = null;
/* 648 */     if (CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 650 */       errorObj = new ErrorObject(
/*     */       
/* 652 */         SubmissionErrorCodes.ERROR_DATAFIELD609_FILEVERSION_NUMBER
/* 653 */         .getErrorCode(), 
/* 654 */         SubmissionErrorCodes.ERROR_DATAFIELD609_FILEVERSION_NUMBER
/*     */         
/*     */ 
/*     */ 
/* 658 */         .getErrorDescription() + 
/* 659 */         "\n" + 
/* 660 */         "RecordType:" + 
/* 661 */         "TFH" + 
/* 662 */         "|" + 
/* 663 */         "RecordNumber:" + 
/* 664 */         recordNumber + 
/* 665 */         "|" + 
/* 666 */         "FileVersionNumber" + 
/* 667 */         "|" + 
/* 668 */         "This field is mandatory and cannot be empty");
/* 669 */       errorCodes.add(errorObj);
/*     */     }
/* 671 */     else if (CommonValidator.validateData(value, 
/* 672 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[9]))
/*     */     {
/* 674 */       String reqLength = CommonValidator.validateLength(value, 8, 8);
/*     */       
/* 676 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/* 678 */         errorObj = new ErrorObject(
/*     */         
/* 680 */           SubmissionErrorCodes.ERROR_DATAFIELD611_FILEVERSION_NUMBER
/* 681 */           .getErrorCode(), 
/* 682 */           SubmissionErrorCodes.ERROR_DATAFIELD611_FILEVERSION_NUMBER
/*     */           
/*     */ 
/*     */ 
/* 686 */           .getErrorDescription() + 
/* 687 */           "\n" + 
/* 688 */           "RecordType:" + 
/* 689 */           "TFH" + 
/* 690 */           "|" + 
/* 691 */           "RecordNumber:" + 
/* 692 */           recordNumber + 
/* 693 */           "|" + 
/* 694 */           "FileVersionNumber" + 
/* 695 */           "|" + 
/* 696 */           "This field length Cannot be greater than 8");
/* 697 */         errorCodes.add(errorObj);
/*     */       }
/*     */     } else {
/* 700 */       errorObj = new ErrorObject(
/*     */       
/* 702 */         SubmissionErrorCodes.ERROR_DATAFIELD610_FILEVERSION_NUMBER
/* 703 */         .getErrorCode(), 
/* 704 */         SubmissionErrorCodes.ERROR_DATAFIELD610_FILEVERSION_NUMBER
/*     */         
/*     */ 
/*     */ 
/* 708 */         .getErrorDescription() + 
/* 709 */         "\n" + 
/* 710 */         "RecordType:" + 
/* 711 */         "TFH" + 
/* 712 */         "|" + 
/* 713 */         "RecordNumber:" + 
/* 714 */         recordNumber + 
/* 715 */         "|" + 
/* 716 */         "FileVersionNumber" + 
/* 717 */         "|" + 
/* 718 */         "This field can only be AlphaNumeric");
/* 719 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\validator\TFHRecValidator.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */