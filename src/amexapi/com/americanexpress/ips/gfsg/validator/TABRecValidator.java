/*      */ package com.americanexpress.ips.gfsg.validator;
/*      */ 
/*      */ import com.americanexpress.ips.gfsg.beans.ErrorObject;
/*      */ import com.americanexpress.ips.gfsg.beans.common.TransactionAdviceAddendumBean;
/*      */ import com.americanexpress.ips.gfsg.beans.common.TransactionAdviceBasicBean;
/*      */ import com.americanexpress.ips.gfsg.beans.common.TransactionAdviceDetailBean;
/*      */ import com.americanexpress.ips.gfsg.beans.common.taa.IndustryTypeTAABean;
/*      */ import com.americanexpress.ips.gfsg.beans.common.taa.LocationDetailTAABean;
/*      */ import com.americanexpress.ips.gfsg.beans.common.taa.NonIndustrySpecificTAABean;
/*      */ import com.americanexpress.ips.gfsg.connection.PropertyReader;
/*      */ import com.americanexpress.ips.gfsg.enumerations.ElectronicCommerceIndicator;
/*      */ import com.americanexpress.ips.gfsg.enumerations.FormatCode;
/*      */ import com.americanexpress.ips.gfsg.enumerations.ProcessingCode;
/*      */ import com.americanexpress.ips.gfsg.enumerations.RecordType;
/*      */ import com.americanexpress.ips.gfsg.enumerations.SubmissionErrorCodes;
/*      */ import com.americanexpress.ips.gfsg.enumerations.SubmissionMethod;
/*      */ import com.americanexpress.ips.gfsg.exceptions.SettlementException;
/*      */ import java.util.ArrayList;
/*      */ import java.util.List;
/*      */ import org.apache.log4j.Logger;
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ public class TABRecValidator
/*      */ {
/*      */   private static String recordNumber;
/*      */   private static String recordType;
/*      */   static TransactionAdviceBasicBean transactionAdviceBasicType;
/*   38 */   private static final Logger LOGGER = Logger.getLogger(TABRecValidator.class);
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static boolean debugFlag;
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void validateTABRecordType(TransactionAdviceBasicBean transactAdviceBasicType, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*   56 */     if (PropertyReader.getDebuggerFlag().equalsIgnoreCase(
/*   57 */       "ON")) {
/*   58 */       debugFlag = true;
/*   59 */       LOGGER.info("Entered into validateTABRecordType()");
/*      */     }
/*      */     
/*   62 */     recordNumber = transactAdviceBasicType.getRecordNumber();
/*   63 */     recordType = transactAdviceBasicType.getRecordType();
/*   64 */     boolean isPaperSubmitter = false;
/*   65 */     if (!CommonValidator.isNullOrEmpty(transactAdviceBasicType
/*   66 */       .getSubmissionMethod()))
/*      */     {
/*   68 */       if (SubmissionMethod.Paper_external_vendor_paper_processor.getSubmissionMethod().equals(
/*   69 */         transactAdviceBasicType.getSubmissionMethod()))
/*   70 */         isPaperSubmitter = true;
/*      */     }
/*   72 */     validateRecordType(transactAdviceBasicType.getRecordType(), 
/*   73 */       errorCodes);
/*   74 */     validateRecordNumber(transactAdviceBasicType.getRecordNumber(), 
/*   75 */       errorCodes);
/*   76 */     validateTransactionIdentifier(transactAdviceBasicType
/*   77 */       .getTransactionIdentifier(), isPaperSubmitter, errorCodes);
/*   78 */     validateFormatCode(transactAdviceBasicType, errorCodes);
/*   79 */     validateMediaCode(transactAdviceBasicType.getMediaCode(), errorCodes);
/*   80 */     validateSubmissionMethod(transactAdviceBasicType
/*   81 */       .getSubmissionMethod(), errorCodes);
/*   82 */     validateApprovalCode(transactAdviceBasicType, errorCodes);
/*   83 */     validatePrimaryAccountNumber(transactAdviceBasicType
/*   84 */       .getPrimaryAccountNumber(), errorCodes);
/*   85 */     validateCardExpiryDate(transactAdviceBasicType.getCardExpiryDate(), 
/*   86 */       errorCodes);
/*   87 */     validateTransactionDate(
/*   88 */       transactAdviceBasicType.getTransactionDate(), errorCodes);
/*   89 */     validateTransactionTime(
/*   90 */       transactAdviceBasicType.getTransactionTime(), errorCodes);
/*   91 */     validateTransactionAmount(transactAdviceBasicType, errorCodes);
/*   92 */     validateProcessingCode(transactAdviceBasicType.getProcessingCode(), 
/*   93 */       errorCodes);
/*   94 */     validateTransactionCurrencyCode(transactAdviceBasicType
/*   95 */       .getTransactionCurrencyCode(), errorCodes);
/*   96 */     validateExtendedPaymentData(transactAdviceBasicType
/*   97 */       .getExtendedPaymentData(), errorCodes);
/*   98 */     validateMerchantId(transactAdviceBasicType.getMerchantId(), 
/*   99 */       errorCodes);
/*  100 */     validateMerchantLocationId(transactAdviceBasicType
/*  101 */       .getMerchantLocationId(), errorCodes);
/*  102 */     validateMerchantContactInfo(transactAdviceBasicType
/*  103 */       .getMerchantContactInfo(), errorCodes);
/*  104 */     validateTerminalId(transactAdviceBasicType.getTerminalId(), 
/*  105 */       errorCodes);
/*  106 */     validatePosDataCode(transactAdviceBasicType.getPosDataCode(), 
/*  107 */       isPaperSubmitter, errorCodes);
/*  108 */     validateInvoiceReferenceNumber(transactAdviceBasicType
/*  109 */       .getInvoiceReferenceNumber(), errorCodes);
/*  110 */     validateTabImageSequenceNumber(transactAdviceBasicType
/*  111 */       .getTabImageSequenceNumber(), isPaperSubmitter, errorCodes);
/*  112 */     validateMatchingKeyType(
/*  113 */       transactAdviceBasicType.getMatchingKeyType(), errorCodes);
/*  114 */     validateMatchingKey(transactAdviceBasicType.getMatchingKey(), 
/*  115 */       errorCodes);
/*  116 */     validateElectronicCommerceIndicator(transactAdviceBasicType
/*  117 */       .getElectronicCommerceIndicator(), errorCodes);
/*      */     
/*      */ 
/*      */ 
/*  121 */     if (transactAdviceBasicType.getLocationDetailTAABean() == null) {
/*  122 */       transactAdviceBasicType.setLocationDetailTAABean(new LocationDetailTAABean());
/*      */     }
/*      */     
/*  125 */     LocationTAARecValidator.validtateLocationRecordType(
/*  126 */       transactAdviceBasicType, errorCodes);
/*  127 */     if ((transactAdviceBasicType.getTransactionAdviceDetailBean() instanceof TransactionAdviceDetailBean))
/*      */     {
/*  129 */       TADRecValidator.validateTADRecord(transactAdviceBasicType, 
/*  130 */         errorCodes);
/*      */     }
/*      */     
/*      */ 
/*  134 */     ArrayList<TransactionAdviceAddendumBean> taaBeanList = transactAdviceBasicType.getArrayTAABean();
/*      */     
/*  136 */     for (TransactionAdviceAddendumBean taaBean : taaBeanList)
/*      */     {
/*  138 */       if ((taaBean instanceof NonIndustrySpecificTAABean))
/*      */       {
/*      */ 
/*  141 */         NonIndustryTypeRecValidator.validtateNonInIndustryRecordType(transactAdviceBasicType, taaBean, errorCodes);
/*      */       }
/*      */       
/*      */ 
/*      */ 
/*  146 */       if ((taaBean instanceof IndustryTypeTAABean))
/*      */       {
/*      */ 
/*  149 */         IndustryTypeRecValidator.validtateIndustryRecordType(transactAdviceBasicType, taaBean, errorCodes);
/*      */       }
/*      */     }
/*      */     
/*  153 */     if (debugFlag) {
/*  154 */       LOGGER.info("Exiting from validateTABRecordType()");
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateRecordType(String recType, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  171 */     ErrorObject errorObject = null;
/*      */     
/*  173 */     if (CommonValidator.isNullOrEmpty(recType)) {
/*  174 */       errorObject = new ErrorObject(
/*  175 */         SubmissionErrorCodes.ERROR_DATAFIELD1_RECORDTYPE
/*  176 */         .getErrorCode(), 
/*  177 */         SubmissionErrorCodes.ERROR_DATAFIELD1_RECORDTYPE
/*  178 */         .getErrorDescription() + 
/*  179 */         "\n" + 
/*  180 */         "TFH" + 
/*  181 */         "|" + 
/*  182 */         "RecordNumber:" + 
/*  183 */         recordNumber + 
/*  184 */         "|" + 
/*  185 */         "RecordType:" + 
/*  186 */         "|" + 
/*  187 */         "This field is mandatory and cannot be empty");
/*  188 */       errorCodes.add(errorObject);
/*      */     }
/*  190 */     else if (!RecordType.Transaction_Advice_Basic.getRecordType().equalsIgnoreCase(recType))
/*      */     {
/*  192 */       errorObject = new ErrorObject(
/*  193 */         SubmissionErrorCodes.ERROR_DATAFIELD1_RECORDTYPE
/*  194 */         .getErrorCode(), 
/*  195 */         SubmissionErrorCodes.ERROR_DATAFIELD1_RECORDTYPE
/*  196 */         .getErrorDescription());
/*  197 */       errorCodes.add(errorObject);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateRecordNumber(String recordNo, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  214 */     ErrorObject errorObj = null;
/*      */     
/*  216 */     if (CommonValidator.isNullOrEmpty(recordNo))
/*      */     {
/*  218 */       errorObj = new ErrorObject(
/*  219 */         SubmissionErrorCodes.ERROR_DATAFIELD585_RECORDNUMBER
/*  220 */         .getErrorCode(), 
/*  221 */         SubmissionErrorCodes.ERROR_DATAFIELD585_RECORDNUMBER
/*  222 */         .getErrorDescription() + 
/*  223 */         "\n" + 
/*  224 */         "RecordType:" + 
/*  225 */         recordType + 
/*  226 */         "|" + 
/*  227 */         "RecordNumber:" + 
/*  228 */         recordNumber + 
/*  229 */         "|" + 
/*  230 */         "RecordNumber:" + 
/*  231 */         "|" + 
/*  232 */         "This field is mandatory and cannot be empty");
/*  233 */       errorCodes.add(errorObj);
/*      */ 
/*      */     }
/*  236 */     else if (CommonValidator.validateData(recordNo, 
/*  237 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[11]))
/*      */     {
/*  239 */       String reqLength = CommonValidator.validateLength(recordNo, 8, 8);
/*      */       
/*  241 */       if (reqLength.equals("greaterThanMax")) {
/*  242 */         errorObj = new ErrorObject(
/*  243 */           SubmissionErrorCodes.ERROR_DATAFIELD3_RECORDNUMBER
/*  244 */           .getErrorCode(), 
/*  245 */           SubmissionErrorCodes.ERROR_DATAFIELD3_RECORDNUMBER
/*  246 */           .getErrorDescription() + 
/*  247 */           "\n" + 
/*  248 */           "RecordType:" + 
/*  249 */           recordType + 
/*  250 */           "|" + 
/*  251 */           "RecordNumber:" + 
/*  252 */           recordNumber + 
/*  253 */           "|" + 
/*  254 */           "RecordNumber:" + 
/*  255 */           "|" + 
/*  256 */           "This field length Cannot be greater than 8");
/*  257 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/*  260 */       errorObj = new ErrorObject(
/*  261 */         SubmissionErrorCodes.ERROR_DATAFIELD2_RECORDNUMBER
/*  262 */         .getErrorCode(), 
/*  263 */         SubmissionErrorCodes.ERROR_DATAFIELD2_RECORDNUMBER
/*  264 */         .getErrorDescription() + 
/*  265 */         "\n" + 
/*  266 */         "RecordType:" + 
/*  267 */         recordType + 
/*  268 */         "|" + 
/*  269 */         "RecordNumber:" + 
/*  270 */         recordNumber + 
/*  271 */         "|" + 
/*  272 */         "RecordNumber:" + 
/*  273 */         "|" + 
/*  274 */         "This field can only be Numeric");
/*  275 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTransactionIdentifier(String transactionIdentifier, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  292 */     ErrorObject errorObj = null;
/*      */     
/*  294 */     if (CommonValidator.isNullOrEmpty(transactionIdentifier)) {
/*  295 */       if (!isPaperSubmitter) {
/*  296 */         errorObj = new ErrorObject(
/*  297 */           SubmissionErrorCodes.ERROR_DATAFIELD25_TRANSACTION_IDENTIFIER
/*  298 */           .getErrorCode(), 
/*  299 */           SubmissionErrorCodes.ERROR_DATAFIELD25_TRANSACTION_IDENTIFIER
/*  300 */           .getErrorDescription() + 
/*  301 */           "\n" + 
/*  302 */           "RecordType:" + 
/*  303 */           recordType + 
/*  304 */           "|" + 
/*  305 */           "RecordNumber:" + 
/*  306 */           recordNumber + 
/*  307 */           "|" + 
/*  308 */           "TransactionIdentifier" + 
/*  309 */           "|" + 
/*  310 */           "This field is mandatory and cannot be empty");
/*  311 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*  314 */     else if (CommonValidator.validateData(transactionIdentifier, 
/*  315 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[23]))
/*      */     {
/*  317 */       String reqLength = 
/*  318 */         CommonValidator.validateLength(transactionIdentifier, 15, 15);
/*      */       
/*  320 */       if (reqLength.equals("greaterThanMax")) {
/*  321 */         errorObj = new ErrorObject(
/*  322 */           SubmissionErrorCodes.ERROR_DATAFIELD25_TRANSACTION_IDENTIFIER
/*  323 */           .getErrorCode(), 
/*  324 */           SubmissionErrorCodes.ERROR_DATAFIELD25_TRANSACTION_IDENTIFIER
/*  325 */           .getErrorDescription() + 
/*  326 */           "\n" + 
/*  327 */           "RecordType:" + 
/*  328 */           recordType + 
/*  329 */           "|" + 
/*  330 */           "RecordNumber:" + 
/*  331 */           recordNumber + 
/*  332 */           "|" + 
/*  333 */           "TransactionIdentifier" + 
/*  334 */           "|" + 
/*  335 */           "This field length Cannot be greater than 15");
/*  336 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/*  339 */       errorObj = new ErrorObject(
/*  340 */         SubmissionErrorCodes.ERROR_DATAFIELD25_TRANSACTION_IDENTIFIER
/*  341 */         .getErrorCode(), 
/*  342 */         SubmissionErrorCodes.ERROR_DATAFIELD25_TRANSACTION_IDENTIFIER
/*  343 */         .getErrorDescription() + 
/*  344 */         "\n" + 
/*  345 */         "RecordType:" + 
/*  346 */         recordType + 
/*  347 */         "|" + 
/*  348 */         "RecordNumber:" + 
/*  349 */         recordNumber + 
/*  350 */         "|" + 
/*  351 */         "TransactionIdentifier" + 
/*  352 */         "|" + 
/*  353 */         "This field can only be Numeric");
/*  354 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateFormatCode(TransactionAdviceBasicBean transactAdviceBasicType, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  372 */     ErrorObject errorObj = null;
/*      */     
/*  374 */     if (CommonValidator.isNullOrEmpty(transactAdviceBasicType
/*  375 */       .getFormatCode())) {
/*  376 */       errorObj = new ErrorObject(
/*  377 */         SubmissionErrorCodes.ERROR_DATAFIELD28_FORMAT_CODE
/*  378 */         .getErrorCode(), 
/*  379 */         SubmissionErrorCodes.ERROR_DATAFIELD28_FORMAT_CODE
/*  380 */         .getErrorDescription() + 
/*  381 */         "\n" + 
/*  382 */         "RecordType:" + 
/*  383 */         recordType + 
/*  384 */         "|" + 
/*  385 */         "RecordNumber:" + 
/*  386 */         recordNumber + 
/*  387 */         "|" + 
/*  388 */         "FormatCode" + 
/*  389 */         "|" + 
/*  390 */         "This field is mandatory and cannot be empty");
/*  391 */       errorCodes.add(errorObj);
/*      */ 
/*      */     }
/*  394 */     else if (CommonValidator.validateData(transactAdviceBasicType
/*  395 */       .getFormatCode(), com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[24]))
/*      */     {
/*  397 */       String reqLength = CommonValidator.validateLength(
/*  398 */         transactAdviceBasicType.getFormatCode(), 2, 2);
/*      */       
/*  400 */       if (reqLength.equals("greaterThanMax")) {
/*  401 */         errorObj = new ErrorObject(
/*  402 */           SubmissionErrorCodes.ERROR_DATAFIELD28_FORMAT_CODE
/*  403 */           .getErrorCode(), 
/*  404 */           SubmissionErrorCodes.ERROR_DATAFIELD28_FORMAT_CODE
/*  405 */           .getErrorDescription() + 
/*  406 */           "\n" + 
/*  407 */           "RecordType:" + 
/*  408 */           recordType + 
/*  409 */           "|" + 
/*  410 */           "RecordNumber:" + 
/*  411 */           recordNumber + 
/*  412 */           "|" + 
/*  413 */           "FormatCode" + 
/*  414 */           "|" + 
/*  415 */           "This field length Cannot be greater than 2");
/*  416 */         errorCodes.add(errorObj);
/*      */ 
/*      */ 
/*      */       }
/*  420 */       else if (CommonValidator.isValidFormatCode(transactAdviceBasicType
/*  421 */         .getFormatCode())) {
/*  422 */         if (transactAdviceBasicType.getFormatCode().equals(
/*  423 */           FormatCode.No_Industry_Specific_TAA_Record
/*  424 */           .getFormatCode())) {
/*  425 */           if ((transactAdviceBasicType
/*  426 */             .getTransactionAdviceAddendumBean() instanceof IndustryTypeTAABean))
/*      */           {
/*  428 */             errorObj = new ErrorObject(
/*  429 */               SubmissionErrorCodes.ERROR_DATAFIELD26_FORMAT_CODE
/*  430 */               .getErrorCode(), 
/*  431 */               SubmissionErrorCodes.ERROR_DATAFIELD26_FORMAT_CODE
/*  432 */               .getErrorDescription() + 
/*  433 */               "\n" + 
/*  434 */               "RecordType:" + 
/*  435 */               recordType + 
/*  436 */               "|" + 
/*  437 */               "RecordNumber:" + 
/*  438 */               recordNumber + 
/*  439 */               "|" + 
/*  440 */               "FormatCode" + 
/*  441 */               "|" + 
/*  442 */               SubmissionErrorCodes.ERROR_DATAFIELD26_FORMAT_CODE
/*  443 */               .getErrorDescription());
/*  444 */             errorCodes.add(errorObj);
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */           }
/*      */           
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */         }
/*      */         
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */       }
/*      */       else
/*      */       {
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  473 */         errorObj = new ErrorObject(
/*  474 */           SubmissionErrorCodes.ERROR_DATAFIELD28_FORMAT_CODE
/*  475 */           .getErrorCode(), 
/*  476 */           SubmissionErrorCodes.ERROR_DATAFIELD28_FORMAT_CODE
/*  477 */           .getErrorDescription() + 
/*  478 */           "\n" + 
/*  479 */           "RecordType:" + 
/*  480 */           recordType + 
/*  481 */           "|" + 
/*  482 */           "RecordNumber:" + 
/*  483 */           recordNumber + 
/*  484 */           "|" + 
/*  485 */           "FormatCode" + 
/*  486 */           "|" + 
/*  487 */           SubmissionErrorCodes.ERROR_DATAFIELD28_FORMAT_CODE
/*  488 */           .getErrorDescription());
/*  489 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */     else {
/*  493 */       errorObj = new ErrorObject(
/*  494 */         SubmissionErrorCodes.ERROR_DATAFIELD28_FORMAT_CODE
/*  495 */         .getErrorCode(), 
/*  496 */         SubmissionErrorCodes.ERROR_DATAFIELD28_FORMAT_CODE
/*  497 */         .getErrorDescription() + 
/*  498 */         "\n" + 
/*  499 */         "RecordType:" + 
/*  500 */         recordType + 
/*  501 */         "|" + 
/*  502 */         "RecordNumber:" + 
/*  503 */         recordNumber + 
/*  504 */         "|" + 
/*  505 */         "FormatCode" + 
/*  506 */         "|" + 
/*  507 */         "This field can only be AlphaNumeric");
/*  508 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateMediaCode(String mediaCode, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  525 */     ErrorObject errorObj = null;
/*      */     
/*  527 */     if (!CommonValidator.isNullOrEmpty(mediaCode)) {
/*  528 */       if (CommonValidator.validateData(mediaCode, 
/*  529 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[25])) {
/*  530 */         String reqLength = CommonValidator.validateLength(mediaCode, 2, 2);
/*      */         
/*  532 */         if (reqLength.equals("greaterThanMax")) {
/*  533 */           errorObj = new ErrorObject(
/*  534 */             SubmissionErrorCodes.ERROR_DATAFIELD29_MEDIA_CODE
/*  535 */             .getErrorCode(), 
/*  536 */             SubmissionErrorCodes.ERROR_DATAFIELD29_MEDIA_CODE
/*  537 */             .getErrorDescription() + 
/*  538 */             "\n" + 
/*  539 */             "RecordType:" + 
/*  540 */             recordType + 
/*  541 */             "|" + 
/*  542 */             "RecordNumber:" + 
/*  543 */             recordNumber + 
/*  544 */             "|" + 
/*  545 */             "MediaCode" + 
/*  546 */             "|" + 
/*  547 */             "This field length Cannot be greater than 2");
/*  548 */           errorCodes.add(errorObj);
/*      */ 
/*      */         }
/*  551 */         else if (!CommonValidator.isValidMediaCode(mediaCode)) {
/*  552 */           errorObj = new ErrorObject(
/*  553 */             SubmissionErrorCodes.ERROR_DATAFIELD29_MEDIA_CODE
/*  554 */             .getErrorCode(), 
/*  555 */             SubmissionErrorCodes.ERROR_DATAFIELD29_MEDIA_CODE
/*  556 */             .getErrorDescription() + 
/*  557 */             "\n" + 
/*  558 */             "RecordType:" + 
/*  559 */             recordType + 
/*  560 */             "|" + 
/*  561 */             "RecordNumber:" + 
/*  562 */             recordNumber + 
/*  563 */             "|" + 
/*  564 */             "MediaCode" + 
/*  565 */             "|" + 
/*  566 */             SubmissionErrorCodes.ERROR_DATAFIELD29_MEDIA_CODE
/*  567 */             .getErrorDescription());
/*  568 */           errorCodes.add(errorObj);
/*      */         }
/*      */       }
/*      */       else {
/*  572 */         errorObj = new ErrorObject(
/*  573 */           SubmissionErrorCodes.ERROR_DATAFIELD29_MEDIA_CODE
/*  574 */           .getErrorCode(), 
/*  575 */           SubmissionErrorCodes.ERROR_DATAFIELD29_MEDIA_CODE
/*  576 */           .getErrorDescription() + 
/*  577 */           "\n" + 
/*  578 */           "RecordType:" + 
/*  579 */           recordType + 
/*  580 */           "|" + 
/*  581 */           "RecordNumber:" + 
/*  582 */           recordNumber + 
/*  583 */           "|" + 
/*  584 */           "MediaCode" + 
/*  585 */           "|" + 
/*  586 */           "This field can only be AlphaNumeric");
/*  587 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateSubmissionMethod(String submissionMethod, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  604 */     ErrorObject errorObj = null;
/*      */     
/*  606 */     if (!CommonValidator.isNullOrEmpty(submissionMethod))
/*      */     {
/*  608 */       if (CommonValidator.validateData(submissionMethod, 
/*  609 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[26]))
/*      */       {
/*  611 */         String reqLength = CommonValidator.validateLength(submissionMethod, 2, 2);
/*      */         
/*  613 */         if (reqLength.equals("greaterThanMax")) {
/*  614 */           errorObj = new ErrorObject(
/*  615 */             SubmissionErrorCodes.ERROR_DATAFIELD30_SUBMISSION_METHOD
/*  616 */             .getErrorCode(), 
/*  617 */             SubmissionErrorCodes.ERROR_DATAFIELD30_SUBMISSION_METHOD
/*  618 */             .getErrorDescription() + 
/*  619 */             "\n" + 
/*  620 */             "RecordType:" + 
/*  621 */             recordType + 
/*  622 */             "|" + 
/*  623 */             "RecordNumber:" + 
/*  624 */             recordNumber + 
/*  625 */             "|" + 
/*  626 */             "SubmissionMethod" + 
/*  627 */             "|" + 
/*  628 */             "This field length Cannot be greater than 2");
/*  629 */           errorCodes.add(errorObj);
/*      */         }
/*  631 */         else if (!CommonValidator.isValidSubCode(submissionMethod)) {
/*  632 */           errorObj = new ErrorObject(
/*  633 */             SubmissionErrorCodes.ERROR_DATAFIELD30_SUBMISSION_METHOD
/*  634 */             .getErrorCode(), 
/*  635 */             SubmissionErrorCodes.ERROR_DATAFIELD30_SUBMISSION_METHOD
/*  636 */             .getErrorDescription() + 
/*  637 */             "\n" + 
/*  638 */             "RecordType:" + 
/*  639 */             recordType + 
/*  640 */             "|" + 
/*  641 */             "RecordNumber:" + 
/*  642 */             recordNumber + 
/*  643 */             "|" + 
/*  644 */             "SubmissionMethod" + 
/*  645 */             "|" + 
/*  646 */             SubmissionErrorCodes.ERROR_DATAFIELD30_SUBMISSION_METHOD
/*  647 */             .getErrorDescription());
/*  648 */           errorCodes.add(errorObj);
/*      */         }
/*      */       }
/*      */       else {
/*  652 */         errorObj = new ErrorObject(
/*  653 */           SubmissionErrorCodes.ERROR_DATAFIELD30_SUBMISSION_METHOD
/*  654 */           .getErrorCode(), 
/*  655 */           SubmissionErrorCodes.ERROR_DATAFIELD30_SUBMISSION_METHOD
/*  656 */           .getErrorDescription() + 
/*  657 */           "\n" + 
/*  658 */           "RecordType:" + 
/*  659 */           recordType + 
/*  660 */           "|" + 
/*  661 */           "RecordNumber:" + 
/*  662 */           recordNumber + 
/*  663 */           "|" + 
/*  664 */           "SubmissionMethod" + 
/*  665 */           "|" + 
/*  666 */           "This field can only be AlphaNumeric");
/*  667 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateApprovalCode(TransactionAdviceBasicBean transactAdviceBasicType, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  685 */     ErrorObject errorObj = null;
/*  686 */     String approvalCode = transactAdviceBasicType.getApprovalCode();
/*  687 */     if (CommonValidator.isNullOrEmpty(approvalCode))
/*      */     {
/*      */ 
/*  690 */       if (!transactAdviceBasicType.getProcessingCode().equalsIgnoreCase(ProcessingCode.CREDIT.getProcessingCode())) {
/*  691 */         errorObj = new ErrorObject(
/*  692 */           SubmissionErrorCodes.ERROR_DATAFIELD32_APPROVAL_CODE
/*  693 */           .getErrorCode(), 
/*  694 */           SubmissionErrorCodes.ERROR_DATAFIELD32_APPROVAL_CODE
/*  695 */           .getErrorDescription() + 
/*  696 */           "\n" + 
/*  697 */           "RecordType:" + 
/*  698 */           recordType + 
/*  699 */           "|" + 
/*  700 */           "RecordNumber:" + 
/*  701 */           recordNumber + 
/*  702 */           "|" + 
/*  703 */           "ApprovalCode" + 
/*  704 */           "|" + 
/*  705 */           "This field is mandatory and cannot be empty");
/*  706 */         errorCodes.add(errorObj);
/*      */       }
/*      */       
/*      */     }
/*  710 */     else if (CommonValidator.validateData(approvalCode, 
/*  711 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[28]))
/*      */     {
/*  713 */       String reqLength = CommonValidator.validateLength(approvalCode, 
/*  714 */         6, 6);
/*  715 */       if (reqLength.equals("greaterThanMax")) {
/*  716 */         errorObj = new ErrorObject(
/*  717 */           SubmissionErrorCodes.ERROR_DATAFIELD32_APPROVAL_CODE
/*  718 */           .getErrorCode(), 
/*  719 */           SubmissionErrorCodes.ERROR_DATAFIELD32_APPROVAL_CODE
/*  720 */           .getErrorDescription() + 
/*  721 */           "\n" + 
/*  722 */           "RecordType:" + 
/*  723 */           recordType + 
/*  724 */           "|" + 
/*  725 */           "RecordNumber:" + 
/*  726 */           recordNumber + 
/*  727 */           "|" + 
/*  728 */           "ApprovalCode" + 
/*  729 */           "|" + 
/*  730 */           "This field length Cannot be greater than 6");
/*  731 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/*  734 */       errorObj = new ErrorObject(
/*  735 */         SubmissionErrorCodes.ERROR_DATAFIELD31_APPROVAL_CODE
/*  736 */         .getErrorCode(), 
/*  737 */         SubmissionErrorCodes.ERROR_DATAFIELD31_APPROVAL_CODE
/*  738 */         .getErrorDescription() + 
/*  739 */         "\n" + 
/*  740 */         "RecordType:" + 
/*  741 */         recordType + 
/*  742 */         "|" + 
/*  743 */         "RecordNumber:" + 
/*  744 */         recordNumber + 
/*  745 */         "|" + 
/*  746 */         "ApprovalCode" + 
/*  747 */         "|" + 
/*  748 */         "This field can only be AlphaNumeric");
/*  749 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validatePrimaryAccountNumber(String primaryAccountNo, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  766 */     ErrorObject errorObj = null;
/*      */     
/*  768 */     if (CommonValidator.isNullOrEmpty(primaryAccountNo))
/*      */     {
/*  770 */       errorObj = new ErrorObject(
/*  771 */         SubmissionErrorCodes.ERROR_DATAFIELD35_PRIMARYAC_NUMBER
/*  772 */         .getErrorCode(), 
/*  773 */         SubmissionErrorCodes.ERROR_DATAFIELD35_PRIMARYAC_NUMBER
/*  774 */         .getErrorDescription() + 
/*  775 */         "\n" + 
/*  776 */         "RecordType:" + 
/*  777 */         recordType + 
/*  778 */         "|" + 
/*  779 */         "RecordNumber:" + 
/*  780 */         recordNumber + 
/*  781 */         "|" + 
/*  782 */         "PrimaryAccountNumber" + 
/*  783 */         "|" + 
/*  784 */         "This field is mandatory and cannot be empty");
/*  785 */       errorCodes.add(errorObj);
/*      */ 
/*      */     }
/*  788 */     else if (CommonValidator.validateData(primaryAccountNo, 
/*  789 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[29]))
/*      */     {
/*  791 */       String reqLength = 
/*  792 */         CommonValidator.validateLength(primaryAccountNo, 19, 19);
/*  793 */       if (reqLength.equals("greaterThanMax")) {
/*  794 */         errorObj = new ErrorObject(
/*      */         
/*      */ 
/*      */ 
/*      */ 
/*  799 */           SubmissionErrorCodes.ERROR_DATAFIELD602_PRIMARYAC_NUMBER
/*  800 */           .getErrorCode(), 
/*  801 */           SubmissionErrorCodes.ERROR_DATAFIELD602_PRIMARYAC_NUMBER
/*  802 */           .getErrorDescription() + 
/*  803 */           "\n" + 
/*  804 */           "RecordType:" + 
/*  805 */           recordType + 
/*  806 */           "|" + 
/*  807 */           "RecordNumber:" + 
/*  808 */           recordNumber + 
/*  809 */           "|" + 
/*  810 */           "PrimaryAccountNumber" + 
/*  811 */           "|" + 
/*  812 */           "This field length Cannot be greater than 19");
/*  813 */         errorCodes.add(errorObj);
/*      */       }
/*  815 */       else if (!CommonValidator.isValidModulusTenCheck(primaryAccountNo)) {
/*  816 */         errorObj = new ErrorObject(
/*      */         
/*      */ 
/*      */ 
/*      */ 
/*  821 */           SubmissionErrorCodes.ERROR_DATAFIELD603_PRIMARYAC_NUMBER
/*  822 */           .getErrorCode(), 
/*  823 */           SubmissionErrorCodes.ERROR_DATAFIELD603_PRIMARYAC_NUMBER
/*  824 */           .getErrorDescription() + 
/*  825 */           "\n" + 
/*  826 */           "RecordType:" + 
/*  827 */           recordType + 
/*  828 */           "|" + 
/*  829 */           "RecordNumber:" + 
/*  830 */           recordNumber + 
/*  831 */           "|" + 
/*  832 */           "PrimaryAccountNumber" + 
/*  833 */           "|" + 
/*  834 */           SubmissionErrorCodes.ERROR_DATAFIELD603_PRIMARYAC_NUMBER
/*  835 */           .getErrorDescription());
/*  836 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */     else {
/*  840 */       errorObj = new ErrorObject(
/*      */       
/*  842 */         SubmissionErrorCodes.ERROR_DATAFIELD601_PRIMARYAC_NUMBER
/*  843 */         .getErrorCode(), 
/*  844 */         SubmissionErrorCodes.ERROR_DATAFIELD601_PRIMARYAC_NUMBER
/*      */         
/*      */ 
/*      */ 
/*  848 */         .getErrorDescription() + 
/*  849 */         "\n" + 
/*  850 */         "RecordType:" + 
/*  851 */         recordType + 
/*  852 */         "|" + 
/*  853 */         "RecordNumber:" + 
/*  854 */         recordNumber + 
/*  855 */         "|" + 
/*  856 */         "PrimaryAccountNumber" + 
/*  857 */         "|" + 
/*  858 */         "This field can only be AlphaNumeric");
/*  859 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateCardExpiryDate(String cardExpiryDate, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  876 */     ErrorObject errorObj = null;
/*      */     
/*  878 */     if (!CommonValidator.isNullOrEmpty(cardExpiryDate)) {
/*  879 */       if (CommonValidator.validateData(cardExpiryDate, 
/*  880 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[30]))
/*      */       {
/*  882 */         String reqLength = CommonValidator.validateLength(cardExpiryDate, 4, 4);
/*  883 */         if ((!reqLength.equals("equal")) || 
/*  884 */           (!CommonValidator.isValidDate(cardExpiryDate, 
/*  885 */           "YYMM"))) {
/*  886 */           errorObj = new ErrorObject(
/*  887 */             SubmissionErrorCodes.ERROR_DATAFIELD37_CARDEXPIRY_DATE
/*  888 */             .getErrorCode(), 
/*  889 */             SubmissionErrorCodes.ERROR_DATAFIELD37_CARDEXPIRY_DATE
/*  890 */             .getErrorDescription() + 
/*  891 */             "\n" + 
/*  892 */             "RecordType:" + 
/*  893 */             recordType + 
/*  894 */             "|" + 
/*  895 */             "RecordNumber:" + 
/*  896 */             recordNumber + 
/*  897 */             "|" + 
/*  898 */             "CardExpiryDate" + 
/*  899 */             "|" + 
/*  900 */             SubmissionErrorCodes.ERROR_DATAFIELD37_CARDEXPIRY_DATE
/*  901 */             .getErrorDescription());
/*  902 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/*  905 */         errorObj = new ErrorObject(
/*  906 */           SubmissionErrorCodes.ERROR_DATAFIELD37_CARDEXPIRY_DATE
/*  907 */           .getErrorCode(), 
/*  908 */           SubmissionErrorCodes.ERROR_DATAFIELD37_CARDEXPIRY_DATE
/*  909 */           .getErrorDescription() + 
/*  910 */           "\n" + 
/*  911 */           "RecordType:" + 
/*  912 */           recordType + 
/*  913 */           "|" + 
/*  914 */           "RecordNumber:" + 
/*  915 */           recordNumber + 
/*  916 */           "|" + 
/*  917 */           "CardExpiryDate" + 
/*  918 */           "|" + 
/*  919 */           "This field can only be Numeric");
/*  920 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTransactionDate(String transactionDate, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/*  937 */     ErrorObject errorObj = null;
/*      */     
/*  939 */     if (CommonValidator.isNullOrEmpty(transactionDate)) {
/*  940 */       errorObj = new ErrorObject(
/*  941 */         SubmissionErrorCodes.ERROR_DATAFIELD38_TRANSACTION_DATE
/*  942 */         .getErrorCode(), 
/*  943 */         SubmissionErrorCodes.ERROR_DATAFIELD38_TRANSACTION_DATE
/*  944 */         .getErrorDescription() + 
/*  945 */         "\n" + 
/*  946 */         "RecordType:" + 
/*  947 */         recordType + 
/*  948 */         "|" + 
/*  949 */         "RecordNumber:" + 
/*  950 */         recordNumber + 
/*  951 */         "|" + 
/*  952 */         "TransactionDate" + 
/*  953 */         "|" + 
/*  954 */         "This field is mandatory and cannot be empty");
/*  955 */       errorCodes.add(errorObj);
/*      */     }
/*  957 */     else if (CommonValidator.validateData(transactionDate, 
/*  958 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[31])) {
/*  959 */       String reqLength = CommonValidator.validateLength(transactionDate, 8, 8);
/*  960 */       if (reqLength.equals("greaterThanMax")) {
/*  961 */         errorObj = new ErrorObject(
/*  962 */           SubmissionErrorCodes.ERROR_DATAFIELD38_TRANSACTION_DATE
/*  963 */           .getErrorCode(), 
/*  964 */           SubmissionErrorCodes.ERROR_DATAFIELD38_TRANSACTION_DATE
/*  965 */           .getErrorDescription() + 
/*  966 */           "\n" + 
/*  967 */           "RecordType:" + 
/*  968 */           recordType + 
/*  969 */           "|" + 
/*  970 */           "RecordNumber:" + 
/*  971 */           recordNumber + 
/*  972 */           "|" + 
/*  973 */           "TransactionDate" + 
/*  974 */           "|" + 
/*  975 */           "This field length Cannot be greater than 8");
/*  976 */         errorCodes.add(errorObj);
/*      */       }
/*  978 */       else if (CommonValidator.isValidDate(transactionDate, 
/*  979 */         "CCYYMMDD")) {
/*  980 */         String dateStatus = 
/*  981 */           CommonValidator.validateTransactionDate(transactionDate);
/*  982 */         if (dateStatus.equals("expired"))
/*      */         {
/*  984 */           errorObj = new ErrorObject(
/*  985 */             SubmissionErrorCodes.ERROR_DATAFIELD40_TRANSACTION_DATE);
/*  986 */           errorCodes.add(errorObj);
/*      */         }
/*  988 */         else if (dateStatus.equals("futureDate")) {
/*  989 */           errorObj = new ErrorObject(
/*  990 */             SubmissionErrorCodes.ERROR_DATAFIELD39_TRANSACTION_DATE);
/*  991 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/*  994 */         errorObj = new ErrorObject(
/*  995 */           SubmissionErrorCodes.ERROR_DATAFIELD38_TRANSACTION_DATE);
/*  996 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */     else {
/* 1000 */       errorObj = new ErrorObject(
/* 1001 */         SubmissionErrorCodes.ERROR_DATAFIELD38_TRANSACTION_DATE
/* 1002 */         .getErrorCode(), 
/* 1003 */         SubmissionErrorCodes.ERROR_DATAFIELD38_TRANSACTION_DATE
/* 1004 */         .getErrorDescription() + 
/* 1005 */         "\n" + 
/* 1006 */         "RecordType:" + 
/* 1007 */         recordType + 
/* 1008 */         "|" + 
/* 1009 */         "RecordNumber:" + 
/* 1010 */         recordNumber + 
/* 1011 */         "|" + 
/* 1012 */         "TransactionDate" + 
/* 1013 */         "|" + 
/* 1014 */         "This field can only be Numeric");
/* 1015 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTransactionTime(String transactionTime, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1032 */     ErrorObject errorObj = null;
/*      */     
/* 1034 */     if (!CommonValidator.isNullOrEmpty(transactionTime)) {
/* 1035 */       if (CommonValidator.validateData(transactionTime, 
/* 1036 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[32])) {
/* 1037 */         String reqLength = CommonValidator.validateLength(transactionTime, 6, 6);
/* 1038 */         if ((!reqLength.equals("equal")) || 
/* 1039 */           (!CommonValidator.validateTime(transactionTime))) {
/* 1040 */           errorObj = new ErrorObject(
/* 1041 */             SubmissionErrorCodes.ERROR_DATAFIELD41_TRANSACTION_TIME
/* 1042 */             .getErrorCode(), 
/* 1043 */             SubmissionErrorCodes.ERROR_DATAFIELD41_TRANSACTION_TIME
/* 1044 */             .getErrorDescription() + 
/* 1045 */             "\n" + 
/* 1046 */             "RecordType:" + 
/* 1047 */             recordType + 
/* 1048 */             "|" + 
/* 1049 */             "RecordNumber:" + 
/* 1050 */             recordNumber + 
/* 1051 */             "|" + 
/* 1052 */             "TransactionTime" + 
/* 1053 */             "|" + 
/* 1054 */             "This field length should be 6");
/* 1055 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/* 1058 */         errorObj = new ErrorObject(
/* 1059 */           SubmissionErrorCodes.ERROR_DATAFIELD41_TRANSACTION_TIME
/* 1060 */           .getErrorCode(), 
/* 1061 */           SubmissionErrorCodes.ERROR_DATAFIELD41_TRANSACTION_TIME
/* 1062 */           .getErrorDescription() + 
/* 1063 */           "\n" + 
/* 1064 */           "RecordType:" + 
/* 1065 */           recordType + 
/* 1066 */           "|" + 
/* 1067 */           "RecordNumber:" + 
/* 1068 */           recordNumber + 
/* 1069 */           "|" + 
/* 1070 */           "TransactionTime" + 
/* 1071 */           "|" + 
/* 1072 */           "This field can only be Numeric");
/* 1073 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTransactionAmount(TransactionAdviceBasicBean transactAdviceBasicType, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1091 */     ErrorObject errorObj = null;
/* 1092 */     String value = transactAdviceBasicType.getTransactionAmount();
/* 1093 */     if (CommonValidator.isNullOrEmpty(value)) {
/* 1094 */       errorObj = new ErrorObject(
/* 1095 */         SubmissionErrorCodes.ERROR_DATAFIELD42_TRANSACTION_AMOUNT
/* 1096 */         .getErrorCode(), 
/* 1097 */         SubmissionErrorCodes.ERROR_DATAFIELD42_TRANSACTION_AMOUNT
/* 1098 */         .getErrorDescription() + 
/* 1099 */         "\n" + 
/* 1100 */         "RecordType:" + 
/* 1101 */         recordType + 
/* 1102 */         "|" + 
/* 1103 */         "RecordNumber:" + 
/* 1104 */         recordNumber + 
/* 1105 */         "|" + 
/* 1106 */         "TransactionAmount" + 
/* 1107 */         "|" + 
/* 1108 */         "This field is mandatory and cannot be empty");
/* 1109 */       errorCodes.add(errorObj);
/*      */     }
/* 1111 */     else if (CommonValidator.validateData(value, 
/* 1112 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[34])) {
/* 1113 */       String reqLength = 
/* 1114 */         CommonValidator.validateLength(value, 12, 12);
/*      */       
/* 1116 */       if (reqLength.equals("greaterThanMax")) {
/* 1117 */         errorObj = new ErrorObject(
/* 1118 */           SubmissionErrorCodes.ERROR_DATAFIELD42_TRANSACTION_AMOUNT
/* 1119 */           .getErrorCode(), 
/* 1120 */           SubmissionErrorCodes.ERROR_DATAFIELD42_TRANSACTION_AMOUNT
/* 1121 */           .getErrorDescription() + 
/* 1122 */           "\n" + 
/* 1123 */           "RecordType:" + 
/* 1124 */           recordType + 
/* 1125 */           "|" + 
/* 1126 */           "RecordNumber:" + 
/* 1127 */           recordNumber + 
/* 1128 */           "|" + 
/* 1129 */           "TransactionAmount" + 
/* 1130 */           "|" + 
/* 1131 */           "This field length Cannot be greater than 12");
/* 1132 */         errorCodes.add(errorObj);
/*      */       }
/* 1134 */       else if (!CommonValidator.isValidAmount(
/* 1135 */         transactAdviceBasicType
/* 1136 */         .getTransactionCurrencyCode(), value)) {
/* 1137 */         errorObj = new ErrorObject(
/* 1138 */           SubmissionErrorCodes.ERROR_DATAFIELD43_TRANSACTION_AMOUNT
/* 1139 */           .getErrorCode(), 
/* 1140 */           SubmissionErrorCodes.ERROR_DATAFIELD43_TRANSACTION_AMOUNT
/* 1141 */           .getErrorDescription() + 
/* 1142 */           "\n" + 
/* 1143 */           "RecordType:" + 
/* 1144 */           recordType + 
/* 1145 */           "|" + 
/* 1146 */           "RecordNumber:" + 
/* 1147 */           recordNumber + 
/* 1148 */           "|" + 
/* 1149 */           "TransactionAmount" + 
/* 1150 */           "|" + 
/* 1151 */           SubmissionErrorCodes.ERROR_DATAFIELD43_TRANSACTION_AMOUNT
/* 1152 */           .getErrorDescription());
/* 1153 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */     else {
/* 1157 */       errorObj = new ErrorObject(
/* 1158 */         SubmissionErrorCodes.ERROR_DATAFIELD42_TRANSACTION_AMOUNT
/* 1159 */         .getErrorCode(), 
/* 1160 */         SubmissionErrorCodes.ERROR_DATAFIELD42_TRANSACTION_AMOUNT
/* 1161 */         .getErrorDescription() + 
/* 1162 */         "\n" + 
/* 1163 */         "RecordType:" + 
/* 1164 */         recordType + 
/* 1165 */         "|" + 
/* 1166 */         "RecordNumber:" + 
/* 1167 */         recordNumber + 
/* 1168 */         "|" + 
/* 1169 */         "TransactionAmount" + 
/* 1170 */         "|" + 
/* 1171 */         "This field can only be Numeric");
/* 1172 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateProcessingCode(String processingCode, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1190 */     ErrorObject errorObj = null;
/*      */     
/* 1192 */     if (CommonValidator.isNullOrEmpty(processingCode)) {
/* 1193 */       errorObj = new ErrorObject(
/* 1194 */         SubmissionErrorCodes.ERROR_DATAFIELD45_PROSESSING_CODE
/* 1195 */         .getErrorCode(), 
/* 1196 */         SubmissionErrorCodes.ERROR_DATAFIELD45_PROSESSING_CODE
/* 1197 */         .getErrorDescription() + 
/* 1198 */         "\n" + 
/* 1199 */         "RecordType:" + 
/* 1200 */         recordType + 
/* 1201 */         "|" + 
/* 1202 */         "RecordNumber:" + 
/* 1203 */         recordNumber + 
/* 1204 */         "|" + 
/* 1205 */         "ProcessingCode" + 
/* 1206 */         "|" + 
/* 1207 */         "This field is mandatory and cannot be empty");
/* 1208 */       errorCodes.add(errorObj);
/*      */ 
/*      */ 
/*      */     }
/* 1212 */     else if (CommonValidator.validateData(processingCode, 
/* 1213 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[35])) {
/* 1214 */       String reqLength = CommonValidator.validateLength(processingCode, 6, 6);
/* 1215 */       if (reqLength.equals("greaterThanMax")) {
/* 1216 */         errorObj = new ErrorObject(
/* 1217 */           SubmissionErrorCodes.ERROR_DATAFIELD45_PROSESSING_CODE
/* 1218 */           .getErrorCode(), 
/* 1219 */           SubmissionErrorCodes.ERROR_DATAFIELD45_PROSESSING_CODE
/* 1220 */           .getErrorDescription() + 
/* 1221 */           "\n" + 
/* 1222 */           "RecordType:" + 
/* 1223 */           recordType + 
/* 1224 */           "|" + 
/* 1225 */           "RecordNumber:" + 
/* 1226 */           recordNumber + 
/* 1227 */           "|" + 
/* 1228 */           "ProcessingCode" + 
/* 1229 */           "|" + 
/* 1230 */           "This field length Cannot be greater than 6");
/* 1231 */         errorCodes.add(errorObj);
/*      */       }
/* 1233 */       else if (!CommonValidator.isValidProcessingCode(processingCode)) {
/* 1234 */         errorObj = new ErrorObject(
/* 1235 */           SubmissionErrorCodes.ERROR_DATAFIELD45_PROSESSING_CODE
/* 1236 */           .getErrorCode(), 
/* 1237 */           SubmissionErrorCodes.ERROR_DATAFIELD45_PROSESSING_CODE
/* 1238 */           .getErrorDescription() + 
/* 1239 */           "\n" + 
/* 1240 */           "RecordType:" + 
/* 1241 */           recordType + 
/* 1242 */           "|" + 
/* 1243 */           "RecordNumber:" + 
/* 1244 */           recordNumber + 
/* 1245 */           "|" + 
/* 1246 */           "ProcessingCode" + 
/* 1247 */           "|" + 
/* 1248 */           SubmissionErrorCodes.ERROR_DATAFIELD45_PROSESSING_CODE
/* 1249 */           .getErrorDescription());
/* 1250 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */     else {
/* 1254 */       errorObj = new ErrorObject(
/* 1255 */         SubmissionErrorCodes.ERROR_DATAFIELD45_PROSESSING_CODE
/* 1256 */         .getErrorCode(), 
/* 1257 */         SubmissionErrorCodes.ERROR_DATAFIELD45_PROSESSING_CODE
/* 1258 */         .getErrorDescription() + 
/* 1259 */         "\n" + 
/* 1260 */         "RecordType:" + 
/* 1261 */         recordType + 
/* 1262 */         "|" + 
/* 1263 */         "RecordNumber:" + 
/* 1264 */         recordNumber + 
/* 1265 */         "|" + 
/* 1266 */         "ProcessingCode" + 
/* 1267 */         "|" + 
/* 1268 */         "This field can only be Numeric");
/* 1269 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTransactionCurrencyCode(String transactionCurrencyCode, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1286 */     ErrorObject errorObj = null;
/*      */     
/* 1288 */     if (CommonValidator.isNullOrEmpty(transactionCurrencyCode)) {
/* 1289 */       errorObj = new ErrorObject(
/* 1290 */         SubmissionErrorCodes.ERROR_DATAFIELD46_CURRENCY_CODE
/* 1291 */         .getErrorCode(), 
/* 1292 */         SubmissionErrorCodes.ERROR_DATAFIELD46_CURRENCY_CODE
/* 1293 */         .getErrorDescription() + 
/* 1294 */         "\n" + 
/* 1295 */         "RecordType:" + 
/* 1296 */         recordType + 
/* 1297 */         "|" + 
/* 1298 */         "RecordNumber:" + 
/* 1299 */         recordNumber + 
/* 1300 */         "|" + 
/* 1301 */         "TransactionCurrencyCode" + 
/* 1302 */         "|" + 
/* 1303 */         "This field is mandatory and cannot be empty");
/* 1304 */       errorCodes.add(errorObj);
/*      */ 
/*      */     }
/* 1307 */     else if (CommonValidator.validateData(transactionCurrencyCode, 
/* 1308 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[36])) {
/* 1309 */       String reqLength = CommonValidator.validateLength(transactionCurrencyCode, 3, 3);
/* 1310 */       if (reqLength.equals("greaterThanMax")) {
/* 1311 */         errorObj = new ErrorObject(
/* 1312 */           SubmissionErrorCodes.ERROR_DATAFIELD46_CURRENCY_CODE
/* 1313 */           .getErrorCode(), 
/* 1314 */           SubmissionErrorCodes.ERROR_DATAFIELD46_CURRENCY_CODE
/* 1315 */           .getErrorDescription() + 
/* 1316 */           "\n" + 
/* 1317 */           "RecordType:" + 
/* 1318 */           recordType + 
/* 1319 */           "|" + 
/* 1320 */           "RecordNumber:" + 
/* 1321 */           recordNumber + 
/* 1322 */           "|" + 
/* 1323 */           "TransactionCurrencyCode" + 
/* 1324 */           "|" + 
/* 1325 */           "This field length Cannot be greater than 3");
/* 1326 */         errorCodes.add(errorObj);
/*      */       }
/* 1328 */       else if (CommonValidator.isProhibitedCurrencyCode(transactionCurrencyCode)) {
/* 1329 */         errorObj = new ErrorObject(
/* 1330 */           SubmissionErrorCodes.ERROR_DATAFIELD47_CURRENCY_CODE
/* 1331 */           .getErrorCode(), 
/* 1332 */           SubmissionErrorCodes.ERROR_DATAFIELD47_CURRENCY_CODE
/* 1333 */           .getErrorDescription() + 
/* 1334 */           "\n" + 
/* 1335 */           "RecordType:" + 
/* 1336 */           recordType + 
/* 1337 */           "|" + 
/* 1338 */           "RecordNumber:" + 
/* 1339 */           recordNumber + 
/* 1340 */           "|" + 
/* 1341 */           "TransactionCurrencyCode" + 
/* 1342 */           "|" + 
/* 1343 */           SubmissionErrorCodes.ERROR_DATAFIELD47_CURRENCY_CODE
/* 1344 */           .getErrorDescription());
/* 1345 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */     else {
/* 1349 */       errorObj = new ErrorObject(
/* 1350 */         SubmissionErrorCodes.ERROR_DATAFIELD46_CURRENCY_CODE
/* 1351 */         .getErrorCode(), 
/* 1352 */         SubmissionErrorCodes.ERROR_DATAFIELD46_CURRENCY_CODE
/* 1353 */         .getErrorDescription() + 
/* 1354 */         "\n" + 
/* 1355 */         "RecordType:" + 
/* 1356 */         recordType + 
/* 1357 */         "|" + 
/* 1358 */         "RecordNumber:" + 
/* 1359 */         recordNumber + 
/* 1360 */         "|" + 
/* 1361 */         "TransactionCurrencyCode" + 
/* 1362 */         "|" + 
/* 1363 */         "This field can only be AlphaNumeric");
/* 1364 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateExtendedPaymentData(String extendedPaymentData, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1381 */     ErrorObject errorObj = null;
/*      */     
/* 1383 */     if (CommonValidator.isNullOrEmpty(extendedPaymentData)) {
/* 1384 */       errorObj = new ErrorObject(
/* 1385 */         SubmissionErrorCodes.ERROR_DATAFIELD49_EXTENDEDPAYMENT_DATA
/* 1386 */         .getErrorCode(), 
/* 1387 */         SubmissionErrorCodes.ERROR_DATAFIELD49_EXTENDEDPAYMENT_DATA
/* 1388 */         .getErrorDescription() + 
/* 1389 */         "\n" + 
/* 1390 */         "RecordType:" + 
/* 1391 */         recordType + 
/* 1392 */         "|" + 
/* 1393 */         "RecordNumber:" + 
/* 1394 */         recordNumber + 
/* 1395 */         "|" + 
/* 1396 */         "ExtendedPaymentData" + 
/* 1397 */         "|" + 
/* 1398 */         "This field is mandatory and cannot be empty");
/* 1399 */       errorCodes.add(errorObj);
/*      */     }
/* 1401 */     else if (CommonValidator.validateData(extendedPaymentData, 
/* 1402 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[37])) {
/* 1403 */       String reqLength = CommonValidator.validateLength(extendedPaymentData, 2, 2);
/* 1404 */       if (reqLength.equals("greaterThanMax")) {
/* 1405 */         errorObj = new ErrorObject(
/* 1406 */           SubmissionErrorCodes.ERROR_DATAFIELD49_EXTENDEDPAYMENT_DATA
/* 1407 */           .getErrorCode(), 
/* 1408 */           SubmissionErrorCodes.ERROR_DATAFIELD49_EXTENDEDPAYMENT_DATA
/* 1409 */           .getErrorDescription() + 
/* 1410 */           "\n" + 
/* 1411 */           "RecordType:" + 
/* 1412 */           recordType + 
/* 1413 */           "|" + 
/* 1414 */           "RecordNumber:" + 
/* 1415 */           recordNumber + 
/* 1416 */           "|" + 
/* 1417 */           "ExtendedPaymentData" + 
/* 1418 */           "|" + 
/* 1419 */           "This field length Cannot be greater than 2");
/* 1420 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/* 1423 */       errorObj = new ErrorObject(
/* 1424 */         SubmissionErrorCodes.ERROR_DATAFIELD49_EXTENDEDPAYMENT_DATA
/* 1425 */         .getErrorCode(), 
/* 1426 */         SubmissionErrorCodes.ERROR_DATAFIELD49_EXTENDEDPAYMENT_DATA
/* 1427 */         .getErrorDescription() + 
/* 1428 */         "\n" + 
/* 1429 */         "RecordType:" + 
/* 1430 */         recordType + 
/* 1431 */         "|" + 
/* 1432 */         "RecordNumber:" + 
/* 1433 */         recordNumber + 
/* 1434 */         "|" + 
/* 1435 */         "ExtendedPaymentData" + 
/* 1436 */         "|" + 
/* 1437 */         "This field can only be Numeric");
/* 1438 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateMerchantId(String merchantID, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1455 */     ErrorObject errorObj = null;
/*      */     
/* 1457 */     if (CommonValidator.isNullOrEmpty(merchantID)) {
/* 1458 */       errorObj = new ErrorObject(
/* 1459 */         SubmissionErrorCodes.ERROR_DATAFIELD52_MERCHANT_DATA
/* 1460 */         .getErrorCode(), 
/* 1461 */         SubmissionErrorCodes.ERROR_DATAFIELD52_MERCHANT_DATA
/* 1462 */         .getErrorDescription() + 
/* 1463 */         "\n" + 
/* 1464 */         "RecordType:" + 
/* 1465 */         recordType + 
/* 1466 */         "|" + 
/* 1467 */         "RecordNumber:" + 
/* 1468 */         recordNumber + 
/* 1469 */         "|" + 
/* 1470 */         "MerchantId" + 
/* 1471 */         "|" + 
/* 1472 */         "This field is mandatory and cannot be empty");
/* 1473 */       errorCodes.add(errorObj);
/*      */     }
/* 1475 */     else if (CommonValidator.validateData(merchantID, 
/* 1476 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[38]))
/*      */     {
/* 1478 */       String reqLength = 
/* 1479 */         CommonValidator.validateLength(merchantID, 15, 15);
/* 1480 */       if (reqLength.equals("greaterThanMax")) {
/* 1481 */         errorObj = new ErrorObject(
/* 1482 */           SubmissionErrorCodes.ERROR_DATAFIELD51_MERCHANT_DATA
/* 1483 */           .getErrorCode(), 
/* 1484 */           SubmissionErrorCodes.ERROR_DATAFIELD51_MERCHANT_DATA
/* 1485 */           .getErrorDescription() + 
/* 1486 */           "\n" + 
/* 1487 */           "RecordType:" + 
/* 1488 */           recordType + 
/* 1489 */           "|" + 
/* 1490 */           "RecordNumber:" + 
/* 1491 */           recordNumber + 
/* 1492 */           "|" + 
/* 1493 */           "MerchantId" + 
/* 1494 */           "|" + 
/* 1495 */           "This field length Cannot be greater than 15");
/* 1496 */         errorCodes.add(errorObj);
/*      */       }
/* 1498 */       else if (!CommonValidator.isValidModulusNineCheck(merchantID)) {
/* 1499 */         errorObj = new ErrorObject(
/* 1500 */           SubmissionErrorCodes.ERROR_DATAFIELD51_MERCHANT_DATA
/* 1501 */           .getErrorCode(), 
/* 1502 */           SubmissionErrorCodes.ERROR_DATAFIELD51_MERCHANT_DATA
/* 1503 */           .getErrorDescription() + 
/* 1504 */           "\n" + 
/* 1505 */           "RecordType:" + 
/* 1506 */           recordType + 
/* 1507 */           "|" + 
/* 1508 */           "RecordNumber:" + 
/* 1509 */           recordNumber + 
/* 1510 */           "|" + 
/* 1511 */           "MerchantId" + 
/* 1512 */           "|" + 
/* 1513 */           SubmissionErrorCodes.ERROR_DATAFIELD51_MERCHANT_DATA
/* 1514 */           .getErrorDescription());
/* 1515 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */     else {
/* 1519 */       errorObj = new ErrorObject(
/* 1520 */         SubmissionErrorCodes.ERROR_DATAFIELD51_MERCHANT_DATA
/* 1521 */         .getErrorCode(), 
/* 1522 */         SubmissionErrorCodes.ERROR_DATAFIELD51_MERCHANT_DATA
/* 1523 */         .getErrorDescription() + 
/* 1524 */         "\n" + 
/* 1525 */         "RecordType:" + 
/* 1526 */         recordType + 
/* 1527 */         "|" + 
/* 1528 */         "RecordNumber:" + 
/* 1529 */         recordNumber + 
/* 1530 */         "|" + 
/* 1531 */         "MerchantId" + 
/* 1532 */         "|" + 
/* 1533 */         "This field can only be AlphaNumeric");
/* 1534 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateMerchantLocationId(String merchantLocationID, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1551 */     ErrorObject errorObj = null;
/*      */     
/* 1553 */     if (!CommonValidator.isNullOrEmpty(merchantLocationID))
/*      */     {
/*      */ 
/*      */ 
/* 1557 */       String reqLength = 
/* 1558 */         CommonValidator.validateLength(merchantLocationID, 15, 15);
/* 1559 */       if (reqLength.equals("greaterThanMax")) {
/* 1560 */         errorObj = new ErrorObject(
/* 1561 */           SubmissionErrorCodes.ERROR_DATAFIELD596_MERCHANT_LOCATIONID
/* 1562 */           .getErrorCode(), 
/* 1563 */           SubmissionErrorCodes.ERROR_DATAFIELD596_MERCHANT_LOCATIONID
/* 1564 */           .getErrorDescription() + 
/* 1565 */           "\n" + 
/* 1566 */           "RecordType:" + 
/* 1567 */           recordType + 
/* 1568 */           "|" + 
/* 1569 */           "RecordNumber:" + 
/* 1570 */           recordNumber + 
/* 1571 */           "|" + 
/* 1572 */           "MerchantLocationId" + 
/* 1573 */           "|" + 
/* 1574 */           "This field length Cannot be greater than 15");
/* 1575 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateMerchantContactInfo(String merchantContactInfo, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1611 */     ErrorObject errorObj = null;
/* 1612 */     if (!CommonValidator.isNullOrEmpty(merchantContactInfo))
/*      */     {
/*      */ 
/*      */ 
/* 1616 */       String reqLength = 
/* 1617 */         CommonValidator.validateLength(merchantContactInfo, 40, 40);
/*      */       
/* 1619 */       if (reqLength.equals("greaterThanMax")) {
/* 1620 */         errorObj = new ErrorObject(
/* 1621 */           SubmissionErrorCodes.ERROR_DATAFIELD596_MERCHANT_CONTACTINFO
/* 1622 */           .getErrorCode(), 
/* 1623 */           SubmissionErrorCodes.ERROR_DATAFIELD596_MERCHANT_CONTACTINFO
/* 1624 */           .getErrorDescription() + 
/* 1625 */           "\n" + 
/* 1626 */           "RecordType:" + 
/* 1627 */           recordType + 
/* 1628 */           "|" + 
/* 1629 */           "RecordNumber:" + 
/* 1630 */           recordNumber + 
/* 1631 */           "|" + 
/* 1632 */           "MerchantContactInfo" + 
/* 1633 */           "|" + 
/* 1634 */           "This field length Cannot be greater than 40");
/* 1635 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTerminalId(String terminalID, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1671 */     ErrorObject errorObj = null;
/*      */     
/* 1673 */     if (!CommonValidator.isNullOrEmpty(terminalID))
/*      */     {
/*      */ 
/*      */ 
/* 1677 */       String reqLength = CommonValidator.validateLength(terminalID, 8, 8);
/* 1678 */       if (reqLength.equals("greaterThanMax")) {
/* 1679 */         errorObj = new ErrorObject(
/* 1680 */           SubmissionErrorCodes.ERROR_DATAFIELD596_TERMINAL_ID
/* 1681 */           .getErrorCode(), 
/* 1682 */           SubmissionErrorCodes.ERROR_DATAFIELD596_TERMINAL_ID
/* 1683 */           .getErrorDescription() + 
/* 1684 */           "\n" + 
/* 1685 */           "RecordType:" + 
/* 1686 */           recordType + 
/* 1687 */           "|" + 
/* 1688 */           "RecordNumber:" + 
/* 1689 */           recordNumber + 
/* 1690 */           "|" + 
/* 1691 */           "Terminal Id" + 
/* 1692 */           "|" + 
/* 1693 */           "This field length Cannot be greater than 8");
/* 1694 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validatePosDataCode(String posDataCode, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1730 */     ErrorObject errorObj = null;
/*      */     
/* 1732 */     if (CommonValidator.isNullOrEmpty(posDataCode)) {
/* 1733 */       if (!isPaperSubmitter) {
/* 1734 */         errorObj = new ErrorObject(
/* 1735 */           SubmissionErrorCodes.ERROR_DATAFIELD57_POSDATA_CODE
/* 1736 */           .getErrorCode(), 
/* 1737 */           SubmissionErrorCodes.ERROR_DATAFIELD57_POSDATA_CODE
/* 1738 */           .getErrorDescription() + 
/* 1739 */           "\n" + 
/* 1740 */           "RecordType:" + 
/* 1741 */           recordType + 
/* 1742 */           "|" + 
/* 1743 */           "RecordNumber:" + 
/* 1744 */           recordNumber + 
/* 1745 */           "|" + 
/* 1746 */           "PosDataCode" + 
/* 1747 */           "|" + 
/* 1748 */           "This field is mandatory and cannot be empty");
/* 1749 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/* 1752 */     else if (CommonValidator.validateData(posDataCode, 
/* 1753 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[42])) {
/* 1754 */       String reqLength = 
/* 1755 */         CommonValidator.validateLength(posDataCode, 12, 12);
/* 1756 */       if (reqLength.equals("greaterThanMax")) {
/* 1757 */         errorObj = new ErrorObject(
/* 1758 */           SubmissionErrorCodes.ERROR_DATAFIELD57_POSDATA_CODE
/* 1759 */           .getErrorCode(), 
/* 1760 */           SubmissionErrorCodes.ERROR_DATAFIELD57_POSDATA_CODE
/* 1761 */           .getErrorDescription() + 
/* 1762 */           "\n" + 
/* 1763 */           "RecordType:" + 
/* 1764 */           recordType + 
/* 1765 */           "|" + 
/* 1766 */           "RecordNumber:" + 
/* 1767 */           recordNumber + 
/* 1768 */           "|" + 
/* 1769 */           "PosDataCode" + 
/* 1770 */           "|" + 
/* 1771 */           "This field length Cannot be greater than 12");
/* 1772 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/* 1775 */       errorObj = new ErrorObject(
/* 1776 */         SubmissionErrorCodes.ERROR_DATAFIELD57_POSDATA_CODE
/* 1777 */         .getErrorCode(), 
/* 1778 */         SubmissionErrorCodes.ERROR_DATAFIELD57_POSDATA_CODE
/* 1779 */         .getErrorDescription() + 
/* 1780 */         "\n" + 
/* 1781 */         "RecordType:" + 
/* 1782 */         recordType + 
/* 1783 */         "|" + 
/* 1784 */         "RecordNumber:" + 
/* 1785 */         recordNumber + 
/* 1786 */         "|" + 
/* 1787 */         "PosDataCode" + 
/* 1788 */         "|" + 
/* 1789 */         "This field can only be AlphaNumeric");
/* 1790 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateInvoiceReferenceNumber(String invoiceReferenceNumber, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1807 */     ErrorObject errorObj = null;
/*      */     
/* 1809 */     if (!CommonValidator.isNullOrEmpty(invoiceReferenceNumber))
/*      */     {
/*      */ 
/*      */ 
/* 1813 */       String reqLength = 
/* 1814 */         CommonValidator.validateLength(invoiceReferenceNumber, 30, 30);
/* 1815 */       if (reqLength.equals("greaterThanMax")) {
/* 1816 */         errorObj = new ErrorObject(
/* 1817 */           SubmissionErrorCodes.ERROR_DATAFIELD596_INVSSRVSNO
/* 1818 */           .getErrorCode(), 
/* 1819 */           SubmissionErrorCodes.ERROR_DATAFIELD596_INVSSRVSNO
/* 1820 */           .getErrorDescription() + 
/* 1821 */           "\n" + 
/* 1822 */           "RecordType:" + 
/* 1823 */           recordType + 
/* 1824 */           "|" + 
/* 1825 */           "RecordNumber:" + 
/* 1826 */           recordNumber + 
/* 1827 */           "|" + 
/* 1828 */           "Invoice Service Number" + 
/* 1829 */           "|" + 
/* 1830 */           "This field length Cannot be greater than 30");
/* 1831 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateTabImageSequenceNumber(String imageSequenceNumber, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1867 */     ErrorObject errorObj = null;
/*      */     
/* 1869 */     if (CommonValidator.isNullOrEmpty(imageSequenceNumber)) {
/* 1870 */       if (isPaperSubmitter) {
/* 1871 */         errorObj = new ErrorObject(
/* 1872 */           SubmissionErrorCodes.ERROR_DATAFIELD58_TABIMGSQNC_NUMBER
/* 1873 */           .getErrorCode(), 
/* 1874 */           SubmissionErrorCodes.ERROR_DATAFIELD58_TABIMGSQNC_NUMBER
/* 1875 */           .getErrorDescription() + 
/* 1876 */           "\n" + 
/* 1877 */           "RecordType:" + 
/* 1878 */           recordType + 
/* 1879 */           "|" + 
/* 1880 */           "RecordNumber:" + 
/* 1881 */           recordNumber + 
/* 1882 */           "|" + 
/* 1883 */           "TabImageSequenceNumber" + 
/* 1884 */           "|" + 
/* 1885 */           "This field is mandatory and cannot be empty");
/* 1886 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/* 1889 */     else if (CommonValidator.validateData(imageSequenceNumber, 
/* 1890 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[48])) {
/* 1891 */       String reqLength = CommonValidator.validateLength(imageSequenceNumber, 8, 8);
/* 1892 */       if (reqLength.equals("greaterThanMax")) {
/* 1893 */         errorObj = new ErrorObject(
/* 1894 */           SubmissionErrorCodes.ERROR_DATAFIELD58_TABIMGSQNC_NUMBER
/* 1895 */           .getErrorCode(), 
/* 1896 */           SubmissionErrorCodes.ERROR_DATAFIELD58_TABIMGSQNC_NUMBER
/* 1897 */           .getErrorDescription() + 
/* 1898 */           "\n" + 
/* 1899 */           "RecordType:" + 
/* 1900 */           recordType + 
/* 1901 */           "|" + 
/* 1902 */           "RecordNumber:" + 
/* 1903 */           recordNumber + 
/* 1904 */           "|" + 
/* 1905 */           "TabImageSequenceNumber" + 
/* 1906 */           "|" + 
/* 1907 */           "This field length Cannot be greater than 8");
/* 1908 */         errorCodes.add(errorObj);
/*      */       }
/*      */     } else {
/* 1911 */       errorObj = new ErrorObject(
/* 1912 */         SubmissionErrorCodes.ERROR_DATAFIELD58_TABIMGSQNC_NUMBER
/* 1913 */         .getErrorCode(), 
/* 1914 */         SubmissionErrorCodes.ERROR_DATAFIELD58_TABIMGSQNC_NUMBER
/* 1915 */         .getErrorDescription() + 
/* 1916 */         "\n" + 
/* 1917 */         "RecordType:" + 
/* 1918 */         recordType + 
/* 1919 */         "|" + 
/* 1920 */         "RecordNumber:" + 
/* 1921 */         recordNumber + 
/* 1922 */         "|" + 
/* 1923 */         "TabImageSequenceNumber" + 
/* 1924 */         "|" + 
/* 1925 */         "This field can only be AlphaNumeric");
/* 1926 */       errorCodes.add(errorObj);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateMatchingKeyType(String matchingKeyType, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 1943 */     ErrorObject errorObj = null;
/*      */     
/* 1945 */     if (!CommonValidator.isNullOrEmpty(matchingKeyType))
/*      */     {
/* 1947 */       if (CommonValidator.validateData(matchingKeyType, 
/* 1948 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[49])) {
/* 1949 */         String reqLength = CommonValidator.validateLength(matchingKeyType, 2, 2);
/* 1950 */         if (reqLength.equals("greaterThanMax")) {
/* 1951 */           errorObj = new ErrorObject(
/* 1952 */             SubmissionErrorCodes.ERROR_DATAFIELD597_MATCHKEY_TYPE
/* 1953 */             .getErrorCode(), 
/* 1954 */             SubmissionErrorCodes.ERROR_DATAFIELD597_MATCHKEY_TYPE
/* 1955 */             .getErrorDescription() + 
/* 1956 */             "\n" + 
/* 1957 */             "RecordType:" + 
/* 1958 */             recordType + 
/* 1959 */             "|" + 
/* 1960 */             "RecordNumber:" + 
/* 1961 */             recordNumber + 
/* 1962 */             "|" + 
/* 1963 */             "MatchingKeyType" + 
/* 1964 */             "|" + 
/* 1965 */             "This field length Cannot be greater than 2");
/* 1966 */           errorCodes.add(errorObj);
/*      */ 
/*      */ 
/*      */         }
/* 1970 */         else if (!"01".equalsIgnoreCase(matchingKeyType))
/*      */         {
/* 1972 */           if (!"02".equalsIgnoreCase(matchingKeyType))
/*      */           {
/* 1974 */             if (!"99".equalsIgnoreCase(matchingKeyType))
/*      */             {
/* 1976 */               if (!"03".equalsIgnoreCase(matchingKeyType))
/*      */               {
/* 1978 */                 if (!"04".equalsIgnoreCase(matchingKeyType)) {
/* 1979 */                   errorObj = new ErrorObject(
/* 1980 */                     SubmissionErrorCodes.ERROR_DATAFIELD597_MATCHKEY_TYPE
/* 1981 */                     .getErrorCode(), 
/* 1982 */                     SubmissionErrorCodes.ERROR_DATAFIELD597_MATCHKEY_TYPE
/* 1983 */                     .getErrorDescription() + 
/* 1984 */                     "\n" + 
/* 1985 */                     "RecordType:" + 
/* 1986 */                     recordType + 
/* 1987 */                     "|" + 
/* 1988 */                     "RecordNumber:" + 
/* 1989 */                     recordNumber + 
/* 1990 */                     "|" + 
/* 1991 */                     "MatchingKeyType" + 
/* 1992 */                     "|" + 
/* 1993 */                     SubmissionErrorCodes.ERROR_DATAFIELD597_MATCHKEY_TYPE
/* 1994 */                     .getErrorDescription());
/* 1995 */                   errorCodes.add(errorObj);
/*      */                 } } } }
/*      */         }
/*      */       } else {
/* 1999 */         errorObj = new ErrorObject(
/* 2000 */           SubmissionErrorCodes.ERROR_DATAFIELD597_MATCHKEY_TYPE
/* 2001 */           .getErrorCode(), 
/* 2002 */           SubmissionErrorCodes.ERROR_DATAFIELD597_MATCHKEY_TYPE
/* 2003 */           .getErrorDescription() + 
/* 2004 */           "\n" + 
/* 2005 */           "RecordType:" + 
/* 2006 */           recordType + 
/* 2007 */           "|" + 
/* 2008 */           "RecordNumber:" + 
/* 2009 */           recordNumber + 
/* 2010 */           "|" + 
/* 2011 */           "MatchingKeyType" + 
/* 2012 */           "|" + 
/* 2013 */           "This field can only be AlphaNumeric");
/* 2014 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateMatchingKey(String matchingKey, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 2031 */     ErrorObject errorObj = null;
/* 2032 */     if (!CommonValidator.isNullOrEmpty(matchingKey)) {
/* 2033 */       if (CommonValidator.validateData(matchingKey, 
/* 2034 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[50])) {
/* 2035 */         String reqLength = 
/* 2036 */           CommonValidator.validateLength(matchingKey, 21, 21);
/* 2037 */         if (reqLength.equals("greaterThanMax")) {
/* 2038 */           errorObj = new ErrorObject(
/* 2039 */             SubmissionErrorCodes.ERROR_DATAFIELD597_MATCHKEY
/* 2040 */             .getErrorCode(), 
/* 2041 */             SubmissionErrorCodes.ERROR_DATAFIELD597_MATCHKEY
/* 2042 */             .getErrorDescription() + 
/* 2043 */             "\n" + 
/* 2044 */             "RecordType:" + 
/* 2045 */             recordType + 
/* 2046 */             "|" + 
/* 2047 */             "RecordNumber:" + 
/* 2048 */             recordNumber + 
/* 2049 */             "|" + 
/* 2050 */             "MatchingKey" + 
/* 2051 */             "|" + 
/* 2052 */             "This field length Cannot be greater than 21");
/* 2053 */           errorCodes.add(errorObj);
/*      */         }
/*      */       } else {
/* 2056 */         errorObj = new ErrorObject(
/* 2057 */           SubmissionErrorCodes.ERROR_DATAFIELD597_MATCHKEY
/* 2058 */           .getErrorCode(), 
/* 2059 */           SubmissionErrorCodes.ERROR_DATAFIELD597_MATCHKEY
/* 2060 */           .getErrorDescription() + 
/* 2061 */           "\n" + 
/* 2062 */           "RecordType:" + 
/* 2063 */           recordType + 
/* 2064 */           "|" + 
/* 2065 */           "RecordNumber:" + 
/* 2066 */           recordNumber + 
/* 2067 */           "|" + 
/* 2068 */           "MatchingKey" + 
/* 2069 */           "|" + 
/* 2070 */           "This field can only be AlphaNumeric");
/* 2071 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static void validateElectronicCommerceIndicator(String electronicCommerceIndicator, List<ErrorObject> errorCodes)
/*      */     throws SettlementException
/*      */   {
/* 2088 */     ErrorObject errorObj = null;
/*      */     
/* 2090 */     if (!CommonValidator.isNullOrEmpty(electronicCommerceIndicator)) {
/* 2091 */       if (CommonValidator.validateData(electronicCommerceIndicator, 
/* 2092 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[51])) {
/* 2093 */         String reqLength = CommonValidator.validateLength(electronicCommerceIndicator, 2, 2);
/* 2094 */         if (reqLength.equals("greaterThanMax")) {
/* 2095 */           errorObj = new ErrorObject(
/* 2096 */             SubmissionErrorCodes.ERROR_DATAFIELD598_ELECOMRCIND
/* 2097 */             .getErrorCode(), 
/* 2098 */             SubmissionErrorCodes.ERROR_DATAFIELD598_ELECOMRCIND
/* 2099 */             .getErrorDescription() + 
/* 2100 */             "\n" + 
/* 2101 */             "RecordType:" + 
/* 2102 */             recordType + 
/* 2103 */             "|" + 
/* 2104 */             "RecordNumber:" + 
/* 2105 */             recordNumber + 
/* 2106 */             "|" + 
/* 2107 */             "ElectronicCommerceIndicator" + 
/* 2108 */             "|" + 
/* 2109 */             "This field length Cannot be greater than 2");
/* 2110 */           errorCodes.add(errorObj);
/*      */ 
/*      */         }
/* 2113 */         else if (!ElectronicCommerceIndicator.Authenticated.getElectronicCommerceIndicator().equalsIgnoreCase(
/* 2114 */           electronicCommerceIndicator))
/*      */         {
/*      */ 
/* 2117 */           if (!ElectronicCommerceIndicator.Attempted.getElectronicCommerceIndicator().equalsIgnoreCase(electronicCommerceIndicator))
/*      */           {
/*      */ 
/* 2120 */             if (!ElectronicCommerceIndicator.Not_authenticated.getElectronicCommerceIndicator().equalsIgnoreCase(electronicCommerceIndicator)) {
/* 2121 */               errorObj = new ErrorObject(
/* 2122 */                 SubmissionErrorCodes.ERROR_DATAFIELD598_ELECOMRCIND
/* 2123 */                 .getErrorCode(), 
/* 2124 */                 SubmissionErrorCodes.ERROR_DATAFIELD598_ELECOMRCIND
/* 2125 */                 .getErrorDescription() + 
/* 2126 */                 "\n" + 
/* 2127 */                 "RecordType:" + 
/* 2128 */                 recordType + 
/* 2129 */                 "|" + 
/* 2130 */                 "RecordNumber:" + 
/* 2131 */                 recordNumber + 
/* 2132 */                 "|" + 
/* 2133 */                 "ElectronicCommerceIndicator" + 
/* 2134 */                 "|" + 
/* 2135 */                 SubmissionErrorCodes.ERROR_DATAFIELD598_ELECOMRCIND
/* 2136 */                 .getErrorDescription());
/* 2137 */               errorCodes.add(errorObj);
/*      */             } }
/*      */         }
/*      */       } else {
/* 2141 */         errorObj = new ErrorObject(
/* 2142 */           SubmissionErrorCodes.ERROR_DATAFIELD598_ELECOMRCIND
/* 2143 */           .getErrorCode(), 
/* 2144 */           SubmissionErrorCodes.ERROR_DATAFIELD598_ELECOMRCIND
/* 2145 */           .getErrorDescription() + 
/* 2146 */           "\n" + 
/* 2147 */           "RecordType:" + 
/* 2148 */           recordType + 
/* 2149 */           "|" + 
/* 2150 */           "RecordNumber:" + 
/* 2151 */           recordNumber + 
/* 2152 */           "|" + 
/* 2153 */           "ElectronicCommerceIndicator" + 
/* 2154 */           "|" + 
/* 2155 */           "This field can only be AlphaNumeric");
/* 2156 */         errorCodes.add(errorObj);
/*      */       }
/*      */     }
/*      */   }
/*      */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\validator\TABRecValidator.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */