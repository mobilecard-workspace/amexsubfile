/*     */ package com.americanexpress.ips.gfsg.validator;
/*     */ 
/*     */ import com.americanexpress.ips.gfsg.beans.ErrorObject;
/*     */ import com.americanexpress.ips.gfsg.beans.common.TransactionAdviceBasicBean;
/*     */ import com.americanexpress.ips.gfsg.beans.common.taa.LocationDetailTAABean;
/*     */ import com.americanexpress.ips.gfsg.enumerations.RecordType;
/*     */ import com.americanexpress.ips.gfsg.enumerations.SubmissionErrorCodes;
/*     */ import com.americanexpress.ips.gfsg.exceptions.SettlementException;
/*     */ import java.util.List;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class LocationTAARecValidator
/*     */ {
/*     */   private static String recordNumber;
/*     */   private static String recordType;
/*     */   
/*     */   public static void validtateLocationRecordType(TransactionAdviceBasicBean transactionAdviceBasicBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/*  40 */     recordNumber = 
/*  41 */       transactionAdviceBasicBean.getLocationDetailTAABean().getRecordNumber();
/*  42 */     recordType = transactionAdviceBasicBean.getLocationDetailTAABean()
/*  43 */       .getRecordType();
/*  44 */     validateRecordType(transactionAdviceBasicBean
/*  45 */       .getLocationDetailTAABean().getRecordType(), errorCodes);
/*  46 */     validateRecordNumber(transactionAdviceBasicBean
/*  47 */       .getLocationDetailTAABean().getRecordNumber(), errorCodes);
/*  48 */     validateTransactionIdentifier(transactionAdviceBasicBean, errorCodes);
/*  49 */     validateAddendaTypeCode(transactionAdviceBasicBean
/*  50 */       .getLocationDetailTAABean().getAddendaTypeCode(), errorCodes);
/*  51 */     validateLocationName(transactionAdviceBasicBean
/*  52 */       .getLocationDetailTAABean().getLocationName(), errorCodes);
/*  53 */     validatelocationAddress(transactionAdviceBasicBean
/*  54 */       .getLocationDetailTAABean().getLocationAddress(), errorCodes);
/*  55 */     validateLocationCity(transactionAdviceBasicBean
/*  56 */       .getLocationDetailTAABean().getLocationCity(), errorCodes);
/*  57 */     validateLocationRegion(transactionAdviceBasicBean
/*  58 */       .getLocationDetailTAABean().getLocationRegion(), errorCodes);
/*  59 */     validateLocationCountryCode(transactionAdviceBasicBean
/*  60 */       .getLocationDetailTAABean().getLocationCountryCode(), 
/*  61 */       errorCodes);
/*  62 */     validateLocationPostalCode(transactionAdviceBasicBean
/*  63 */       .getLocationDetailTAABean().getLocationPostalCode(), errorCodes);
/*  64 */     validateMerchantCategoryCode(transactionAdviceBasicBean
/*  65 */       .getLocationDetailTAABean().getMerchantCategoryCode(), 
/*  66 */       errorCodes);
/*  67 */     validateSellerId(transactionAdviceBasicBean.getLocationDetailTAABean()
/*  68 */       .getSellerId(), errorCodes);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateRecordType(String recType, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/*  86 */     ErrorObject errorObj = null;
/*     */     
/*  88 */     if (CommonValidator.isNullOrEmpty(recType))
/*     */     {
/*  90 */       errorObj = new ErrorObject(
/*  91 */         SubmissionErrorCodes.ERROR_DATAFIELD1_RECORDTYPE
/*  92 */         .getErrorCode(), 
/*  93 */         SubmissionErrorCodes.ERROR_DATAFIELD1_RECORDTYPE
/*  94 */         .getErrorDescription() + 
/*  95 */         "\n" + 
/*  96 */         "TFH" + 
/*  97 */         "|" + 
/*  98 */         "RecordNumber:" + 
/*  99 */         recordNumber + 
/* 100 */         "|" + 
/* 101 */         "RecordType:" + 
/* 102 */         "|" + 
/* 103 */         "This field is mandatory and cannot be empty");
/* 104 */       errorCodes.add(errorObj);
/*     */     }
/* 106 */     else if (!RecordType.Transaction_Advice_Addendum.getRecordType().equalsIgnoreCase(recType))
/*     */     {
/*     */ 
/* 109 */       errorObj = new ErrorObject(
/* 110 */         SubmissionErrorCodes.ERROR_DATAFIELD1_RECORDTYPE);
/* 111 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateRecordNumber(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 128 */     ErrorObject errorObj = null;
/*     */     
/* 130 */     if (CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 132 */       errorObj = new ErrorObject(
/* 133 */         SubmissionErrorCodes.ERROR_DATAFIELD585_RECORDNUMBER
/* 134 */         .getErrorCode(), 
/* 135 */         SubmissionErrorCodes.ERROR_DATAFIELD585_RECORDNUMBER
/* 136 */         .getErrorDescription() + 
/* 137 */         "\n" + 
/* 138 */         "RecordType:" + 
/* 139 */         recordType + 
/* 140 */         "|" + 
/* 141 */         "RecordNumber:" + 
/* 142 */         recordNumber + 
/* 143 */         "|" + 
/* 144 */         "RecordNumber:" + 
/* 145 */         "|" + 
/* 146 */         "This field is mandatory and cannot be empty");
/* 147 */       errorCodes.add(errorObj);
/*     */     }
/* 149 */     else if (CommonValidator.validateData(value, 
/* 150 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[54]))
/*     */     {
/* 152 */       String reqLength = CommonValidator.validateLength(value, 8, 8);
/*     */       
/* 154 */       if (reqLength.equals("greaterThanMax")) {
/* 155 */         errorObj = new ErrorObject(
/* 156 */           SubmissionErrorCodes.ERROR_DATAFIELD3_RECORDNUMBER
/* 157 */           .getErrorCode(), 
/* 158 */           SubmissionErrorCodes.ERROR_DATAFIELD3_RECORDNUMBER
/* 159 */           .getErrorDescription() + 
/* 160 */           "\n" + 
/* 161 */           "RecordType:" + 
/* 162 */           recordType + 
/* 163 */           "|" + 
/* 164 */           "RecordNumber:" + 
/* 165 */           recordNumber + 
/* 166 */           "|" + 
/* 167 */           "RecordNumber:" + 
/* 168 */           "|" + 
/* 169 */           "This field length Cannot be greater than 8");
/* 170 */         errorCodes.add(errorObj);
/*     */       }
/*     */     } else {
/* 173 */       errorObj = new ErrorObject(
/* 174 */         SubmissionErrorCodes.ERROR_DATAFIELD2_RECORDNUMBER
/* 175 */         .getErrorCode(), 
/* 176 */         SubmissionErrorCodes.ERROR_DATAFIELD2_RECORDNUMBER
/* 177 */         .getErrorDescription() + 
/* 178 */         "\n" + 
/* 179 */         "RecordType:" + 
/* 180 */         recordType + 
/* 181 */         "|" + 
/* 182 */         "RecordNumber:" + 
/* 183 */         recordNumber + 
/* 184 */         "|" + 
/* 185 */         "RecordNumber:" + 
/* 186 */         "|" + 
/* 187 */         "This field can only be Numeric");
/* 188 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateTransactionIdentifier(TransactionAdviceBasicBean transactionAdviceBasicBean, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 206 */     ErrorObject errorObj = null;
/* 207 */     String transIdLOC = transactionAdviceBasicBean
/* 208 */       .getLocationDetailTAABean().getTransactionIdentifier();
/* 209 */     if (CommonValidator.isNullOrEmpty(transIdLOC))
/*     */     {
/* 211 */       errorObj = new ErrorObject(
/* 212 */         SubmissionErrorCodes.ERROR_DATAFIELD25_TRANSACTION_IDENTIFIER
/* 213 */         .getErrorCode(), 
/* 214 */         SubmissionErrorCodes.ERROR_DATAFIELD25_TRANSACTION_IDENTIFIER
/* 215 */         .getErrorDescription() + 
/* 216 */         "\n" + 
/* 217 */         "RecordType:" + 
/* 218 */         recordType + 
/* 219 */         "|" + 
/* 220 */         "RecordNumber:" + 
/* 221 */         recordNumber + 
/* 222 */         "|" + 
/* 223 */         "TransactionIdentifier" + 
/* 224 */         "|" + 
/* 225 */         "This field is mandatory and cannot be empty");
/* 226 */       errorCodes.add(errorObj);
/*     */     }
/* 228 */     else if (CommonValidator.validateData(transIdLOC, 
/* 229 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[55]))
/*     */     {
/* 231 */       String reqLength = CommonValidator.validateLength(transIdLOC, 
/* 232 */         15, 15);
/*     */       
/* 234 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/* 236 */         errorObj = new ErrorObject(
/* 237 */           SubmissionErrorCodes.ERROR_DATAFIELD25_TRANSACTION_IDENTIFIER
/* 238 */           .getErrorCode(), 
/* 239 */           SubmissionErrorCodes.ERROR_DATAFIELD25_TRANSACTION_IDENTIFIER
/* 240 */           .getErrorDescription() + 
/* 241 */           "\n" + 
/* 242 */           "RecordType:" + 
/* 243 */           recordType + 
/* 244 */           "|" + 
/* 245 */           "RecordNumber:" + 
/* 246 */           recordNumber + 
/* 247 */           "|" + 
/* 248 */           "TransactionIdentifier" + 
/* 249 */           "|" + 
/* 250 */           "This field length Cannot be greater than 15");
/* 251 */         errorCodes.add(errorObj);
/*     */ 
/*     */       }
/* 254 */       else if (!transIdLOC.equalsIgnoreCase(
/* 255 */         transactionAdviceBasicBean.getTransactionIdentifier()))
/*     */       {
/* 257 */         errorObj = new ErrorObject(
/* 258 */           SubmissionErrorCodes.ERROR_DATAFIELD67_TRANSACTION_IDENTIFIER
/* 259 */           .getErrorCode(), 
/* 260 */           SubmissionErrorCodes.ERROR_DATAFIELD67_TRANSACTION_IDENTIFIER
/* 261 */           .getErrorDescription() + 
/* 262 */           "\n" + 
/* 263 */           "RecordType:" + 
/* 264 */           recordType + 
/* 265 */           "|" + 
/* 266 */           "RecordNumber:" + 
/* 267 */           recordNumber + 
/* 268 */           "|" + 
/* 269 */           "TransactionIdentifier" + 
/* 270 */           "|" + 
/* 271 */           SubmissionErrorCodes.ERROR_DATAFIELD67_TRANSACTION_IDENTIFIER
/* 272 */           .getErrorDescription());
/* 273 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */     else
/*     */     {
/* 278 */       errorObj = new ErrorObject(
/* 279 */         SubmissionErrorCodes.ERROR_DATAFIELD25_TRANSACTION_IDENTIFIER
/* 280 */         .getErrorCode(), 
/* 281 */         SubmissionErrorCodes.ERROR_DATAFIELD25_TRANSACTION_IDENTIFIER
/* 282 */         .getErrorDescription() + 
/* 283 */         "\n" + 
/* 284 */         "RecordType:" + 
/* 285 */         recordType + 
/* 286 */         "|" + 
/* 287 */         "RecordNumber:" + 
/* 288 */         recordNumber + 
/* 289 */         "|" + 
/* 290 */         "TransactionIdentifier" + 
/* 291 */         "|" + 
/* 292 */         "This field can only be Numeric");
/* 293 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateAddendaTypeCode(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 310 */     ErrorObject errorObj = null;
/*     */     
/* 312 */     if (CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 314 */       errorObj = new ErrorObject(
/* 315 */         SubmissionErrorCodes.ERROR_DATAFIELD68_ADDENDA_TYPECODE
/* 316 */         .getErrorCode(), 
/* 317 */         SubmissionErrorCodes.ERROR_DATAFIELD68_ADDENDA_TYPECODE
/* 318 */         .getErrorDescription() + 
/* 319 */         "\n" + 
/* 320 */         "RecordType:" + 
/* 321 */         recordType + 
/* 322 */         "|" + 
/* 323 */         "RecordNumber:" + 
/* 324 */         recordNumber + 
/* 325 */         "|" + 
/* 326 */         "AddendaTypeCode" + 
/* 327 */         "|" + 
/* 328 */         "This field is mandatory and cannot be empty");
/* 329 */       errorCodes.add(errorObj);
/*     */     }
/* 331 */     else if (CommonValidator.validateData(value, 
/* 332 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[57]))
/*     */     {
/* 334 */       String reqLength = CommonValidator.validateLength(value, 2, 2);
/*     */       
/* 336 */       if (reqLength.equals("greaterThanMax"))
/*     */       {
/* 338 */         errorObj = new ErrorObject(
/* 339 */           SubmissionErrorCodes.ERROR_DATAFIELD68_ADDENDA_TYPECODE
/* 340 */           .getErrorCode(), 
/* 341 */           SubmissionErrorCodes.ERROR_DATAFIELD68_ADDENDA_TYPECODE
/* 342 */           .getErrorDescription() + 
/* 343 */           "\n" + 
/* 344 */           "RecordType:" + 
/* 345 */           recordType + 
/* 346 */           "|" + 
/* 347 */           "RecordNumber:" + 
/* 348 */           recordNumber + 
/* 349 */           "|" + 
/* 350 */           "AddendaTypeCode" + 
/* 351 */           "|" + 
/* 352 */           "This field length Cannot be greater than 2");
/* 353 */         errorCodes.add(errorObj);
/*     */ 
/*     */ 
/*     */       }
/* 357 */       else if (!CommonValidator.isValidAddendaTypeCode(value))
/*     */       {
/* 359 */         errorObj = new ErrorObject(
/* 360 */           SubmissionErrorCodes.ERROR_DATAFIELD70_ADDENDA_TYPECODE
/* 361 */           .getErrorCode(), 
/* 362 */           SubmissionErrorCodes.ERROR_DATAFIELD70_ADDENDA_TYPECODE
/* 363 */           .getErrorDescription() + 
/* 364 */           "\n" + 
/* 365 */           "RecordType:" + 
/* 366 */           recordType + 
/* 367 */           "|" + 
/* 368 */           "RecordNumber:" + 
/* 369 */           recordNumber + 
/* 370 */           "|" + 
/* 371 */           "AddendaTypeCode" + 
/* 372 */           "|" + 
/* 373 */           SubmissionErrorCodes.ERROR_DATAFIELD70_ADDENDA_TYPECODE
/* 374 */           .getErrorDescription());
/* 375 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */     else {
/* 379 */       errorObj = new ErrorObject(
/* 380 */         SubmissionErrorCodes.ERROR_DATAFIELD68_ADDENDA_TYPECODE
/* 381 */         .getErrorCode(), 
/* 382 */         SubmissionErrorCodes.ERROR_DATAFIELD68_ADDENDA_TYPECODE
/* 383 */         .getErrorDescription() + 
/* 384 */         "\n" + 
/* 385 */         "RecordType:" + 
/* 386 */         recordType + 
/* 387 */         "|" + 
/* 388 */         "RecordNumber:" + 
/* 389 */         recordNumber + 
/* 390 */         "|" + 
/* 391 */         "AddendaTypeCode" + 
/* 392 */         "|" + 
/* 393 */         "This field can only be AlphaNumeric");
/* 394 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateLocationName(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 411 */     ErrorObject errorObj = null;
/*     */     
/* 413 */     if (CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 415 */       errorObj = new ErrorObject(
/* 416 */         SubmissionErrorCodes.ERROR_DATAFIELD61_LOCATION_NAME
/* 417 */         .getErrorCode(), 
/* 418 */         SubmissionErrorCodes.ERROR_DATAFIELD61_LOCATION_NAME
/* 419 */         .getErrorDescription() + 
/* 420 */         "\n" + 
/* 421 */         "RecordType:" + 
/* 422 */         recordType + 
/* 423 */         "|" + 
/* 424 */         "RecordNumber:" + 
/* 425 */         recordNumber + 
/* 426 */         "|" + 
/* 427 */         "LocationName" + 
/* 428 */         "|" + 
/* 429 */         "This field is mandatory and cannot be empty");
/* 430 */       errorCodes.add(errorObj);
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/* 436 */       String reqLength = 
/* 437 */         CommonValidator.validateLength(value, 38, 38);
/*     */       
/* 439 */       if (reqLength.equals("greaterThanMax")) {
/* 440 */         errorObj = new ErrorObject(
/* 441 */           SubmissionErrorCodes.ERROR_DATAFIELD61_LOCATION_NAME
/* 442 */           .getErrorCode(), 
/* 443 */           SubmissionErrorCodes.ERROR_DATAFIELD61_LOCATION_NAME
/* 444 */           .getErrorDescription() + 
/* 445 */           "\n" + 
/* 446 */           "RecordType:" + 
/* 447 */           recordType + 
/* 448 */           "|" + 
/* 449 */           "RecordNumber:" + 
/* 450 */           recordNumber + 
/* 451 */           "|" + 
/* 452 */           "LocationName" + 
/* 453 */           "|" + 
/* 454 */           "This field length Cannot be greater than 38");
/* 455 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validatelocationAddress(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 492 */     ErrorObject errorObj = null;
/*     */     
/* 494 */     if (CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 496 */       errorObj = new ErrorObject(
/* 497 */         SubmissionErrorCodes.ERROR_DATAFIELD62_LOCATION_ADDRESS
/* 498 */         .getErrorCode(), 
/* 499 */         SubmissionErrorCodes.ERROR_DATAFIELD62_LOCATION_ADDRESS
/* 500 */         .getErrorDescription() + 
/* 501 */         "\n" + 
/* 502 */         "RecordType:" + 
/* 503 */         recordType + 
/* 504 */         "|" + 
/* 505 */         "RecordNumber:" + 
/* 506 */         recordNumber + 
/* 507 */         "|" + 
/* 508 */         "LocationAddress" + 
/* 509 */         "|" + 
/* 510 */         "This field is mandatory and cannot be empty");
/* 511 */       errorCodes.add(errorObj);
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/* 517 */       String reqLength = 
/* 518 */         CommonValidator.validateLength(value, 38, 38);
/* 519 */       if (reqLength.equals("greaterThanMax")) {
/* 520 */         errorObj = new ErrorObject(
/* 521 */           SubmissionErrorCodes.ERROR_DATAFIELD62_LOCATION_ADDRESS
/* 522 */           .getErrorCode(), 
/* 523 */           SubmissionErrorCodes.ERROR_DATAFIELD62_LOCATION_ADDRESS
/* 524 */           .getErrorDescription() + 
/* 525 */           "\n" + 
/* 526 */           "RecordType:" + 
/* 527 */           recordType + 
/* 528 */           "|" + 
/* 529 */           "RecordNumber:" + 
/* 530 */           recordNumber + 
/* 531 */           "|" + 
/* 532 */           "LocationAddress" + 
/* 533 */           "|" + 
/* 534 */           "This field length Cannot be greater than 38");
/* 535 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateLocationCity(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 570 */     ErrorObject errorObj = null;
/*     */     
/* 572 */     if (CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 574 */       errorObj = new ErrorObject(
/* 575 */         SubmissionErrorCodes.ERROR_DATAFIELD63_LOCATION_CITY
/* 576 */         .getErrorCode(), 
/* 577 */         SubmissionErrorCodes.ERROR_DATAFIELD63_LOCATION_CITY
/* 578 */         .getErrorDescription() + 
/* 579 */         "\n" + 
/* 580 */         "RecordType:" + 
/* 581 */         recordType + 
/* 582 */         "|" + 
/* 583 */         "RecordNumber:" + 
/* 584 */         recordNumber + 
/* 585 */         "|" + 
/* 586 */         "LocationCity" + 
/* 587 */         "|" + 
/* 588 */         "This field is mandatory and cannot be empty");
/* 589 */       errorCodes.add(errorObj);
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/* 595 */       String reqLength = 
/* 596 */         CommonValidator.validateLength(value, 21, 21);
/*     */       
/* 598 */       if (reqLength.equals("greaterThanMax")) {
/* 599 */         errorObj = new ErrorObject(
/* 600 */           SubmissionErrorCodes.ERROR_DATAFIELD63_LOCATION_CITY
/* 601 */           .getErrorCode(), 
/* 602 */           SubmissionErrorCodes.ERROR_DATAFIELD63_LOCATION_CITY
/* 603 */           .getErrorDescription() + 
/* 604 */           "\n" + 
/* 605 */           "RecordType:" + 
/* 606 */           recordType + 
/* 607 */           "|" + 
/* 608 */           "RecordNumber:" + 
/* 609 */           recordNumber + 
/* 610 */           "|" + 
/* 611 */           "LocationCity" + 
/* 612 */           "|" + 
/* 613 */           "This field length Cannot be greater than 21");
/* 614 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateLocationRegion(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 649 */     ErrorObject errorObj = null;
/*     */     
/* 651 */     if (CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 653 */       errorObj = new ErrorObject(
/* 654 */         SubmissionErrorCodes.ERROR_DATAFIELD64_LOCATION_REGION
/* 655 */         .getErrorCode(), 
/* 656 */         SubmissionErrorCodes.ERROR_DATAFIELD64_LOCATION_REGION
/* 657 */         .getErrorDescription() + 
/* 658 */         "\n" + 
/* 659 */         "RecordType:" + 
/* 660 */         recordType + 
/* 661 */         "|" + 
/* 662 */         "RecordNumber:" + 
/* 663 */         recordNumber + 
/* 664 */         "|" + 
/* 665 */         "LocationRegion" + 
/* 666 */         "|" + 
/* 667 */         "This field is mandatory and cannot be empty");
/* 668 */       errorCodes.add(errorObj);
/*     */     }
/* 670 */     else if (CommonValidator.validateData(value, 
/* 671 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[61]))
/*     */     {
/* 673 */       String reqLength = CommonValidator.validateLength(value, 3, 3);
/*     */       
/* 675 */       if (reqLength.equals("greaterThanMax")) {
/* 676 */         errorObj = new ErrorObject(
/* 677 */           SubmissionErrorCodes.ERROR_DATAFIELD64_LOCATION_REGION
/* 678 */           .getErrorCode(), 
/* 679 */           SubmissionErrorCodes.ERROR_DATAFIELD64_LOCATION_REGION
/* 680 */           .getErrorDescription() + 
/* 681 */           "\n" + 
/* 682 */           "RecordType:" + 
/* 683 */           recordType + 
/* 684 */           "|" + 
/* 685 */           "RecordNumber:" + 
/* 686 */           recordNumber + 
/* 687 */           "|" + 
/* 688 */           "LocationRegion" + 
/* 689 */           "|" + 
/* 690 */           "This field length Cannot be greater than 3");
/* 691 */         errorCodes.add(errorObj);
/*     */       }
/*     */     } else {
/* 694 */       errorObj = new ErrorObject(
/* 695 */         SubmissionErrorCodes.ERROR_DATAFIELD64_LOCATION_REGION
/* 696 */         .getErrorCode(), 
/* 697 */         SubmissionErrorCodes.ERROR_DATAFIELD64_LOCATION_REGION
/* 698 */         .getErrorDescription() + 
/* 699 */         "\n" + 
/* 700 */         "RecordType:" + 
/* 701 */         recordType + 
/* 702 */         "|" + 
/* 703 */         "RecordNumber:" + 
/* 704 */         recordNumber + 
/* 705 */         "|" + 
/* 706 */         "LocationRegion" + 
/* 707 */         "|" + 
/* 708 */         "This field can only be AlphaNumeric");
/* 709 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateLocationCountryCode(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 726 */     ErrorObject errorObj = null;
/*     */     
/* 728 */     if (CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 730 */       errorObj = new ErrorObject(
/* 731 */         SubmissionErrorCodes.ERROR_DATAFIELD65_LOCATIONCNTRY_CODE
/* 732 */         .getErrorCode(), 
/* 733 */         SubmissionErrorCodes.ERROR_DATAFIELD65_LOCATIONCNTRY_CODE
/* 734 */         .getErrorDescription() + 
/* 735 */         "\n" + 
/* 736 */         "RecordType:" + 
/* 737 */         recordType + 
/* 738 */         "|" + 
/* 739 */         "RecordNumber:" + 
/* 740 */         recordNumber + 
/* 741 */         "|" + 
/* 742 */         "LocationCountryCode" + 
/* 743 */         "|" + 
/* 744 */         "This field is mandatory and cannot be empty");
/* 745 */       errorCodes.add(errorObj);
/*     */     }
/* 747 */     else if (CommonValidator.validateData(value, 
/* 748 */       com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[62]))
/*     */     {
/* 750 */       String reqLength = CommonValidator.validateLength(value, 3, 3);
/*     */       
/* 752 */       if (reqLength.equals("greaterThanMax")) {
/* 753 */         errorObj = new ErrorObject(
/* 754 */           SubmissionErrorCodes.ERROR_DATAFIELD65_LOCATIONCNTRY_CODE
/* 755 */           .getErrorCode(), 
/* 756 */           SubmissionErrorCodes.ERROR_DATAFIELD65_LOCATIONCNTRY_CODE
/* 757 */           .getErrorDescription() + 
/* 758 */           "\n" + 
/* 759 */           "RecordType:" + 
/* 760 */           recordType + 
/* 761 */           "|" + 
/* 762 */           "RecordNumber:" + 
/* 763 */           recordNumber + 
/* 764 */           "|" + 
/* 765 */           "LocationCountryCode" + 
/* 766 */           "|" + 
/* 767 */           "This field length Cannot be greater than 3");
/* 768 */         errorCodes.add(errorObj);
/*     */       }
/*     */     } else {
/* 771 */       errorObj = new ErrorObject(
/* 772 */         SubmissionErrorCodes.ERROR_DATAFIELD65_LOCATIONCNTRY_CODE
/* 773 */         .getErrorCode(), 
/* 774 */         SubmissionErrorCodes.ERROR_DATAFIELD65_LOCATIONCNTRY_CODE
/* 775 */         .getErrorDescription() + 
/* 776 */         "\n" + 
/* 777 */         "RecordType:" + 
/* 778 */         recordType + 
/* 779 */         "|" + 
/* 780 */         "RecordNumber:" + 
/* 781 */         recordNumber + 
/* 782 */         "|" + 
/* 783 */         "LocationCountryCode" + 
/* 784 */         "|" + 
/* 785 */         "This field can only be AlphaNumeric");
/* 786 */       errorCodes.add(errorObj);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateLocationPostalCode(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 803 */     ErrorObject errorObj = null;
/*     */     
/* 805 */     if (CommonValidator.isNullOrEmpty(value))
/*     */     {
/* 807 */       errorObj = new ErrorObject(
/* 808 */         SubmissionErrorCodes.ERROR_DATAFIELD66_LOCATIONPOSTAL_CODE
/* 809 */         .getErrorCode(), 
/* 810 */         SubmissionErrorCodes.ERROR_DATAFIELD66_LOCATIONPOSTAL_CODE
/* 811 */         .getErrorDescription() + 
/* 812 */         "\n" + 
/* 813 */         "RecordType:" + 
/* 814 */         recordType + 
/* 815 */         "|" + 
/* 816 */         "RecordNumber:" + 
/* 817 */         recordNumber + 
/* 818 */         "|" + 
/* 819 */         "LocationPostalCode" + 
/* 820 */         "|" + 
/* 821 */         "This field is mandatory and cannot be empty");
/* 822 */       errorCodes.add(errorObj);
/*     */ 
/*     */     }
/*     */     else
/*     */     {
/*     */ 
/* 828 */       String reqLength = 
/* 829 */         CommonValidator.validateLength(value, 15, 15);
/*     */       
/* 831 */       if (reqLength.equals("greaterThanMax")) {
/* 832 */         errorObj = new ErrorObject(
/* 833 */           SubmissionErrorCodes.ERROR_DATAFIELD66_LOCATIONPOSTAL_CODE
/* 834 */           .getErrorCode(), 
/* 835 */           SubmissionErrorCodes.ERROR_DATAFIELD66_LOCATIONPOSTAL_CODE
/* 836 */           .getErrorDescription() + 
/* 837 */           "\n" + 
/* 838 */           "RecordType:" + 
/* 839 */           recordType + 
/* 840 */           "|" + 
/* 841 */           "RecordNumber:" + 
/* 842 */           recordNumber + 
/* 843 */           "|" + 
/* 844 */           "LocationPostalCode" + 
/* 845 */           "|" + 
/* 846 */           "This field length Cannot be greater than 15");
/* 847 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateMerchantCategoryCode(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 882 */     ErrorObject errorObj = null;
/*     */     
/* 884 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 903 */       if (CommonValidator.validateData(value, 
/* 904 */         com.americanexpress.ips.gfsg.constants.SubmissionConstants.RECORD_VALUE[64]))
/*     */       {
/* 906 */         String reqLength = CommonValidator.validateLength(value, 4, 4);
/*     */         
/* 908 */         if (reqLength.equals("greaterThanMax")) {
/* 909 */           errorObj = new ErrorObject(
/* 910 */             SubmissionErrorCodes.ERROR_DATAFIELD599_MRCHNT_CATGRYCODE
/* 911 */             .getErrorCode(), 
/* 912 */             SubmissionErrorCodes.ERROR_DATAFIELD599_MRCHNT_CATGRYCODE
/* 913 */             .getErrorDescription() + 
/* 914 */             "\n" + 
/* 915 */             "RecordType:" + 
/* 916 */             recordType + 
/* 917 */             "|" + 
/* 918 */             "RecordNumber:" + 
/* 919 */             recordNumber + 
/* 920 */             "|" + 
/* 921 */             "MerchantCategoryCode" + 
/* 922 */             "|" + 
/* 923 */             "This field length Cannot be greater than 4");
/* 924 */           errorCodes.add(errorObj);
/*     */         }
/*     */       } else {
/* 927 */         errorObj = new ErrorObject(
/* 928 */           SubmissionErrorCodes.ERROR_DATAFIELD599_MRCHNT_CATGRYCODE
/* 929 */           .getErrorCode(), 
/* 930 */           SubmissionErrorCodes.ERROR_DATAFIELD599_MRCHNT_CATGRYCODE
/* 931 */           .getErrorDescription() + 
/* 932 */           "\n" + 
/* 933 */           "RecordType:" + 
/* 934 */           recordType + 
/* 935 */           "|" + 
/* 936 */           "RecordNumber:" + 
/* 937 */           recordNumber + 
/* 938 */           "|" + 
/* 939 */           "MerchantCategoryCode" + 
/* 940 */           "|" + 
/* 941 */           "This field can only be Numeric");
/* 942 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void validateSellerId(String value, List<ErrorObject> errorCodes)
/*     */     throws SettlementException
/*     */   {
/* 959 */     ErrorObject errorObj = null;
/*     */     
/* 961 */     if (!CommonValidator.isNullOrEmpty(value))
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 967 */       String reqLength = 
/* 968 */         CommonValidator.validateLength(value, 20, 20);
/*     */       
/* 970 */       if (reqLength.equals("greaterThanMax")) {
/* 971 */         errorObj = new ErrorObject(
/* 972 */           SubmissionErrorCodes.ERROR_DATAFIELD600_SELLERID
/* 973 */           .getErrorCode(), 
/* 974 */           SubmissionErrorCodes.ERROR_DATAFIELD600_SELLERID
/* 975 */           .getErrorDescription() + 
/* 976 */           "\n" + 
/* 977 */           "RecordType:" + 
/* 978 */           recordType + 
/* 979 */           "|" + 
/* 980 */           "RecordNumber:" + 
/* 981 */           recordNumber + 
/* 982 */           "|" + 
/* 983 */           "SellerId" + 
/* 984 */           "|" + 
/* 985 */           "This field length Cannot be greater than 20");
/* 986 */         errorCodes.add(errorObj);
/*     */       }
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\validator\LocationTAARecValidator.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */