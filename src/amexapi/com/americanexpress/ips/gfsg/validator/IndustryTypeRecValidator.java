/*    */ package com.americanexpress.ips.gfsg.validator;
/*    */ 
/*    */ import com.americanexpress.ips.gfsg.beans.ErrorObject;
/*    */ import com.americanexpress.ips.gfsg.beans.common.TransactionAdviceAddendumBean;
/*    */ import com.americanexpress.ips.gfsg.beans.common.TransactionAdviceBasicBean;
/*    */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.AirlineIndustryTAABean;
/*    */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.AutoRentalIndustryTAABean;
/*    */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.CommunicationServicesIndustryBean;
/*    */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.EntertainmentTicketingIndustryTAABean;
/*    */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.InsuranceIndustryTAABean;
/*    */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.LodgingIndustryTAABean;
/*    */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.RailIndustryTAABean;
/*    */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.RetailIndustryTAABean;
/*    */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.TravelCruiseIndustryTAABean;
/*    */ import com.americanexpress.ips.gfsg.exceptions.SettlementException;
/*    */ import com.americanexpress.ips.gfsg.validator.industry.AirLineRecordValidator;
/*    */ import com.americanexpress.ips.gfsg.validator.industry.AutoRentalRecordValidator;
/*    */ import com.americanexpress.ips.gfsg.validator.industry.CMSIndRecordValidator;
/*    */ import com.americanexpress.ips.gfsg.validator.industry.ETTIndRecordValidator;
/*    */ import com.americanexpress.ips.gfsg.validator.industry.InsuranceRecordValidator;
/*    */ import com.americanexpress.ips.gfsg.validator.industry.LodgingRecordValidator;
/*    */ import com.americanexpress.ips.gfsg.validator.industry.RailIndRecordValidator;
/*    */ import com.americanexpress.ips.gfsg.validator.industry.RetailRecordValidator;
/*    */ import com.americanexpress.ips.gfsg.validator.industry.TCIndRecordValidator;
/*    */ import java.util.List;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class IndustryTypeRecValidator
/*    */ {
/*    */   public static void validtateIndustryRecordType(TransactionAdviceBasicBean transactionAdviceBasicType, TransactionAdviceAddendumBean transactionAddendumType, List<ErrorObject> errorCodes)
/*    */     throws SettlementException
/*    */   {
/* 50 */     if ((transactionAddendumType instanceof AirlineIndustryTAABean))
/*    */     {
/* 52 */       AirLineRecordValidator.validateAirLineTypeRec(transactionAdviceBasicType, transactionAddendumType, errorCodes);
/*    */     }
/* 54 */     else if ((transactionAddendumType instanceof AutoRentalIndustryTAABean))
/*    */     {
/* 56 */       AutoRentalRecordValidator.validateAutoRentalRecord(transactionAdviceBasicType, transactionAddendumType, errorCodes);
/*    */     }
/* 58 */     else if ((transactionAddendumType instanceof CommunicationServicesIndustryBean))
/*    */     {
/* 60 */       CMSIndRecordValidator.validateCMSIndRecord(transactionAdviceBasicType, transactionAddendumType, errorCodes);
/*    */     }
/* 62 */     else if ((transactionAddendumType instanceof EntertainmentTicketingIndustryTAABean))
/*    */     {
/* 64 */       ETTIndRecordValidator.validateETTIndRecord(transactionAdviceBasicType, transactionAddendumType, errorCodes);
/*    */     }
/* 66 */     else if ((transactionAddendumType instanceof InsuranceIndustryTAABean))
/*    */     {
/* 68 */       InsuranceRecordValidator.validateInsuranceRecord(transactionAdviceBasicType, transactionAddendumType, errorCodes);
/*    */     }
/* 70 */     else if ((transactionAddendumType instanceof LodgingIndustryTAABean))
/*    */     {
/* 72 */       LodgingRecordValidator.validateLodgingTypeRec(transactionAdviceBasicType, transactionAddendumType, errorCodes);
/*    */     }
/* 74 */     else if ((transactionAddendumType instanceof RailIndustryTAABean))
/*    */     {
/* 76 */       RailIndRecordValidator.validateRailIndRecord(transactionAdviceBasicType, transactionAddendumType, errorCodes);
/*    */     }
/* 78 */     else if ((transactionAddendumType instanceof RetailIndustryTAABean))
/*    */     {
/* 80 */       RetailRecordValidator.validateRetailIndRecord(transactionAdviceBasicType, transactionAddendumType, errorCodes);
/*    */     }
/* 82 */     else if ((transactionAddendumType instanceof TravelCruiseIndustryTAABean))
/*    */     {
/* 84 */       TCIndRecordValidator.validateTCIndRecord(transactionAdviceBasicType, transactionAddendumType, errorCodes);
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\validator\IndustryTypeRecValidator.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */