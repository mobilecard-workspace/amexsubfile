/*    */ package com.americanexpress.ips.gfsg.validator;
/*    */ 
/*    */ import com.americanexpress.ips.gfsg.beans.ErrorObject;
/*    */ import com.americanexpress.ips.gfsg.beans.common.TransactionAdviceAddendumBean;
/*    */ import com.americanexpress.ips.gfsg.beans.common.TransactionAdviceBasicBean;
/*    */ import com.americanexpress.ips.gfsg.beans.common.taa.nonindustrytype.CPSLevel2Bean;
/*    */ import com.americanexpress.ips.gfsg.beans.common.taa.nonindustrytype.DeferredPaymentPlanBean;
/*    */ import com.americanexpress.ips.gfsg.beans.common.taa.nonindustrytype.EMVBean;
/*    */ import com.americanexpress.ips.gfsg.exceptions.SettlementException;
/*    */ import com.americanexpress.ips.gfsg.validator.nonindustry.CPSL2RecordValidator;
/*    */ import com.americanexpress.ips.gfsg.validator.nonindustry.DPPRecordValidator;
/*    */ import com.americanexpress.ips.gfsg.validator.nonindustry.EMVRecordValidator;
/*    */ import java.util.List;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class NonIndustryTypeRecValidator
/*    */ {
/*    */   public static void validtateNonInIndustryRecordType(TransactionAdviceBasicBean transactionAdviceBasicType, TransactionAdviceAddendumBean transactionAdviceAddendumType, List<ErrorObject> errorCodes)
/*    */     throws SettlementException
/*    */   {
/* 39 */     if ((transactionAdviceAddendumType instanceof CPSLevel2Bean))
/*    */     {
/* 41 */       CPSL2RecordValidator.validateCPSL2Record(transactionAdviceBasicType, transactionAdviceAddendumType, errorCodes);
/*    */ 
/*    */     }
/* 44 */     else if ((transactionAdviceAddendumType instanceof DeferredPaymentPlanBean))
/*    */     {
/* 46 */       DPPRecordValidator.validateDPPRecord(transactionAdviceBasicType, transactionAdviceAddendumType, errorCodes);
/*    */     }
/* 48 */     else if ((transactionAdviceAddendumType instanceof EMVBean))
/*    */     {
/* 50 */       EMVRecordValidator.validateEMVRecord(transactionAdviceBasicType, transactionAdviceAddendumType, errorCodes);
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\com\americanexpress\ips\gfsg\validator\NonIndustryTypeRecValidator.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */