/*      */ import com.americanexpress.ips.gfsg.beans.ErrorObject;
/*      */ import com.americanexpress.ips.gfsg.beans.SettlementRequestDataObject;
/*      */ import com.americanexpress.ips.gfsg.beans.SettlementResponseDataObject;
/*      */ import com.americanexpress.ips.gfsg.beans.acknowledgement.AcknowledgementReportObject;
/*      */ import com.americanexpress.ips.gfsg.beans.common.TransactionAdviceBasicBean;
/*      */ import com.americanexpress.ips.gfsg.beans.common.TransactionAdviceDetailBean;
/*      */ import com.americanexpress.ips.gfsg.beans.common.TransactionBatchTrailerBean;
/*      */ import com.americanexpress.ips.gfsg.beans.common.TransactionFileHeaderBean;
/*      */ import com.americanexpress.ips.gfsg.beans.common.TransactionFileSummaryBean;
/*      */ import com.americanexpress.ips.gfsg.beans.common.TransactionTBTSpecificBean;
/*      */ import com.americanexpress.ips.gfsg.beans.common.taa.LocationDetailTAABean;
/*      */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.AirlineIndustryTAABean;
/*      */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.AutoRentalIndustryTAABean;
/*      */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.CommunicationServicesIndustryBean;
/*      */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.EntertainmentTicketingIndustryTAABean;
/*      */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.InsuranceIndustryTAABean;
/*      */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.LodgingIndustryTAABean;
/*      */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.RailIndustryTAABean;
/*      */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.RetailIndustryTAABean;
/*      */ import com.americanexpress.ips.gfsg.beans.common.taa.industrytype.TravelCruiseIndustryTAABean;
/*      */ import com.americanexpress.ips.gfsg.beans.common.taa.nonindustrytype.CPSLevel2Bean;
/*      */ import com.americanexpress.ips.gfsg.beans.common.taa.nonindustrytype.DeferredPaymentPlanBean;
/*      */ import com.americanexpress.ips.gfsg.beans.common.taa.nonindustrytype.EMVBean;
/*      */ import com.americanexpress.ips.gfsg.connection.PropertyReader;
/*      */ import com.americanexpress.ips.gfsg.processor.iso.RequestProcessorFactory;
/*      */ import java.io.BufferedReader;
/*      */ import java.io.DataInputStream;
/*      */ import java.io.FileInputStream;
/*      */ import java.io.FileNotFoundException;
/*      */ import java.io.InputStreamReader;
/*      */ import java.io.PrintStream;
/*      */ import java.text.SimpleDateFormat;
/*      */ import java.util.ArrayList;
/*      */ import java.util.Calendar;
/*      */ import java.util.Iterator;
/*      */ import java.util.List;
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ public class SettlementTest
/*      */ {
/*      */   public static void main(String[] args)
/*      */     throws Exception
/*      */   {
/*   50 */     SettlementTest objTest = new SettlementTest();
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*   66 */     PropertyReader propertyReader = new PropertyReader();
/*      */     
/*   68 */     List<ErrorObject> connErrorList = new ArrayList();
/*   69 */     propertyReader = propertyReader.loadPropertyFile("/settlement.properties");
/*      */     
/*   71 */     List<?> ackResult = RequestProcessorFactory.fetchAcknowledgementInPrintedFormat(propertyReader, connErrorList);
/*      */     
/*   73 */     AcknowledgementReportObject ackObj = (AcknowledgementReportObject)ackResult.get(0);
/*   74 */     System.out.println("Acknowledgement Parameters in Test File :::" + ackObj.getSubFileDate() + "||" + 
/*   75 */       ackObj.getRefNumber() + "||" + 
/*   76 */       ackObj.getSeqNumber() + "||" + 
/*   77 */       ackObj.getFileTrackNumber());
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void lodgingTest()
/*      */     throws Exception
/*      */   {
/*   88 */     SimpleDateFormat datefmt = new SimpleDateFormat("MMdd");
/*      */     
/*   90 */     SimpleDateFormat timefmt = new SimpleDateFormat("hhmm");
/*   91 */     StringBuffer ab = new StringBuffer();
/*   92 */     ab.append(datefmt.format(Calendar.getInstance().getTime()));
/*   93 */     ab.append(timefmt.format(Calendar.getInstance().getTime()));
/*   94 */     System.out.println("asdfsadsad" + ab.toString());
/*      */     
/*   96 */     TransactionFileHeaderBean transactionFileHeaderBean = new TransactionFileHeaderBean();
/*      */     
/*   98 */     transactionFileHeaderBean.setRecordType("TFH");
/*   99 */     transactionFileHeaderBean.setRecordNumber("00000001");
/*  100 */     transactionFileHeaderBean.setSubmitterId("GWS002");
/*      */     
/*  102 */     transactionFileHeaderBean.setSubmitterFileReferenceNumber("1" + ab.toString());
/*      */     
/*  104 */     transactionFileHeaderBean.setFileCreationDate("20110524");
/*  105 */     transactionFileHeaderBean.setFileCreationTime("172020");
/*  106 */     transactionFileHeaderBean.setFileVersionNumber("12010000");
/*      */     
/*      */ 
/*      */ 
/*  110 */     TransactionFileSummaryBean transactionFileSummaryBean = new TransactionFileSummaryBean();
/*      */     
/*  112 */     transactionFileSummaryBean.setRecordType("TFS");
/*  113 */     transactionFileSummaryBean.setRecordNumber("00000007");
/*  114 */     transactionFileSummaryBean.setNumberOfDebits("1");
/*  115 */     transactionFileSummaryBean.setHashTotalDebitAmount("500");
/*  116 */     transactionFileSummaryBean.setNumberOfCredits("0");
/*  117 */     transactionFileSummaryBean.setHashTotalCreditAmount("0");
/*  118 */     transactionFileSummaryBean.setHashTotalAmount("500");
/*      */     
/*      */ 
/*      */ 
/*  122 */     TransactionBatchTrailerBean transactionBatchTrailerBean = new TransactionBatchTrailerBean();
/*      */     
/*  124 */     transactionBatchTrailerBean.setMerchantId("5021045083");
/*  125 */     transactionBatchTrailerBean.setRecordNumber("00000006");
/*  126 */     transactionBatchTrailerBean.setRecordType("TBT");
/*  127 */     transactionBatchTrailerBean.setTbtCurrencyCode("840");
/*  128 */     transactionBatchTrailerBean.setTbtAmount("500");
/*  129 */     transactionBatchTrailerBean.setTbtAmountSign("+");
/*  130 */     transactionBatchTrailerBean.setTbtCreationDate("21110524");
/*  131 */     transactionBatchTrailerBean.setTbtIdentificationNumber("100000000000005");
/*  132 */     transactionBatchTrailerBean.setTbtImageSequenceNumber("654321");
/*  133 */     transactionBatchTrailerBean.setTotalNoOfTabs("1");
/*      */     
/*      */ 
/*      */ 
/*  137 */     LocationDetailTAABean locationDetailTAABean = new LocationDetailTAABean();
/*      */     
/*      */ 
/*  140 */     locationDetailTAABean.setRecordType("TAA");
/*  141 */     locationDetailTAABean.setRecordNumber("00000005");
/*  142 */     locationDetailTAABean.setLocationName("ABC");
/*  143 */     locationDetailTAABean.setLocationAddress("ABC");
/*  144 */     locationDetailTAABean.setLocationCity("Sydney");
/*  145 */     locationDetailTAABean.setLocationRegion("AU");
/*  146 */     locationDetailTAABean.setLocationCountryCode("036");
/*  147 */     locationDetailTAABean.setLocationPostalCode("2000");
/*  148 */     locationDetailTAABean.setTransactionIdentifier("373872186011087");
/*  149 */     locationDetailTAABean.setAddendaTypeCode("99");
/*  150 */     locationDetailTAABean.setMerchantCategoryCode("7011");
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*  155 */     TransactionAdviceBasicBean transactionAdviceBasicBean = new TransactionAdviceBasicBean();
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*  160 */     LodgingIndustryTAABean lodgingIndustryTAABean = new LodgingIndustryTAABean();
/*      */     
/*  162 */     lodgingIndustryTAABean.setRecordType("TAA");
/*  163 */     lodgingIndustryTAABean.setRecordNumber("00000004");
/*  164 */     lodgingIndustryTAABean.setTransactionIdentifier("373872186011087");
/*  165 */     lodgingIndustryTAABean.setFormatCode("01");
/*  166 */     lodgingIndustryTAABean.setLodgingSpecialProgramCode("1");
/*  167 */     lodgingIndustryTAABean.setLodgingCheckInDate("110415");
/*  168 */     lodgingIndustryTAABean.setLodgingCheckOutDate("110419");
/*  169 */     lodgingIndustryTAABean.setLodgingRoomRate1("100");
/*  170 */     lodgingIndustryTAABean.setNumberOfNightsAtRoomRate1("3");
/*  171 */     lodgingIndustryTAABean.setLodgingRoomRate2("100");
/*  172 */     lodgingIndustryTAABean.setNumberOfNightsAtRoomRate2("3");
/*  173 */     lodgingIndustryTAABean.setLodgingRoomRate3("100");
/*  174 */     lodgingIndustryTAABean.setNumberOfNightsAtRoomRate3("3");
/*  175 */     lodgingIndustryTAABean.setLodgingRenterName("ABCRenter");
/*  176 */     lodgingIndustryTAABean.setLodgingFolioNumber("Lod123");
/*  177 */     lodgingIndustryTAABean.setAddendaTypeCode("02");
/*      */     
/*  179 */     transactionAdviceBasicBean.setTransactionAdviceAddendumBean(lodgingIndustryTAABean);
/*      */     
/*      */ 
/*      */ 
/*  183 */     DeferredPaymentPlanBean dpPlanBean = new DeferredPaymentPlanBean();
/*      */     
/*  185 */     dpPlanBean.setRecordType("TAA");
/*  186 */     dpPlanBean.setRecordNumber("04");
/*  187 */     dpPlanBean.setTransactionIdentifier("373872186011087");
/*  188 */     dpPlanBean.setAddendaTypeCode("02");
/*  189 */     dpPlanBean.setFullTransactionAmount("01000100010");
/*  190 */     dpPlanBean.setTypeOfPlanCode("003");
/*  191 */     dpPlanBean.setNoOfInstallments("0010");
/*  192 */     dpPlanBean.setAmountOfInstallment("123456789012");
/*  193 */     dpPlanBean.setInstallmentNumber("0001");
/*  194 */     dpPlanBean.setContractNumber("12345678901234");
/*  195 */     dpPlanBean.setPaymentTypeCode1("02");
/*  196 */     dpPlanBean.setPaymentTypeAmount1("00000100076");
/*  197 */     dpPlanBean.setPaymentTypeCode2("02");
/*  198 */     dpPlanBean.setPaymentTypeAmount2("00000100076");
/*  199 */     dpPlanBean.setPaymentTypeCode3("02");
/*  200 */     dpPlanBean.setPaymentTypeAmount3("00000100076");
/*  201 */     dpPlanBean.setPaymentTypeCode4("02");
/*  202 */     dpPlanBean.setPaymentTypeAmount4("00000100076");
/*  203 */     dpPlanBean.setPaymentTypeCode5("02");
/*  204 */     dpPlanBean.setPaymentTypeAmount5("00000100076");
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*  209 */     TransactionAdviceDetailBean transactionAdviceDetailBean = new TransactionAdviceDetailBean();
/*      */     
/*  211 */     transactionAdviceDetailBean.setRecordType("TAD");
/*  212 */     transactionAdviceDetailBean.setRecordNumber("00000003");
/*  213 */     transactionAdviceDetailBean.setTransactionIdentifier("373872186011087");
/*  214 */     transactionAdviceDetailBean.setAdditionalAmountType1("061");
/*  215 */     transactionAdviceDetailBean.setAdditionalAmount1("100");
/*  216 */     transactionAdviceDetailBean.setAdditionalAmountSign1("+");
/*  217 */     transactionAdviceDetailBean.setAdditionalAmountType2("034");
/*  218 */     transactionAdviceDetailBean.setAdditionalAmount2("100");
/*  219 */     transactionAdviceDetailBean.setAdditionalAmountSign2("-");
/*  220 */     transactionAdviceDetailBean.setAdditionalAmountType3("050");
/*  221 */     transactionAdviceDetailBean.setAdditionalAmount3("100");
/*  222 */     transactionAdviceDetailBean.setAdditionalAmountSign3("+");
/*  223 */     transactionAdviceDetailBean.setAdditionalAmountType4("054");
/*  224 */     transactionAdviceDetailBean.setAdditionalAmount4("100");
/*  225 */     transactionAdviceDetailBean.setAdditionalAmountSign4("-");
/*  226 */     transactionAdviceDetailBean.setAdditionalAmountType5("049");
/*  227 */     transactionAdviceDetailBean.setAdditionalAmount5("100");
/*  228 */     transactionAdviceDetailBean.setAdditionalAmountSign5("+");
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  234 */     List<TransactionAdviceBasicBean> transactionAdviceBasicType = new ArrayList();
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  240 */     transactionAdviceBasicBean.setRecordNumber("00000002");
/*  241 */     transactionAdviceBasicBean.setRecordType("TAB");
/*  242 */     transactionAdviceBasicBean.setTransactionIdentifier("373872186011087");
/*  243 */     transactionAdviceBasicBean.setFormatCode("01");
/*  244 */     transactionAdviceBasicBean.setMediaCode("01");
/*  245 */     transactionAdviceBasicBean.setSubmissionMethod("01");
/*  246 */     transactionAdviceBasicBean.setApprovalCode("NNNNN");
/*  247 */     transactionAdviceBasicBean.setPrimaryAccountNumber("373953192351004");
/*  248 */     transactionAdviceBasicBean.setCardExpiryDate("1209");
/*  249 */     transactionAdviceBasicBean.setTransactionDate("20110524");
/*  250 */     transactionAdviceBasicBean.setTransactionTime("172020");
/*  251 */     transactionAdviceBasicBean.setTransactionAmount("500");
/*  252 */     transactionAdviceBasicBean.setProcessingCode("000000");
/*  253 */     transactionAdviceBasicBean.setTransactionCurrencyCode("840");
/*  254 */     transactionAdviceBasicBean.setExtendedPaymentData("01");
/*  255 */     transactionAdviceBasicBean.setMerchantId("5021045083");
/*  256 */     transactionAdviceBasicBean.setPosDataCode("161201254110");
/*  257 */     transactionAdviceBasicBean.setInvoiceReferenceNumber("1234");
/*  258 */     transactionAdviceBasicBean.setElectronicCommerceIndicator("05");
/*      */     
/*      */ 
/*  261 */     transactionAdviceBasicBean.setLocationDetailTAABean(locationDetailTAABean);
/*  262 */     transactionAdviceBasicBean.setTransactionAdviceDetailBean(transactionAdviceDetailBean);
/*  263 */     transactionAdviceBasicBean.setTransactionAdviceAddendumBean(lodgingIndustryTAABean);
/*      */     
/*  265 */     transactionAdviceBasicType.add(transactionAdviceBasicBean);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  272 */     TransactionTBTSpecificBean transactionTBTSpecificBean = new TransactionTBTSpecificBean();
/*      */     
/*  274 */     transactionTBTSpecificBean
/*  275 */       .setTransactionAdviceBasicType(transactionAdviceBasicType);
/*  276 */     transactionTBTSpecificBean
/*  277 */       .setTransactionBatchTrailerBean(transactionBatchTrailerBean);
/*      */     
/*  279 */     List<TransactionTBTSpecificBean> transactionTBTSpecificType = new ArrayList();
/*      */     
/*  281 */     transactionTBTSpecificType.add(transactionTBTSpecificBean);
/*      */     
/*      */ 
/*      */ 
/*  285 */     SettlementRequestDataObject settlementRequestDataObject = new SettlementRequestDataObject();
/*      */     
/*  287 */     settlementRequestDataObject.setFormatCode("");
/*  288 */     settlementRequestDataObject.setMerchantId("");
/*  289 */     settlementRequestDataObject.setTransactionIdentifier("");
/*  290 */     settlementRequestDataObject
/*  291 */       .setTransactionFileHeaderType(transactionFileHeaderBean);
/*  292 */     settlementRequestDataObject
/*  293 */       .setTransactionFileSummaryType(transactionFileSummaryBean);
/*  294 */     settlementRequestDataObject
/*  295 */       .setTransactionTBTSpecificType(transactionTBTSpecificType);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  301 */     PropertyReader propertyReader = new PropertyReader();
/*      */     
/*  303 */     List<ErrorObject> connErrorList = new ArrayList();
/*  304 */     propertyReader = propertyReader.loadPropertyFile("/settlement.properties");
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*  309 */     SettlementResponseDataObject settlementResponseDataObject = RequestProcessorFactory.processRequest(settlementRequestDataObject, propertyReader);
/*      */     
/*      */ 
/*      */ 
/*  313 */     RequestProcessorFactory.fetchConfirmationReportInRawData(propertyReader, connErrorList);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */     try
/*      */     {
/*  320 */       if (!settlementResponseDataObject.getActionCodeDescription().isEmpty())
/*      */       {
/*  322 */         Iterator<?> iterater = settlementResponseDataObject.getActionCodeDescription().iterator();
/*  323 */         while (iterater.hasNext()) {
/*  324 */           ErrorObject errorObject = (ErrorObject)iterater.next();
/*  325 */           System.out.print(errorObject.getErrorCode() + "-" + errorObject.getErrorDescription() + "\n");
/*      */         }
/*      */       } else {
/*  328 */         System.out.print("Error List is empty\n");
/*      */       }
/*      */       
/*      */ 
/*      */     }
/*      */     catch (Exception e)
/*      */     {
/*      */ 
/*  336 */       System.out.println("Error Object is Empty");
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void airLineTest()
/*      */     throws Exception
/*      */   {
/*  348 */     SimpleDateFormat datefmt = new SimpleDateFormat("MMdd");
/*      */     
/*  350 */     SimpleDateFormat timefmt = new SimpleDateFormat("hhmm");
/*  351 */     StringBuffer ab = new StringBuffer();
/*  352 */     ab.append(datefmt.format(Calendar.getInstance().getTime()));
/*  353 */     ab.append(timefmt.format(Calendar.getInstance().getTime()));
/*      */     
/*      */ 
/*  356 */     TransactionFileHeaderBean transactionFileHeaderBean = new TransactionFileHeaderBean();
/*      */     
/*  358 */     transactionFileHeaderBean.setRecordType("TFH");
/*  359 */     transactionFileHeaderBean.setRecordNumber("00000001");
/*  360 */     transactionFileHeaderBean.setSubmitterId("GWS002");
/*      */     
/*  362 */     transactionFileHeaderBean.setSubmitterFileReferenceNumber("1" + ab.toString());
/*      */     
/*  364 */     transactionFileHeaderBean.setFileCreationDate("20110524");
/*  365 */     transactionFileHeaderBean.setFileCreationTime("172020");
/*  366 */     transactionFileHeaderBean.setFileVersionNumber("12010000");
/*      */     
/*      */ 
/*  369 */     TransactionAdviceBasicBean transactionAdviceBasicBean = new TransactionAdviceBasicBean();
/*      */     
/*      */ 
/*      */ 
/*  373 */     transactionAdviceBasicBean.setRecordNumber("00000002");
/*  374 */     transactionAdviceBasicBean.setRecordType("TAB");
/*  375 */     transactionAdviceBasicBean.setTransactionIdentifier("373872186011087");
/*  376 */     transactionAdviceBasicBean.setFormatCode("01");
/*  377 */     transactionAdviceBasicBean.setMediaCode("01");
/*  378 */     transactionAdviceBasicBean.setSubmissionMethod("01");
/*  379 */     transactionAdviceBasicBean.setApprovalCode("NNNNN");
/*  380 */     transactionAdviceBasicBean.setPrimaryAccountNumber("373953192351004");
/*  381 */     transactionAdviceBasicBean.setCardExpiryDate("1209");
/*  382 */     transactionAdviceBasicBean.setTransactionDate("20110524");
/*  383 */     transactionAdviceBasicBean.setTransactionTime("172020");
/*  384 */     transactionAdviceBasicBean.setTransactionAmount("500");
/*  385 */     transactionAdviceBasicBean.setProcessingCode("000000");
/*  386 */     transactionAdviceBasicBean.setTransactionCurrencyCode("840");
/*  387 */     transactionAdviceBasicBean.setExtendedPaymentData("01");
/*  388 */     transactionAdviceBasicBean.setMerchantId("5021045083");
/*  389 */     transactionAdviceBasicBean.setPosDataCode("161201254110");
/*  390 */     transactionAdviceBasicBean.setInvoiceReferenceNumber("1234");
/*  391 */     transactionAdviceBasicBean.setElectronicCommerceIndicator("05");
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*  396 */     TransactionAdviceDetailBean transactionAdviceDetailBean = new TransactionAdviceDetailBean();
/*      */     
/*  398 */     transactionAdviceDetailBean.setRecordType("TAD");
/*  399 */     transactionAdviceDetailBean.setRecordNumber("00000008");
/*  400 */     transactionAdviceDetailBean.setTransactionIdentifier("373872186011087");
/*  401 */     transactionAdviceDetailBean.setAdditionalAmountType1("29");
/*  402 */     transactionAdviceDetailBean.setAdditionalAmount1("905");
/*  403 */     transactionAdviceDetailBean.setAdditionalAmountSign1("+");
/*  404 */     transactionAdviceDetailBean.setAdditionalAmountType2("37");
/*  405 */     transactionAdviceDetailBean.setAdditionalAmount2("2000");
/*  406 */     transactionAdviceDetailBean.setAdditionalAmountSign2("+");
/*  407 */     transactionAdviceDetailBean.setAdditionalAmountType3("48");
/*  408 */     transactionAdviceDetailBean.setAdditionalAmount3("1000");
/*  409 */     transactionAdviceDetailBean.setAdditionalAmountSign3("+");
/*  410 */     transactionAdviceDetailBean.setAdditionalAmountType4("052");
/*  411 */     transactionAdviceDetailBean.setAdditionalAmount4("500");
/*  412 */     transactionAdviceDetailBean.setAdditionalAmountSign4("+");
/*  413 */     transactionAdviceDetailBean.setAdditionalAmountType5("52");
/*  414 */     transactionAdviceDetailBean.setAdditionalAmount5("500");
/*  415 */     transactionAdviceDetailBean.setAdditionalAmountSign5("+");
/*      */     
/*  417 */     transactionAdviceBasicBean.setTransactionAdviceDetailBean(transactionAdviceDetailBean);
/*      */     
/*      */ 
/*  420 */     LocationDetailTAABean locationDetailTAABean = new LocationDetailTAABean();
/*      */     
/*      */ 
/*  423 */     locationDetailTAABean.setRecordType("TAA");
/*  424 */     locationDetailTAABean.setRecordNumber("00000005");
/*  425 */     locationDetailTAABean.setLocationName("Newyork");
/*  426 */     locationDetailTAABean.setLocationAddress("Newyork Airport");
/*  427 */     locationDetailTAABean.setLocationCity("Sydney");
/*  428 */     locationDetailTAABean.setLocationRegion("CA");
/*  429 */     locationDetailTAABean.setLocationCountryCode("840");
/*  430 */     locationDetailTAABean.setLocationPostalCode("ABCDE1234567890");
/*  431 */     locationDetailTAABean.setTransactionIdentifier("373872186011087");
/*  432 */     locationDetailTAABean.setAddendaTypeCode("99");
/*  433 */     locationDetailTAABean.setMerchantCategoryCode("4511");
/*      */     
/*      */ 
/*  436 */     transactionAdviceBasicBean.setLocationDetailTAABean(locationDetailTAABean);
/*      */     
/*  438 */     AirlineIndustryTAABean airBean = new AirlineIndustryTAABean();
/*      */     
/*  440 */     airBean.setRecordType("TAA");
/*  441 */     airBean.setRecordNumber("004");
/*  442 */     airBean.setTransactionIdentifier("373872186011087");
/*  443 */     airBean.setFormatCode("01");
/*  444 */     airBean.setAddendaTypeCode("03");
/*  445 */     airBean.setTransactionType("TKT");
/*  446 */     airBean.setTicketNumber("TKT12345678901");
/*  447 */     airBean.setDocumentType("01");
/*  448 */     airBean.setAirlineProcessIdentifier("A12");
/*  449 */     airBean.setIataNumericCode("TKT12345");
/*  450 */     airBean.setTicketingCarrierName("TKTCAR1234567891234567890");
/*  451 */     airBean.setTicketIssueCity("NEWYORK");
/*  452 */     airBean.setTicketIssueDate("20110204");
/*  453 */     airBean.setNumberInParty("1");
/*  454 */     airBean.setPassengerName("THERISA");
/*  455 */     airBean.setConjunctionTicketIndicator("N");
/*  456 */     airBean.setTotalNumberOfAirSegments("4");
/*  457 */     airBean.setElectronicTicketIndicator("Y");
/*  458 */     airBean.setOriginalTransactionAmount("0000000000");
/*  459 */     airBean.setOriginalCurrencyCode("840");
/*      */     
/*  461 */     airBean.setStopoverIndicator1("O");
/*  462 */     airBean.setDepartureLocationCodeSegment1("MUM");
/*  463 */     airBean.setDepartureDateSegment1("20110216");
/*  464 */     airBean.setArrivalLocationCodeSegment1("DEL");
/*  465 */     airBean.setSegmentCarrierCode1("A1");
/*  466 */     airBean.setSegment1FareBasis("FAIRSEG12345678");
/*  467 */     airBean.setClassOfServiceCodeSegment1("A");
/*  468 */     airBean.setFlightNumberSegment1("ABC1");
/*  469 */     airBean.setSegment1Fare("1000");
/*      */     
/*  471 */     airBean.setStopoverIndicator2("X");
/*  472 */     airBean.setDepartureLocationCodeSegment2("DEL");
/*  473 */     airBean.setDepartureDateSegment2("20110224");
/*  474 */     airBean.setArrivalLocationCodeSegment2("NY");
/*  475 */     airBean.setSegmentCarrierCode2("A1");
/*  476 */     airBean.setSegment2FareBasis("FAIRSEG12345678");
/*  477 */     airBean.setClassOfServiceCodeSegment2("A2");
/*  478 */     airBean.setFlightNumberSegment2("ABC2");
/*  479 */     airBean.setSegment2Fare("2000");
/*      */     
/*  481 */     airBean.setStopoverIndicator3(" ");
/*  482 */     airBean.setDepartureLocationCodeSegment3("NY");
/*  483 */     airBean.setDepartureDateSegment3("20110216");
/*  484 */     airBean.setArrivalLocationCodeSegment3("DAL");
/*  485 */     airBean.setSegmentCarrierCode3("A1");
/*  486 */     airBean.setSegment3FareBasis("FAIRSEG12345678");
/*  487 */     airBean.setClassOfServiceCodeSegment3("A2");
/*  488 */     airBean.setFlightNumberSegment3("ABC3");
/*  489 */     airBean.setSegment3Fare("1000");
/*      */     
/*  491 */     airBean.setStopoverIndicator4("O");
/*  492 */     airBean.setDepartureLocationCodeSegment4("DAL");
/*  493 */     airBean.setDepartureDateSegment4("20110216");
/*  494 */     airBean.setArrivalLocationCodeSegment4("PHX");
/*  495 */     airBean.setSegmentCarrierCode4("A1");
/*  496 */     airBean.setSegment4FareBasis("FAIRSEG12345678");
/*  497 */     airBean.setClassOfServiceCodeSegment4("A2");
/*  498 */     airBean.setFlightNumberSegment4("ABC4");
/*  499 */     airBean.setSegment4Fare("100");
/*      */     
/*  501 */     airBean.setExchangedOrOriginalTicketNumber("");
/*      */     
/*      */ 
/*  504 */     transactionAdviceBasicBean.setTransactionAdviceAddendumBean(airBean);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  510 */     TransactionBatchTrailerBean transactionBatchTrailerBean = new TransactionBatchTrailerBean();
/*      */     
/*  512 */     transactionBatchTrailerBean.setMerchantId("5021045083");
/*  513 */     transactionBatchTrailerBean.setRecordNumber("00000006");
/*  514 */     transactionBatchTrailerBean.setRecordType("TBT");
/*  515 */     transactionBatchTrailerBean.setTbtCurrencyCode("840");
/*  516 */     transactionBatchTrailerBean.setTbtAmount("500");
/*  517 */     transactionBatchTrailerBean.setTbtAmountSign("+");
/*  518 */     transactionBatchTrailerBean.setTbtCreationDate("21110523");
/*  519 */     transactionBatchTrailerBean.setTbtIdentificationNumber("100000000000001");
/*  520 */     transactionBatchTrailerBean.setTbtImageSequenceNumber("654321");
/*  521 */     transactionBatchTrailerBean.setTotalNoOfTabs("1");
/*      */     
/*      */ 
/*  524 */     List<TransactionAdviceBasicBean> transactionAdviceBasicType = new ArrayList();
/*      */     
/*  526 */     transactionAdviceBasicType.add(transactionAdviceBasicBean);
/*      */     
/*  528 */     TransactionFileSummaryBean transactionFileSummaryBean = new TransactionFileSummaryBean();
/*      */     
/*  530 */     transactionFileSummaryBean.setRecordType("TFS");
/*  531 */     transactionFileSummaryBean.setRecordNumber("00000007");
/*  532 */     transactionFileSummaryBean.setNumberOfDebits("1");
/*  533 */     transactionFileSummaryBean.setHashTotalDebitAmount("500");
/*  534 */     transactionFileSummaryBean.setNumberOfCredits("0");
/*  535 */     transactionFileSummaryBean.setHashTotalCreditAmount("0");
/*  536 */     transactionFileSummaryBean.setHashTotalAmount("500");
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  542 */     TransactionTBTSpecificBean transactionTBTSpecificBean = new TransactionTBTSpecificBean();
/*      */     
/*  544 */     transactionTBTSpecificBean.setTransactionAdviceBasicType(transactionAdviceBasicType);
/*  545 */     transactionTBTSpecificBean.setTransactionBatchTrailerBean(transactionBatchTrailerBean);
/*      */     
/*  547 */     List<TransactionTBTSpecificBean> transactionTBTSpecificType = new ArrayList();
/*      */     
/*  549 */     transactionTBTSpecificType.add(transactionTBTSpecificBean);
/*      */     
/*      */ 
/*      */ 
/*  553 */     SettlementRequestDataObject settlementRequestDataObject = new SettlementRequestDataObject();
/*      */     
/*  555 */     settlementRequestDataObject
/*  556 */       .setTransactionFileHeaderType(transactionFileHeaderBean);
/*  557 */     settlementRequestDataObject
/*  558 */       .setTransactionFileSummaryType(transactionFileSummaryBean);
/*  559 */     settlementRequestDataObject
/*  560 */       .setTransactionTBTSpecificType(transactionTBTSpecificType);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  566 */     PropertyReader propertyReader = new PropertyReader();
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  575 */     SettlementResponseDataObject settlementResponseDataObject = RequestProcessorFactory.processRequest(settlementRequestDataObject, propertyReader);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */     try
/*      */     {
/*  586 */       if (!settlementResponseDataObject.getActionCodeDescription().isEmpty())
/*      */       {
/*  588 */         Iterator<?> iterater = settlementResponseDataObject.getActionCodeDescription().iterator();
/*  589 */         while (iterater.hasNext()) {
/*  590 */           ErrorObject errorObject = (ErrorObject)iterater.next();
/*  591 */           System.out.print(errorObject.getErrorCode() + "-" + errorObject.getErrorDescription() + "\n");
/*      */         }
/*      */       } else {
/*  594 */         System.out.print("Error List is empty\n");
/*      */       }
/*      */       
/*      */ 
/*      */     }
/*      */     catch (Exception e)
/*      */     {
/*      */ 
/*  602 */       System.out.println("Error Object is Empty");
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void insuranceTest()
/*      */     throws Exception
/*      */   {
/*  614 */     SimpleDateFormat datefmt = new SimpleDateFormat("MMdd");
/*      */     
/*  616 */     SimpleDateFormat timefmt = new SimpleDateFormat("hhmm");
/*  617 */     StringBuffer ab = new StringBuffer();
/*  618 */     ab.append(datefmt.format(Calendar.getInstance().getTime()));
/*  619 */     ab.append(timefmt.format(Calendar.getInstance().getTime()));
/*      */     
/*      */ 
/*  622 */     TransactionFileHeaderBean transactionFileHeaderBean = new TransactionFileHeaderBean();
/*      */     
/*  624 */     transactionFileHeaderBean.setRecordType("TFH");
/*  625 */     transactionFileHeaderBean.setRecordNumber("00000001");
/*  626 */     transactionFileHeaderBean.setSubmitterId("GWS002");
/*      */     
/*  628 */     transactionFileHeaderBean.setSubmitterFileReferenceNumber("1" + ab.toString());
/*      */     
/*  630 */     transactionFileHeaderBean.setFileCreationDate("20110523");
/*  631 */     transactionFileHeaderBean.setFileCreationTime("172020");
/*  632 */     transactionFileHeaderBean.setFileVersionNumber("12010000");
/*      */     
/*      */ 
/*  635 */     TransactionAdviceBasicBean transactionAdviceBasicBean = new TransactionAdviceBasicBean();
/*      */     
/*      */ 
/*      */ 
/*  639 */     transactionAdviceBasicBean.setRecordNumber("00000002");
/*  640 */     transactionAdviceBasicBean.setRecordType("TAB");
/*  641 */     transactionAdviceBasicBean.setTransactionIdentifier("373872186011087");
/*  642 */     transactionAdviceBasicBean.setFormatCode("01");
/*  643 */     transactionAdviceBasicBean.setMediaCode("01");
/*  644 */     transactionAdviceBasicBean.setSubmissionMethod("01");
/*  645 */     transactionAdviceBasicBean.setApprovalCode("NNNNN");
/*  646 */     transactionAdviceBasicBean.setPrimaryAccountNumber("373953192351004");
/*  647 */     transactionAdviceBasicBean.setCardExpiryDate("1209");
/*  648 */     transactionAdviceBasicBean.setTransactionDate("20110523");
/*  649 */     transactionAdviceBasicBean.setTransactionTime("172020");
/*  650 */     transactionAdviceBasicBean.setTransactionAmount("500");
/*  651 */     transactionAdviceBasicBean.setProcessingCode("000000");
/*  652 */     transactionAdviceBasicBean.setTransactionCurrencyCode("840");
/*  653 */     transactionAdviceBasicBean.setExtendedPaymentData("01");
/*  654 */     transactionAdviceBasicBean.setMerchantId("5021045083");
/*  655 */     transactionAdviceBasicBean.setPosDataCode("161201254110");
/*  656 */     transactionAdviceBasicBean.setInvoiceReferenceNumber("1234");
/*  657 */     transactionAdviceBasicBean.setElectronicCommerceIndicator("05");
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*  662 */     TransactionAdviceDetailBean transactionAdviceDetailBean = new TransactionAdviceDetailBean();
/*      */     
/*  664 */     transactionAdviceDetailBean.setRecordType("TAD");
/*  665 */     transactionAdviceDetailBean.setRecordNumber("00000003");
/*  666 */     transactionAdviceDetailBean.setTransactionIdentifier("373872186011087");
/*  667 */     transactionAdviceDetailBean.setAdditionalAmountType1("036");
/*  668 */     transactionAdviceDetailBean.setAdditionalAmount1("10200");
/*  669 */     transactionAdviceDetailBean.setAdditionalAmountSign1("+");
/*  670 */     transactionAdviceDetailBean.setAdditionalAmountType2("31");
/*  671 */     transactionAdviceDetailBean.setAdditionalAmount2("40000");
/*  672 */     transactionAdviceDetailBean.setAdditionalAmountSign2("+");
/*  673 */     transactionAdviceDetailBean.setAdditionalAmountType3("50");
/*  674 */     transactionAdviceDetailBean.setAdditionalAmount3("20000");
/*  675 */     transactionAdviceDetailBean.setAdditionalAmountSign3("+");
/*  676 */     transactionAdviceDetailBean.setAdditionalAmountType4("052");
/*  677 */     transactionAdviceDetailBean.setAdditionalAmount4("500");
/*  678 */     transactionAdviceDetailBean.setAdditionalAmountSign4("+");
/*  679 */     transactionAdviceDetailBean.setAdditionalAmountType5("48");
/*  680 */     transactionAdviceDetailBean.setAdditionalAmount5("500");
/*  681 */     transactionAdviceDetailBean.setAdditionalAmountSign5("+");
/*      */     
/*  683 */     transactionAdviceBasicBean.setTransactionAdviceDetailBean(transactionAdviceDetailBean);
/*      */     
/*      */ 
/*  686 */     LocationDetailTAABean locationDetailTAABean = new LocationDetailTAABean();
/*      */     
/*      */ 
/*  689 */     locationDetailTAABean.setRecordType("TAA");
/*  690 */     locationDetailTAABean.setRecordNumber("00000005");
/*  691 */     locationDetailTAABean.setLocationName("XYZ Insurance");
/*  692 */     locationDetailTAABean.setLocationAddress("XYZ Insurance Tokyo");
/*  693 */     locationDetailTAABean.setLocationCity("Tokyo");
/*  694 */     locationDetailTAABean.setLocationRegion("CA");
/*  695 */     locationDetailTAABean.setLocationCountryCode("392");
/*  696 */     locationDetailTAABean.setLocationPostalCode("Tokyo");
/*  697 */     locationDetailTAABean.setTransactionIdentifier("373872186011087");
/*  698 */     locationDetailTAABean.setAddendaTypeCode("99");
/*  699 */     locationDetailTAABean.setMerchantCategoryCode("6300");
/*  700 */     locationDetailTAABean.setSellerId("SELLR123456789012345");
/*      */     
/*  702 */     transactionAdviceBasicBean.setLocationDetailTAABean(locationDetailTAABean);
/*      */     
/*  704 */     InsuranceIndustryTAABean insBean = new InsuranceIndustryTAABean();
/*      */     
/*  706 */     insBean.setRecordType("TAA");
/*  707 */     insBean.setRecordNumber("04");
/*  708 */     insBean.setTransactionIdentifier("373872186011087");
/*  709 */     insBean.setFormatCode("04");
/*  710 */     insBean.setAddendaTypeCode("03");
/*  711 */     insBean.setInsurancePolicyNumber("1245325123");
/*  712 */     insBean.setInsuranceCoverageStartDate("21110204");
/*  713 */     insBean.setInsuranceCoverageEndDate("21110204");
/*  714 */     insBean.setInsurancePolicyPremiumFrequency("MONTHLY");
/*  715 */     insBean.setAdditionalInsurancePolicyNumber("");
/*  716 */     insBean.setNameOfInsured("ANY NAME");
/*  717 */     insBean.setTypeOfPolicy("TERM LIFE");
/*      */     
/*  719 */     transactionAdviceBasicBean.setTransactionAdviceAddendumBean(insBean);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  726 */     TransactionBatchTrailerBean transactionBatchTrailerBean = new TransactionBatchTrailerBean();
/*      */     
/*  728 */     transactionBatchTrailerBean.setMerchantId("5021045083");
/*  729 */     transactionBatchTrailerBean.setRecordNumber("00000006");
/*  730 */     transactionBatchTrailerBean.setRecordType("TBT");
/*  731 */     transactionBatchTrailerBean.setTbtCurrencyCode("392");
/*  732 */     transactionBatchTrailerBean.setTbtAmount("4905");
/*  733 */     transactionBatchTrailerBean.setTbtAmountSign("+");
/*  734 */     transactionBatchTrailerBean.setTbtCreationDate("21110402");
/*  735 */     transactionBatchTrailerBean.setTbtIdentificationNumber("1000000000000002");
/*  736 */     transactionBatchTrailerBean.setTbtImageSequenceNumber("654321");
/*  737 */     transactionBatchTrailerBean.setTotalNoOfTabs("1");
/*      */     
/*      */ 
/*  740 */     List<TransactionAdviceBasicBean> transactionAdviceBasicType = new ArrayList();
/*      */     
/*  742 */     transactionAdviceBasicType.add(transactionAdviceBasicBean);
/*      */     
/*  744 */     TransactionFileSummaryBean transactionFileSummaryBean = new TransactionFileSummaryBean();
/*      */     
/*  746 */     transactionFileSummaryBean.setRecordType("TFS");
/*  747 */     transactionFileSummaryBean.setRecordNumber("00000007");
/*  748 */     transactionFileSummaryBean.setNumberOfDebits("1");
/*  749 */     transactionFileSummaryBean.setHashTotalDebitAmount("500");
/*  750 */     transactionFileSummaryBean.setNumberOfCredits("0");
/*  751 */     transactionFileSummaryBean.setHashTotalCreditAmount("0");
/*  752 */     transactionFileSummaryBean.setHashTotalAmount("500");
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  758 */     TransactionTBTSpecificBean transactionTBTSpecificBean = new TransactionTBTSpecificBean();
/*      */     
/*  760 */     transactionTBTSpecificBean.setTransactionAdviceBasicType(transactionAdviceBasicType);
/*  761 */     transactionTBTSpecificBean.setTransactionBatchTrailerBean(transactionBatchTrailerBean);
/*      */     
/*  763 */     List<TransactionTBTSpecificBean> transactionTBTSpecificType = new ArrayList();
/*      */     
/*  765 */     transactionTBTSpecificType.add(transactionTBTSpecificBean);
/*      */     
/*      */ 
/*      */ 
/*  769 */     SettlementRequestDataObject settlementRequestDataObject = new SettlementRequestDataObject();
/*      */     
/*  771 */     settlementRequestDataObject
/*  772 */       .setTransactionFileHeaderType(transactionFileHeaderBean);
/*  773 */     settlementRequestDataObject
/*  774 */       .setTransactionFileSummaryType(transactionFileSummaryBean);
/*  775 */     settlementRequestDataObject
/*  776 */       .setTransactionTBTSpecificType(transactionTBTSpecificType);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  782 */     PropertyReader propertyReader = new PropertyReader();
/*      */     
/*      */ 
/*  785 */     propertyReader = propertyReader.loadPropertyFile("/settlement.properties");
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*  790 */     SettlementResponseDataObject settlementResponseDataObject = RequestProcessorFactory.processRequest(settlementRequestDataObject, propertyReader);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */     try
/*      */     {
/*  801 */       if (!settlementResponseDataObject.getActionCodeDescription().isEmpty())
/*      */       {
/*  803 */         Iterator<?> iterater = settlementResponseDataObject.getActionCodeDescription().iterator();
/*  804 */         while (iterater.hasNext()) {
/*  805 */           ErrorObject errorObject = (ErrorObject)iterater.next();
/*  806 */           System.out.print(errorObject.getErrorCode() + "-" + errorObject.getErrorDescription() + "\n");
/*      */         }
/*      */       } else {
/*  809 */         System.out.print("Error List is empty\n");
/*      */       }
/*      */       
/*      */ 
/*      */     }
/*      */     catch (Exception e)
/*      */     {
/*      */ 
/*  817 */       System.out.println("Error Object is Empty");
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void autoRentalTest()
/*      */     throws Exception
/*      */   {
/*  829 */     SimpleDateFormat datefmt = new SimpleDateFormat("MMdd");
/*      */     
/*  831 */     SimpleDateFormat timefmt = new SimpleDateFormat("hhmm");
/*  832 */     StringBuffer ab = new StringBuffer();
/*  833 */     ab.append(datefmt.format(Calendar.getInstance().getTime()));
/*  834 */     ab.append(timefmt.format(Calendar.getInstance().getTime()));
/*      */     
/*      */ 
/*  837 */     TransactionFileHeaderBean transactionFileHeaderBean = new TransactionFileHeaderBean();
/*      */     
/*  839 */     transactionFileHeaderBean.setRecordType("TFH");
/*  840 */     transactionFileHeaderBean.setRecordNumber("00000001");
/*  841 */     transactionFileHeaderBean.setSubmitterId("GWS002");
/*      */     
/*  843 */     transactionFileHeaderBean.setSubmitterFileReferenceNumber("1" + ab.toString());
/*      */     
/*  845 */     transactionFileHeaderBean.setFileCreationDate("20110523");
/*  846 */     transactionFileHeaderBean.setFileCreationTime("172020");
/*  847 */     transactionFileHeaderBean.setFileVersionNumber("12010000");
/*      */     
/*      */ 
/*  850 */     TransactionAdviceBasicBean transactionAdviceBasicBean = new TransactionAdviceBasicBean();
/*      */     
/*      */ 
/*      */ 
/*  854 */     transactionAdviceBasicBean.setRecordNumber("00000002");
/*  855 */     transactionAdviceBasicBean.setRecordType("TAB");
/*  856 */     transactionAdviceBasicBean.setTransactionIdentifier("373872186011087");
/*  857 */     transactionAdviceBasicBean.setFormatCode("05");
/*  858 */     transactionAdviceBasicBean.setMediaCode("01");
/*  859 */     transactionAdviceBasicBean.setSubmissionMethod("05");
/*  860 */     transactionAdviceBasicBean.setApprovalCode("712389");
/*  861 */     transactionAdviceBasicBean.setPrimaryAccountNumber("373953192351004");
/*  862 */     transactionAdviceBasicBean.setCardExpiryDate("1405");
/*  863 */     transactionAdviceBasicBean.setTransactionDate("21110402");
/*  864 */     transactionAdviceBasicBean.setTransactionTime("143000");
/*  865 */     transactionAdviceBasicBean.setTransactionAmount("80200");
/*  866 */     transactionAdviceBasicBean.setProcessingCode("000000");
/*  867 */     transactionAdviceBasicBean.setTransactionCurrencyCode("124");
/*  868 */     transactionAdviceBasicBean.setExtendedPaymentData("01");
/*  869 */     transactionAdviceBasicBean.setMerchantId("5021011432");
/*  870 */     transactionAdviceBasicBean.setMerchantLocationId("Can123456789012");
/*  871 */     transactionAdviceBasicBean.setPosDataCode("161201554110");
/*  872 */     transactionAdviceBasicBean.setInvoiceReferenceNumber("AMEXExpress10000000000000000");
/*  873 */     transactionAdviceBasicBean.setElectronicCommerceIndicator("05");
/*  874 */     transactionAdviceBasicBean.setTerminalId("Ter12345");
/*  875 */     transactionAdviceBasicBean.setTabImageSequenceNumber("Tab12345");
/*      */     
/*      */ 
/*      */ 
/*  879 */     TransactionAdviceDetailBean transactionAdviceDetailBean = new TransactionAdviceDetailBean();
/*      */     
/*  881 */     transactionAdviceDetailBean.setRecordType("TAD");
/*  882 */     transactionAdviceDetailBean.setRecordNumber("00000003");
/*  883 */     transactionAdviceDetailBean.setTransactionIdentifier("373872186011087");
/*  884 */     transactionAdviceDetailBean.setAdditionalAmountType1("036");
/*  885 */     transactionAdviceDetailBean.setAdditionalAmount1("10200");
/*  886 */     transactionAdviceDetailBean.setAdditionalAmountSign1("+");
/*  887 */     transactionAdviceDetailBean.setAdditionalAmountType2("31");
/*  888 */     transactionAdviceDetailBean.setAdditionalAmount2("40000");
/*  889 */     transactionAdviceDetailBean.setAdditionalAmountSign2("+");
/*  890 */     transactionAdviceDetailBean.setAdditionalAmountType3("50");
/*  891 */     transactionAdviceDetailBean.setAdditionalAmount3("20000");
/*  892 */     transactionAdviceDetailBean.setAdditionalAmountSign3("+");
/*  893 */     transactionAdviceDetailBean.setAdditionalAmountType4("052");
/*  894 */     transactionAdviceDetailBean.setAdditionalAmount4("500");
/*  895 */     transactionAdviceDetailBean.setAdditionalAmountSign4("+");
/*  896 */     transactionAdviceDetailBean.setAdditionalAmountType5("48");
/*  897 */     transactionAdviceDetailBean.setAdditionalAmount5("500");
/*  898 */     transactionAdviceDetailBean.setAdditionalAmountSign5("+");
/*      */     
/*  900 */     transactionAdviceBasicBean.setTransactionAdviceDetailBean(transactionAdviceDetailBean);
/*      */     
/*      */ 
/*  903 */     LocationDetailTAABean locationDetailTAABean = new LocationDetailTAABean();
/*      */     
/*      */ 
/*  906 */     locationDetailTAABean.setRecordType("TAA");
/*  907 */     locationDetailTAABean.setRecordNumber("00000005");
/*  908 */     locationDetailTAABean.setLocationName("ABC Auto Ltd ");
/*  909 */     locationDetailTAABean.setLocationAddress("XYZ Insurance Tokyo");
/*  910 */     locationDetailTAABean.setLocationCity("Tokyo");
/*  911 */     locationDetailTAABean.setLocationRegion("13");
/*  912 */     locationDetailTAABean.setLocationCountryCode("124");
/*  913 */     locationDetailTAABean.setLocationPostalCode("Tokyo");
/*  914 */     locationDetailTAABean.setTransactionIdentifier("373872186011087");
/*  915 */     locationDetailTAABean.setAddendaTypeCode("99");
/*  916 */     locationDetailTAABean.setMerchantCategoryCode("5531");
/*  917 */     locationDetailTAABean.setSellerId("SELLR123456789012345");
/*      */     
/*  919 */     transactionAdviceBasicBean.setLocationDetailTAABean(locationDetailTAABean);
/*      */     
/*  921 */     AutoRentalIndustryTAABean autoBean = new AutoRentalIndustryTAABean();
/*      */     
/*  923 */     autoBean.setRecordType("TAA");
/*  924 */     autoBean.setRecordNumber("0000004");
/*  925 */     autoBean.setFormatCode("05");
/*  926 */     autoBean.setAddendaTypeCode("07");
/*  927 */     autoBean.setTransactionIdentifier("373872186011087");
/*  928 */     autoBean.setAutoRentalAgreementNumber("Auto1234567890");
/*  929 */     autoBean.setAutoRentalPickupLocation("New Brunswick Caneda");
/*  930 */     autoBean.setAutoRentalPickupCityName("New Brunswick Caneda");
/*  931 */     autoBean.setAutoRentalPickupRegionCode("NS");
/*  932 */     autoBean.setAutoRentalPickupCountryCode("124");
/*  933 */     autoBean.setAutoRentalPickupTime("21110402");
/*  934 */     autoBean.setAutoRentalPickupDate("110414");
/*  935 */     autoBean.setAutoRentalReturnCityName("143000");
/*  936 */     autoBean.setAutoRentalReturnRegionCode("NS");
/*  937 */     autoBean.setAutoRentalReturnCountryCode("124");
/*  938 */     autoBean.setAutoRentalReturnDate("21110402");
/*  939 */     autoBean.setAutoRentalReturnTime("146000");
/*  940 */     autoBean.setAutoRentalRenterName("ABC Auto Ltd");
/*  941 */     autoBean.setAutoRentalVehicleClassId("1");
/*  942 */     autoBean.setAutoRentalDistance("145");
/*  943 */     autoBean.setAutoRentalDistanceUnitOfMeasure("K");
/*  944 */     autoBean.setAutoRentalAuditAdjustmentIndicator("Y");
/*  945 */     autoBean.setAutoRentalAuditAdjustmentAmount("1000");
/*      */     
/*  947 */     transactionAdviceBasicBean.setTransactionAdviceAddendumBean(autoBean);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  955 */     TransactionBatchTrailerBean transactionBatchTrailerBean = new TransactionBatchTrailerBean();
/*      */     
/*  957 */     transactionBatchTrailerBean.setMerchantId("5021011432");
/*  958 */     transactionBatchTrailerBean.setRecordNumber("00000006");
/*  959 */     transactionBatchTrailerBean.setRecordType("TBT");
/*  960 */     transactionBatchTrailerBean.setTbtCurrencyCode("124");
/*  961 */     transactionBatchTrailerBean.setTbtAmount("80200");
/*  962 */     transactionBatchTrailerBean.setTbtAmountSign("+");
/*  963 */     transactionBatchTrailerBean.setTbtCreationDate("21110402");
/*  964 */     transactionBatchTrailerBean.setTbtIdentificationNumber("1000000000000003");
/*  965 */     transactionBatchTrailerBean.setTbtImageSequenceNumber("654321");
/*  966 */     transactionBatchTrailerBean.setTotalNoOfTabs("1");
/*      */     
/*      */ 
/*  969 */     List<TransactionAdviceBasicBean> transactionAdviceBasicType = new ArrayList();
/*      */     
/*  971 */     transactionAdviceBasicType.add(transactionAdviceBasicBean);
/*      */     
/*  973 */     TransactionFileSummaryBean transactionFileSummaryBean = new TransactionFileSummaryBean();
/*      */     
/*  975 */     transactionFileSummaryBean.setRecordType("TFS");
/*  976 */     transactionFileSummaryBean.setRecordNumber("00000007");
/*  977 */     transactionFileSummaryBean.setNumberOfDebits("1");
/*  978 */     transactionFileSummaryBean.setHashTotalDebitAmount("500");
/*  979 */     transactionFileSummaryBean.setNumberOfCredits("0");
/*  980 */     transactionFileSummaryBean.setHashTotalCreditAmount("0");
/*  981 */     transactionFileSummaryBean.setHashTotalAmount("500");
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  987 */     TransactionTBTSpecificBean transactionTBTSpecificBean = new TransactionTBTSpecificBean();
/*      */     
/*  989 */     transactionTBTSpecificBean.setTransactionAdviceBasicType(transactionAdviceBasicType);
/*  990 */     transactionTBTSpecificBean.setTransactionBatchTrailerBean(transactionBatchTrailerBean);
/*      */     
/*  992 */     List<TransactionTBTSpecificBean> transactionTBTSpecificType = new ArrayList();
/*      */     
/*  994 */     transactionTBTSpecificType.add(transactionTBTSpecificBean);
/*      */     
/*      */ 
/*      */ 
/*  998 */     SettlementRequestDataObject settlementRequestDataObject = new SettlementRequestDataObject();
/*      */     
/* 1000 */     settlementRequestDataObject
/* 1001 */       .setTransactionFileHeaderType(transactionFileHeaderBean);
/* 1002 */     settlementRequestDataObject
/* 1003 */       .setTransactionFileSummaryType(transactionFileSummaryBean);
/* 1004 */     settlementRequestDataObject
/* 1005 */       .setTransactionTBTSpecificType(transactionTBTSpecificType);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1011 */     PropertyReader propertyReader = new PropertyReader();
/*      */     
/*      */ 
/* 1014 */     propertyReader = propertyReader.loadPropertyFile("/settlement.properties");
/*      */     
/*      */ 
/*      */ 
/*      */ 
/* 1019 */     SettlementResponseDataObject settlementResponseDataObject = RequestProcessorFactory.processRequest(settlementRequestDataObject, propertyReader);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */     try
/*      */     {
/* 1030 */       if (!settlementResponseDataObject.getActionCodeDescription().isEmpty())
/*      */       {
/* 1032 */         Iterator<?> iterater = settlementResponseDataObject.getActionCodeDescription().iterator();
/* 1033 */         while (iterater.hasNext()) {
/* 1034 */           ErrorObject errorObject = (ErrorObject)iterater.next();
/* 1035 */           System.out.print(errorObject.getErrorCode() + "-" + errorObject.getErrorDescription() + "\n");
/*      */         }
/*      */       } else {
/* 1038 */         System.out.print("Error List is empty\n");
/*      */       }
/*      */       
/*      */ 
/*      */     }
/*      */     catch (Exception e)
/*      */     {
/*      */ 
/* 1046 */       System.out.println("Error Object is Empty");
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void railIndTest()
/*      */     throws Exception
/*      */   {
/* 1059 */     SimpleDateFormat datefmt = new SimpleDateFormat("MMdd");
/*      */     
/* 1061 */     SimpleDateFormat timefmt = new SimpleDateFormat("hhmm");
/* 1062 */     StringBuffer ab = new StringBuffer();
/* 1063 */     ab.append(datefmt.format(Calendar.getInstance().getTime()));
/* 1064 */     ab.append(timefmt.format(Calendar.getInstance().getTime()));
/*      */     
/*      */ 
/* 1067 */     TransactionFileHeaderBean transactionFileHeaderBean = new TransactionFileHeaderBean();
/*      */     
/* 1069 */     transactionFileHeaderBean.setRecordType("TFH");
/* 1070 */     transactionFileHeaderBean.setRecordNumber("00000001");
/* 1071 */     transactionFileHeaderBean.setSubmitterId("GWS002");
/*      */     
/* 1073 */     transactionFileHeaderBean.setSubmitterFileReferenceNumber("1" + ab.toString());
/*      */     
/* 1075 */     transactionFileHeaderBean.setFileCreationDate("20110523");
/* 1076 */     transactionFileHeaderBean.setFileCreationTime("172020");
/* 1077 */     transactionFileHeaderBean.setFileVersionNumber("12010000");
/*      */     
/*      */ 
/* 1080 */     TransactionAdviceBasicBean transactionAdviceBasicBean = new TransactionAdviceBasicBean();
/*      */     
/*      */ 
/*      */ 
/* 1084 */     transactionAdviceBasicBean.setRecordNumber("00000002");
/* 1085 */     transactionAdviceBasicBean.setRecordType("TAB");
/* 1086 */     transactionAdviceBasicBean.setTransactionIdentifier("373872186011087");
/* 1087 */     transactionAdviceBasicBean.setFormatCode("06");
/* 1088 */     transactionAdviceBasicBean.setMediaCode("01");
/* 1089 */     transactionAdviceBasicBean.setSubmissionMethod("05");
/* 1090 */     transactionAdviceBasicBean.setApprovalCode("712389");
/* 1091 */     transactionAdviceBasicBean.setPrimaryAccountNumber("341111599241000");
/* 1092 */     transactionAdviceBasicBean.setCardExpiryDate("1405");
/* 1093 */     transactionAdviceBasicBean.setTransactionDate("21110402");
/* 1094 */     transactionAdviceBasicBean.setTransactionTime("143000");
/* 1095 */     transactionAdviceBasicBean.setTransactionAmount("1000");
/* 1096 */     transactionAdviceBasicBean.setProcessingCode("000000");
/* 1097 */     transactionAdviceBasicBean.setTransactionCurrencyCode("978");
/* 1098 */     transactionAdviceBasicBean.setExtendedPaymentData("01");
/* 1099 */     transactionAdviceBasicBean.setMerchantId("5021045083");
/* 1100 */     transactionAdviceBasicBean.setMerchantLocationId("Eur123456789012");
/* 1101 */     transactionAdviceBasicBean.setPosDataCode("161201554110");
/* 1102 */     transactionAdviceBasicBean.setInvoiceReferenceNumber("InvoiceNo123456");
/* 1103 */     transactionAdviceBasicBean.setElectronicCommerceIndicator("");
/* 1104 */     transactionAdviceBasicBean.setTerminalId("Ter12345");
/* 1105 */     transactionAdviceBasicBean.setTabImageSequenceNumber(" ");
/*      */     
/*      */ 
/*      */ 
/* 1109 */     TransactionAdviceDetailBean transactionAdviceDetailBean = new TransactionAdviceDetailBean();
/*      */     
/* 1111 */     transactionAdviceDetailBean.setRecordType("TAD");
/* 1112 */     transactionAdviceDetailBean.setRecordNumber("00000003");
/* 1113 */     transactionAdviceDetailBean.setTransactionIdentifier("373872186011087");
/* 1114 */     transactionAdviceDetailBean.setAdditionalAmountType1("61");
/* 1115 */     transactionAdviceDetailBean.setAdditionalAmount1("200");
/* 1116 */     transactionAdviceDetailBean.setAdditionalAmountSign1("+");
/* 1117 */     transactionAdviceDetailBean.setAdditionalAmountType2("34");
/* 1118 */     transactionAdviceDetailBean.setAdditionalAmount2("200");
/* 1119 */     transactionAdviceDetailBean.setAdditionalAmountSign2("+");
/* 1120 */     transactionAdviceDetailBean.setAdditionalAmountType3("50");
/* 1121 */     transactionAdviceDetailBean.setAdditionalAmount3("200");
/* 1122 */     transactionAdviceDetailBean.setAdditionalAmountSign3("+");
/* 1123 */     transactionAdviceDetailBean.setAdditionalAmountType4("052");
/* 1124 */     transactionAdviceDetailBean.setAdditionalAmount4("200");
/* 1125 */     transactionAdviceDetailBean.setAdditionalAmountSign4("+");
/* 1126 */     transactionAdviceDetailBean.setAdditionalAmountType5("49");
/* 1127 */     transactionAdviceDetailBean.setAdditionalAmount5("200");
/* 1128 */     transactionAdviceDetailBean.setAdditionalAmountSign5("+");
/*      */     
/* 1130 */     transactionAdviceBasicBean.setTransactionAdviceDetailBean(transactionAdviceDetailBean);
/*      */     
/*      */ 
/* 1133 */     LocationDetailTAABean locationDetailTAABean = new LocationDetailTAABean();
/*      */     
/*      */ 
/* 1136 */     locationDetailTAABean.setRecordType("TAA");
/* 1137 */     locationDetailTAABean.setRecordNumber("00000005");
/* 1138 */     locationDetailTAABean.setLocationName("ABC Auto Ltd ");
/* 1139 */     locationDetailTAABean.setLocationAddress("XYZ Insurance Tokyo");
/* 1140 */     locationDetailTAABean.setLocationCity("Tokyo");
/* 1141 */     locationDetailTAABean.setLocationRegion("13");
/* 1142 */     locationDetailTAABean.setLocationCountryCode("124");
/* 1143 */     locationDetailTAABean.setLocationPostalCode("Tokyo");
/* 1144 */     locationDetailTAABean.setTransactionIdentifier("373872186011087");
/* 1145 */     locationDetailTAABean.setAddendaTypeCode("99");
/* 1146 */     locationDetailTAABean.setMerchantCategoryCode("4112");
/* 1147 */     locationDetailTAABean.setSellerId("SELLR123456789012345");
/*      */     
/* 1149 */     transactionAdviceBasicBean.setLocationDetailTAABean(locationDetailTAABean);
/*      */     
/* 1151 */     RailIndustryTAABean raiBean = new RailIndustryTAABean();
/*      */     
/* 1153 */     raiBean.setRecordType("TAA");
/* 1154 */     raiBean.setRecordNumber("004");
/* 1155 */     raiBean.setTransactionIdentifier("373872186011087");
/* 1156 */     raiBean.setFormatCode("06");
/* 1157 */     raiBean.setAddendaTypeCode("03");
/* 1158 */     raiBean.setTransactionType("EXC");
/* 1159 */     raiBean.setTicketNumber("TKT012345678912");
/* 1160 */     raiBean.setPassengerName("Ram Aur Shyam");
/* 1161 */     raiBean.setIataCarrierCode(" ");
/* 1162 */     raiBean.setTicketIssueCity("Berlin");
/* 1163 */     raiBean.setTicketIssuerName("Sai Agency");
/*      */     
/* 1165 */     raiBean.setDepartureStation1("Berlin");
/* 1166 */     raiBean.setDepartureDate1("20080216");
/* 1167 */     raiBean.setArrivalStation1("Chisinau");
/*      */     
/* 1169 */     raiBean.setDepartureStation2("London");
/* 1170 */     raiBean.setDepartureDate2("20080218");
/* 1171 */     raiBean.setArrivalStation2("Minsk");
/*      */     
/* 1173 */     raiBean.setDepartureStation3("Monaco");
/* 1174 */     raiBean.setDepartureDate3("20080220");
/* 1175 */     raiBean.setArrivalStation3("Paris");
/*      */     
/* 1177 */     raiBean.setDepartureStation4("Riga");
/* 1178 */     raiBean.setDepartureDate4("20080222");
/* 1179 */     raiBean.setArrivalStation4("Stockholm");
/*      */     
/*      */ 
/* 1182 */     transactionAdviceBasicBean.setTransactionAdviceAddendumBean(raiBean);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1189 */     TransactionBatchTrailerBean transactionBatchTrailerBean = new TransactionBatchTrailerBean();
/*      */     
/* 1191 */     transactionBatchTrailerBean.setMerchantId("5021045083");
/* 1192 */     transactionBatchTrailerBean.setRecordNumber("00000006");
/* 1193 */     transactionBatchTrailerBean.setRecordType("TBT");
/* 1194 */     transactionBatchTrailerBean.setTbtCurrencyCode("978");
/* 1195 */     transactionBatchTrailerBean.setTbtAmount("1000");
/* 1196 */     transactionBatchTrailerBean.setTbtAmountSign("+");
/* 1197 */     transactionBatchTrailerBean.setTbtCreationDate("21110402");
/* 1198 */     transactionBatchTrailerBean.setTbtIdentificationNumber("1000000000000004");
/* 1199 */     transactionBatchTrailerBean.setTbtImageSequenceNumber(" ");
/* 1200 */     transactionBatchTrailerBean.setTotalNoOfTabs("1");
/*      */     
/*      */ 
/* 1203 */     List<TransactionAdviceBasicBean> transactionAdviceBasicType = new ArrayList();
/*      */     
/* 1205 */     transactionAdviceBasicType.add(transactionAdviceBasicBean);
/*      */     
/* 1207 */     TransactionFileSummaryBean transactionFileSummaryBean = new TransactionFileSummaryBean();
/*      */     
/* 1209 */     transactionFileSummaryBean.setRecordType("TFS");
/* 1210 */     transactionFileSummaryBean.setRecordNumber("00000007");
/* 1211 */     transactionFileSummaryBean.setNumberOfDebits("1");
/* 1212 */     transactionFileSummaryBean.setHashTotalDebitAmount("500");
/* 1213 */     transactionFileSummaryBean.setNumberOfCredits("0");
/* 1214 */     transactionFileSummaryBean.setHashTotalCreditAmount("0");
/* 1215 */     transactionFileSummaryBean.setHashTotalAmount("500");
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1221 */     TransactionTBTSpecificBean transactionTBTSpecificBean = new TransactionTBTSpecificBean();
/*      */     
/* 1223 */     transactionTBTSpecificBean.setTransactionAdviceBasicType(transactionAdviceBasicType);
/* 1224 */     transactionTBTSpecificBean.setTransactionBatchTrailerBean(transactionBatchTrailerBean);
/*      */     
/* 1226 */     List<TransactionTBTSpecificBean> transactionTBTSpecificType = new ArrayList();
/*      */     
/* 1228 */     transactionTBTSpecificType.add(transactionTBTSpecificBean);
/*      */     
/*      */ 
/*      */ 
/* 1232 */     SettlementRequestDataObject settlementRequestDataObject = new SettlementRequestDataObject();
/*      */     
/* 1234 */     settlementRequestDataObject
/* 1235 */       .setTransactionFileHeaderType(transactionFileHeaderBean);
/* 1236 */     settlementRequestDataObject
/* 1237 */       .setTransactionFileSummaryType(transactionFileSummaryBean);
/* 1238 */     settlementRequestDataObject
/* 1239 */       .setTransactionTBTSpecificType(transactionTBTSpecificType);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1245 */     PropertyReader propertyReader = new PropertyReader();
/*      */     
/*      */ 
/* 1248 */     propertyReader = propertyReader.loadPropertyFile("/settlement.properties");
/*      */     
/*      */ 
/*      */ 
/*      */ 
/* 1253 */     SettlementResponseDataObject settlementResponseDataObject = RequestProcessorFactory.processRequest(settlementRequestDataObject, propertyReader);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */     try
/*      */     {
/* 1264 */       if (!settlementResponseDataObject.getActionCodeDescription().isEmpty())
/*      */       {
/* 1266 */         Iterator<?> iterater = settlementResponseDataObject.getActionCodeDescription().iterator();
/* 1267 */         while (iterater.hasNext()) {
/* 1268 */           ErrorObject errorObject = (ErrorObject)iterater.next();
/* 1269 */           System.out.print(errorObject.getErrorCode() + "-" + errorObject.getErrorDescription() + "\n");
/*      */         }
/*      */       } else {
/* 1272 */         System.out.print("Error List is empty\n");
/*      */       }
/*      */       
/*      */ 
/*      */     }
/*      */     catch (Exception e)
/*      */     {
/*      */ 
/* 1280 */       System.out.println("Error Object is Empty");
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void comAndServicesTest()
/*      */     throws Exception
/*      */   {
/* 1292 */     SimpleDateFormat datefmt = new SimpleDateFormat("MMdd");
/*      */     
/* 1294 */     SimpleDateFormat timefmt = new SimpleDateFormat("hhmm");
/* 1295 */     StringBuffer ab = new StringBuffer();
/* 1296 */     ab.append(datefmt.format(Calendar.getInstance().getTime()));
/* 1297 */     ab.append(timefmt.format(Calendar.getInstance().getTime()));
/*      */     
/*      */ 
/* 1300 */     TransactionFileHeaderBean transactionFileHeaderBean = new TransactionFileHeaderBean();
/*      */     
/* 1302 */     transactionFileHeaderBean.setRecordType("TFH");
/* 1303 */     transactionFileHeaderBean.setRecordNumber("00000001");
/* 1304 */     transactionFileHeaderBean.setSubmitterId("GWS002");
/*      */     
/* 1306 */     transactionFileHeaderBean.setSubmitterFileReferenceNumber("1" + ab.toString());
/*      */     
/* 1308 */     transactionFileHeaderBean.setFileCreationDate("20110523");
/* 1309 */     transactionFileHeaderBean.setFileCreationTime("172020");
/* 1310 */     transactionFileHeaderBean.setFileVersionNumber("12010000");
/*      */     
/*      */ 
/* 1313 */     TransactionAdviceBasicBean transactionAdviceBasicBean = new TransactionAdviceBasicBean();
/*      */     
/*      */ 
/*      */ 
/* 1317 */     transactionAdviceBasicBean.setRecordNumber("00000002");
/* 1318 */     transactionAdviceBasicBean.setRecordType("TAB");
/* 1319 */     transactionAdviceBasicBean.setTransactionIdentifier("373872186011087");
/* 1320 */     transactionAdviceBasicBean.setFormatCode("13");
/* 1321 */     transactionAdviceBasicBean.setMediaCode("01");
/* 1322 */     transactionAdviceBasicBean.setSubmissionMethod("05");
/* 1323 */     transactionAdviceBasicBean.setApprovalCode("712389");
/* 1324 */     transactionAdviceBasicBean.setPrimaryAccountNumber("373953192351004");
/* 1325 */     transactionAdviceBasicBean.setCardExpiryDate("1405");
/* 1326 */     transactionAdviceBasicBean.setTransactionDate("21110402");
/* 1327 */     transactionAdviceBasicBean.setTransactionTime("143000");
/* 1328 */     transactionAdviceBasicBean.setTransactionAmount("87200");
/* 1329 */     transactionAdviceBasicBean.setProcessingCode("000000");
/* 1330 */     transactionAdviceBasicBean.setTransactionCurrencyCode("840");
/* 1331 */     transactionAdviceBasicBean.setExtendedPaymentData("01");
/* 1332 */     transactionAdviceBasicBean.setMerchantId("5021045083");
/* 1333 */     transactionAdviceBasicBean.setMerchantLocationId("USA123456789012");
/* 1334 */     transactionAdviceBasicBean.setPosDataCode("161201554110");
/* 1335 */     transactionAdviceBasicBean.setInvoiceReferenceNumber("InvoiceNo123456");
/* 1336 */     transactionAdviceBasicBean.setElectronicCommerceIndicator("");
/* 1337 */     transactionAdviceBasicBean.setTerminalId("Ter12345");
/* 1338 */     transactionAdviceBasicBean.setTabImageSequenceNumber(" ");
/*      */     
/*      */ 
/*      */ 
/* 1342 */     TransactionAdviceDetailBean transactionAdviceDetailBean = new TransactionAdviceDetailBean();
/*      */     
/* 1344 */     transactionAdviceDetailBean.setRecordType("TAD");
/* 1345 */     transactionAdviceDetailBean.setRecordNumber("00000003");
/* 1346 */     transactionAdviceDetailBean.setTransactionIdentifier("373872186011087");
/* 1347 */     transactionAdviceDetailBean.setAdditionalAmountType1("52");
/* 1348 */     transactionAdviceDetailBean.setAdditionalAmount1("7200");
/* 1349 */     transactionAdviceDetailBean.setAdditionalAmountSign1("+");
/* 1350 */     transactionAdviceDetailBean.setAdditionalAmountType2("34");
/* 1351 */     transactionAdviceDetailBean.setAdditionalAmount2("2000");
/* 1352 */     transactionAdviceDetailBean.setAdditionalAmountSign2("+");
/* 1353 */     transactionAdviceDetailBean.setAdditionalAmountType3("50");
/* 1354 */     transactionAdviceDetailBean.setAdditionalAmount3("2000");
/* 1355 */     transactionAdviceDetailBean.setAdditionalAmountSign3("+");
/* 1356 */     transactionAdviceDetailBean.setAdditionalAmountType4("052");
/* 1357 */     transactionAdviceDetailBean.setAdditionalAmount4("2000");
/* 1358 */     transactionAdviceDetailBean.setAdditionalAmountSign4("+");
/* 1359 */     transactionAdviceDetailBean.setAdditionalAmountType5("49");
/* 1360 */     transactionAdviceDetailBean.setAdditionalAmount5("2000");
/* 1361 */     transactionAdviceDetailBean.setAdditionalAmountSign5("+");
/*      */     
/* 1363 */     transactionAdviceBasicBean.setTransactionAdviceDetailBean(transactionAdviceDetailBean);
/*      */     
/*      */ 
/* 1366 */     LocationDetailTAABean locationDetailTAABean = new LocationDetailTAABean();
/*      */     
/*      */ 
/* 1369 */     locationDetailTAABean.setRecordType("TAA");
/* 1370 */     locationDetailTAABean.setRecordNumber("00000005");
/* 1371 */     locationDetailTAABean.setLocationName("Empire CA ");
/* 1372 */     locationDetailTAABean.setLocationAddress("Empire CA");
/* 1373 */     locationDetailTAABean.setLocationCity("Empire CA");
/* 1374 */     locationDetailTAABean.setLocationRegion("13");
/* 1375 */     locationDetailTAABean.setLocationCountryCode("840");
/* 1376 */     locationDetailTAABean.setLocationPostalCode("Postalcode123");
/* 1377 */     locationDetailTAABean.setTransactionIdentifier("373872186011087");
/* 1378 */     locationDetailTAABean.setAddendaTypeCode("99");
/* 1379 */     locationDetailTAABean.setMerchantCategoryCode("4112");
/* 1380 */     locationDetailTAABean.setSellerId("SELLR123456789012345");
/*      */     
/* 1382 */     transactionAdviceBasicBean.setLocationDetailTAABean(locationDetailTAABean);
/*      */     
/* 1384 */     CommunicationServicesIndustryBean comBean = new CommunicationServicesIndustryBean();
/* 1385 */     comBean.setRecordType("TAA");
/* 1386 */     comBean.setRecordNumber("004");
/* 1387 */     comBean.setTransactionIdentifier("373872186011087");
/* 1388 */     comBean.setFormatCode("06");
/* 1389 */     comBean.setAddendaTypeCode("03");
/* 1390 */     comBean.setCallDate("20080216");
/* 1391 */     comBean.setCallTime("143000");
/* 1392 */     comBean.setCallDurationTime("120000");
/* 1393 */     comBean.setCallFromLocationName("Empire CA");
/* 1394 */     comBean.setCallFromRegionCode("CA");
/* 1395 */     comBean.setCallFromCountryCode("840");
/* 1396 */     comBean.setCallFromPhoneNumber("6025372121");
/* 1397 */     comBean.setCallToLocationName("Easton CA");
/* 1398 */     comBean.setCallToRegionCode("CA");
/* 1399 */     comBean.setCallToCountryCode("840");
/* 1400 */     comBean.setCallToPhoneNumber("6025272854");
/* 1401 */     comBean.setPhoneCardId("12345678");
/* 1402 */     comBean.setServiceDescription("dataandvoice");
/* 1403 */     comBean.setBillingPeriod("");
/* 1404 */     comBean.setCommunicationsCallTypeCode("Local");
/* 1405 */     comBean.setCommunicationsRateClass("D");
/*      */     
/* 1407 */     transactionAdviceBasicBean.setTransactionAdviceAddendumBean(comBean);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1413 */     TransactionBatchTrailerBean transactionBatchTrailerBean = new TransactionBatchTrailerBean();
/*      */     
/* 1415 */     transactionBatchTrailerBean.setMerchantId("5021045083");
/* 1416 */     transactionBatchTrailerBean.setRecordNumber("00000006");
/* 1417 */     transactionBatchTrailerBean.setRecordType("TBT");
/* 1418 */     transactionBatchTrailerBean.setTbtCurrencyCode("840");
/* 1419 */     transactionBatchTrailerBean.setTbtAmount("87200");
/* 1420 */     transactionBatchTrailerBean.setTbtAmountSign("+");
/* 1421 */     transactionBatchTrailerBean.setTbtCreationDate("21110402");
/* 1422 */     transactionBatchTrailerBean.setTbtIdentificationNumber("1000000000000006");
/* 1423 */     transactionBatchTrailerBean.setTbtImageSequenceNumber(" ");
/* 1424 */     transactionBatchTrailerBean.setTotalNoOfTabs("1");
/*      */     
/*      */ 
/* 1427 */     List<TransactionAdviceBasicBean> transactionAdviceBasicType = new ArrayList();
/*      */     
/* 1429 */     transactionAdviceBasicType.add(transactionAdviceBasicBean);
/*      */     
/* 1431 */     TransactionFileSummaryBean transactionFileSummaryBean = new TransactionFileSummaryBean();
/*      */     
/* 1433 */     transactionFileSummaryBean.setRecordType("TFS");
/* 1434 */     transactionFileSummaryBean.setRecordNumber("00000007");
/* 1435 */     transactionFileSummaryBean.setNumberOfDebits("1");
/* 1436 */     transactionFileSummaryBean.setHashTotalDebitAmount("500");
/* 1437 */     transactionFileSummaryBean.setNumberOfCredits("0");
/* 1438 */     transactionFileSummaryBean.setHashTotalCreditAmount("0");
/* 1439 */     transactionFileSummaryBean.setHashTotalAmount("500");
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1445 */     TransactionTBTSpecificBean transactionTBTSpecificBean = new TransactionTBTSpecificBean();
/*      */     
/* 1447 */     transactionTBTSpecificBean.setTransactionAdviceBasicType(transactionAdviceBasicType);
/* 1448 */     transactionTBTSpecificBean.setTransactionBatchTrailerBean(transactionBatchTrailerBean);
/*      */     
/* 1450 */     List<TransactionTBTSpecificBean> transactionTBTSpecificType = new ArrayList();
/*      */     
/* 1452 */     transactionTBTSpecificType.add(transactionTBTSpecificBean);
/*      */     
/*      */ 
/*      */ 
/* 1456 */     SettlementRequestDataObject settlementRequestDataObject = new SettlementRequestDataObject();
/*      */     
/* 1458 */     settlementRequestDataObject
/* 1459 */       .setTransactionFileHeaderType(transactionFileHeaderBean);
/* 1460 */     settlementRequestDataObject
/* 1461 */       .setTransactionFileSummaryType(transactionFileSummaryBean);
/* 1462 */     settlementRequestDataObject
/* 1463 */       .setTransactionTBTSpecificType(transactionTBTSpecificType);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1469 */     PropertyReader propertyReader = new PropertyReader();
/*      */     
/*      */ 
/* 1472 */     propertyReader = propertyReader.loadPropertyFile("/settlement.properties");
/*      */     
/*      */ 
/*      */ 
/*      */ 
/* 1477 */     SettlementResponseDataObject settlementResponseDataObject = RequestProcessorFactory.processRequest(settlementRequestDataObject, propertyReader);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */     try
/*      */     {
/* 1488 */       if (!settlementResponseDataObject.getActionCodeDescription().isEmpty())
/*      */       {
/* 1490 */         Iterator<?> iterater = settlementResponseDataObject.getActionCodeDescription().iterator();
/* 1491 */         while (iterater.hasNext()) {
/* 1492 */           ErrorObject errorObject = (ErrorObject)iterater.next();
/* 1493 */           System.out.print(errorObject.getErrorCode() + "-" + errorObject.getErrorDescription() + "\n");
/*      */         }
/*      */       } else {
/* 1496 */         System.out.print("Error List is empty\n");
/*      */       }
/*      */       
/*      */ 
/*      */     }
/*      */     catch (Exception e)
/*      */     {
/*      */ 
/* 1504 */       System.out.println("Error Object is Empty");
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void travelCruiseTest()
/*      */     throws Exception
/*      */   {
/* 1516 */     SimpleDateFormat datefmt = new SimpleDateFormat("MMdd");
/*      */     
/* 1518 */     SimpleDateFormat timefmt = new SimpleDateFormat("hhmm");
/* 1519 */     StringBuffer ab = new StringBuffer();
/* 1520 */     ab.append(datefmt.format(Calendar.getInstance().getTime()));
/* 1521 */     ab.append(timefmt.format(Calendar.getInstance().getTime()));
/*      */     
/*      */ 
/* 1524 */     TransactionFileHeaderBean transactionFileHeaderBean = new TransactionFileHeaderBean();
/*      */     
/* 1526 */     transactionFileHeaderBean.setRecordType("TFH");
/* 1527 */     transactionFileHeaderBean.setRecordNumber("00000001");
/* 1528 */     transactionFileHeaderBean.setSubmitterId("GWS002");
/*      */     
/* 1530 */     transactionFileHeaderBean.setSubmitterFileReferenceNumber("1" + ab.toString());
/*      */     
/* 1532 */     transactionFileHeaderBean.setFileCreationDate("20110523");
/* 1533 */     transactionFileHeaderBean.setFileCreationTime("172020");
/* 1534 */     transactionFileHeaderBean.setFileVersionNumber("12010000");
/*      */     
/*      */ 
/* 1537 */     TransactionAdviceBasicBean transactionAdviceBasicBean = new TransactionAdviceBasicBean();
/*      */     
/*      */ 
/*      */ 
/* 1541 */     transactionAdviceBasicBean.setRecordNumber("00000002");
/* 1542 */     transactionAdviceBasicBean.setRecordType("TAB");
/* 1543 */     transactionAdviceBasicBean.setTransactionIdentifier("373872186011087");
/* 1544 */     transactionAdviceBasicBean.setFormatCode("14");
/* 1545 */     transactionAdviceBasicBean.setMediaCode("01");
/* 1546 */     transactionAdviceBasicBean.setSubmissionMethod("");
/* 1547 */     transactionAdviceBasicBean.setApprovalCode("712389");
/* 1548 */     transactionAdviceBasicBean.setPrimaryAccountNumber("373953192351004");
/* 1549 */     transactionAdviceBasicBean.setCardExpiryDate("1405");
/* 1550 */     transactionAdviceBasicBean.setTransactionDate("21110402");
/* 1551 */     transactionAdviceBasicBean.setTransactionTime("143000");
/* 1552 */     transactionAdviceBasicBean.setTransactionAmount("35000");
/* 1553 */     transactionAdviceBasicBean.setProcessingCode("000000");
/* 1554 */     transactionAdviceBasicBean.setTransactionCurrencyCode("840");
/* 1555 */     transactionAdviceBasicBean.setExtendedPaymentData("01");
/* 1556 */     transactionAdviceBasicBean.setMerchantId("5021045083");
/* 1557 */     transactionAdviceBasicBean.setMerchantLocationId("USA123456789012");
/* 1558 */     transactionAdviceBasicBean.setPosDataCode("161201554110");
/* 1559 */     transactionAdviceBasicBean.setInvoiceReferenceNumber("InvoiceNo123456");
/* 1560 */     transactionAdviceBasicBean.setElectronicCommerceIndicator("");
/* 1561 */     transactionAdviceBasicBean.setTerminalId("Ter12345");
/* 1562 */     transactionAdviceBasicBean.setTabImageSequenceNumber(" ");
/*      */     
/*      */ 
/*      */ 
/* 1566 */     TransactionAdviceDetailBean transactionAdviceDetailBean = new TransactionAdviceDetailBean();
/*      */     
/* 1568 */     transactionAdviceDetailBean.setRecordType("TAD");
/* 1569 */     transactionAdviceDetailBean.setRecordNumber("00000003");
/* 1570 */     transactionAdviceDetailBean.setTransactionIdentifier("373872186011087");
/* 1571 */     transactionAdviceDetailBean.setAdditionalAmountType1("23");
/* 1572 */     transactionAdviceDetailBean.setAdditionalAmount1("7000");
/* 1573 */     transactionAdviceDetailBean.setAdditionalAmountSign1("+");
/* 1574 */     transactionAdviceDetailBean.setAdditionalAmountType2("52");
/* 1575 */     transactionAdviceDetailBean.setAdditionalAmount2("7000");
/* 1576 */     transactionAdviceDetailBean.setAdditionalAmountSign2("+");
/* 1577 */     transactionAdviceDetailBean.setAdditionalAmountType3("24");
/* 1578 */     transactionAdviceDetailBean.setAdditionalAmount3("7000");
/* 1579 */     transactionAdviceDetailBean.setAdditionalAmountSign3("+");
/* 1580 */     transactionAdviceDetailBean.setAdditionalAmountType4("61");
/* 1581 */     transactionAdviceDetailBean.setAdditionalAmount4("7000");
/* 1582 */     transactionAdviceDetailBean.setAdditionalAmountSign4("+");
/* 1583 */     transactionAdviceDetailBean.setAdditionalAmountType5("49");
/* 1584 */     transactionAdviceDetailBean.setAdditionalAmount5("7000");
/* 1585 */     transactionAdviceDetailBean.setAdditionalAmountSign5("+");
/*      */     
/* 1587 */     transactionAdviceBasicBean.setTransactionAdviceDetailBean(transactionAdviceDetailBean);
/*      */     
/*      */ 
/* 1590 */     LocationDetailTAABean locationDetailTAABean = new LocationDetailTAABean();
/*      */     
/*      */ 
/* 1593 */     locationDetailTAABean.setRecordType("TAA");
/* 1594 */     locationDetailTAABean.setRecordNumber("00000005");
/* 1595 */     locationDetailTAABean.setLocationName("ABC Atlanta ");
/* 1596 */     locationDetailTAABean.setLocationAddress("ABC Atlanta");
/* 1597 */     locationDetailTAABean.setLocationCity("ABC Atlanta");
/* 1598 */     locationDetailTAABean.setLocationRegion("CA");
/* 1599 */     locationDetailTAABean.setLocationCountryCode("840");
/* 1600 */     locationDetailTAABean.setLocationPostalCode("85024");
/* 1601 */     locationDetailTAABean.setTransactionIdentifier("373872186011087");
/* 1602 */     locationDetailTAABean.setAddendaTypeCode("99");
/* 1603 */     locationDetailTAABean.setMerchantCategoryCode("4112");
/* 1604 */     locationDetailTAABean.setSellerId("SELLR123456789012345");
/*      */     
/* 1606 */     transactionAdviceBasicBean.setLocationDetailTAABean(locationDetailTAABean);
/*      */     
/* 1608 */     TravelCruiseIndustryTAABean travelCruiseIndustryTAABean = new TravelCruiseIndustryTAABean();
/*      */     
/* 1610 */     travelCruiseIndustryTAABean.setRecordType("TAA");
/* 1611 */     travelCruiseIndustryTAABean.setRecordNumber("04");
/* 1612 */     travelCruiseIndustryTAABean.setTransactionIdentifier("373872186011087");
/* 1613 */     travelCruiseIndustryTAABean.setAddendaTypeCode("03");
/* 1614 */     travelCruiseIndustryTAABean.setFormatCode("14");
/* 1615 */     travelCruiseIndustryTAABean.setIataCarrierCode(" ");
/* 1616 */     travelCruiseIndustryTAABean.setIataAgencyNumber(" ");
/* 1617 */     travelCruiseIndustryTAABean.setTravelPackageIndicator("A");
/* 1618 */     travelCruiseIndustryTAABean.setPassengerName("Ram Aur Shyam");
/* 1619 */     travelCruiseIndustryTAABean.setTravelTicketNumber("A0123456789123");
/* 1620 */     travelCruiseIndustryTAABean.setTravelDepartureDateSegment1("20080216");
/* 1621 */     travelCruiseIndustryTAABean.setTravelDestinationCodeSegment1("ATL");
/* 1622 */     travelCruiseIndustryTAABean.setTravelDepartureAirportSegment1("BOS");
/* 1623 */     travelCruiseIndustryTAABean.setAirCarrierCodeSegment1("BA");
/* 1624 */     travelCruiseIndustryTAABean.setFlightNumberSegment1("BA38");
/* 1625 */     travelCruiseIndustryTAABean.setClassCodeSegment1("F");
/*      */     
/* 1627 */     travelCruiseIndustryTAABean.setTravelDepartureDateSegment2("20080218");
/* 1628 */     travelCruiseIndustryTAABean.setTravelDestinationCodeSegment2("BOS");
/* 1629 */     travelCruiseIndustryTAABean.setTravelDepartureAirportSegment2("ATL");
/* 1630 */     travelCruiseIndustryTAABean.setAirCarrierCodeSegment2("BA");
/* 1631 */     travelCruiseIndustryTAABean.setFlightNumberSegment2("BA50");
/* 1632 */     travelCruiseIndustryTAABean.setClassCodeSegment2("A");
/* 1633 */     travelCruiseIndustryTAABean.setTravelDepartureDateSegment3("20080220");
/* 1634 */     travelCruiseIndustryTAABean.setTravelDestinationCodeSegment3("ATL");
/* 1635 */     travelCruiseIndustryTAABean.setTravelDepartureAirportSegment3("BOS");
/* 1636 */     travelCruiseIndustryTAABean.setAirCarrierCodeSegment3("BA");
/* 1637 */     travelCruiseIndustryTAABean.setFlightNumberSegment3("BA53");
/* 1638 */     travelCruiseIndustryTAABean.setClassCodeSegment3("A");
/* 1639 */     travelCruiseIndustryTAABean.setTravelDepartureDateSegment4("20080222");
/* 1640 */     travelCruiseIndustryTAABean.setTravelDestinationCodeSegment4("BOS");
/* 1641 */     travelCruiseIndustryTAABean.setTravelDepartureAirportSegment4("ATL");
/* 1642 */     travelCruiseIndustryTAABean.setAirCarrierCodeSegment4("");
/* 1643 */     travelCruiseIndustryTAABean.setFlightNumberSegment4("");
/* 1644 */     travelCruiseIndustryTAABean.setClassCodeSegment4("B");
/* 1645 */     travelCruiseIndustryTAABean.setLodgingCheckInDate("20080216");
/* 1646 */     travelCruiseIndustryTAABean.setLodgingCheckOutDate("20080222");
/* 1647 */     travelCruiseIndustryTAABean.setLodgingRoomRate1("3500");
/* 1648 */     travelCruiseIndustryTAABean.setNumberOfNightsAtRoomRate1("1");
/* 1649 */     travelCruiseIndustryTAABean.setLodgingName("ABC Lodging");
/* 1650 */     travelCruiseIndustryTAABean.setLodgingRegionCode("CA");
/* 1651 */     travelCruiseIndustryTAABean.setLodgingCountryCode("840");
/* 1652 */     travelCruiseIndustryTAABean.setLodgingCityName("Boston");
/*      */     
/*      */ 
/*      */ 
/* 1656 */     transactionAdviceBasicBean.setTransactionAdviceAddendumBean(travelCruiseIndustryTAABean);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1663 */     TransactionBatchTrailerBean transactionBatchTrailerBean = new TransactionBatchTrailerBean();
/*      */     
/* 1665 */     transactionBatchTrailerBean.setMerchantId("5021045083");
/* 1666 */     transactionBatchTrailerBean.setRecordNumber("00000006");
/* 1667 */     transactionBatchTrailerBean.setRecordType("TBT");
/* 1668 */     transactionBatchTrailerBean.setTbtCurrencyCode("840");
/* 1669 */     transactionBatchTrailerBean.setTbtAmount("35000");
/* 1670 */     transactionBatchTrailerBean.setTbtAmountSign("+");
/* 1671 */     transactionBatchTrailerBean.setTbtCreationDate("21110402");
/* 1672 */     transactionBatchTrailerBean.setTbtIdentificationNumber("1000000000000007");
/* 1673 */     transactionBatchTrailerBean.setTbtImageSequenceNumber(" ");
/* 1674 */     transactionBatchTrailerBean.setTotalNoOfTabs("1");
/*      */     
/*      */ 
/* 1677 */     List<TransactionAdviceBasicBean> transactionAdviceBasicType = new ArrayList();
/*      */     
/* 1679 */     transactionAdviceBasicType.add(transactionAdviceBasicBean);
/*      */     
/* 1681 */     TransactionFileSummaryBean transactionFileSummaryBean = new TransactionFileSummaryBean();
/*      */     
/* 1683 */     transactionFileSummaryBean.setRecordType("TFS");
/* 1684 */     transactionFileSummaryBean.setRecordNumber("00000007");
/* 1685 */     transactionFileSummaryBean.setNumberOfDebits("1");
/* 1686 */     transactionFileSummaryBean.setHashTotalDebitAmount("500");
/* 1687 */     transactionFileSummaryBean.setNumberOfCredits("0");
/* 1688 */     transactionFileSummaryBean.setHashTotalCreditAmount("0");
/* 1689 */     transactionFileSummaryBean.setHashTotalAmount("500");
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1695 */     TransactionTBTSpecificBean transactionTBTSpecificBean = new TransactionTBTSpecificBean();
/*      */     
/* 1697 */     transactionTBTSpecificBean.setTransactionAdviceBasicType(transactionAdviceBasicType);
/* 1698 */     transactionTBTSpecificBean.setTransactionBatchTrailerBean(transactionBatchTrailerBean);
/*      */     
/* 1700 */     List<TransactionTBTSpecificBean> transactionTBTSpecificType = new ArrayList();
/*      */     
/* 1702 */     transactionTBTSpecificType.add(transactionTBTSpecificBean);
/*      */     
/*      */ 
/*      */ 
/* 1706 */     SettlementRequestDataObject settlementRequestDataObject = new SettlementRequestDataObject();
/*      */     
/* 1708 */     settlementRequestDataObject
/* 1709 */       .setTransactionFileHeaderType(transactionFileHeaderBean);
/* 1710 */     settlementRequestDataObject
/* 1711 */       .setTransactionFileSummaryType(transactionFileSummaryBean);
/* 1712 */     settlementRequestDataObject
/* 1713 */       .setTransactionTBTSpecificType(transactionTBTSpecificType);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1719 */     PropertyReader propertyReader = new PropertyReader();
/*      */     
/*      */ 
/* 1722 */     propertyReader = propertyReader.loadPropertyFile("/settlement.properties");
/*      */     
/*      */ 
/*      */ 
/*      */ 
/* 1727 */     SettlementResponseDataObject settlementResponseDataObject = RequestProcessorFactory.processRequest(settlementRequestDataObject, propertyReader);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */     try
/*      */     {
/* 1738 */       if (!settlementResponseDataObject.getActionCodeDescription().isEmpty())
/*      */       {
/* 1740 */         Iterator<?> iterater = settlementResponseDataObject.getActionCodeDescription().iterator();
/* 1741 */         while (iterater.hasNext()) {
/* 1742 */           ErrorObject errorObject = (ErrorObject)iterater.next();
/* 1743 */           System.out.print(errorObject.getErrorCode() + "-" + errorObject.getErrorDescription() + "\n");
/*      */         }
/*      */       } else {
/* 1746 */         System.out.print("Error List is empty\n");
/*      */       }
/*      */       
/*      */ 
/*      */     }
/*      */     catch (Exception e)
/*      */     {
/*      */ 
/* 1754 */       System.out.println("Error Object is Empty");
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void retailTest()
/*      */     throws Exception
/*      */   {
/* 1766 */     SimpleDateFormat datefmt = new SimpleDateFormat("MMdd");
/*      */     
/* 1768 */     SimpleDateFormat timefmt = new SimpleDateFormat("hhmm");
/* 1769 */     StringBuffer ab = new StringBuffer();
/* 1770 */     ab.append(datefmt.format(Calendar.getInstance().getTime()));
/* 1771 */     ab.append(timefmt.format(Calendar.getInstance().getTime()));
/*      */     
/*      */ 
/* 1774 */     TransactionFileHeaderBean transactionFileHeaderBean = new TransactionFileHeaderBean();
/*      */     
/* 1776 */     transactionFileHeaderBean.setRecordType("TFH");
/* 1777 */     transactionFileHeaderBean.setRecordNumber("00000001");
/* 1778 */     transactionFileHeaderBean.setSubmitterId("GWS002");
/*      */     
/* 1780 */     transactionFileHeaderBean.setSubmitterFileReferenceNumber("1" + ab.toString());
/*      */     
/* 1782 */     transactionFileHeaderBean.setFileCreationDate("20110523");
/* 1783 */     transactionFileHeaderBean.setFileCreationTime("172020");
/* 1784 */     transactionFileHeaderBean.setFileVersionNumber("12010000");
/*      */     
/*      */ 
/* 1787 */     TransactionAdviceBasicBean transactionAdviceBasicBean = new TransactionAdviceBasicBean();
/*      */     
/*      */ 
/*      */ 
/* 1791 */     transactionAdviceBasicBean.setRecordNumber("00000002");
/* 1792 */     transactionAdviceBasicBean.setRecordType("TAB");
/* 1793 */     transactionAdviceBasicBean.setTransactionIdentifier("373872186011087");
/* 1794 */     transactionAdviceBasicBean.setFormatCode("20");
/* 1795 */     transactionAdviceBasicBean.setMediaCode("01");
/* 1796 */     transactionAdviceBasicBean.setSubmissionMethod("");
/* 1797 */     transactionAdviceBasicBean.setApprovalCode("712389");
/* 1798 */     transactionAdviceBasicBean.setPrimaryAccountNumber("373953192351004");
/* 1799 */     transactionAdviceBasicBean.setCardExpiryDate("1405");
/* 1800 */     transactionAdviceBasicBean.setTransactionDate("21110402");
/* 1801 */     transactionAdviceBasicBean.setTransactionTime("143000");
/* 1802 */     transactionAdviceBasicBean.setTransactionAmount("10900");
/* 1803 */     transactionAdviceBasicBean.setProcessingCode("000000");
/* 1804 */     transactionAdviceBasicBean.setTransactionCurrencyCode("124");
/* 1805 */     transactionAdviceBasicBean.setExtendedPaymentData("01");
/* 1806 */     transactionAdviceBasicBean.setMerchantId("5021045083");
/* 1807 */     transactionAdviceBasicBean.setMerchantLocationId("CAN123456789012");
/* 1808 */     transactionAdviceBasicBean.setPosDataCode("161201554110");
/* 1809 */     transactionAdviceBasicBean.setInvoiceReferenceNumber("InvoiceNo123456");
/* 1810 */     transactionAdviceBasicBean.setElectronicCommerceIndicator("");
/* 1811 */     transactionAdviceBasicBean.setTerminalId("Ter12345");
/* 1812 */     transactionAdviceBasicBean.setTabImageSequenceNumber(" ");
/*      */     
/*      */ 
/*      */ 
/* 1816 */     TransactionAdviceDetailBean transactionAdviceDetailBean = new TransactionAdviceDetailBean();
/*      */     
/* 1818 */     transactionAdviceDetailBean.setRecordType("TAD");
/* 1819 */     transactionAdviceDetailBean.setRecordNumber("00000003");
/* 1820 */     transactionAdviceDetailBean.setTransactionIdentifier("373872186011087");
/* 1821 */     transactionAdviceDetailBean.setAdditionalAmountType1("56");
/* 1822 */     transactionAdviceDetailBean.setAdditionalAmount1("900");
/* 1823 */     transactionAdviceDetailBean.setAdditionalAmountSign1("+");
/* 1824 */     transactionAdviceDetailBean.setAdditionalAmountType2("067");
/* 1825 */     transactionAdviceDetailBean.setAdditionalAmount2("2500");
/* 1826 */     transactionAdviceDetailBean.setAdditionalAmountSign2("+");
/* 1827 */     transactionAdviceDetailBean.setAdditionalAmountType3("14");
/* 1828 */     transactionAdviceDetailBean.setAdditionalAmount3("2500");
/* 1829 */     transactionAdviceDetailBean.setAdditionalAmountSign3("+");
/* 1830 */     transactionAdviceDetailBean.setAdditionalAmountType4("57");
/* 1831 */     transactionAdviceDetailBean.setAdditionalAmount4("2500");
/* 1832 */     transactionAdviceDetailBean.setAdditionalAmountSign4("+");
/* 1833 */     transactionAdviceDetailBean.setAdditionalAmountType5("10");
/* 1834 */     transactionAdviceDetailBean.setAdditionalAmount5("2500");
/* 1835 */     transactionAdviceDetailBean.setAdditionalAmountSign5("+");
/*      */     
/* 1837 */     transactionAdviceBasicBean.setTransactionAdviceDetailBean(transactionAdviceDetailBean);
/*      */     
/*      */ 
/* 1840 */     LocationDetailTAABean locationDetailTAABean = new LocationDetailTAABean();
/*      */     
/*      */ 
/* 1843 */     locationDetailTAABean.setRecordType("TAA");
/* 1844 */     locationDetailTAABean.setRecordNumber("00000005");
/* 1845 */     locationDetailTAABean.setLocationName("Canada");
/* 1846 */     locationDetailTAABean.setLocationAddress("Canada");
/* 1847 */     locationDetailTAABean.setLocationCity("Canada");
/* 1848 */     locationDetailTAABean.setLocationRegion("NS");
/* 1849 */     locationDetailTAABean.setLocationCountryCode("124");
/* 1850 */     locationDetailTAABean.setLocationPostalCode("85024");
/* 1851 */     locationDetailTAABean.setTransactionIdentifier("373872186011087");
/* 1852 */     locationDetailTAABean.setAddendaTypeCode("99");
/* 1853 */     locationDetailTAABean.setMerchantCategoryCode("4112");
/* 1854 */     locationDetailTAABean.setSellerId("SELLR123456789012345");
/*      */     
/* 1856 */     transactionAdviceBasicBean.setLocationDetailTAABean(locationDetailTAABean);
/*      */     
/* 1858 */     RetailIndustryTAABean retailIndustryTAABean = new RetailIndustryTAABean();
/*      */     
/* 1860 */     retailIndustryTAABean.setRecordType("TAA");
/* 1861 */     retailIndustryTAABean.setRecordNumber("004");
/* 1862 */     retailIndustryTAABean.setTransactionIdentifier("123456");
/* 1863 */     retailIndustryTAABean.setFormatCode("01");
/* 1864 */     retailIndustryTAABean.setAddendaTypeCode("03");
/*      */     
/* 1866 */     retailIndustryTAABean.setRetailDepartmentName("XYZ Stores Toronto ");
/* 1867 */     retailIndustryTAABean.setRetailItemDescription1("Pen");
/* 1868 */     retailIndustryTAABean.setRetailItemQuantity1("1");
/* 1869 */     retailIndustryTAABean.setRetailItemAmount1("900");
/*      */     
/* 1871 */     retailIndustryTAABean.setRetailItemDescription2("Pencil");
/* 1872 */     retailIndustryTAABean.setRetailItemQuantity2("5");
/* 1873 */     retailIndustryTAABean.setRetailItemAmount2("400");
/*      */     
/* 1875 */     retailIndustryTAABean.setRetailItemDescription3("Long Book");
/* 1876 */     retailIndustryTAABean.setRetailItemQuantity3("4");
/* 1877 */     retailIndustryTAABean.setRetailItemAmount3("500");
/*      */     
/* 1879 */     retailIndustryTAABean.setRetailItemDescription4("Short Book");
/* 1880 */     retailIndustryTAABean.setRetailItemQuantity4("2");
/* 1881 */     retailIndustryTAABean.setRetailItemAmount4("1000");
/*      */     
/* 1883 */     retailIndustryTAABean.setRetailItemDescription5("Short Notepad");
/* 1884 */     retailIndustryTAABean.setRetailItemQuantity5("1");
/* 1885 */     retailIndustryTAABean.setRetailItemAmount5("1000");
/*      */     
/* 1887 */     retailIndustryTAABean.setRetailItemDescription6("Long Notepad");
/* 1888 */     retailIndustryTAABean.setRetailItemQuantity6("1");
/* 1889 */     retailIndustryTAABean.setRetailItemAmount6("3000");
/*      */     
/* 1891 */     transactionAdviceBasicBean.setTransactionAdviceAddendumBean(retailIndustryTAABean);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1898 */     TransactionBatchTrailerBean transactionBatchTrailerBean = new TransactionBatchTrailerBean();
/*      */     
/* 1900 */     transactionBatchTrailerBean.setMerchantId("5021045083");
/* 1901 */     transactionBatchTrailerBean.setRecordNumber("00000006");
/* 1902 */     transactionBatchTrailerBean.setRecordType("TBT");
/* 1903 */     transactionBatchTrailerBean.setTbtCurrencyCode("124");
/* 1904 */     transactionBatchTrailerBean.setTbtAmount("10900");
/* 1905 */     transactionBatchTrailerBean.setTbtAmountSign("+");
/* 1906 */     transactionBatchTrailerBean.setTbtCreationDate("21110402");
/* 1907 */     transactionBatchTrailerBean.setTbtIdentificationNumber("1000000000000007");
/* 1908 */     transactionBatchTrailerBean.setTbtImageSequenceNumber(" ");
/* 1909 */     transactionBatchTrailerBean.setTotalNoOfTabs("1");
/*      */     
/*      */ 
/* 1912 */     List<TransactionAdviceBasicBean> transactionAdviceBasicType = new ArrayList();
/*      */     
/* 1914 */     transactionAdviceBasicType.add(transactionAdviceBasicBean);
/*      */     
/* 1916 */     TransactionFileSummaryBean transactionFileSummaryBean = new TransactionFileSummaryBean();
/*      */     
/* 1918 */     transactionFileSummaryBean.setRecordType("TFS");
/* 1919 */     transactionFileSummaryBean.setRecordNumber("00000007");
/* 1920 */     transactionFileSummaryBean.setNumberOfDebits("1");
/* 1921 */     transactionFileSummaryBean.setHashTotalDebitAmount("500");
/* 1922 */     transactionFileSummaryBean.setNumberOfCredits("0");
/* 1923 */     transactionFileSummaryBean.setHashTotalCreditAmount("0");
/* 1924 */     transactionFileSummaryBean.setHashTotalAmount("500");
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1930 */     TransactionTBTSpecificBean transactionTBTSpecificBean = new TransactionTBTSpecificBean();
/*      */     
/* 1932 */     transactionTBTSpecificBean.setTransactionAdviceBasicType(transactionAdviceBasicType);
/* 1933 */     transactionTBTSpecificBean.setTransactionBatchTrailerBean(transactionBatchTrailerBean);
/*      */     
/* 1935 */     List<TransactionTBTSpecificBean> transactionTBTSpecificType = new ArrayList();
/*      */     
/* 1937 */     transactionTBTSpecificType.add(transactionTBTSpecificBean);
/*      */     
/*      */ 
/*      */ 
/* 1941 */     SettlementRequestDataObject settlementRequestDataObject = new SettlementRequestDataObject();
/*      */     
/* 1943 */     settlementRequestDataObject
/* 1944 */       .setTransactionFileHeaderType(transactionFileHeaderBean);
/* 1945 */     settlementRequestDataObject
/* 1946 */       .setTransactionFileSummaryType(transactionFileSummaryBean);
/* 1947 */     settlementRequestDataObject
/* 1948 */       .setTransactionTBTSpecificType(transactionTBTSpecificType);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1954 */     PropertyReader propertyReader = new PropertyReader();
/*      */     
/*      */ 
/* 1957 */     propertyReader = propertyReader.loadPropertyFile("/settlement.properties");
/*      */     
/*      */ 
/*      */ 
/*      */ 
/* 1962 */     SettlementResponseDataObject settlementResponseDataObject = RequestProcessorFactory.processRequest(settlementRequestDataObject, propertyReader);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */     try
/*      */     {
/* 1973 */       if (!settlementResponseDataObject.getActionCodeDescription().isEmpty())
/*      */       {
/* 1975 */         Iterator<?> iterater = settlementResponseDataObject.getActionCodeDescription().iterator();
/* 1976 */         while (iterater.hasNext()) {
/* 1977 */           ErrorObject errorObject = (ErrorObject)iterater.next();
/* 1978 */           System.out.print(errorObject.getErrorCode() + "-" + errorObject.getErrorDescription() + "\n");
/*      */         }
/*      */       } else {
/* 1981 */         System.out.print("Error List is empty\n");
/*      */       }
/*      */       
/*      */ 
/*      */     }
/*      */     catch (Exception e)
/*      */     {
/*      */ 
/* 1989 */       System.out.println("Error Object is Empty");
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   public void enterTainmentTest()
/*      */     throws Exception
/*      */   {
/* 1999 */     SimpleDateFormat datefmt = new SimpleDateFormat("MMdd");
/*      */     
/* 2001 */     SimpleDateFormat timefmt = new SimpleDateFormat("hhmm");
/* 2002 */     StringBuffer ab = new StringBuffer();
/* 2003 */     ab.append(datefmt.format(Calendar.getInstance().getTime()));
/* 2004 */     ab.append(timefmt.format(Calendar.getInstance().getTime()));
/*      */     
/*      */ 
/* 2007 */     TransactionFileHeaderBean transactionFileHeaderBean = new TransactionFileHeaderBean();
/*      */     
/* 2009 */     transactionFileHeaderBean.setRecordType("TFH");
/* 2010 */     transactionFileHeaderBean.setRecordNumber("00000001");
/* 2011 */     transactionFileHeaderBean.setSubmitterId("GWS002");
/*      */     
/* 2013 */     transactionFileHeaderBean.setSubmitterFileReferenceNumber("1" + ab.toString());
/*      */     
/* 2015 */     transactionFileHeaderBean.setFileCreationDate("20110523");
/* 2016 */     transactionFileHeaderBean.setFileCreationTime("172020");
/* 2017 */     transactionFileHeaderBean.setFileVersionNumber("12010000");
/*      */     
/*      */ 
/* 2020 */     TransactionAdviceBasicBean transactionAdviceBasicBean = new TransactionAdviceBasicBean();
/*      */     
/*      */ 
/*      */ 
/* 2024 */     transactionAdviceBasicBean.setRecordNumber("00000002");
/* 2025 */     transactionAdviceBasicBean.setRecordType("TAB");
/* 2026 */     transactionAdviceBasicBean.setTransactionIdentifier("373872186011087");
/* 2027 */     transactionAdviceBasicBean.setFormatCode("22");
/* 2028 */     transactionAdviceBasicBean.setMediaCode("01");
/* 2029 */     transactionAdviceBasicBean.setSubmissionMethod("");
/* 2030 */     transactionAdviceBasicBean.setApprovalCode("712389");
/* 2031 */     transactionAdviceBasicBean.setPrimaryAccountNumber("373953192351004");
/* 2032 */     transactionAdviceBasicBean.setCardExpiryDate("1405");
/* 2033 */     transactionAdviceBasicBean.setTransactionDate("21110402");
/* 2034 */     transactionAdviceBasicBean.setTransactionTime("143000");
/* 2035 */     transactionAdviceBasicBean.setTransactionAmount("76000");
/* 2036 */     transactionAdviceBasicBean.setProcessingCode("000000");
/* 2037 */     transactionAdviceBasicBean.setTransactionCurrencyCode("392");
/* 2038 */     transactionAdviceBasicBean.setExtendedPaymentData("01");
/* 2039 */     transactionAdviceBasicBean.setMerchantId("5021045083");
/* 2040 */     transactionAdviceBasicBean.setMerchantLocationId("JAP123456789012");
/* 2041 */     transactionAdviceBasicBean.setPosDataCode("161201554110");
/* 2042 */     transactionAdviceBasicBean.setInvoiceReferenceNumber("InvoiceNo123456");
/* 2043 */     transactionAdviceBasicBean.setElectronicCommerceIndicator("");
/* 2044 */     transactionAdviceBasicBean.setTerminalId("Ter12345");
/* 2045 */     transactionAdviceBasicBean.setTabImageSequenceNumber(" ");
/*      */     
/*      */ 
/*      */ 
/* 2049 */     TransactionAdviceDetailBean transactionAdviceDetailBean = new TransactionAdviceDetailBean();
/*      */     
/* 2051 */     transactionAdviceDetailBean.setRecordType("TAD");
/* 2052 */     transactionAdviceDetailBean.setRecordNumber("00000003");
/* 2053 */     transactionAdviceDetailBean.setTransactionIdentifier("373872186011087");
/* 2054 */     transactionAdviceDetailBean.setAdditionalAmountType1("56");
/* 2055 */     transactionAdviceDetailBean.setAdditionalAmount1("15200");
/* 2056 */     transactionAdviceDetailBean.setAdditionalAmountSign1("+");
/* 2057 */     transactionAdviceDetailBean.setAdditionalAmountType2("57");
/* 2058 */     transactionAdviceDetailBean.setAdditionalAmount2("15200");
/* 2059 */     transactionAdviceDetailBean.setAdditionalAmountSign2("+");
/* 2060 */     transactionAdviceDetailBean.setAdditionalAmountType3("12");
/* 2061 */     transactionAdviceDetailBean.setAdditionalAmount3("15200");
/* 2062 */     transactionAdviceDetailBean.setAdditionalAmountSign3("+");
/* 2063 */     transactionAdviceDetailBean.setAdditionalAmountType4("13");
/* 2064 */     transactionAdviceDetailBean.setAdditionalAmount4("15200");
/* 2065 */     transactionAdviceDetailBean.setAdditionalAmountSign4("+");
/* 2066 */     transactionAdviceDetailBean.setAdditionalAmountType5("14");
/* 2067 */     transactionAdviceDetailBean.setAdditionalAmount5("15200");
/* 2068 */     transactionAdviceDetailBean.setAdditionalAmountSign5("+");
/*      */     
/* 2070 */     transactionAdviceBasicBean.setTransactionAdviceDetailBean(transactionAdviceDetailBean);
/*      */     
/*      */ 
/* 2073 */     LocationDetailTAABean locationDetailTAABean = new LocationDetailTAABean();
/*      */     
/*      */ 
/* 2076 */     locationDetailTAABean.setRecordType("TAA");
/* 2077 */     locationDetailTAABean.setRecordNumber("00000005");
/* 2078 */     locationDetailTAABean.setLocationName("Canada");
/* 2079 */     locationDetailTAABean.setLocationAddress("Canada");
/* 2080 */     locationDetailTAABean.setLocationCity("Canada");
/* 2081 */     locationDetailTAABean.setLocationRegion("NS");
/* 2082 */     locationDetailTAABean.setLocationCountryCode("392");
/* 2083 */     locationDetailTAABean.setLocationPostalCode("85024");
/* 2084 */     locationDetailTAABean.setTransactionIdentifier("373872186011087");
/* 2085 */     locationDetailTAABean.setAddendaTypeCode("99");
/* 2086 */     locationDetailTAABean.setMerchantCategoryCode("4112");
/* 2087 */     locationDetailTAABean.setSellerId("SELLR123456789012345");
/*      */     
/* 2089 */     transactionAdviceBasicBean.setLocationDetailTAABean(locationDetailTAABean);
/*      */     
/* 2091 */     EntertainmentTicketingIndustryTAABean etBean = new EntertainmentTicketingIndustryTAABean();
/* 2092 */     etBean.setRecordType("TAA");
/* 2093 */     etBean.setRecordNumber("04");
/* 2094 */     etBean.setTransactionIdentifier("373872186011087");
/* 2095 */     etBean.setAddendaTypeCode("22");
/* 2096 */     etBean.setFormatCode("03");
/*      */     
/* 2098 */     etBean.setEventName("ANY");
/* 2099 */     etBean.setEventDate("20080216");
/* 2100 */     etBean.setEventIndividualTicketPriceAmount("76000");
/* 2101 */     etBean.setEventTicketQuantity("10");
/* 2102 */     etBean.setEventLocation("JAPAN");
/* 2103 */     etBean.setEventRegionCode("JP");
/* 2104 */     etBean.setEventCountryCode("392");
/*      */     
/* 2106 */     transactionAdviceBasicBean.setTransactionAdviceAddendumBean(etBean);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 2113 */     TransactionBatchTrailerBean transactionBatchTrailerBean = new TransactionBatchTrailerBean();
/*      */     
/* 2115 */     transactionBatchTrailerBean.setMerchantId("5021045083");
/* 2116 */     transactionBatchTrailerBean.setRecordNumber("00000006");
/* 2117 */     transactionBatchTrailerBean.setRecordType("TBT");
/* 2118 */     transactionBatchTrailerBean.setTbtCurrencyCode("392");
/* 2119 */     transactionBatchTrailerBean.setTbtAmount("76000");
/* 2120 */     transactionBatchTrailerBean.setTbtAmountSign("+");
/* 2121 */     transactionBatchTrailerBean.setTbtCreationDate("21110402");
/* 2122 */     transactionBatchTrailerBean.setTbtIdentificationNumber("1000000000000009");
/* 2123 */     transactionBatchTrailerBean.setTbtImageSequenceNumber(" ");
/* 2124 */     transactionBatchTrailerBean.setTotalNoOfTabs("1");
/*      */     
/*      */ 
/* 2127 */     List<TransactionAdviceBasicBean> transactionAdviceBasicType = new ArrayList();
/*      */     
/* 2129 */     transactionAdviceBasicType.add(transactionAdviceBasicBean);
/*      */     
/* 2131 */     TransactionFileSummaryBean transactionFileSummaryBean = new TransactionFileSummaryBean();
/*      */     
/* 2133 */     transactionFileSummaryBean.setRecordType("TFS");
/* 2134 */     transactionFileSummaryBean.setRecordNumber("00000007");
/* 2135 */     transactionFileSummaryBean.setNumberOfDebits("1");
/* 2136 */     transactionFileSummaryBean.setHashTotalDebitAmount("500");
/* 2137 */     transactionFileSummaryBean.setNumberOfCredits("0");
/* 2138 */     transactionFileSummaryBean.setHashTotalCreditAmount("0");
/* 2139 */     transactionFileSummaryBean.setHashTotalAmount("500");
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 2145 */     TransactionTBTSpecificBean transactionTBTSpecificBean = new TransactionTBTSpecificBean();
/*      */     
/* 2147 */     transactionTBTSpecificBean.setTransactionAdviceBasicType(transactionAdviceBasicType);
/* 2148 */     transactionTBTSpecificBean.setTransactionBatchTrailerBean(transactionBatchTrailerBean);
/*      */     
/* 2150 */     List<TransactionTBTSpecificBean> transactionTBTSpecificType = new ArrayList();
/*      */     
/* 2152 */     transactionTBTSpecificType.add(transactionTBTSpecificBean);
/*      */     
/*      */ 
/*      */ 
/* 2156 */     SettlementRequestDataObject settlementRequestDataObject = new SettlementRequestDataObject();
/*      */     
/* 2158 */     settlementRequestDataObject
/* 2159 */       .setTransactionFileHeaderType(transactionFileHeaderBean);
/* 2160 */     settlementRequestDataObject
/* 2161 */       .setTransactionFileSummaryType(transactionFileSummaryBean);
/* 2162 */     settlementRequestDataObject
/* 2163 */       .setTransactionTBTSpecificType(transactionTBTSpecificType);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 2169 */     PropertyReader propertyReader = new PropertyReader();
/*      */     
/*      */ 
/* 2172 */     propertyReader = propertyReader.loadPropertyFile("/settlement.properties");
/*      */     
/*      */ 
/*      */ 
/*      */ 
/* 2177 */     SettlementResponseDataObject settlementResponseDataObject = RequestProcessorFactory.processRequest(settlementRequestDataObject, propertyReader);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */     try
/*      */     {
/* 2188 */       if (!settlementResponseDataObject.getActionCodeDescription().isEmpty())
/*      */       {
/* 2190 */         Iterator<?> iterater = settlementResponseDataObject.getActionCodeDescription().iterator();
/* 2191 */         while (iterater.hasNext()) {
/* 2192 */           ErrorObject errorObject = (ErrorObject)iterater.next();
/* 2193 */           System.out.print(errorObject.getErrorCode() + "-" + errorObject.getErrorDescription() + "\n");
/*      */         }
/*      */       } else {
/* 2196 */         System.out.print("Error List is empty\n");
/*      */       }
/*      */       
/*      */ 
/*      */     }
/*      */     catch (Exception e)
/*      */     {
/*      */ 
/* 2204 */       System.out.println("Error Object is Empty");
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void cpsl2Test()
/*      */     throws Exception
/*      */   {
/* 2215 */     SimpleDateFormat datefmt = new SimpleDateFormat("MMdd");
/*      */     
/* 2217 */     SimpleDateFormat timefmt = new SimpleDateFormat("hhmm");
/* 2218 */     StringBuffer ab = new StringBuffer();
/* 2219 */     ab.append(datefmt.format(Calendar.getInstance().getTime()));
/* 2220 */     ab.append(timefmt.format(Calendar.getInstance().getTime()));
/*      */     
/*      */ 
/* 2223 */     TransactionFileHeaderBean transactionFileHeaderBean = new TransactionFileHeaderBean();
/*      */     
/* 2225 */     transactionFileHeaderBean.setRecordType("TFH");
/* 2226 */     transactionFileHeaderBean.setRecordNumber("00000001");
/* 2227 */     transactionFileHeaderBean.setSubmitterId("GWS002");
/*      */     
/* 2229 */     transactionFileHeaderBean.setSubmitterFileReferenceNumber("1" + ab.toString());
/*      */     
/* 2231 */     transactionFileHeaderBean.setFileCreationDate("20110523");
/* 2232 */     transactionFileHeaderBean.setFileCreationTime("172020");
/* 2233 */     transactionFileHeaderBean.setFileVersionNumber("12010000");
/*      */     
/*      */ 
/* 2236 */     TransactionAdviceBasicBean transactionAdviceBasicBean = new TransactionAdviceBasicBean();
/*      */     
/*      */ 
/*      */ 
/* 2240 */     transactionAdviceBasicBean.setRecordNumber("00000002");
/* 2241 */     transactionAdviceBasicBean.setRecordType("TAB");
/* 2242 */     transactionAdviceBasicBean.setTransactionIdentifier("373872186011087");
/* 2243 */     transactionAdviceBasicBean.setFormatCode("02");
/* 2244 */     transactionAdviceBasicBean.setMediaCode("01");
/* 2245 */     transactionAdviceBasicBean.setSubmissionMethod("");
/* 2246 */     transactionAdviceBasicBean.setApprovalCode("712389");
/* 2247 */     transactionAdviceBasicBean.setPrimaryAccountNumber("373953192351004");
/* 2248 */     transactionAdviceBasicBean.setCardExpiryDate("1405");
/* 2249 */     transactionAdviceBasicBean.setTransactionDate("21110402");
/* 2250 */     transactionAdviceBasicBean.setTransactionTime("143000");
/* 2251 */     transactionAdviceBasicBean.setTransactionAmount("27300");
/* 2252 */     transactionAdviceBasicBean.setProcessingCode("000000");
/* 2253 */     transactionAdviceBasicBean.setTransactionCurrencyCode("840");
/* 2254 */     transactionAdviceBasicBean.setExtendedPaymentData("01");
/* 2255 */     transactionAdviceBasicBean.setMerchantId("5021045083");
/* 2256 */     transactionAdviceBasicBean.setMerchantLocationId("JAP123456789012");
/* 2257 */     transactionAdviceBasicBean.setPosDataCode("161201554110");
/* 2258 */     transactionAdviceBasicBean.setInvoiceReferenceNumber("InvoiceNo123456");
/* 2259 */     transactionAdviceBasicBean.setElectronicCommerceIndicator("");
/* 2260 */     transactionAdviceBasicBean.setTerminalId("Ter12345");
/* 2261 */     transactionAdviceBasicBean.setTabImageSequenceNumber(" ");
/*      */     
/*      */ 
/*      */ 
/* 2265 */     TransactionAdviceDetailBean transactionAdviceDetailBean = new TransactionAdviceDetailBean();
/*      */     
/* 2267 */     transactionAdviceDetailBean.setRecordType("TAD");
/* 2268 */     transactionAdviceDetailBean.setRecordNumber("00000003");
/* 2269 */     transactionAdviceDetailBean.setTransactionIdentifier("373872186011087");
/* 2270 */     transactionAdviceDetailBean.setAdditionalAmountType1("11");
/* 2271 */     transactionAdviceDetailBean.setAdditionalAmount1("7300");
/* 2272 */     transactionAdviceDetailBean.setAdditionalAmountSign1("+");
/* 2273 */     transactionAdviceDetailBean.setAdditionalAmountType2("12");
/* 2274 */     transactionAdviceDetailBean.setAdditionalAmount2("5000");
/* 2275 */     transactionAdviceDetailBean.setAdditionalAmountSign2("+");
/* 2276 */     transactionAdviceDetailBean.setAdditionalAmountType3("13");
/* 2277 */     transactionAdviceDetailBean.setAdditionalAmount3("5000");
/* 2278 */     transactionAdviceDetailBean.setAdditionalAmountSign3("+");
/* 2279 */     transactionAdviceDetailBean.setAdditionalAmountType4("14");
/* 2280 */     transactionAdviceDetailBean.setAdditionalAmount4("5000");
/* 2281 */     transactionAdviceDetailBean.setAdditionalAmountSign4("+");
/* 2282 */     transactionAdviceDetailBean.setAdditionalAmountType5("02");
/* 2283 */     transactionAdviceDetailBean.setAdditionalAmount5("5000");
/* 2284 */     transactionAdviceDetailBean.setAdditionalAmountSign5("+");
/*      */     
/* 2286 */     transactionAdviceBasicBean.setTransactionAdviceDetailBean(transactionAdviceDetailBean);
/*      */     
/*      */ 
/* 2289 */     LocationDetailTAABean locationDetailTAABean = new LocationDetailTAABean();
/*      */     
/*      */ 
/* 2292 */     locationDetailTAABean.setRecordType("TAA");
/* 2293 */     locationDetailTAABean.setRecordNumber("00000005");
/* 2294 */     locationDetailTAABean.setLocationName("XYZ Stores Nework");
/* 2295 */     locationDetailTAABean.setLocationAddress("XYZ Stores Nework");
/* 2296 */     locationDetailTAABean.setLocationCity("XYZ Stores Nework");
/* 2297 */     locationDetailTAABean.setLocationRegion("CA");
/* 2298 */     locationDetailTAABean.setLocationCountryCode("840");
/* 2299 */     locationDetailTAABean.setLocationPostalCode("Postalcode123");
/* 2300 */     locationDetailTAABean.setTransactionIdentifier("373872186011087");
/* 2301 */     locationDetailTAABean.setAddendaTypeCode("99");
/* 2302 */     locationDetailTAABean.setMerchantCategoryCode("4112");
/* 2303 */     locationDetailTAABean.setSellerId("SELLR123456789012345");
/* 2304 */     transactionAdviceBasicBean.setLocationDetailTAABean(locationDetailTAABean);
/* 2305 */     CPSLevel2Bean cpsl2Bean = new CPSLevel2Bean();
/* 2306 */     cpsl2Bean.setRecordType("TAA");
/* 2307 */     cpsl2Bean.setRecordNumber("04");
/* 2308 */     cpsl2Bean.setTransactionIdentifier("123456");
/* 2309 */     cpsl2Bean.setAddendaTypeCode("01");
/* 2310 */     cpsl2Bean.setRequesterName("Therisa");
/* 2311 */     cpsl2Bean.setChargeDescription1("Pen");
/* 2312 */     cpsl2Bean.setChargeItemQuantity1("1");
/* 2313 */     cpsl2Bean.setChargeItemAmount1("7300");
/* 2314 */     cpsl2Bean.setChargeDescription2("Pencil");
/* 2315 */     cpsl2Bean.setChargeItemQuantity2("4");
/* 2316 */     cpsl2Bean.setChargeItemAmount2("500");
/* 2317 */     cpsl2Bean.setChargeDescription3("Note Book");
/* 2318 */     cpsl2Bean.setChargeItemQuantity3("4");
/* 2319 */     cpsl2Bean.setChargeItemAmount3("500");
/* 2320 */     cpsl2Bean.setChargeDescription4("Note Pad");
/* 2321 */     cpsl2Bean.setChargeItemQuantity4("4");
/* 2322 */     cpsl2Bean.setChargeItemAmount4("4000");
/* 2323 */     cpsl2Bean.setCardMemberReferenceNumber("CARDNO123456789");
/* 2324 */     cpsl2Bean.setShipToPostalCode("SHIP0123456789");
/* 2325 */     cpsl2Bean.setTotalTaxAmount("10");
/* 2326 */     cpsl2Bean.setTaxTypeCode("56");
/*      */     
/* 2328 */     transactionAdviceBasicBean.setTransactionAdviceAddendumBean(cpsl2Bean);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 2335 */     TransactionBatchTrailerBean transactionBatchTrailerBean = new TransactionBatchTrailerBean();
/*      */     
/* 2337 */     transactionBatchTrailerBean.setMerchantId("5021045083");
/* 2338 */     transactionBatchTrailerBean.setRecordNumber("00000006");
/* 2339 */     transactionBatchTrailerBean.setRecordType("TBT");
/* 2340 */     transactionBatchTrailerBean.setTbtCurrencyCode("840");
/* 2341 */     transactionBatchTrailerBean.setTbtAmount("27300");
/* 2342 */     transactionBatchTrailerBean.setTbtAmountSign("+");
/* 2343 */     transactionBatchTrailerBean.setTbtCreationDate("21110402");
/* 2344 */     transactionBatchTrailerBean.setTbtIdentificationNumber("10000000000000010");
/* 2345 */     transactionBatchTrailerBean.setTbtImageSequenceNumber(" ");
/* 2346 */     transactionBatchTrailerBean.setTotalNoOfTabs("1");
/*      */     
/*      */ 
/* 2349 */     List<TransactionAdviceBasicBean> transactionAdviceBasicType = new ArrayList();
/*      */     
/* 2351 */     transactionAdviceBasicType.add(transactionAdviceBasicBean);
/*      */     
/* 2353 */     TransactionFileSummaryBean transactionFileSummaryBean = new TransactionFileSummaryBean();
/*      */     
/* 2355 */     transactionFileSummaryBean.setRecordType("TFS");
/* 2356 */     transactionFileSummaryBean.setRecordNumber("00000007");
/* 2357 */     transactionFileSummaryBean.setNumberOfDebits("1");
/* 2358 */     transactionFileSummaryBean.setHashTotalDebitAmount("500");
/* 2359 */     transactionFileSummaryBean.setNumberOfCredits("0");
/* 2360 */     transactionFileSummaryBean.setHashTotalCreditAmount("0");
/* 2361 */     transactionFileSummaryBean.setHashTotalAmount("500");
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 2367 */     TransactionTBTSpecificBean transactionTBTSpecificBean = new TransactionTBTSpecificBean();
/*      */     
/* 2369 */     transactionTBTSpecificBean.setTransactionAdviceBasicType(transactionAdviceBasicType);
/* 2370 */     transactionTBTSpecificBean.setTransactionBatchTrailerBean(transactionBatchTrailerBean);
/*      */     
/* 2372 */     List<TransactionTBTSpecificBean> transactionTBTSpecificType = new ArrayList();
/*      */     
/* 2374 */     transactionTBTSpecificType.add(transactionTBTSpecificBean);
/*      */     
/*      */ 
/*      */ 
/* 2378 */     SettlementRequestDataObject settlementRequestDataObject = new SettlementRequestDataObject();
/*      */     
/* 2380 */     settlementRequestDataObject
/* 2381 */       .setTransactionFileHeaderType(transactionFileHeaderBean);
/* 2382 */     settlementRequestDataObject
/* 2383 */       .setTransactionFileSummaryType(transactionFileSummaryBean);
/* 2384 */     settlementRequestDataObject
/* 2385 */       .setTransactionTBTSpecificType(transactionTBTSpecificType);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 2391 */     PropertyReader propertyReader = new PropertyReader();
/*      */     
/*      */ 
/* 2394 */     propertyReader = propertyReader.loadPropertyFile("/settlement.properties");
/*      */     
/*      */ 
/*      */ 
/*      */ 
/* 2399 */     SettlementResponseDataObject settlementResponseDataObject = RequestProcessorFactory.processRequest(settlementRequestDataObject, propertyReader);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */     try
/*      */     {
/* 2410 */       if (!settlementResponseDataObject.getActionCodeDescription().isEmpty())
/*      */       {
/* 2412 */         Iterator<?> iterater = settlementResponseDataObject.getActionCodeDescription().iterator();
/* 2413 */         while (iterater.hasNext()) {
/* 2414 */           ErrorObject errorObject = (ErrorObject)iterater.next();
/* 2415 */           System.out.print(errorObject.getErrorCode() + "-" + errorObject.getErrorDescription() + "\n");
/*      */         }
/*      */       } else {
/* 2418 */         System.out.print("Error List is empty\n");
/*      */       }
/*      */       
/*      */ 
/*      */     }
/*      */     catch (Exception e)
/*      */     {
/*      */ 
/* 2426 */       System.out.println("Error Object is Empty");
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void dppTest()
/*      */     throws Exception
/*      */   {
/* 2437 */     SimpleDateFormat datefmt = new SimpleDateFormat("MMdd");
/*      */     
/* 2439 */     SimpleDateFormat timefmt = new SimpleDateFormat("hhmm");
/* 2440 */     StringBuffer ab = new StringBuffer();
/* 2441 */     ab.append(datefmt.format(Calendar.getInstance().getTime()));
/* 2442 */     ab.append(timefmt.format(Calendar.getInstance().getTime()));
/*      */     
/*      */ 
/* 2445 */     TransactionFileHeaderBean transactionFileHeaderBean = new TransactionFileHeaderBean();
/*      */     
/* 2447 */     transactionFileHeaderBean.setRecordType("TFH");
/* 2448 */     transactionFileHeaderBean.setRecordNumber("00000001");
/* 2449 */     transactionFileHeaderBean.setSubmitterId("GWS002");
/*      */     
/* 2451 */     transactionFileHeaderBean.setSubmitterFileReferenceNumber("1" + ab.toString());
/*      */     
/* 2453 */     transactionFileHeaderBean.setFileCreationDate("20110523");
/* 2454 */     transactionFileHeaderBean.setFileCreationTime("172020");
/* 2455 */     transactionFileHeaderBean.setFileVersionNumber("12010000");
/*      */     
/*      */ 
/* 2458 */     TransactionAdviceBasicBean transactionAdviceBasicBean = new TransactionAdviceBasicBean();
/*      */     
/*      */ 
/*      */ 
/* 2462 */     transactionAdviceBasicBean.setRecordNumber("00000002");
/* 2463 */     transactionAdviceBasicBean.setRecordType("TAB");
/* 2464 */     transactionAdviceBasicBean.setTransactionIdentifier("373872186011087");
/* 2465 */     transactionAdviceBasicBean.setFormatCode("02");
/* 2466 */     transactionAdviceBasicBean.setMediaCode("01");
/* 2467 */     transactionAdviceBasicBean.setSubmissionMethod("");
/* 2468 */     transactionAdviceBasicBean.setApprovalCode("712389");
/* 2469 */     transactionAdviceBasicBean.setPrimaryAccountNumber("373953192351004");
/* 2470 */     transactionAdviceBasicBean.setCardExpiryDate("1405");
/* 2471 */     transactionAdviceBasicBean.setTransactionDate("21110402");
/* 2472 */     transactionAdviceBasicBean.setTransactionTime("143000");
/* 2473 */     transactionAdviceBasicBean.setTransactionAmount("143000");
/* 2474 */     transactionAdviceBasicBean.setProcessingCode("000000");
/* 2475 */     transactionAdviceBasicBean.setTransactionCurrencyCode("124");
/* 2476 */     transactionAdviceBasicBean.setExtendedPaymentData("01");
/* 2477 */     transactionAdviceBasicBean.setMerchantId("5021045083");
/* 2478 */     transactionAdviceBasicBean.setMerchantLocationId("CAN123456789012");
/* 2479 */     transactionAdviceBasicBean.setPosDataCode("161201554110");
/* 2480 */     transactionAdviceBasicBean.setInvoiceReferenceNumber("InvoiceNo123456");
/* 2481 */     transactionAdviceBasicBean.setElectronicCommerceIndicator("");
/* 2482 */     transactionAdviceBasicBean.setTerminalId("Ter12345");
/* 2483 */     transactionAdviceBasicBean.setTabImageSequenceNumber(" ");
/*      */     
/*      */ 
/*      */ 
/* 2487 */     TransactionAdviceDetailBean transactionAdviceDetailBean = new TransactionAdviceDetailBean();
/*      */     
/* 2489 */     transactionAdviceDetailBean.setRecordType("TAD");
/* 2490 */     transactionAdviceDetailBean.setRecordNumber("00000003");
/* 2491 */     transactionAdviceDetailBean.setTransactionIdentifier("373872186011087");
/* 2492 */     transactionAdviceDetailBean.setAdditionalAmountType1("11");
/* 2493 */     transactionAdviceDetailBean.setAdditionalAmount1("300");
/* 2494 */     transactionAdviceDetailBean.setAdditionalAmountSign1("+");
/* 2495 */     transactionAdviceDetailBean.setAdditionalAmountType2("12");
/* 2496 */     transactionAdviceDetailBean.setAdditionalAmount2("1000");
/* 2497 */     transactionAdviceDetailBean.setAdditionalAmountSign2("+");
/* 2498 */     transactionAdviceDetailBean.setAdditionalAmountType3("13");
/* 2499 */     transactionAdviceDetailBean.setAdditionalAmount3("1000");
/* 2500 */     transactionAdviceDetailBean.setAdditionalAmountSign3("+");
/* 2501 */     transactionAdviceDetailBean.setAdditionalAmountType4("14");
/* 2502 */     transactionAdviceDetailBean.setAdditionalAmount4("1000");
/* 2503 */     transactionAdviceDetailBean.setAdditionalAmountSign4("+");
/* 2504 */     transactionAdviceDetailBean.setAdditionalAmountType5("02");
/* 2505 */     transactionAdviceDetailBean.setAdditionalAmount5("1000");
/* 2506 */     transactionAdviceDetailBean.setAdditionalAmountSign5("+");
/*      */     
/* 2508 */     transactionAdviceBasicBean.setTransactionAdviceDetailBean(transactionAdviceDetailBean);
/*      */     
/*      */ 
/* 2511 */     LocationDetailTAABean locationDetailTAABean = new LocationDetailTAABean();
/*      */     
/*      */ 
/* 2514 */     locationDetailTAABean.setRecordType("TAA");
/* 2515 */     locationDetailTAABean.setRecordNumber("00000005");
/* 2516 */     locationDetailTAABean.setLocationName("XYZ Stores Nework");
/* 2517 */     locationDetailTAABean.setLocationAddress("XYZ Stores Nework");
/* 2518 */     locationDetailTAABean.setLocationCity("XYZ Stores Nework");
/* 2519 */     locationDetailTAABean.setLocationRegion("CA");
/* 2520 */     locationDetailTAABean.setLocationCountryCode("124");
/* 2521 */     locationDetailTAABean.setLocationPostalCode("Postalcode123");
/* 2522 */     locationDetailTAABean.setTransactionIdentifier("373872186011087");
/* 2523 */     locationDetailTAABean.setAddendaTypeCode("99");
/* 2524 */     locationDetailTAABean.setMerchantCategoryCode("4112");
/* 2525 */     locationDetailTAABean.setSellerId("SELLR123456789012345");
/* 2526 */     transactionAdviceBasicBean.setLocationDetailTAABean(locationDetailTAABean);
/* 2527 */     DeferredPaymentPlanBean dpPlanBean = new DeferredPaymentPlanBean();
/*      */     
/* 2529 */     dpPlanBean.setRecordType("TAA");
/* 2530 */     dpPlanBean.setRecordNumber("04");
/* 2531 */     dpPlanBean.setTransactionIdentifier("123456");
/* 2532 */     dpPlanBean.setAddendaTypeCode("01");
/* 2533 */     dpPlanBean.setRecordType("TAA");
/* 2534 */     dpPlanBean.setRecordNumber("04");
/* 2535 */     dpPlanBean.setTransactionIdentifier("123456");
/* 2536 */     dpPlanBean.setAddendaTypeCode("01");
/*      */     
/* 2538 */     dpPlanBean.setFullTransactionAmount("4300");
/* 2539 */     dpPlanBean.setTypeOfPlanCode("005");
/* 2540 */     dpPlanBean.setNoOfInstallments("100");
/* 2541 */     dpPlanBean.setAmountOfInstallment("002");
/* 2542 */     dpPlanBean.setInstallmentNumber("0001");
/* 2543 */     dpPlanBean.setContractNumber("0");
/* 2544 */     dpPlanBean.setPaymentTypeCode1(" ");
/* 2545 */     dpPlanBean.setPaymentTypeAmount1("000000000000");
/* 2546 */     dpPlanBean.setPaymentTypeCode2("000");
/* 2547 */     dpPlanBean.setPaymentTypeAmount2("000000000000");
/* 2548 */     dpPlanBean.setPaymentTypeCode3("000");
/* 2549 */     dpPlanBean.setPaymentTypeAmount3("000000000000");
/* 2550 */     dpPlanBean.setPaymentTypeCode4("000");
/* 2551 */     dpPlanBean.setPaymentTypeAmount4("000000000000");
/* 2552 */     dpPlanBean.setPaymentTypeCode5("000");
/* 2553 */     dpPlanBean.setPaymentTypeAmount5("000000000000");
/*      */     
/* 2555 */     transactionAdviceBasicBean.setTransactionAdviceAddendumBean(dpPlanBean);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 2563 */     TransactionBatchTrailerBean transactionBatchTrailerBean = new TransactionBatchTrailerBean();
/*      */     
/* 2565 */     transactionBatchTrailerBean.setMerchantId("5021045083");
/* 2566 */     transactionBatchTrailerBean.setRecordNumber("00000006");
/* 2567 */     transactionBatchTrailerBean.setRecordType("TBT");
/* 2568 */     transactionBatchTrailerBean.setTbtCurrencyCode("124");
/* 2569 */     transactionBatchTrailerBean.setTbtAmount("4300");
/* 2570 */     transactionBatchTrailerBean.setTbtAmountSign("+");
/* 2571 */     transactionBatchTrailerBean.setTbtCreationDate("21110402");
/* 2572 */     transactionBatchTrailerBean.setTbtIdentificationNumber("10000000000000011");
/* 2573 */     transactionBatchTrailerBean.setTbtImageSequenceNumber(" ");
/* 2574 */     transactionBatchTrailerBean.setTotalNoOfTabs("1");
/*      */     
/*      */ 
/* 2577 */     List<TransactionAdviceBasicBean> transactionAdviceBasicType = new ArrayList();
/*      */     
/* 2579 */     transactionAdviceBasicType.add(transactionAdviceBasicBean);
/*      */     
/* 2581 */     TransactionFileSummaryBean transactionFileSummaryBean = new TransactionFileSummaryBean();
/*      */     
/* 2583 */     transactionFileSummaryBean.setRecordType("TFS");
/* 2584 */     transactionFileSummaryBean.setRecordNumber("00000007");
/* 2585 */     transactionFileSummaryBean.setNumberOfDebits("1");
/* 2586 */     transactionFileSummaryBean.setHashTotalDebitAmount("500");
/* 2587 */     transactionFileSummaryBean.setNumberOfCredits("0");
/* 2588 */     transactionFileSummaryBean.setHashTotalCreditAmount("0");
/* 2589 */     transactionFileSummaryBean.setHashTotalAmount("500");
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 2595 */     TransactionTBTSpecificBean transactionTBTSpecificBean = new TransactionTBTSpecificBean();
/*      */     
/* 2597 */     transactionTBTSpecificBean.setTransactionAdviceBasicType(transactionAdviceBasicType);
/* 2598 */     transactionTBTSpecificBean.setTransactionBatchTrailerBean(transactionBatchTrailerBean);
/*      */     
/* 2600 */     List<TransactionTBTSpecificBean> transactionTBTSpecificType = new ArrayList();
/*      */     
/* 2602 */     transactionTBTSpecificType.add(transactionTBTSpecificBean);
/*      */     
/*      */ 
/*      */ 
/* 2606 */     SettlementRequestDataObject settlementRequestDataObject = new SettlementRequestDataObject();
/*      */     
/* 2608 */     settlementRequestDataObject
/* 2609 */       .setTransactionFileHeaderType(transactionFileHeaderBean);
/* 2610 */     settlementRequestDataObject
/* 2611 */       .setTransactionFileSummaryType(transactionFileSummaryBean);
/* 2612 */     settlementRequestDataObject
/* 2613 */       .setTransactionTBTSpecificType(transactionTBTSpecificType);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 2619 */     PropertyReader propertyReader = new PropertyReader();
/*      */     
/*      */ 
/* 2622 */     propertyReader = propertyReader.loadPropertyFile("/settlement.properties");
/*      */     
/*      */ 
/*      */ 
/*      */ 
/* 2627 */     SettlementResponseDataObject settlementResponseDataObject = RequestProcessorFactory.processRequest(settlementRequestDataObject, propertyReader);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */     try
/*      */     {
/* 2638 */       if (!settlementResponseDataObject.getActionCodeDescription().isEmpty())
/*      */       {
/* 2640 */         Iterator<?> iterater = settlementResponseDataObject.getActionCodeDescription().iterator();
/* 2641 */         while (iterater.hasNext()) {
/* 2642 */           ErrorObject errorObject = (ErrorObject)iterater.next();
/* 2643 */           System.out.print(errorObject.getErrorCode() + "-" + errorObject.getErrorDescription() + "\n");
/*      */         }
/*      */       } else {
/* 2646 */         System.out.print("Error List is empty\n");
/*      */       }
/*      */       
/*      */ 
/*      */     }
/*      */     catch (Exception e)
/*      */     {
/*      */ 
/* 2654 */       System.out.println("Error Object is Empty");
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public void emvTest()
/*      */     throws Exception
/*      */   {
/* 2666 */     SimpleDateFormat datefmt = new SimpleDateFormat("MMdd");
/*      */     
/* 2668 */     SimpleDateFormat timefmt = new SimpleDateFormat("hhmm");
/* 2669 */     StringBuffer ab = new StringBuffer();
/* 2670 */     ab.append(datefmt.format(Calendar.getInstance().getTime()));
/* 2671 */     ab.append(timefmt.format(Calendar.getInstance().getTime()));
/*      */     
/*      */ 
/* 2674 */     TransactionFileHeaderBean transactionFileHeaderBean = new TransactionFileHeaderBean();
/*      */     
/* 2676 */     transactionFileHeaderBean.setRecordType("TFH");
/* 2677 */     transactionFileHeaderBean.setRecordNumber("00000001");
/* 2678 */     transactionFileHeaderBean.setSubmitterId("GWS002");
/*      */     
/* 2680 */     transactionFileHeaderBean.setSubmitterFileReferenceNumber("1" + ab.toString());
/*      */     
/* 2682 */     transactionFileHeaderBean.setFileCreationDate("20110523");
/* 2683 */     transactionFileHeaderBean.setFileCreationTime("172020");
/* 2684 */     transactionFileHeaderBean.setFileVersionNumber("12010000");
/*      */     
/*      */ 
/* 2687 */     TransactionAdviceBasicBean transactionAdviceBasicBean = new TransactionAdviceBasicBean();
/*      */     
/*      */ 
/*      */ 
/* 2691 */     transactionAdviceBasicBean.setRecordNumber("00000002");
/* 2692 */     transactionAdviceBasicBean.setRecordType("TAB");
/* 2693 */     transactionAdviceBasicBean.setTransactionIdentifier("373872186011087");
/* 2694 */     transactionAdviceBasicBean.setFormatCode("02");
/* 2695 */     transactionAdviceBasicBean.setMediaCode("01");
/* 2696 */     transactionAdviceBasicBean.setSubmissionMethod("");
/* 2697 */     transactionAdviceBasicBean.setApprovalCode("712389");
/* 2698 */     transactionAdviceBasicBean.setPrimaryAccountNumber("373953192351004");
/* 2699 */     transactionAdviceBasicBean.setCardExpiryDate("1405");
/* 2700 */     transactionAdviceBasicBean.setTransactionDate("21110402");
/* 2701 */     transactionAdviceBasicBean.setTransactionTime("143000");
/* 2702 */     transactionAdviceBasicBean.setTransactionAmount("23900");
/* 2703 */     transactionAdviceBasicBean.setProcessingCode("000000");
/* 2704 */     transactionAdviceBasicBean.setTransactionCurrencyCode("124");
/* 2705 */     transactionAdviceBasicBean.setExtendedPaymentData("01");
/* 2706 */     transactionAdviceBasicBean.setMerchantId("5021045083");
/* 2707 */     transactionAdviceBasicBean.setMerchantLocationId("CAN123456789012");
/* 2708 */     transactionAdviceBasicBean.setPosDataCode("161201554110");
/* 2709 */     transactionAdviceBasicBean.setInvoiceReferenceNumber("InvoiceNo123456");
/* 2710 */     transactionAdviceBasicBean.setElectronicCommerceIndicator("");
/* 2711 */     transactionAdviceBasicBean.setTerminalId("Ter12345");
/* 2712 */     transactionAdviceBasicBean.setTabImageSequenceNumber(" ");
/*      */     
/*      */ 
/*      */ 
/* 2716 */     TransactionAdviceDetailBean transactionAdviceDetailBean = new TransactionAdviceDetailBean();
/*      */     
/* 2718 */     transactionAdviceDetailBean.setRecordType("TAD");
/* 2719 */     transactionAdviceDetailBean.setRecordNumber("00000003");
/* 2720 */     transactionAdviceDetailBean.setTransactionIdentifier("373872186011087");
/* 2721 */     transactionAdviceDetailBean.setAdditionalAmountType1("11");
/* 2722 */     transactionAdviceDetailBean.setAdditionalAmount1("3900");
/* 2723 */     transactionAdviceDetailBean.setAdditionalAmountSign1("+");
/* 2724 */     transactionAdviceDetailBean.setAdditionalAmountType2("12");
/* 2725 */     transactionAdviceDetailBean.setAdditionalAmount2("5000");
/* 2726 */     transactionAdviceDetailBean.setAdditionalAmountSign2("+");
/* 2727 */     transactionAdviceDetailBean.setAdditionalAmountType3("13");
/* 2728 */     transactionAdviceDetailBean.setAdditionalAmount3("5000");
/* 2729 */     transactionAdviceDetailBean.setAdditionalAmountSign3("+");
/* 2730 */     transactionAdviceDetailBean.setAdditionalAmountType4("14");
/* 2731 */     transactionAdviceDetailBean.setAdditionalAmount4("5000");
/* 2732 */     transactionAdviceDetailBean.setAdditionalAmountSign4("+");
/* 2733 */     transactionAdviceDetailBean.setAdditionalAmountType5("02");
/* 2734 */     transactionAdviceDetailBean.setAdditionalAmount5("5000");
/* 2735 */     transactionAdviceDetailBean.setAdditionalAmountSign5("+");
/*      */     
/* 2737 */     transactionAdviceBasicBean.setTransactionAdviceDetailBean(transactionAdviceDetailBean);
/*      */     
/*      */ 
/* 2740 */     LocationDetailTAABean locationDetailTAABean = new LocationDetailTAABean();
/*      */     
/*      */ 
/* 2743 */     locationDetailTAABean.setRecordType("TAA");
/* 2744 */     locationDetailTAABean.setRecordNumber("00000005");
/* 2745 */     locationDetailTAABean.setLocationName("XYZ Stores Nework");
/* 2746 */     locationDetailTAABean.setLocationAddress("XYZ Stores Nework");
/* 2747 */     locationDetailTAABean.setLocationCity("XYZ Stores Nework");
/* 2748 */     locationDetailTAABean.setLocationRegion("CA");
/* 2749 */     locationDetailTAABean.setLocationCountryCode("124");
/* 2750 */     locationDetailTAABean.setLocationPostalCode("Postalcode123");
/* 2751 */     locationDetailTAABean.setTransactionIdentifier("373872186011087");
/* 2752 */     locationDetailTAABean.setAddendaTypeCode("99");
/* 2753 */     locationDetailTAABean.setMerchantCategoryCode("4112");
/* 2754 */     locationDetailTAABean.setSellerId("SELLR123456789012345");
/* 2755 */     transactionAdviceBasicBean.setLocationDetailTAABean(locationDetailTAABean);
/* 2756 */     EMVBean emvBean = new EMVBean();
/*      */     
/* 2758 */     emvBean.setRecordType("TAA");
/* 2759 */     emvBean.setRecordNumber("04");
/* 2760 */     emvBean.setTransactionIdentifier("373872186011087");
/* 2761 */     emvBean.setAddendaTypeCode("07");
/* 2762 */     emvBean.setEMVFormatType("01");
/* 2763 */     emvBean.setFormatCode("");
/* 2764 */     emvBean.setICCSystemRelatedData("");
/*      */     
/* 2766 */     transactionAdviceBasicBean.setTransactionAdviceAddendumBean(emvBean);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 2774 */     TransactionBatchTrailerBean transactionBatchTrailerBean = new TransactionBatchTrailerBean();
/*      */     
/* 2776 */     transactionBatchTrailerBean.setMerchantId("5021045083");
/* 2777 */     transactionBatchTrailerBean.setRecordNumber("00000006");
/* 2778 */     transactionBatchTrailerBean.setRecordType("TBT");
/* 2779 */     transactionBatchTrailerBean.setTbtCurrencyCode("124");
/* 2780 */     transactionBatchTrailerBean.setTbtAmount("23900");
/* 2781 */     transactionBatchTrailerBean.setTbtAmountSign("+");
/* 2782 */     transactionBatchTrailerBean.setTbtCreationDate("21110402");
/* 2783 */     transactionBatchTrailerBean.setTbtIdentificationNumber("10000000000000012");
/* 2784 */     transactionBatchTrailerBean.setTbtImageSequenceNumber(" ");
/* 2785 */     transactionBatchTrailerBean.setTotalNoOfTabs("1");
/*      */     
/*      */ 
/* 2788 */     List<TransactionAdviceBasicBean> transactionAdviceBasicType = new ArrayList();
/*      */     
/* 2790 */     transactionAdviceBasicType.add(transactionAdviceBasicBean);
/*      */     
/* 2792 */     TransactionFileSummaryBean transactionFileSummaryBean = new TransactionFileSummaryBean();
/*      */     
/* 2794 */     transactionFileSummaryBean.setRecordType("TFS");
/* 2795 */     transactionFileSummaryBean.setRecordNumber("00000007");
/* 2796 */     transactionFileSummaryBean.setNumberOfDebits("1");
/* 2797 */     transactionFileSummaryBean.setHashTotalDebitAmount("500");
/* 2798 */     transactionFileSummaryBean.setNumberOfCredits("0");
/* 2799 */     transactionFileSummaryBean.setHashTotalCreditAmount("0");
/* 2800 */     transactionFileSummaryBean.setHashTotalAmount("500");
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 2806 */     TransactionTBTSpecificBean transactionTBTSpecificBean = new TransactionTBTSpecificBean();
/*      */     
/* 2808 */     transactionTBTSpecificBean.setTransactionAdviceBasicType(transactionAdviceBasicType);
/* 2809 */     transactionTBTSpecificBean.setTransactionBatchTrailerBean(transactionBatchTrailerBean);
/*      */     
/* 2811 */     List<TransactionTBTSpecificBean> transactionTBTSpecificType = new ArrayList();
/*      */     
/* 2813 */     transactionTBTSpecificType.add(transactionTBTSpecificBean);
/*      */     
/*      */ 
/*      */ 
/* 2817 */     SettlementRequestDataObject settlementRequestDataObject = new SettlementRequestDataObject();
/*      */     
/* 2819 */     settlementRequestDataObject
/* 2820 */       .setTransactionFileHeaderType(transactionFileHeaderBean);
/* 2821 */     settlementRequestDataObject
/* 2822 */       .setTransactionFileSummaryType(transactionFileSummaryBean);
/* 2823 */     settlementRequestDataObject
/* 2824 */       .setTransactionTBTSpecificType(transactionTBTSpecificType);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 2830 */     PropertyReader propertyReader = new PropertyReader();
/*      */     
/*      */ 
/* 2833 */     propertyReader = propertyReader.loadPropertyFile("/settlement.properties");
/*      */     
/*      */ 
/*      */ 
/*      */ 
/* 2838 */     SettlementResponseDataObject settlementResponseDataObject = RequestProcessorFactory.processRequest(settlementRequestDataObject, propertyReader);
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */     try
/*      */     {
/* 2849 */       if (!settlementResponseDataObject.getActionCodeDescription().isEmpty())
/*      */       {
/* 2851 */         Iterator<?> iterater = settlementResponseDataObject.getActionCodeDescription().iterator();
/* 2852 */         while (iterater.hasNext()) {
/* 2853 */           ErrorObject errorObject = (ErrorObject)iterater.next();
/* 2854 */           System.out.print(errorObject.getErrorCode() + "-" + errorObject.getErrorDescription() + "\n");
/*      */         }
/*      */       } else {
/* 2857 */         System.out.print("Error List is empty\n");
/*      */       }
/*      */       
/*      */ 
/*      */     }
/*      */     catch (Exception e)
/*      */     {
/*      */ 
/* 2865 */       System.out.println("Error Object is Empty");
/*      */     }
/*      */   }
/*      */   
/*      */   public void parseFile() throws FileNotFoundException
/*      */   {
/*      */     try {
/* 2872 */       FileInputStream fstream = new FileInputStream("D://GWS/Fast_Followers/Confrm_Report_Parser/TST004.GWS001.A#P07CAAN4038KNE");
/*      */       
/* 2874 */       DataInputStream in = new DataInputStream(fstream);
/* 2875 */       BufferedReader br = new BufferedReader(new InputStreamReader(in));
/*      */       
/* 2877 */       StringBuffer sb = new StringBuffer();
/* 2878 */       SettlementRequestDataObject settDataObject = new SettlementRequestDataObject();
/*      */       
/*      */       String strLine;
/*      */       
/* 2882 */       while ((strLine = br.readLine()) != null)
/*      */       {
/*      */         String strLine;
/* 2885 */         if (strLine.substring(5, 9).equalsIgnoreCase("TEST")) {
/* 2886 */           System.out.println("Time Stamp ==" + strLine.substring(32, 54));
/* 2887 */           System.out.println("Reference Number ==" + strLine.substring(77, 86));
/*      */         }
/* 2889 */         if (strLine.substring(5, 10).equalsIgnoreCase("Track")) {
/* 2890 */           System.out.println("Tracking Number ==" + strLine.substring(22, 36));
/*      */         }
/*      */       }
/*      */     }
/*      */     catch (Exception localException) {}
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static void setTFHRecord(String strLine, SettlementRequestDataObject settDataObject)
/*      */   {
/* 2941 */     List<String> ls = new ArrayList();
/*      */     
/* 2943 */     getDataFieldValue(strLine, ls);
/*      */     
/* 2945 */     TransactionFileHeaderBean tfhBean = new TransactionFileHeaderBean();
/* 2946 */     tfhBean.setRecordType((String)ls.get(0));
/* 2947 */     tfhBean.setRecordNumber((String)ls.get(1));
/* 2948 */     tfhBean.setSubmitterId((String)ls.get(2));
/* 2949 */     tfhBean.setSubmitterFileReferenceNumber((String)ls.get(3));
/* 2950 */     tfhBean.setSubmitterFileSequenceNumber((String)ls.get(4));
/* 2951 */     tfhBean.setFileCreationDate((String)ls.get(5));
/* 2952 */     tfhBean.setFileCreationTime((String)ls.get(6));
/* 2953 */     tfhBean.setFileVersionNumber((String)ls.get(7));
/*      */     
/*      */ 
/* 2956 */     settDataObject.setTransactionFileHeaderType(tfhBean);
/*      */   }
/*      */   
/*      */   public static String getDataFieldValue(String recordValue, List<String> ls)
/*      */   {
/* 2961 */     StringBuffer field = new StringBuffer();
/*      */     
/*      */ 
/*      */ 
/* 2965 */     char[] ca = recordValue.toCharArray();
/* 2966 */     char type; for (int i = 0; i < ca.length; i++)
/*      */     {
/* 2968 */       type = ca[i];
/* 2969 */       if (type != '<')
/*      */       {
/* 2971 */         if (type == '>')
/*      */         {
/* 2973 */           ls.add(field.toString().trim());
/*      */           
/* 2975 */           field.delete(0, field.length());
/*      */         } else {
/* 2977 */           field.append(type);
/*      */         }
/*      */       }
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 2988 */     for (String s : ls)
/*      */     {
/* 2990 */       System.out.println("list values ::" + s);
/*      */     }
/* 2992 */     return null;
/*      */   }
/*      */ }


/* Location:              C:\Users\Jesus\Documents\Trabajo\Addcel\Addcel\AMEX\settlementapi.jar!\SettlementTest.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */